// @ts-check

import webpack from "webpack";
import path from "node:path";
import { fileURLToPath } from "node:url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const nodeTarget = "node20.1";

/**
 * @type {import("webpack").Configuration}
 */
const configuration = {
  target: nodeTarget,
  mode: "production",
  devtool: "source-map",
  entry: {
    index: "./src/index.ts",
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  externalsPresets: {
    node: true,
  },
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js",
    clean: true,
  },
  optimization: {
    runtimeChunk: false,
  },
};

export default configuration;
