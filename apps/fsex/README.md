# `fsEx`

## Develop
1. Install dependencies:
   ```sh
   npm install
   ```
2. Build CLI:
   ```sh
   npx turbo run build --filter=fsex
   ```
3. Now it can be called with `npx fsex`.
