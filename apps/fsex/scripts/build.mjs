// @ts-check
import { execSync } from "node:child_process";
import { error as logError } from "node:console";
import path from "node:path";
import { cwd, env, execPath } from "node:process";
import { platform } from "node:os";
import fs from "node:fs/promises";

const isWindows = platform() === "win32";
const isMacOS = platform() === "darwin";

run().catch((error) => {
  logError(error);
  process.exitCode = 1;
});

async function run() {
  const buildPath = path.join(cwd(), "build");
  const distPath = path.join(cwd(), "dist");
  const callEnv = { ...env, NODE_ENV: "production" };
  const binaryName = isWindows ? "fsex.exe" : "fsex";
  const binaryBuildPath = path.join(buildPath, binaryName);
  const binaryDistPath = path.join(distPath, binaryName);
  const blobName = "sea-prep.blob";
  const blobPath = path.join(buildPath, blobName);
  const nodeSEAFuse = "NODE_SEA_FUSE_fce680ab2cc467b6e072b8b5df1996b2";

  execSync("webpack", { env: callEnv });
  execSync("node --experimental-sea-config sea-config.json");
  await copyNodeJSBinary(binaryBuildPath);
  await removeBinarySiganture(binaryBuildPath);
  await injectBlob(binaryBuildPath, blobPath, nodeSEAFuse);
  await fs.copyFile(binaryBuildPath, binaryDistPath)
}

/**
 *
 * @param {string} binaryBuildPath
 */
async function copyNodeJSBinary(binaryBuildPath) {
  await fs.copyFile(execPath, binaryBuildPath);
}

/**
 *
 * @param {string} binaryBuildPath
 */
async function removeBinarySiganture(binaryBuildPath) {
  if (isMacOS) {
    execSync(`codesign --remove-signature "${binaryBuildPath}"`);
  }

  if (isWindows) {
    execSync(`signtool remove /s ${binaryBuildPath}`);
  }
}

/**
 *
 * @param {string} binaryBuildPath
 * @param {string} blobPath
 * @param {string} nodeSEAFuse
 */
async function injectBlob(binaryBuildPath, blobPath, nodeSEAFuse) {
  if (isWindows) {
    execSync(`npx postject ${binaryBuildPath} NODE_SEA_BLOB ${blobPath} ^
      --sentinel-fuse ${nodeSEAFuse}`);
  } else if (isMacOS) {
    execSync(`npx postject ${binaryBuildPath} NODE_SEA_BLOB ${blobPath} \
    --sentinel-fuse ${nodeSEAFuse} \
    --macho-segment-name NODE_SEA`);
  } else {
    execSync(`npx postject ${binaryBuildPath} NODE_SEA_BLOB ${blobPath} \
    --sentinel-fuse ${nodeSEAFuse}`);
  }
}
