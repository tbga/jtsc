import { Command } from "@commander-js/extra-typings";
import packageJSON from "../../package.json";
import { symlinkCommand } from "./symlink";

export const program = new Command();
const name = packageJSON.name;
const description = packageJSON.description;
const version = packageJSON.version;

program
  .name(name)
  .description(description)
  .version(version, "--version", "Output the current version")
  .addCommand(symlinkCommand);
