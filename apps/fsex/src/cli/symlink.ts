import path from "node:path";
import fs from "node:fs/promises";
import { cwd } from "node:process";
import { Command } from "@commander-js/extra-typings";

export const symlinkCommand = new Command("symlink");

symlinkCommand
  .description(
    "Create a relative symlink at <link_path> pointing to <target_path>."
  )
  .argument(
    "<link_path>",
    "Path to create the link at. If the path is a folder, create a link within with the name of the <target_path>."
  )
  .argument("<target_path>", "Path to relatively refer to from symlink path.")
  .option(
    "--overwrite-link-path",
    "Overwrite existing link at the path. Only overwrites if the file at the path is a symlink."
  )
  .action(handleCommand);

async function handleCommand(
  link_path: string,
  target_path: string,
  options: { overwriteLinkPath?: boolean }
) {
  const isOverwritingPathLink = Boolean(options.overwriteLinkPath);
  const resolvedTargetPath = path.isAbsolute(target_path)
    ? target_path
    : path.resolve(cwd(), target_path);
  const resolvedLinkPath = await resolveLinkPath(link_path, resolvedTargetPath);
  const linkFolder = path.dirname(resolvedLinkPath);

  const relativePath = path.relative(linkFolder, resolvedTargetPath);

  await validateLinkPath(resolvedLinkPath, isOverwritingPathLink);
  if (isOverwritingPathLink) {
    await fs.rm(resolvedLinkPath, { force: true });
  }
  await fs.symlink(relativePath, resolvedLinkPath);

  const message = { resolvedLinkPath, resolvedTargetPath, relativePath };
  console.log(message);
}

async function resolveLinkPath(
  linkPath: string,
  targetPath: string
): Promise<string> {
  const absoluteLinkPath = path.isAbsolute(linkPath)
    ? linkPath
    : path.resolve(cwd(), linkPath);

  let resolvedLinkPath = absoluteLinkPath;
  try {
    const linkPathStat = await fs.lstat(absoluteLinkPath);

    if (linkPathStat.isDirectory()) {
      const name = path.basename(targetPath);

      resolvedLinkPath = path.join(absoluteLinkPath, name);
    }
  } catch (error) {
    // @TODO error filtering
  }

  await fs.mkdir(path.dirname(resolvedLinkPath), { recursive: true });

  return resolvedLinkPath;
}

async function validateLinkPath(
  link_path: string,
  isOverwritingPathLink: boolean
) {
  try {
    const linkPathStat = await fs.lstat(link_path);
    const isSymbolicLink = linkPathStat.isSymbolicLink();

    if (!isOverwritingPathLink) {
      throw new Error("Link path already exists.");
    }

    if (isOverwritingPathLink && !isSymbolicLink) {
      throw new Error("Link path already exists and is not a symbolic link.");
    }
  } catch (error) {
    // @TODO error filtering
    return;
  }
}
