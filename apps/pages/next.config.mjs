async function createNextConfig() {
  /** @type {import('next').NextConfig} */
  const nextConfig = {
    reactStrictMode: true,
    output: "export",
    trailingSlash: true,
    skipTrailingSlashRedirect: true,
    distDir: "dist",
    basePath: "/jtsc",
    transpilePackages: ["@repos/ui"],
  };

  return nextConfig;
}

export default createNextConfig;
