import { createContext, ReactNode, useContext } from "react";
import { useRouter } from "next/router";
import {
  useClient as useClientBase,
  ClientProvider as ClientProviderBase,
} from "@repo/ui/browser/hooks";

type IClientContext =
  | {
      isClient: false;
    }
  | { isClient: true; clientData: IClientData };

interface IClientData {
  isWebkitDirectorySupported: boolean;
}

const defaultContext: IClientContext = {
  isClient: false,
};

const ClientContext = createContext<IClientContext>(defaultContext);

export function ClientProvider({ children }: { children: ReactNode }) {
  const router = useRouter();
  const client = useClientBase();
  const { isReady } = router;

  return (
    <ClientProviderBase>
      <ClientContext.Provider
        value={
          !(isReady && client.isClient === true)
            ? {
                isClient: false,
              }
            : client
        }
      >
        {children}
      </ClientContext.Provider>
    </ClientProviderBase>
  );
}

export function useClient(): IClientContext {
  const context = useContext(ClientContext);

  return context;
}
