import { ReactNode } from "react";
import { AnchourExternal } from "@repo/ui/components/anchours";
import { AnchourInternal } from "#components/anchours";
import { createHomePageURL } from "#lib/urls";

import styles from "./layout.module.scss";
import { SITE_TITLE } from "#environment";

interface IProps {
  children?: ReactNode;
}

export function Layout({ children }: IProps) {
  return (
    <>
      <header className={styles.header}>
        <nav>
          <ul>
            <li>
              <AnchourInternal href={createHomePageURL()}>
                {SITE_TITLE}
              </AnchourInternal>
            </li>
          </ul>
        </nav>
      </header>

      <main className={styles.main}>{children}</main>

      <footer className={styles.footer}>
        <nav>
          <ul>
            <li>
              <AnchourExternal href="https://gitlab.com/the-coommectors/jtsc">
                Source Code
              </AnchourExternal>
            </li>
          </ul>
        </nav>
      </footer>
    </>
  );
}
