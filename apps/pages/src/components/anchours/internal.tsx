import { createBlockComponent, IBlockProps } from "@repo/ui/components/meta";
import { baseStyles } from "@repo/ui/components/anchours";
import Link, { LinkProps } from "next/link";

import styles from "./internal.module.scss";

interface IProps extends LinkProps, Omit<IBlockProps<"a">, "href"> {}

export const AnchourInternal: ReturnType<typeof createBlockComponent<IProps>> =
  createBlockComponent(
    [baseStyles.block as string, styles.block as string],
    Component
  );

function Component({ ...props }: IProps) {
  return <Link {...props} />;
}
