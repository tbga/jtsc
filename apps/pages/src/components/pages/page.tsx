import { ReactNode } from "react";
import Head from "next/head";
import { createBlockComponent, IBlockProps } from "@repo/ui/components/meta";
import { Heading } from "@repo/ui/components/headings";
import { SITE_TITLE } from "#environment";

import styles from "./page.module.scss";

interface IProps extends IBlockProps<"section"> {
  title: string;
  heading?: ReactNode;
  children?: ReactNode;
}

export const Page: ReturnType<typeof createBlockComponent<IProps>> =
  createBlockComponent(styles, Component);

function Component({ title, heading, children, ...blockProps }: IProps) {
  return (
    <>
      <Head>
        <title key="title">{`${title} | ${SITE_TITLE}`}</title>
      </Head>

      {heading && <Heading level={1}>{heading}</Heading>}

      <section {...blockProps}>{children}</section>
    </>
  );
}
