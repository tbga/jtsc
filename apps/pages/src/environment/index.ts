const NODE_ENV = process.env.NODE_ENV;
export const SITE_TITLE = process.env.NEXT_PUBLIC_SITE_TITLE;

export const IS_DEVELOPMENT = NODE_ENV === "development";
