import { useState } from "react";
import { IProgress } from "@repo/core/progress";
import {
  Article,
  ArticleBody,
  ArticleHeader,
} from "@repo/ui/components/articles";
import { FormClient, Output } from "@repo/ui/components/forms";
import { Page } from "#components/pages";
import {
  FormSectionFile,
  FormSectionFolder,
} from "@repo/ui/components/forms/sections";
import {
  ITorrentData,
  parseTorrentFile,
  TorrentOverview,
} from "#entities/torrents";
import { ProgressBar } from "@repo/ui/components/progress";
import { createFolderFromFiles } from "#entities/folders";

const acceptTypes = [".torrent", "application/x-bittorrent"] as const;

function TorrentViewerPage() {
  const [torrentData, changeTorrentData] = useState<ITorrentData>();
  const [progress, changeProgress] = useState<IProgress>();
  const title = "Torrent viewer";
  const heading = "Torrent Viewer";
  const formID = "torrent-viewer";
  const progressID = "torrent-progress";

  async function handleFileParsing(data: FormData) {
    const torrentFile = data.get("file") as File | null;
    const folderFiles = data.getAll("folder") as File[];

    if (!torrentFile) {
      throw new Error("No file is selected.");
    }

    try {
      const total =
        torrentFile.size +
        folderFiles.reduce((totalSize, { size }) => totalSize + size, 0);
      let current = 0;

      changeProgress({ current, total });

      const folder = await createFolderFromFiles(
        folderFiles,
        async (currentSize) => {
          current = currentSize;
          changeProgress({ current, total });
        },
      );

      const torrentData = await parseTorrentFile(torrentFile, folder);
      current = current + torrentFile.size;

      changeProgress({ current, total });

      changeTorrentData(torrentData);
    } finally {
      changeProgress(undefined);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article headingLevel={2}>
        {() => (
          <>
            <ArticleHeader>
              <FormClient
                id={formID}
                submitButton="Parse"
                onSubmit={async (event) => {
                  const form = event.currentTarget;
                  const formData = new FormData(form);
                  await handleFileParsing(formData);
                }}
              >
                {(formID) => (
                  <>
                    <FormSectionFile
                      name="file"
                      label={<>File</>}
                      id={`${formID}-file`}
                      form={formID}
                      accept={acceptTypes}
                      required
                    >
                      Select a `.torrent` file.
                    </FormSectionFile>

                    <FormSectionFolder
                      name="folder"
                      label={<>Folder</>}
                      id={`${formID}-folder`}
                      form={formID}
                    >
                      Select a folder which holds files from this torrent.
                    </FormSectionFolder>
                  </>
                )}
              </FormClient>
            </ArticleHeader>

            <ArticleBody>
              <Output form={formID}>
                {progress ? (
                  <ProgressBar
                    id={progressID}
                    progress={progress}
                    label={"Analyzing files..."}
                  />
                ) : (
                  torrentData && (
                    <TorrentOverview
                      key={torrentData.infoHash}
                      torrentData={torrentData}
                    />
                  )
                )}
              </Output>
            </ArticleBody>
          </>
        )}
      </Article>
    </Page>
  );
}

export default TorrentViewerPage;
