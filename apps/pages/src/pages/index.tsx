import { useEffect } from "react";
import { sleep } from "@repo/core/utils";
import {
  Article,
  ArticleHeader,
} from "@repo/ui/components/articles";
import {
  createFolderViewerPageURL,
  createTorrentViewerPageURL,
} from "#lib/urls";
import { Page } from "#components/pages";
import { AnchourInternal } from "#components/anchours";
import { ListItem, ListUnordered } from "@repo/ui/components/lists";

function HomePage() {
  const title = "Navigation";
  const heading = "Navigation";

  useEffect(() => {
    sleep(1500).then(() => console.log("Hello from core lib."));
  }, []);

  return (
    <Page title={title} heading={heading}>
      <Article headingLevel={2}>
        {() => (
          <>
            <ArticleHeader>
              <ListUnordered>
                <ListItem>
                  <AnchourInternal href={createTorrentViewerPageURL()}>
                    Torrent Viewer
                  </AnchourInternal>
                </ListItem>
                <ListItem>
                  <AnchourInternal href={createFolderViewerPageURL()}>
                    Folder Viewer
                  </AnchourInternal>
                </ListItem>
              </ListUnordered>
            </ArticleHeader>
          </>
        )}
      </Article>
    </Page>
  );
}

export default HomePage;
