import { FormEvent, useState } from "react";
import { IProgress } from "@repo/core/progress";
import {
  Article,
  ArticleBody,
  ArticleHeader,
} from "@repo/ui/components/articles";
import { FormClient } from "@repo/ui/components/forms";
import { FormSectionFolder } from "@repo/ui/components/forms/sections";
import { ProgressBar } from "@repo/ui/components/progress";
import { NumberFormatted, SizeFormatted } from "@repo/ui/components/formatting";
import { Page } from "#components/pages";
import {
  createFolderFromFiles,
  FolderView,
  IWebkitFolder,
} from "#entities/folders";

interface IParseProgress extends IProgress<IParseData> {}

interface IParseData {
  currentFilesCount: number;
  totalFilesCount: number;
}

function FolderViewerPage() {
  const [progress, changeProgress] = useState<IParseProgress>();
  const [folder, changeFolder] = useState<IWebkitFolder>();
  const title = "Folder viewer";
  const heading = "Folder Viewer";
  const formID = "folder-viewer";
  const progressID = "folder-progress";

  async function handleProgress(
    current: number,
    total: number,
    data: IParseData,
  ) {
    changeProgress({ current, total, data });
  }

  async function handleSubmit(event: FormEvent<HTMLFormElement>) {
    const form = event.currentTarget;
    const formData = new FormData(form);
    const files = formData.getAll("folder") as File[];

    const nextFolder = await createFolderFromFiles(files, handleProgress);

    changeFolder(nextFolder);

    changeProgress(undefined);
  }

  return (
    <Page title={title} heading={heading}>
      <Article headingLevel={2}>
        {() => (
          <>
            <ArticleHeader>
              <FormClient
                id={formID}
                submitButton="Parse"
                onSubmit={handleSubmit}
              >
                {(formID) => (
                  <>
                    <FormSectionFolder
                      name="folder"
                      label={<>Folder</>}
                      id={`${formID}-folder`}
                      form={formID}
                    >
                      Select a folder.
                    </FormSectionFolder>
                  </>
                )}
              </FormClient>
            </ArticleHeader>

            <ArticleBody>
              {progress ? (
                <ProgressBar<IParseData>
                  id={progressID}
                  progress={progress}
                  label={(currentSize, totalSize, data) => (
                    <div>
                      Analyzing files:{" "}
                      <span>
                        <NumberFormatted value={data!.currentFilesCount} /> (
                        <SizeFormatted value={currentSize} />)
                      </span>{" "}
                      out of{" "}
                      <span>
                        <NumberFormatted value={data!.totalFilesCount} /> (
                        <SizeFormatted value={totalSize} />)
                      </span>
                    </div>
                  )}
                />
              ) : (
                folder && <FolderView folder={folder} />
              )}
            </ArticleBody>
          </>
        )}
      </Article>
    </Page>
  );
}

export default FolderViewerPage;
