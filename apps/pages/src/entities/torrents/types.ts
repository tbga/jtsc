import { Instance } from "parse-torrent";
import { GIGABYTE } from "@repo/core/numbers";
import { IElementType } from "@repo/core/types";
import { IFile } from "#entities/folders";

export interface ITorrentData extends Omit<Instance, "files"> {
  comment?: string;
  private?: boolean;
  files?: ITorrentFiles;
  stats: ITorrentStats;
}

export interface ITorrentStats {
  portability:
    | "portable"
    | "network_portable"
    | "device_portable"
    | "not_portable";
  mixedCase: number;
  nonPortableSize: number;
  largestFileSize: number
  smallestFileSize: number
}

export interface ITorrentFiles extends Map<string, ITorrentFile> {}

export interface ITorrentFile
  extends IElementType<Required<Instance>["files"]>,
    Pick<Partial<File>, "type">,
    Pick<Partial<IFile>, "hashSHA256"> {
  isMixedCase: boolean;
  isPortableSize: boolean;
}

export interface ITorrentFilesFolderView
  extends Map<string, ITorrentFileEntry> {}

type ITorrentFileEntry = ITorrentFileFolder | ITorrentFileFile;

interface ITorrentFileFolder {
  type: "folder";
  data: {
    path: string;
    name: string;
  };
}

interface ITorrentFileFile {
  type: "file";
  data: ITorrentFile;
}

export const portableSize = 4 * GIGABYTE;
