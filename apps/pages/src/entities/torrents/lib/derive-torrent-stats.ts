import { ITorrentFiles, ITorrentStats } from "../types";

export function deriveTorrentStats(torrentFiles: ITorrentFiles): ITorrentStats {
  let mixedCase = 0;
  let nonPortableSize = 0;
  let smallestFileSize = 0;
  let largestFileSize = 0;

  for (const file of torrentFiles.values()) {
    smallestFileSize =
      smallestFileSize === 0
        ? file.length
        : Math.min(smallestFileSize, file.length);
    largestFileSize = Math.max(largestFileSize, file.length);

    if (file.isMixedCase) {
      mixedCase = mixedCase + 1;
    }

    if (!file.isPortableSize) {
      nonPortableSize = nonPortableSize + 1;
    }
  }

  const portability: ITorrentStats["portability"] =
    mixedCase === 0 && nonPortableSize === 0
      ? "portable"
      : mixedCase !== 0 && nonPortableSize !== 0
        ? "not_portable"
        : mixedCase === 0 && nonPortableSize !== 0
          ? "network_portable"
          : "device_portable";

  const stats: ITorrentStats = {
    portability,
    mixedCase,
    nonPortableSize,
    smallestFileSize,
    largestFileSize,
  };

  return stats;
}
