import { Instance } from "parse-torrent";
import { isMixedCase } from "@repo/core/strings";
import { IWebkitFolder } from "#entities/folders";
import { ITorrentFile, ITorrentFiles, portableSize } from "../types";

export async function parseFilesOfTorrent(
  files: Required<Instance>["files"],
  folder?: IWebkitFolder,
): Promise<ITorrentFiles> {
  const torrentFiles: ITorrentFiles = new Map();

  for (const file of files) {
    const folderFile = folder?.entries.get(file.path);
    const isMixed = isMixedCase(file.path);
    const isPortableSize = file.length <= portableSize;
    const mergedFile: ITorrentFile = !folderFile
      ? { ...file, isMixedCase: isMixed, isPortableSize }
      : {
          ...file,
          isMixedCase: isMixed,
          isPortableSize,
          hashSHA256: folderFile.hashSHA256,
          type: folderFile.data.type,
        };

    torrentFiles.set(file.path, mergedFile);
  }

  return torrentFiles;
}
