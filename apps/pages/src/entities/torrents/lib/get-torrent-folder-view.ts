import { countOccurencesOfCharacter } from "@repo/core/strings";
import { ITorrentFiles, ITorrentFilesFolderView } from "../types";

export function getTorrentFolderView(
  files: ITorrentFiles,
  folderPath: string,
): ITorrentFilesFolderView {
  const folderView: ITorrentFilesFolderView = new Map();

  for (const [filePath, file] of files) {
    const isSubpath = file.path.includes(folderPath);

    if (!isSubpath) {
      continue;
    }

    const subpath = file.path.slice(folderPath.length + 1);
    const isFile = countOccurencesOfCharacter(subpath, "/") === 0;

    if (!isFile) {
      const name = subpath.slice(0, subpath.indexOf("/"));
      const path = `${folderPath}/${name}`;
      folderView.set(path, { type: "folder", data: { path, name } });
    } else {
      folderView.set(filePath, { type: "file", data: file });
    }
  }

  return folderView;
}
