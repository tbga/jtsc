import ParseTorrent, { Instance } from "parse-torrent";
import { IWebkitFolder } from "#entities/folders";
import { ITorrentData, ITorrentStats } from "../types";
import { parseFilesOfTorrent } from "./parse-files-fo-torrent";
import { deriveTorrentStats } from "./derive-torrent-stats";

export async function parseTorrentFile(
  file: File,
  folder?: IWebkitFolder,
): Promise<ITorrentData> {
  const buffer = await file.arrayBuffer();
  const fileBuffer = Buffer.from(buffer);
  const { files, ...data } = (await ParseTorrent(fileBuffer)) as Instance;
  const parsedFiles = !files
    ? undefined
    : await parseFilesOfTorrent(files, folder);
  const stats: ITorrentStats = !parsedFiles
    ? {
        portability: "not_portable",
        mixedCase: 0,
        nonPortableSize: 0,
        smallestFileSize: 0,
        largestFileSize: 0,
      }
    : deriveTorrentStats(parsedFiles);
  const result: ITorrentData = {
    ...data,
    files: parsedFiles,
    stats,
  };

  return result;
}
