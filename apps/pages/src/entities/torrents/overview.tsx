import { useState } from "react";
import { createBlockComponent } from "@repo/ui/components/meta";
import {
  DescriptionList,
  DescriptionSection,
  IDescriptionListProps,
  ListItem,
  ListUnordered,
} from "@repo/ui/components/lists";
import {
  NumberFormatted,
  Pre,
  SizeFormatted,
} from "@repo/ui/components/formatting";
import { Details } from "@repo/ui/components/details";
import { Button } from "@repo/ui/components/buttons";
import { getTorrentFolderView } from "./lib/get-torrent-folder-view";
import { ITorrentData, portableSize } from "./types";
import { Portability } from "./portability";

interface IProps extends IDescriptionListProps {
  torrentData: ITorrentData;
}

export const TorrentOverview: ReturnType<typeof createBlockComponent<IProps>> =
  createBlockComponent(undefined, Component);

function Component({ torrentData, ...props }: IProps) {
  const {
    name,
    infoHash,
    created,
    createdBy,
    comment,
    announce,
    files,
    length,
    pieceLength,
    pieces,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    info,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    infoBuffer,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    infoHashBuffer,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    lastPieceLength,
    private: privacyState,
    urlList,
    stats,
    ...otherKeys
  } = torrentData;
  const hashKeys = !info ? undefined : Object.keys(info);
  const unknownKeys = Object.keys(otherKeys);

  return (
    <DescriptionList {...props}>
      <DescriptionSection dKey="Title" dValue={<Pre>{name}</Pre>} />

      <DescriptionSection dKey="Info hash" dValue={<Pre>{infoHash}</Pre>} />

      {hashKeys && (
        <DescriptionSection
          dKey="Info hash keys"
          dValue={
            <Details summary={<NumberFormatted value={hashKeys.length} />}>
              <ListUnordered>
                {hashKeys.map((key, index) => (
                  <ListItem key={`${index}-${key}`}>
                    <Pre>{key}</Pre>
                  </ListItem>
                ))}
              </ListUnordered>
            </Details>
          }
        />
      )}

      <DescriptionSection
        dKey="Portability"
        dValue={
          <Details summary={<Portability portability={stats.portability} />}>
            <ListUnordered>
              <ListItem>
                {stats.mixedCase === 0 ? (
                  <>All file paths are lower case.</>
                ) : (
                  <>
                    <NumberFormatted value={stats.mixedCase} /> file paths are
                    mixed case, which can lead to problems transferring files
                    between case-aware systems like Linux and case-agnostic ones
                    like Windows.
                  </>
                )}
              </ListItem>
              <ListItem>
                {stats.nonPortableSize === 0 ? (
                  <>
                    All files in the torrent are within a portable size and
                    therefore can be transferred on portable drives.
                  </>
                ) : (
                  <>
                    {stats.nonPortableSize} files in this torrent are above the
                    portable size of <SizeFormatted value={portableSize} /> and
                    therefore it cannot be transferred on portable drives
                    without some transformation or relying on a non-portable
                    file system of the drive.
                  </>
                )}
              </ListItem>
            </ListUnordered>
          </Details>
        }
      />

      <DescriptionSection
        dKey="Creation date"
        dValue={created && <Pre>{created.toISOString()}</Pre>}
      />

      {createdBy && (
        <DescriptionSection
          dKey="Creator"
          dValue={createdBy && <Pre>{createdBy}</Pre>}
        />
      )}

      {comment && (
        <DescriptionSection
          dKey="Comment"
          dValue={comment && <Pre>{comment}</Pre>}
        />
      )}

      {privacyState && (
        <DescriptionSection
          dKey="Private"
          dValue={<Pre>{String(privacyState)}</Pre>}
          isHorizontal
        />
      )}

      {announce && announce.length !== 0 && (
        <DescriptionSection
          dKey="Trackers"
          dValue={announce && <Trackers trackers={announce} />}
        />
      )}

      {urlList && urlList.length !== 0 && (
        <DescriptionSection
          dKey="URL List"
          dValue={
            <ListUnordered>
              {urlList.map((url, index) => (
                <ListItem key={`${index}-${url}`}>{url}</ListItem>
              ))}
            </ListUnordered>
          }
        />
      )}

      <DescriptionSection
        dKey="Size"
        dValue={length && <SizeFormatted value={length} />}
        isHorizontal
      />

      <DescriptionSection
        dKey="Smallest file size"
        dValue={<SizeFormatted value={stats.smallestFileSize} />}
        isHorizontal
      />

      <DescriptionSection
        dKey="Largest file size"
        dValue={<SizeFormatted value={stats.largestFileSize} />}
        isHorizontal
      />

      <DescriptionSection
        dKey="Piece size"
        dValue={pieceLength && <SizeFormatted value={pieceLength} />}
        isHorizontal
      />

      <DescriptionSection
        dKey="Pieces"
        dValue={pieces && <NumberFormatted value={pieces.length} />}
        isHorizontal
      />

      {unknownKeys.length !== 0 && (
        <DescriptionSection
          dKey="Unknown keys"
          dValue={
            <>
              <ListUnordered>
                {unknownKeys.map((key) => (
                  <ListItem key={key}>
                    <Pre>{key}</Pre>
                  </ListItem>
                ))}
              </ListUnordered>

              <Button onClick={() => console.log(otherKeys)}>
                Print in console
              </Button>
            </>
          }
        />
      )}

      <DescriptionSection
        dKey="Files"
        dValue={files && <Files files={files} />}
      />
    </DescriptionList>
  );
}

interface ITrackersProps {
  trackers: Required<ITorrentData>["announce"];
}

function Trackers({ trackers }: ITrackersProps) {
  return (
    <Details summary={<NumberFormatted value={trackers.length} />}>
      <ListUnordered>
        {trackers.map((tracker, index) => (
          <ListItem key={`${index}-${tracker}`}>
            <Pre>{tracker}</Pre>
          </ListItem>
        ))}
      </ListUnordered>
    </Details>
  );
}

interface IFilesProps {
  files: Required<ITorrentData>["files"];
}

function Files({ files }: IFilesProps) {
  const root = files.values().next().value!.path.split("/")[0]!;
  const [currentFolder, changeCurrentFolder] = useState(root);
  const folderView = getTorrentFolderView(files, currentFolder);
  const compare = new Intl.Collator().compare;
  const entries = Array.from(folderView.values()).sort((prev, next) => {
    const result =
      prev.type === next.type
        ? compare(prev.data.name, next.data.name)
        : prev.type === "folder"
          ? -1
          : 1;
    return result;
  });

  return (
    <Details summary={<NumberFormatted value={files.size} />}>
      <DescriptionList>
        <DescriptionSection
          dKey="Folder"
          dValue={
            <FolderPath
              path={currentFolder}
              onSelect={async (path) => changeCurrentFolder(path)}
            />
          }
        />
        <DescriptionSection
          dKey="Entries"
          dValue={
            <Details summary={<Pre>{entries.length}</Pre>}>
              <ListUnordered>
                {entries.map((entry) =>
                  entry.type === "folder" ? (
                    <ListItem key={`${entry.data.path}`}>
                      <Button
                        onClick={async () =>
                          changeCurrentFolder(entry.data.path)
                        }
                      >
                        <Pre>{entry.data.name}</Pre>
                      </Button>
                    </ListItem>
                  ) : (
                    <ListItem key={`${entry.data.path}`}>
                      <Details summary={<Pre>{entry.data.name}</Pre>}>
                        <DescriptionList>
                          {entry.data.hashSHA256 && (
                            <DescriptionSection
                              dKey="Hash SHA256"
                              dValue={<Pre>{entry.data.hashSHA256}</Pre>}
                            />
                          )}

                          {entry.data.type && (
                            <DescriptionSection
                              dKey="Media Type"
                              dValue={<Pre>{entry.data.type}</Pre>}
                              isHorizontal
                            />
                          )}

                          <DescriptionSection
                            dKey="Size"
                            dValue={<SizeFormatted value={entry.data.length} />}
                            isHorizontal
                          />
                        </DescriptionList>
                      </Details>
                    </ListItem>
                  ),
                )}
              </ListUnordered>
            </Details>
          }
        />
      </DescriptionList>
    </Details>
  );
}

interface IFolderPathProps {
  path: string;
  onSelect: (path: string) => Promise<void>;
}

function FolderPath({ path, onSelect }: IFolderPathProps) {
  const segments = path
    .split("/")
    .reduce<[string, string][]>((mapping, segment, index, segments) => {
      const segmentPath = segments.slice(0, index + 1).join("/");
      const name = segment;

      mapping.push([segmentPath, `/${name}`]);

      return mapping;
    }, []);

  return (
    <Details summary={<Pre>{path}</Pre>}>
      <ListUnordered>
        {segments.map(([segmentPath, name]) => (
          <ListItem key={segmentPath}>
            {segmentPath === path ? (
              <Pre>{name}</Pre>
            ) : (
              <Button onClick={async () => await onSelect(segmentPath)}>
                <Pre>{name}</Pre>
              </Button>
            )}
          </ListItem>
        ))}
      </ListUnordered>
    </Details>
  );
}
