import { ITorrentStats } from "./types";

import styles from "./portability.module.scss";

interface IProps {
  portability: ITorrentStats["portability"];
}

export function Portability({ portability }: IProps) {
  switch (portability) {
    case "portable": {
      return <span className={styles.portable}>Portable</span>;
    }
    case "network_portable": {
      return <span className={styles.network_portable}>Network portable</span>;
    }
    case "device_portable": {
      return <span className={styles.device_portable}>Device portable</span>;
    }
    case "not_portable": {
      return <span className={styles.not_portable}>Not portable</span>;
    }

    default: {
      throw new Error(
        `Unknown value of portability "${portability satisfies never}".`,
      );
    }
  }
}
