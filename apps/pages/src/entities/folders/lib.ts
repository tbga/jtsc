import { countOccurencesOfCharacter } from "@repo/core/strings";
import { getHashSHA256 } from "@repo/core/hashing";
import type { IWebkitFolder, IFolderView } from "./types";

interface IParseData {
  currentFilesCount: number;
  totalFilesCount: number;
}

export async function createFolderFromFiles(
  files: File[],
  onProgress?: (
    current: number,
    total: number,
    data: IParseData,
  ) => Promise<void>,
): Promise<IWebkitFolder> {
  if (files.length === 0) {
    throw new Error("The folder is empty.");
  }

  const basePath = files[0]!.webkitRelativePath;
  const root = basePath.slice(0, basePath.indexOf("/"));
  const entries: IWebkitFolder["entries"] = new Map();
  const total = files.length;
  let currentSize = 0;
  let currentFileCount = 0;
  const totalSize = files.reduce((total, { size }) => {
    return total + size;
  }, 0);

  if (onProgress) {
    await onProgress(currentSize, totalSize, {
      currentFilesCount: currentFileCount,
      totalFilesCount: total,
    });
  }

  for await (const file of files) {
    const hashSHA256 = await getHashSHA256(file);
    entries.set(file.webkitRelativePath, {
      hashSHA256,
      name: file.name,
      data: file,
    });

    currentSize = currentSize + file.size;
    currentFileCount = currentFileCount + 1;

    if (onProgress) {
      await onProgress(currentSize, totalSize, {
        currentFilesCount: currentFileCount,
        totalFilesCount: total,
      });
    }
  }

  const folder: IWebkitFolder = {
    root,
    totalSize,
    entries,
  };

  return folder;
}

export function getViewOfFolder(
  folder: IWebkitFolder,
  folderPath: string,
): IFolderView {
  const folderView: IFolderView = new Map();

  for (const file of folder.entries.values()) {
    const isSubpath = file.data.webkitRelativePath.startsWith(folderPath);

    if (!isSubpath) {
      continue;
    }

    const subPath = file.data.webkitRelativePath.slice(folderPath.length + 1);
    const isFile = countOccurencesOfCharacter(subPath, "/") === 0;

    if (isFile) {
      folderView.set(subPath, { type: "file", data: file });
      continue;
    }

    const name = subPath.slice(0, subPath.indexOf("/"));
    const path = `${folderPath}/${name}`;
    folderView.set(name, { type: "folder", data: { path, name } });
  }

  return folderView;
}
