import { useState } from "react";
import {
  DescriptionList,
  DescriptionSection,
  ListItem,
  ListUnordered,
} from "@repo/ui/components/lists";
import { Pre, SizeFormatted } from "@repo/ui/components/formatting";
import { Details } from "@repo/ui/components/details";
import { Button } from "@repo/ui/components/buttons";
import { getViewOfFolder } from "./lib";
import { IWebkitFolder, IFile } from "./types";

interface IProps {
  folder: IWebkitFolder;
}

export function FolderView({ folder }: IProps) {
  const root = folder.entries
    .values()
    .next()
    .value!.data.webkitRelativePath.split("/")[0]!;
  const [currentFolder, changeCurrentFolder] = useState(root);
  const folderView = getViewOfFolder(folder, currentFolder);
  const compare = new Intl.Collator().compare;
  const entries = Array.from(folderView.values()).sort((prev, next) => {
    const result =
      prev.type === next.type
        ? compare(prev.data.name, next.data.name)
        : prev.type === "folder"
          ? -1
          : 1;
    return result;
  });

  return (
    <DescriptionList>
      <DescriptionSection
        dKey="Folder"
        dValue={
          <FolderPath
            path={currentFolder}
            onSelect={async (path) => changeCurrentFolder(path)}
          />
        }
      />
      <DescriptionSection
        dKey="Entries"
        dValue={<Pre>{folderView.size}</Pre>}
        isHorizontal
      />
      <DescriptionSection
        dKey="Entries rundown"
        dValue={
          <ListUnordered>
            {entries.map((entry) =>
              entry.type === "file" ? (
                <ListItem key={entry.data.data.webkitRelativePath}>
                  <File file={entry.data} />
                </ListItem>
              ) : (
                <ListItem key={entry.data.path}>
                  <Folder
                    path={entry.data.path}
                    name={entry.data.name}
                    onSelect={async (path) => changeCurrentFolder(path)}
                  />
                </ListItem>
              ),
            )}
          </ListUnordered>
        }
      />
    </DescriptionList>
  );
}

interface IFolderPathProps {
  path: string;
  onSelect: (path: string) => Promise<void>;
}

function FolderPath({ path, onSelect }: IFolderPathProps) {
  const segments = path
    .split("/")
    .reduce<[string, string][]>((mapping, segment, index, segments) => {
      const segmentPath = segments.slice(0, index + 1).join("/");
      const name = segment;

      mapping.push([segmentPath, `/${name}`]);

      return mapping;
    }, []);

  return (
    <Details summary={<Pre>{path}</Pre>}>
      <ListUnordered>
        {segments.map(([segmentPath, name]) => (
          <ListItem key={segmentPath}>
            {segmentPath === path ? (
              <Pre>{name}</Pre>
            ) : (
              <Button onClick={async () => await onSelect(segmentPath)}>
                <Pre>{name}</Pre>
              </Button>
            )}
          </ListItem>
        ))}
      </ListUnordered>
    </Details>
  );
}

interface IFileProps {
  file: IFile;
}

function File({ file }: IFileProps) {
  const { hashSHA256, data } = file;
  const { name, type, size, lastModified } = data;

  return (
    <Details summary={<Pre>{name}</Pre>}>
      <DescriptionList>
        <DescriptionSection
          dKey="Hash SHA256"
          dValue={<Pre>{hashSHA256}</Pre>}
        />
        <DescriptionSection
          dKey="Media Type"
          dValue={<Pre>{type}</Pre>}
          isHorizontal
        />
        <DescriptionSection
          dKey="Size"
          dValue={<SizeFormatted value={size} />}
          isHorizontal
        />
        <DescriptionSection
          dKey="Last Modified"
          dValue={<Pre>{new Date(lastModified).toISOString()}</Pre>}
          isHorizontal
        />
      </DescriptionList>
    </Details>
  );
}

interface IFolderProps {
  path: string;
  name: string;

  onSelect: (path: string) => Promise<void>;
}

function Folder({ path, name, onSelect }: IFolderProps) {
  return (
    <Button onClick={async () => await onSelect(path)}>
      <Pre>{name}</Pre>
    </Button>
  );
}
