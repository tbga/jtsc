export interface IWebkitFolder {
  root: string;
  totalSize: number;
  entries: Map<string, IFile>;
}
export interface IFolderView extends Map<string, IEntry> {}

type IEntry = IFolder | IFileEntry;

interface IFolder {
  type: "folder";
  data: {
    path: string;
    name: string;
  };
}

interface IFileEntry {
  type: "file";
  data: IFile;
}

export interface IFile {
  name: File["name"];
  hashSHA256: string;
  data: File;
}
