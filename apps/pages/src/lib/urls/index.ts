export function createHomePageURL() {
  const path = "/";

  return path;
}

export function createTorrentViewerPageURL() {
  const path = "/torrent-viewer";

  return path;
}

export function createFolderViewerPageURL() {
  const path = `/view/folder`;

  return path;
}
