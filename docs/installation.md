# Installation

## Dependencies

Follow related installation instructions for your platform:

- [`git`](https://git-scm.com/)
- [`PostgreSQL`](https://www.postgresql.org/)
- [`pgAdmin`](https://www.pgadmin.org/)
- [`PGroonga`](https://pgroonga.github.io/)
- [`NodeJS`](https://nodejs.org/en)

## The Program

1. Clone the repo:
   ```sh
   git clone https://gitlab.com/the-coommectors/jtsc.git
   ```
2. Switch to cloned folder:
   ```sh
   cd ./jtsc
   ```
3. Install dependencies:

   ```sh
   npm run install
   ```

4. Build the program:

   ```sh
   npm run build
   ```

5. Prep the database with `pgAdmin`:

   1. Right click `Servers` -> `Register` -> `Server...`.
   2. Fill out required values.
   3. Right click `Servers` -> `<server_name>` -> `Databases` -> `Create` -> `Database...`.

6. Create `configuration.json` and `environment.json` files inside `./backend/configs/production`:

   - `configuration.json`
     - `<database_url>` - a connection string in this format:
       ```
       postgres://<username>:<password>@<host>:<port>/<database-name>
       ```
     - `<secret_key>` - a key to sign auth cookies with.
     - `<admin_invite>`
       A [`nanoid`](https://github.com/ai/nanoid) string for the admin invite. Can be generated with `npx nanoid`.
     ```json
     {
       "IS_HTTPS_ENABLED": false,
       "STORAGE_ORIGIN": "http://localhost:6969/storage",
       "DATABASE_URL": <database_url>,
       "SECRET_KEY": <secret_key>,
       "ADMIN_INVITE": <admin_invite>
     }
     ```
   - `environment.json`
     ```json
     {}
     ```

7. Start the server:

   ```sh
   npm start
   ```
