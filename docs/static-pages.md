# Static Pages

The pages are all static with logic realized by client scripts.<br>
NextJS also provides its client-side routing, but it doesn't work for dynamic pages entered from outside the script.
Therefore the server has to have a way to map urls to NextJS static files. Preferably without manual logic.

## Creation Logic
1. Frontend builds its files and the json file with its routes info.
2. Backend consumes that file and builds its own static routes for them.
