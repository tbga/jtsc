# Storage

File storage consists of 2 parts:

- local storage by hash
- public storage by symlinks

Each is a folder with complete controls of its descendants.<br>
They cannnot be the same folder or be a subpath of each other.

## Local Storage

A folder on the file system in which the file hashes are stored.<br>
The files paths consist of 3 parts:

- local root
  A folder on the local file system according to configuration value.

- nested directories
  Created by splitting lowercased hash letters into equal groups (i.e. 2/4/8/16/32). This way the files will be spread out across folders without blowing up a single one too much.

- filename
  The hash of the file with an extension according to its mime type.

The file itself can be an actual file (if the application is its primary source) or a symlink to one (when the application adds existing files).

## Public Storage

A folder on the file system according to configuration value.<br>
It is used as public part of the storage.<br>
The contents are arbitrary paths with files being symlinks to the files in the local storage.<br>
