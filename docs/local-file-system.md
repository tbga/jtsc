# Local File System

Storage relies on the symlinks to the local file system.<br>
But the browser doesn't give any information whatsoever about the host's file system.<br>
Therefore there is a need of the local file system browser on the frontend backed by the endpoints on the backend.
