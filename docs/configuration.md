# Configuration

Configuration files live in a separate folder and follow this structure:

- `<project_environment>`
  A folder named after the `PROJECT_ENV` value.
  - `configuration.json`
    Configuration values. Is not checked into the repo.
  - `configuration.default.json`
    Default configration values. Is checked into the repo and is ignored by resolver in production environment.
  - `environment.json`
    The values for other environment variables.


The config is resolved by shallowly merging the values of the default file and the base file.

## Environment
`PROJECT_ENV` is a core environment variable which decides the values of other environment variables. Its value is set by context (i.e. dev script will have `development` value while build script - `production`).

## Frontend
Because `NextJS` uses its own config logic relying on `.env` files, it won't have config implementation for now.
