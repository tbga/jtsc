# File Upload

The process of file uploading looks like this:
- pass the local path of the file to the backend
- get the needed metadata for that file
- create a symlink to it at the storage path provided
- create `File` entity for said file


## Folder Upload
The content of the folder gets uploaded recursively.
