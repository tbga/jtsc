# Localization

## Frontend

Environment variables:

- `NEXT_PUBLIC_SUPPORTED_LANGUAGES`
  JSON array of locale strings.
- `NEXT_PUBLIC_DEFAULT_LANGUAGE`
  A locale string of a default language used as a fallback.
  Must be present in the supported languages array also.

### Front page
It doesn't have a URL prefix, but needs to show some text anyway, therefore the locale is pulled from cookie.<br>
Otherwise show default language.

### Locale resolution
Locale is derived entirely from `lang` url segment. If the prefix doesn't match any of supported locales, redirect to `404` page.
