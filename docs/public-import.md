# Public Import

Public import is initiated by providing the path to the finished public export folder.<br>
The folder and all its files must be validated
according to this logic:

- The folder must only include expected files/folders.
- All files must conform to related json schemas.

After that the exported entries can be added to the public imports database.<br>
At this stage the creator of import has to choose which entries must be consumed on case-by-case basis.<br>
Then added to the import queue. Upon its success all public import subentries are must be deleted.
