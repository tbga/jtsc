# Operations

The tasks which can't be resolved within a single http request are put into operations queue.
Operations are `entities` with extra keys:
- `type`
  A discriminator key for the shape of `input_data` and `result_data` values.
- `status`
  The status of the operation.
- `input_data`
  A json string of input data which started the operation.
- `result_data`
  A json string of data which resulted from the resolved operation.
