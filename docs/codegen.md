# Codegen

## Codegen graph

The codegen module is a folder of this structure:
- `generator`<br>
  A module which exports a single function which returns an object with these values:
  - `type`<br>
    Must be of `"graph"` value.
  - `root`<br>
    The name of the folder within the codegen module to which the codegen graph is output.
  - `nodes`<br>
    An array of codegen nodes.

## Codegen node

A codegen node is a folder within the `root` folder which holds module files, some of them are:
- `lib`<br>
  Holds all non-abstract code for the given node.
- `types`<br>
  Holds all abstract code for the given node.
- `_index`<br>
  Re-exports all exported symbols from `lib` and `types`.

## JSON Schema
Due to short-lived nature of client scripts and ever-increasing size of the schema collection (and therefore bundle size), frontend must inline validators as separate modules.
