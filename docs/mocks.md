# Mocks

Mocks are generated at database level using pre-existing query functions.<br>
This way it also doubles up as a test for them, therefore the amount of mock-specific SQL queries must be kept at minimum.<br>
The generation runs in a single transaction which is split into 4 stages:

1. [Primary generation](#primary-generation)
2. [Secondary generation](#secondary-generation)
3. [Updates](#updates)
4. [Deletions](#deletions)

## Primary generation

Creates entities with initializers not dependant on any of the reference to existing entities.<br>
The hybrid initializers must only use non-referential initializers at this stage.

## Secondary generation

Creates entities which depend on existing items in the database.<br>
The hybrid initializers must also be reran at this stage with hybrid initialization.

## Updates

Runs all updates on the subset of existing entities.<br>
The range of subset must be at minimum 1% and at maximum 99%,
to ensure there are guarantied to be updated and non-updated items each run.

## Deletions

Runs all deletions on the subset of existing entities.<br>
The range of subset must be at minimum 1% and at maximum 99%,
to ensure there are guarantied deletions but not a complete wipe.
