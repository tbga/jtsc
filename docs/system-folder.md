# System Folder

System folder is a folder within `PROJECT_PATH` named `.system`, which holds all runtime-specific data, so it would be scoped within a single folder.<br>
The contents of this folder are folders named after [valid `ProjectEnvironment` values](../schema/configs/project-environment.schema.json), mostly so the repo setup could run in several environment modes without too much friction.<br>
Any other files and folder within it are ignored.<br>
Each package creates its own folder within the resulting path for its own use.<br>
Within the repo it is not checked into version control. This also means this folder should not be critical to the function of the application and therefore it should be able to start in any of the modes even with empty folder.<br>
The application has to at least ensure the existence of the folder for its environment.

## Reserved Folders

- `database` - holds database-related runtime data.
- `temporary` - holds folders with temporary data.
- `storage` - stores local and public files.

## Storage

Consists of two folders:
- `files`<br>
    Holds files or symlinks with standard filename.
- `public`<br>
    Holds symlinks which point to files in the `files` folder.
