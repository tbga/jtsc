# Media Types (formerly MIME Types)

## Known Types
Basically a deserialized compilation of types from [IANA registry](https://www.iana.org/assignments/media-types/media-types.xhtml).
