# Public Import

Public import exists in these states:

- `pending`<br>
  The import can be constructed.
- `in-progress`<br>
  The import is being consumed. No changes are allowed to be made to it until it's resolved.
- `consumed`<br>
  The import has been consumed and sub-entities deleted.

## Entities

The entities of an import are split into these categories:
- `new`<br>
  The public ID of the entity does not have a match in local entities. A new entity will be added on consumption.
- `same`<br>
  The public ID of the entity has a match in local entities and all their fields are equal. It will be ignored on consumption.
- `different`<br>
  The public ID of the entity has a match in local entities and at least one field is not equal. Approving this type of entity will update the existing one on consumption.
