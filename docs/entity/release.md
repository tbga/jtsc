# Release

A release is a profile's post on a site. In general sites would call them posts instead. The problem of this naming arises when a single artist reposts the content of one profile to another, thus the amount unique content from the artist is less than a sum of his posts on profiles. Therefore there is a need to separate the two and "release" sounds more concrete than "post".

## Create

There is no such thing as a release without a post. However, post reference is optional and therefore an empty post for the new release must be created.

## Read

## Update

## Delete
