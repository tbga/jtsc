# POST

Post is a collection of releases. Any given release can have many posts and post can have many releases.<br>
Each post can also have many artists, which are the "cast" of the post.<br>
Therefore "artist posts" and "post artists" means different things.<br>
The former being all the posts of the profile releases for the artist.
