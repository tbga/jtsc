# Public Export

Public exports exist in 3 stages:

- `in-progress`<br>
  The work is being done currently.
- `finished`<br>
  The export is finished and exported to a file.<br>
  No changes are allowed to be done to the export afterwards.
- `error`<br>
  The export was rejected with at least one error.
