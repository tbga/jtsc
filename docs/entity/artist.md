# Artist

## Fields

### Name

Artist name is a derivative value and must follow this algo:

1. All profiles in `profile_artists` with `artist_id` are picked.
2. Out of this profiles subset the `name_id` with the lowest `id` in `profile_names` is picked.
3. The `full_name` from `names` for the given `name_id` is picked as an artist name. Otherwise `NULL`.
