# Pagination

## Backend

### API

Paginated collections in the API are expressed in the form of `/items` and `/items/:page`.<br>
The base path only returns the pagination info for the given collection.<br>
The paged path returns the items for the page and the pagination info.<br>
It is an error to have the page number below 1 and more than the total pages provided by the pagination info.<br>

## Frontend

### Components

The pagination can be separated into 3 types:

- `Internal`

  The pages are links, therefore the page switching incurs navigation.<br>
  For this reason there is no point in having more than one internal pagination on the page.<br>

- `Local`

  The pages are buttons and clicking on them will invoke the page change callback.<br>
  There is no limit on how many local paginations can be present on the page.<br>
  But because they rely on the client script, they should be client-rendered.<br>

- `Inline`

  The pages are buttons but unlike `local` it uses the collection as an input, instead of abstract pagination.<br>
  Mainly for presenting a paginated view on arbitrary collections not coming from backend.<br>

### Pages

Pages for paginated collections follow the same naming scheme as API endpoints.<br>
The base path fetches pagination info and redirects to the last page of the collection.<br>
The paged path shows the items for the given page.<br>
