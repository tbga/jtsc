# Public Export

Public export is a folder with following content:
- `meta.json`<br>
  A file with metadata.
- `data.json`<br>
  A file with exported data.
