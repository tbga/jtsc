import concurrently from "concurrently";

develop();

async function develop() {
  // @ts-expect-error typescript esm meme
  /** @type {typeof concurrently["default"]} */ (concurrently)(
    [
      {
        name: "backend",
        command: "npm run develop --workspace=backend",
        env: { PROJECT_ENV: "development" },
      },
      {
        name: "frontend",
        command: "npm run develop --workspace=frontend",
        env: { PROJECT_ENV: "development" },
      },
    ],
    {
      killOthers: "failure",
    },
  );
}
