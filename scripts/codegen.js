import { spawn } from "node:child_process";
import { env } from "node:process";

await codegen();

async function codegen() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const codegen = spawn("npm run codegen --workspaces --if-present", {
    env: env,
    shell: true,
  });

  codegen.stdout.on("data", (data) => {
    console.log(String(data));
  });

  codegen.stderr.on("data", (data) => {
    console.error(String(data));
  });

  codegen.on("error", (error) => {
    console.log(String(error.message));
  });

  codegen.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly ran codegen."
        : `Child process exited with code ${code}`,
    );
  });
}
