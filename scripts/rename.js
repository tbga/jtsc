import { log } from "node:console";
import { opendir, rename } from "node:fs/promises";
import path from "node:path";
import { parseArgs } from "node:util";

await run();

async function run() {
  const parsedArgs = parseArgs({
    options: {
      folder: {
        type: "string",
      },
      "old-name": {
        type: "string",
      },
      "new-name": {
        type: "string",
      },
    },
  });
  const { folder } = parsedArgs.values;
  const oldName = parsedArgs.values["old-name"];
  const newName = parsedArgs.values["new-name"];

  if (!folder) {
    throw new Error(`Option "--folder" must be provided.`);
  }

  if (!oldName) {
    throw new Error(`Option "--old-name" must be provided.`);
  }
  if (!newName) {
    throw new Error(`Option "--new-name" must be provided.`);
  }

  await renameFiles(folder, oldName, newName);
}

/**
 *
 * @param {string} folderPath
 * @param {string} oldName
 * @param {string} newName
 */
async function renameFiles(folderPath, oldName, newName) {
  const folder = await opendir(folderPath);

  for await (const entry of folder) {
    const isValidEntry = entry.isDirectory() || entry.isFile();

    if (!isValidEntry) {
      continue;
    }

    if (entry.isDirectory()) {
      const entryPath = path.join(folderPath, entry.name);
      await renameFiles(entryPath, oldName, newName);
      continue;
    }

    // the entry is a file
    const baseName = entry.name.slice(0, entry.name.lastIndexOf("."));

    if (baseName !== oldName) {
      continue;
    }

    const extension = entry.name.slice(entry.name.lastIndexOf("."));
    const oldPath = path.join(folder.path, entry.name);
    const newPath = path.join(folder.path, `${newName}${extension}`);
    await rename(oldPath, newPath);

    log(`"${oldPath}" -> "${newPath}"`);
  }
}
