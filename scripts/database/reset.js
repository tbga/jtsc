import { spawn } from "node:child_process";
import { env } from "node:process";

await reset();

async function reset() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const reset = spawn("cd backend && npm run reset", {
    env: env,
    shell: true,
  });

  reset.stdout.on("data", (data) => {
    console.log(String(data));
  });

  reset.stderr.on("data", (data) => {
    console.error(String(data));
  });

  reset.on("error", (error) => {
    console.log(String(error.message));
  });

  reset.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly reset database."
        : `Child process exited with code ${code}`,
    );
  });
}
