import { spawn } from "node:child_process";
import { env } from "node:process";

await wipe();

async function wipe() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const wipe = spawn("cd backend && npm run wipe", {
    env: env,
    shell: true,
  });

  wipe.stdout.on("data", (data) => {
    console.log(String(data));
  });

  wipe.stderr.on("data", (data) => {
    console.error(String(data));
  });

  wipe.on("error", (error) => {
    console.log(String(error.message));
  });

  wipe.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly wiped database."
        : `Child process exited with code ${code}`,
    );
  });
}
