import { spawn } from "node:child_process";
import { env } from "node:process";

await cleanse();

async function cleanse() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const cleanse = spawn("cd backend && npm run cleanse", {
    env: env,
    shell: true,
  });

  cleanse.stdout.on("data", (data) => {
    console.log(String(data));
  });

  cleanse.stderr.on("data", (data) => {
    console.error(String(data));
  });

  cleanse.on("error", (error) => {
    console.log(String(error.message));
  });

  cleanse.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly cleansed database."
        : `Child process exited with code ${code}`,
    );
  });
}
