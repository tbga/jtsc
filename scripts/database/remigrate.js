import { spawn } from "node:child_process";
import { env } from "node:process";

await remigrate();

async function remigrate() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const remigrate = spawn("cd backend && npm run remigrate", {
    env: env,
    shell: true,
  });

  remigrate.stdout.on("data", (data) => {
    console.log(String(data));
  });

  remigrate.stderr.on("data", (data) => {
    console.error(String(data));
  });

  remigrate.on("error", (error) => {
    console.log(String(error.message));
  });

  remigrate.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly reran migrations."
        : `Child process exited with code ${code}`,
    );
  });
}
