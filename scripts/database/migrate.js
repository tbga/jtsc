import { spawn } from "node:child_process";
import { env } from "node:process";

await migrate();

async function migrate() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const migrate = spawn("cd backend && npm run migrate", {
    env: env,
    shell: true,
  });

  migrate.stdout.on("data", (data) => {
    console.log(String(data));
  });

  migrate.stderr.on("data", (data) => {
    console.error(String(data));
  });

  migrate.on("error", (error) => {
    console.log(String(error.message));
  });

  migrate.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly ran migrations."
        : `Child process exited with code ${code}`,
    );
  });
}
