import { spawn } from "node:child_process";
import { env } from "node:process";

await regenerate();

async function regenerate() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const regenerate = spawn("cd backend && npm run regenerate", {
    env: env,
    shell: true,
  });

  regenerate.stdout.on("data", (data) => {
    console.log(String(data));
  });

  regenerate.stderr.on("data", (data) => {
    console.error(String(data));
  });

  regenerate.on("error", (error) => {
    console.log(String(error.message));
  });

  regenerate.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly regenerated mocks."
        : `Child process exited with code ${code}`,
    );
  });
}
