import { spawn } from "node:child_process";
import { env } from "node:process";

await generate();

async function generate() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "development",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "development") {
    throw new Error("This command runs in development mode only.");
  }

  const generate = spawn("cd backend && npm run generate", {
    env: env,
    shell: true,
  });

  generate.stdout.on("data", (data) => {
    console.log(String(data));
  });

  generate.stderr.on("data", (data) => {
    console.error(String(data));
  });

  generate.on("error", (error) => {
    console.log(String(error.message));
  });

  generate.on("close", (code) => {
    console.log(
      code === 0
        ? "Successfuly generated mocks."
        : `Child process exited with code ${code}`,
    );
  });
}
