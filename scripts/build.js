import { spawn } from "node:child_process";
import fs from "node:fs/promises";
import path from "node:path";
import { cwd, env } from "node:process";

await build();

async function build() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "production",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "production") {
    throw new Error("This command runs in production mode only.");
  }

  const build = spawn("npm run build --workspaces", {
    env: env,
    shell: true,
  });

  build.stdout.on("data", (data) => {
    console.log(String(data));
  });

  build.stderr.on("data", (data) => {
    console.error(String(data));
  });

  build.on("error", (error) => {
    console.log(String(error.message));
  });

  build.on("close", async (code, signal) => {
    if (code !== 0) {
      console.log(`Exited with code ${code}`);
      return;
    }

    await transformBuildOutput();

    console.log("Successfuly built the sources.");
  });
}

async function transformBuildOutput() {
  const backendFolder = path.join(cwd(), "backend");
  const backendOutputFolder = path.join(backendFolder, "dist");
  const frontendOutputFolder = path.join(cwd(), "frontend", "dist");
  const publicFolder = path.join(backendOutputFolder, "public");

  console.log("Collecting output files...");

  console.log("Removing old static files...");

  await fs.rm(publicFolder, { force: true, recursive: true });

  console.log("Removed old static files.");

  console.log("Collecting frontend output...");

  await fs.cp(frontendOutputFolder, backendOutputFolder, {
    preserveTimestamps: true,
    recursive: true,
  });

  console.log("Collected frontend output.");

  console.log("Collected output files.");
}
