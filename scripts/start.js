import { spawn } from "node:child_process";
import { cwd, env } from "node:process";

await start();

async function start() {
  if (env.PROJECT_ENV === undefined) {
    Object.defineProperty(env, "PROJECT_ENV", {
      value: "production",
      enumerable: true,
      configurable: true,
      writable: true,
    });
  }

  if (env.PROJECT_ENV !== "production") {
    throw new Error("This command runs in production mode only.");
  }

  const build = spawn("npm run start --workspace=backend", {
    env: env,
    shell: true,
  });

  build.stdout.on("data", (data) => {
    console.log(String(data));
  });

  build.stderr.on("data", (data) => {
    console.error(String(data));
  });

  build.on("error", (error) => {
    console.log(String(error.message));
  });
}
