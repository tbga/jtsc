import { createReadStream } from "node:fs";
import fs from "node:fs/promises";
import path from "node:path";
import { cwd } from "node:process";
import { parse } from "csv";

/**
 * @typedef IMediaSchema
 * @property {string} $id
 * @property {string} title
 * @property {"object"} type
 * @property {false} additionalProperties
 * @property {["type", "subtype"]} required
 * @property {IMediaSchemaProperties} properties
 */

/**
 * @typedef IMediaSchemaProperties
 * @property {{ type: "string", const: string }} type
 * @property {{ anyOf: IMediaSchemaSubtype[] }} subtype
 */

/**
 * @typedef IMediaSchemaSubtype
 * @property {string} description
 * @property {"string"} type
 * @property {string} const
 */

await build();

/**
 * @TODO better flow
 * @typedef IRecord
 * @property {string} name
 * @property {string} reference
 */
async function build() {
  const mediaTypesFolder = path.join(cwd(), "configs", "media-types");
  const outputFolder = path.join(mediaTypesFolder, "output");

  await createSchemas(mediaTypesFolder, outputFolder);
}

/**
 * @param {string} mediaTypesFolder
 * @param {string} outputFolder
 */
async function createSchemas(mediaTypesFolder, outputFolder) {
  const dirEntries = await fs.readdir(mediaTypesFolder, {
    encoding: "utf-8",
    withFileTypes: true,
  });

  const csvFiles = dirEntries.filter((entry) => {
    return entry.isFile() && entry.name.endsWith(".csv");
  });

  for await (const file of csvFiles) {
    console.log(`Reading file "${file.name}"`);

    const filePath = path.join(mediaTypesFolder, file.name);
    const typeName = path.basename(filePath, ".csv");
    const outputFile = path.join(outputFolder, `${typeName}.schema.json`);
    const schema = await createSchema(filePath);
    await fs.writeFile(outputFile, JSON.stringify(schema, undefined, 2), {
      encoding: "utf-8",
    });
  }
}

/**
 * @param {string} filePath
 * @returns {Promise<IMediaSchema>}
 */
async function createSchema(filePath) {
  const typeName = path.basename(filePath, ".csv");
  const title = `${typeName[0].toUpperCase()}${typeName.slice(1)}`;

  const parser = parse({
    encoding: "utf-8",
    /**
     * @param {string[]} headers
     */

    columns: (headers) => {
      return headers.map((name) => name.toLowerCase());
    },
  });

  /**
   * @type {IMediaSchemaSubtype[]}
   */
  const subtypes = [];
  const records = createReadStream(filePath).pipe(parser);

  for await (const record of records) {
    /**
     * @type {IRecord}
     */
    const { name, reference } = record;
    /**
     * @type {IMediaSchemaSubtype}
     */
    const subType = {
      description: reference,
      type: "string",
      const: name,
    };

    subtypes.push(subType);
  }

  /**
   * @type {IMediaSchema}
   */
  const schema = {
    $id: `https://jtsc-schemas.org/media/${typeName}.schema.json`,
    title: `MediaType${title}`,
    type: "object",
    additionalProperties: false,
    required: ["type", "subtype"],
    properties: {
      type: {
        type: "string",
        const: typeName,
      },
      subtype: {
        anyOf: subtypes,
      },
    },
  };

  return schema;
}
