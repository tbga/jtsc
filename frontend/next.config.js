// @ts-check
const path = require("path");

/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "export",
  distDir: "./dist/public",
  trailingSlash: true,
  reactStrictMode: true,
  swcMinify: true,
  images: {
    unoptimized: true,
  },
  eslint: {
    dirs: ["src"],
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
};

module.exports = nextConfig;
