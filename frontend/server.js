// @ts-check
// server.js
const { createServer } = require("http");
const { createServer: createSecureServer } = require("https");
const fs = require("fs").promises;
const { parse } = require("url");
const next = require("next");

const hostname = "localhost";
const port = 3000;
// next server is only ran in development
// so no oneed to check for env var
const app = next({ dev: true, hostname, port });
const handle = app.getRequestHandler();

app.prepare().then(async () => {
  // reading env after `Next.prepare()` call
  // https://github.com/vercel/next.js/issues/12269#issuecomment-933322120

  /**
   * @type {boolean}
   */
  // @ts-expect-error parsing undefined
  const IS_HTTPS_ENABLED = JSON.parse(process.env.IS_HTTPS_ENABLED);

  if (!IS_HTTPS_ENABLED) {
    const server = createServer(async (req, res) => {
      // Be sure to pass `true` as the second argument to `url.parse`.
      // This tells it to parse the query portion of the URL.
      const parsedUrl = parse(req.url, true);
      await handle(req, res, parsedUrl);
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://${hostname}:${port}`);
    });
  } else {
    const HTTPS_KEY_PATH = process.env.HTTPS_KEY_PATH;
    const HTTPS_CERT_PATH = process.env.HTTPS_CERT_PATH;
    // @ts-expect-error env types
    const key = await fs.readFile(HTTPS_KEY_PATH, { encoding: "utf-8" });
    // @ts-expect-error env types
    const cert = await fs.readFile(HTTPS_CERT_PATH, { encoding: "utf-8" });

    const server = createSecureServer({ key, cert }, async (req, res) => {
      // Be sure to pass `true` as the second argument to `url.parse`.
      // This tells it to parse the query portion of the URL.
      const parsedUrl = parse(req.url, true);
      await handle(req, res, parsedUrl);
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on https://${hostname}:${port}`);
    });
  }
});
