import { runCodegen } from "#server/codegen";

(async () => {
  console.log("Codegen started.");

  await runCodegen();

  console.log("Codegen finished.");
})();
