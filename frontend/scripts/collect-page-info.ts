import path from "path";
import { cwd } from "process";
import { savePagePathInfoToFile } from "#server/lib/nextjs";

(async () => {
  await run();
})();

async function run() {
  console.log("Collection page path info...");
  const outputPath = path.join(cwd(), "dist", "path-info.json");
  await savePagePathInfoToFile(outputPath);

  console.log(`Collected page path info at "${outputPath}".`);
}
