import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPost, fetchPostArtists } from "#api/post";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { CardList, Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import { ArtistCard } from "#entities/artist";
import {
  PostArtistsCreateURL,
  PostArtistsRemoveURL,
  PostArtistsURL,
  PostURL,
} from "#entities/post";

function PostArtistsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [post, changePost] = useState<Awaited<ReturnType<typeof fetchPost>>>();
  const [artistsData, changeArtistsData] =
    useState<Awaited<ReturnType<typeof fetchPostArtists>>>();
  const { query, isReady } = router;
  const post_id = resolveQueryValue(query.post_id);
  const page = resolveQueryValue(query.page);
  const title = !post
    ? "Post artists"
    : !artistsData
      ? `Post (${post.id}) artists`
      : `Post  (${post.id}) artists page ${artistsData.pagination.current_page} out of ${artistsData.pagination.total_pages}`;
  const heading = !post ? "Post Artists" : `Post (${post.id}) Artists`;

  // fetch profile
  useEffect(() => {
    if (!isReady || !post_id || !page) {
      return;
    }

    (async () => {
      const isSameID = post_id === post?.id;
      const profileID = isSameID ? post.id : post_id;

      if (!isSameID) {
        const newPost = await fetchPost(post_id);

        changePost(newPost);
      }

      const newArtists = await fetchPostArtists(profileID, page);

      changeArtistsData(newArtists);
    })();
  }, [isReady, post_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!(post && artistsData) ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <>
          <CardList
            isDescending
            pagination={artistsData.pagination}
            urlBuilder={(page) => new PostArtistsURL(post.id, String(page))}
          >
            {artistsData.artists.map((artist) => (
              <ArtistCard key={artist.id} artist={artist} />
            ))}
          </CardList>
        </>
      )}

      <Article>
        <ArticleBody>
          <List>
            <Item>
              {!post ? (
                <LoadingBar />
              ) : (
                <Link href={new PostURL(language, post.id)}>Post</Link>
              )}
            </Item>
            <Item>
              <ListButtons>
                {!post ? (
                  <LoadingBar />
                ) : (
                  <>
                    <LinkButton
                      href={new PostArtistsRemoveURL(language, post.id)}
                    >
                      Remove
                    </LinkButton>
                    <LinkButton
                      href={new PostArtistsCreateURL(language, post.id)}
                    >
                      Add
                    </LinkButton>
                  </>
                )}
              </ListButtons>
            </Item>
          </List>
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default PostArtistsPage;
