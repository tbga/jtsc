import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchPostArtistsPagination } from "#api/post";
import { paginationRedirectMulti } from "#components";
import { PostArtistsCreateURL, PostArtistsURL } from "#entities/post";

const PostArtists = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      post_id: resolveQueryValue(query.post_id)!,
    };
  },
  prefix: "post artists",
  title: (query) => "Post Artists",
  paginationFetcher: async (query) => {
    return fetchPostArtistsPagination(query.post_id);
  },
  urlBuilder: (page, query) =>
    new PostArtistsURL(DEFAULT_LANGUAGE, query.post_id, page),
  newLink: (query) => new PostArtistsCreateURL(DEFAULT_LANGUAGE, query.post_id),
});

export default PostArtists;
