import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPost, fetchPostReleases } from "#api/post";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  PostReleasesCreateURL,
  PostReleasesURL,
  PostURL,
} from "#entities/post";
import { ReleaseCard } from "#entities/release";

function PostReleasesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const [post, changePost] = useState<Awaited<ReturnType<typeof fetchPost>>>();
  const [releasesData, changeReleasesData] =
    useState<Awaited<ReturnType<typeof fetchPostReleases>>>();
  const { query, isReady } = router;
  const post_id = resolveQueryValue(query.post_id);
  const page = resolveQueryValue(query.page);
  const title = !post
    ? "Post releases"
    : !releasesData
      ? `Releases of post (${post.id})`
      : `Releases of post (${post.id}) page ${releasesData.pagination.current_page} out of ${releasesData.pagination.total_pages}`;
  const heading = !post ? "Post Releases" : `Releases of Post (${post.id})`;

  // fetch profile
  useEffect(() => {
    if (!isReady || !post_id || !page) {
      return;
    }

    (async () => {
      const isSameID = post_id === post?.id;
      const postID = isSameID ? post.id : post_id;

      if (!isSameID) {
        const newPost = await fetchPost(post_id);

        changePost(newPost);
      }

      const newReleases = await fetchPostReleases(postID, page);

      changeReleasesData(newReleases);
    })();
  }, [isReady, post_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!post || !releasesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <>
          <CardList
            isDescending
            pagination={releasesData.pagination}
            urlBuilder={(page) =>
              new PostReleasesURL(language, post.id, String(page))
            }
          >
            {releasesData.releases.map((release) => (
              <ReleaseCard key={release.id} release={release} />
            ))}
          </CardList>
        </>
      )}
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!post ? (
                <LoadingBar />
              ) : (
                <Link href={new PostURL(language, post.id)}>Post</Link>
              )}
            </Item>
            {isRegistered && (
              <Item>
                {!post ? (
                  <LoadingBar />
                ) : (
                  <Link href={new PostReleasesCreateURL(language, post.id)}>
                    Add releases
                  </Link>
                )}
              </Item>
            )}
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default PostReleasesPage;
