import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchPostReleasesPagination } from "#api/post";
import { paginationRedirectMulti } from "#components";
import { PostReleasesCreateURL, PostReleasesURL } from "#entities/post";

const PostReleases = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      post_id: resolveQueryValue(query.post_id)!,
    };
  },
  prefix: "post releases",
  title: (query) => "Post Releases",
  paginationFetcher: async (query) => {
    return fetchPostReleasesPagination(query.post_id);
  },
  urlBuilder: (page, query) =>
    new PostReleasesURL(DEFAULT_LANGUAGE, query.post_id, page),
  newLink: (query) =>
    new PostReleasesCreateURL(DEFAULT_LANGUAGE, query.post_id),
});

export default PostReleases;
