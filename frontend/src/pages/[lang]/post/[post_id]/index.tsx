import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPost } from "#api/post";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { PostArticle } from "#entities/post";

function PostDetails() {
  const router = useRouter();
  const [post, changePost] = useState<Awaited<ReturnType<typeof fetchPost>>>();
  const { query, isReady } = router;
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "Post"
    : `Post "${post.title ?? "Untitled"}" (${post.id})`;
  const heading = !post
    ? "Post"
    : `Post "${post.title ?? "Untitled"}" (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      const newPost = await fetchPost(post_id);
      changePost(newPost);
    })();
  }, [isReady, post_id]);

  return (
    <Page title={title} heading={heading}>
      {!post ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <PostArticle post={post} />
      )}
    </Page>
  );
}

export default PostDetails;
