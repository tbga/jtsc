import { useEffect, useState } from "react";
import { useAccount } from "#hooks";
import { pageComponent } from "#components/pages";
import { AccountArticle, type IUserAgent } from "#entities/account";
import { fetchUserAgent } from "#api/account";

const AccountOverview = pageComponent(
  { title: () => "Account overview", heading: () => "Account overview" },
  () => {
    const { account } = useAccount();
    const [userAgent, changeUserAgent] = useState<IUserAgent>();

    useEffect(() => {
      (async () => {
        const serverAgent = await fetchUserAgent();
        const clientAgent = navigator.userAgent;
        const userAgent: IUserAgent = {
          server: serverAgent,
          client: clientAgent,
        };

        changeUserAgent(userAgent);
      })();
    }, []);

    return (
      <AccountArticle
        headingLevel={2}
        account={account}
        userAgent={userAgent}
      />
    );
  },
);

export default AccountOverview;
