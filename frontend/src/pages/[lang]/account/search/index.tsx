import { SearchURL } from "#lib/urls";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, Page } from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";

interface INavLink {
  name: string;
  title: string;
}

const NAV_LINKS: readonly INavLink[] = [
  { name: "artists", title: "Artists" },
  { name: "posts", title: "Posts" },
  { name: "sites", title: "Sites" },
  { name: "profiles", title: "Profiles" },
  { name: "releases", title: "Releases" },
] as const;

function SearchPage() {
  const { language } = useLanguage();
  const title = "Search items";
  const heading = "Search Items";

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            {NAV_LINKS.map(({ name, title }, index) => (
              <Item key={index}>
                <Link href={new SearchURL({ language, pathname: `/${name}` })}>
                  {title}
                </Link>
              </Item>
            ))}
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchPage;
