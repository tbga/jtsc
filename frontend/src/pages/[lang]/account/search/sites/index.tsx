import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import { fetchSiteSearchPagination } from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";

function SearchSites() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const title = !searchQuery
    ? "Search sites"
    : `Search sites with "${searchQuery}"`;
  const heading = "Search Sites";

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/sites/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-sites"
              paginationFetch={async ({ query }) =>
                fetchSiteSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchSites;
