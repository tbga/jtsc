import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import {
  fetchSiteSearch,
  fetchSiteSearchPagination,
} from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import { SiteCard, SiteCreateURL } from "#entities/site";
import { type IAsyncReturnType } from "#types";

function SearchSitesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [sitesData, changeSitesData] =
    useState<IAsyncReturnType<typeof fetchSiteSearch>>();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const page = resolveQueryValue(query.page);
  const title = !searchQuery
    ? "Search sites"
    : `Search sites with "${searchQuery}"`;
  const heading = "Search Sites";

  useEffect(() => {
    if (!isReady || !searchQuery || !page) {
      return;
    }

    (async () => {
      const artistsData = await fetchSiteSearch(searchQuery, page);
      changeSitesData(artistsData);
    })();
  }, [isReady, searchQuery, page]);

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/sites/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-sites"
              paginationFetch={async ({ query }) =>
                fetchSiteSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>

      {!sitesData ? (
        <LoadingBar />
      ) : (
        <CardList
          pagination={sitesData.pagination}
          urlBuilder={(page) =>
            new SearchURL({
              language,
              pathname: `/sites/${page}`,
              searchParams: new URLSearchParams([["query", searchQuery!]]),
            })
          }
        >
          {sitesData.sites.map((site) => (
            <SiteCard key={site.id} site={site} />
          ))}
        </CardList>
      )}
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              <Link href={new SiteCreateURL(language)}>Add site</Link>
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchSitesPage;
