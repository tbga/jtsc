import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import {
  fetchArtistSearch,
  fetchArtistSearchPagination,
} from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";
import { CardList } from "#components/lists";
import { ArtistCard } from "#entities/artist";
import { EMPTY_ARRAY, type IAsyncReturnType } from "#types";

function SearchArtistsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [artistsData, changeArtistsData] =
    useState<IAsyncReturnType<typeof fetchArtistSearch>>();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const page = resolveQueryValue(query.page);
  const title = !artistsData
    ? "Search artists"
    : `Search artists with "${searchQuery}" page ${artistsData.pagination.current_page} out of ${artistsData.pagination.total_pages}`;
  const heading = "Search Artists";

  useEffect(() => {
    if (!isReady || !searchQuery || !page) {
      return;
    }

    (async () => {
      const artistsData = await fetchArtistSearch(searchQuery, page);
      changeArtistsData(artistsData);
    })();
  }, [isReady, searchQuery, page]);

  async function handleSubmit(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/artists/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-artists"
              fieldNames={EMPTY_ARRAY}
              paginationFetch={async ({ query }) =>
                fetchArtistSearchPagination(query)
              }
              getResult={async ({ query }) => handleSubmit(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>

      {!artistsData ? (
        <LoadingBar />
      ) : (
        <CardList
          pagination={artistsData.pagination}
          urlBuilder={(page) => {
            const searchParams = new URLSearchParams([["query", searchQuery!]]);
            const url = new SearchURL({
              language,
              pathname: `/artists/${page}`,
              searchParams,
            });

            return url;
          }}
        >
          {artistsData.artists.map((artist) => (
            <ArtistCard key={artist.id} artist={artist} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default SearchArtistsPage;
