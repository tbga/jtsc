import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import { fetchArtistSearchPagination } from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";
import { EMPTY_ARRAY } from "#types";

function SearchArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const title = "Search artists";
  const heading = "Search Artists";

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    const url = new SearchURL({
      language,
      pathname: "/artists/1",
      searchParams,
    });
    router.push(url);
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-artists"
              fieldNames={EMPTY_ARRAY}
              paginationFetch={async ({ query }) =>
                fetchArtistSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchArtists;
