import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import {
  fetchPostSearch,
  fetchPostSearchPagination,
} from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";
import { type IBigSerialInteger, type IAsyncReturnType } from "#types";
import { CardList } from "#components/lists";
import { PostCard, PostsSearchURL } from "#entities/post";

function SearchPostsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [postData, changePostData] =
    useState<IAsyncReturnType<typeof fetchPostSearch>>();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const page = resolveQueryValue(query.page);
  const title = !searchQuery
    ? "Search posts"
    : !postData
      ? `Search posts with "${searchQuery}"`
      : `Search posts with "${searchQuery}" page ${postData.pagination.current_page} out of ${postData.pagination.total_pages}`;
  const heading = "Search Posts";

  useEffect(() => {
    if (!isReady || !searchQuery || !page) {
      return;
    }

    (async () => {
      const newReleaseData = await fetchPostSearch(searchQuery, page);
      changePostData(newReleaseData);
    })();
  }, [isReady, searchQuery, page]);

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/posts/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-posts"
              paginationFetch={async ({ query }) =>
                fetchPostSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>

      {!postData ? (
        <LoadingBar />
      ) : (
        <CardList
          pagination={postData.pagination}
          urlBuilder={(page) =>
            new PostsSearchURL(
              language,
              searchQuery!,
              page as IBigSerialInteger,
            )
          }
        >
          {postData.posts.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default SearchPostsPage;
