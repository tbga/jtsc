import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import { fetchPostSearchPagination } from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";

function SearchPosts() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const title = !searchQuery
    ? "Search posts"
    : `Search posts with "${searchQuery}"`;
  const heading = "Search Posts";

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/posts/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-posts"
              paginationFetch={async ({ query }) =>
                fetchPostSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchPosts;
