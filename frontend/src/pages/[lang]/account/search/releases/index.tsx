import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import { fetchReleaseSearchPagination } from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";

function SearchReleases() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const title = !searchQuery
    ? "Search releases"
    : `Search releases with "${searchQuery}"`;
  const heading = "Search Releases";

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/releases/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-releases"
              paginationFetch={async ({ query }) =>
                fetchReleaseSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchReleases;
