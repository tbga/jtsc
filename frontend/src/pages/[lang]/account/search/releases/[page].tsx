import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import {
  fetchReleaseSearch,
  fetchReleaseSearchPagination,
} from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchForm } from "#components/forms";
import { type IBigSerialInteger, type IAsyncReturnType } from "#types";
import { CardList } from "#components/lists";
import { ReleaseCard, ReleaseSearchURL } from "#entities/release";

function SearchReleasesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [releaseData, changeReleaseData] =
    useState<IAsyncReturnType<typeof fetchReleaseSearch>>();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const page = resolveQueryValue(query.page);
  const title = !searchQuery
    ? "Search releases"
    : !releaseData
      ? `Search releases with "${searchQuery}"`
      : `Search releases with "${searchQuery}" page ${releaseData.pagination.current_page} out of ${releaseData.pagination.total_pages}`;
  const heading = "Search Releases";

  useEffect(() => {
    if (!isReady || !searchQuery || !page) {
      return;
    }

    (async () => {
      const newReleaseData = await fetchReleaseSearch(searchQuery, page);
      changeReleaseData(newReleaseData);
    })();
  }, [isReady, searchQuery, page]);

  async function handleSearch(newQuery: string) {
    const searchParams = new URLSearchParams([["query", newQuery]]);
    router.push(
      new SearchURL({ language, pathname: "/releases/1", searchParams }),
    );
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchForm
              id="search-releases"
              paginationFetch={async ({ query }) =>
                fetchReleaseSearchPagination(query)
              }
              getResult={async ({ query }) => handleSearch(query)}
              defaultValue={searchQuery}
            />
          )}
        </ArticleHeader>
      </Article>

      {!releaseData ? (
        <LoadingBar />
      ) : (
        <CardList
          pagination={releaseData.pagination}
          urlBuilder={(page) =>
            new ReleaseSearchURL(
              language,
              searchQuery!,
              page as IBigSerialInteger,
            )
          }
        >
          {releaseData.releases.map((release) => (
            <ReleaseCard key={release.id} release={release} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default SearchReleasesPage;
