import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { validatePageNumber } from "#lib/pagination";
import { SearchURL } from "#lib/urls";
import { fetchSite } from "#api/sites";
import {
  fetchSearchProfiles,
  fetchSearchProfilesPagination,
} from "#api/account/search";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { ProfileCard, SearchProfilesForm } from "#entities/profile";
import { IAsyncReturnType } from "#types";

function SearchProfilesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const [profilesData, changeProfileData] =
    useState<IAsyncReturnType<typeof fetchSearchProfiles>>();
  const searchQuery = resolveQueryValue(query.query);
  const site_id = resolveQueryValue(query.site_id);
  const page = resolveQueryValue(query.page);
  const title = getTitle(searchQuery, profilesData, site);
  const heading = !site
    ? "Search Profiles"
    : `Search profiles for Site "${site.title}" (${site.id})`;

  if (isReady && page) {
    validatePageNumber(page);
  }

  useEffect(() => {
    if (!isReady) {
      return;
    }

    if (!site_id) {
      if (site) {
        changeSite(undefined);
      }
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  useEffect(() => {
    if (!isReady) {
      return;
    }

    if (!searchQuery || !page) {
      return;
    }

    (async () => {
      const newProfilesData = await fetchSearchProfiles(
        { query: searchQuery, site_id },
        page,
      );
      changeProfileData(newProfilesData);
    })();
  }, [isReady, searchQuery, page, site_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchProfilesForm
              id="search-profiles"
              searchQuery={searchQuery}
              site={site}
              onSearch={async ({ query, site_id }) => {
                const searchParams = new URLSearchParams([["query", query]]);

                if (site_id) {
                  searchParams.set("site_id", site_id);
                }

                router.push(
                  new SearchURL({
                    language,
                    pathname: "/profiles/1",
                    searchParams,
                  }),
                );
              }}
            />
          )}
        </ArticleHeader>
      </Article>

      {!profilesData ? (
        <LoadingBar />
      ) : (
        <CardList
          pagination={profilesData.pagination}
          urlBuilder={(page) => {
            const searchParams = new URLSearchParams([["query", searchQuery!]]);

            if (site_id) {
              searchParams.set("site_id", site_id);
            }

            const url = new SearchURL({
              language,
              pathname: `/profiles/${page}`,
              searchParams,
            });

            return url;
          }}
        >
          {profilesData.profiles.map((profile) => (
            <ProfileCard key={profile.id} profile={profile} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

function getTitle(
  searchQuery?: string,
  profilesData?: IAsyncReturnType<typeof fetchSearchProfiles>,
  site?: IAsyncReturnType<typeof fetchSite>,
) {
  const siteTitle = !site ? "" : `for site "${site.title}" (${site.id}) `;
  const title = !profilesData
    ? "Search profiles"
    : `Search Profiles ${siteTitle}with "${searchQuery}" page ${profilesData.pagination.current_page} out of ${profilesData.pagination.total_pages}`;

  return title;
}

export default SearchProfilesPage;
