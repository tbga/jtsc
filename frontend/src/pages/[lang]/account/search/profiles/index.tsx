import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { SearchURL } from "#lib/urls";
import { fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { SearchProfilesForm } from "#entities/profile";
import { type IAsyncReturnType } from "#types";

function SearchProfilesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const { isReady, query } = router;
  const searchQuery = resolveQueryValue(query.query);
  const site_id = resolveQueryValue(query.site_id);
  const title = !site
    ? "Search profiles"
    : `Search profiles for site "${site.title}" (${site.id})`;
  const heading = !site
    ? "Search Profiles"
    : `Search profiles for Site "${site.title}" (${site.id})`;

  useEffect(() => {
    if (!isReady) {
      return;
    }

    if (!site_id) {
      if (site) {
        changeSite(undefined);
      }
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [site_id, isReady]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <SearchProfilesForm
              id="search-profiles"
              searchQuery={searchQuery}
              site={site}
              onSearch={async ({ query, site_id }) => {
                const searchParams = new URLSearchParams([["query", query]]);

                if (site_id) {
                  searchParams.set("site_id", site_id);
                }

                router.push(
                  new SearchURL({
                    language,
                    pathname: "/profiles/1",
                    searchParams,
                  }),
                );
              }}
            />
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SearchProfilesPage;
