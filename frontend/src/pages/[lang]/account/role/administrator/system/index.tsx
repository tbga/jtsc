import { Article, Page } from "#components";
import { LocalFileSystem } from "#components/local-file-system";

function System() {
  const title = "System";
  const heading = "System";

  return (
    <Page title={title} heading={heading}>
      <Article>
        <LocalFileSystem />
      </Article>
    </Page>
  );
}

export default System;
