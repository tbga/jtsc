import { AdministratorURL } from "#lib/urls";
import { useLanguage } from "#hooks";
import { Article, Page } from "#components";
import { Link } from "#components/links";
import { Item, List } from "#components/lists";
import { PublicExportsURL } from "#entities/public-export";
import { PublicImportsURL } from "#entities/public-import";

/**
 * @TODO reorganize at semver major
 */
function AdministratorOverview() {
  const { language } = useLanguage();
  const title = "Administrator Overview";

  return (
    <Page title={title}>
      <Article>
        <List>
          <Item>
            <Link
              href={new AdministratorURL({ language, pathname: "/system" })}
            >
              System
            </Link>
          </Item>

          <Item>
            <Link
              href={new AdministratorURL({ language, pathname: "/operations" })}
            >
              Operations
            </Link>
          </Item>

          <Item>
            <Link href={new PublicExportsURL(language)}>Public exports</Link>
          </Item>

          <Item>
            <Link href={new PublicImportsURL(language)}>Public imports</Link>
          </Item>
        </List>
      </Article>
    </Page>
  );
}

export default AdministratorOverview;
