import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPost } from "#api/post";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { NewPostArtistsForm, PostURL } from "#entities/post";
import { SearchArtistsForm } from "#entities/artist";
import { type IAsyncReturnType } from "#types";

function CreatePostArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "New post artists"
    : `New artists for post "${post.title ?? "Untitled"}" (${post.id})`;
  const heading = !post
    ? "New Post Artists"
    : `New Artists for Post "${post.title ?? "Untitled"}" (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      const newPost = await fetchPost(post_id);

      changePost(newPost);
    })();
  }, [isReady, post_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          {!post ? (
            <LoadingBar />
          ) : (
            <List>
              <Item>
                <Link href={new PostURL(language, post.id)}>Post</Link>
              </Item>
            </List>
          )}
        </ArticleHeader>
        <ArticleBody>
          {!post ? <LoadingBar /> : <SearchArtistsForm />}
        </ArticleBody>
        <ArticleFooter>
          {!post ? (
            <LoadingBar />
          ) : (
            <NewPostArtistsForm
              id={`create-post-${post.id}-artists`}
              post_id={post.id}
            />
          )}
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default CreatePostArtists;
