import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleHeader, Page } from "#components";
import { Link } from "#components/links";
import { Item, List } from "#components/lists";
import { ProfileCreateURL } from "#entities/profile";
import { SiteCreateURL } from "#entities/site";
import { PublicExportCreateURL } from "#entities/public-export";
import { PublicImportCreateURL } from "#entities/public-import";
import { ReleaseCreateURL } from "#entities/release";

function CreateEntities() {
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const title = "Create";
  const heading = "Create";

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              <Link href={new SiteCreateURL(language)}>Sites</Link>
            </Item>

            <Item>
              <Link href={new ProfileCreateURL(language)}>Profiles</Link>
            </Item>

            <Item>
              <Link href={new ReleaseCreateURL(language)}>Releases</Link>
            </Item>

            {isAdmin && (
              <>
                <Item>
                  <Link href={new PublicExportCreateURL(language)}>
                    Public Exports
                  </Link>
                </Item>

                <Item>
                  <Link href={new PublicImportCreateURL(language)}>
                    Public Imports
                  </Link>
                </Item>
              </>
            )}
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default CreateEntities;
