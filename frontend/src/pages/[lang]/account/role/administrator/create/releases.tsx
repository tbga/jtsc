import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { NewReleaseForm } from "#entities/release";
import { SearchPostsForm } from "#entities/post";

function CreateReleases() {
  const router = useRouter();
  const { query, isReady } = router;
  const profile_ids = resolveQueryValue(query.profile_id, "all");
  const site_id = resolveQueryValue(query.site_id);
  const title = "Create releases";
  const heading = "Create Releases";

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <SearchPostsForm />
        </ArticleHeader>

        <ArticleBody>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <NewReleaseForm
              id={"new-release"}
              profile_ids={profile_ids}
              site_id={site_id}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreateReleases;
