import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import {
  getProfile,
  NewProfileNamesForm,
  ProfileEditURL,
  ProfileURL,
} from "#entities/profile";
import { type IAsyncReturnType } from "#types";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";

function NewName() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profileData, changeProfileData] =
    useState<IAsyncReturnType<typeof getProfile>>();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const title = !profileData
    ? "New profile name"
    : `New name for profile "${profileData.name ?? "Unknown"}" (${
        profileData.profile.id
      })`;
  const heading = !profileData
    ? "New Profile Name"
    : `New Name for Profile "${profileData.name ?? "Unknown"}" (${
        profileData.profile.id
      })`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const newProfileData = await getProfile(profile_id);
      changeProfileData(newProfileData);
    })();
  }, [isReady, profile_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link
                  href={
                    new ProfileEditURL(
                      language,
                      profileData.profile.id,
                      "edit-profile-names",
                    )
                  }
                >
                  Profile
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          {!profileData ? (
            <LoadingBar />
          ) : (
            <NewProfileNamesForm profile_id={profileData.profile.id} />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default NewName;
