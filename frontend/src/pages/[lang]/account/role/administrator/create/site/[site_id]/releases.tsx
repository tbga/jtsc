import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { SiteURL } from "#entities/site";
import { NewReleaseForm } from "#entities/release";
import { type IAsyncReturnType } from "#types";

function CreateSiteReleases() {
  const router = useRouter();
  const { language } = useLanguage();
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const { isReady, query } = router;
  const site_id = resolveQueryValue(query.site_id);
  const title = !site
    ? "Create site releases"
    : `Create releases for site "${site.title}" (${site.id})`;
  const heading = !site
    ? "Create Site Releases"
    : `Create Releases for Site "${site.title}" (${site.id})`;

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteURL(language, site.id)}>Site</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
        <ArticleBody>
          {!site ? (
            <LoadingBar />
          ) : (
            <NewReleaseForm
              id={`create-site-${site.id}-release`}
              site_id={site.id}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreateSiteReleases;
