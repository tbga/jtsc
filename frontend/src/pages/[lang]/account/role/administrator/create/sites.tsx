import { Article, ArticleBody, ArticleHeader, Page } from "#components";
import { NewSiteForm, SearchSitesForm } from "#entities/site";

function CreateSites() {
  const title = "New site";
  const heading = "New Site";

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <SearchSitesForm id="search-sites" />
        </ArticleHeader>
        <ArticleBody>
          <NewSiteForm id="create-new-site" />
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreateSites;
