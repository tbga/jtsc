import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  getProfile,
  NewProfileArtistsForm,
  ProfileURL,
} from "#entities/profile";
import { SearchArtistsForm } from "#entities/artist";
import { type IAsyncReturnType } from "#types";

function CreateProfileArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profileData, changeProfileData] =
    useState<IAsyncReturnType<typeof getProfile>>();
  const { isReady, query } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const profileName = profileData?.name ?? "Unknown";
  const title = !profileData
    ? "Create profile's artists"
    : `Create profile's "${profileName}" (${profile_id}) artists`;
  const heading = !profileData
    ? "Create Profile's Artists"
    : `Create Profile's "${profileName}" (${profile_id}) Artists`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const newProfileData = await getProfile(profile_id);

      changeProfileData(newProfileData);
    })();
  }, [isReady, profile_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link href={new ProfileURL(language, profileData.profile.id)}>
                  Profile
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <SearchArtistsForm />
        </ArticleBody>

        <ArticleFooter>
          {!profileData ? (
            <LoadingBar />
          ) : (
            <NewProfileArtistsForm
              id={`new-profile-${profileData.profile.id}-artists`}
              profile_id={profileData.profile.id}
            />
          )}
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default CreateProfileArtists;
