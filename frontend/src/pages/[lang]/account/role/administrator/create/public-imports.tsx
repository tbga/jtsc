import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleBody, ArticleHeader, Page } from "#components";
import { Button } from "#components/buttons";
import { LocalFileSystem } from "#components/local-file-system";
import { NewPublicImportForm } from "#entities/public-import";

function CreatePublicImports() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const [isMenuOpen, switchMenu] = useState(false);
  const title = "New public imports";
  const heading = "New Public Imports";

  useEffect(() => {
    if (!isAdmin) {
      router.push("/404");
    }
  });

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <Button
            onClick={() => {
              switchMenu(!isMenuOpen);
            }}
          >
            {isMenuOpen ? "Close" : "Open"} filesystem
          </Button>
          {/* @TODO fix inline style */}
          {isMenuOpen && <LocalFileSystem />}
        </ArticleHeader>
        <ArticleBody>
          <NewPublicImportForm id="create-new-public-import" />
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreatePublicImports;
