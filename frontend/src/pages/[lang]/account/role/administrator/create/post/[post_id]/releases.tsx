import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPost } from "#api/post";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { NewPostReleasesForm, PostURL } from "#entities/post";
import { NewReleaseForm, SearchReleasesForm } from "#entities/release";
import { type IAsyncReturnType } from "#types";

function CreatePostReleases() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "New post releases"
    : `New releases for post "${post.title ?? "Untitled"}" (${post.id})`;
  const heading = !post
    ? "New Post Releases"
    : `New Releases for Post "${post.title ?? "Untitled"}" (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      const newPost = await fetchPost(post_id);

      changePost(newPost);
    })();
  }, [isReady, post_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!post ? (
                <LoadingBar />
              ) : (
                <Link href={new PostURL(language, post.id)}>Post</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <SearchReleasesForm />
        </ArticleBody>

        <ArticleFooter>
          <Heading level={2}>Existing Releases</Heading>
          {!post ? (
            <LoadingBar />
          ) : (
            <NewPostReleasesForm
              id={`existing-${post.id}-releases`}
              post_id={post.id}
            />
          )}

          <Heading level={2}>New Releases</Heading>
          {!post ? (
            <LoadingBar />
          ) : (
            <NewReleaseForm
              id={`new-${post.id}-releases`}
              post_ids={[post.id]}
            />
          )}
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default CreatePostReleases;
