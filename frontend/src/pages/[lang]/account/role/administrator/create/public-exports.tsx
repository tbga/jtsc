import { useEffect } from "react";
import { useRouter } from "next/router";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleHeader, Page } from "#components";
import { NewPublicExportForm } from "#entities/public-export";

function CreatePublicExports() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const title = "New public exports";
  const heading = "New Public Exports";

  useEffect(() => {
    if (!isAdmin) {
      router.push("/404");
    }
  });

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <NewPublicExportForm id="create-new-public-export" />
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default CreatePublicExports;
