import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease } from "#api/releases";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { NewReleasePostsForm, ReleaseURL } from "#entities/release";
import { SearchPostsForm } from "#entities/post";
import { type IAsyncReturnType } from "#types";

function CreateReleasePosts() {
  const router = useRouter();
  const { language } = useLanguage();
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const { query, isReady } = router;
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "Create posts for release"
    : `Create posts for release "${release.title ?? "Unknown"}" (${
        release.id
      })`;
  const heading = !release
    ? "Create Posts for Release"
    : `Create Posts for Release "${release.title ?? "Unknown"}" (${
        release.id
      })`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newRelease = await fetchRelease(release_id);
      changeRelease(newRelease);
    })();
  }, [isReady, release_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <SearchPostsForm />
        </ArticleBody>

        <ArticleFooter>
          {!release ? (
            <LoadingBar />
          ) : (
            <NewReleasePostsForm
              id={`new-release-${release.id}-posts`}
              key={release.id}
              release_id={release.id}
            />
          )}
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default CreateReleasePosts;
