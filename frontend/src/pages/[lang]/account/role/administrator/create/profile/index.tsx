import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { NewProfileForm } from "#entities/profile";
import { type ISite } from "#entities/site";
import { ArtistURL, getArtist, type IArtist } from "#entities/artist";

function NewProfilePage() {
  const router = useRouter();
  const { isReady, query } = router;
  const [site, changeSite] = useState<ISite>();
  const [artist, changeArtist] = useState<IArtist>();
  const { isLoading, language } = useLanguage();
  const artist_id = resolveQueryValue(query.artist_id);
  const site_id = resolveQueryValue(query.site_id);
  const title = !isReady
    ? "New Profile"
    : artist
      ? `New Profile for Artist "${artist.name ?? "Unknown"}" (${artist.id})`
      : site
        ? `New Profile for Site "${site.title}" (${site.id})`
        : "New Profile";

  useEffect(() => {
    if (!isReady || !artist_id) {
      return;
    }

    (async () => {
      const newArtist = await getArtist(artist_id);
      changeArtist(newArtist);
    })();
  }, [isReady, artist_id]);

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  return (
    <Page title={title}>
      <Article>
        <ArticleHeader>
          {artist &&
            (isLoading ? (
              <LoadingBar />
            ) : (
              <List>
                <Item>
                  <Link href={new ArtistURL(language, artist.id)}>Artist</Link>
                </Item>
              </List>
            ))}
        </ArticleHeader>

        <ArticleBody>
          {!isReady ? (
            <LoadingBar />
          ) : (
            <NewProfileForm
              id="create-new-profile"
              artist_ids={!artist_id ? undefined : [artist_id]}
              site_id={site_id}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default NewProfilePage;
