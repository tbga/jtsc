import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease } from "#api/releases";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { NewReleaseProfilesForm, ReleaseURL } from "#entities/release";
import { NewProfileForm } from "#entities/profile";
import { type IAsyncReturnType } from "#types";

function CreateReleaseProfiles() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isReady, query } = router;
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "New release profiles"
    : `New profiles for release "${release.title ?? "Untitled"}" (${
        release.id
      })`;
  const heading = !release
    ? "New Release Profiles"
    : `New Profiles for Release "${release.title ?? "Untitled"}" (${
        release.id
      })`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newPost = await fetchRelease(release_id);

      changeRelease(newPost);
    })();
  }, [isReady, release_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>Existing Profiles</Heading>
        </ArticleHeader>
        <ArticleBody>
          {!release ? (
            <LoadingBar />
          ) : (
            <NewReleaseProfilesForm
              id={`existing-${release.id}-profiles`}
              release_id={release.id}
            />
          )}
        </ArticleBody>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>New Profiles</Heading>
        </ArticleHeader>
        <ArticleBody>
          {!release ? (
            <LoadingBar />
          ) : (
            <NewProfileForm
              id={`new-${release.id}-profiles`}
              site_id={release.site?.id}
              release_ids={[release.id]}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreateReleaseProfiles;
