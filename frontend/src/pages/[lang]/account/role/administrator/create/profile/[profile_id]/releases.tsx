import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchProfile } from "#api/profiles";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import { ProfileURL } from "#entities/profile";
import { NewReleaseForm } from "#entities/release";

function CreateProfileReleases() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profile, changeProfile] =
    useState<Awaited<ReturnType<typeof fetchProfile>>>();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const title = !profile
    ? "New profile releases"
    : `New releases for profile "${profile.name ?? "Unknown"}" (${profile.id})`;
  const heading = !profile
    ? "New Profile Releases"
    : `New Releases for Profile "${profile.name ?? "Unknown"}" (${profile.id})`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const newProfile = await fetchProfile(profile_id);
      changeProfile(newProfile);
    })();
  }, [isReady, profile_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profile ? (
                <LoadingBar />
              ) : (
                <Link href={new ProfileURL(language, profile.id)}>Profile</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
        <ArticleBody>
          {!profile ? (
            <LoadingBar />
          ) : (
            <NewReleaseForm
              id="new-release"
              site_id={profile.site?.id}
              profile_ids={[profile.id]}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default CreateProfileReleases;
