import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { capitalizeString } from "#lib/strings";
import { fetchPublicImport } from "#api/account/administrator/public-imports";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { PublicImportArticle } from "#entities/public-import";
import { type IAsyncReturnType } from "#types";

function PublicImportDetails() {
  const router = useRouter();
  const [publicImport, changePublicImport] =
    useState<IAsyncReturnType<typeof fetchPublicImport>>();
  const { query, isReady } = router;
  const public_import_id = resolveQueryValue(query.public_import_id);
  const title = !publicImport
    ? "Public import"
    : `${capitalizeString(publicImport.status)} public import "${
        publicImport.title
      }" (${publicImport.id})`;
  const heading = "Public Import";

  useEffect(() => {
    if (!isReady || !public_import_id) {
      return;
    }

    (async () => {
      const newPublicImport = await fetchPublicImport(public_import_id);
      changePublicImport(newPublicImport);
    })();
  }, [isReady, public_import_id]);

  return (
    <Page title={title} heading={heading}>
      {!publicImport ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <PublicImportArticle
          publicImport={publicImport}
          onPublicImportChange={async (updatedImport) => {
            changePublicImport(updatedImport);
          }}
        />
      )}
    </Page>
  );
}

export default PublicImportDetails;
