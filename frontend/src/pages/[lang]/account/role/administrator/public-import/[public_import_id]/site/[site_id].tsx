import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import {
  fetchPublicImport,
  fetchPublicImportSite,
} from "#api/account/administrator/public-imports";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { PublicImportSiteArticle } from "#entities/public-import";
import { IAsyncReturnType } from "#types";

function PublicImportSite() {
  const router = useRouter();
  const [publicImport, changePublicImport] =
    useState<IAsyncReturnType<typeof fetchPublicImport>>();
  const [site, changeSite] =
    useState<IAsyncReturnType<typeof fetchPublicImportSite>>();
  const { query, isReady } = router;
  const publicImportID = resolveQueryValue(query.public_import_id);
  const siteID = resolveQueryValue(query.site_id);
  const title =
    !publicImport || !site
      ? "Public import site"
      : `Site "${site.title}" (${site.id}) of public import "${publicImport.title}" (${publicImport.id})`;

  useEffect(() => {
    if (!isReady || !siteID || !publicImportID) {
      return;
    }

    (async () => {
      const newPublicImport = await fetchPublicImport(publicImportID);
      changePublicImport(newPublicImport);
      const newSite = await fetchPublicImportSite(publicImportID, siteID);
      changeSite(newSite);
    })();
  }, [isReady, publicImportID, siteID]);

  return (
    <Page title={title} heading={null}>
      {!site ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <PublicImportSiteArticle
          key={site.id}
          site={site}
          headingLevel={1}
          onApprovalChange={async (updatedSite) => changeSite(updatedSite)}
        />
      )}
    </Page>
  );
}

export default PublicImportSite;
