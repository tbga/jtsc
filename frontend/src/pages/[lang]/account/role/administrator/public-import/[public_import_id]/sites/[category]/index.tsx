import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { isIncludedInArray } from "#lib/std/array";
import { ProjectError } from "#lib/errors";
import { capitalizeString } from "#lib/strings";
import {
  type IPublicImportSiteCategoryAPI,
  fetchPublicImportSitesPagination,
} from "#api/account/administrator/public-imports";
import { paginationRedirectMulti } from "#components";
import { PublicImportSitesURL } from "#entities/public-import";
import { type IBigSerialInteger } from "#types";

const PublicImportSites = paginationRedirectMulti({
  queryResolver: (query) => {
    const category = resolveQueryValue(query.category);

    // if (!isIncludedInArray(publicImportSiteCategories, category)) {
    //   throw new ProjectError(`Unknown category "${category}"`);
    // }

    return {
      public_import_id: resolveQueryValue(query.public_import_id),
      category,
    };
  },
  prefix: ({ category }) => `${category} public import sites`,
  title: ({ category }) => `${category} Public Import Sites`,
  paginationFetcher: async ({ public_import_id, category }) => {
    return fetchPublicImportSitesPagination(
      public_import_id as IBigSerialInteger,
      category as IPublicImportSiteCategoryAPI,
    );
  },
  urlBuilder: (page, { public_import_id, category }) =>
    new PublicImportSitesURL(
      DEFAULT_LANGUAGE,
      public_import_id as IBigSerialInteger,
      category as IPublicImportSiteCategoryAPI,
      page as IBigSerialInteger,
    ),
});

export default PublicImportSites;
