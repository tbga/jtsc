import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { capitalizeString } from "#lib/strings";
import { isIncludedInArray } from "#lib/std/array";
import { ProjectError } from "#lib/errors";
import {
  fetchPublicImport,
  fetchPublicImportSites,
  publicImportSiteCategoriesAPI,
} from "#api/account/administrator/public-imports";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  PublicImportSiteCard,
  PublicImportSitesURL,
  PublicImportURL,
} from "#entities/public-import";
import { type IAsyncReturnType } from "#types";

function PublicImportSitesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicImport, changePublicImport] =
    useState<IAsyncReturnType<typeof fetchPublicImport>>();
  const [sitesData, changeSitesData] =
    useState<IAsyncReturnType<typeof fetchPublicImportSites>>();
  const { isReady, query } = router;
  const publicImportID = resolveQueryValue(query.public_import_id);
  const category = resolveQueryValue(query.category);
  const page = resolveQueryValue(query.page);
  const title =
    !publicImport || !sitesData || !category
      ? "Public import sites"
      : `${capitalizeString(category)} public import "${publicImport.title}" (${
          publicImport.id
        }) sites page ${sitesData.pagination.current_page} out of ${
          sitesData.pagination.total_pages
        }`;
  const heading = !category
    ? "Public Import Sites"
    : `${capitalizeString(category)} Public Import Sites`;

  useEffect(() => {
    if (!isReady || !publicImportID || !category || !page) {
      return;
    }

    if (!isIncludedInArray(publicImportSiteCategoriesAPI, category)) {
      throw new ProjectError(`Unknown category "${category}".`);
    }

    (async () => {
      const newPublicExport = await fetchPublicImport(publicImportID);

      changePublicImport(newPublicExport);

      const newSites = await fetchPublicImportSites(
        newPublicExport.id,
        category,
        page,
      );

      changeSitesData(newSites);
    })();
  }, [isReady, page, publicImportID]);

  return (
    <Page title={title} heading={heading}>
      {!publicImport ||
      !sitesData ||
      !isIncludedInArray(publicImportSiteCategoriesAPI, category) ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          pagination={sitesData.pagination}
          urlBuilder={(page) =>
            new PublicImportSitesURL(
              language,
              publicImportID as string,
              category,
              page as string,
            )
          }
        >
          {sitesData.sites.map((site) => (
            <PublicImportSiteCard key={site.id} headingLevel={2} site={site} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!publicImport ? (
                <LoadingBar />
              ) : (
                <Link href={new PublicImportURL(language, publicImport.id)}>
                  Public import
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default PublicImportSitesPage;
