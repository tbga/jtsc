import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { DURATION_MS } from "#environment/constants";
import { resolveQueryValue } from "#lib/util";
import { capitalizeString } from "#lib/strings";
import { fetchOperation } from "#api/account/administrator/operations";
import { useInterval } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { OperationArticle } from "#entities/operation";

function OperationDetails() {
  const router = useRouter();
  const { query, isReady } = router;
  const [operation, changeOperation] =
    useState<Awaited<ReturnType<typeof fetchOperation>>>();
  const operation_id = resolveQueryValue(query.operation_id);
  const isResolved =
    operation &&
    (operation.status === "finished" || operation.status === "failed");
  const title = capitalizeString(
    !operation
      ? "Operation"
      : `${operation.status} operation "${operation.type}" (${operation.id})`,
  );
  const heading = "Operation";

  useInterval(
    async () => {
      if (!isReady || !operation_id) {
        return;
      }

      const newOperation = await fetchOperation(operation_id);
      changeOperation(newOperation);
    },
    isResolved ? undefined : DURATION_MS.SECOND * 30,
  );

  useEffect(() => {
    if (!isReady || !operation_id) {
      return;
    }
    (async () => {
      const newOperation = await fetchOperation(operation_id);
      changeOperation(newOperation);
    })();
  }, [isReady, operation_id]);

  return (
    <Page title={title} heading={heading}>
      {!operation ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <OperationArticle operation={operation} headingLevel={2} />
      )}
    </Page>
  );
}

export default OperationDetails;
