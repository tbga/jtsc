import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPublicExports } from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { type IAsyncReturnType } from "#types";
import { PublicExportCard, PublicExportsURL } from "#entities/public-export";

function PublicExportsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicExportsData, changePublicExportsData] =
    useState<IAsyncReturnType<typeof fetchPublicExports>>();
  const { query, isReady } = router;
  const page = resolveQueryValue(query.page);
  const title = !publicExportsData
    ? "Public exports"
    : `Public exports ${publicExportsData.pagination.current_page} out of ${publicExportsData.pagination.total_pages}`;
  const heading = "Public Exports";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }
    (async () => {
      const newPublicExports = await fetchPublicExports(page);
      changePublicExportsData(newPublicExports);
    })();
  }, [isReady, page]);

  return (
    <Page title={title} heading={heading}>
      {!publicExportsData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={publicExportsData.pagination}
          urlBuilder={(page) => new PublicExportsURL(language, page as string)}
        >
          {publicExportsData.public_exports.map((publicExport) => (
            <PublicExportCard
              key={publicExport.id}
              publicExport={publicExport}
            />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default PublicExportsPage;
