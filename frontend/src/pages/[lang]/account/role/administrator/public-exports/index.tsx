import { DEFAULT_LANGUAGE } from "#environment/variables";
import { fetchPublicExportsPagination } from "#api/account/administrator/public-exports";
import { paginationRedirectMulti } from "#components";
import { PublicExportsURL } from "#entities/public-export";
import { EMPTY_OBJECT } from "#types";

const PublicExports = paginationRedirectMulti({
  queryResolver: (query) => EMPTY_OBJECT,
  prefix: "public exports",
  title: (query) => "Public Exports",
  paginationFetcher: fetchPublicExportsPagination,
  urlBuilder: (page) => new PublicExportsURL(DEFAULT_LANGUAGE, page as string),
});

export default PublicExports;
