import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { sortByIDAsc } from "#lib/entities";
import { fetchProfileArtists } from "#api/profiles";
import { fetchRemoveProfileArtists } from "#api/account/administrator/profile";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { CardListInternal, Item, List, ListButtons } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { CardListLocal } from "#components/lists/cards";
import { Link } from "#components/links";
import { getProfile, ProfileEditURL } from "#entities/profile";
import { ArtistCard, type IArtistPreview } from "#entities/artist";
import { BIGINT_ONE, type IAsyncReturnType } from "#types";

function RemoveProfileArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [profileData, changeProfileData] =
    useState<IAsyncReturnType<typeof getProfile>>();
  const [artistsData, changeArtistsData] =
    useState<IAsyncReturnType<typeof fetchProfileArtists>>();
  const [selectedArtists, changeSelectedArtists] = useState<IArtistPreview[]>(
    [],
  );
  const { isReady, query } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const profilename = profileData?.name ?? "Unknown";
  const title = !profileData
    ? "Remove artists from profile"
    : `Remove artists from profile "${profilename}" (${profileData.profile.id})`;
  const heading = !profileData
    ? "Remove Artists from Profile"
    : `Remove Artists from Profile "${profilename}" (${profileData.profile.id})`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const isNewProfileData = profile_id !== profileData?.profile.id;
        const newProfileData = !isNewProfileData
          ? profileData
          : await getProfile(profile_id);

        if (isNewProfileData) {
          changeProfileData(newProfileData);
        }

        const newArtistsData = await fetchProfileArtists(
          newProfileData.profile.id,
          "1",
        );

        changeArtistsData(newArtistsData);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isReady, profile_id]);

  function selectArtist(artist: IArtistPreview) {
    const selectedArtist = selectedArtists.find(({ id }) => id === artist.id);

    if (selectedArtist) {
      return;
    }

    const newSelection = Array.from(selectedArtists);
    newSelection.push(artist);
    newSelection.sort(sortByIDAsc);

    changeSelectedArtists(newSelection);
  }

  function deselectArtist(artist: IArtistPreview) {
    const newSelection = selectedArtists.filter(({ id }) => id !== artist.id);

    if (newSelection.length === selectedArtists.length) {
      return;
    }

    changeSelectedArtists(newSelection);
  }

  async function removeArtists() {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      const selectedIDs = selectedArtists.map(({ id }) => id);
      await fetchRemoveProfileArtists(profileData!.profile.id, selectedIDs);

      const newArtistsData = await fetchProfileArtists(
        profileData!.profile.id,
        String(BIGINT_ONE),
      );

      changeArtistsData(newArtistsData);
      changeSelectedArtists([]);
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link
                  href={new ProfileEditURL(language, profileData.profile.id)}
                >
                  Profile
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
      <Heading isExternal level={2}>
        Artists
      </Heading>
      {!isReady ? (
        <LoadingBar />
      ) : !artistsData ? (
        <Article>
          <ArticleHeader>No artists available for this profile.</ArticleHeader>
        </Article>
      ) : (
        <CardListInternal
          pagination={artistsData.pagination}
          onPageChange={async (page) => {
            const newArtistsData = await fetchProfileArtists(profile_id!, page);

            changeArtistsData(newArtistsData);
          }}
        >
          {artistsData.artists.map((artist) => {
            const isSelected = selectedArtists.includes(artist);

            return (
              <ArtistCard key={artist.id} artist={artist}>
                <Button
                  onClick={() => {
                    !isSelected ? selectArtist(artist) : deselectArtist(artist);
                  }}
                >
                  {!isSelected ? "Select" : "Deselect"}
                </Button>
              </ArtistCard>
            );
          })}
        </CardListInternal>
      )}
      <Heading isExternal level={2}>
        Selected artists
      </Heading>
      {!selectedArtists.length ? (
        <Article>
          <ArticleHeader>No artists were selected for removal.</ArticleHeader>
        </Article>
      ) : (
        <CardListLocal
          items={selectedArtists.map((artist) => (
            <ArtistCard key={artist.id} artist={artist}>
              <Button
                onClick={() => {
                  deselectArtist(artist);
                }}
              >
                Deselect
              </Button>
            </ArtistCard>
          ))}
        />
      )}
      <Article>
        <ArticleBody>
          <ListButtons>
            <ButtonNegative
              disabled={isLoading || selectedArtists.length === 0}
              onClick={async () => {
                await removeArtists();
              }}
            >
              Remove Selected
            </ButtonNegative>
          </ListButtons>
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default RemoveProfileArtists;
