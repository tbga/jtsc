import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { sortByIDAsc } from "#lib/entities";
import {
  fetchPost,
  fetchPostArtists,
  fetchPostArtistsPagination,
} from "#api/post";
import { fetchPostArtistsRemove } from "#api/account/administrator/posts";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { CardListInternal, Item, List, ListButtons } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { CardListLocal } from "#components/lists/cards";
import { Link } from "#components/links";
import { ArtistCard, type IArtistPreview } from "#entities/artist";
import { PostURL } from "#entities/post";
import { BIGINT_ONE, BIGINT_ZERO, type IAsyncReturnType } from "#types";

function RemovePostArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const [artistsData, changeArtistsData] =
    useState<IAsyncReturnType<typeof fetchPostArtists>>();
  const [selectedArtists, changeSelectedArtists] = useState<IArtistPreview[]>(
    [],
  );
  const { isReady, query } = router;
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "Remove artists from post"
    : `Remove artists from post (${post.id})`;
  const heading = !post
    ? "Remove Artists from Post"
    : `Remove Artists from Post (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const isNewPost = post_id !== post?.id;
        const newPost = !isNewPost ? post : await fetchPost(post_id);

        if (isNewPost) {
          changePost(newPost);
        }

        const pagination = await fetchPostArtistsPagination(newPost.id);

        if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
          return;
        }

        const newArtistsData = await fetchPostArtists(
          newPost.id,
          String(BIGINT_ONE),
        );

        changeArtistsData(newArtistsData);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isReady, post_id]);

  function selectArtist(artist: IArtistPreview) {
    const selectedArtist = selectedArtists.find(({ id }) => id === artist.id);

    if (selectedArtist) {
      return;
    }

    const newSelection = Array.from(selectedArtists);
    newSelection.push(artist);
    newSelection.sort(sortByIDAsc);

    changeSelectedArtists(newSelection);
  }

  function deselectArtist(artist: IArtistPreview) {
    const newSelection = selectedArtists.filter(({ id }) => id !== artist.id);

    if (newSelection.length === selectedArtists.length) {
      return;
    }

    changeSelectedArtists(newSelection);
  }

  async function removeArtists() {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      const selectedIDs = selectedArtists.map(({ id }) => id);
      await fetchPostArtistsRemove(post!.id, selectedIDs);

      const pagination = await fetchPostArtistsPagination(post!.id);

      if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
        changeArtistsData(undefined);
        changeSelectedArtists([]);
      } else {
        const newArtistsData = await fetchPostArtists(
          post!.id,
          String(BIGINT_ONE),
        );

        changeArtistsData(newArtistsData);
        changeSelectedArtists([]);
      }
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleBody>
          {!post ? (
            <LoadingBar />
          ) : (
            <List>
              <Item>
                <Link href={new PostURL(language, post.id)}>Post</Link>
              </Item>
            </List>
          )}
        </ArticleBody>
      </Article>
      <Heading isExternal level={2}>
        Post artists
      </Heading>
      {!isReady ? (
        <LoadingBar />
      ) : !artistsData ? (
        <Article>
          <ArticleHeader>No artists available for this post.</ArticleHeader>
        </Article>
      ) : (
        <CardListInternal
          pagination={artistsData.pagination}
          onPageChange={async (page) => {
            const newArtistsData = await fetchPostArtists(post_id!, page);

            changeArtistsData(newArtistsData);
          }}
        >
          {artistsData.artists.map((artist) => {
            const isSelected = selectedArtists.includes(artist);

            return (
              <ArtistCard key={artist.id} artist={artist}>
                <Button
                  onClick={() => {
                    !isSelected ? selectArtist(artist) : deselectArtist(artist);
                  }}
                >
                  {!isSelected ? "Select" : "Deselect"}
                </Button>
              </ArtistCard>
            );
          })}
        </CardListInternal>
      )}
      <Heading isExternal level={2}>
        Selected artists
      </Heading>
      {!selectedArtists.length ? (
        <Article>
          <ArticleHeader>No artists were selected for removal.</ArticleHeader>
        </Article>
      ) : (
        <CardListLocal
          items={selectedArtists.map((artist) => (
            <ArtistCard key={artist.id} artist={artist}>
              <Button
                onClick={() => {
                  deselectArtist(artist);
                }}
              >
                Deselect
              </Button>
            </ArtistCard>
          ))}
        />
      )}
      <Article>
        <ArticleBody>
          <ListButtons>
            <ButtonNegative
              disabled={isLoading || selectedArtists.length === 0}
              onClick={async () => {
                await removeArtists();
              }}
            >
              Remove Selected
            </ButtonNegative>
          </ListButtons>
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default RemovePostArtists;
