import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease } from "#api/releases";
import { useLanguage } from "#hooks";
import { Article, LoadingBar, Page } from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  ReleaseEditURL,
  ReleaseURL,
  RemoveReleaseProfilesForm,
} from "#entities/release";
import { type IAsyncReturnType } from "#types";

function RemoveReleaseProfiles() {
  const router = useRouter();
  const { language } = useLanguage();
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const { isReady, query } = router;
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "Remove release profiles"
    : `Remove profiles from release "${release.title ?? "Untitled"}" (${
        release.id
      })`;
  const heading = !release
    ? "Remove Release Profiles"
    : `Remove Profiles from Release "${release.title ?? "Untitled"}" (${
        release.id
      })`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const release = await fetchRelease(release_id);
      changeRelease(release);
    })();
  }, [isReady, release_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <List>
          <Item>
            {!release ? (
              <LoadingBar />
            ) : (
              <Link href={new ReleaseEditURL(language, release.id, "profiles")}>
                Release
              </Link>
            )}
          </Item>
        </List>
      </Article>

      {!release ? (
        <LoadingBar />
      ) : (
        <RemoveReleaseProfilesForm release_id={release.id} />
      )}
    </Page>
  );
}

export default RemoveReleaseProfiles;
