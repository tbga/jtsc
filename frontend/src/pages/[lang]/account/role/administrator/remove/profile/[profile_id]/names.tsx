import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { type IEntityItem, sortByIDAsc } from "#lib/entities";
import { fetchProfileNames, fetchProfileNamesPagination } from "#api/profiles";
import { fetchProfileNamesRemove } from "#api/account/administrator/profile";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { Item, List } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { getProfile, ProfileEditURL } from "#entities/profile";
import { BIGINT_ONE, BIGINT_ZERO, type IAsyncReturnType } from "#types";
import { Link } from "#components/links";
import { ListInternal, ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";

import styles from "./names.module.scss";

function RemoveProfileArtists() {
  const router = useRouter();
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [profileData, changeProfileData] =
    useState<IAsyncReturnType<typeof getProfile>>();
  const [namesData, changeNamesData] =
    useState<IAsyncReturnType<typeof fetchProfileNames>>();
  const [selectedNames, changeSelectedNames] = useState<IEntityItem[]>([]);
  const { isReady, query } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const profilename = profileData?.name ?? "Unknown";
  const title = !profileData
    ? "Remove profile names"
    : `Remove names from profile "${profilename}" (${profileData.profile.id})`;
  const heading = !profileData
    ? "Remove Profile Names"
    : `Remove Names from Profile "${profilename}" (${profileData.profile.id})`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const newProfileData = await getProfile(profile_id);

        changeProfileData(newProfileData);

        const pagination = await fetchProfileNamesPagination(profile_id);

        if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
          return;
        }

        const names = await fetchProfileNames(profile_id, String(BIGINT_ONE));

        changeNamesData(names);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isReady, profile_id]);

  function selectName(name: IEntityItem) {
    const selectedName = selectedNames.find(({ id }) => id === name.id);

    if (selectedName) {
      return;
    }

    const newSelection = Array.from(selectedNames);
    newSelection.push(name);
    newSelection.sort(sortByIDAsc);

    changeSelectedNames(newSelection);
  }

  function deselectName(name: IEntityItem) {
    const newSelection = selectedNames.filter(({ id }) => id !== name.id);

    if (newSelection.length === selectedNames.length) {
      return;
    }

    changeSelectedNames(newSelection);
  }

  async function removeNames() {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      const selectedIDs = selectedNames.map(({ id }) => id);
      await fetchProfileNamesRemove(profileData!.profile.id, selectedIDs);

      const pagination = await fetchProfileNamesPagination(
        profileData!.profile.id,
      );

      if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
        changeNamesData(undefined);
        changeSelectedNames([]);
        return;
      }

      const names = await fetchProfileNames(
        profileData!.profile.id,
        String(BIGINT_ONE),
      );

      changeNamesData(names);
      changeSelectedNames([]);
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link
                  href={
                    new ProfileEditURL(
                      language,
                      profileData.profile.id,
                      "edit-profile-names",
                    )
                  }
                >
                  Profile
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
      <Article>
        <ArticleHeader>
          <Heading level={2}>Names</Heading>
        </ArticleHeader>
        <ArticleBody>
          {!profileData || !namesData ? (
            <LoadingBar />
          ) : (
            <ListInternal
              isAlternating
              pagination={namesData.pagination}
              onPageChange={async (page) => {
                const newNames = await fetchProfileNames(
                  profileData.profile.id,
                  page,
                );
                changeNamesData(newNames);
              }}
            >
              {namesData.names.map((name) => {
                const isSelected = Boolean(
                  selectedNames.find(({ id }) => id === name.id),
                );

                return (
                  <EntityItem key={name.id} item={name}>
                    <Button
                      className={styles.select}
                      onClick={async () => {
                        !isSelected ? selectName(name) : deselectName(name);
                      }}
                    >
                      {!isSelected ? "Select" : "Deselect"}
                    </Button>
                  </EntityItem>
                );
              })}
            </ListInternal>
          )}
        </ArticleBody>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>Selected Names</Heading>
        </ArticleHeader>

        <ArticleBody>
          {!selectedNames.length ? (
            <p>No names are selected</p>
          ) : (
            <ListLocal
              items={selectedNames.map((name) => {
                return (
                  <EntityItem key={name.id} item={name}>
                    <Button
                      className={styles.select}
                      onClick={async () => {
                        deselectName(name);
                      }}
                    >
                      Deselect
                    </Button>
                  </EntityItem>
                );
              })}
            />
          )}
        </ArticleBody>

        <ArticleFooter>
          <ButtonNegative
            disabled={!selectedNames.length}
            onClick={async () => {
              removeNames();
            }}
          >
            Remove
          </ButtonNegative>
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default RemoveProfileArtists;
