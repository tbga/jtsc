import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { sortByIDAsc } from "#lib/entities";
import {
  fetchPost,
  fetchPostReleases,
  fetchPostReleasesPagination,
} from "#api/post";
import { fetchPostReleasesRemove } from "#api/account/administrator/posts";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, Heading, LoadingBar, Page } from "#components";
import { CardListInternal, Item, List, ListButtons } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { CardListLocal } from "#components/lists/cards";
import { Link } from "#components/links";
import { PostURL } from "#entities/post";
import { ReleaseCard, type IReleasePreview } from "#entities/release";
import { BIGINT_ONE, BIGINT_ZERO, type IAsyncReturnType } from "#types";

function RemovePostReleases() {
  const router = useRouter();
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const [releasesData, changeReleasesData] =
    useState<IAsyncReturnType<typeof fetchPostReleases>>();
  const [selectedReleases, changeSelectedReleases] = useState<
    IReleasePreview[]
  >([]);
  const { isReady, query } = router;
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "Remove releases from post"
    : `Remove releases from post (${post.id})`;
  const heading = !post
    ? "Remove Releases from Post"
    : `Remove Releases from Post (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const newPost = await fetchPost(post_id);

        changePost(newPost);

        const pagination = await fetchPostReleasesPagination(newPost.id);

        if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
          return;
        }

        const newReleasesData = await fetchPostReleases(
          newPost.id,
          String(BIGINT_ONE),
        );

        changeReleasesData(newReleasesData);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isReady, post_id]);

  function selectRelease(release: IReleasePreview) {
    const selectedRelease = selectedReleases.find(
      ({ id }) => id === release.id,
    );

    if (selectedRelease) {
      return;
    }

    const newSelection = Array.from(selectedReleases);
    newSelection.push(release);
    newSelection.sort(sortByIDAsc);

    changeSelectedReleases(newSelection);
  }

  function deselectRelease(release: IReleasePreview) {
    const newSelection = selectedReleases.filter(({ id }) => id !== release.id);

    if (newSelection.length === selectedReleases.length) {
      return;
    }

    changeSelectedReleases(newSelection);
  }

  async function removeReleases() {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      const selectedIDs = selectedReleases.map(({ id }) => id);
      await fetchPostReleasesRemove(post!.id, selectedIDs);

      const pagination = await fetchPostReleasesPagination(post!.id);

      if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
        changeReleasesData(undefined);
        changeSelectedReleases([]);
      } else {
        const newReleaseData = await fetchPostReleases(
          post!.id,
          String(BIGINT_ONE),
        );

        changeReleasesData(newReleaseData);
        changeSelectedReleases([]);
      }
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!post ? (
                <LoadingBar />
              ) : (
                <Link href={new PostURL(language, post.id)}>Post</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>

      <Heading isExternal level={2}>
        Post releases
      </Heading>
      {!isReady ? (
        <LoadingBar />
      ) : !releasesData ? (
        <Article>
          <ArticleHeader>No releases available for this post.</ArticleHeader>
        </Article>
      ) : (
        <CardListInternal
          pagination={releasesData.pagination}
          onPageChange={async (page) => {
            const newReleaseData = await fetchPostReleases(post_id!, page);

            changeReleasesData(newReleaseData);
          }}
        >
          {releasesData.releases.map((release) => {
            const isSelected = selectedReleases.includes(release);

            return (
              <ReleaseCard key={release.id} release={release}>
                <Button
                  onClick={() => {
                    !isSelected
                      ? selectRelease(release)
                      : deselectRelease(release);
                  }}
                >
                  {!isSelected ? "Select" : "Deselect"}
                </Button>
              </ReleaseCard>
            );
          })}
        </CardListInternal>
      )}

      <Heading isExternal level={2}>
        Selected releases
      </Heading>
      {!selectedReleases.length ? (
        <Article>
          <ArticleHeader>No releases were selected for removal.</ArticleHeader>
        </Article>
      ) : (
        <CardListLocal
          items={selectedReleases.map((release) => (
            <ReleaseCard key={release.id} release={release}>
              <Button
                onClick={() => {
                  deselectRelease(release);
                }}
              >
                Deselect
              </Button>
            </ReleaseCard>
          ))}
        />
      )}

      <Article>
        <ArticleHeader>
          <ListButtons>
            <ButtonNegative
              disabled={isLoading || selectedReleases.length === 0}
              onClick={async () => {
                await removeReleases();
              }}
            >
              Remove Selected
            </ButtonNegative>
          </ListButtons>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default RemovePostReleases;
