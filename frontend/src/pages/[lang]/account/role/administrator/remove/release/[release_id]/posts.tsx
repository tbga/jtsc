import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { isEmpty, resolveQueryValue } from "#lib/util";
import { sortByIDAsc } from "#lib/entities";
import {
  fetchRelease,
  fetchReleasePosts,
  fetchReleasePostsPagination,
} from "#api/releases";
import { fetchRemoveReleasePosts } from "#api/account/administrator/releases";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { CardListInternal, Item, List, ListButtons } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { CardListLocal } from "#components/lists/cards";
import { PostCard, type IPostPreview } from "#entities/post";
import { BIGINT_ONE, BIGINT_ZERO, type IAsyncReturnType } from "#types";
import { Link } from "#components/links";
import { ReleaseURL } from "#entities/release";

function RemoveReleasePosts() {
  const router = useRouter();
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const [postsData, changePostsData] =
    useState<IAsyncReturnType<typeof fetchReleasePosts>>();
  const [selectedPosts, changeSelectedPosts] = useState<IPostPreview[]>([]);
  const { isReady, query } = router;
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "Remove posts from release"
    : `Remove posts from release "${release.title ?? "Untitled"}" (${
        release.id
      })`;
  const heading = !release
    ? "Remove Posts from Release"
    : `Remove Posts from Release "${release.title ?? "Untitled"}" (${
        release.id
      })`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const newRelease = await fetchRelease(release_id);

        changeRelease(newRelease);

        const pagination = await fetchReleasePostsPagination(newRelease.id);

        if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
          return;
        }

        const newPostsData = await fetchReleasePosts(
          newRelease.id,
          String(BIGINT_ONE),
        );

        changePostsData(newPostsData);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isReady, release_id]);

  function selectPost(post: IPostPreview) {
    const posts = selectedPosts.find(({ id }) => id === post.id);

    if (posts) {
      return;
    }

    const newSelection = Array.from(selectedPosts);
    newSelection.push(post);
    newSelection.sort(sortByIDAsc);

    changeSelectedPosts(newSelection);
  }

  function deselectPost(post: IPostPreview) {
    const newSelection = selectedPosts.filter(({ id }) => id !== post.id);

    if (newSelection.length === selectedPosts.length) {
      return;
    }

    changeSelectedPosts(newSelection);
  }

  async function removePosts() {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      const selectedIDs = selectedPosts.map(({ id }) => id);
      await fetchRemoveReleasePosts(release!.id, selectedIDs);

      const pagination = await fetchReleasePostsPagination(release!.id);

      if (BigInt(pagination.total_pages) === BIGINT_ZERO) {
        changePostsData(undefined);
        changeSelectedPosts([]);
        return;
      }

      const newPostsData = await fetchReleasePosts(
        release!.id,
        String(BIGINT_ONE),
      );

      changePostsData(newPostsData);
      changeSelectedPosts([]);
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
      <Heading isExternal level={2}>
        Release Posts
      </Heading>
      {!isReady ? (
        <LoadingBar />
      ) : !postsData ? (
        <Article>
          <ArticleHeader>No posts available for this release.</ArticleHeader>
        </Article>
      ) : (
        <CardListInternal
          pagination={postsData.pagination}
          onPageChange={async (page) => {
            const newPostsData = await fetchReleasePosts(release_id!, page);

            changePostsData(newPostsData);
          }}
        >
          {postsData.posts.map((post) => {
            const isSelected = selectedPosts.includes(post);

            return (
              <PostCard key={post.id} post={post}>
                <Button
                  onClick={() => {
                    !isSelected ? selectPost(post) : deselectPost(post);
                  }}
                >
                  {!isSelected ? "Select" : "Deselect"}
                </Button>
              </PostCard>
            );
          })}
        </CardListInternal>
      )}
      <Heading isExternal level={2}>
        Selected Posts
      </Heading>
      {!selectedPosts.length ? (
        <Article>
          <ArticleHeader>No posts were selected for removal.</ArticleHeader>
        </Article>
      ) : (
        <CardListLocal
          items={selectedPosts.map((post) => (
            <PostCard key={post.id} post={post}>
              <Button
                onClick={() => {
                  deselectPost(post);
                }}
              >
                Deselect
              </Button>
            </PostCard>
          ))}
        />
      )}
      <Article>
        <ArticleBody>
          <ListButtons buttonWidth={"10em"}>
            <ButtonNegative
              disabled={isLoading || isEmpty(selectedPosts)}
              onClick={async () => {
                await removePosts();
              }}
            >
              Remove Selected
            </ButtonNegative>
          </ListButtons>
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default RemoveReleasePosts;
