import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import {
  ArtistEditForm,
  ArtistURL,
  getArtist,
  NewArtistProfileURL,
} from "#entities/artist";
import { Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import { useLanguage } from "#hooks";

function ArtistEdit() {
  const router = useRouter();
  const { language } = useLanguage();
  const [artist, changeArtist] =
    useState<Awaited<ReturnType<typeof getArtist>>>();
  const { query, isReady } = router;
  const artist_id = resolveQueryValue(query.artist_id);
  const title = !artist
    ? "Edit artist"
    : `Edit artist "${artist.name ?? "Unknown"}" (${artist.id})`;
  const heading = !artist
    ? "Edit Artist"
    : `Edit Artist "${artist.name ?? "Unknown"}" (${artist.id})`;

  useEffect(() => {
    if (!isReady || !artist_id) {
      return;
    }

    (async () => {
      const newArtist = await getArtist(artist_id);
      changeArtist(newArtist);
    })();
  }, [isReady, artist_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!artist ? (
                <LoadingBar />
              ) : (
                <Link href={new ArtistURL(language, artist.id)}>Artist</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <Heading level={2}>Profiles</Heading>
          <ListButtons>
            {!artist ? (
              <LoadingBar />
            ) : (
              <LinkButton href={new NewArtistProfileURL(language, artist.id)}>
                Add
              </LinkButton>
            )}
          </ListButtons>
        </ArticleBody>

        <ArticleFooter>
          {!artist ? <LoadingBar /> : <ArtistEditForm artist={artist} />}
        </ArticleFooter>
      </Article>
    </Page>
  );
}

export default ArtistEdit;
