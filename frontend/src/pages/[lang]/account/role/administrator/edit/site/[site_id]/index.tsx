import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchSite } from "#api/sites";
import { fetchPublishSite } from "#api/account/administrator/publish";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { Item, List, ListButtons } from "#components/lists";
import { Link } from "#components/links";
import {
  EditSiteForm,
  SiteProfilesCreateURL,
  SiteReleasesCreateURL,
  SiteURL,
} from "#entities/site";
import { type IAsyncReturnType } from "#types";
import { Button } from "#components/buttons";

function EditSite() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const { isReady, query } = router;
  const site_id = resolveQueryValue(query.site_id);
  const title = !site ? "Edit site" : `Edit site "${site.title}" (${site.id})`;
  const heading = !site
    ? "Edit Site"
    : `Edit Site "${site.title}" (${site.id})`;

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);

      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteURL(language, site.id)}>Site</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <List>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteProfilesCreateURL(language, site.id)}>
                  Add profile
                </Link>
              )}
            </Item>

            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteReleasesCreateURL(language, site.id)}>
                  Add release
                </Link>
              )}
            </Item>
          </List>
        </ArticleBody>

        <ArticleFooter>
          {!site ? (
            <LoadingBar />
          ) : (
            <EditSiteForm id={`edit-site-${site.id}`} site={site} />
          )}
        </ArticleFooter>
      </Article>

      <Article>
        <ArticleHeader>
          <ListButtons>
            {!site ? (
              <LoadingBar />
            ) : (
              <Button
                disabled={!isAdmin || Boolean(site.public_id)}
                onClick={async () => {
                  const publishedSite = await fetchPublishSite(site.id);
                  changeSite(publishedSite);
                }}
              >
                Publish
              </Button>
            )}
          </ListButtons>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default EditSite;
