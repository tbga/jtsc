import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { AccountURL } from "#lib/urls";
import { fetchRelease } from "#api/releases";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import {
  EditReleaseForm,
  NewReleaseProfilesURL,
  ReleaseURL,
  RemoveReleaseProfilesURL,
} from "#entities/release";
import { type IAsyncReturnType } from "#types";

/**
 * @TODO create and remove URLs
 */
function EditRelease() {
  const router = useRouter();
  const { language } = useLanguage();
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const { isReady, query } = router;
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "Edit release"
    : `Edit release "${release.title ?? "Untitled"}" (${release.id})`;
  const heading = !release
    ? "Edit Release"
    : `Edit Release "${release.title ?? "Untitled"}" (${release.id})`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newRelease = await fetchRelease(release_id);
      changeRelease(newRelease);
    })();
  }, [isReady, release_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <Heading id="profiles" level={2}>
            Profiles
          </Heading>
          <ListButtons isSpread>
            {!release ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new RemoveReleaseProfilesURL(language, release.id)}
                >
                  Remove
                </LinkButton>
                <LinkButton
                  href={new NewReleaseProfilesURL(language, release.id)}
                >
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>
        </ArticleBody>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading id="edit" level={2}>
            Edit
          </Heading>
        </ArticleHeader>
        <ArticleBody>
          {!release ? (
            <LoadingBar />
          ) : (
            <EditReleaseForm
              id={`edit-release-${release.id}`}
              release={release}
              onEdit={async (newRelease) => {
                changeRelease(newRelease);
              }}
            />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default EditRelease;
