import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { fetchPost } from "#api/post";
import { Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import {
  PostArtistsCreateURL,
  PostArtistsRemoveURL,
  PostReleasesCreateURL,
  PostReleasesRemoveURL,
  PostURL,
} from "#entities/post";
import { type IAsyncReturnType } from "#types";

function EditPost() {
  const router = useRouter();
  const { language } = useLanguage();
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const { isReady, query } = router;
  const post_id = resolveQueryValue(query.post_id);
  const title = !post
    ? "Edit post"
    : `Edit post "${post.title ?? "Untitled"}" (${post.id})`;
  const heading = !post
    ? "Edit Post"
    : `Edit Post "${post.title ?? "Untitled"}" (${post.id})`;

  useEffect(() => {
    if (!isReady || !post_id) {
      return;
    }

    (async () => {
      const newPost = await fetchPost(post_id);
      changePost(newPost);
    })();
  }, [isReady, post_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!post ? (
                <LoadingBar />
              ) : (
                <Link href={new PostURL(language, post.id)}>Post</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>Releases</Heading>
        </ArticleHeader>
        <ArticleBody>
          {!post ? (
            <LoadingBar />
          ) : (
            <ListButtons isSpread>
              <LinkButton href={new PostReleasesRemoveURL(language, post.id)}>
                Remove
              </LinkButton>
              <LinkButton href={new PostReleasesCreateURL(language, post.id)}>
                Add
              </LinkButton>
            </ListButtons>
          )}
        </ArticleBody>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>Artists</Heading>
        </ArticleHeader>
        <ArticleBody>
          {!post ? (
            <LoadingBar />
          ) : (
            <ListButtons isSpread>
              <LinkButton href={new PostArtistsRemoveURL(language, post.id)}>
                Remove
              </LinkButton>
              <LinkButton href={new PostArtistsCreateURL(language, post.id)}>
                Add
              </LinkButton>
            </ListButtons>
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default EditPost;
