import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import {
  ProfileUpdateForm,
  getProfile,
  ProfileURL,
  RemoveProfileNamesURL,
  NewProfileNamesURL,
  IProfile,
  NewProfileArtistsURL,
  RemoveProfileArtistsURL,
  NewProfileReleasesURL,
} from "#entities/profile";
import { Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";

/**
 * @TODO releases
 */
function ProfileEdit() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profile, updateProfile] =
    useState<Awaited<ReturnType<typeof getProfile>>["profile"]>();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const title = !profile
    ? "Edit Profile"
    : `Edit Profile "${profile.name ?? "Unknown"}" (${profile.id})`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const newProfile = await getProfile(profile_id);
      updateProfile(newProfile.profile);
    })();
  }, [isReady, profile_id]);

  return (
    <Page title={title}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profile ? (
                <LoadingBar />
              ) : (
                <Link href={new ProfileURL(language, profile.id)}>Profile</Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          <Heading id="edit-profile-releases" level={2}>
            Releases
          </Heading>
          <ListButtons isSpread>
            {!profile ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new NewProfileReleasesURL(language, profile.id)}
                >
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>

          <Heading id="edit-profile-names" level={2}>
            Names
          </Heading>
          <ListButtons isSpread>
            {!profile ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new RemoveProfileNamesURL(language, profile.id)}
                >
                  Remove
                </LinkButton>
                <LinkButton href={new NewProfileNamesURL(language, profile.id)}>
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>

          <Heading id="edit-profile-artists" level={2}>
            Artists
          </Heading>
          <ListButtons isSpread>
            {!profile ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new RemoveProfileArtistsURL(language, profile.id)}
                >
                  Remove
                </LinkButton>
                <LinkButton
                  href={new NewProfileArtistsURL(language, profile.id)}
                >
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>
        </ArticleBody>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading id="edit-profile-values" level={2}>
            Edit
          </Heading>
        </ArticleHeader>

        <ArticleBody>
          {!profile ? (
            <LoadingBar />
          ) : (
            <ProfileUpdateForm id="profile-edit" profile={profile} />
          )}
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default ProfileEdit;
