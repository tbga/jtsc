import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPublicExports } from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { type IAsyncReturnType } from "#types";
import { PublicExportCard, PublicExportsURL } from "#entities/public-export";
import { fetchPublicImports } from "#api/account/administrator/public-imports";
import { PublicImportCard, PublicImportsURL } from "#entities/public-import";

function PublicImportsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicImportsData, changePublicImportsData] =
    useState<IAsyncReturnType<typeof fetchPublicImports>>();
  const { query, isReady } = router;
  const page = resolveQueryValue(query.page);
  const title = !publicImportsData
    ? "Public imports"
    : `Public imports ${publicImportsData.pagination.current_page} out of ${publicImportsData.pagination.total_pages}`;
  const heading = "Public Imports";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }
    (async () => {
      const newPublicExports = await fetchPublicImports(page);
      changePublicImportsData(newPublicExports);
    })();
  }, [isReady, page]);

  return (
    <Page title={title} heading={heading}>
      {!publicImportsData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={publicImportsData.pagination}
          urlBuilder={(page) => new PublicImportsURL(language, page as string)}
        >
          {publicImportsData.public_imports.map((publicImport) => (
            <PublicImportCard
              key={publicImport.id}
              publicImport={publicImport}
            />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default PublicImportsPage;
