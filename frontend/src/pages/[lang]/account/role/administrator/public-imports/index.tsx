import { DEFAULT_LANGUAGE } from "#environment/variables";
import { fetchPublicImportsPagination } from "#api/account/administrator/public-imports";
import { paginationRedirectMulti } from "#components";
import { PublicImportsURL } from "#entities/public-import";
import { EMPTY_OBJECT } from "#types";

const PublicImports = paginationRedirectMulti({
  queryResolver: (query) => EMPTY_OBJECT,
  prefix: "public imports",
  title: (query) => "Public Imports",
  paginationFetcher: fetchPublicImportsPagination,
  urlBuilder: (page) => new PublicImportsURL(DEFAULT_LANGUAGE, page),
});

export default PublicImports;
