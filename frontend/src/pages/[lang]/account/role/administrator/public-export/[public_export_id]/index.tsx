import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchPublicExport } from "#api/account/administrator/public-exports";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { PublicExportArticle } from "#entities/public-export";
import { type IAsyncReturnType } from "#types";

function PublicExportDetails() {
  const router = useRouter();
  const [publicExport, changePublicExport] =
    useState<IAsyncReturnType<typeof fetchPublicExport>>();
  const { query, isReady } = router;
  const public_export_id = resolveQueryValue(query.public_export_id);
  const title = !publicExport
    ? "Public export"
    : `Public export "${publicExport.title}" (${publicExport.id})`;
  const heading = "Public Export";

  useEffect(() => {
    if (!isReady || !public_export_id) {
      return;
    }

    (async () => {
      const newPublicExport = await fetchPublicExport(public_export_id);
      changePublicExport(newPublicExport);
    })();
  }, [isReady, public_export_id]);

  return (
    <Page title={title} heading={heading}>
      {!publicExport ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <PublicExportArticle publicExport={publicExport} />
      )}
    </Page>
  );
}

export default PublicExportDetails;
