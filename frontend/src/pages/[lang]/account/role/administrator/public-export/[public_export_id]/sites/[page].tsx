import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import {
  fetchPublicExport,
  fetchPublicExportSites,
} from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import { PublicExportSitesURL, PublicExportURL } from "#entities/public-export";
import { SiteCard } from "#entities/site";
import { type IAsyncReturnType } from "#types";

function PublicExportSitesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicExport, changePublicExport] =
    useState<IAsyncReturnType<typeof fetchPublicExport>>();
  const [sitesData, changeSitesData] =
    useState<IAsyncReturnType<typeof fetchPublicExportSites>>();
  const { isReady, query } = router;
  const publicExportID = resolveQueryValue(query.public_export_id);
  const page = resolveQueryValue(query.page);
  const title =
    !publicExport || !sitesData
      ? "Public export sites"
      : `Public export "${publicExport.title}" (${publicExport.id}) sites page ${sitesData.pagination.current_page} out of ${sitesData.pagination.total_pages}`;
  const heading = "Public Export Sites";

  useEffect(() => {
    if (!isReady || !publicExportID || !page) {
      return;
    }

    (async () => {
      const newPublicExport = await fetchPublicExport(publicExportID);

      changePublicExport(newPublicExport);

      const newSites = await fetchPublicExportSites(newPublicExport.id, page);

      changeSitesData(newSites);
    })();
  }, [isReady, page, publicExportID]);

  return (
    <Page title={title} heading={heading}>
      {!sitesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          pagination={sitesData.pagination}
          urlBuilder={(page) =>
            new PublicExportSitesURL(
              language,
              publicExportID as string,
              page as string,
            )
          }
        >
          {sitesData.sites.map((site) => (
            <SiteCard key={site.id} site={site} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!publicExport ? (
                <LoadingBar />
              ) : (
                <Link href={new PublicExportURL(language, publicExport.id)}>
                  Public export
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default PublicExportSitesPage;
