import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { type IEntityItem } from "#lib/entities";
import {
  fetchPublicExport,
  fetchPublicExportFinalize,
  fetchPublicExportFinish,
} from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Page,
} from "#components";
import { Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import { Button } from "#components/buttons";
import {
  PublicExportEditForm,
  PublicExportSitesAddURL,
  PublicExportSitesRemoveURL,
  PublicExportURL,
} from "#entities/public-export";
import { type IAsyncReturnType } from "#types";
import { OperationURL } from "#entities/operation";

function PublicExportEdit() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicExport, changePublicExport] =
    useState<IAsyncReturnType<typeof fetchPublicExport>>();
  const [operationItem, changeOperationItem] = useState<IEntityItem>();
  const { query, isReady } = router;
  const public_export_id = resolveQueryValue(query.public_export_id);
  const title = !publicExport
    ? "Edit public export"
    : `Edit public export "${publicExport.title}" (${publicExport.id})`;
  const heading = !publicExport
    ? "Edit Public Export"
    : `Edit Public Export "${publicExport.title}" (${publicExport.id})`;

  useEffect(() => {
    if (!isReady || !public_export_id) {
      return;
    }

    (async () => {
      const newPublicExport = await fetchPublicExport(public_export_id);
      changePublicExport(newPublicExport);
    })();
  }, [isReady, public_export_id]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!publicExport ? (
                <LoadingBar />
              ) : (
                <Link href={new PublicExportURL(language, publicExport.id)}>
                  Public export
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>

        <ArticleBody>
          {!publicExport ? (
            <LoadingBar />
          ) : (
            publicExport.status === "in-progress" && (
              <PublicExportEditForm
                key={publicExport.id}
                id={`edit-public-export-${publicExport.id}`}
                publicExport={publicExport}
                onUpdate={async (update) => {
                  changePublicExport(update);
                }}
              />
            )
          )}
        </ArticleBody>

        <ArticleFooter>
          <ListButtons isSpread>
            {!publicExport ? (
              <LoadingBar />
            ) : (
              <>
                <Button
                  disabled={publicExport.status !== "in-progress"}
                  onClick={async () => {
                    const updatedPublicExport = await fetchPublicExportFinalize(
                      publicExport.id,
                    );

                    changePublicExport(updatedPublicExport);
                  }}
                >
                  Finalize
                </Button>

                <Button
                  disabled={publicExport.status !== "finalized"}
                  onClick={async () => {
                    const operationItem = await fetchPublicExportFinish(
                      publicExport.id,
                    );

                    changeOperationItem(operationItem);
                  }}
                >
                  Finish
                </Button>
              </>
            )}
          </ListButtons>
          {operationItem && (
            <p>
              Public export operation{" "}
              <Link href={new OperationURL(language, operationItem.id)}>
                &quot;{operationItem.name ?? "Unknown"}
                &quot; ({operationItem.id})
              </Link>{" "}
              added to queue.
            </p>
          )}
        </ArticleFooter>
      </Article>

      <Article>
        <ArticleHeader>
          <Heading level={2}>Sites</Heading>
        </ArticleHeader>
        <ArticleBody>
          <ListButtons isSpread>
            {!publicExport ? (
              <LoadingBar />
            ) : (
              publicExport.status === "in-progress" && (
                <>
                  <LinkButton
                    href={
                      new PublicExportSitesRemoveURL(language, publicExport.id)
                    }
                  >
                    Remove
                  </LinkButton>
                  <LinkButton
                    href={
                      new PublicExportSitesAddURL(language, publicExport.id)
                    }
                  >
                    Add
                  </LinkButton>
                </>
              )
            )}
          </ListButtons>
        </ArticleBody>
      </Article>
    </Page>
  );
}

export default PublicExportEdit;
