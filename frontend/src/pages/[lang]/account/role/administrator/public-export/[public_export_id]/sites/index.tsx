import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchPublicExportSitesPagination } from "#api/account/administrator/public-exports";
import { paginationRedirectMulti } from "#components";
import { PublicExportSitesURL } from "#entities/public-export";
import { type IBigSerialInteger } from "#types";

const PublicExportSites = paginationRedirectMulti({
  queryResolver: (query) => {
    return { public_export_id: resolveQueryValue(query.public_export_id) };
  },
  prefix: "public export sites",
  title: (query) => "Public Export Sites",
  paginationFetcher: async ({ public_export_id }) => {
    return fetchPublicExportSitesPagination(
      public_export_id as IBigSerialInteger,
    );
  },
  urlBuilder: (page, { public_export_id }) =>
    new PublicExportSitesURL(
      DEFAULT_LANGUAGE,
      public_export_id as IBigSerialInteger,
      page as IBigSerialInteger,
    ),
});

export default PublicExportSites;
