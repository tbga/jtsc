import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useLanguage } from "#hooks";
import { IAsyncReturnType } from "#types";
import { fetchPublicExport } from "#api/account/administrator/public-exports";
import { resolveQueryValue } from "#lib/util";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  PublicExportSitesRemoveForm,
  PublicExportURL,
} from "#entities/public-export";

function PublicExportSitesRemove() {
  const router = useRouter();
  const { language } = useLanguage();
  const [publicExport, changePublicExport] =
    useState<IAsyncReturnType<typeof fetchPublicExport>>();
  const { isReady, query } = router;
  const publicExportID = resolveQueryValue(query.public_export_id);
  const title = "Remove sites from public export";
  const heading = "Remove Sites from Public Export";

  useEffect(() => {
    if (!isReady || !publicExportID) {
      return;
    }

    (async () => {
      const newPublicExport = await fetchPublicExport(publicExportID);
      changePublicExport(newPublicExport);
    })();
  }, [isReady, publicExportID]);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!publicExport ? (
                <LoadingBar />
              ) : (
                <Link href={new PublicExportURL(language, publicExport.id)}>
                  Public export
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>

      <Article>
        <ArticleHeader>
          {!publicExport ? (
            <LoadingBar />
          ) : (
            publicExport.status === "in-progress" && (
              <PublicExportSitesRemoveForm
                key={publicExport.id}
                id={`remove-public-export-${publicExport.id}-sites`}
                public_export_id={publicExport.id}
              />
            )
          )}
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default PublicExportSitesRemove;
