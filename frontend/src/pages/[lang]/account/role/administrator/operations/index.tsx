import { useEffect, useState } from "react";
import { fetchOperationsStats } from "#api/account/administrator/operations";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { DL, DS } from "#components/lists";
import { Link } from "#components/links";
import { OperationsAllURL, Status } from "#entities/operation";
import { type IAsyncReturnType } from "#types";

interface ILink {
  title: string;
  key: keyof Omit<IAsyncReturnType<typeof fetchOperationsStats>, "all">;
}

const links: readonly ILink[] = [
  { key: "pending", title: "Pending" },
  { key: "in_progress", title: "In-progress" },
  { key: "finished", title: "Finished" },
  { key: "failed", title: "Failed" },
] as const;

function OperationsInfo() {
  const { language } = useLanguage();
  const [stats, changeStats] =
    useState<IAsyncReturnType<typeof fetchOperationsStats>>();
  const title = "Operation stats";
  const heading = "Operation Stats";

  useEffect(() => {
    (async () => {
      const newStats = await fetchOperationsStats();
      changeStats(newStats);
    })();
  }, []);

  return (
    <Page title={title} heading={heading}>
      <Article>
        <ArticleHeader>
          <DL>
            <DS
              isHorizontal
              dKey="All"
              dValue={
                !stats ? (
                  <LoadingBar />
                ) : (
                  <Link href={new OperationsAllURL(language)}>{stats.all}</Link>
                )
              }
            />
            {links.map(({ key, title }) => (
              <DS
                isHorizontal
                key={key}
                dKey={
                  <Status status={key === "in_progress" ? "in-progress" : key}>
                    {title}
                  </Status>
                }
                dValue={
                  !stats ? (
                    <LoadingBar />
                  ) : (
                    <Status
                      status={key === "in_progress" ? "in-progress" : key}
                    >
                      {stats[key]}
                    </Status>
                  )
                }
              />
            ))}
          </DL>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default OperationsInfo;
