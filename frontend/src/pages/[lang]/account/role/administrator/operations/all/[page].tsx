import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchOperations } from "#api/account/administrator/operations";
import { useLanguage } from "#hooks";
import { LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { OperationCard, OperationsAllURL } from "#entities/operation";

function OperationsPage() {
  const { language } = useLanguage();
  const router = useRouter();
  const [operationsInfo, changeOperationsInfo] =
    useState<Awaited<ReturnType<typeof fetchOperations>>>();
  const { query, isReady } = router;
  const page = resolveQueryValue(query.page);
  const title = "Operations Page";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }

    (async () => {
      const newOperationsInfo = await fetchOperations(page);
      changeOperationsInfo(newOperationsInfo);
    })();
  }, [isReady, page]);

  return (
    <Page title={title}>
      {!operationsInfo ? (
        <LoadingBar />
      ) : (
        <CardList
          isDescending
          pagination={operationsInfo.pagination}
          urlBuilder={(page) => new OperationsAllURL(language, page as string)}
        >
          {operationsInfo.operations.map((operation) => (
            <OperationCard key={operation.id} operation={operation} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default OperationsPage;
