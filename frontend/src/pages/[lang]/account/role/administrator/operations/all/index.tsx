import { DEFAULT_LANGUAGE } from "#environment/variables";
import { fetchOperationsPagination } from "#api/account/administrator/operations";
import { paginationRedirect } from "#components";
import { OperationsAllURL } from "#entities/operation";

const OperationsAll = paginationRedirect({
  prefix: "operations",
  title: "Operations",
  paginationFetcher: fetchOperationsPagination,
  urlBuilder: (page) => new OperationsAllURL(DEFAULT_LANGUAGE, page as string),
});

export default OperationsAll;
