import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease, fetchReleasePosts } from "#api/releases";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import {
  NewReleasePostsURL,
  ReleasePostsURL,
  ReleaseURL,
  RemoveReleasePostsURL,
} from "#entities/release";
import { PostCard } from "#entities/post";

function ReleasePostsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [release, changeRelease] =
    useState<Awaited<ReturnType<typeof fetchRelease>>>();
  const [postData, changePostData] =
    useState<Awaited<ReturnType<typeof fetchReleasePosts>>>();
  const { query, isReady } = router;
  const release_id = resolveQueryValue(query.release_id);
  const page = resolveQueryValue(query.page);
  const title =
    !release || !postData
      ? "Release posts"
      : `Posts of release "${release.title ?? "Untitled"}" (${
          release.id
        }) page ${postData.pagination.current_page} out of ${
          postData.pagination.total_pages
        }`;
  const heading = !release
    ? "Release Posts"
    : `Posts of Release "${release.title ?? "Untitled"}" (${release.id})`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newRelease = await fetchRelease(release_id);
      changeRelease(newRelease);
    })();
  }, [isReady, release_id]);

  useEffect(() => {
    if (!isReady || !release_id || !page) {
      return;
    }

    (async () => {
      const newPostData = await fetchReleasePosts(release_id, page);
      changePostData(newPostData);
    })();
  }, [isReady, release_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!release || !postData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          pagination={postData.pagination}
          urlBuilder={(page) =>
            new ReleasePostsURL(language, release.id, String(page))
          }
        >
          {postData.posts.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
          <ListButtons>
            {!release ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new RemoveReleasePostsURL(language, release.id)}
                >
                  Remove
                </LinkButton>
                <LinkButton href={new NewReleasePostsURL(language, release.id)}>
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ReleasePostsPage;
