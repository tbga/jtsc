import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchReleasePostsPagination } from "#api/releases";
import { paginationRedirectMulti } from "#components";
import { NewReleasePostsURL, ReleasePostsURL } from "#entities/release";

const ReleasePosts = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      release_id: resolveQueryValue(query.release_id),
    };
  },
  prefix: "release posts",
  title: (query, isReady) =>
    !isReady ? "Release Posts" : `Release "${query.release_id}" Posts`,
  paginationFetcher: async ({ release_id }) => {
    return fetchReleasePostsPagination(release_id!);
  },
  urlBuilder: (page, { release_id }) => {
    return new ReleasePostsURL(DEFAULT_LANGUAGE, release_id!, page);
  },
  newLink: ({ release_id }) => {
    return new NewReleasePostsURL(DEFAULT_LANGUAGE, release_id!);
  },
});

export default ReleasePosts;
