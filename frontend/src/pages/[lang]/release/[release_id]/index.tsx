import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease } from "#api/releases";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { ReleaseArticle } from "#entities/release";

function ReleaseDetails() {
  const router = useRouter();
  const [release, changeRelease] =
    useState<Awaited<ReturnType<typeof fetchRelease>>>();
  const { query, isReady } = router;
  const release_id = resolveQueryValue(query.release_id);
  const title = !release
    ? "Release Details"
    : `Release "${release.title ?? "Unknown"}" (${release.id})`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newRelease = await fetchRelease(release_id);
      changeRelease(newRelease);
    })();
  }, [isReady, release_id]);

  return (
    <Page title={title} heading={null}>
      {!release ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>

          <ArticleBody>
            <LoadingBar />
          </ArticleBody>

          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <ReleaseArticle release={release} headingLevel={1} />
      )}
    </Page>
  );
}

export default ReleaseDetails;
