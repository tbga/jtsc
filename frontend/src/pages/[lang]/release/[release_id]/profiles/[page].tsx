import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchRelease, fetchReleaseProfiles } from "#api/releases";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import {
  NewReleaseProfilesURL,
  ReleaseProfilesURL,
  ReleaseURL,
  RemoveReleaseProfilesURL,
} from "#entities/release";
import { ProfileCard } from "#entities/profile";

function ReleasePostsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [release, changeRelease] =
    useState<Awaited<ReturnType<typeof fetchRelease>>>();
  const [profileData, changeProfileData] =
    useState<Awaited<ReturnType<typeof fetchReleaseProfiles>>>();
  const { query, isReady } = router;
  const release_id = resolveQueryValue(query.release_id);
  const page = resolveQueryValue(query.page);
  const title =
    !release || !profileData
      ? "Release profiles"
      : `Profiles of release "${release.title ?? "Untitled"}" (${
          release.id
        }) page ${profileData.pagination.current_page} out of ${
          profileData.pagination.total_pages
        }`;
  const heading = !release
    ? "Release Profiles"
    : `Profiles of Release "${release.title ?? "Untitled"}" (${release.id})`;

  useEffect(() => {
    if (!isReady || !release_id) {
      return;
    }

    (async () => {
      const newRelease = await fetchRelease(release_id);
      changeRelease(newRelease);
    })();
  }, [isReady, release_id]);

  useEffect(() => {
    if (!isReady || !release_id || !page) {
      return;
    }

    (async () => {
      const newPostData = await fetchReleaseProfiles(release_id, page);
      changeProfileData(newPostData);
    })();
  }, [isReady, release_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!release || !profileData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          pagination={profileData.pagination}
          urlBuilder={(page) =>
            new ReleaseProfilesURL(language, release.id, String(page))
          }
        >
          {profileData.profiles.map((profile) => (
            <ProfileCard key={profile.id} profile={profile} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!release ? (
                <LoadingBar />
              ) : (
                <Link href={new ReleaseURL(language, release.id)}>Release</Link>
              )}
            </Item>
          </List>
          <ListButtons>
            {!release ? (
              <LoadingBar />
            ) : (
              <>
                <LinkButton
                  href={new RemoveReleaseProfilesURL(language, release.id)}
                >
                  Remove
                </LinkButton>
                <LinkButton
                  href={new NewReleaseProfilesURL(language, release.id)}
                >
                  Add
                </LinkButton>
              </>
            )}
          </ListButtons>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ReleasePostsPage;
