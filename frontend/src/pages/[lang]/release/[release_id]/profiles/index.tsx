import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchReleaseProfilesPagination } from "#api/releases";
import { paginationRedirectMulti } from "#components";
import { NewReleaseProfilesURL, ReleaseProfilesURL } from "#entities/release";

const ReleasePosts = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      release_id: resolveQueryValue(query.release_id),
    };
  },
  prefix: "release profiles",
  title: (query, isReady) =>
    !isReady ? "Release Profiles" : `Release "${query.release_id}" Profiles`,
  paginationFetcher: async ({ release_id }) => {
    return fetchReleaseProfilesPagination(release_id!);
  },
  urlBuilder: (page, { release_id }) => {
    return new ReleaseProfilesURL(DEFAULT_LANGUAGE, release_id!, page);
  },
  newLink: ({ release_id }) => {
    return new NewReleaseProfilesURL(DEFAULT_LANGUAGE, release_id!);
  },
});

export default ReleasePosts;
