import { DEFAULT_LANGUAGE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { fetchArtistsPagination } from "#api/artists";
import { paginationRedirect } from "#components";

const Artists = paginationRedirect({
  prefix: "artists",
  title: "Artists",
  paginationFetcher: fetchArtistsPagination,
  urlBuilder: (page) =>
    new ProjectURL({
      language: DEFAULT_LANGUAGE,
      pathname: `/artists/${page}`,
    }),
});

export default Artists;
