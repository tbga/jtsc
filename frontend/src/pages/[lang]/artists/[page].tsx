import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { ProjectURL } from "#lib/urls";
import { resolveQueryValue } from "#lib/util";
import { fetchArtists } from "#api/artists";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { ArtistCard } from "#entities/artist";

function ArtistsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { query, isReady } = router;
  const [artistData, changeArtistsData] =
    useState<Awaited<ReturnType<typeof fetchArtists>>>();
  const currentPage = resolveQueryValue(query.page);
  const title = !artistData
    ? "Artists"
    : `Artists page ${artistData.pagination.current_page} out of ${artistData.pagination.total_pages}`;
  const heading = "Artists";

  useEffect(() => {
    if (!isReady || !currentPage) {
      return;
    }
    (async () => {
      const newArtistData = await fetchArtists(currentPage);
      changeArtistsData(newArtistData);
    })();
  }, [isReady, currentPage]);

  return (
    <Page title={title} heading={heading}>
      {!artistData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={artistData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({ language, pathname: `/artists/${page}` })
          }
        >
          {artistData.artists.map((artist) => (
            <ArtistCard key={artist.id} artist={artist} headingLevel={2} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default ArtistsPage;
