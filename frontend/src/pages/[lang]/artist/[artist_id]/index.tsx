import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { LoadingBar, Page } from "#components";
import {
  type IArtist,
  getArtist,
  ArtistArticle,
  artistName,
} from "#entities/artist";

function ArtistPage() {
  const router = useRouter();
  const { query, isReady } = router;
  const [artist, changeArtist] = useState<IArtist>();
  const artist_id = resolveQueryValue(query.artist_id);
  const title = !artist
    ? "Artist"
    : `Artist "${artistName(artist)}" (${artist.id})`;

  useEffect(() => {
    if (!isReady || !artist_id) {
      return;
    }

    (async () => {
      const profile = await getArtist(artist_id);
      changeArtist(profile);
    })();
  }, [isReady, artist_id]);

  return (
    <Page title={title} heading={null}>
      {!artist ? (
        <LoadingBar />
      ) : (
        <ArtistArticle headingLevel={1} artist={artist} />
      )}
    </Page>
  );
}

export default ArtistPage;
