import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { fetchArtist } from "#api/artists";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { LinkInternal } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import {
  ArtistURL,
  NewArtistProfileURL,
  getArtistProfiles,
} from "#entities/artist";
import { ProfileCard } from "#entities/profile";
import { type IAsyncReturnType } from "#types";

function ArtistProfilesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { query, isReady } = router;
  const [artist, changeArtist] =
    useState<IAsyncReturnType<typeof fetchArtist>>();
  const [profilesData, changeArtistProfilesData] =
    useState<Awaited<ReturnType<typeof getArtistProfiles>>>();
  const artist_id = resolveQueryValue(query.artist_id);
  const page = resolveQueryValue(query.page);
  // profiles data is derivative of `artist`
  const title =
    !profilesData || !artist
      ? "Artist profiles"
      : `Artist "${artist.name ?? "Unknown"}" (${artist.id}) profiles page ${
          profilesData.pagination.current_page
        } out of ${profilesData.pagination.total_pages}`;
  const heading = !artist
    ? "Artist Profiles"
    : `Artist "${artist.name ?? "Unknown"}" (${artist.id}) Profiles`;

  useEffect(() => {
    if (!isReady || !artist_id || !page) {
      return;
    }

    (async () => {
      const isSameArtist = Boolean(artist && artist_id === artist.id);
      const newArtist = isSameArtist ? artist! : await fetchArtist(artist_id);

      if (!isSameArtist) {
        changeArtist(newArtist);
      }

      const newArtistsProfileData = await getArtistProfiles(newArtist.id, page);
      changeArtistProfilesData(newArtistsProfileData);
    })();
  }, [isReady, artist_id, page, artist]);

  return (
    <Page title={title} heading={heading}>
      {!profilesData ? (
        <LoadingBar />
      ) : (
        <CardList
          isDescending
          pagination={profilesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({
              language,
              pathname: `/artist/${artist_id}/profiles/${page}`,
            })
          }
        >
          {profilesData.profiles.map((artistProfile) => (
            <ProfileCard
              key={artistProfile.id}
              profile={artistProfile}
              headingLevel={2}
            />
          ))}
        </CardList>
      )}
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!artist ? (
                <LoadingBar />
              ) : (
                <LinkInternal href={new ArtistURL(language, artist.id)}>
                  Artist
                </LinkInternal>
              )}
            </Item>
            <Item>
              {!artist ? (
                <LoadingBar />
              ) : (
                <LinkInternal
                  href={new NewArtistProfileURL(language, artist.id)}
                  target="_blank"
                >
                  Add profile
                </LinkInternal>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ArtistProfilesPage;
