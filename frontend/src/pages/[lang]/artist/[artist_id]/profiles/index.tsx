import { useEffect } from "react";
import { useRouter } from "next/router";
import { ProjectURL } from "#lib/urls";
import { resolveQueryValue } from "#lib/util";
import { fetchArtistProfilesPagination } from "#api/artists";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, Page } from "#components";

function ArtistProfiles() {
  const router = useRouter();
  const { language } = useLanguage();
  const { query, isReady } = router;
  const artist_id = resolveQueryValue(query.artist_id);

  useEffect(() => {
    if (!isReady || !artist_id) {
      return;
    }

    (async () => {
      const pagination = await fetchArtistProfilesPagination(artist_id);
      router.push(
        new ProjectURL({
          language,
          pathname: `/artist/${artist_id}/profiles/${pagination.current_page}`,
        }),
      );
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isReady, artist_id]);

  return (
    <Page title="Artist profiles">
      <Article>
        <ArticleHeader>
          <p>Redirecting to last artist profiles page...</p>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ArtistProfiles;
