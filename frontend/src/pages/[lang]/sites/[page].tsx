import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { ProjectURL } from "#lib/urls";
import { fetchSites } from "#api/sites";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { LinkInternal } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import { SiteCard, SiteCreateURL } from "#entities/site";

function SitesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const { query, isReady } = router;
  const [sitesData, changeSitesData] =
    useState<Awaited<ReturnType<typeof fetchSites>>>();
  const { page } = query;
  const currentPage =
    page !== undefined ? (Array.isArray(page) ? page[0] : page) : undefined;

  useEffect(() => {
    if (!isReady || !currentPage) {
      return;
    }

    (async () => {
      const newSites = await fetchSites(currentPage);
      changeSitesData(newSites);
    })();
  }, [isReady, currentPage]);

  return (
    <Page title="Sites">
      {!sitesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={sitesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({ language, pathname: `/sites/${page}` })
          }
        >
          {sitesData.sites.map((site) => (
            <SiteCard key={site.id} site={site} />
          ))}
        </CardList>
      )}

      {isRegistered && (
        <Article>
          <ArticleHeader>
            <List>
              <Item>
                <LinkInternal href={new SiteCreateURL(language)}>
                  Add a site
                </LinkInternal>
              </Item>
            </List>
          </ArticleHeader>
        </Article>
      )}
    </Page>
  );
}

export default SitesPage;
