import { DEFAULT_LANGUAGE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { fetchSitesPagination } from "#api/sites";
import { paginationRedirect } from "#components";
import { SiteCreateURL } from "#entities/site";

const Sites = paginationRedirect({
  prefix: "sites",
  title: "Sites",
  paginationFetcher: fetchSitesPagination,
  urlBuilder: (page) =>
    new ProjectURL({ language: DEFAULT_LANGUAGE, pathname: `/sites/${page}` }),
  newLink: String(new SiteCreateURL(DEFAULT_LANGUAGE)),
});

export default Sites;
