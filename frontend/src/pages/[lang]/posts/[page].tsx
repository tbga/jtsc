import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { fetchPosts } from "#api/posts";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList } from "#components/lists";
import { PostCard } from "#entities/post";

function PostsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { query, isReady } = router;
  const [postsData, changePostsData] =
    useState<Awaited<ReturnType<typeof fetchPosts>>>();
  const page = resolveQueryValue(query.page);
  const title = !postsData
    ? "Posts"
    : `Posts page ${postsData.pagination.current_page} out of ${postsData.pagination.total_pages}`;
  const heading = "Posts";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }

    (async () => {
      const newPostsData = await fetchPosts(page);
      changePostsData(newPostsData);
    })();
  }, [isReady, page]);

  return (
    <Page title={title} heading={heading}>
      {!postsData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={postsData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({ language, pathname: `/posts/${page}` })
          }
        >
          {postsData.posts.map((post) => (
            <PostCard key={post.id} headingLevel={2} post={post} />
          ))}
        </CardList>
      )}
    </Page>
  );
}

export default PostsPage;
