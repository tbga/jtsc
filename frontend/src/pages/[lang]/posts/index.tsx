import { DEFAULT_LANGUAGE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { fetchPostsPagination } from "#api/posts";
import { paginationRedirect } from "#components";

const Posts = paginationRedirect({
  prefix: "posts",
  title: "Posts",
  paginationFetcher: fetchPostsPagination,
  urlBuilder: (page) =>
    new ProjectURL({ language: DEFAULT_LANGUAGE, pathname: `/posts/${page}` }),
});

export default Posts;
