import { useEffect, useState } from "react";
import { fetchStats } from "#api/stats";
import { Page } from "#components";
import { StatsOverview } from "#components/stats";
import { type IAsyncReturnType } from "#types";

function BasicStats() {
  const [stats, changeStats] = useState<IAsyncReturnType<typeof fetchStats>>();
  const title = "Stats";
  const heading = "Stats";

  useEffect(() => {
    (async () => {
      const newStats = await fetchStats();
      changeStats(newStats);
    })();
  }, []);

  return (
    <Page title={title} heading={heading}>
      <StatsOverview stats={stats} />
    </Page>
  );
}

export default BasicStats;
