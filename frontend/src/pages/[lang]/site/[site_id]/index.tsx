import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchSite } from "#api/sites";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { SiteArticle, type ISite } from "#entities/site";

function Site() {
  const router = useRouter();
  const [site, changeSite] = useState<ISite>();
  const { query, isReady } = router;
  const site_id = resolveQueryValue(query.site_id);
  const title = !site ? "Site" : `Site "${site.title}" (${site.id})`;

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  return (
    <Page title={title} heading={null}>
      {!site ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <SiteArticle key={site.id} site={site} headingLevel={1} />
      )}
    </Page>
  );
}

export default Site;
