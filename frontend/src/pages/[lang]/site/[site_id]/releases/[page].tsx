import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { fetchSite, fetchSiteReleases } from "#api/sites";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { Link } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import { ReleaseCard } from "#entities/release";
import { SiteReleasesCreateURL, SiteURL } from "#entities/site";

/**
 * @TODO a single `useEffect()`
 */
function SiteReleasesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [site, changeSite] = useState<Awaited<ReturnType<typeof fetchSite>>>();
  const [releasesData, changeReleasesData] =
    useState<Awaited<ReturnType<typeof fetchSiteReleases>>>();
  const { query, isReady } = router;
  const site_id = resolveQueryValue(query.site_id);
  const page = resolveQueryValue(query.page);
  const title = !site
    ? "Site releases"
    : !releasesData
      ? `Site "${site.title}" releases`
      : `Site "${site.title}" releases page ${releasesData.pagination.current_page} out of ${releasesData.pagination.total_pages}`;
  const heading = !site
    ? "Site Releases"
    : `Site "${site.title}" (${site.id}) Releases`;

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  useEffect(() => {
    if (!isReady || !site_id || !page) {
      return;
    }

    (async () => {
      const newSiteProfilesData = await fetchSiteReleases(site_id, page);
      changeReleasesData(newSiteProfilesData);
    })();
  }, [isReady, site_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!releasesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={releasesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({
              language,
              pathname: `/site/${site_id}/releases/${page}`,
            })
          }
        >
          {releasesData.releases.map((release) => (
            <ReleaseCard key={release.id} release={release} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteURL(language, site.id)}>Site</Link>
              )}
            </Item>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteReleasesCreateURL(language, site.id)}>
                  Add release
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SiteReleasesPage;
