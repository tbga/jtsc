import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { paginationRedirectMulti } from "#components";
import { fetchSiteReleasesPagination } from "#api/sites";
import { SiteReleasesCreateURL, SiteReleasesURL } from "#entities/site";

const SiteReleases = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      site_id: resolveQueryValue(query.site_id),
    };
  },
  prefix: "site releases",
  title: (query) => "Site Releases",
  paginationFetcher: async (query) =>
    fetchSiteReleasesPagination(query.site_id!),
  urlBuilder: (page, query) =>
    new SiteReleasesURL(DEFAULT_LANGUAGE, query.site_id!, page),
  newLink: (query) =>
    new SiteReleasesCreateURL(DEFAULT_LANGUAGE, query.site_id!),
});

export default SiteReleases;
