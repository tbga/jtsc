import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchSiteProfilesPagination } from "#api/sites";
import { paginationRedirectMulti } from "#components";
import { SiteProfilesCreateURL, SiteProfilesURL } from "#entities/site";

const SiteProfiles = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      site_id: resolveQueryValue(query.site_id),
    };
  },
  prefix: "site profiles",
  title: (query) => "Site Profiles",
  paginationFetcher: async (query) =>
    fetchSiteProfilesPagination(query.site_id!),
  urlBuilder: (page, query) =>
    new SiteProfilesURL(DEFAULT_LANGUAGE, query.site_id!, page),
  newLink: (query) =>
    new SiteProfilesCreateURL(DEFAULT_LANGUAGE, query.site_id!),
});

export default SiteProfiles;
