import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { fetchSiteProfiles, fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { Link } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import { ProfileCard } from "#entities/profile";
import { SiteProfilesCreateURL, SiteURL } from "#entities/site";

function SiteProfilesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [site, changeSite] = useState<Awaited<ReturnType<typeof fetchSite>>>();
  const [siteProfilesData, changeSiteProfilesData] =
    useState<Awaited<ReturnType<typeof fetchSiteProfiles>>>();
  const { query, isReady } = router;
  const site_id = resolveQueryValue(query.site_id);
  const profiles_page = resolveQueryValue(query.profiles_page);
  const title = !site
    ? "Site profiles"
    : !siteProfilesData
      ? `Site "${site.title}" (${site.id}) profiles`
      : `Site "${site.title}" (${site.id}) profiles page ${siteProfilesData.pagination.current_page} out of ${siteProfilesData.pagination.total_pages}`;
  const heading = !site
    ? "Site Profiles"
    : `Site "${site.title}" (${site.id}) Profiles`;

  useEffect(() => {
    if (!isReady || !site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [isReady, site_id]);

  useEffect(() => {
    if (!isReady || !site_id || !profiles_page) {
      return;
    }

    (async () => {
      const newSiteProfilesData = await fetchSiteProfiles(
        site_id,
        profiles_page,
      );
      changeSiteProfilesData(newSiteProfilesData);
    })();
  }, [isReady, site_id, profiles_page]);

  return (
    <Page title={title} heading={heading}>
      {!siteProfilesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={siteProfilesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({
              language,
              pathname: `/site/${site_id}/profiles/${page}`,
            })
          }
        >
          {siteProfilesData.profiles.map((profile) => (
            <ProfileCard key={profile.id} profile={profile} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteURL(language, site.id)}>Site</Link>
              )}
            </Item>
            <Item>
              {!site ? (
                <LoadingBar />
              ) : (
                <Link href={new SiteProfilesCreateURL(language, site.id)}>
                  Create profiles
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default SiteProfilesPage;
