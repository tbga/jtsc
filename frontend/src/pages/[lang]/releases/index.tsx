import { DEFAULT_LANGUAGE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { fetchReleasesPagination } from "#api/releases";
import { paginationRedirect } from "#components";
import { ReleaseCreateURL } from "#entities/release";

const Releases = paginationRedirect({
  prefix: "releases",
  title: "Releases",
  paginationFetcher: fetchReleasesPagination,
  urlBuilder: (page) =>
    new ProjectURL({
      language: DEFAULT_LANGUAGE,
      pathname: `/releases/${page}`,
    }),
  newLink: String(new ReleaseCreateURL(DEFAULT_LANGUAGE)),
});

export default Releases;
