import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { fetchReleases } from "#api/releases";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { Link } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import { ReleaseCard } from "#entities/release";

function ReleasesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { query, isReady } = router;
  const [releasesData, changeReleasesData] =
    useState<Awaited<ReturnType<typeof fetchReleases>>>();
  const page = resolveQueryValue(query.page);
  const title = !releasesData
    ? "Releases"
    : `Releases page ${releasesData.pagination.current_page} out of ${releasesData.pagination.total_pages}`;
  const heading = "Releases";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }

    (async () => {
      const newReleasesData = await fetchReleases(page);
      changeReleasesData(newReleasesData);
    })();
  }, [isReady, page]);

  return (
    <Page title={title} heading={heading}>
      {!releasesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={releasesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({ language, pathname: `/releases/${page}` })
          }
        >
          {releasesData.releases.map((release) => (
            <ReleaseCard key={release.id} release={release} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              <Link
                href={
                  new ProjectURL({ language, pathname: "/create/releases" })
                }
              >
                Create
              </Link>
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ReleasesPage;
