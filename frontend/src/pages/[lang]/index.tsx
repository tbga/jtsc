import { TITLE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { Article, ArticleHeader } from "#components";
import { pageComponent } from "#components/pages";
import { Link } from "#components/links";
import { Item, List } from "#components/lists";

const Home = pageComponent(
  { heading: () => `Welcome to ${TITLE}`, title: () => `Welcome to ${TITLE}` },
  ({ query, language }) => {
    return (
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              <Link href={new ProjectURL({ language, pathname: "/stats" })}>
                Stats
              </Link>
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    );
  },
);

export default Home;
