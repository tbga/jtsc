import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { ProjectURL } from "#lib/urls";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { LinkInternal } from "#components/links";
import { CardList, Item, List } from "#components/lists";
import { getProfiles, ProfileCard, ProfileCreateURL } from "#entities/profile";

function ProfilesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const [profilesData, changeProfilesData] =
    useState<Awaited<ReturnType<typeof getProfiles>>>();
  const { query, isReady } = router;
  const page = resolveQueryValue(query.page);
  const title = !profilesData
    ? "Profiles"
    : `Profiles page ${profilesData.pagination.current_page} out of ${profilesData.pagination.total_pages}`;
  const heading = "Profiles";

  useEffect(() => {
    if (!isReady || !page) {
      return;
    }
    (async () => {
      const newProfilesData = await getProfiles(page);
      changeProfilesData(newProfilesData);
    })();
  }, [isReady, page]);

  return (
    <Page title={title} heading={heading}>
      {!profilesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={profilesData.pagination}
          urlBuilder={(page) =>
            new ProjectURL({ language, pathname: `/profiles/${page}` })
          }
        >
          {profilesData.profiles.map((profile) => (
            <ProfileCard key={profile.id} profile={profile} />
          ))}
        </CardList>
      )}

      {isRegistered && (
        <Article>
          <ArticleHeader>
            <List>
              <Item>
                <LinkInternal
                  href={new ProfileCreateURL(language)}
                  target="_blank"
                >
                  Create new profile
                </LinkInternal>
              </Item>
            </List>
          </ArticleHeader>
        </Article>
      )}
    </Page>
  );
}

export default ProfilesPage;
