import { DEFAULT_LANGUAGE } from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { fetchProfilesPagination } from "#api/profiles";
import { paginationRedirect } from "#components";
import { ProfileCreateURL } from "#entities/profile";

const Profiles = paginationRedirect({
  prefix: "profiles",
  title: "Profiles",
  paginationFetcher: fetchProfilesPagination,
  urlBuilder: (page) =>
    new ProjectURL({
      language: DEFAULT_LANGUAGE,
      pathname: `/profiles/${page}`,
    }),
  newLink: String(new ProfileCreateURL(DEFAULT_LANGUAGE)),
});

export default Profiles;
