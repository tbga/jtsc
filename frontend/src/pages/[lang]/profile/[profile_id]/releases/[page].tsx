import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchProfileReleases } from "#api/profiles";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  getProfile,
  NewProfileReleasesURL,
  ProfileReleasesURL,
  ProfileURL,
} from "#entities/profile";
import { ReleaseCard } from "#entities/release";

function ProfileReleasesPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profileData, changeProfileData] =
    useState<Awaited<ReturnType<typeof getProfile>>>();
  const [releasesData, changeReleaseData] =
    useState<Awaited<ReturnType<typeof fetchProfileReleases>>>();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const page = resolveQueryValue(query.page);
  const profileName = profileData?.name ?? "Unknown";
  const title = !profileData
    ? "Profile releases"
    : !releasesData
      ? `Profile "${profileName}" (${profileData.profile.id}) releases`
      : `Profile "${profileName}" (${profileData.profile.id}) releases page ${releasesData.pagination.current_page} out of ${releasesData.pagination.total_pages}`;
  const heading = !profileData
    ? "Profile Releases"
    : `Profile "${profileName}" (${profileData.profile.id}) Releases`;

  // fetch profile
  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const newProfile = await getProfile(profile_id);
      changeProfileData(newProfile);
    })();
  }, [isReady, profile_id]);

  // fetch releases
  useEffect(() => {
    if (!isReady || !profileData || !page) {
      return;
    }

    (async () => {
      const newReleases = await fetchProfileReleases(
        profileData.profile.id,
        page,
      );
      changeReleaseData(newReleases);
    })();
  }, [isReady, profileData, page]);

  return (
    <Page title={title} heading={heading}>
      {!profileData || !releasesData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={releasesData.pagination}
          urlBuilder={(page) =>
            new ProfileReleasesURL(
              language,
              profileData.profile.id,
              String(page),
            )
          }
        >
          {releasesData.releases.map((release) => (
            <ReleaseCard key={release.id} release={release} />
          ))}
        </CardList>
      )}

      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link href={new ProfileURL(language, profileData.profile.id)}>
                  Profile
                </Link>
              )}
            </Item>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link
                  href={
                    new NewProfileReleasesURL(language, profileData.profile.id)
                  }
                >
                  Add
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ProfileReleasesPage;
