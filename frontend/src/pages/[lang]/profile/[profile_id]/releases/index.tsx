import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchProfileReleasesPagination } from "#api/profiles";
import { paginationRedirectMulti } from "#components";
import { NewProfileReleasesURL, ProfileReleasesURL } from "#entities/profile";

const ProfileReleases = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      profile_id: resolveQueryValue(query.profile_id),
    };
  },
  prefix: "profile releases",
  title: (query) => "Profile Releases",
  paginationFetcher: async (query) =>
    fetchProfileReleasesPagination(query.profile_id!),
  urlBuilder: (page, query) =>
    new ProfileReleasesURL(DEFAULT_LANGUAGE, query.profile_id!, page),
  newLink: (query) =>
    new NewProfileReleasesURL(DEFAULT_LANGUAGE, query.profile_id!),
});

export default ProfileReleases;
