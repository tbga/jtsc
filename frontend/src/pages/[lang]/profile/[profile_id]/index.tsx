import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  LoadingBar,
  Page,
} from "#components";
import { getProfile, ProfileArticle } from "#entities/profile";

function ProfilePage() {
  const router = useRouter();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const [profileData, changeProfile] =
    useState<Awaited<ReturnType<typeof getProfile>>>();
  const title = !profileData?.namesData
    ? "Profile"
    : `Profile ${
        profileData.namesData.names[0]
          ? profileData.namesData.names[0].name
          : "Unknown"
      } (${profileData.profile.id})`;

  useEffect(() => {
    if (!isReady || !profile_id) {
      return;
    }

    (async () => {
      const profile = await getProfile(profile_id);
      changeProfile(profile);
    })();
  }, [isReady, profile_id]);

  return (
    <Page title={title} heading={null}>
      {!profileData ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </Article>
      ) : (
        <ProfileArticle
          headingLevel={1}
          profile={profileData.profile}
          namesData={profileData.namesData}
        />
      )}
    </Page>
  );
}

export default ProfilePage;
