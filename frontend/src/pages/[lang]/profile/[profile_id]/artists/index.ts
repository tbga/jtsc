import { DEFAULT_LANGUAGE } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { fetchProfileArtistPagination } from "#api/profiles";
import { paginationRedirectMulti } from "#components";
import { NewProfileArtistsURL, ProfileArtistsURL } from "#entities/profile";

const ProfileArtists = paginationRedirectMulti({
  queryResolver: (query) => {
    return {
      profile_id: resolveQueryValue(query.profile_id)!,
    };
  },
  prefix: "profile artists",
  title: (query) => "Profile Artists",
  paginationFetcher: async (query) => {
    return fetchProfileArtistPagination(query.profile_id);
  },
  urlBuilder: (page, query) =>
    new ProfileArtistsURL(DEFAULT_LANGUAGE, query.profile_id, page),
  newLink: (query) =>
    new NewProfileArtistsURL(DEFAULT_LANGUAGE, query.profile_id),
});

export default ProfileArtists;
