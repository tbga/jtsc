import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { resolveQueryValue } from "#lib/util";
import { fetchProfileArtists } from "#api/profiles";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar, Page } from "#components";
import { CardList, Item, List } from "#components/lists";
import { Link } from "#components/links";
import {
  getProfile,
  NewProfileArtistsURL,
  ProfileArtistsURL,
  ProfileURL,
} from "#entities/profile";
import { ArtistCard } from "#entities/artist";

function ProfileArtistsPage() {
  const router = useRouter();
  const { language } = useLanguage();
  const [profileData, changeProfileData] =
    useState<Awaited<ReturnType<typeof getProfile>>>();
  const [artistsData, changeArtistsData] =
    useState<Awaited<ReturnType<typeof fetchProfileArtists>>>();
  const { query, isReady } = router;
  const profile_id = resolveQueryValue(query.profile_id);
  const page = resolveQueryValue(query.page);
  const profileName = profileData?.name ?? "Unknown";
  const title = !profileData
    ? "Profile artists"
    : !artistsData
      ? `Profile "${profileName}" (${profileData.profile.id}) artists`
      : `Profile "${profileName}" (${profileData.profile.id}) artists page ${artistsData.pagination.current_page} out of ${artistsData.pagination.total_pages}`;
  const heading = !profileData
    ? "Profile Artists"
    : `Profile "${profileName}" (${profileData.profile.id}) Artists`;

  // fetch profile
  useEffect(() => {
    if (!isReady || !profile_id || !page) {
      return;
    }

    (async () => {
      const isSameID = profile_id === profileData?.profile.id;
      const profileID = isSameID ? profileData.profile.id : profile_id;

      if (!isSameID) {
        const newProfile = await getProfile(profile_id);

        changeProfileData(newProfile);
      }

      const newArtists = await fetchProfileArtists(profileID, page);

      changeArtistsData(newArtists);
    })();
  }, [isReady, profile_id, page]);

  return (
    <Page title={title} heading={heading}>
      {!(profileData && artistsData) ? (
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      ) : (
        <CardList
          isDescending
          pagination={artistsData.pagination}
          urlBuilder={(page) =>
            new ProfileArtistsURL(
              language,
              profileData.profile.id,
              String(page),
            )
          }
        >
          {artistsData.artists.map((artist) => (
            <ArtistCard key={artist.id} artist={artist} />
          ))}
        </CardList>
      )}
      <Article>
        <ArticleHeader>
          <List>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link href={new ProfileURL(language, profileData.profile.id)}>
                  Profile
                </Link>
              )}
            </Item>
            <Item>
              {!profileData ? (
                <LoadingBar />
              ) : (
                <Link
                  href={
                    new NewProfileArtistsURL(language, profileData.profile.id)
                  }
                >
                  Add
                </Link>
              )}
            </Item>
          </List>
        </ArticleHeader>
      </Article>
    </Page>
  );
}

export default ProfileArtistsPage;
