import { ProjectURL } from "#lib/urls";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleBody, ArticleHeader, Page } from "#components";
import { Link } from "#components/links";
import { RegisterAccountForm } from "#entities/account";

function RegisterAccount() {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const title = "Register account";
  const heading = "Register Account";

  return (
    <Page title={title} heading={heading}>
      <Article>
        {isRegistered ? (
          <ArticleHeader>
            <p>You are registered.</p>
          </ArticleHeader>
        ) : (
          <>
            <ArticleHeader>
              <p>
                Already registered?{" "}
                <Link
                  href={
                    new ProjectURL({
                      language,
                      pathname: "/authentication/login",
                    })
                  }
                >
                  Login instead
                </Link>
                .
              </p>
            </ArticleHeader>
            <ArticleBody>
              <RegisterAccountForm id="register-account" />
            </ArticleBody>
          </>
        )}
      </Article>
    </Page>
  );
}

export default RegisterAccount;
