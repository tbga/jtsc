import { ProjectURL } from "#lib/urls";
import { useAccount, useLanguage } from "#hooks";
import { Article, ArticleBody, ArticleHeader, Page } from "#components";
import { Link } from "#components/links";
import { LoginAccountForm } from "#entities/account";

function LoginAccount() {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const title = "Log in to account";
  const heading = "Log in to Account";

  return (
    <Page title={title} heading={heading}>
      <Article>
        {isRegistered ? (
          <ArticleHeader>
            <p>You are logged in.</p>
          </ArticleHeader>
        ) : (
          <>
            <ArticleHeader>
              <p>
                Don&apos;t have an account?{" "}
                <Link
                  href={
                    new ProjectURL({
                      language,
                      pathname: "/authentication/register",
                    })
                  }
                >
                  Register instead
                </Link>
                .
              </p>
            </ArticleHeader>
            <ArticleBody>
              <LoginAccountForm id="login-account" />
            </ArticleBody>
          </>
        )}
      </Article>
    </Page>
  );
}

export default LoginAccount;
