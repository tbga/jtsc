import { Html, Head, Main, NextScript, DocumentProps } from "next/document";
import { DEFAULT_LANGUAGE, DEFAULT_THEME } from "#environment/variables";

function Document({ __NEXT_DATA__ }: DocumentProps) {
  const currentLanguage = __NEXT_DATA__?.locale || DEFAULT_LANGUAGE;

  return (
    <Html lang={currentLanguage} data-theme={DEFAULT_THEME}>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

export default Document;
