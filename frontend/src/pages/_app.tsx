import "../styles/global.scss";
import { useEffect } from "react";
import { type AppProps } from "next/app";
import { useRouter } from "next/router";
import Head from "next/head";
import { DEFAULT_THEME, SUPPORTED_LANGUAGES } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import {
  AccountProvider,
  LanguageProvider,
  LocalFileSystemProvider,
} from "#hooks";
import { ErrorBoundary } from "#components/errors";
import { Layout } from "#components/layout";

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { isReady, query, pathname } = router;
  const [_, lang] = pathname.split("/");
  const isRootPage = Boolean(lang) === false;
  const langQuery = resolveQueryValue(query.lang);

  useEffect(() => {
    // if the page is not root
    // and `lang` value is not known,
    // redirect to 404 page
    if (
      !isReady ||
      isRootPage ||
      (langQuery && SUPPORTED_LANGUAGES.includes(langQuery))
    ) {
      return;
    }

    router.push("/404");
  }, [isReady, isRootPage, langQuery]);

  return (
    <>
      <Head>
        <script
          id="theme-setup"
          async
          // rome-ignore lint/security/noDangerouslySetInnerHtml: it has to be this way
          dangerouslySetInnerHTML={{
            __html: `
            (() => {
              const theme = document.cookie
                .split("; ")
                .find((row) => row.startsWith("theme="));
              const value = theme ? theme.split("=")[1] : "${DEFAULT_THEME}";
              document.documentElement.dataset.theme = value;
            })()
            `,
          }}
        />
      </Head>

      <ErrorBoundary headingLevel={1}>
        <LanguageProvider>
          <AccountProvider>
            <LocalFileSystemProvider>
              {isRootPage ? (
                <Component {...pageProps} />
              ) : (
                <Layout>
                  <Component {...pageProps} />
                </Layout>
              )}
            </LocalFileSystemProvider>
          </AccountProvider>
        </LanguageProvider>
      </ErrorBoundary>
    </>
  );
}

export default MyApp;
