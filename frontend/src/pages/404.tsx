import { Heading } from "#components";

function Error404() {
  return (
    <section>
      <Heading level={1}>404</Heading>
      <Heading level={2}>Page Not Found</Heading>
    </section>
  );
}

export default Error404;
