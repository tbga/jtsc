import { useLanguage } from "#hooks";
import { Heading } from "#components";
import { Link } from "#components/links";
import { AccountLoginURL, AccountRegistrationURL } from "#entities/account";

function Error401() {
  const { language } = useLanguage();
  return (
    <section>
      <Heading level={1}>401</Heading>
      <Heading level={2}>Unauthorized</Heading>
      <p>
        If you have an account -{" "}
        <Link href={new AccountLoginURL(language)}>log in</Link>. Otherwise{" "}
        <Link href={new AccountRegistrationURL(language)}>register</Link>.
      </p>
    </section>
  );
}

export default Error401;
