import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { parse as parseLang, type Schema as LangSchema } from "bcp-47";
import iso6391 from "iso-639-1";
import Head from "next/head";
import {
  DEFAULT_LANGUAGE,
  SUPPORTED_LANGUAGES,
  TITLE,
} from "#environment/variables";
import { ProjectURL } from "#lib/urls";
import { COOKIE, setCookie, getCookie } from "#store/cookie";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
} from "#components";
import { LinkButton } from "#components/links";
import { Item, List } from "#components/lists";

import styles from "./index.module.scss";

const langMap = SUPPORTED_LANGUAGES.sort().reduce<Record<string, LangSchema>>(
  (map, lang) => {
    const schema = parseLang(lang);

    map[lang] = schema;

    return map;
  },
  {},
);

/**
 * Rendering root page differently because
 * it's the only page which doesn't derive
 * its language from url.
 *
 * @TODO fix language guess defaulting to `en`
 */
function Landing() {
  const router = useRouter();
  const [language, changeLanguage] = useState<string>();
  const isLoading = !language;

  useEffect(
    () => {
      // if the amount of supported languages is less then 2
      // redirect to the default language
      if (SUPPORTED_LANGUAGES.length < 2) {
        router.push(new ProjectURL({ language: DEFAULT_LANGUAGE }));
      }

      (async () => {
        const cookieLocale = getCookie(COOKIE.LANGUAGE);
        // if no locale in the cookie or the locale isn't supported
        // pick default langauge
        const newLanguage =
          !cookieLocale || !SUPPORTED_LANGUAGES.includes(cookieLocale)
            ? DEFAULT_LANGUAGE
            : cookieLocale;

        changeLanguage(newLanguage);
      })();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [language],
  );

  return (
    <>
      <Head>
        <title key="page-title">{TITLE}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <Article>
          <ArticleHeader>
            <Heading level={1}>{TITLE}</Heading>
          </ArticleHeader>
          <ArticleBody>
            {isLoading ? (
              <LoadingBar />
            ) : (
              <>
                {/* @TODO: actula multilang */}
                <p>Choose language:</p>
                <List>
                  {Object.entries(langMap).map(([lang, { language }]) => (
                    <Item key={lang}>
                      <LinkButton
                        href={new ProjectURL({ language: lang })}
                        onClick={async () => {
                          setCookie(COOKIE.LANGUAGE, lang);
                        }}
                      >
                        <span className={styles.lang}>{language}</span>{" "}
                        <span>{iso6391.getNativeName(language!)}</span> (
                        <span>{iso6391.getName(language!)}</span>)
                      </LinkButton>
                    </Item>
                  ))}
                </List>
              </>
            )}
          </ArticleBody>
        </Article>
      </main>
    </>
  );
}

export default Landing;
