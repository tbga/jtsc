import { Heading } from "#components";

function Error500() {
  return (
    <section>
      <Heading level={1}>500</Heading>
      <Heading level={2}>Unknown Error</Heading>
    </section>
  );
}

export default Error500;
