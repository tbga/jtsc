import clsx from "clsx";
import { CSSProperties } from "react";
import { blockComponent, IBlockProps } from "#components/meta";

import styles from "./buttons.module.scss";

export interface IListButtonsProps extends Omit<IBlockProps<"div">, "style"> {
  buttonWidth?: string | number;
  isSpread?: boolean;
}

export const ListButtons = blockComponent(styles.block, Component);

function Component({
  buttonWidth,
  isSpread,
  children,
  className,
  ...blockProps
}: IListButtonsProps) {
  const blockClass = clsx(className, isSpread && styles.spread);
  const style = !buttonWidth
    ? undefined
    : ({ "--local-button-width": buttonWidth } as CSSProperties);

  return (
    <div className={blockClass} {...blockProps} style={style}>
      {children}
    </div>
  );
}
