import { type CSSProperties } from "react";
import clsx from "clsx";
import { blockComponent, type IBlockProps } from "#components/meta";
import { type IHeadingLevel } from "#components";

import styles from "./card.module.scss";

/**
 * @TODO make heading level required
 */
export interface ICardProps extends IBlockProps<"article"> {
  headingLevel?: IHeadingLevel;
  isMerged?: boolean;
  minHeight?: string;
}
export interface ICardHeaderProps extends IBlockProps<"header"> {}
export interface ICardBodyProps extends IBlockProps<"section"> {}
export interface ICardFooterProps extends IBlockProps<"footer"> {}

export const Card = blockComponent(styles.block, CardComponent);
export const CardHeader = blockComponent(styles.header, CardHeaderComponent);
export const CardBody = blockComponent(styles.body, CardBodyComponent);
export const CardFooter = blockComponent(styles.footer, CardFooterComponent);

function CardComponent({
  className,
  minHeight,
  isMerged,
  children,
  ...blockProps
}: ICardProps) {
  const style = minHeight
    ? ({
        "--local-min-height": minHeight,
      } as CSSProperties)
    : undefined;
  const blockClass = clsx(className, isMerged && styles.merged);

  return (
    <article className={blockClass} style={style} {...blockProps}>
      {children}
    </article>
  );
}

function CardHeaderComponent({ children, ...blockProps }: ICardHeaderProps) {
  return <header {...blockProps}>{children}</header>;
}

function CardBodyComponent({ children, ...blockProps }: ICardBodyProps) {
  return <section {...blockProps}>{children}</section>;
}

function CardFooterComponent({ children, ...blockProps }: ICardFooterProps) {
  return <footer {...blockProps}>{children}</footer>;
}
