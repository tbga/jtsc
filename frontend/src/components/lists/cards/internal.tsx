import clsx from "clsx";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  PaginationInfo,
  PaginationInternal,
  type IPaginationProps,
} from "#components/pagination";
import { IBigIntegerPositive } from "#types";

import styles from "./internal.module.scss";

export interface ICardListInternalProps extends IBlockProps<"div"> {
  pagination: IPaginationProps["pagination"];
  onPageChange: (page: IBigIntegerPositive) => Promise<void>;
  isDescending?: boolean;
}

export const CardListInternal = blockComponent(undefined, Component);

function Component({
  pagination,
  onPageChange,
  isDescending,
  children,
  ...blockProps
}: ICardListInternalProps) {
  const listClass = clsx(styles.list, isDescending && styles.descending);

  return (
    <div {...blockProps}>
      <PaginationInfo pagination={pagination} />
      <div className={listClass}>{children}</div>
      <PaginationInternal pagination={pagination} onPageChange={onPageChange} />
    </div>
  );
}
