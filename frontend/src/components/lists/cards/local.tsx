import clsx from "clsx";
import { useState, type ReactNode } from "react";
import { DEFAULT_LIMIT } from "#lib/pagination";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  IPaginationLocalProps,
  PaginationInfo,
  PaginationLocal,
} from "#components/pagination";

import styles from "./internal.module.scss";

export interface ICardListLocalProps
  extends IBlockProps<"div">,
    Pick<IPaginationLocalProps, "items"> {
  limit?: number;
  items: ReactNode[];
  isDescending?: boolean;
}

export const CardListLocal = blockComponent(undefined, Component);

function Component({
  items,
  limit = DEFAULT_LIMIT,
  isDescending,
  children,
  ...blockProps
}: ICardListLocalProps) {
  const [currentPage, changeCurrentPage] = useState(1);
  const totalCount = items.length;
  const totalPages = Math.ceil(totalCount / limit);
  const isFirstPage = currentPage === 1;
  const isLastPage = currentPage === totalPages;
  const currentMin = !isFirstPage ? (currentPage - 1) * limit : 0;
  const currentMax = !isLastPage ? currentMin + limit : totalCount;
  const currentRange = currentMax - currentMin;
  // fill up the list with fillers if the current range is less than limit
  const currentItems =
    currentRange === limit
      ? items.slice(currentMin, currentMax)
      : items.slice(currentMin, currentMax);
  const listClass = clsx(styles.list, isDescending && styles.descending);

  return (
    <div {...blockProps}>
      <PaginationInfo
        pagination={{
          current_page: String(currentPage),
          limit,
          total_count: String(totalCount),
          total_pages: String(totalPages),
        }}
      />
      <div className={listClass}>{currentItems}</div>
      <PaginationLocal
        items={items}
        limit={limit}
        onPageChange={async (nextPage) => {
          changeCurrentPage(nextPage);
        }}
      />
    </div>
  );
}
