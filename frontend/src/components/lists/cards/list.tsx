import clsx from "clsx";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  PaginationInfo,
  Pagination,
  IPaginationProps,
} from "#components/pagination";

import styles from "./list.module.scss";

export interface ICardListProps extends IBlockProps<"div"> {
  pagination: IPaginationProps["pagination"];
  /**
   * A function to create a URL for a different page.
   */
  urlBuilder: IPaginationProps["urlBuilder"];
  isDescending?: boolean;
}

export const CardList = blockComponent(styles.block, Component);

function Component({
  pagination,
  urlBuilder,
  isDescending = false,
  children,
  ...blockProps
}: ICardListProps) {
  const listClass = clsx(styles.list, isDescending && styles.descending);

  return (
    <div {...blockProps}>
      <PaginationInfo pagination={pagination} />
      <div className={listClass}>{children}</div>
      <Pagination pagination={pagination} urlBuilder={urlBuilder} />
    </div>
  );
}
