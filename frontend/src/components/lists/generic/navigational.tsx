import { type IPagination, type IURLBuilder } from "#lib/pagination";
import { blockComponent, type IBlockProps } from "#components/meta";
import { PaginationInfo, Pagination } from "#components/pagination";
import { type IListProps, List } from "./list";

import styles from "./navigational.module.scss";

export interface IListNavigationalProps
  extends IBlockProps<"div">,
    Pick<
      IListProps,
      "type" | "isHorizontal" | "isAlternating" | "isDescending"
    > {
  pagination: IPagination;
  urlBuilder: IURLBuilder;
}

export const ListNavigational = blockComponent(styles.block, Component);

function Component({
  pagination,
  urlBuilder,
  type,
  isHorizontal,
  isAlternating,
  isDescending,
  children,
  ...blockProps
}: IListNavigationalProps) {
  return (
    <div {...blockProps}>
      <List
        className={styles.list}
        type={type}
        isHorizontal={isHorizontal}
        isAlternating={isAlternating}
        isDescending={isDescending}
      >
        {children}
      </List>
      <Pagination
        className={styles.pagination}
        pagination={pagination}
        urlBuilder={urlBuilder}
      />
      <PaginationInfo pagination={pagination} />
    </div>
  );
}
