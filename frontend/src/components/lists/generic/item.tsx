import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./item.module.scss";

export interface IItemProps extends IBlockProps<"li"> {}

export const Item = blockComponent(styles.item, ItemComponent);

function ItemComponent({ children, ...blockProps }: IItemProps) {
  return <li {...blockProps}>{children}</li>;
}
