import { type ReactNode, useState } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { Item } from "#components/lists";
import { PaginationLocal, PaginationInfo } from "#components/pagination";
import { type IListProps, List } from "./list";

import styles from "./local.module.scss";

export interface IListLocalProps
  extends IBlockProps<"div">,
    Pick<
      IListProps,
      "type" | "isHorizontal" | "isAlternating" | "isDescending"
    > {
  items: ReactNode[];
  limit?: number;
}

export const ListLocal = blockComponent(styles.block, Component);

function Component({
  items,
  limit = 10,
  type,
  isHorizontal,
  isAlternating,
  isDescending,
  children,
  ...blockProps
}: IListLocalProps) {
  const [currentPage, changeCurrentPage] = useState(1);
  const totalCount = items.length;
  const totalPages = Math.ceil(totalCount / limit);
  const isFirstPage = currentPage === 1;
  const isLastPage = currentPage === totalPages;
  const currentMin = !isFirstPage ? (currentPage - 1) * limit : 0;
  const currentMax = !isLastPage ? currentMin + limit : totalCount;
  const currentRange = currentMax - currentMin;
  // fill up the list with fillers if the current range is less than limit
  const currentItems =
    currentRange === limit
      ? items.slice(currentMin, currentMax)
      : items
          .slice(currentMin, currentMax)
          .concat(
            new Array(limit - currentRange).fill(
              <Item className={styles.filler} />,
            ),
          );

  return (
    <div {...blockProps}>
      <List
        className={styles.list}
        type={type}
        isHorizontal={isHorizontal}
        isAlternating={isAlternating}
        isDescending={isDescending}
      >
        {currentItems}
      </List>
      <PaginationLocal
        items={items}
        limit={limit}
        onPageChange={async (nextPage) => {
          changeCurrentPage(nextPage);
        }}
      />
      <PaginationInfo
        pagination={{
          current_page: String(currentPage),
          limit,
          total_count: String(totalCount),
          total_pages: String(totalPages),
        }}
      />
    </div>
  );
}
