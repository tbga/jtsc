import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./unordered.module.scss";

export interface IListUnorderedProps extends IBlockProps<"ul"> {}

export const ListUnordered = blockComponent(styles.unordered, Component);

function Component({ children, ...blockProps }: IListUnorderedProps) {
  return <ul {...blockProps}>{children}</ul>;
}
