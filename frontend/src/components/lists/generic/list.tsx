import clsx from "clsx";
import { ProjectError } from "#lib/errors";
import { blockComponent } from "#components/meta";
import { type IListUnorderedProps, ListUnordered } from "./unordered";
import { type IListOrderedProps, ListOrdered } from "./ordered";

import styles from "./list.module.scss";

export type IListProps = {
  isHorizontal?: boolean;
  isAlternating?: boolean;
  isDescending?: boolean;
} & (
  | ({ type?: "ordered" } & IListOrderedProps)
  | ({ type?: "unordered" } & IListUnorderedProps)
);

export const List = blockComponent(styles.block, ListComponent);

function ListComponent({
  type = "unordered",
  isDescending,
  isHorizontal,
  isAlternating,
  children,
  className,
  ...blockProps
}: IListProps) {
  const blockClass = clsx(
    className,
    isHorizontal ? styles.horizontal : styles.vertical,
    !isDescending
      ? undefined
      : isHorizontal
        ? styles.horizontal_descending
        : styles.vertical_descending,
    isAlternating && styles.alternating,
  );

  switch (type) {
    case "unordered": {
      return (
        <ListUnordered className={blockClass} {...blockProps}>
          {children}
        </ListUnordered>
      );
    }

    // @ts-expect-error ts doesn't pick up on the union despite
    // the props being a carbon copy of `ILinkProps`
    case "ordered": {
      return (
        <ListOrdered className={blockClass} {...blockProps}>
          {children}
        </ListOrdered>
      );
    }

    default: {
      throw new ProjectError(`Illegal list type "${type}".`);
    }
  }
}
