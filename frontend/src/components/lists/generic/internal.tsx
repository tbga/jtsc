import { type IPagination } from "#lib/pagination";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  PaginationInfo,
  PaginationInternal,
  type IPaginationInternalProps,
} from "#components/pagination";
import { type IListProps, List } from "./list";

import styles from "./internal.module.scss";

export interface IListInternalProps
  extends IBlockProps<"div">,
    Pick<
      IListProps,
      "type" | "isHorizontal" | "isAlternating" | "isDescending"
    > {
  pagination: IPagination;
  onPageChange: IPaginationInternalProps["onPageChange"];
}

export const ListInternal = blockComponent(styles.block, Component);

function Component({
  pagination,
  onPageChange,
  type,
  isHorizontal,
  isAlternating,
  isDescending,
  children,
  ...blockProps
}: IListInternalProps) {
  return (
    <div {...blockProps}>
      <List
        className={styles.list}
        type={type}
        isHorizontal={isHorizontal}
        isAlternating={isAlternating}
        isDescending={isDescending}
      >
        {children}
      </List>
      <PaginationInternal pagination={pagination} onPageChange={onPageChange} />
      <PaginationInfo pagination={pagination} />
    </div>
  );
}
