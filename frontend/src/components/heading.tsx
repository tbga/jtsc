import { type ReactNode } from "react";
import clsx from "clsx";
import { ProjectError } from "#lib/errors";

import styles from "./heading.module.scss";

const headingLevels = [1, 2, 3, 4, 5, 6] as const;

export type IHeadingLevel = (typeof headingLevels)[number];

export interface IHeadingProps {
  id?: string;
  level?: IHeadingLevel;
  children?: ReactNode;
  isExternal?: boolean;
}

export const DEFAULT_HEADING_LEVEL = 2 as const;

export function Heading({
  id,
  level = DEFAULT_HEADING_LEVEL,
  isExternal,
  children,
}: IHeadingProps) {
  const className = clsx(
    styles.block,
    styles[`h${level}`],
    isExternal && styles.external,
  );

  switch (level) {
    case 1: {
      return (
        <h1 id={id} className={className}>
          {children}
        </h1>
      );
    }

    case 2: {
      return (
        <h2 id={id} className={className}>
          {children}
        </h2>
      );
    }

    case 3: {
      return (
        <h3 id={id} className={className}>
          {children}
        </h3>
      );
    }

    case 4: {
      return (
        <h4 id={id} className={className}>
          {children}
        </h4>
      );
    }

    case 5: {
      return (
        <h5 id={id} className={className}>
          {children}
        </h5>
      );
    }

    case 6: {
      return (
        <h6 id={id} className={className}>
          {children}
        </h6>
      );
    }

    default: {
      throw new ProjectError(`Illegal heading level of ${level}`);
    }
  }
}
