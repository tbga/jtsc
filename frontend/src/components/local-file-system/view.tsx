import { IPlatform } from "#codegen/schema/lib/types/nodejs/platform";
import { LocalPath } from "#lib/local-file-system";
import { copyToClipBoard } from "#browser/clipboard";
import { useLocalFileSystem } from "#hooks";
import { blockComponent } from "#components/meta";
import { LoadingBar } from "#components";
import { DL, DS, type IDLProps, Item, List } from "#components/lists";
import { ListLocal } from "#components/lists/generic";
import { CurrentFolder } from "./current-folder";
import { Entry } from "./entry";

import styles from "./view.module.scss";

interface LocalFileSystemProps extends IDLProps {}
interface IPlatformProps {
  platform: IPlatform;
}

export const LocalFileSystem = blockComponent(styles.block, Component);

function Component({ ...blockProps }: LocalFileSystemProps) {
  const { currentFolder, systemInfo, errors } = useLocalFileSystem();

  return (
    <DL {...blockProps}>
      <DS
        isHorizontal
        dKey="Platform"
        dValue={
          !systemInfo ? (
            <LoadingBar />
          ) : (
            <Platform platform={systemInfo.platform} />
          )
        }
      />
      <DS
        dKey="Path"
        dValue={!currentFolder ? <LoadingBar /> : <CurrentFolder />}
      />
      <DS dKey="Content" dValue={<FolderContent />} />
      {Boolean(errors.length) && (
        <DS
          dKey="Errors"
          dValue={
            <List>
              {errors.map((error, index) => (
                <Item key={index}>{error.message}</Item>
              ))}
            </List>
          }
        />
      )}
    </DL>
  );
}

function Platform({ platform, ...blockProps }: IPlatformProps) {
  switch (platform) {
    case "linux": {
      return <span {...blockProps}>Linux</span>;
    }
    case "win32": {
      return <span {...blockProps}>Windows</span>;
    }

    default: {
      return <span {...blockProps}>{platform} (unknown)</span>;
    }
  }
}

function FolderContent() {
  const { currentFolder, getNextFolder, isReady } = useLocalFileSystem();

  return (
    <ListLocal
      key={isReady ? currentFolder.path : "default_path"}
      className={styles.content}
      isAlternating
      limit={5}
      items={
        !currentFolder
          ? [<LoadingBar key="loading" />]
          : currentFolder.entries.map((entry, index) => (
              <Entry
                key={index}
                entry={entry}
                onNextEntry={async (entry) => {
                  const nextPath = new LocalPath(currentFolder.path).join(
                    entry.name,
                  );
                  await getNextFolder(nextPath);
                }}
                onPathCopy={async (entry) => {
                  const entryPath = new LocalPath(currentFolder.path).join(
                    entry.name,
                  );
                  await copyToClipBoard(entryPath.toString());
                }}
              />
            ))
      }
    />
  );
}
