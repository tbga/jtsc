import { blockComponent } from "#components/meta";
import { Item, IItemProps } from "#components/lists";
import { type IEntry } from "#lib/local-file-system";
import { Button } from "#components/buttons";

import styles from "./entry.module.scss";
import { EMPTY_FUNCTION } from "#types";

interface IEntryProps extends IItemProps {
  entry: IEntry;
  onNextEntry?: (entry: IEntry) => Promise<void>;
  onPathCopy?: (entry: IEntry) => Promise<void>;
}

export function Entry({ entry, onNextEntry, onPathCopy }: IEntryProps) {
  const { type, name, entries, size } = entry;
  const isFolder = type === "folder";

  return (
    <Item className={styles.block}>
      {isFolder ? (
        <Button
          // @TODO: proper `onNextEntry` behaviour
          onClick={
            !onNextEntry
              ? EMPTY_FUNCTION
              : async () => {
                  await onNextEntry(entry);
                }
          }
        >
          {type}: {name} {entries && <>(${entries} items)</>}
        </Button>
      ) : (
        <span>
          {type}: {name} {size && <>({size})</>}
        </span>
      )}
      {onPathCopy && (
        <Button
          className={styles.copy}
          onClick={async () => {
            await onPathCopy(entry);
          }}
        >
          Copy Path
        </Button>
      )}
    </Item>
  );
}
