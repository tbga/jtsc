import { LocalPath } from "#lib/local-file-system";
import { useLocalFileSystem } from "#hooks";
import { blockComponent, type IBlockProps } from "#components/meta";
import { Button } from "#components/buttons";
import { IListButtonsProps, Item, List, ListButtons } from "#components/lists";

import styles from "./current-folder.module.scss";
import { copyToClipBoard } from "#browser/clipboard";

interface ICurrentFolderProps extends IListButtonsProps {}

export const CurrentFolder = blockComponent(styles.block, Component);

function Component({ ...blockProps }: ICurrentFolderProps) {
  const { currentFolder, getNextFolder } = useLocalFileSystem();
  const localPath = new LocalPath(currentFolder.path);
  const segments = localPath.segments();

  return (
    <ListButtons {...blockProps}>
      <Button
        className={styles.back}
        onClick={async () => {
          const newPath = localPath.dirName();
          await getNextFolder(newPath);
        }}
      >
        Back
      </Button>
      <List isHorizontal className={styles.segments}>
        {segments.map((segment, index) => (
          <Item key={index} className={styles.segment}>
            <span>{decodeURIComponent(segment)}</span>
            {index === segments.length - 1 ? undefined : (
              <span>{localPath.separator}</span>
            )}
          </Item>
        ))}
      </List>
      <Button
        className={styles.copy}
        onClick={() => {
          copyToClipBoard(currentFolder.path);
        }}
      >
        Copy path
      </Button>
    </ListButtons>
  );
}
