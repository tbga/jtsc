import clsx from "clsx";
import { useState } from "react";
import { type IPagination } from "#lib/pagination";
import { ListButtons, type IListButtonsProps } from "#components/lists";
import { blockComponent } from "#components/meta";
import { Button } from "#components/buttons";
import { BIGINT_ONE, type IBigSerialInteger } from "#types";

import styles from "./internal.module.scss";

export interface IPaginationInternalProps extends IListButtonsProps {
  pagination: IPagination;
  onPageChange: (page: IBigSerialInteger) => Promise<void>;
}

/**
 * Pagination but only controlled by fetches.
 */
export const PaginationInternal = blockComponent(styles.block, Component);

function Component({
  pagination,
  onPageChange,
  ...blockProps
}: IPaginationInternalProps) {
  const [isSwitching, changeSwitching] = useState(false);
  const current_page = BigInt(pagination.current_page);
  const total_pages = BigInt(pagination.total_pages);
  const isFirstPage = current_page === BIGINT_ONE;
  const isLastPage = current_page === total_pages;

  async function changePage(nextPage: IBigSerialInteger) {
    if (isSwitching) {
      return;
    }

    try {
      changeSwitching(true);
      await onPageChange(nextPage);
    } finally {
      changeSwitching(false);
    }
  }

  return (
    <ListButtons {...blockProps}>
      <Button
        className={styles.page}
        disabled={isFirstPage}
        onClick={async () => {
          await changePage(String(1));
        }}
      >
        1
      </Button>

      <Button
        className={styles.page}
        disabled={current_page - BIGINT_ONE <= BIGINT_ONE}
        onClick={async () => {
          await changePage(String(current_page - BIGINT_ONE));
        }}
      >
        {current_page - BIGINT_ONE <= BIGINT_ONE
          ? "..."
          : String(current_page - BIGINT_ONE)}
      </Button>

      <span className={clsx(styles.page, styles.current)}>
        {String(current_page)}
      </span>

      <Button
        className={styles.page}
        disabled={current_page + BIGINT_ONE >= total_pages}
        onClick={async () => {
          await changePage(String(current_page + BIGINT_ONE));
        }}
      >
        {current_page + BIGINT_ONE >= total_pages
          ? "..."
          : String(current_page + BIGINT_ONE)}
      </Button>

      <Button
        className={styles.page}
        disabled={isLastPage}
        onClick={async () => {
          await changePage(String(total_pages));
        }}
      >
        {String(total_pages)}
      </Button>
    </ListButtons>
  );
}
