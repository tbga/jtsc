import { useState } from "react";
import { DEFAULT_LIMIT, PaginationError } from "#lib/pagination";
import { IListButtonsProps, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { Button } from "#components/buttons";

import styles from "./local.module.scss";
import clsx from "clsx";

export interface IPaginationLocalProps<ItemType = unknown>
  extends IListButtonsProps {
  items: ItemType[];
  limit?: number;
  onPageChange: (page: number) => Promise<void>;
}

/**
 * Pagination but for raw arrays.
 */
export const PaginationLocal = blockComponent(styles.block, Component);

function Component<ItemType = unknown>({
  items,
  limit = DEFAULT_LIMIT,
  onPageChange,
  ...blockProps
}: IPaginationLocalProps<ItemType>) {
  const [isSwitching, changeSwitching] = useState(false);
  const [currentPage, switchCurrentPage] = useState(1);
  const totalPages = Math.ceil(items.length / limit);
  async function changePage(nextPage: number) {
    if (
      isSwitching ||
      nextPage === currentPage ||
      nextPage < 1 ||
      nextPage > totalPages
    ) {
      return;
    }

    try {
      changeSwitching(true);
      await onPageChange(nextPage);
      switchCurrentPage(nextPage);
    } catch (error) {
      if (!PaginationError.isError(error)) {
        throw error;
      }

      return;
    } finally {
      changeSwitching(false);
    }
  }

  return (
    <ListButtons {...blockProps}>
      <Button
        className={styles.page}
        disabled={currentPage === 1}
        onClick={async () => {
          await changePage(1);
        }}
      >
        1
      </Button>

      <Button
        className={styles.page}
        disabled={currentPage - 1 <= 1}
        onClick={async () => {
          await changePage(currentPage - 1);
        }}
      >
        {currentPage - 1 <= 1 ? "..." : currentPage - 1}
      </Button>

      <span className={clsx(styles.page, styles.current)}>{currentPage}</span>

      <Button
        className={styles.page}
        disabled={currentPage + 1 >= totalPages}
        onClick={async () => {
          await changePage(currentPage + 1);
        }}
      >
        {currentPage + 1 >= totalPages ? "..." : currentPage + 1}
      </Button>

      <Button
        className={styles.page}
        disabled={currentPage === totalPages}
        onClick={async () => {
          await changePage(totalPages);
        }}
      >
        {currentPage === totalPages ? "..." : totalPages}
      </Button>
    </ListButtons>
  );
}
