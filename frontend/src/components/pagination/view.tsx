import clsx from "clsx";
import { LinkButton, LinkInternal } from "#components/links";
import { List, Item, type IListProps } from "#components/lists";
import { blockComponent } from "#components/meta";
import { type IPagination, type IURLBuilder } from "#lib/pagination";
import { BIGINT_ONE } from "#types";

import styles from "./view.module.scss";

export type IPaginationProps = IListProps & {
  pagination: IPagination;
  urlBuilder: IURLBuilder;
};

export const Pagination = blockComponent(styles.block, Component);

function Component({
  pagination,
  urlBuilder,
  ...blockProps
}: IPaginationProps) {
  const current_page = BigInt(pagination.current_page);
  const total_pages = BigInt(pagination.total_pages);
  const isFirstPage = current_page === BIGINT_ONE;
  const isLastPage = current_page === total_pages;

  return (
    <List isHorizontal {...blockProps}>
      <Item className={clsx(styles.page, isFirstPage && styles.empty)}>
        {isFirstPage ? (
          1
        ) : (
          <LinkButton className={styles.link} href={urlBuilder(1)}>
            1
          </LinkButton>
        )}
      </Item>

      <Item
        className={clsx(
          styles.page,
          current_page - BIGINT_ONE <= BIGINT_ONE && styles.empty,
        )}
      >
        {current_page - BIGINT_ONE <= BIGINT_ONE ? (
          "..."
        ) : (
          <LinkButton
            className={styles.link}
            href={urlBuilder(String(current_page - BIGINT_ONE))}
          >
            {String(current_page - BIGINT_ONE)}
          </LinkButton>
        )}
      </Item>

      <Item className={clsx(styles.page, styles.current)}>
        {String(current_page)}
      </Item>

      <Item
        className={clsx(
          styles.page,
          current_page + BIGINT_ONE >= total_pages && styles.empty,
        )}
      >
        {current_page + BIGINT_ONE >= total_pages ? (
          "..."
        ) : (
          <LinkButton
            className={styles.link}
            href={urlBuilder(String(current_page + BIGINT_ONE))}
          >
            {String(current_page + BIGINT_ONE)}
          </LinkButton>
        )}
      </Item>

      <Item className={clsx(styles.page, isLastPage && styles.empty)}>
        {isLastPage ? (
          String(total_pages)
        ) : (
          <LinkButton
            className={styles.link}
            href={urlBuilder(String(total_pages))}
          >
            {String(total_pages)}
          </LinkButton>
        )}
      </Item>
    </List>
  );
}
