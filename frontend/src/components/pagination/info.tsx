import { blockComponent, type IBlockProps } from "#components/meta";
import { type IPagination } from "#lib/pagination";
import { BIGINT_ONE } from "#types";

import styles from "./info.module.scss";

export interface IPaginationInfoProps extends IBlockProps<"p"> {
  pagination: IPagination;
}

export const PaginationInfo = blockComponent(styles.block, Component);

function Component({ pagination, ...blockProps }: IPaginationInfoProps) {
  const { limit } = pagination;
  const current_page = BigInt(pagination.current_page);
  const total_count = BigInt(pagination.total_count);
  const total_pages = BigInt(pagination.total_pages);
  const isLastPage = current_page === total_pages;
  const currentMin = (current_page - BIGINT_ONE) * BigInt(limit) + BIGINT_ONE;
  const currentMax = isLastPage
    ? total_count
    : currentMin + BigInt(limit) - BIGINT_ONE;

  return (
    <p {...blockProps}>
      Showing {String(currentMin)}-{String(currentMax)} out of total{" "}
      {String(total_count)}.
    </p>
  );
}
