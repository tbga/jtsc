import { blockComponent, type IChildlessBlockProps } from "#components/meta";

interface IAudioProps extends IChildlessBlockProps<"audio"> {}

export const Audio = blockComponent(undefined, Component);

function Component({ ...blockProps }: IAudioProps) {
  return <audio {...blockProps} />;
}
