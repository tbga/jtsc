import { type Ref, useState, forwardRef } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { NEWLINE_SEPARATOR } from "#lib/strings";

import styles from "./textarea.module.scss";

interface IBaseProps
  extends Omit<IBlockProps<"div">, "id" | "name">,
    Pick<Required<IBlockProps<"textarea">>, "id" | "form" | "name">,
    Pick<
      IBlockProps<"textarea">,
      | "required"
      | "readOnly"
      | "disabled"
      | "minLength"
      | "maxLength"
      | "defaultValue"
      | "rows"
    > {}

export interface ITextAreaProps extends IBaseProps {}

/**
 * @TODO
 * - wrapping lines count
 * - initial row count
 */
export const TextArea = forwardRef<HTMLTextAreaElement, ITextAreaProps>(
  blockComponent(styles.block, Component),
);

function Component(
  {
    id,
    form,
    name,
    required,
    readOnly,
    disabled,
    minLength,
    maxLength,
    defaultValue,
    rows,
    children,
    ...blockProps
  }: ITextAreaProps,
  ref?: Ref<HTMLTextAreaElement>,
) {
  const [lines, changeLines] = useState(getLines(String(defaultValue)));

  function getLines(value: string) {
    if (!value) {
      return [1];
    }

    const lines = value.split(NEWLINE_SEPARATOR).map((line) => line.length);

    return lines;
  }

  return (
    <div {...blockProps}>
      <div className={styles.lines}>
        {lines.map((height, index) => (
          <span key={index} className={styles.line} />
        ))}
      </div>
      <textarea
        ref={ref}
        id={id}
        form={form}
        className={styles.area}
        name={name}
        required={required}
        readOnly={readOnly}
        disabled={disabled}
        minLength={minLength}
        maxLength={maxLength}
        defaultValue={defaultValue}
        rows={rows}
        onKeyUp={(event) => {
          const newLines = getLines(
            (event.target as HTMLTextAreaElement).value,
          );

          changeLines(newLines);
        }}
        onKeyDown={(event) => {
          const area = event.target as HTMLTextAreaElement;

          // add a white space on pressing `tab`
          if (event.key === "Tab") {
            event.preventDefault();
            const start = area.selectionStart;
            const end = area.selectionEnd;

            area.value = `${area.value.substring(
              0,
              start,
            )} ${area.value.substring(end)}`;
          }
        }}
      >
        {children}
      </textarea>
    </div>
  );
}
