import { type CSSProperties, type Ref, forwardRef } from "react";
import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "./html";

import styles from "./text.module.scss";

interface ITextInputProps extends Omit<IInputHTMLProps, "type" | "style"> {}

export const TextInput = forwardRef<HTMLInputElement, ITextInputProps>(
  blockComponent(styles.block, Component),
);

function Component(
  { minLength, maxLength, ...blockProps }: ITextInputProps,
  ref?: Ref<HTMLInputElement>,
) {
  const highestValue = Math.max(minLength ?? 0, maxLength ?? 0);
  const maxWidth = highestValue === 0 ? undefined : highestValue - 1.5;

  return (
    <InputHTML
      type="text"
      style={
        !maxWidth
          ? undefined
          : ({ "--local-max-width": `${maxWidth}em` } as CSSProperties)
      }
      minLength={minLength}
      maxLength={maxLength}
      {...blockProps}
      ref={ref}
    />
  );
}
