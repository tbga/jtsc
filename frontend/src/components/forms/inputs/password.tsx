import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "./html";

export const PASSWORD_AUTOCOMPLETE = {
  ON: "on",
  OFF: "off",
  CURRENT_PASSWORD: "current-password",
  NEW_PASSWORD: "new-password",
} as const;

export type IPasswordAutoComplete =
  (typeof PASSWORD_AUTOCOMPLETE)[keyof typeof PASSWORD_AUTOCOMPLETE];

export interface IPasswordInputProps extends Omit<IInputHTMLProps, "type"> {
  autoComplete: IPasswordAutoComplete;
}

export const PasswordInput = blockComponent(undefined, Component);

function Component({ ...blockProps }: IPasswordInputProps) {
  return <InputHTML {...blockProps} type="password" />;
}
