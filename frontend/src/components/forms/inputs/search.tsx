import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "./html";

interface IProps extends Omit<IInputHTMLProps, "type"> {}

export const SearchInput = blockComponent(undefined, Component);

function Component({ ...blockProps }: IProps) {
  return <InputHTML {...blockProps} type="search" />;
}
