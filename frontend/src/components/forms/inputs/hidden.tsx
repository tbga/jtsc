import { InputHTML, IInputHTMLProps } from "./html";

import { blockComponent } from "#components/meta";

interface IHiddenProps extends Omit<IInputHTMLProps, "type"> {}

export const HiddenInput = blockComponent(undefined, Component);

function Component({ ...blockProps }: IHiddenProps) {
  return <InputHTML {...blockProps} type="hidden" />;
}
