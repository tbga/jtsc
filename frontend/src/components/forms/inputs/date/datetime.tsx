import { forwardRef, type Ref } from "react";
import { blockComponent } from "#components/meta";
import { validateDateTime } from "#lib/dates";
import { type IInputHTMLProps, InputHTML } from "../html";

interface IProps extends Omit<IInputHTMLProps, "type"> {}

export const DateTimeInput = forwardRef<HTMLInputElement, IProps>(
  blockComponent(undefined, Component),
);

function Component(
  { onChange, ...blockProps }: IProps,
  ref?: Ref<HTMLInputElement>,
) {
  return (
    <InputHTML
      {...blockProps}
      onChange={(event) => {
        const value = event.currentTarget.value;
        validateDateTime(value);
        onChange?.(event);
      }}
      type="text"
      ref={ref}
    />
  );
}
