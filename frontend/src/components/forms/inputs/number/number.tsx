import { type CSSProperties, forwardRef, type Ref } from "react";
import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "../html";

import styles from "./number.module.scss";

/**
 * `minLength` and `maxLength` are excluded because the width
 * can be derived from `min` and `max`.
 */
export interface INumberInputProps
  extends Omit<
    IInputHTMLProps,
    "type" | "min" | "max" | "style" | "minLength" | "maxLength"
  > {
  min?: number;
  max?: number;
}

export const NumberInput = forwardRef<HTMLInputElement, INumberInputProps>(
  blockComponent(styles.block, Component),
);

function Component(
  { min, max, ...blockProps }: INumberInputProps,
  ref?: Ref<HTMLInputElement>,
) {
  const biggestValue = Math.max(min ?? 0, max ?? 0);
  const maxWidth =
    biggestValue === 0 ? undefined : biggestValue.toString().length + 1.5;

  return (
    <InputHTML
      ref={ref}
      type="number"
      min={min}
      max={max}
      style={
        !maxWidth
          ? undefined
          : ({ "--local-max-width": `${maxWidth}em` } as CSSProperties)
      }
      {...blockProps}
    />
  );
}
