import { forwardRef, type Ref } from "react";
import { blockComponent } from "#components/meta";
import { type INumberInputProps, NumberInput } from "./number";

interface IIntegerInputProps extends Omit<INumberInputProps, "step" | "min"> {}

export const IntegerInput = forwardRef<HTMLInputElement, INumberInputProps>(
  blockComponent(undefined, Component),
);

function Component(
  { ...blockProps }: IIntegerInputProps,
  ref?: Ref<HTMLInputElement>,
) {
  return <NumberInput {...blockProps} step={1} min={1} ref={ref} />;
}
