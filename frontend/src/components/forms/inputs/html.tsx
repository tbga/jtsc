import { forwardRef, type Ref } from "react";
import { blockComponent, type IChildlessBlockProps } from "#components/meta";
import { type IRequiredSome } from "#types";

import styles from "./html.module.scss";

export interface IInputHTMLProps
  extends IRequiredSome<
    IChildlessBlockProps<"input">,
    "id" | "name" | "form"
  > {}

/**
 * @TODO
 * Make `id` optional and derive its value from `form` and `name`
 */
export const InputHTML = forwardRef<HTMLInputElement, IInputHTMLProps>(
  blockComponent(styles.block, Component),
);

function Component(
  { ...blockProps }: IInputHTMLProps,
  ref?: Ref<HTMLInputElement>,
) {
  return <input {...blockProps} ref={ref} />;
}
