import { forwardRef, type Ref } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { type IRequiredSome } from "#types";

import styles from "./select.module.scss";

export interface ISelectInputProps
  extends IRequiredSome<IBlockProps<"select">, "id" | "name" | "form"> {}
export interface ISelectOptionProps extends IBlockProps<"option"> {
  value: string | number;
}

export const SelectInput = forwardRef<HTMLSelectElement, ISelectInputProps>(
  blockComponent(styles.block, Component),
);

export const SelectOption = blockComponent(styles.option, OptionComponent);

function Component(
  { ...blockProps }: ISelectInputProps,
  ref?: Ref<HTMLSelectElement>,
) {
  return <select {...blockProps} ref={ref} />;
}

function OptionComponent({ ...blockProps }: ISelectOptionProps) {
  return <option {...blockProps} />;
}
