import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "./html";

interface IRadioProps extends Omit<IInputHTMLProps, "type"> {}

export const RadioInput = blockComponent(undefined, Component);

function Component({ ...blockProps }: IRadioProps) {
  return <InputHTML {...blockProps} type="radio" />;
}
