import { blockComponent } from "#components/meta";
import { type IInputHTMLProps, InputHTML } from "./html";

export interface IFileInputProps extends Omit<IInputHTMLProps, "type"> {}

export const FileInput = blockComponent(undefined, Component);

function Component({ ...blockProps }: IFileInputProps) {
  return <InputHTML {...blockProps} type="file" />;
}
