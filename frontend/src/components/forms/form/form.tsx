import { blockComponent } from "#components/meta";
import { FormHTML, type IFormHTMLProps } from "./html";
import { FormClient, type IFormClientProps } from "./client";

/**
 * @TODO less heavy-handed discrimination
 */
export type IFormProps<
  FieldNames extends string = string,
  InitType extends unknown = unknown,
  ResultType extends unknown = unknown,
> =
  | ({ isClient?: false } & IFormHTMLProps)
  | ({ isClient: true } & IFormClientProps<FieldNames, InitType, ResultType>);

export const Form = blockComponent(undefined, Component);

function Component<
  FieldNames extends string = string,
  InitType extends unknown = unknown,
  ResultType extends unknown = unknown,
>(props: IFormProps<FieldNames, InitType, ResultType>) {
  if (props.isClient) {
    const { isClient, ...newProps } = props;

    return <FormClient {...newProps} />;
  }

  return <FormHTML {...props} />;
}
