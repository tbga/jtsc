import { ReactNode, useState } from "react";
import { defineProperty } from "#lib/std";
import { blockComponent, ClientComponent } from "#components/meta";
import { LoadingBar } from "#components";
import { ButtonReset, ButtonSubmit } from "#components/buttons";
import { Item, List } from "#components/lists";
import { FormSection, type ISubmitEvent } from "#components/forms";
import { FormHTML, type IFormHTMLProps } from "./html";

export interface IFormClientProps<
  FieldNames extends string = string,
  InitType extends unknown = unknown,
  ResultType extends unknown = unknown,
> extends IFormHTMLProps {
  /**
   * The list field names.
   */
  fieldNames: readonly FieldNames[];
  /**
   * Collect an initializer from form fields.
   * All values are trimmed beforehand and falsy values are skipped.
   * @param init An initiaizer to mutate.
   * @param fieldName The name of the current field.
   */
  collectInit?: (init: InitType, fieldName: FieldNames, value: string) => void;
  /**
   * Validate collected initializer.
   */
  validateInit?: (input: unknown) => input is InitType;
  /**
   * Get result using collected initializer.
   */
  getResult: (init: InitType) => Promise<ResultType>;
  /**
   * Control result view. Pass `null` to not render it at all.
   */
  resultView?: null | ((formID: string, result: ResultType) => ReactNode);
  resetButton?: null | string;
  onReset?: (event: ISubmitEvent<FieldNames>) => Promise<void>;
}

export const FormClient = blockComponent(undefined, Component);

function Component<
  FieldNames extends string,
  InitType extends unknown,
  ResultType extends unknown,
>({
  fieldNames,
  collectInit = collectInitDefault,
  validateInit,
  getResult,
  resultView,
  submitButton,
  resetButton,
  onReset,
  children,
  isNested,
  ...blockProps
}: IFormClientProps<FieldNames, InitType, ResultType>) {
  const [isSubmitting, switchSubmitting] = useState(false);
  const [errors, changeErrors] = useState<string[]>([]);
  const [result, changeResult] = useState<ResultType>();

  async function handleSubmit(event: ISubmitEvent<FieldNames>) {
    // prevent form submit
    event.preventDefault();

    // do not submit if the current submit is in progress
    if (isSubmitting) {
      return;
    }

    // handle submit
    try {
      switchSubmitting(true);

      const elements = event.currentTarget.elements;

      // collect init from fields
      const init = fieldNames.reduce((init, fieldName) => {
        const value = elements[fieldName].value.trim();

        if (!value) {
          return init;
        }

        collectInit(init, fieldName, value);

        return init;
      }, {} as InitType);

      // validate if provided
      if (validateInit) {
        validateInit(init);
      }

      // get result
      const result = await getResult(init);
      changeResult(result);

      if (errors.length) {
        changeErrors([]);
      }
    } catch (error) {
      changeErrors([String(error)]);
    } finally {
      switchSubmitting(false);
    }
  }

  async function handleReset(event: ISubmitEvent<FieldNames>) {
    if (isSubmitting) {
      return;
    }

    try {
      switchSubmitting(true);
      changeErrors([]);
      changeResult(undefined);

      if (onReset) {
        await onReset(event);
      }
    } finally {
      switchSubmitting(false);
    }
  }

  return (
    <ClientComponent>
      <FormHTML
        submitButton={null}
        onSubmit={handleSubmit}
        onReset={handleReset}
        isNested={isNested}
        {...blockProps}
      >
        {(formID) =>
          resultView !== null && result ? (
            <>
              {resultView?.(formID, result) ?? "Successfully submitted."}
              {resetButton !== null && (
                <FormSection>
                  <ButtonReset form={formID} disabled={isSubmitting}>
                    {resetButton}
                  </ButtonReset>
                </FormSection>
              )}
            </>
          ) : (
            <>
              {children?.(formID)}
              <Status isSubmitting={isSubmitting} errors={errors} />
              {submitButton !== null && (
                <FormSection>
                  <ButtonSubmit
                    form={formID}
                    visualType={isNested ? "button" : "submit"}
                    disabled={isSubmitting}
                  >{submitButton}</ButtonSubmit>
                </FormSection>
              )}
            </>
          )
        }
      </FormHTML>
    </ClientComponent>
  );
}

interface IStatusProps {
  isSubmitting: boolean;
  errors: string[];
}

function Status({ isSubmitting, errors }: IStatusProps) {
  const isErrored = Boolean(errors.length);

  return (
    <FormSection>
      {isSubmitting ? (
        <LoadingBar>Submit is in progress...</LoadingBar>
      ) : (
        isErrored && <Errors errors={errors} />
      )}
    </FormSection>
  );
}

function Errors({ errors }: { errors: string[] }) {
  return (
    <List>
      {errors.map((message, index) => (
        <Item key={index}>{message}</Item>
      ))}
    </List>
  );
}

function collectInitDefault<
  FieldNames extends string,
  InitType extends unknown,
>(init: InitType, fieldName: FieldNames, value: string) {
  defineProperty(init as Record<string, unknown>, fieldName, {
    enumerable: true,
    value,
  });
}
