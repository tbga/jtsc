import clsx from "clsx";
import { ReactNode } from "react";
import { blockComponent, type IChildlessBlockProps } from "#components/meta";
import { FormSection } from "#components/forms";
import { ButtonSubmit } from "#components/buttons";

import styles from "./html.module.scss";

interface IBaseProps
  extends Omit<IChildlessBlockProps<"div">, "onSubmit" | "onReset">,
    Pick<IChildlessBlockProps<"form">, "onSubmit" | "onReset"> {}

export interface IFormHTMLProps extends IBaseProps {
  /**
   * ID of the root element.
   */
  id: string;
  /**
   * Form ID constructor.
   */
  constructFormID?: typeof constructFormIDDefault;
  isNested?: boolean;

  /**
   * Configure the submit button.
   * If `null` is passed, no button will be rendered.
   */
  submitButton?: null | string;
  children?: (formID: string) => ReactNode;
}

/**
 * A baseline form component.
 * Must be server-side friendly,
 * therefore has no state and handlers are optional.
 */
export const FormHTML = blockComponent(styles.block, Component);

function Component({
  id,
  constructFormID = constructFormIDDefault,
  isNested,
  submitButton,
  onSubmit,
  onReset,
  className,
  children,
  ...blockProps
}: IFormHTMLProps) {
  const finalClassname = clsx(className, isNested && styles.nested);
  const formID = constructFormID(id);

  return (
    <div className={finalClassname} {...blockProps}>
      {children?.(formID)}

      {submitButton !== null && (
        <FormSection>
          <ButtonSubmit
            form={formID}
            visualType={isNested ? "button" : "submit"}
          >{submitButton}</ButtonSubmit>
        </FormSection>
      )}

      <form
        id={formID}
        className={styles.form}
        onSubmit={onSubmit}
        onReset={onReset}
      />
    </div>
  );
}

function constructFormIDDefault(id: string) {
  return `${id}-form`;
}
