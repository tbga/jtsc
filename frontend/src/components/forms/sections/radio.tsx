import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { RadioInput } from "#components/forms/inputs";
import { type IInputSectionProps } from "./types";

import styles from "./radio.module.scss";

export interface IRadioSectionProps extends IInputSectionProps {
  defaultChecked?: boolean;
}

export const RadioSection = blockComponent(styles.block, Component);

function Component({
  id,
  name,
  form,
  defaultValue,
  defaultChecked,
  disabled,
  children,
  ...blockProps
}: IRadioSectionProps) {
  return (
    <FormSection {...blockProps}>
      <RadioInput
        id={id}
        form={form}
        className={styles.input}
        name={name}
        defaultValue={defaultValue}
        defaultChecked={defaultChecked}
        disabled={disabled}
      />
      <Label className={styles.label} htmlFor={id}>
        {children}
      </Label>
    </FormSection>
  );
}
