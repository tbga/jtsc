import { type ReactNode } from "react";
import { blockComponent } from "#components/meta";
import { Fieldset, Legend, type IFieldsetProps } from "#components/forms";
import { RadioSection, type IRadioSectionProps } from "./radio";

import styles from "./radio-group.module.scss";

export interface IRadioGroupProps extends IFieldsetProps {
  name: string;
  form: string;
  legend: ReactNode;
  options: IRadioOption[];
}

interface IRadioOption
  extends Required<Pick<IRadioSectionProps, "id" | "defaultValue">>,
    Pick<IRadioSectionProps, "defaultChecked" | "disabled"> {
  title: ReactNode;
}

export const RadioGroup = blockComponent(styles.block, Component);

function Component({
  name,
  form,
  legend,
  options,
  children,
  ...blockProps
}: IRadioGroupProps) {
  return (
    <Fieldset {...blockProps}>
      <Legend>{legend}</Legend>
      {options.map(({ id, title, defaultValue, defaultChecked, disabled }) => (
        <RadioSection
          key={id}
          id={id}
          form={form}
          name={name}
          defaultValue={defaultValue}
          defaultChecked={defaultChecked}
          disabled={disabled}
        >
          {title}
        </RadioSection>
      ))}
      <div>{children}</div>
    </Fieldset>
  );
}
