import { type ReactNode, type Ref, forwardRef } from "react";
import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { ITextAreaProps, TextArea, TextInput } from "#components/forms/inputs";
import { type IInputSectionProps } from "./types";

interface ITextProps
  extends IInputSectionProps,
    Pick<ITextAreaProps, "rows" | "minLength" | "maxLength"> {
  label: ReactNode;
}

export type TextSectionRef = HTMLInputElement | HTMLTextAreaElement;

export const TextSection = forwardRef<TextSectionRef, ITextProps>(
  blockComponent(undefined, Component),
);

function Component(
  {
    id,
    form,
    name,
    label,
    defaultValue,
    readOnly,
    required,
    disabled,
    minLength,
    maxLength,
    rows,
    children,
    ...blockProps
  }: ITextProps,
  ref?: Ref<TextSectionRef>,
) {
  const isShort = Boolean(maxLength && maxLength < 26);

  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      {isShort ? (
        <TextInput
          id={id}
          form={form}
          name={name}
          required={required}
          readOnly={readOnly}
          disabled={disabled}
          minLength={minLength}
          maxLength={maxLength}
          defaultValue={defaultValue}
          ref={ref as Ref<HTMLInputElement>}
        />
      ) : (
        <TextArea
          id={id}
          form={form}
          name={name}
          required={required}
          readOnly={readOnly}
          disabled={disabled}
          minLength={minLength}
          maxLength={maxLength}
          defaultValue={defaultValue}
          rows={rows}
          ref={ref as Ref<HTMLTextAreaElement>}
        />
      )}
      {children && <div>{children}</div>}
    </FormSection>
  );
}
