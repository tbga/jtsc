import { type IFormSectionProps } from "#components/forms";
import { type IInputHTMLProps } from "#components/forms/inputs";

export interface IInputSectionProps
  extends Omit<IFormSectionProps, "id">,
    Pick<
      IInputHTMLProps,
      "name" | "required" | "defaultValue" | "readOnly" | "disabled"
    >,
    Pick<Required<IInputHTMLProps>, "id" | "form"> {}
