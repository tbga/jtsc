import { blockComponent } from "#components/meta";
import { NumberSection, type IInputNumberProps } from "./number";

interface IIntegerProps extends Omit<IInputNumberProps, "step" | "min"> {}

export const IntegerSection = blockComponent(undefined, Component);

function Component({ ...blockProps }: IIntegerProps) {
  return <NumberSection {...blockProps} step={1} min={1} />;
}
