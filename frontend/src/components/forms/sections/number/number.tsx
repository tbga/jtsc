import { type ReactNode } from "react";
import { FormSection, Label } from "#components/forms";
import { blockComponent } from "#components/meta";
import { NumberInput } from "#components/forms/inputs";
import { type IInputSectionProps } from "../types";

import styles from "./number.module.scss";

export interface IInputNumberProps extends IInputSectionProps {
  min?: number;
  max?: number;
  step?: number;
  label: ReactNode;
}

export const NumberSection = blockComponent(styles.block, Component);

function Component({
  id,
  form,
  name,
  label,
  defaultValue,
  min,
  max,
  step,
  children,
  ...blockProps
}: IInputNumberProps) {
  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      <NumberInput
        id={id}
        form={form}
        name={name}
        min={min}
        max={max}
        step={step}
        defaultValue={defaultValue}
      />
      {children && <div>{children}</div>}
    </FormSection>
  );
}
