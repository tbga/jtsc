import { type ReactNode } from "react";
import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import {
  PasswordInput,
  type IPasswordInputProps,
} from "#components/forms/inputs";
import { type IInputSectionProps } from "./types";

interface IProps
  extends IInputSectionProps,
    Pick<IPasswordInputProps, "autoComplete" | "minLength" | "maxLength"> {
  label: ReactNode;
}

export const PasswordSection = blockComponent(undefined, Component);

function Component({
  id,
  form,
  name,
  label,
  required,
  readOnly,
  disabled,
  minLength,
  maxLength,
  autoComplete,
  defaultValue,
  children,
  ...blockProps
}: IProps) {
  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      <PasswordInput
        id={id}
        form={form}
        name={name}
        required={required}
        readOnly={readOnly}
        disabled={disabled}
        minLength={minLength}
        maxLength={maxLength}
        autoComplete={autoComplete}
        defaultValue={defaultValue}
      />
      {children && <div>{children}</div>}
    </FormSection>
  );
}
