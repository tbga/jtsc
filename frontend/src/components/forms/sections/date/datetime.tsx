import { useRef, type ReactNode } from "react";
import { type IDateTime, todayDateTime } from "#lib/dates";
import { blockComponent } from "#components/meta";
import {
  Fieldset,
  type IFieldsetProps,
  Legend,
  DateTimeForm,
  FormSection,
} from "#components/forms";
import { DateTimeInput, type IInputHTMLProps } from "#components/forms/inputs";
import { DateTime } from "#components/dates";

import styles from "./datetime.module.scss";

interface IProps
  extends Omit<IFieldsetProps, "form" | "id" | "name">,
    Pick<IInputHTMLProps, "form" | "id" | "name"> {
  label: ReactNode;
  defaultValue?: IDateTime;
  onDateTimeChange?: (newDateTime: IDateTime) => Promise<void>;
}

/**
 * @TODO timezone input
 */
export const DateTimeSection = blockComponent(styles.block, Component);

function Component({
  id,
  form,
  name,
  label,
  defaultValue,
  onDateTimeChange,
  children,
  ...blockProps
}: IProps) {
  const dateRef = useRef<HTMLInputElement>(null);
  const dateTime = defaultValue ?? todayDateTime();

  return (
    <Fieldset {...blockProps}>
      <Legend>{label}</Legend>

      <DateTimeForm
        // @TODO coordinate with default value without flickering
        key={dateTime}
        id={id}
        isNested
        defaultValue={dateTime}
        onNewDate={async (newDate) => {
          if (dateRef.current) {
            dateRef.current.value = newDate;
          }

          onDateTimeChange?.(newDate);
        }}
      />

      <FormSection className={styles.view}>
        <DateTime dateTime={defaultValue} />
      </FormSection>

      {children}

      <DateTimeInput
        ref={dateRef}
        id={id}
        className={styles.input}
        form={form}
        name={name}
        defaultValue={defaultValue}
      />
    </Fieldset>
  );
}
