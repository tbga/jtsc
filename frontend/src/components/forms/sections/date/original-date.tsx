import clsx from "clsx";
import { useRef, type ReactNode, useState } from "react";
import { toDateTime, type IDateTime, todayDateTime } from "#lib/dates";
import { blockComponent } from "#components/meta";
import { Button } from "#components/buttons";
import { FormSection } from "../../section";
import { TextSection, TextSectionRef } from "../text";
import { DateTimeSection } from "../date/datetime";
import type { IInputSectionProps } from "../types";

import styles from "./original-date.module.scss";

interface IProps extends Omit<IInputSectionProps, " children"> {
  label: ReactNode;
  timestampInputID: string;
  timestampInputName: string;
  timestampInputLabel: string;
  timestampInputChildren?: ReactNode;
  parsedDateTime?: IDateTime;
}

export const OriginalDateSection = blockComponent(styles.block, Component);

function Component({
  id,
  form,
  name,
  label,
  required,
  readOnly,
  disabled,
  defaultValue,
  children,
  timestampInputID,
  timestampInputName,
  timestampInputLabel,
  timestampInputChildren,
  parsedDateTime,
  ...blockProps
}: IProps) {
  const [currentDateTime, changeCurrentDateTime] = useState(parsedDateTime);
  const originalRef = useRef<TextSectionRef>(null);
  const dateTimeSectionClassName = clsx(!currentDateTime && styles.hidden);

  return (
    <FormSection {...blockProps}>
      <TextSection
        ref={originalRef}
        id={id}
        form={form}
        name={name}
        label={label}
        defaultValue={defaultValue}
      >
        {children && <div>{children}</div>}
        <Button
          onClick={async () => {
            if (!originalRef.current) {
              return;
            }

            const dateString = originalRef.current.value.trim();

            if (!dateString) {
              return;
            }

            const parsedDate = Date.parse(dateString);
            // assign today timestamp if the value isn't parsable
            const dateTime = Number.isNaN(parsedDate)
              ? todayDateTime()
              : toDateTime(new Date(parsedDate));
            changeCurrentDateTime(dateTime);
          }}
        >
          Parse
        </Button>
      </TextSection>

      <DateTimeSection
        id={timestampInputID}
        className={dateTimeSectionClassName}
        form={form}
        name={timestampInputName}
        label={timestampInputLabel}
        defaultValue={currentDateTime}
        onDateTimeChange={async (newDateTime) =>
          changeCurrentDateTime(newDateTime)
        }
      >
        {timestampInputChildren}
      </DateTimeSection>
    </FormSection>
  );
}
