import type { ReactNode } from "react";
import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import {
  ISelectInputProps,
  ISelectOptionProps,
  SelectInput,
  SelectOption,
} from "#components/forms/inputs";
import type { IInputSectionProps } from "./types";

interface ISelectSectionProps
  extends IInputSectionProps,
    Pick<ISelectInputProps, "multiple" | "size"> {
  label: ReactNode;
  options: { value: ISelectOptionProps["value"]; content: ReactNode }[];
}

export const SelectSection = blockComponent(undefined, Component);

function Component({
  id,
  name,
  form,
  label,
  options,
  defaultValue,
  required,
  disabled,
  multiple,
  size,
  children,
  ...blockProps
}: ISelectSectionProps) {
  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      <SelectInput
        id={id}
        form={form}
        name={name}
        required={required}
        defaultValue={defaultValue}
        disabled={disabled}
        multiple={multiple}
        size={size}
      >
        {options.map(({ value, content }) => (
          <SelectOption key={value} value={value}>
            {content}
          </SelectOption>
        ))}
      </SelectInput>
      {children && <div>{children}</div>}
    </FormSection>
  );
}
