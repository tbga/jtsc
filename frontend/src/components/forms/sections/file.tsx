import { useState } from "react";
import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { FileInput, IFileInputProps } from "#components/forms/inputs";
import { Item, List } from "#components/lists";
import { type IInputSectionProps } from "./types";

import styles from "./file.module.scss";

interface IProps
  extends IInputSectionProps,
    Pick<IFileInputProps, "accept" | "multiple" | "capture"> {}

export const FileSection = blockComponent(styles.block, Component);

function Component({
  id,
  form,
  name,
  accept,
  multiple,
  capture,
  children,
  ...blockProps
}: IProps) {
  const [files, changeFiles] = useState<File[]>([]);

  return (
    <FormSection {...blockProps}>
      <Label className={styles.label} htmlFor={id}>
        {children}
      </Label>
      <FileInput
        id={id}
        form={form}
        className={styles.input}
        name={name}
        accept={accept}
        multiple={multiple}
        capture={capture}
        onChange={async (event) => {
          const files = event.target.files ?? [];
          changeFiles(Array.from(files));
        }}
      />
      <List>
        {!files.length ? (
          <Item>No files are selected for upload</Item>
        ) : (
          files.map(({ name, type, size }, index) => (
            <Item key={index}>
              {name} ({type}) - {size}
            </Item>
          ))
        )}
      </List>
    </FormSection>
  );
}
