import { type ReactNode } from "react";
import { blockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { type IInputSectionProps } from "./types";
import { SearchInput } from "#components/forms/inputs";
import { type IPartialSome } from "#types";

interface ISearchProps extends IPartialSome<IInputSectionProps, "name"> {
  minLength?: number;
  maxLength?: number;
  label?: ReactNode;
}

export const SearchSection = blockComponent(undefined, Component);

function Component({
  id,
  form,
  label = "Query",
  name = "query",
  required,
  readOnly,
  disabled,
  minLength = 1,
  maxLength,
  defaultValue,
  children,
  ...blockProps
}: ISearchProps) {
  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      <SearchInput
        id={id}
        form={form}
        name={name}
        required={required}
        readOnly={readOnly}
        disabled={disabled}
        minLength={minLength}
        maxLength={maxLength}
        defaultValue={defaultValue}
      />
      <div>{children}</div>
    </FormSection>
  );
}
