import { ProjectError } from "#lib/errors";
import {
  getDate,
  getHours,
  getMilliseconds,
  getMinutes,
  getMonth,
  getSeconds,
  getTimezone,
  getYear,
  ZERO_DATETIME,
  type IDateTime,
} from "#lib/dates";
import { blockComponent } from "#components/meta";
import { ButtonSubmit } from "#components/buttons";
import { Form } from "../form/form";
import { type IFormClientProps } from "../form/client";
import { FormSection } from "../section";
import { NumberSection } from "../sections/number/number";
import { TextSection } from "../sections/text";
import { SelectSection } from "../sections/select";

import styles from "./datetime.module.scss";

const FORM_FIELDS = {
  YEAR: {
    name: "year",
    label: "Year",
  },
  MONTH: {
    name: "month",
    label: "Month",
  },
  DAY: {
    name: "day",
    label: "Day",
  },
  HOUR: {
    name: "hour",
    label: "Hour",
  },
  MINUTE: {
    name: "minute",
    label: "Minute",
  },
  SECOND: {
    name: "second",
    label: "Second",
  },
  MILLISECOND: {
    name: "millisecond",
    label: "Millisecond",
  },
  TIMEZONE: {
    name: "timezone",
    label: "Timezone",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];
interface IInitType extends Record<IFieldName, string> {}
interface IBaseProps
  extends IFormClientProps<IFieldName, IInitType, IDateTime> {}

type IDateFormProps = Omit<
  IBaseProps,
  "isClient" | "fieldNames" | "collectInit" | "validateInit" | "getResult"
> & {
  defaultValue?: IDateTime;
  onNewDate: (date: IDateTime) => Promise<void>;
};

/**
 * @TODO locale-specific formatting
 */
const months = [
  { long: "January", short: "Jan." },
  { long: "February", short: "Feb." },
  { long: "March", short: "Mar." },
  { long: "April", short: "Apr." },
  { long: "May", short: "May" },
  { long: "June", short: "June" },
  { long: "July", short: "July" },
  { long: "August", short: "Aug." },
  { long: "September", short: "Sept." },
  { long: "October", short: "Oct." },
  { long: "November", short: "Nov." },
  { long: "December", short: "Dec." },
];

/**
 * @TODOs
 * - fix `onReset()` type
 * - month input select
 * - day limit on per month
 */
export const DateTimeForm = blockComponent(styles.block, Component);

function Component({
  id,
  defaultValue = ZERO_DATETIME,
  onNewDate,
  onReset,
  ...blockProps
}: IDateFormProps) {
  return (
    <Form<IFieldName, IInitType, IDateTime>
      id={`${id}-date-picker`}
      isClient
      fieldNames={FIELD_NAMES}
      submitButton={"Update"}
      resultView={null}
      onReset={
        !onReset
          ? undefined
          : async (event) => {
              onReset(event);
            }
      }
      collectInit={collectInit}
      getResult={async (date) => {
        const {
          year,
          month,
          day,
          hour,
          minute,
          second,
          millisecond,
          timezone,
        } = date;
        const dateTimeString = `${year}-${month}-${day}T${hour}:${minute}:${second}.${millisecond}${timezone}`;

        onNewDate(dateTimeString);

        return dateTimeString;
      }}

      {...blockProps}
    >
      {(formID) => (
        <>
          <FormSection className={styles.section}>
            <NumberSection
              id={`${formID}-${FORM_FIELDS.YEAR.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.YEAR.label}
              name={FORM_FIELDS.YEAR.name}
              min={0}
              max={9999}
              step={1}
              defaultValue={getYear(defaultValue)}
            />

            <SelectSection
              id={`${formID}-${FORM_FIELDS.MONTH.name}`}
              className={styles.subsection}
              form={formID}
              name={FORM_FIELDS.MONTH.name}
              label={FORM_FIELDS.MONTH.label}
              defaultValue={getMonth(defaultValue) + 1}
              options={months.map(({ long, short }, index) => {
                return {
                  value: index + 1,
                  content: `${index + 1}. ${long} (${short})`,
                };
              })}
            />

            <NumberSection
              id={`${formID}-${FORM_FIELDS.DAY.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.DAY.label}
              name={FORM_FIELDS.DAY.name}
              min={1}
              max={31}
              // max={getDaysInMonth(defaultValue)}
              step={1}
              defaultValue={getDate(defaultValue)}
            />
          </FormSection>

          <FormSection className={styles.section}>
            <NumberSection
              id={`${formID}-${FORM_FIELDS.HOUR.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.HOUR.label}
              name={FORM_FIELDS.HOUR.name}
              min={0}
              max={23}
              step={1}
              defaultValue={getHours(defaultValue)}
            />
            <NumberSection
              id={`${formID}-${FORM_FIELDS.MINUTE.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.MINUTE.label}
              name={FORM_FIELDS.MINUTE.name}
              min={0}
              max={59}
              step={1}
              defaultValue={getMinutes(defaultValue)}
            />

            <NumberSection
              id={`${formID}-${FORM_FIELDS.SECOND.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.SECOND.label}
              name={FORM_FIELDS.SECOND.name}
              min={0}
              max={59}
              step={1}
              defaultValue={getSeconds(defaultValue)}
            />

            <NumberSection
              id={`${formID}-${FORM_FIELDS.MILLISECOND.name}`}
              className={styles.subsection}
              form={formID}
              label={FORM_FIELDS.MILLISECOND.label}
              name={FORM_FIELDS.MILLISECOND.name}
              min={0}
              max={999}
              step={1}
              defaultValue={getMilliseconds(defaultValue)}
            />
          </FormSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.TIMEZONE.name}`}
            className={styles.timezone}
            form={formID}
            label={FORM_FIELDS.TIMEZONE.label}
            name={FORM_FIELDS.TIMEZONE.name}
            minLength={1}
            maxLength={6}
            defaultValue={getTimezone(defaultValue)}
          />
        </>
      )}
    </Form>
  );
}

function collectInit(init: IInitType, fieldName: IFieldName, value: string) {
  switch (fieldName) {
    case FORM_FIELDS.TIMEZONE.name: {
      const [hour, minute] = value.split(":");
      const timeZone = `${hour.padStart(2, "0")}:${minute.padStart(2, "0")}`;
      init[fieldName] = timeZone;

      break;
    }

    case FORM_FIELDS.MONTH.name:
    case FORM_FIELDS.DAY.name:
    case FORM_FIELDS.HOUR.name:
    case FORM_FIELDS.MINUTE.name:
    case FORM_FIELDS.SECOND.name: {
      const parsedValue = value.padStart(2, "0");
      init[fieldName] = parsedValue;

      break;
    }

    case FORM_FIELDS.YEAR.name: {
      const year = value.padStart(4, "0");
      init[fieldName] = year;

      break;
    }

    case FORM_FIELDS.MILLISECOND.name: {
      const milliseconds = value.padStart(3, "0");
      init[fieldName] = milliseconds;

      break;
    }

    default: {
      throw new ProjectError(`Unknown key "${fieldName satisfies never}".`);
    }
  }
}
