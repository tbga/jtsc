import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./section.module.scss";

export interface IFormSectionProps extends IBlockProps<"div"> {}

/**
 * Generic separator between form inputs.
 */
export const FormSection = blockComponent(styles.block, Component);

function Component({ children, ...blockProps }: IFormSectionProps) {
  return <div {...blockProps}>{children}</div>;
}
