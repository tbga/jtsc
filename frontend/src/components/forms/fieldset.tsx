import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./fieldset.module.scss";

export interface IFieldsetProps extends IBlockProps<"fieldset"> {}
export interface ILegendProps extends IBlockProps<"legend"> {}

export const Fieldset = blockComponent(styles.block, FieldsetComponent);
export const Legend = blockComponent(styles.legend, LegendComponent);

function FieldsetComponent({ children, ...blockProps }: IFieldsetProps) {
  return <fieldset {...blockProps}>{children}</fieldset>;
}

function LegendComponent({ children, ...blockProps }: ILegendProps) {
  return <legend {...blockProps}>{children}</legend>;
}
