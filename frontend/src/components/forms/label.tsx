import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./label.module.scss";

export interface ILabelProps extends IBlockProps<"label"> {
  htmlFor: string;
}

export const Label = blockComponent(styles.block, Component);

function Component({ children, ...blockProps }: ILabelProps) {
  return <label {...blockProps}>{children}</label>;
}
