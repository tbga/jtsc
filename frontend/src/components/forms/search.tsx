import { ReactNode, useState } from "react";
import { validateNonEmptyString } from "#lib/strings";
import { type IPagination } from "#lib/pagination";
import { ProjectError } from "#lib/errors";
import { Details } from "#components";
import { blockComponent } from "#components/meta";
import { Button } from "#components/buttons";
import { Form } from "#components/forms";
import { SearchSection } from "#components/forms/sections";
import { BIGINT_ZERO } from "#types";
import { FormSection } from "./section";
import { type IFormClientProps } from "./form/client";

import styles from "./search.module.scss";

const FIELD_NAMES = ["query"] as const;
type IFieldName = (typeof FIELD_NAMES)[number];

export type ISearchFormProps<
  FieldNames extends Exclude<string, IFieldName> = string,
  InitType extends unknown = unknown,
  ResultType extends unknown = unknown,
> = {
  /**
   * A function to fetch pagination for the search.
   */
  paginationFetch: (init: InitType) => Promise<IPagination>;
  getResult: (init: InitType) => Promise<ResultType>;
  resultView?: ((result: ResultType) => ReactNode) | null;
  /**
   * A component which always be shown.
   */
  requiredExtra?: (formID: string) => ReactNode;
  /**
   * A component which will be hidden behind `"More"` button.
   */
  optionalExtra?: (formID: string) => ReactNode;
  fieldNames?: readonly FieldNames[];
} & Omit<IFormClientProps<FieldNames, InitType, ResultType>, "fieldNames">;

export const SearchForm = blockComponent(styles.block, Component);

function Component<
  FieldNames extends Exclude<string, IFieldName> = string,
  InitType extends { query: string } = { query: string },
  ResultType extends unknown = unknown,
>({
  id,
  constructFormID,
  fieldNames,
  collectInit,
  validateInit,
  getResult,
  paginationFetch,
  requiredExtra,
  optionalExtra,
  resultView,
  defaultValue,
  ...blockProps
}: ISearchFormProps<FieldNames, InitType, ResultType>) {
  const [currentResult, changeCurrentResult] = useState<ResultType>();
  const finalNames = !fieldNames
    ? FIELD_NAMES
    : // @TODO a way to infer the final array
      ([...FIELD_NAMES, ...fieldNames] as readonly (FieldNames | IFieldName)[]);
  type IFinalName = (typeof finalNames)[number];

  function handleInitCollection(
    init: InitType,
    key: IFinalName,
    value: string,
  ) {
    if (key === "query") {
      validateNonEmptyString(value);
      init.query = value;
    }

    collectInit?.(
      init,
      // @ts-expect-error @TODO extendable dynamic tuples somehow
      key,
      value,
    );
  }

  async function handleResultFetch(init: InitType) {
    validateInit?.(init);

    const pagination = await paginationFetch(init);

    const totalCount = BigInt(pagination.total_count);

    if (totalCount === BIGINT_ZERO) {
      throw new ProjectError("No entities found for this query.");
    }

    const result = await getResult?.(init);
    changeCurrentResult(result);
    return result;
  }

  return (
    <>
      <Form<IFinalName, { query: string } & InitType, ResultType>
        isClient
        id={id}
        fieldNames={finalNames}
        collectInit={handleInitCollection}
        getResult={handleResultFetch}
        resultView={null}
        {...blockProps}
      >
        {(formID) => (
          <>
            <SearchSection
              id={`${formID}-search-query`}
              form={formID}
              required
              defaultValue={defaultValue}
            >
              Must include at least one character.
            </SearchSection>
            {requiredExtra && (
              <FormSection className={styles.required}>
                {requiredExtra(formID)}
              </FormSection>
            )}
            {optionalExtra && (
              <>
                <Details summary="More">
                  <FormSection className={styles.optional}>
                    {optionalExtra(formID)}
                  </FormSection>
                </Details>
              </>
            )}
          </>
        )}
      </Form>
      {resultView && currentResult && resultView(currentResult)}
    </>
  );
}
