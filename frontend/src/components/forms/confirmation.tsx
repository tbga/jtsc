import { useState } from "react";
import { Button, ButtonNegative, ButtonSubmit } from "#components/buttons";
import { blockComponent, IBlockProps } from "#components/meta";

interface IConfirmation extends IBlockProps<"div"> {
  form: string;
}

/**
 * @TODO finish it once nested forms are in place.
 */
const Confirmation = blockComponent(undefined, Component);

function Component({ form, children, ...blockProps }: IConfirmation) {
  const [isConfirming, switchConfirming] = useState(false);

  return (
    <div {...blockProps}>
      {!isConfirming ? (
        <Button onClick={() => switchConfirming(true)}>{children}</Button>
      ) : (
        <div>
          <p>Are you sure?</p>
          <ButtonNegative
            onClick={() => {
              switchConfirming(false);
            }}
          >
            No
          </ButtonNegative>
          <ButtonSubmit
            form={form}
            onClick={() => {
              switchConfirming(false);
            }}
          >
            Yes
          </ButtonSubmit>
        </div>
      )}
    </div>
  );
}
