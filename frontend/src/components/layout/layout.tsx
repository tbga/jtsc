import { type ReactNode } from "react";
import { REPO } from "#environment/variables";
import { Link } from "#components/links";
import { Item, List } from "#components/lists";
import { Header } from "./header";

import styles from "./layout.module.scss";

export interface ILayoutProps {
  children: ReactNode;
  header?: ReactNode;
  footer?: ReactNode;
}

export function Layout({ header, footer, children }: ILayoutProps) {
  return (
    <>
      {header ?? <Header />}

      <main className={styles.main}>{children}</main>

      {footer ?? (
        <footer className={styles.footer}>
          <nav>
            <List>
              <Item>
                <Link href={REPO}>Repository</Link>
              </Item>
            </List>
          </nav>
        </footer>
      )}
    </>
  );
}
