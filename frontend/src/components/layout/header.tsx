import clsx from "clsx";
import { useState } from "react";
import { TITLE } from "#environment/variables";
import { getTheme, setTheme, THEME } from "#lib/theme";
import { AccountURL, AdministratorURL, ProjectURL } from "#lib/urls";
import { useAccount, useLanguage } from "#hooks";
import { LoadingBar } from "#components";
import { LinkButton, LinkInternal } from "#components/links";
import { Item, List } from "#components/lists";
import { Button } from "#components/buttons";

import styles from "./header.module.scss";

export function Header() {
  const { language, isLoading } = useLanguage();
  const { isRegistered } = useAccount();

  return (
    <header className={styles.block}>
      <nav className={styles.nav}>
        <List className={styles.navList}>
          <Item>
            {isLoading ? (
              <LoadingBar />
            ) : (
              <LinkInternal href={new ProjectURL({ language })}>
                {TITLE}
              </LinkInternal>
            )}
          </Item>
        </List>

        {isRegistered && (
          <List className={styles.navList}>
            <Item>
              {isLoading ? (
                <LoadingBar />
              ) : (
                <LinkInternal
                  href={new AccountURL({ language, pathname: "/search" })}
                >
                  Search
                </LinkInternal>
              )}
            </Item>
          </List>
        )}

        <Account />
      </nav>
    </header>
  );
}

function Account() {
  const { language, isLoading: isLanguageLoading } = useLanguage();
  const { isReady, isRegistered, logout } = useAccount();
  const [isOpen, switchOpen] = useState(false);
  const accClass = clsx(styles.account, !isReady && styles.loading);
  const listClass = clsx(styles.list, isOpen && styles.open);
  const isLoading = !isReady || isLanguageLoading;

  function changeTheme() {
    const nextTheme = getTheme() === THEME.DARK ? THEME.LIGHT : THEME.DARK;

    setTheme(nextTheme);
  }

  return (
    <List className={styles.navList} isHorizontal>
      <Item className={accClass}>
        <Button
          className={styles.button}
          disabled={isLoading}
          onClick={async () => {
            if (isLoading) {
              return;
            }

            switchOpen((isOpen) => !isOpen);
          }}
        >
          {isLoading ? <LoadingBar /> : "Account"}
        </Button>
        <List className={listClass}>
          <Item>
            <Button
              className={styles.button}
              disabled={isLoading}
              onClick={changeTheme}
            >
              {isLoading ? <LoadingBar /> : "Theme"}
            </Button>
          </Item>
          {isLoading ? (
            <LoadingBar />
          ) : !isRegistered ? (
            <>
              <Item className={styles.item}>
                <LinkButton
                  className={styles.link}
                  href={
                    new ProjectURL({
                      language,
                      pathname: "/authentication/login",
                    })
                  }
                  target="_blank"
                >
                  Log in
                </LinkButton>
              </Item>
              <Item className={styles.item}>
                <LinkButton
                  className={styles.link}
                  href={
                    new ProjectURL({
                      language,
                      pathname: "/authentication/register",
                    })
                  }
                  target="_blank"
                >
                  Register
                </LinkButton>
              </Item>
            </>
          ) : (
            <>
              <Item>
                <LinkButton
                  className={styles.link}
                  href={new AccountURL({ language })}
                >
                  Info
                </LinkButton>
              </Item>
              <Item>
                <LinkButton
                  className={styles.link}
                  href={new AdministratorURL({ language, pathname: "/create" })}
                >
                  Create
                </LinkButton>
              </Item>
              <Item className={styles.item}>
                <Button
                  className={styles.button}
                  onClick={async () => {
                    await logout();
                  }}
                >
                  Log out
                </Button>
              </Item>
            </>
          )}
        </List>
      </Item>
    </List>
  );
}
