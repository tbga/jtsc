import { useState } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { ButtonSwitch } from "#components/buttons";
import { type IDynamicChildren } from "#types";

import styles from "./switchable.module.scss";

interface ISwitchableProps extends IBlockProps<"div"> {
  isOn?: boolean;
  button?: IDynamicChildren<[boolean | undefined]>;
}

const Switchable = blockComponent(styles.block, Component);

function Component({
  isOn: isOnProp,
  button,
  children,
  ...blockProps
}: ISwitchableProps) {
  const [isOn, switchOn] = useState(isOnProp);

  return (
    <div {...blockProps}>
      {/* <ButtonSwitch
        isOn={isOn}
        onClick={() => {
          switchOn(isOn);
        }}
      >
        {button(isOn)}
      </ButtonSwitch> */}
      {!isOn ? undefined : children}
    </div>
  );
}
