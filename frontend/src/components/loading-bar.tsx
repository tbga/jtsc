import { blockComponent, IBlockProps } from "#components/meta";

export interface ILoadingBarProps extends IBlockProps<"div"> {}

export const LoadingBar = blockComponent(undefined, Component);

function Component({ children, ...blockProps }: ILoadingBarProps) {
  return <div {...blockProps}>{children ?? "Loading..."}</div>;
}
