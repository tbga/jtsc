import { blockComponent, type IChildlessBlockProps } from "#components/meta";

import styles from "./html.module.scss";

export interface IImageHTMLProps
  extends Omit<IChildlessBlockProps<"img">, "src"> {
  src: string | URL;
}

export const ImageHTML = blockComponent(styles.block, Component);

function Component({
  src,
  alt = "",
  loading = "lazy",
  ...blockProps
}: IImageHTMLProps) {
  const finalSrc = src instanceof URL ? src.toString() : src;

  return <img src={finalSrc} alt={alt} loading={loading} {...blockProps} />;
}
