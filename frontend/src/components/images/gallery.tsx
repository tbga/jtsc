import clsx from "clsx";
import { useState } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { Item, List } from "#components/lists";
import { Button } from "#components/buttons";
import { Image, type IImageProps } from "./image";

import styles from "./gallery.module.scss";

export interface IGalleryProps extends IBlockProps<"div"> {
  items: IImageItem[];
  imageHeight?: IImageProps["height"];
}

export interface IImageItem extends Pick<IImageProps, "src" | "href"> {}

export const Gallery = blockComponent(styles.block, Component);

function Component({
  items,
  imageHeight,
  children,
  ...blockProps
}: IGalleryProps) {
  const [currentIndex, changeCurrentIndex] = useState(0);
  const isFirstItem = currentIndex === 0;
  const isLastItem = currentIndex === items.length - 1;

  return (
    <div {...blockProps}>
      <List className={styles.list}>
        {items.map(({ href, src }, index) => (
          <Item
            key={index}
            className={clsx(
              styles.item,
              index === currentIndex && styles.current,
            )}
          >
            <Image href={href} src={src} height={imageHeight} />
          </Item>
        ))}
      </List>
      <div className={styles.controls}>
        <Button
          className={styles.button}
          disabled={isFirstItem}
          onClick={() => {
            if (isFirstItem) {
              return;
            }

            changeCurrentIndex(currentIndex - 1);
          }}
        >
          Previous
        </Button>
        <span>
          {currentIndex + 1}/{items.length}
        </span>
        <Button
          className={styles.button}
          disabled={isLastItem}
          onClick={() => {
            if (isLastItem) {
              return;
            }

            changeCurrentIndex(currentIndex + 1);
          }}
        >
          Next
        </Button>
      </div>
      {children}
    </div>
  );
}
