export { Gallery } from "./gallery";
export type { IGalleryProps, IImageItem } from "./gallery";
export { Image } from "./image";
export type { IImageProps } from "./image";
