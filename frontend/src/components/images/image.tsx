import { CSSProperties } from "react";
import clsx from "clsx";
import { blockComponent, type IChildlessBlockProps } from "#components/meta";
import { Link, type ILinkProps } from "#components/links";
import { ImageHTML, type IImageHTMLProps } from "./html";

import styles from "./image.module.scss";

export interface IImageProps extends IChildlessBlockProps<"span"> {
  src?: IImageHTMLProps["src"] | null;
  alt?: IImageHTMLProps["alt"];
  /**
   * @default "lazy"
   */
  loading?: IImageHTMLProps["loading"];
  href?: ILinkProps["href"] | null;
  height?: string;
}

export const Image = blockComponent(styles.block, Component);

function Component({
  src,
  alt,
  loading,
  href,
  height,
  className,
  ...blockProps
}: IImageProps) {
  const blockClass = clsx(className, href && styles.link);
  const style = height
    ? ({
        "--local-image-height": height,
      } as CSSProperties)
    : undefined;

  if (href) {
    return (
      <Link
        className={blockClass}
        href={href}
        {...blockProps}
        target="_blank"
        style={style}
      >
        {src ? (
          <ImageHTML
            className={styles.image}
            src={src}
            alt={alt}
            loading={loading}
          />
        ) : (
          <span>No image available</span>
        )}
      </Link>
    );
  }

  return (
    <span className={blockClass} {...blockProps} style={style}>
      {src ? (
        <ImageHTML
          className={styles.image}
          src={src}
          alt={alt}
          loading={loading}
        />
      ) : (
        "No image available"
      )}
    </span>
  );
}
