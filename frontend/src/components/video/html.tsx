import { Link } from "#components/links";
import { blockComponent, type IChildlessBlockProps } from "#components/meta";

import styles from "./html.module.scss";

export interface IVideoHTMLProps
  extends IChildlessBlockProps<"video">,
    Pick<ISourceHTMLProps, "type"> {}

interface ISourceHTMLProps extends IChildlessBlockProps<"source"> {}
export const VideoHTML = blockComponent(styles.block, Component);

function Component({ src, type, ...blockProps }: IVideoHTMLProps) {
  return (
    <video {...blockProps}>
      <source src={src} type={type} />
      {!src ? (
        "No file available"
      ) : (
        <>
          Download the <Link href={src}>file</Link>.
        </>
      )}
    </video>
  );
}
