import { blockComponent, IBlockProps } from "#components/meta";
import { VideoHTML, type IVideoHTMLProps } from "./html";

import styles from "./video.module.scss";

interface IVideoProps
  extends IBlockProps<"span">,
    Pick<IVideoHTMLProps, "src" | "controls" | "loop" | "poster" | "type"> {}

export const Video = blockComponent(styles.block, Component);

function Component({
  src,
  type,
  controls,
  loop,
  poster,
  ...blockProps
}: IVideoProps) {
  return (
    <span {...blockProps}>
      <VideoHTML
        className={styles.video}
        src={src}
        type={type}
        controls={controls}
        loop={loop}
        poster={poster}
      />
    </span>
  );
}
