import { blockComponent, type IBlockProps } from "#components/meta";
import { type IHeadingLevel } from "#components";

import styles from "./article.module.scss";

export interface IArticleProps extends IBlockProps<"article"> {
  headingLevel?: IHeadingLevel;
}
export interface IArticleHeaderProps extends IBlockProps<"header"> {}
export interface IArticleBodyProps extends IBlockProps<"section"> {}
export interface IArticleFooterProps extends IBlockProps<"footer"> {}

export const Article = blockComponent(styles.block, ArticleComponent);
export const ArticleHeader = blockComponent(
  styles.header,
  ArticleHeaderComponent,
);
export const ArticleBody = blockComponent(styles.body, ArticleBodyComponent);
export const ArticleFooter = blockComponent(
  styles.footer,
  ArticleFooterComponent,
);

function ArticleComponent({ children, ...blockProps }: IArticleProps) {
  return <article {...blockProps}>{children}</article>;
}
function ArticleHeaderComponent({
  children,
  ...blockProps
}: IArticleHeaderProps) {
  return <header {...blockProps}>{children}</header>;
}
function ArticleBodyComponent({ children, ...blockProps }: IArticleBodyProps) {
  return <section {...blockProps}>{children}</section>;
}
function ArticleFooterComponent({
  children,
  ...blockProps
}: IArticleFooterProps) {
  return <footer {...blockProps}>{children}</footer>;
}
