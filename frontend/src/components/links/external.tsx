import { blockComponent } from "#components/meta";
import { ILinkHTMLProps, LinkHTML } from "./html";

import styles from "./external.module.scss";

export interface ILinkExternalProps
  extends Omit<ILinkHTMLProps, "referrerPolicy"> {}

export const LinkExternal = blockComponent(styles.block, Component);

function Component({
  target = "_blank",
  children,
  ...blockProps
}: ILinkExternalProps) {
  return (
    <LinkHTML {...blockProps} target={target} referrerPolicy="no-referrer">
      {children}
    </LinkHTML>
  );
}
