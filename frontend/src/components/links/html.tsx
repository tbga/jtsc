import { forwardRef, type LegacyRef } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./html.module.scss";

export interface ILinkHTMLProps extends Omit<IBlockProps<"a">, "href"> {
  href: URL | string;
}

/**
 * @TODO optional `href`
 */
export const LinkHTML = forwardRef<HTMLAnchorElement, ILinkHTMLProps>(
  blockComponent(styles.block, Component),
);

function Component(
  { href, children, ...blockProps }: ILinkHTMLProps,
  ref?: LegacyRef<HTMLAnchorElement>,
) {
  const finalHref = href instanceof URL ? href.toString() : href;

  return (
    <a href={finalHref} {...blockProps} ref={ref}>
      {children ?? finalHref}
    </a>
  );
}
