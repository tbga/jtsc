import { useCallback } from "react";
import { type UrlObject } from "url";
import { ProjectError } from "#lib/errors";
import { guessURLType, LINK_TYPE } from "#lib/urls";
import { blockComponent } from "#components/meta";
import { type ILinkExternalProps, LinkExternal } from "./external";
import { type ILinkInternalProps, LinkInternal } from "./internal";

import styles from "./link.module.scss";

export type ILinkProps =
  | ({ type?: "external" } & ILinkExternalProps)
  | ({ type?: "internal" } & ILinkInternalProps);

export const Link = blockComponent(styles.block, Component);

function Component({ type, href, children, ...blockProps }: ILinkProps) {
  const guessLinkType = useCallback(guessURLType, []);

  if (!type) {
    type = guessLinkType(href);
  }

  switch (type) {
    case LINK_TYPE.EXTERNAL: {
      return (
        <LinkExternal
          href={href as unknown as Exclude<typeof href, UrlObject>}
          {...blockProps}
        >
          {children}
        </LinkExternal>
      );
    }

    case LINK_TYPE.INTERNAL: {
      return (
        <LinkInternal href={href} {...blockProps}>
          {children}
        </LinkInternal>
      );
    }

    default: {
      throw new ProjectError(`Illegal link type "${type}"`);
    }
  }
}
