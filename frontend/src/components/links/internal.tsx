import Link, { type LinkProps } from "next/link";
import { ProjectURL } from "#lib/urls";
import { blockComponent } from "#components/meta";
import { type ILinkHTMLProps, LinkHTML } from "./html";

import styles from "./internal.module.scss";

export interface ILinkInternalProps
  extends Omit<LinkProps, "passHref">,
    Pick<
      ILinkHTMLProps,
      "className" | "children" | "target" | "onClick" | "style"
    > {
  href: ProjectURL | LinkProps["href"];
}

/**
 * @TODOs
 * - rewrite into non-legacy behaviour
 * - optional `href` somehow
 */
export const LinkInternal = blockComponent(styles.block, Component);

function Component({
  href,
  target = "_self",
  style,
  className,
  onClick,
  children,
  ...blockProps
}: ILinkInternalProps) {
  return (
    <Link href={String(href)} {...blockProps} passHref legacyBehavior>
      {/* @ts-expect-error `passHref` */}
      <LinkHTML
        style={style}
        className={className}
        target={target}
        onClick={onClick}
      >
        {children ?? String(href)}
      </LinkHTML>
    </Link>
  );
}
