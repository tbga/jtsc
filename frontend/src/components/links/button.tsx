import { blockComponent } from "#components/meta";
import { Link, type ILinkProps } from "./link";

import styles from "./button.module.scss";

type ILinkButtonProps = ILinkProps & {};

/**
 * @TODO disabled state
 */
export const LinkButton = blockComponent(styles.block, Component);

function Component({ children, ...blockProps }: ILinkButtonProps) {
  return (
    <Link {...blockProps}>
      <span className={styles.content}>{children}</span>
    </Link>
  );
}
