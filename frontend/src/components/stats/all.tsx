import { ProjectURL } from "#lib/urls";
import { useLanguage } from "#hooks";
import { Article, ArticleHeader, LoadingBar } from "#components";
import { DL, DS } from "#components/lists";
import { Link } from "#components/links";
import { SitesURL } from "#entities/site";
import { type IStatsAll, type IStatsSites } from "./types";

import styles from "./all.module.scss";
import { IS_DEVELOPMENT } from "#environment/derived-variables";

interface IProps {
  stats?: IStatsAll;
}

export function StatsOverview({ stats }: IProps) {
  const { language } = useLanguage();

  return (
    <Article>
      <ArticleHeader>
        <DL>
          <DS
            isHorizontal
            dKey="Artists"
            dValue={
              !stats ? (
                <LoadingBar />
              ) : (
                <Link href={new ProjectURL({ language, pathname: "/artists" })}>
                  {stats.artists}
                </Link>
              )
            }
          />

          <DS
            isHorizontal
            dKey="Posts"
            dValue={
              !stats ? (
                <LoadingBar />
              ) : (
                <Link href={new ProjectURL({ language, pathname: "/posts" })}>
                  {stats.posts}
                </Link>
              )
            }
          />

          <DS dKey="Sites" dValue={<StatsSites stats={stats?.sites} />} />

          <DS
            isHorizontal
            dKey="Profiles"
            dValue={
              !stats ? (
                <LoadingBar />
              ) : (
                <Link
                  href={new ProjectURL({ language, pathname: "/profiles" })}
                >
                  {stats.profiles}
                </Link>
              )
            }
          />

          <DS
            isHorizontal
            dKey="Releases"
            dValue={
              !stats ? (
                <LoadingBar />
              ) : (
                <Link
                  href={new ProjectURL({ language, pathname: "/releases" })}
                >
                  {stats.releases}
                </Link>
              )
            }
          />
        </DL>
      </ArticleHeader>
    </Article>
  );
}

function StatsSites({ stats }: { stats?: IStatsSites }) {
  const { language } = useLanguage();

  return (
    <DL className={styles.sub}>
      <DS
        isHorizontal
        dKey="All"
        dValue={
          !stats ? (
            <LoadingBar />
          ) : (
            <Link href={new SitesURL(language)}>{stats.all}</Link>
          )
        }
      />
      <DS
        isHorizontal
        dKey="Published"
        dValue={!stats ? <LoadingBar /> : stats.published}
      />
      <DS
        isHorizontal
        dKey="Local"
        dValue={!stats ? <LoadingBar /> : stats.local}
      />
    </DL>
  );
}
