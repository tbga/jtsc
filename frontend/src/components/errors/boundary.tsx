import { Heading, IHeadingLevel } from "#components";
import { Button } from "#components/buttons";
import { Component, type ReactNode } from "react";

export interface IErrorBoundaryProps {
  headingLevel: IHeadingLevel;
  children: ReactNode;
}

export interface IErrorBoundaryState {
  hasError: boolean;
}

export class ErrorBoundary extends Component<
  IErrorBoundaryProps,
  IErrorBoundaryState
> {
  // @ts-expect-error it's not picking up on generic type for some reason
  constructor(props) {
    super(props);

    this.state = { hasError: false };
  }

  /**
   * Update state so the next render will show the fallback UI
   */
  static getDerivedStateFromError(error: unknown) {
    return { hasError: true };
  }

  componentDidCatch(error: unknown, errorInfo: unknown) {
    console.log({ error, errorInfo });
  }

  render() {
    const { state, props } = this;
    const { hasError } = state;
    const { headingLevel, children } = props;

    return (
      <>
        {!hasError ? (
          children
        ) : (
          <>
            <Heading level={headingLevel}>Oops, there is an error!</Heading>
            <Button onClick={() => this.setState({ hasError: false })}>
              Try again?
            </Button>
          </>
        )}
      </>
    );
  }
}
