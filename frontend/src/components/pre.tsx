import { blockComponent, IBlockProps } from "#components/meta";

import styles from "./pre.module.scss";

export interface IPreProps extends IBlockProps<"pre"> {}

export const Pre = blockComponent(styles.block, Component);

function Component({ children, ...blockProps }: IPreProps) {
  return <pre {...blockProps}>{children}</pre>;
}
