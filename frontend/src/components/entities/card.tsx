import { ReactNode } from "react";
import { type IEntityItem } from "#lib/entities";
import { IHeadingLevel } from "#components";
import { blockComponent } from "#components/meta";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  type ICardProps,
} from "#components/lists";
import { Button } from "#components/buttons";

interface IProps<EntityType extends IEntityItem>
  extends Omit<ICardProps, "children" | "onSelect"> {
  entity: EntityType;
  header?: (entity: EntityType, headingLevel: IHeadingLevel) => ReactNode;
  footer?: (entity: EntityType, headingLevel: IHeadingLevel) => ReactNode;
  children?: (entity: EntityType, headingLevel: IHeadingLevel) => ReactNode;
  onSelect?: (entity: EntityType) => Promise<void>;
}

/**
 * @TODO work on it properly
 */
const EntityCard = blockComponent(undefined, Component);

function Component<EntityType extends IEntityItem>({
  entity,
  header,
  footer,
  children,
  onSelect,
  headingLevel = 2,
  ...blockProps
}: IProps<EntityType>) {
  return (
    <Card {...blockProps}>
      {header && <CardHeader>{header(entity, headingLevel)}</CardHeader>}

      {children && <CardBody>{children(entity, headingLevel)}</CardBody>}

      {!footer && !onSelect ? undefined : (
        <CardFooter>
          {footer?.(entity, headingLevel)}
          {!onSelect ? undefined : (
            <Button onClick={async () => await onSelect(entity)}>Select</Button>
          )}
        </CardFooter>
      )}
    </Card>
  );
}
