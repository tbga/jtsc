import { ReactNode } from "react";
import { type IEntityItem } from "#lib/entities";
import { blockComponent } from "#components/meta";
import { type IItemProps, Item } from "#components/lists";
import { Link } from "#components/links";
import { type IBigSerialInteger } from "#types";
import { EntityID } from "./id";

import styles from "./item.module.scss";

interface IEntityItemProps extends IItemProps {
  item: IEntityItem;
  urlBuilder?: (id: IBigSerialInteger) => string | URL;
}

/**
 * @TODO decouple it from `<li>`
 */
export const EntityItem = blockComponent(undefined, Component);

function Component({
  item,
  urlBuilder,
  children,
  ...blockProps
}: IEntityItemProps) {
  const { id, name } = item;

  return (
    <Item {...blockProps}>
      <DefaultItem id={id} urlBuilder={urlBuilder}>
        <span>&quot;{name ?? "Unknown"}&quot;</span>
        <EntityID className={styles.id}>({id})</EntityID>
      </DefaultItem>
      {children}
    </Item>
  );
}

interface IDefaultItemProps extends Pick<IEntityItemProps, "urlBuilder"> {
  id: IEntityItem["id"];
  children: ReactNode;
}

/**
 * @TODO stable root
 */
function DefaultItem({ id, urlBuilder, children }: IDefaultItemProps) {
  return !urlBuilder ? (
    <span className={styles.item}>{children}</span>
  ) : (
    <Link className={styles.item} href={urlBuilder(id)} target="_blank">
      {children}
    </Link>
  );
}
