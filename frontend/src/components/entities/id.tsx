import { blockComponent, type IBlockProps } from "#components/meta";

import styles from "./id.module.scss";

interface EntityIDProps extends IBlockProps<"span"> {}

/**
 * @TODO colour inheritance
 */
export const EntityID = blockComponent(styles.block, Component);

function Component({ children, ...blockProps }: EntityIDProps) {
  return <span {...blockProps}>{children}</span>;
}
