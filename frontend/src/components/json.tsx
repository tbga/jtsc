import { useCallback } from "react";
import { Pre } from "#components";
import { blockComponent, type IBlockProps } from "#components/meta";
import { toJSONPretty } from "#lib/json";

import styles from "./json.module.scss";

export interface IJSONViewProps extends IBlockProps<"pre"> {
  json: any;
}

export const JSONView = blockComponent(styles.block, Component);

function Component({ json, ...blockProps }: IJSONViewProps) {
  const toJSON = useCallback(toJSONPretty, []);

  return <Pre {...blockProps}>{toJSON(json)}</Pre>;
}
