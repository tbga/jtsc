import { blockComponent } from "#components/meta";
import { Button, type IButtonProps } from "./button";

interface IButtonNegativeProps extends Omit<IButtonProps, "visualType"> {}

export const ButtonNegative = blockComponent(undefined, Component);

function Component({ ...blockProps }: IButtonNegativeProps) {
  return <Button {...blockProps} visualType="negative" />;
}
