import { blockComponent } from "#components/meta";
import { type IButtonHTMLProps, ButtonHTML } from "./html";

interface IBaseProps
  extends Omit<IButtonHTMLProps, "type" | "form">,
    Pick<Required<IButtonHTMLProps>, "form"> {}

export interface IButtonResetProps extends IBaseProps {}

export const ButtonReset = blockComponent(undefined, Component);

function Component({ children, ...blockProps }: IButtonResetProps) {
  return (
    <ButtonHTML {...blockProps} type="reset">
      {children ?? "Reset"}
    </ButtonHTML>
  );
}
