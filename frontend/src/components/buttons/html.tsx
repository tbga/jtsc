import clsx from "clsx";
import { useState } from "react";
import { blockComponent, type IBlockProps } from "#components/meta";
import { type IVisualType, type IClickEvent } from "./types";

import styles from "./html.module.scss";

export interface IButtonHTMLProps extends IBlockProps<"button"> {
  visualType?: IVisualType;
}

export const ButtonHTML = blockComponent(styles.block, Component);

function Component({
  onClick,
  visualType = "button",
  className,
  children,
  ...blockProps
}: IButtonHTMLProps) {
  const [isClicking, switchClicking] = useState(false);
  const blockClass = clsx(
    className,
    styles[visualType],
    isClicking && styles.block_clicking,
  );

  async function handleClick(event: IClickEvent) {
    if (isClicking) {
      return;
    }

    try {
      switchClicking(true);
      onClick && (await onClick(event));
    } finally {
      switchClicking(false);
    }
  }

  return (
    <button className={blockClass} onClick={handleClick} {...blockProps}>
      {children}
    </button>
  );
}
