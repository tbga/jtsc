import { blockComponent } from "#components/meta";
import { Button, type IButtonProps } from "./button";

interface IButtonPositiveProps extends Omit<IButtonProps, "visualType"> {}

export const ButtonPositive = blockComponent(undefined, Component);

function Component({ ...blockProps }: IButtonPositiveProps) {
  return <Button {...blockProps} visualType="positive" />;
}
