import { blockComponent } from "#components/meta";
import { type IButtonHTMLProps, ButtonHTML } from "./html";
import { IVisualType, type IClickEvent } from "./types";

export interface IButtonProps
  extends Omit<IButtonHTMLProps, "type" | "visualType"> {
  onClick: (event: IClickEvent) => Promise<void> | void;
  visualType?: Exclude<IVisualType, "submit" | "reset">;
}

export const Button = blockComponent(undefined, Component);

function Component({
  visualType = "button",
  children,
  ...blockProps
}: IButtonProps) {
  return (
    <ButtonHTML {...blockProps} type="button" visualType={visualType}>
      {children}
    </ButtonHTML>
  );
}
