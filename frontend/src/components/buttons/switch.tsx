import { type ReactNode, useState } from "react";
import { blockComponent } from "#components/meta";
import { Button, type IButtonProps } from "./button";
import { type IClickEvent } from "./types";

interface IButtonSwitchProps extends Omit<IButtonProps, "children"> {
  isOn?: boolean;
  children: (isOn?: boolean) => ReactNode;
}

export const ButtonSwitch = blockComponent(undefined, Component);

function Component({
  isOn: isOnProp,
  onClick,
  children,
  ...blockProps
}: IButtonSwitchProps) {
  const [isOn, switchOn] = useState(isOnProp);

  async function handleClick(event: IClickEvent) {
    await onClick(event);
    switchOn(!isOn);
  }

  return (
    <Button onClick={handleClick} {...blockProps}>
      {children ? children(isOn) : isOn ? "Off" : "On"}
    </Button>
  );
}
