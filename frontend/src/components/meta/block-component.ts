import clsx from "clsx";

import { IFuncComponent } from "./types";

interface IBaseProps {
  className?: string;
}

export function blockComponent<Props extends IBaseProps = IBaseProps>(
  blockClassName: string | string[] | undefined,
  functionComponent: IFuncComponent<Props>,
): typeof functionComponent {
  return (...args: Parameters<typeof functionComponent>) => {
    const [props, ref] = args;
    const baseClass = clsx(blockClassName, props.className);

    return functionComponent({ ...props, className: baseClass }, ref);
  };
}
