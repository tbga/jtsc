import { useEffect, useState, type ReactElement } from "react";
import { useRouter } from "next/router";
import { IS_BROWSER } from "#environment/constants";
import { LoadingBar } from "#components";

export interface IClientComponentProps {
  children: ReactElement;
  // also wait for router to be ready
  isRouter?: boolean;
}

/**
 * Renders the children only upon hydration.
 */
export function ClientComponent({ isRouter, children }: IClientComponentProps) {
  const router = useRouter();
  const [isEnabled, enableComponent] = useState(false);
  const { isReady } = router;
  const isRenderReady = !isRouter ? isEnabled : isEnabled && isReady;

  useEffect(() => {
    IS_BROWSER && enableComponent(true);
  }, []);

  return !isRenderReady ? <LoadingBar /> : children;
}
