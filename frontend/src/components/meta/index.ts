export { blockComponent } from "./block-component";
export { ClientComponent } from "./client-component";
export type { IClientComponentProps } from "./client-component";
export type {
  IFuncComponent as FuncComponent,
  IBlockProps,
  IChildlessBlockProps,
} from "./types";
