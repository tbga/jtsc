import { blockComponent, type IBlockProps } from "#components/meta";
import { type IDateTime, formatDate } from "#lib/dates";

import styles from "./date-time.module.scss";

export interface IDateTimeProps extends IBlockProps<"time"> {
  dateTime?: IDateTime;
}

export const DateTime = blockComponent(styles.block, Component);

function Component({ children, dateTime, ...blockProps }: IDateTimeProps) {
  return (
    <time dateTime={dateTime} {...blockProps}>
      {children ?? (dateTime ? formatDate(dateTime) : "Unknown")}
    </time>
  );
}
