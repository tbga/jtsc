import { IBlockProps, blockComponent } from "#components/meta";

import styles from "./insert.module.scss";

interface IProps extends IBlockProps<"ins"> {}

export const Insert = blockComponent(styles.block, Component);

function Component({ ...props }: IProps) {
  return <ins {...props} />;
}
