import { IBlockProps, blockComponent } from "#components/meta";

import styles from "./delete.module.scss";

interface IProps extends IBlockProps<"del"> {}

export const Delete = blockComponent(styles.block, Component);

function Component({ ...props }: IProps) {
  return <del {...props} />;
}
