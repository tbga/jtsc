import type { ReactNode } from "react";
import { type IBlockProps, blockComponent } from "#components/meta";
import { Delete } from "./delete";
import { Insert } from "./insert";

interface IProps extends IBlockProps<"span"> {
  oldValue: ReactNode;
  newValue: ReactNode;
}

/**
 * @TODO comparator function
 */
export const Difference = blockComponent(undefined, Component);

function Component({ oldValue, newValue, ...props }: IProps) {
  return (
    <span {...props}>
      <Delete>{oldValue}</Delete>
      <Insert>{newValue}</Insert>
    </span>
  );
}
