import { type ReactNode } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { IS_PUBLIC, TITLE } from "#environment/variables";
import { blockComponent, type IBlockProps } from "#components/meta";
import { useAccount, useLanguage } from "#hooks";
import { LoadingBar, Heading, Article, ArticleHeader } from "#components";

import styles from "./page.module.scss";

export interface IPageProps extends IBlockProps<"section"> {
  title: string;
  heading?: ReactNode;
  children?: ReactNode;
}

/**
 * @TODOs
 * - fix auth check
 * - localized server render
 */
export const Page = blockComponent(styles.block, Component);

function Component({
  title,
  heading = title,
  children,
  ...blockProps
}: IPageProps) {
  const router = useRouter();
  const { isLoading } = useLanguage();
  const { isReady: isAuthready, isRegistered } = useAccount();
  const { isReady, pathname } = router;
  const [lang, topLevel] = pathname.split("/").slice(1);
  const isAuthPage = topLevel === "authentication";
  const isIndexPage = !lang;

  if (isLoading) {
    return (
      <>
        <Head>
          <title key="page-title">{TITLE}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {heading === null ? undefined : <Heading level={1}>{TITLE}</Heading>}
        <section>
          <Article>
            <ArticleHeader>
              <LoadingBar />
            </ArticleHeader>
          </Article>
        </section>
      </>
    );
  }

  if (IS_PUBLIC) {
    const isAccountPage = topLevel === "account";

    return (
      <>
        <Head>
          <title key="page-title">{`${title} | ${TITLE}`}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {heading === null ? undefined : <Heading level={1}>{heading}</Heading>}
        <section {...blockProps}>
          {isAccountPage && isReady && isAuthready && !isRegistered ? (
            <Article>
              <ArticleHeader>
                <p>You need an account to view this page.</p>
              </ArticleHeader>
            </Article>
          ) : (
            children
          )}
        </section>
      </>
    );
  }

  return (
    <>
      <Head>
        <title key="page-title">{`${title} | ${TITLE}`}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {heading === null ? undefined : <Heading level={1}>{heading}</Heading>}
      <section {...blockProps}>
        {!isReady || !isAuthready ? (
          <LoadingBar />
        ) : !isIndexPage && !isAuthPage && !isRegistered ? (
          <Article>
            <ArticleHeader>
              <p>You need an account to view this page.</p>
            </ArticleHeader>
          </Article>
        ) : (
          children
        )}
      </section>
    </>
  );
}
