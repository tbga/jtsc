import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { type IPagination } from "#lib/pagination";
import { LinkInternal } from "#components/links";
import { Article, ArticleHeader } from "#components";
import { type IPageProps, Page } from "./page";

interface IPaginationRedirectMultiArgs<
  ResolvedQuery extends Record<
    string,
    ParsedUrlQuery[keyof ParsedUrlQuery]
  > = Record<string, string | undefined>,
> {
  queryResolver: (query: ParsedUrlQuery) => ResolvedQuery;
  prefix: string | ((query: ResolvedQuery) => string);
  title: (query: ResolvedQuery, isReady: boolean) => IPageProps["title"];
  paginationFetcher: (query: ResolvedQuery) => Promise<IPagination>;
  urlBuilder: (page: string, query: ResolvedQuery) => string | URL;
  newLink?: (query: ResolvedQuery) => string | URL;
}

/**
 * Pagination redirect but for multi-param pages.
 * @TODOs
 * - `lang` support
 * - defer query resolving
 */
export function paginationRedirectMulti<
  ResolvedQuery extends Record<
    string,
    ParsedUrlQuery[keyof ParsedUrlQuery]
  > = Record<string, string | undefined>,
>({
  queryResolver,
  prefix,
  title,
  paginationFetcher,
  urlBuilder,
  newLink,
}: IPaginationRedirectMultiArgs<ResolvedQuery>) {
  function PageComponent() {
    const router = useRouter();
    const [pagination, changePagination] = useState<IPagination>();
    const { query, isReady } = router;
    const resolvedQuery = queryResolver(query);
    const pageTitle = title(resolvedQuery, isReady);
    const resolvedPrefix =
      typeof prefix === "string" ? prefix : prefix(resolvedQuery);

    useEffect(() => {
      if (!isReady) {
        return;
      }

      (async () => {
        const newPagination = await paginationFetcher(resolvedQuery);
        changePagination(newPagination);
      })();
    }, [isReady]);

    useEffect(() => {
      if (!pagination || BigInt(pagination.total_count) < 1) {
        return;
      }

      const url = urlBuilder(pagination.current_page, resolvedQuery);
      router.replace(url);
    }, [pagination, resolvedQuery]);

    return (
      <Page title={pageTitle}>
        <Article>
          <ArticleHeader>
            <p>
              {!pagination ? (
                `Analyzing ${resolvedPrefix} page stats...`
              ) : BigInt(pagination.total_count) < 1 ? (
                <>
                  There are no {resolvedPrefix}.{" "}
                  {newLink && (
                    <LinkInternal href={newLink(resolvedQuery)}>
                      Add one
                    </LinkInternal>
                  )}
                </>
              ) : (
                `Redirecting to the last ${prefix} page...`
              )}
            </p>
          </ArticleHeader>
        </Article>
      </Page>
    );
  }

  return PageComponent;
}
