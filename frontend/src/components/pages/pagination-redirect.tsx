import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { type IPagination, IURLBuilder } from "#lib/pagination";
import { LinkInternal } from "#components/links";
import { Page, type IPageProps } from "./page";
import { Article, ArticleHeader } from "#components";

interface IPaginationRedirectArgs {
  prefix: string;
  title: IPageProps["title"];
  paginationFetcher: () => Promise<IPagination>;
  urlBuilder: IURLBuilder;
  newLink?: string;
}

/**
 * @TODO language support
 */
export function paginationRedirect({
  prefix,
  title,
  paginationFetcher,
  urlBuilder,
  newLink,
}: IPaginationRedirectArgs) {
  function PageComponent() {
    const router = useRouter();
    const [isNoPages, switchIsNopages] = useState<boolean>();

    useEffect(() => {
      (async () => {
        const pagination = await paginationFetcher();

        if (parseInt(pagination.total_pages) === 0) {
          switchIsNopages(true);
          return;
        }

        switchIsNopages(false);
        router.replace(urlBuilder(pagination.current_page));
      })();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
      <Page title={title}>
        <Article>
          <ArticleHeader>
            <p>
              {isNoPages === undefined ? (
                `Analyzing ${prefix} page stats...`
              ) : isNoPages ? (
                <>
                  There are no {prefix}.{" "}
                  {newLink && (
                    <LinkInternal href={newLink}>Add one</LinkInternal>
                  )}
                </>
              ) : (
                `Redirecting to the last ${prefix} page...`
              )}
            </p>
          </ArticleHeader>
        </Article>
      </Page>
    );
  }

  return PageComponent;
}
