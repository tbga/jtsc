import Head from "next/head";
import { TITLE } from "#environment/variables";
import { Article, ArticleHeader, Heading } from "#components";
import {
  type IPageContext,
  type IPageComponentOptions,
  type IPageFunc,
} from "./types";

import styles from "../page.module.scss";

interface IProps extends Pick<IPageComponentOptions, "title" | "heading"> {
  isAuthPage: boolean;
  isRegistered: boolean;
  func: IPageFunc;
  pageContext: IPageContext;
}

export function PageNonPublic({
  title,
  heading,
  isAuthPage,
  isRegistered,
  func,
  pageContext,
}: IProps) {
  const resultTitle = title?.(pageContext);

  return (
    <>
      <Head>
        <title key="page-title">
          {!resultTitle ? TITLE : `${resultTitle} | ${TITLE}`}
        </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {heading !== null && (
        <Heading level={1}>{!heading ? TITLE : heading(pageContext)}</Heading>
      )}

      <section className={styles.block}>
        {!isAuthPage && !isRegistered ? (
          <Article>
            <ArticleHeader>
              <p>You need an account to view this page.</p>
            </ArticleHeader>
          </Article>
        ) : (
          func(pageContext)
        )}
      </section>
    </>
  );
}
