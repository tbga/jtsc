import Head from "next/head";
import { TITLE } from "#environment/variables";
import { Article, ArticleHeader, Heading, LoadingBar } from "#components";

import styles from "../page.module.scss";

interface IProps {
  isHeading: boolean;
}

export function PageInit({ isHeading }: IProps) {
  return (
    <>
      <Head>
        <title key="page-title">{TITLE}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {isHeading && <Heading level={1}>{TITLE}</Heading>}

      <section className={styles.block}>
        <Article>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
        </Article>
      </section>
    </>
  );
}
