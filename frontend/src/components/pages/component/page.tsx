import { useRouter } from "next/router";
import { IS_PUBLIC } from "#environment/variables";
import { useAccount, useLanguage } from "#hooks";
import { PageInit } from "./init";
import { PagePublic } from "./public";
import { PageNonPublic } from "./non-public";
import { type IPageFunc, type IPageComponentOptions } from "./types";

/**
 * @TODOs
 * - default render
 * - public and non-public renders as a single component
 */
export function pageComponent(options: IPageComponentOptions, func: IPageFunc) {
  const { title, heading } = options;

  function Page() {
    const router = useRouter();
    const { language, isLoading: isLanguageLoading } = useLanguage();
    const { isReady: isAuthReady, isRegistered } = useAccount();

    const { isReady: isRouterReady, pathname, query } = router;
    const [topLevel] = pathname.split("/").slice(2);
    const isAuthPage = topLevel === "authentication";
    const isLoading = !isRouterReady || !isAuthReady || isLanguageLoading;
    const pageContext = { query, language };

    if (isLoading) {
      return <PageInit isHeading={options.heading !== null} />;
    }

    if (IS_PUBLIC) {
      return (
        <PagePublic
          topLevel={topLevel}
          title={title}
          heading={heading}
          isRegistered={isRegistered}
          func={func}
          pageContext={pageContext}
        />
      );
    }

    return (
      <PageNonPublic
        title={title}
        heading={heading}
        isAuthPage={isAuthPage}
        isRegistered={isRegistered}
        func={func}
        pageContext={pageContext}
      />
    );
  }

  return Page;
}
