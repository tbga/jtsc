import { type ParsedUrlQuery } from "querystring";

export type IPageComponentOptions = {
  title?: (ctx: IPageContext) => string;
  heading?: null | ((ctx: IPageContext) => string);
};

export interface IPageContext {
  query: ParsedUrlQuery;
  language: string;
}
export interface IPageFunc {
  (ctx: IPageContext): JSX.Element;
}
