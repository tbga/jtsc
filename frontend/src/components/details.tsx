import { type ReactNode } from "react";
import { type IBlockProps, blockComponent } from "#components/meta";

import styles from "./details.module.scss";

interface IProps extends IBlockProps<"details"> {
  summary: ReactNode;
}

export const Details = blockComponent(styles.block, Component);

function Component({ summary, children, ...blockProps }: IProps) {
  return (
    <details {...blockProps}>
      <summary className={styles.summary}>{summary}</summary>
      {children}
    </details>
  );
}
