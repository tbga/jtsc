import { useEffect, useRef, useState, type ReactNode } from "react";
import { fetchRelease } from "#api/releases";
import { LoadingBar } from "#components";
import { blockComponent } from "#components/meta";
import { Fieldset, IFieldsetProps, Legend } from "#components/forms";
import { IntegerInput } from "#components/forms/inputs";
import { Button } from "#components/buttons";
import { ListButtons } from "#components/lists";
import { type IAsyncReturnType } from "#types";
import { ReleaseCard, IReleaseCardProps } from "./card";
import { SearchReleasesForm } from "./search";

import styles from "./section.module.scss";

interface IProps
  extends IFieldsetProps,
    Pick<IReleaseCardProps, "headingLevel"> {
  id: string;
  form: string;
  name: string;
  label?: ReactNode;
}

export const ReleaseSection = blockComponent(styles.block, Component);

function Component({
  id,
  name,
  form,
  headingLevel,
  label,
  defaultValue,
  children,
  ...blockProps
}: IProps) {
  const [isLoading, switchLoading] = useState(Boolean(defaultValue));
  const [release, changeRelease] =
    useState<IAsyncReturnType<typeof fetchRelease>>();
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!defaultValue) {
      return;
    }

    const parsedID = (
      Array.isArray(defaultValue)
        ? (defaultValue as string[])[0]
        : String(defaultValue)
    ).trim();

    if (!parsedID) {
      return;
    }

    (async () => {
      try {
        const newRelease = await fetchRelease(String(parsedID));
        changeRelease(newRelease);
        if (inputRef.current) {
          inputRef.current.value = newRelease.id;
        }
      } finally {
        switchLoading(false);
      }
    })();
  }, [defaultValue]);

  async function resetRelease(isFull: boolean = false) {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      if (isFull || !defaultValue) {
        changeRelease(undefined);
        if (inputRef.current) {
          inputRef.current.value = "";
        }

        return;
      }

      const parsedID = (
        Array.isArray(defaultValue)
          ? (defaultValue as string[])[0]
          : String(defaultValue)
      ).trim();

      if (!parsedID) {
        return;
      }

      const newRelease = await fetchRelease(String(parsedID));
      changeRelease(newRelease);
      if (inputRef.current) {
        inputRef.current.value = newRelease.id;
      }
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Fieldset {...blockProps}>
      <Legend>{label ?? "Release"}</Legend>

      {isLoading ? (
        <LoadingBar />
      ) : !release ? (
        <SearchReleasesForm
          className={styles.search}
          onSelect={async ({ id }) => {
            const newRelease = await fetchRelease(id);
            changeRelease(newRelease);
            if (inputRef.current) {
              inputRef.current.value = newRelease.id;
            }
          }}
        />
      ) : (
        <ReleaseCard
          className={styles.card}
          // @ts-expect-error @TODO generic card component
          release={release}
        >
          <ListButtons isSpread>
            <Button onClick={async () => resetRelease()}>Reset</Button>
            {defaultValue && (
              <Button onClick={async () => resetRelease(true)}>Clear</Button>
            )}
          </ListButtons>
        </ReleaseCard>
      )}
      <IntegerInput
        ref={inputRef}
        id={id}
        className={styles.id}
        form={form}
        name={name}
        defaultValue={defaultValue}
      />
      {children}
    </Fieldset>
  );
}
