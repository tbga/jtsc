import { useEffect, useState } from "react";
import { parseTemplate } from "url-template";
import {
  fetchReleasePostsPagination,
  fetchReleaseProfilesPagination,
} from "#api/releases";
import { fetchSite } from "#api/sites";
import { useAccount, useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Pre,
  type IArticleProps,
} from "#components";
import { DL, DS, ListButtons } from "#components/lists";
import { Link, LinkButton } from "#components/links";
import { EntityID } from "#components/entities";
import { DateTime } from "#components/dates";
import { SiteURL } from "#entities/site";
import { BIGINT_ZERO, IAsyncReturnType } from "#types";
import {
  NewReleasePostsURL,
  ReleaseEditURL,
  ReleasePostsURL,
  ReleaseProfilesURL,
} from "./urls";
import { type IRelease } from "./types";

import styles from "./article.module.scss";

export interface IReleaseArticleProps extends IArticleProps {
  release: IRelease;
}

/**
 * @TODO edit link
 */
export const ReleaseArticle = blockComponent(styles.block, Component);

function Component({
  release,
  headingLevel = 2,
  ...blockProps
}: IReleaseArticleProps) {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const [fullSite, changeFullSite] =
    useState<IAsyncReturnType<typeof fetchSite>>();
  const { id, title, description, original_release_id, site, released_at } =
    release;
  const siteID = site?.id;
  const siteURL = fullSite && new URL(fullSite.home_page);
  const originalReleaseURL =
    !original_release_id || !siteURL || !fullSite?.url_templates?.release
      ? undefined
      : decodeURIComponent(
          parseTemplate(fullSite.url_templates?.release).expand({
            protocol: siteURL.protocol,
            host: siteURL.host,
            origin: siteURL.origin,
            release_id: original_release_id,
          }),
        );

  useEffect(() => {
    if (!siteID) {
      return;
    }
    (async () => {
      const newFullSite = await fetchSite(siteID);
      changeFullSite(newFullSite);
    })();
  }, [siteID]);

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{title ?? "Untitled"}</Heading>
        <EntityID>{id}</EntityID>
      </ArticleHeader>
      <ArticleBody>
        <DL>
          <DS
            isHorizontal
            dKey="Site"
            dValue={
              !site ? (
                "None"
              ) : (
                <Link href={new SiteURL(language, site.id)} target="_blank">
                  {site.name ?? "Untitled"}
                </Link>
              )
            }
          />
          <DS
            isHorizontal
            dKey="Profiles"
            dValue={<Profiles release_id={release.id} />}
          />
          <DS dKey="Original ID" dValue={original_release_id} />
          <DS
            dKey={"Original URL"}
            dValue={originalReleaseURL && <Link href={originalReleaseURL} />}
          />
          <DS
            dKey="Release Date"
            dValue={<DateTime dateTime={released_at} />}
          />
          <DS dKey="Description" dValue={<Pre>{description}</Pre>} />
          <DS
            isHorizontal
            dKey="Posts"
            dValue={<Posts release_id={release.id} />}
          />
        </DL>
      </ArticleBody>
      <ArticleFooter>
        {isRegistered && (
          <ListButtons>
            <LinkButton href={new ReleaseEditURL(language, release.id)}>
              Edit
            </LinkButton>
          </ListButtons>
        )}
      </ArticleFooter>
    </Article>
  );
}

function Profiles({ release_id }: { release_id: IRelease["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchReleaseProfilesPagination>>>();

  useEffect(() => {
    (async () => {
      const newPagination = await fetchReleaseProfilesPagination(release_id);
      changePagination(newPagination);
    })();
  }, [release_id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <span>{pagination.total_count}</span>
  ) : (
    <Link href={new ReleaseProfilesURL(language, release_id)}>
      {pagination.total_count}
    </Link>
  );
}

function Posts({ release_id }: { release_id: IRelease["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchReleasePostsPagination>>>();

  useEffect(() => {
    (async () => {
      const newPagination = await fetchReleasePostsPagination(release_id);
      changePagination(newPagination);
    })();
  }, [release_id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <Link href={new NewReleasePostsURL(language, release_id)}>Add</Link>
  ) : (
    <Link href={new ReleasePostsURL(language, release_id)}>
      {pagination.total_count}
    </Link>
  );
}
