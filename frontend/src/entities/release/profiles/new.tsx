import { ProjectError } from "#lib/errors";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { isEmpty } from "#lib/util";
import { useLanguage } from "#hooks";
import { fetchReleaseProfilesCreate } from "#api/account/administrator/releases";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { ProfileURL, type IProfile } from "#entities/profile";
import { type IRelease } from "../types";

const FORM_FIELDS = {
  PROFILE_IDS: { name: "profile_ids", label: "Profile IDs" },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  release_id: IRelease["id"];
}

export const NewReleaseProfilesForm = blockComponent(undefined, Component);

function Component({ release_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();

  function collectInit(
    init: { ids: IProfile["id"][] },
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.PROFILE_IDS.name: {
        const ids = collectIDs(value);

        if (isEmpty(ids)) {
          break;
        }

        init.ids = ids;

        break;
      }

      default: {
        throw new ProjectError(
          `Unknown input name "${fieldName satisfies never}".`,
        );
      }
    }
  }

  return (
    <Form<IFieldName, { ids: IRelease["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) => fetchReleaseProfilesCreate(release_id, ids)}
      resultView={(formID, result) => (
        <>
          <p>Added profiles to the release:</p>
          <ListLocal
            items={result.map((profile) => (
              <EntityItem
                key={profile.id}
                item={profile}
                urlBuilder={(id) => new ProfileURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.PROFILE_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.PROFILE_IDS.name}
          label={FORM_FIELDS.PROFILE_IDS.label}
        >
          Newline-separated list of profile IDs.
        </TextSection>
      )}
    </Form>
  );
}
