import clsx from "clsx";
import { useEffect, useState } from "react";
import {
  fetchReleaseProfiles,
  fetchReleaseProfilesPagination,
} from "#api/releases";
import { fetchReleaseProfilesRemove } from "#api/account/administrator/releases";
import { useLanguage } from "#hooks";
import { blockComponent, IBlockProps } from "#components/meta";
import { CardListInternal } from "#components/lists/cards";
import { ListButtons } from "#components/lists";
import { Button, ButtonNegative } from "#components/buttons";
import { type IProfile, ProfileCard } from "#entities/profile";
import { BIGINT_ZERO, type IAsyncReturnType } from "#types";
import { type IRelease } from "../types";

import styles from "./remove.module.scss";

interface IProps extends IBlockProps<"div"> {
  release_id: IRelease["id"];
}

export const RemoveReleaseProfilesForm = blockComponent(
  styles.block,
  Component,
);

function Component({ release_id, ...blockProps }: IProps) {
  const { language } = useLanguage();
  const [selectedIDs, changeSelectedIDs] = useState<Set<IProfile["id"]>>(
    new Set(),
  );
  const [profileData, changeProfileData] =
    useState<IAsyncReturnType<typeof fetchReleaseProfiles>>();

  useEffect(() => {
    (async () => {
      const pagination = await fetchReleaseProfilesPagination(release_id);

      // do not fetch further if there is nothing
      if (BigInt(pagination.total_count) === BIGINT_ZERO) {
        changeProfileData(undefined);
        return;
      }

      const newPorfileData = await fetchReleaseProfiles(release_id, "1");
      changeProfileData(newPorfileData);
    })();
  }, [release_id]);

  function isSelected(file_id: IProfile["id"]) {
    return selectedIDs.has(file_id);
  }

  function selectPreview(file_id: IProfile["id"]) {
    if (selectedIDs.has(file_id)) {
      return;
    }

    const newSet = new Set(selectedIDs);
    newSet.add(file_id);
    changeSelectedIDs(newSet);
  }

  function unselectPreview(file_id: IProfile["id"]) {
    if (!selectedIDs.has(file_id)) {
      return;
    }

    const newSet = new Set(selectedIDs);
    newSet.delete(file_id);
    changeSelectedIDs(newSet);
  }

  function selectPage() {
    if (!profileData) {
      return;
    }

    const newIDs = profileData.profiles.map(({ id }) => id);
    const isAllSelected = newIDs.every((id) => selectedIDs.has(id));

    if (isAllSelected) {
      return;
    }

    const newSet = new Set(selectedIDs);
    newIDs.forEach((id) => newSet.add(id));
    changeSelectedIDs(newSet);
  }

  async function removeSelected() {
    if (!profileData) {
      return;
    }

    const removalIDs = Array.from(selectedIDs);
    await fetchReleaseProfilesRemove(release_id, removalIDs);
    changeSelectedIDs(new Set());

    const pagination = await fetchReleaseProfilesPagination(release_id);

    if (BigInt(pagination.total_count) === BIGINT_ZERO) {
      changeProfileData(undefined);
      return;
    }

    const currentPage = BigInt(profileData.pagination.current_page);
    const newTotalPages = BigInt(pagination.total_pages);
    const nextPage = currentPage > newTotalPages ? newTotalPages : currentPage;

    const newProfileData = await fetchReleaseProfiles(
      release_id,
      String(nextPage),
    );
    changeProfileData(newProfileData);
  }

  return (
    <div {...blockProps}>
      {!profileData ? (
        <p>No profiles exist for this release.</p>
      ) : (
        <>
          <p>
            {!selectedIDs.size
              ? "No profiles selected for deletion."
              : `Selected ${selectedIDs.size} profiles out of ${profileData.pagination.total_count} for deletion.`}
          </p>
          <CardListInternal
            pagination={profileData.pagination}
            onPageChange={async (page) => {
              const newPreviewData = await fetchReleaseProfiles(
                release_id,
                page,
              );
              changeProfileData(newPreviewData);
            }}
          >
            {profileData.profiles.map((profile) => {
              const isSelectedForDelete = isSelected(profile.id);
              const blockClass = clsx(isSelectedForDelete && styles.selected);

              return (
                <ProfileCard
                  className={blockClass}
                  key={profile.id}
                  profile={profile}
                >
                  <ListButtons className={styles.fileControls}>
                    <Button
                      onClick={() => {
                        if (isSelectedForDelete) {
                          unselectPreview(profile.id);
                        } else {
                          selectPreview(profile.id);
                        }
                      }}
                    >
                      {!isSelectedForDelete ? "Select" : "Unselect"}
                    </Button>
                  </ListButtons>
                </ProfileCard>
              );
            })}
          </CardListInternal>
          <ListButtons className={styles.controls}>
            <Button className={styles.control} onClick={selectPage}>
              Select Page
            </Button>
            <ButtonNegative className={styles.control} onClick={removeSelected}>
              Remove Selected
            </ButtonNegative>
          </ListButtons>
        </>
      )}
    </div>
  );
}
