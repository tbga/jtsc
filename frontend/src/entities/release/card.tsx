import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Heading } from "#components";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  type ICardProps,
} from "#components/lists";
import { Link } from "#components/links";
import { DateTime } from "#components/dates";
import { EntityID } from "#components/entities";
import { SiteURL } from "#entities/site";
import { ReleaseURL } from "./urls";
import { type IReleasePreview } from "./types";

import styles from "./card.module.scss";

export interface IReleaseCardProps extends ICardProps {
  release: IReleasePreview;
}

export const ReleaseCard = blockComponent(styles.block, Component);

function Component({
  release,
  headingLevel = 2,
  children,
  ...blockProps
}: IReleaseCardProps) {
  const { language } = useLanguage();
  const { id, title, site, profiles, original_release_id, posts, released_at } =
    release;

  return (
    <Card {...blockProps}>
      <CardHeader>
        <Heading level={headingLevel}>{title ?? "Untitled"}</Heading>
      </CardHeader>

      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS
            dKey="Site"
            dValue={
              site && (
                <Link href={new SiteURL(language, site.id)} target="_blank">
                  {site.name ?? "Untitled"} ({site.id})
                </Link>
              )
            }
          />
          <DS dKey="Original ID" dValue={original_release_id} />
          <DS isHorizontal dKey="Profiles" dValue={profiles} />
          <DS isHorizontal dKey="Posts" dValue={posts} />
          <DS
            dKey="Release Date"
            dValue={<DateTime dateTime={released_at} />}
          />
        </DL>
      </CardBody>

      <CardFooter>
        <Link
          className={styles.details}
          href={new ReleaseURL(language, id)}
          target="_blank"
        >
          Details
        </Link>
        {children}
      </CardFooter>
    </Card>
  );
}
