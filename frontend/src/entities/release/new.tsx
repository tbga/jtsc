import { useEffect, useState } from "react";
import { ProjectError } from "#lib/errors";
import { NEWLINE_SEPARATOR } from "#lib/strings";
import { isEmpty } from "#lib/util";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { fetchSite } from "#api/sites";
import { fetchCreateRelease } from "#api/account/administrator/releases";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Details, Pre } from "#components";
import { Form, type IBaseFormProps } from "#components/forms";
import {
  NumberSection,
  TextSection,
  DateTimeSection,
  OriginalDateSection,
} from "#components/forms/sections";
import { Link } from "#components/links";
import { type IProfile } from "#entities/profile";
import { type IPost, type IPostInit } from "#entities/post";
import { type ISite, SiteSection } from "#entities/site";
import { OperationURL } from "#entities/operation";
import { EMPTY_OBJECT, IAsyncReturnType } from "#types";
import { validateReleaseInit } from "./lib";
import { type IReleaseInit } from "./types";

const FORM_FIELDS = {
  SITE_ID: {
    name: "site_id",
  },
  PROFILE_IDS: {
    name: "profile_ids",
    label: "Profile IDs",
  },
  ORIGINAL_ID: {
    name: "original_release_id",
    label: "Original ID",
  },
  TITLE: {
    name: "title",
    label: "Title",
  },
  DESCRIPTION: {
    name: "description",
    label: "Description",
  },
  RELEASED_AT: {
    name: "released_at",
    label: "Release Date",
  },
  RELEASED_AT_ORIGINAL: {
    name: "released_at_original",
    label: "Original Release Date",
  },
  POST_IDS: {
    name: "post_ids",
    label: "Post IDs",
  },
  NEW_POSTS: {
    name: "new_posts",
    label: "New Posts",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

export interface INewReleaseFormProps extends IBaseFormProps {
  site_id?: ISite["id"];
  profile_ids?: IProfile["id"][];
  readonly post_ids?: IPost["id"][];
}

export const NewReleaseForm = blockComponent(undefined, Component);

function Component({
  id,
  site_id,
  profile_ids,
  post_ids,
  ...blockProps
}: INewReleaseFormProps) {
  const { language } = useLanguage();
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const parsedProfileIDs = profile_ids?.join(NEWLINE_SEPARATOR);
  const parsedPostIDs = post_ids?.join(NEWLINE_SEPARATOR);

  useEffect(() => {
    if (!site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [site_id]);

  return (
    <Form<IFieldName, IReleaseInit, IEntityItem>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      validateInit={validateReleaseInit}
      getResult={async (init) => fetchCreateRelease(init)}
      resultView={(_, { id, name }) => (
        <p>
          Created a new release operation{" "}
          <Link href={new OperationURL(language, id)} target="_blank">
            &quot;{name}&quot; ({id})
          </Link>
        </p>
      )}
      resetButton="Create another"
      {...blockProps}
    >
      {(formID) => (
        <>
          <SiteSection
            id={`${formID}-${FORM_FIELDS.SITE_ID.name}`}
            form={formID}
            name={FORM_FIELDS.SITE_ID.name}
            defaultValue={site_id}
            onSiteChange={async (newSite) => changeSite(newSite)}
            onSiteClear={async () => changeSite(undefined)}
          >
            Site of the release.
          </SiteSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.PROFILE_IDS.name}`}
            form={formID}
            name={FORM_FIELDS.PROFILE_IDS.name}
            label={FORM_FIELDS.PROFILE_IDS.label}
            defaultValue={parsedProfileIDs}
          >
            Newline-separated list of IDs of the existing profile.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_ID.name}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_ID.name}
            label={FORM_FIELDS.ORIGINAL_ID.label}
          >
            {/* Show release URL template if available */}
            {site?.url_templates?.release && (
              <Pre>{site.url_templates.release}</Pre>
            )}
            <p>Original ID of the release.</p>
            {/* Show release URL template notes if available */}
            {site?.url_templates?.release_notes && (
              <Details summary="Notes">
                {site.url_templates.release_notes}
              </Details>
            )}
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
          >
            The title of the release.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.DESCRIPTION.name}`}
            form={formID}
            name={FORM_FIELDS.DESCRIPTION.name}
            label={FORM_FIELDS.DESCRIPTION.label}
          >
            The description of the release.
          </TextSection>

          <OriginalDateSection
            id={`${formID}-${FORM_FIELDS.RELEASED_AT_ORIGINAL.name}`}
            form={formID}
            name={FORM_FIELDS.RELEASED_AT_ORIGINAL.name}
            label={FORM_FIELDS.RELEASED_AT_ORIGINAL.label}
            timestampInputID={`${formID}-${FORM_FIELDS.RELEASED_AT.name}`}
            timestampInputName={FORM_FIELDS.RELEASED_AT.name}
            timestampInputLabel={FORM_FIELDS.RELEASED_AT.label}
            timestampInputChildren={<>Parsed release date.</>}
          >
            Original release date as plain text.
          </OriginalDateSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.POST_IDS.name}`}
            form={formID}
            name={FORM_FIELDS.POST_IDS.name}
            label={FORM_FIELDS.POST_IDS.label}
            defaultValue={parsedPostIDs}
          >
            Newline-separated list of existing post IDs.
          </TextSection>

          <NumberSection
            id={`${formID}-${FORM_FIELDS.NEW_POSTS.name}`}
            form={formID}
            name={FORM_FIELDS.NEW_POSTS.name}
            label={FORM_FIELDS.NEW_POSTS.label}
            min={0}
            step={1}
            defaultValue={!parsedPostIDs ? 1 : undefined}
          >
            Amount of new posts to create.
          </NumberSection>
        </>
      )}
    </Form>
  );
}

function collectInit(init: IReleaseInit, fieldName: IFieldName, value: string) {
  switch (fieldName) {
    case FORM_FIELDS.SITE_ID.name:
    case FORM_FIELDS.ORIGINAL_ID.name:
    case FORM_FIELDS.TITLE.name:
    case FORM_FIELDS.DESCRIPTION.name:
    case FORM_FIELDS.RELEASED_AT.name:
    case FORM_FIELDS.RELEASED_AT_ORIGINAL.name: {
      init[fieldName] = value;
      break;
    }

    case FORM_FIELDS.PROFILE_IDS.name: {
      const ids = collectIDs(value);

      if (isEmpty(ids)) {
        break;
      }

      if (!init.profiles) {
        init.profiles = {};
      }

      init.profiles.ids = ids as [IProfile["id"], ...IProfile["id"][]];

      break;
    }
    case FORM_FIELDS.POST_IDS.name: {
      const ids = collectIDs(value);

      if (isEmpty(ids)) {
        break;
      }

      if (!init.posts) {
        init.posts = {};
      }

      init.posts.ids = ids as [IPost["id"], ...IPost["id"][]];

      break;
    }

    case FORM_FIELDS.NEW_POSTS.name: {
      const newPostsNumber = Number(value);

      if (!newPostsNumber) {
        break;
      }

      if (!init.posts) {
        init.posts = {};
      }

      const newPosts = new Array<IPostInit>(newPostsNumber).fill(EMPTY_OBJECT);

      init.posts.inits = newPosts as [IPostInit, ...IPostInit[]];

      break;
    }

    default: {
      throw new ProjectError(`Unknown field name "${fieldName}".`);
    }
  }
}
