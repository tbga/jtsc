import { useEffect, useState } from "react";
import { ProjectError } from "#lib/errors";
import { fetchEditRelease } from "#api/account/administrator/releases";
import { fetchSite } from "#api/sites";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { OriginalDateSection, TextSection } from "#components/forms/sections";
import { Details, Pre } from "#components";
import { SiteSection } from "#entities/site";
import { validateReleaseUpdate } from "./lib";
import { type IReleaseUpdate, type IRelease } from "./types";
import { type IAsyncReturnType } from "#types";

const FORM_FIELDS = {
  SITE_ID: {
    name: "site_id",
  },
  ORIGINAL_ID: {
    name: "original_release_id",
    label: "Original ID",
  },
  TITLE: {
    name: "title",
    label: "Title",
  },
  DESCRIPTION: {
    name: "description",
    label: "Description",
  },
  RELEASED_AT: {
    name: "released_at",
    label: "Release Date",
  },
  RELEASED_AT_ORIGINAL: {
    name: "released_at_original",
    label: "Original Release Date",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  release: IRelease;
  onEdit?: (release: IRelease) => Promise<void>;
}

export const EditReleaseForm = blockComponent(undefined, Component);

function Component({ release, id: formID, onEdit, ...blockProps }: IProps) {
  const [siteEntity, changeSiteEntity] =
    useState<IAsyncReturnType<typeof fetchSite>>();
  const {
    id,
    site,
    original_release_id,
    title,
    description,
    released_at,
    released_at_original,
  } = release;
  const siteID = site?.id;

  useEffect(() => {
    if (siteID === siteEntity?.id || !siteID) {
      return;
    }

    (async () => {
      const newEntity = await fetchSite(siteID);
      changeSiteEntity(newEntity);
    })();
  }, [siteID]);

  function collectInit(init: IReleaseUpdate, key: IFieldName, value: string) {
    switch (key) {
      case FORM_FIELDS.ORIGINAL_ID.name:
      case FORM_FIELDS.TITLE.name:
      case FORM_FIELDS.DESCRIPTION.name:
      case FORM_FIELDS.RELEASED_AT.name:
      case FORM_FIELDS.RELEASED_AT_ORIGINAL.name: {
        if (release[key] === value) {
          break;
        }

        init[key] = value;

        break;
      }

      case FORM_FIELDS.SITE_ID.name: {
        if (release.site?.id === value) {
          break;
        }

        init[key] = value;

        break;
      }

      default: {
        const exhaustiveCheck: never = key;
        throw new ProjectError(`Unknown field "${exhaustiveCheck}".`);
      }
    }

    return;
  }

  return (
    <Form<IFieldName, IReleaseUpdate, IRelease>
      isClient
      id={formID}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      validateInit={validateReleaseUpdate}
      getResult={async (init) => {
        const update = await fetchEditRelease(id, init);

        await onEdit?.(update);

        return update;
      }}
      resultView={(_, result) => <p>Successfully updated the release.</p>}
      resetButton="Edit again"
      {...blockProps}
    >
      {(formID) => (
        <>
          <SiteSection
            id={`${formID}-${FORM_FIELDS.SITE_ID.name}`}
            form={formID}
            name={FORM_FIELDS.SITE_ID.name}
            defaultValue={site?.id}
            onSiteChange={async (newSite) => changeSiteEntity(newSite)}
            onSiteClear={async () => changeSiteEntity(undefined)}
          >
            Site of the release.
          </SiteSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_ID.name}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_ID.name}
            label={FORM_FIELDS.ORIGINAL_ID.label}
            defaultValue={original_release_id}
          >
            {/* show URL template if any */}
            {siteEntity?.url_templates?.release && (
              <Pre>{siteEntity.url_templates.release}</Pre>
            )}
            Original ID of the release.
            {/* show URL template notes if any */}
            {siteEntity?.url_templates?.release_notes && (
              <Details summary="Notes">
                <p>{siteEntity.url_templates.release_notes}</p>
              </Details>
            )}
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
            defaultValue={title}
          >
            The title of the release.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.DESCRIPTION.name}`}
            form={formID}
            name={FORM_FIELDS.DESCRIPTION.name}
            label={FORM_FIELDS.DESCRIPTION.label}
            defaultValue={description}
          >
            The description of the release.
          </TextSection>

          <OriginalDateSection
            id={`${formID}-${FORM_FIELDS.RELEASED_AT_ORIGINAL.name}`}
            form={formID}
            name={FORM_FIELDS.RELEASED_AT_ORIGINAL.name}
            label={FORM_FIELDS.RELEASED_AT_ORIGINAL.label}
            defaultValue={released_at_original}
            timestampInputID={`${formID}-${FORM_FIELDS.RELEASED_AT.name}`}
            timestampInputName={FORM_FIELDS.RELEASED_AT.name}
            timestampInputLabel={FORM_FIELDS.RELEASED_AT.label}
            timestampInputChildren={<>Parsed release date.</>}
            parsedDateTime={released_at}
          >
            Original release date as plain text.
          </OriginalDateSection>
        </>
      )}
    </Form>
  );
}
