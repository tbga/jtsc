import {
  AccountURL,
  CreatePageURL,
  EditURL,
  ProjectURL,
  RemovePageURL,
  SearchURL,
} from "#lib/urls";
import { type IBigSerialInteger, type IBigIntegerPositive } from "#types";
import { type IRelease } from "./types";

export class ReleaseURL extends ProjectURL {
  constructor(language: string, release_id: IRelease["id"]) {
    super({ language, pathname: `/release/${release_id}` });
  }
}

export class ReleaseCreateURL extends CreatePageURL {
  constructor(language: string) {
    super({ language, pathname: "/releases" });
  }
}

export class ReleaseEditURL extends EditURL {
  constructor(
    language: string,
    release_id: IRelease["id"],
    hash?: "profiles" | "previews",
  ) {
    super({ language, pathname: `/release/${release_id}`, hash });
  }
}

export class ReleaseProfilesURL extends ProjectURL {
  constructor(
    language: string,
    release_id: IRelease["id"],
    page?: IBigIntegerPositive,
  ) {
    const pathname = !page
      ? `/release/${release_id}/profiles`
      : `/release/${release_id}/profiles/${page}`;
    super({
      language,
      pathname,
    });
  }
}

export class NewReleaseProfilesURL extends CreatePageURL {
  constructor(language: string, release_id: IRelease["id"]) {
    const pathname = `/release/${release_id}/profiles`;
    super({
      language,
      pathname,
    });
  }
}

export class RemoveReleaseProfilesURL extends RemovePageURL {
  constructor(language: string, release_id: IRelease["id"]) {
    const pathname = `/release/${release_id}/profiles`;
    super({
      language,
      pathname,
    });
  }
}

export class ReleasesURL extends ProjectURL {
  constructor(language: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/releases" : `/releases/${page}`;
    super({ language, pathname });
  }
}

export class ReleaseSearchURL extends SearchURL {
  constructor(language: string, query: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/releases" : `/releases/${page}`;
    const searchParams = new URLSearchParams([["query", query]]);
    super({ language, pathname, searchParams });
  }
}

export class ReleasePostsURL extends ProjectURL {
  constructor(
    language: string,
    release_id: IRelease["id"],
    page?: IBigIntegerPositive,
  ) {
    super({
      language,
      pathname: !page
        ? `/release/${release_id}/posts`
        : `/release/${release_id}/posts/${page}`,
    });
  }
}

export class NewReleasePostsURL extends CreatePageURL {
  constructor(language: string, release_id: IRelease["id"]) {
    super({ language, pathname: `/release/${release_id}/posts` });
  }
}

export class RemoveReleasePostsURL extends RemovePageURL {
  constructor(language: string, release_id: IRelease["id"]) {
    super({ language, pathname: `/release/${release_id}/posts` });
  }
}
