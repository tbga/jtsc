export {
  releaseInitSchema,
  validateReleaseInit,
} from "#codegen/schema/lib/entities/release/init";
export {
  releaseUpdateSchema,
  validateReleaseUpdate,
} from "#codegen/schema/lib/entities/release/update";
