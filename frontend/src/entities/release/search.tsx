import { useState } from "react";
import { type IEntityItem } from "#lib/entities";
import { useLanguage } from "#hooks";
import { SearchForm } from "#components/forms";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  fetchReleaseItemSearch,
  fetchReleaseSearchPagination,
} from "#api/account/search";
import { ListInternal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { Button, ButtonSwitch } from "#components/buttons";
import { BIGINT_ONE, type IAsyncReturnType } from "#types";
import { ReleaseURL } from "./urls";

import styles from "./search.module.scss";

interface ISearchSitesForm extends Omit<IBlockProps<"div">, "onSelect"> {
  onSelect?: (site: IEntityItem) => Promise<void>;
}

/**
 * @TODO close on select
 */
export const SearchReleasesForm = blockComponent(styles.block, Component);

function Component({ onSelect, ...blockProps }: ISearchSitesForm) {
  const { language } = useLanguage();
  const [isOpened, switchOpened] = useState(false);
  const [query, changeQuery] = useState<string>();
  const [releaseItems, changeReleaseItems] =
    useState<IAsyncReturnType<typeof fetchReleaseItemSearch>>();

  async function handleSearch(query: string) {
    const newSiteItems = await fetchReleaseItemSearch(
      query,
      String(BIGINT_ONE),
    );
    changeReleaseItems(newSiteItems);
    changeQuery(query);
  }

  return (
    <div {...blockProps}>
      <ButtonSwitch
        className={styles.switch}
        isOn={isOpened}
        onClick={() => {
          switchOpened(!isOpened);
        }}
      >
        {(isOn) => (isOn ? "Close" : "Search Releases")}
      </ButtonSwitch>
      {!isOpened ? undefined : (
        <>
          <SearchForm
            id="search-release-UpzdBvVvXQNPELNPGOEJ5"
            paginationFetch={async ({ query }) =>
              fetchReleaseSearchPagination(query)
            }
            getResult={async ({ query }) => handleSearch(query)}
          />
          {!releaseItems ? undefined : (
            <ListInternal
              pagination={releaseItems.pagination}
              onPageChange={async (page) => {
                const newReleaseItems = await fetchReleaseItemSearch(
                  query!,
                  page,
                );
                changeReleaseItems(newReleaseItems);
              }}
            >
              {releaseItems.releases.map((release) => (
                <EntityItem
                  key={release.id}
                  item={release}
                  urlBuilder={(id) => new ReleaseURL(language, id)}
                >
                  {onSelect && (
                    <Button
                      className={styles.select}
                      onClick={async () => {
                        onSelect(release);
                      }}
                    >
                      Select
                    </Button>
                  )}
                </EntityItem>
              ))}
            </ListInternal>
          )}
        </>
      )}
    </div>
  );
}
