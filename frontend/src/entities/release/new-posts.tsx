import { ProjectError } from "#lib/errors";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { isEmpty } from "#lib/util";
import { useLanguage } from "#hooks";
import { fetchCreateReleasePosts } from "#api/account/administrator/releases";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { type IPost, PostURL } from "#entities/post";
import { type IRelease } from "./types";

const FORM_FIELDS = {
  POST_IDS: { name: "post_ids", label: "Post IDs" },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  release_id: IRelease["id"];
}

export const NewReleasePostsForm = blockComponent(undefined, Component);

function Component({ release_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();
  async function collectInit(
    init: { post_ids: IPost["id"][] },
    key: IFieldName,
    value: string,
  ) {
    switch (key) {
      case FORM_FIELDS.POST_IDS.name: {
        const post_ids = collectIDs(value);

        if (isEmpty(post_ids)) {
          break;
        }

        init.post_ids = post_ids;

        break;
      }

      default: {
        throw new ProjectError(`Illegal form key "${key}".`);
      }
    }
  }

  return (
    <Form<IFieldName, { post_ids: IPost["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ post_ids }) =>
        fetchCreateReleasePosts(release_id, post_ids)
      }
      resultView={(_, result) => (
        <>
          <p>New posts were added to the release:</p>
          <ListLocal
            items={result.map((item) => (
              <EntityItem
                key={item.id}
                item={item}
                urlBuilder={(id) => new PostURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${formID}-${FORM_FIELDS.POST_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.POST_IDS.name}
          label={FORM_FIELDS.POST_IDS.label}
        >
          Newline-separated list of post IDs.
        </TextSection>
      )}
    </Form>
  );
}
