import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Heading } from "#components";
import { Link } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  Item,
  List,
  type ICardProps,
} from "#components/lists";
import { EntityID } from "#components/entities";
import { PublicImportStatus } from "./status";
import { PublicImportURL } from "./urls";
import { type IPublicImportPreview } from "./types";

import styles from "./card.module.scss";

interface IProps extends Omit<ICardProps, "id"> {
  publicImport: IPublicImportPreview;
}

export const PublicImportCard = blockComponent(styles.block, Component);

function Component({
  publicImport,
  headingLevel,
  children,
  ...blockProps
}: IProps) {
  const { language } = useLanguage();
  const { id, title, created_by, sites, public_id, status } = publicImport;

  return (
    <Card {...blockProps} id={`public-import-${id}`}>
      <CardHeader>
        <Heading>{title}</Heading>
      </CardHeader>

      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS dKey="Public ID" dValue={public_id} />
          <DS
            isHorizontal
            dKey="Status"
            dValue={<PublicImportStatus status={status} />}
          />
          <DS isHorizontal dKey="Creator" dValue={created_by} />
          <DS isHorizontal dKey="Sites" dValue={sites} />
        </DL>
      </CardBody>

      <CardFooter>
        <List>
          <Item className={styles.item}>
            <Link
              className={styles.details}
              href={new PublicImportURL(language, id)}
              target="_blank"
            >
              Details
            </Link>
          </Item>
        </List>
        {children}
      </CardFooter>
    </Card>
  );
}
