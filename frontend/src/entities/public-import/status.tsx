import clsx from "clsx";
import { ProjectError } from "#lib/errors";
import { isIncludedInArray } from "#lib/std/array";
import { type IBlockProps, blockComponent } from "#components/meta";
import { publicImportStatuses, type IPublicImportStatus } from "./types";

import styles from "./status.module.scss";

interface IProps extends IBlockProps<"span"> {
  status: IPublicImportStatus;
}

export const PublicImportStatus = blockComponent(styles.block, Component);

function Component({ status, className, ...blockProps }: IProps) {
  let statusClass;

  if (!isIncludedInArray(publicImportStatuses, status)) {
    throw new ProjectError(
      `Unknown public import status "${status satisfies never}".`,
    );
  } else {
    statusClass = styles[status];
  }

  const finalClass = clsx(className, statusClass);

  return (
    <span className={finalClass} {...blockProps}>
      {status}
    </span>
  );
}
