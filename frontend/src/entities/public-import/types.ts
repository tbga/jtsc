import { publicImportStatusSchema } from "#codegen/schema/lib/entities/public-import/status";
import { publicImportSiteCategorySchema } from "#codegen/schema/lib/entities/public-import/site/category";

export type { IPublicImport } from "#codegen/schema/lib/entities/public-import/entity";
export type { IPublicImportPreview } from "#codegen/schema/lib/entities/public-import/preview";
export type { IPublicImportStats } from "#codegen/schema/lib/entities/public-import/stats";
export type { IPublicImportStatus } from "#codegen/schema/lib/entities/public-import/status";
export type { IPublicImportSite } from "#codegen/schema/lib/entities/public-import/site/entity";
export type { IPublicImportSitePreview } from "#codegen/schema/lib/entities/public-import/site/preview";
export type { IPublicImportSiteCategory } from "#codegen/schema/lib/entities/public-import/site/category";

export const publicImportStatuses = publicImportStatusSchema.anyOf.map(
  (value) => value.const,
);
export const publicImportSiteCategories =
  publicImportSiteCategorySchema.anyOf.map((value) => value.const);
