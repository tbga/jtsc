import { IPublicImportSiteCategoryAPI } from "#codegen/schema/lib/entities/public-import/site/category-api";
import { AdministratorURL, CreatePageURL } from "#lib/urls";
import { type ISite } from "#entities/site";
import { type IBigSerialInteger } from "#types";
import { type IPublicImport } from "./types";

export class PublicImportCreateURL extends CreatePageURL {
  constructor(language: string) {
    const pathname = "/public-imports";
    super({ language, pathname });
  }
}

export class PublicImportURL extends AdministratorURL {
  constructor(language: string, id: IPublicImport["id"], pathname?: string) {
    const finalPathname = !pathname
      ? `/public-import/${id}`
      : `/public-import/${id}${pathname}`;
    super({ language, pathname: finalPathname });
  }
}

export class PublicImportSiteURL extends PublicImportURL {
  constructor(
    language: string,
    public_import_id: IPublicImport["id"],
    site_id: ISite["id"],
  ) {
    const pathname = `/site/${site_id}`;
    super(language, public_import_id, pathname);
  }
}

export class PublicImportSitesURL extends PublicImportURL {
  constructor(
    language: string,
    public_import_id: IPublicImport["id"],
    category: IPublicImportSiteCategoryAPI,
    page?: IBigSerialInteger,
  ) {
    const pathname = !page
      ? `/sites/${category}`
      : `/sites/${category}/${page}`;
    super(language, public_import_id, pathname);
  }
}

export class PublicImportsURL extends AdministratorURL {
  constructor(language: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/public-imports" : `/public-imports/${page}`;
    super({ language, pathname });
  }
}
