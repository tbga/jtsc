import { useEffect, useState } from "react";
import { ProjectError } from "#lib/errors";
import { entityTitle } from "#lib/entities";
import {
  fetchPublicImportSiteApprove,
  fetchPublicImportSiteReject,
} from "#api/account/administrator/public-imports";
import { fetchSite } from "#api/sites";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  Pre,
  type IArticleProps,
  ArticleFooter,
} from "#components";
import { EntityID, EntityItem } from "#components/entities";
import { DL, DS, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { DateTime } from "#components/dates";
import { Link } from "#components/links";
import { ButtonNegative, ButtonPositive } from "#components/buttons";
import { Difference } from "#components/difference";
import { PublicImportURL } from "#entities/public-import";
import { type ISite, SiteURL } from "#entities/site";
import { ApprovalStatus } from "./approval-status";
import { type IPublicImportSite } from "../types";

import styles from "./article.module.scss";

interface IProps extends IArticleProps {
  site: IPublicImportSite;
  onApprovalChange?: (updatedSite: IPublicImportSite) => Promise<void>;
}

export const PublicImportSiteArticle = blockComponent<IProps>(
  styles.block,
  Component,
);

function Component({
  site,
  headingLevel = 2,
  onApprovalChange,
  ...blockProps
}: IProps) {
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const [localSite, changeLocalSite] = useState<ISite>();
  const {
    id,
    public_import,
    local_site,
    category,
    approval_status,
    title,
    url_templates,
  } = site;
  const isDifferent = category === "different";
  const isSameTitle = !isDifferent || !localSite || localSite.title === title;

  useEffect(() => {
    // only `"different"` sites need a local site
    if (!isDifferent) {
      return;
    }

    if (!local_site) {
      throw new ProjectError(
        `Public import site ${entityTitle(
          id,
          title,
        )} of category "${category}" does not have a local site.`,
      );
    }

    (async () => {
      const newLocalSite = await fetchSite(local_site.id);
      changeLocalSite(newLocalSite);
    })();
  }, [id]);

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>
          {isSameTitle ? (
            title
          ) : (
            <Difference oldValue={localSite.title} newValue={title} />
          )}
        </Heading>
      </ArticleHeader>

      <ArticleBody>
        <EntityID>{id}</EntityID>

        <Heading
          // @ts-expect-error tuple type
          level={headingLevel + 1}
        >
          Meta
        </Heading>
        <Meta site={site} />

        <Heading
          // @ts-expect-error tuple type
          level={headingLevel + 1}
        >
          Details
        </Heading>
        <Details site={site} localSite={localSite} />

        {url_templates && (
          <>
            <Heading
              // @ts-expect-error tuple type
              level={headingLevel + 1}
            >
              URL Templates
            </Heading>
            <URLTemplates
              isDifferent={isDifferent}
              urlTemplates={url_templates}
              localSite={localSite}
            />
          </>
        )}
      </ArticleBody>

      <ArticleFooter>
        <ListButtons isSpread>
          <ButtonNegative
            disabled={approval_status === "rejected" || category === "same"}
            onClick={async () => {
              const updatedSite = await fetchPublicImportSiteReject(
                public_import.id,
                id,
              );

              await onApprovalChange?.(updatedSite);
            }}
          >
            Reject
          </ButtonNegative>

          <ButtonPositive
            disabled={approval_status === "approved" || category === "same"}
            onClick={async () => {
              const updatedSite = await fetchPublicImportSiteApprove(
                public_import.id,
                id,
              );

              await onApprovalChange?.(updatedSite);
            }}
          >
            Approve
          </ButtonPositive>
        </ListButtons>
      </ArticleFooter>
    </Article>
  );
}

interface IMetaProps {
  site: IPublicImportSite;
}

function Meta({ site }: IMetaProps) {
  const { language } = useLanguage();
  const {
    public_import,
    public_id,
    local_site,
    category,
    approval_status,
    created_at,
    updated_at,
  } = site;

  return (
    <DL>
      <DS
        dKey="Public Import"
        dValue={
          <Link href={new PublicImportURL(language, public_import.id)}>
            &quot;{public_import.name}&quot; ({public_import.id})
          </Link>
        }
      />
      <DS dKey="Public ID" dValue={public_id} />
      <DS
        dKey="Local Site"
        dValue={
          local_site && (
            <EntityItem
              item={local_site}
              urlBuilder={(id) => new SiteURL(language, id)}
            />
          )
        }
      />
      <DS isHorizontal dKey="Category" dValue={category} />
      <DS
        isHorizontal
        dKey="Approval Status"
        dValue={<ApprovalStatus approvalStatus={approval_status} />}
      />
      <DS dKey="Creation date" dValue={<DateTime dateTime={created_at} />} />
      <DS dKey="Last updated" dValue={<DateTime dateTime={updated_at} />} />
    </DL>
  );
}

interface IDetailsProps {
  site: IPublicImportSite;
  localSite?: ISite;
}

function Details({ site, localSite }: IDetailsProps) {
  const { category, home_page, long_title, description } = site;
  const isDifferent = category === "different";
  const isSameHomepage =
    !isDifferent || !localSite || localSite.home_page === home_page;
  const isSameLongTitle =
    !isDifferent || !localSite || localSite.long_title === long_title;
  const isSameLongDescription =
    !isDifferent || !localSite || localSite.description === description;

  return (
    <DL>
      <DS
        dKey="Home Page"
        dValue={
          isSameHomepage ? (
            <Link href={home_page} target="_blank">
              {home_page}
            </Link>
          ) : (
            <Difference
              oldValue={
                <Link href={home_page} target="_blank">
                  {localSite.home_page}
                </Link>
              }
              newValue={
                <Link href={home_page} target="_blank">
                  {home_page}
                </Link>
              }
            />
          )
        }
      />
      <DS
        dKey="Long Title"
        dValue={
          isSameLongTitle ? (
            long_title
          ) : (
            <Difference oldValue={localSite.long_title} newValue={long_title} />
          )
        }
      />
      <DS
        dKey="Description"
        dValue={
          isSameLongDescription ? (
            description
          ) : (
            <Difference
              oldValue={localSite.description}
              newValue={description}
            />
          )
        }
      />
    </DL>
  );
}

interface IURLTemplatesProps {
  isDifferent: boolean;
  urlTemplates: Required<IPublicImportSite>["url_templates"];
  localSite?: ISite;
}

function URLTemplates({
  isDifferent,
  urlTemplates,
  localSite,
}: IURLTemplatesProps) {
  const {
    profile_list,
    profile,
    release_list,
    release,
    profile_list_notes,
    profile_notes,
    release_list_notes,
    release_notes,
  } = urlTemplates;
  const isSameProfileList =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.profile_list === profile_list;
  const isSameProfile =
    !isDifferent || !localSite || localSite.url_templates?.profile === profile;
  const isSameReleaseList =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.release_list === release_list;
  const isSameRelease =
    !isDifferent || !localSite || localSite.url_templates?.release === release;
  const isSameProfileListNotes =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.profile_list_notes === profile_list_notes;
  const isSameProfileNotes =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.profile_notes === profile_notes;
  const isSameReleaseListNotes =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.release_list_notes === release_list_notes;
  const isSameReleaseNotes =
    !isDifferent ||
    !localSite ||
    localSite.url_templates?.release_notes === release_notes;

  return (
    <DL>
      <DS
        isHorizontal
        dKey="Profile List"
        dValue={
          isSameProfileList ? (
            urlTemplates.profile_list && <Pre>{urlTemplates.profile_list}</Pre>
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.profile_list && (
                  <Pre>{localSite.url_templates.profile_list}</Pre>
                )
              }
              newValue={
                urlTemplates.profile_list && (
                  <Pre>{urlTemplates.profile_list}</Pre>
                )
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Profile List Notes"
        dValue={
          isSameProfileListNotes ? (
            urlTemplates.profile_list_notes && (
              <Pre>{urlTemplates.profile_list_notes}</Pre>
            )
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.profile_list_notes && (
                  <Pre>{localSite.url_templates.profile_list_notes}</Pre>
                )
              }
              newValue={
                urlTemplates.profile_list_notes && (
                  <Pre>{urlTemplates.profile_list_notes}</Pre>
                )
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Profile"
        dValue={
          isSameProfile ? (
            urlTemplates.profile && <Pre>{urlTemplates.profile}</Pre>
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.profile && (
                  <Pre>{localSite.url_templates.profile}</Pre>
                )
              }
              newValue={
                urlTemplates.profile && <Pre>{urlTemplates.profile}</Pre>
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Profile Notes"
        dValue={
          isSameProfileNotes ? (
            urlTemplates.profile_notes && (
              <Pre>{urlTemplates.profile_notes}</Pre>
            )
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.profile_notes && (
                  <Pre>{localSite.url_templates.profile_notes}</Pre>
                )
              }
              newValue={
                urlTemplates.profile_notes && (
                  <Pre>{urlTemplates.profile_notes}</Pre>
                )
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Release List"
        dValue={
          isSameReleaseList ? (
            urlTemplates.release_list && <Pre>{urlTemplates.release_list}</Pre>
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.release_list && (
                  <Pre>{localSite.url_templates.release_list}</Pre>
                )
              }
              newValue={
                urlTemplates.release_list && (
                  <Pre>{urlTemplates.release_list}</Pre>
                )
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Release List Notes"
        dValue={
          isSameReleaseListNotes ? (
            urlTemplates.release_list_notes && (
              <Pre>{urlTemplates.release_list_notes}</Pre>
            )
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.release_list_notes && (
                  <Pre>{localSite.url_templates.release_list_notes}</Pre>
                )
              }
              newValue={
                urlTemplates.release_list_notes && (
                  <Pre>{urlTemplates.release_list_notes}</Pre>
                )
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Release"
        dValue={
          isSameRelease ? (
            urlTemplates.release && <Pre>{urlTemplates.release}</Pre>
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.release && (
                  <Pre>{localSite.url_templates.release}</Pre>
                )
              }
              newValue={
                urlTemplates.release && <Pre>{urlTemplates.release}</Pre>
              }
            />
          )
        }
      />

      <DS
        isHorizontal
        dKey="Release"
        dValue={
          isSameReleaseNotes ? (
            urlTemplates.release_notes && (
              <Pre>{urlTemplates.release_notes}</Pre>
            )
          ) : (
            <Difference
              oldValue={
                localSite.url_templates?.release_notes && (
                  <Pre>{localSite.url_templates.release_notes}</Pre>
                )
              }
              newValue={
                urlTemplates.release_notes && (
                  <Pre>{urlTemplates.release_notes}</Pre>
                )
              }
            />
          )
        }
      />
    </DL>
  );
}
