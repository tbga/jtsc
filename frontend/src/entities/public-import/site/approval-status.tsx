import clsx from "clsx";
import { ProjectError } from "#lib/errors";
import { type IBlockProps, blockComponent } from "#components/meta";
import { type IPublicImportSiteApprovalStatus } from "./types";

import styles from "./approval-status.module.scss";

interface IProps extends IBlockProps<"span"> {
  approvalStatus: IPublicImportSiteApprovalStatus;
}

export const ApprovalStatus = blockComponent(styles.block, Component);

function Component({ approvalStatus, className, ...blockProps }: IProps) {
  let statusClass;

  switch (approvalStatus) {
    case "undecided": {
      statusClass = styles[approvalStatus];
      break;
    }

    case "approved": {
      statusClass = styles[approvalStatus];
      break;
    }

    case "rejected": {
      statusClass = styles[approvalStatus];
      break;
    }

    default: {
      throw new ProjectError(
        `Unknown approval status "${approvalStatus satisfies never}".`,
      );
    }
  }

  const finalClass = clsx(className, statusClass);

  return (
    <span className={finalClass} {...blockProps}>
      {approvalStatus}
    </span>
  );
}
