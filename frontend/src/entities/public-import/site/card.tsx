import { useLanguage } from "#hooks";
import { Heading } from "#components";
import { blockComponent } from "#components/meta";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  List,
  type ICardProps,
  Item,
  DL,
  DS,
} from "#components/lists";
import { EntityID, EntityItem } from "#components/entities";
import { Link } from "#components/links";
import { PublicImportSiteURL, PublicImportURL } from "#entities/public-import";
import { SiteURL } from "#entities/site";
import { ApprovalStatus } from "./approval-status";
import { type IPublicImportSitePreview } from "../types";

import styles from "./card.module.scss";

interface IProps extends ICardProps {
  site: IPublicImportSitePreview;
}

export const PublicImportSiteCard = blockComponent(styles.block, Component);

function Component({ site, headingLevel, children, ...blockProps }: IProps) {
  const { language } = useLanguage();
  const {
    id,
    title,
    public_id,
    public_import,
    local_site,
    category,
    approval_status,
  } = site;

  return (
    <Card {...blockProps}>
      <CardHeader>
        <Heading level={headingLevel}>{title}</Heading>
      </CardHeader>

      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS
            dKey="Public Import"
            dValue={
              <Link href={new PublicImportURL(language, public_import.id)}>
                &quot;{public_import.name}&quot; ({public_import.id})
              </Link>
            }
          />
          <DS dKey="Public ID" dValue={public_id} />
          <DS
            dKey="Local Site"
            dValue={
              local_site && (
                <EntityItem
                  item={local_site}
                  urlBuilder={(id) => new SiteURL(language, id)}
                />
              )
            }
          />
          <DS isHorizontal dKey="Category" dValue={category} />
          <DS
            isHorizontal
            dKey="Approval Status"
            dValue={<ApprovalStatus approvalStatus={approval_status} />}
          />
        </DL>
      </CardBody>

      <CardFooter>
        <List>
          <Item>
            <Link
              className={styles.details}
              href={new PublicImportSiteURL(language, public_import.id, id)}
              target="_blank"
            >
              Details
            </Link>
          </Item>
        </List>
        {children}
      </CardFooter>
    </Card>
  );
}
