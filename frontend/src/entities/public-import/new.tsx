import { ProjectError } from "#lib/errors";
import { type IEntityItem } from "#lib/entities";
import { type IPath } from "#lib/local-file-system";
import { fetchPublicImportCreate } from "#api/account/administrator/public-imports";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { Link } from "#components/links";
import { OperationURL } from "#entities/operation";

interface Init {
  fileURL: IPath;
}

const FORM_FIELDS = {
  FILE_URL: {
    name: "file_url",
    label: "File URL",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {}

export const NewPublicImportForm = blockComponent(undefined, Component);

function Component({ id, ...blockProps }: IProps) {
  const { language } = useLanguage();

  return (
    <Form<IFieldName, Init, IEntityItem>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ fileURL }) => fetchPublicImportCreate(fileURL)}
      resultView={(_, { id, name }) => (
        <p>
          An operation{" "}
          <Link href={new OperationURL(language, id)} target="_blank">
            &quot;{name}&quot; ({id})
          </Link>{" "}
          for import creation was added to queue.
        </p>
      )}
      resetButton={"Create new one"}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${formID}-${FORM_FIELDS.FILE_URL.name}`}
            form={formID}
            name={FORM_FIELDS.FILE_URL.name}
            label={FORM_FIELDS.FILE_URL.label}
          >
            A file URL to the import folder.
          </TextSection>
        </>
      )}
    </Form>
  );
}

function collectInit(init: Init, fieldName: IFieldName, value: string) {
  switch (fieldName) {
    case FORM_FIELDS.FILE_URL.name: {
      init.fileURL = value;
      break;
    }

    default: {
      throw new ProjectError(
        `Unknown field name "${fieldName satisfies never}".`,
      );
    }
  }
}
