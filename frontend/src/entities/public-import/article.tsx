import { useEffect, useState } from "react";
import {
  fetchPublicImport,
  fetchPublicImportConsume,
  fetchPublicImportStats,
} from "#api/account/administrator/public-imports";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleHeader,
  Heading,
  LoadingBar,
  type IArticleProps,
  ArticleFooter,
} from "#components";
import { EntityID } from "#components/entities";
import { DL, DS, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { DateTime } from "#components/dates";
import { Link } from "#components/links";
import { Button } from "#components/buttons";
import { PublicImportSitesURL } from "#entities/public-import";
import { BIGINT_ZERO, IAsyncReturnType } from "#types";
import { PublicImportStatus } from "./status";
import { type IPublicImport } from "./types";

import styles from "./article.module.scss";

interface IProps extends IArticleProps {
  publicImport: IPublicImport;
  onPublicImportChange?: (changedImport: IPublicImport) => Promise<void>;
}

export const PublicImportArticle = blockComponent<IProps>(undefined, Component);

function Component({
  publicImport,
  headingLevel = 2,
  onPublicImportChange,
  ...blockProps
}: IProps) {
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const [stats, changeStats] =
    useState<IAsyncReturnType<typeof fetchPublicImportStats>>();
  const {
    id,
    title,
    status,
    created_at,
    created_by,
    updated_at,
    public_id,
    type,
    version,
    is_consumable,
  } = publicImport;

  useEffect(() => {
    (async () => {
      const newStats = await fetchPublicImportStats(id);
      changeStats(newStats);
    })();
  }, [id]);

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{title}</Heading>
        <EntityID>{id}</EntityID>
      </ArticleHeader>
      <ArticleBody>
        <DL>
          <DS dKey="Public ID" dValue={public_id} />
          <DS
            isHorizontal
            dKey="Status"
            dValue={<PublicImportStatus status={status} />}
          />
          <DS isHorizontal dKey="Version" dValue={version} />
          <DS isHorizontal dKey="Type" dValue={type} />
          <DS isHorizontal dKey="Creator" dValue={created_by} />
          <DS
            dKey="Creation date"
            dValue={<DateTime dateTime={created_at} />}
          />
          <DS dKey="Last updated" dValue={<DateTime dateTime={updated_at} />} />
          {status !== "consumed" && (
            <DS
              dKey="Sites"
              dValue={
                <SiteStats publicImportID={publicImport.id} stats={stats} />
              }
            />
          )}
        </DL>
      </ArticleBody>

      <ArticleFooter>
        <ListButtons>
          <Button
            disabled={status !== "pending" || !is_consumable}
            onClick={async () => {
              await fetchPublicImportConsume(id);

              if (onPublicImportChange) {
                const updatedPublicImport = await fetchPublicImport(id);
                await onPublicImportChange(updatedPublicImport);
              }
            }}
          >
            Consume
          </Button>
        </ListButtons>
      </ArticleFooter>
    </Article>
  );
}

interface ISiteStatsProps {
  publicImportID: IPublicImport["id"];
  stats?: IAsyncReturnType<typeof fetchPublicImportStats>;
}

function SiteStats({ stats, publicImportID }: ISiteStatsProps) {
  const { language } = useLanguage();

  return !stats ? (
    <LoadingBar />
  ) : BigInt(stats.sites.all) === BIGINT_ZERO ? (
    <>{stats.sites.all}</>
  ) : (
    <DL className={styles.sublist}>
      <DS
        isHorizontal
        dKey="All"
        dValue={
          <Link
            href={new PublicImportSitesURL(language, publicImportID, "all")}
          >
            {stats.sites.all}
          </Link>
        }
      />

      <DS
        isHorizontal
        dKey="New"
        dValue={
          BigInt(stats.sites.new) === BIGINT_ZERO ? (
            <>{stats.sites.new}</>
          ) : (
            <Link
              href={new PublicImportSitesURL(language, publicImportID, "new")}
            >
              {stats.sites.new}
            </Link>
          )
        }
      />

      <DS
        isHorizontal
        dKey="Same"
        dValue={
          BigInt(stats.sites.same) === BIGINT_ZERO ? (
            <>{stats.sites.same}</>
          ) : (
            <Link
              href={new PublicImportSitesURL(language, publicImportID, "same")}
            >
              {stats.sites.same}
            </Link>
          )
        }
      />

      <DS
        isHorizontal
        dKey="Different"
        dValue={
          BigInt(stats.sites.different) === BIGINT_ZERO ? (
            <>{stats.sites.different}</>
          ) : (
            <Link
              href={
                new PublicImportSitesURL(language, publicImportID, "different")
              }
            >
              {stats.sites.different}
            </Link>
          )
        }
      />
    </DL>
  );
}
