import {
  AdministratorURL,
  CreatePageURL,
  EditURL,
  ProjectURL,
} from "#lib/urls";
import { type IBigIntegerPositive } from "#types";
import { type ISite } from "./types";

export class SiteURL extends ProjectURL {
  constructor(language: string, id: ISite["id"]) {
    super({ language, pathname: `/site/${id}` });
  }
}

/**
 * @TODO create page URL
 */
export class SiteCreateURL extends AdministratorURL {
  constructor(language: string) {
    super({ language, pathname: "/create/sites" });
  }
}

export class SiteEditURL extends EditURL {
  constructor(language: string, id: ISite["id"]) {
    super({ language, pathname: `/site/${id}` });
  }
}

export class SiteProfilesURL extends ProjectURL {
  constructor(language: string, id: ISite["id"], page?: IBigIntegerPositive) {
    super({
      language,
      pathname: !page ? `/site/${id}/profiles` : `/site/${id}/profiles/${page}`,
    });
  }
}

export class SiteProfilesCreateURL extends CreatePageURL {
  constructor(language: string, id: ISite["id"]) {
    super({ language, pathname: `/site/${id}/profiles` });
  }
}

export class SiteReleasesURL extends ProjectURL {
  constructor(language: string, id: ISite["id"], page?: IBigIntegerPositive) {
    super({
      language,
      pathname: !page ? `/site/${id}/releases` : `/site/${id}/releases/${page}`,
    });
  }
}

export class SiteReleasesCreateURL extends CreatePageURL {
  constructor(language: string, id: ISite["id"]) {
    super({ language, pathname: `/site/${id}/releases` });
  }
}

export class SitesURL extends ProjectURL {
  constructor(language: string, page?: IBigIntegerPositive) {
    super({ language, pathname: !page ? "/sites" : `/sites/${page}` });
  }
}
