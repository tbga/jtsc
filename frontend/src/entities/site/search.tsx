import { useState } from "react";
import { type IEntityItem } from "#lib/entities";
import { useLanguage } from "#hooks";
import { Details } from "#components";
import { SearchForm } from "#components/forms";
import { blockComponent, IBlockProps } from "#components/meta";
import {
  fetchSiteItemSearch,
  fetchSiteSearchPagination,
} from "#api/account/search";
import { ListInternal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { Button } from "#components/buttons";
import { BIGINT_ONE, type IAsyncReturnType } from "#types";
import { SiteURL } from "./urls";

import styles from "./search.module.scss";

interface ISearchSitesForm extends Omit<IBlockProps<"div">, "onSelect"> {
  id: string;
  isAutoSelectEnabled?: boolean;
  onSelect?: (site: IEntityItem) => Promise<void>;
}

export const SearchSitesForm = blockComponent(styles.block, Component);

function Component({
  id,
  isAutoSelectEnabled,
  onSelect,
  ...blockProps
}: ISearchSitesForm) {
  const { language } = useLanguage();
  const [isOpened, switchOpened] = useState(false);
  const [query, changeQuery] = useState<string>();
  const [siteItems, changeSiteItems] =
    useState<IAsyncReturnType<typeof fetchSiteItemSearch>>();

  async function handleSearch(query: string) {
    const newSiteItems = await fetchSiteItemSearch(query, "1");

    // autoselect if there is only one result
    if (
      isAutoSelectEnabled &&
      BigInt(newSiteItems.pagination.total_count) == BIGINT_ONE
    ) {
      await onSelect?.(newSiteItems.sites[0]);
      switchOpened(false);
    }

    changeSiteItems(newSiteItems);
    changeQuery(query);
  }

  return (
    <div {...blockProps}>
      <Details summary="Search Sites" open={isOpened}>
        <SearchForm
          id={id}
          className={styles.form}
          paginationFetch={async ({ query }) =>
            fetchSiteSearchPagination(query)
          }
          getResult={async ({ query }) => handleSearch(query)}
        />
        {!siteItems ? undefined : (
          <ListInternal
            pagination={siteItems.pagination}
            onPageChange={async (page) => {
              const newSiteItems = await fetchSiteItemSearch(
                query as string,
                page,
              );
              changeSiteItems(newSiteItems);
            }}
          >
            {siteItems.sites.map((site) => (
              <EntityItem
                key={site.id}
                item={site}
                urlBuilder={(id) => new SiteURL(language, id)}
              >
                {onSelect && (
                  <Button
                    className={styles.select}
                    onClick={async () => {
                      await onSelect(site);
                      switchOpened(false);
                    }}
                  >
                    Select
                  </Button>
                )}
              </EntityItem>
            ))}
          </ListInternal>
        )}
      </Details>
    </div>
  );
}
