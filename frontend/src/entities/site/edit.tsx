import { ProjectError } from "#lib/errors";
import { toURLTemplateValue } from "#lib/urls/templates";
import { fetchSiteUpdate } from "#api/account/administrator/sites";
import { Details } from "#components";
import { Fieldset, Form, Legend, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { blockComponent } from "#components/meta";
import { Link } from "#components/links";
import { Item, List } from "#components/lists";
import { validateSiteUpdate } from "./lib";
import { type ISiteURLTemplates, type ISite, type ISiteUpdate } from "./types";

const FORM_FIELDS = {
  HOME_PAGE: {
    name: "home_page",
    label: "Home page",
  },
  TITLE: {
    name: "title",
    label: "Title",
  },
  LONG_TITLE: {
    name: "long_title",
    label: "Long title",
  },
  DESCRIPTION: {
    name: "description",
    label: "Description",
  },
  TEMPLATE_PROFILE_LIST: {
    name: "template_profile_list",
    label: "Profile list",
  },
  TEMPLATE_PROFILE_LIST_NOTES: {
    name: "template_profile_list_notes",
    label: "Profile list notes",
  },
  TEMPLATE_PROFILE: {
    name: "template_profile",
    label: "Profile",
  },
  TEMPLATE_PROFILE_NOTES: {
    name: "template_profile_notes",
    label: "Profile notes",
  },
  TEMPLATE_RELEASE_LIST: {
    name: "template_release_list",
    label: "Release list",
  },
  TEMPLATE_RELEASE_LIST_NOTES: {
    name: "template_release_list_notes",
    label: "Release list notes",
  },
  TEMPLATE_RELEASE: {
    name: "template_release",
    label: "Release",
  },
  TEMPLATE_RELEASE_NOTES: {
    name: "template_release_notes",
    label: "Release notes",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IEditSiteFormProps extends IBaseFormProps {
  site: ISite;
}

export const EditSiteForm = blockComponent(undefined, Component);

function Component({ site, id: formID, ...blockProps }: IEditSiteFormProps) {
  const { id, home_page, title, long_title, description, url_templates } = site;

  function collectInit(init: ISiteUpdate, key: IFieldName, value: string) {
    switch (key) {
      case FORM_FIELDS.HOME_PAGE.name:
      case FORM_FIELDS.TITLE.name:
      case FORM_FIELDS.LONG_TITLE.name:
      case FORM_FIELDS.DESCRIPTION.name: {
        if (site[key] === value) {
          break;
        }

        init[key] = value;
        break;
      }

      case FORM_FIELDS.TEMPLATE_PROFILE_LIST.name:
      case FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name:
      case FORM_FIELDS.TEMPLATE_PROFILE.name:
      case FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name:
      case FORM_FIELDS.TEMPLATE_RELEASE_LIST.name:
      case FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name:
      case FORM_FIELDS.TEMPLATE_RELEASE.name:
      case FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name: {
        const keyName = key.slice(
          "template_".length,
        ) as keyof ISiteURLTemplates;

        if (url_templates?.[keyName] === value) {
          break;
        }

        if (!init.url_templates) {
          init.url_templates = {};
        }

        init.url_templates[keyName] = value;

        break;
      }

      default: {
        throw new ProjectError(`Unknown field name "${key satisfies never}".`);
      }
    }
  }

  return (
    <Form<IFieldName, ISiteUpdate, ISite>
      isClient
      id={formID}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      validateInit={validateSiteUpdate}
      getResult={async (init) => fetchSiteUpdate(id, init)}
      resultView={() => <p>Site was successfully updated.</p>}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${formID}-${FORM_FIELDS.HOME_PAGE.name}`}
            form={formID}
            name={FORM_FIELDS.HOME_PAGE.name}
            label={FORM_FIELDS.HOME_PAGE.label}
            defaultValue={home_page}
          >
            The URL of the site&apos;s home page.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
            defaultValue={title}
          >
            The title of the site.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.LONG_TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.LONG_TITLE.name}
            label={FORM_FIELDS.LONG_TITLE.label}
            defaultValue={long_title}
          >
            The long title of the site.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.DESCRIPTION.name}`}
            form={formID}
            name={FORM_FIELDS.DESCRIPTION.name}
            label={FORM_FIELDS.DESCRIPTION.label}
            defaultValue={description}
          >
            The description of the site.
          </TextSection>

          <Fieldset>
            <Legend>URL Templates</Legend>
            <Details summary="Available Values">
              <p>
                All templates support these values of the homepage{" "}
                <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL">
                  URL
                </Link>
                :
                <List>
                  <Item>
                    <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/protocol">
                      {toURLTemplateValue("protocol")}
                    </Link>
                  </Item>
                  <Item>
                    <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/host">
                      {toURLTemplateValue("host")}
                    </Link>
                  </Item>
                  <Item>
                    <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/origin">
                      {toURLTemplateValue("origin")}
                    </Link>
                  </Item>
                </List>
              </p>
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_LIST.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_PROFILE_LIST.name}
              label={FORM_FIELDS.TEMPLATE_PROFILE_LIST.label}
              defaultValue={url_templates?.profile_list}
            />
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.label}
                defaultValue={url_templates?.profile_list_notes}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_PROFILE.name}
              label={FORM_FIELDS.TEMPLATE_PROFILE.label}
              defaultValue={url_templates?.profile}
            />
            <Details summary="Extra values">
              <List>
                <Item>{toURLTemplateValue("profile_id")}</Item>
              </List>
            </Details>
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_PROFILE_NOTES.label}
                defaultValue={url_templates?.profile_notes}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_LIST.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_RELEASE_LIST.name}
              label={FORM_FIELDS.TEMPLATE_RELEASE_LIST.label}
              defaultValue={url_templates?.release_list}
            />
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.label}
                defaultValue={url_templates?.release_list_notes}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_RELEASE.name}
              label={FORM_FIELDS.TEMPLATE_RELEASE.label}
              defaultValue={url_templates?.release}
            />
            <Details summary="Extra values">
              <List>
                <Item>{toURLTemplateValue("release_id")}</Item>
              </List>
            </Details>
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_RELEASE_NOTES.label}
                defaultValue={url_templates?.release_notes}
              />
            </Details>
          </Fieldset>
        </>
      )}
    </Form>
  );
}
