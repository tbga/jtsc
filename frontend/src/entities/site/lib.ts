export {
  siteInitSchema,
  validateSiteInit,
} from "#codegen/schema/lib/entities/site/init";
export { validateSiteUpdate } from "#codegen/schema/lib/entities/site/update";
