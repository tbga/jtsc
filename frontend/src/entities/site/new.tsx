import { ProjectError } from "#lib/errors";
import { toURLTemplateValue } from "#lib/urls/templates";
import { fetchNewSite } from "#api/account/administrator/sites";
import { useLanguage } from "#hooks";
import { Details } from "#components";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form, Fieldset, Legend } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { Link, LinkInternal } from "#components/links";
import { Item, List } from "#components/lists";
import { validateSiteInit } from "./lib";
import { SiteURL } from "./urls";
import { type ISiteInit, type ISite, ISiteURLTemplates } from "./types";

const FORM_FIELDS = {
  HOME_PAGE: {
    name: "home_page",
    label: "Home page",
  },
  TITLE: {
    name: "title",
    label: "Title",
  },
  LONG_TITLE: {
    name: "long_title",
    label: "Long title",
  },
  DESCRIPTION: {
    name: "description",
    label: "Description",
  },
  TEMPLATE_PROFILE_LIST: {
    name: "template_profile_list",
    label: "Profile list",
  },
  TEMPLATE_PROFILE_LIST_NOTES: {
    name: "template_profile_list_notes",
    label: "Profile list notes",
  },
  TEMPLATE_PROFILE: {
    name: "template_profile",
    label: "Profile",
  },
  TEMPLATE_PROFILE_NOTES: {
    name: "template_profile_notes",
    label: "Profile notes",
  },
  TEMPLATE_RELEASE_LIST: {
    name: "template_release_list",
    label: "Release list",
  },
  TEMPLATE_RELEASE_LIST_NOTES: {
    name: "template_release_list_notes",
    label: "Release list notes",
  },
  TEMPLATE_RELEASE: {
    name: "template_release",
    label: "Release",
  },
  TEMPLATE_RELEASE_NOTES: {
    name: "template_release_notes",
    label: "Release notes",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

export interface INewSiteFormProps extends IBaseFormProps {}

export const NewSiteForm = blockComponent(undefined, Component);

function Component({ id, ...blockProps }: INewSiteFormProps) {
  const { language } = useLanguage();

  return (
    <Form<IFieldName, ISiteInit, ISite>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      validateInit={validateSiteInit}
      getResult={fetchNewSite}
      resultView={(_, { id, name }) => (
        <p>
          Created new site{" "}
          <LinkInternal href={new SiteURL(language, id)}>
            &quot;{name}&quot; ({id})
          </LinkInternal>
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${formID}-${FORM_FIELDS.HOME_PAGE.name}`}
            form={formID}
            name={FORM_FIELDS.HOME_PAGE.name}
            label={FORM_FIELDS.HOME_PAGE.label}
          >
            <p>The URL of the site&apos;s home page.</p>
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
          >
            <p>The title of the site.</p>
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.LONG_TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.LONG_TITLE.name}
            label={FORM_FIELDS.LONG_TITLE.label}
          >
            <p>The long title of the site.</p>
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.DESCRIPTION.name}`}
            form={formID}
            name={FORM_FIELDS.DESCRIPTION.name}
            label={FORM_FIELDS.DESCRIPTION.label}
          >
            <p>The description of the site.</p>
          </TextSection>

          <Fieldset>
            <Legend>URL Templates</Legend>
            <Details summary="Available Values">
              <p>
                All templates support these values of the homepage{" "}
                <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL">
                  URL
                </Link>
                :
              </p>
              <List>
                <Item>
                  <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/protocol">
                    {toURLTemplateValue("protocol")}
                  </Link>
                </Item>
                <Item>
                  <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/host">
                    {toURLTemplateValue("host")}
                  </Link>
                </Item>
                <Item>
                  <Link href="https://developer.mozilla.org/en-US/docs/Web/API/URL/origin">
                    {toURLTemplateValue("origin")}
                  </Link>
                </Item>
              </List>
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_LIST.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_PROFILE_LIST.name}
              label={FORM_FIELDS.TEMPLATE_PROFILE_LIST.label}
            />
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.label}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_PROFILE.name}
              label={FORM_FIELDS.TEMPLATE_PROFILE.label}
            />
            <Details summary="Extra values">
              <List>
                <Item>{toURLTemplateValue("profile_id")}</Item>
              </List>
            </Details>
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_PROFILE_NOTES.label}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_LIST.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_RELEASE_LIST.name}
              label={FORM_FIELDS.TEMPLATE_RELEASE_LIST.label}
            />
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.label}
              />
            </Details>

            <TextSection
              id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE.name}`}
              form={formID}
              name={FORM_FIELDS.TEMPLATE_RELEASE.name}
              label={FORM_FIELDS.TEMPLATE_RELEASE.label}
            />
            <Details summary="Extra values">
              <List>
                <Item>{toURLTemplateValue("release_id")}</Item>
              </List>
            </Details>
            <Details summary="Notes">
              <TextSection
                id={`${formID}-${FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name}`}
                form={formID}
                name={FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name}
                label={FORM_FIELDS.TEMPLATE_RELEASE_NOTES.label}
              />
            </Details>
          </Fieldset>
        </>
      )}
    </Form>
  );
}

function collectInit(init: ISiteInit, key: IFieldName, value: string) {
  switch (key) {
    case FORM_FIELDS.HOME_PAGE.name:
    case FORM_FIELDS.TITLE.name:
    case FORM_FIELDS.LONG_TITLE.name:
    case FORM_FIELDS.DESCRIPTION.name: {
      init[key] = value;

      break;
    }

    case FORM_FIELDS.TEMPLATE_PROFILE_LIST.name:
    case FORM_FIELDS.TEMPLATE_PROFILE_LIST_NOTES.name:
    case FORM_FIELDS.TEMPLATE_PROFILE.name:
    case FORM_FIELDS.TEMPLATE_PROFILE_NOTES.name:
    case FORM_FIELDS.TEMPLATE_RELEASE_LIST.name:
    case FORM_FIELDS.TEMPLATE_RELEASE_LIST_NOTES.name:
    case FORM_FIELDS.TEMPLATE_RELEASE.name:
    case FORM_FIELDS.TEMPLATE_RELEASE_NOTES.name: {
      if (!init.url_templates) {
        init.url_templates = {};
      }

      const keyName = key.slice("template_".length) as keyof ISiteURLTemplates;

      init.url_templates[keyName] = value;

      break;
    }

    default: {
      throw new ProjectError(`Unknown field "${key satisfies never}".`);
    }
  }
}
