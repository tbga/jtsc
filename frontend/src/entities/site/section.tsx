import { useEffect, useRef, useState, type ReactNode } from "react";
import { fetchSite } from "#api/sites";
import { LoadingBar } from "#components";
import { blockComponent } from "#components/meta";
import { Fieldset, IFieldsetProps, Legend } from "#components/forms";
import { IntegerInput } from "#components/forms/inputs";
import { Button } from "#components/buttons";
import { ListButtons } from "#components/lists";
import { type IAsyncReturnType } from "#types";
import { type ISiteCardProps, SiteCard } from "./card";
import { SearchSitesForm } from "./search";
import type { ISite } from "./types";

import styles from "./section.module.scss";

interface IProps extends IFieldsetProps, Pick<ISiteCardProps, "headingLevel"> {
  id: string;
  form: string;
  name: string;
  label?: ReactNode;
  onSiteChange?: (newSite: ISite) => Promise<void>;
  onSiteClear?: () => Promise<void>;
}

/**
 * @TODO remove internal site state
 */
export const SiteSection = blockComponent(styles.block, Component);

function Component({
  id,
  name,
  form,
  headingLevel,
  label,
  defaultValue,
  onSiteChange,
  onSiteClear,
  children,
  ...blockProps
}: IProps) {
  const [isLoading, switchLoading] = useState(Boolean(defaultValue));
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const inputRef = useRef<HTMLInputElement>(null);
  const siteSearchFormID = `${id}-search-sites`;

  useEffect(() => {
    if (!defaultValue) {
      return;
    }

    const parsedID = (
      Array.isArray(defaultValue)
        ? (defaultValue as string[])[0]
        : String(defaultValue)
    ).trim();

    if (!parsedID) {
      return;
    }

    (async () => {
      try {
        const newSite = await fetchSite(String(parsedID));
        if (inputRef.current) {
          inputRef.current.value = newSite.id;
        }
        changeSite(newSite);
        await onSiteChange?.(newSite);
      } finally {
        switchLoading(false);
      }
    })();
  }, [defaultValue]);

  async function resetSite(isFull: boolean = false) {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      if (isFull || !defaultValue) {
        changeSite(undefined);
        await onSiteClear?.();
        if (inputRef.current) {
          inputRef.current.value = "";
        }

        return;
      }

      const parsedID = (
        Array.isArray(defaultValue)
          ? (defaultValue as string[])[0]
          : String(defaultValue)
      ).trim();

      if (!parsedID) {
        return;
      }

      const newSite = await fetchSite(String(parsedID));
      changeSite(newSite);
      await onSiteChange?.(newSite);
      if (inputRef.current) {
        inputRef.current.value = newSite.id;
      }
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Fieldset {...blockProps}>
      <Legend>{label ?? "Site"}</Legend>

      {isLoading ? (
        <LoadingBar />
      ) : !site ? (
        <SearchSitesForm
          id={siteSearchFormID}
          className={styles.search}
          isAutoSelectEnabled
          onSelect={async ({ id }) => {
            const newSite = await fetchSite(id);
            changeSite(newSite);
            await onSiteChange?.(newSite);
            if (inputRef.current) {
              inputRef.current.value = newSite.id;
            }
          }}
        />
      ) : (
        <SiteCard
          className={styles.card}
          // @ts-expect-error @TODO generic card component
          site={site}
          hiddenFields={["profiles", "releases", "public_id", "published_at"]}
        >
          <ListButtons isSpread>
            <Button onClick={async () => resetSite()}>Reset</Button>
            {defaultValue && (
              <Button onClick={async () => resetSite(true)}>Clear</Button>
            )}
          </ListButtons>
        </SiteCard>
      )}
      <IntegerInput
        ref={inputRef}
        id={id}
        className={styles.id}
        form={form}
        name={name}
        defaultValue={defaultValue}
      />
      {children}
    </Fieldset>
  );
}
