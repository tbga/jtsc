import { useLanguage } from "#hooks";
import { Heading } from "#components";
import { EntityID } from "#components/entities";
import { LinkExternal, LinkInternal } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  type ICardProps,
} from "#components/lists";
import { blockComponent } from "#components/meta";
import { DateTime } from "#components/dates";
import { SiteURL } from "#entities/site";
import { type ISitePreview } from "./types";

import styles from "./card.module.scss";

export interface ISiteCardProps extends Omit<ICardProps, "id"> {
  site: ISitePreview;
  hiddenFields?: Exclude<keyof ISitePreview, "id" | "title" | "name">[];
}

export const SiteCard = blockComponent(styles.block, Component);

function Component({
  site,
  headingLevel,
  hiddenFields,
  children,
  ...blockProps
}: ISiteCardProps) {
  const { language } = useLanguage();
  const { id, title, home_page, profiles, releases, public_id, published_at } =
    site;

  return (
    <Card {...blockProps}>
      <CardHeader>
        <Heading level={headingLevel}>{title}</Heading>
      </CardHeader>

      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          {hiddenFields?.includes("home_page") ? undefined : (
            <DS dKey="Home page" dValue={<LinkExternal href={home_page} />} />
          )}
          {hiddenFields?.includes("profiles") ? undefined : (
            <DS isHorizontal dKey="Profiles" dValue={profiles} />
          )}
          {hiddenFields?.includes("releases") ? undefined : (
            <DS isHorizontal dKey="Releases" dValue={releases} />
          )}
          {hiddenFields?.includes("public_id") ? undefined : (
            <DS dKey="Public ID" dValue={public_id ?? "Not published"} />
          )}
          {hiddenFields?.includes("published_at") ? undefined : (
            <DS
              dKey="Publish Date"
              dValue={
                !published_at ? (
                  "Not published"
                ) : (
                  <DateTime dateTime={published_at} />
                )
              }
            />
          )}
        </DL>
      </CardBody>

      <CardFooter>
        <LinkInternal
          className={styles.details}
          href={new SiteURL(language, id)}
          target="_blank"
        >
          Details
        </LinkInternal>
        {children}
      </CardFooter>
    </Card>
  );
}
