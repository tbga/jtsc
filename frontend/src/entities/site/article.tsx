import { useEffect, useState } from "react";
import { parseTemplate } from "url-template";
import { ProjectURL } from "#lib/urls";
import {
  fetchSiteProfilesPagination,
  fetchSiteReleasesPagination,
} from "#api/sites";
import { useAccount, useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Pre,
  type IArticleProps,
  Details,
} from "#components";
import {
  Link,
  LinkButton,
  LinkExternal,
  LinkInternal,
} from "#components/links";
import { DL, DS, Item, List, ListButtons } from "#components/lists";
import { EntityID } from "#components/entities";
import { DateTime } from "#components/dates";
import { SiteEditURL, SiteReleasesURL } from "#entities/site";
import { BIGINT_ZERO } from "#types";
import { type ISite } from "./types";

import styles from "./article.module.scss";

interface ISitearticleProps extends IArticleProps {
  site: ISite;
}

/**
 * @TODOs
 * - move extra profile/release links to `edit` component
 * - respect linebreaks in descriptions
 */
export const SiteArticle = blockComponent(styles.block, Component);

function Component({
  site,
  headingLevel = 2,
  ...blockProps
}: ISitearticleProps) {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const {
    id,
    title,
    long_title,
    description,
    published_at,
    published_by,
    public_id,
    url_templates,
    home_page,
  } = site;
  const siteURL = new URL(home_page);
  const { origin } = siteURL;
  const profilesURL = !url_templates?.profile_list
    ? undefined
    : decodeURIComponent(
        parseTemplate(url_templates?.profile_list).expand({ origin }),
      );
  const releasesURL = !url_templates?.release_list
    ? undefined
    : decodeURIComponent(
        parseTemplate(url_templates?.release_list).expand({ origin }),
      );

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{title ?? "Untitled"}</Heading>
        <EntityID>{id}</EntityID>
      </ArticleHeader>
      <ArticleBody>
        <Heading
          // @ts-expect-error number literal type
          level={headingLevel + 1}
        >
          Info
        </Heading>
        <DL>
          <DS
            dKey="Long title"
            dValue={<p>{long_title ?? "No long title provided."}</p>}
          />
          <DS
            dKey="Description"
            dValue={
              <p className={styles.description}>
                {description ?? "No descripton provided."}
              </p>
            }
          />
          <DS
            dKey="Home page"
            dValue={<LinkExternal href={site.home_page} />}
          />
          <DS isHorizontal dKey="Profiles" dValue={<Profiles id={id} />} />
          <DS isHorizontal dKey="Releases" dValue={<Releases id={id} />} />
        </DL>

        <Heading
          // @ts-expect-error number literal type
          level={headingLevel + 1}
        >
          Public info
        </Heading>
        {!public_id ? (
          <p>This site is not published.</p>
        ) : (
          <DL>
            <DS dKey="Public ID" dValue={public_id} />
            <DS
              dKey="Publication Date"
              dValue={<DateTime dateTime={published_at} />}
            />

            <DS isHorizontal dKey="Publisher" dValue={published_by} />
          </DL>
        )}

        <Heading
          // @ts-expect-error number literal type
          level={headingLevel + 1}
        >
          URL Templates
        </Heading>
        <URLTemplates url_templates={url_templates} />

        <Heading
          // @ts-expect-error number literal type
          level={headingLevel + 1}
        >
          Links
        </Heading>
        <List>
          <Item>
            <Link href={home_page}>Home Page</Link>
          </Item>

          {profilesURL && (
            <Item>
              <Link href={profilesURL}>Profiles</Link>
            </Item>
          )}

          {releasesURL && (
            <Item>
              <Link href={releasesURL}>Releases</Link>
            </Item>
          )}
        </List>
      </ArticleBody>

      {isRegistered && (
        <ArticleFooter>
          <ListButtons>
            <LinkButton href={new SiteEditURL(language, id)}>Edit</LinkButton>
          </ListButtons>
        </ArticleFooter>
      )}
    </Article>
  );
}

function Profiles({ id }: { id: ISite["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchSiteProfilesPagination>>>();

  useEffect(() => {
    (async () => {
      const artistPagination = await fetchSiteProfilesPagination(id);

      changePagination(artistPagination);
    })();
  }, [id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <span>{pagination.total_count}</span>
  ) : (
    <LinkInternal
      href={new ProjectURL({ language, pathname: `/site/${id}/profiles` })}
    >
      {pagination.total_count}
    </LinkInternal>
  );
}

function Releases({ id }: { id: ISite["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchSiteReleasesPagination>>>();

  useEffect(() => {
    (async () => {
      const artistPagination = await fetchSiteReleasesPagination(id);

      changePagination(artistPagination);
    })();
  }, [id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <span>{pagination.total_count}</span>
  ) : (
    <LinkInternal href={new SiteReleasesURL(language, id)}>
      {pagination.total_count}
    </LinkInternal>
  );
}

interface IURLTemplatesProps
  extends Pick<ISitearticleProps["site"], "url_templates"> {}

function URLTemplates({ url_templates }: IURLTemplatesProps) {
  return (
    <DL>
      <DS
        isHorizontal
        dKey="Profile List"
        dValue={
          <URLTemplate
            template={url_templates?.profile_list}
            notes={url_templates?.profile_list_notes}
          />
        }
      />
      <DS
        isHorizontal
        dKey="Profile"
        dValue={
          <URLTemplate
            template={url_templates?.profile}
            notes={url_templates?.profile_notes}
          />
        }
      />
      <DS
        isHorizontal
        dKey="Release List"
        dValue={
          <URLTemplate
            template={url_templates?.release_list}
            notes={url_templates?.release_list_notes}
          />
        }
      />
      <DS
        isHorizontal
        dKey="Release"
        dValue={
          <URLTemplate
            template={url_templates?.release}
            notes={url_templates?.release_notes}
          />
        }
      />
    </DL>
  );
}

interface IURLTemplateProps {
  template?: string;
  notes?: string;
}

function URLTemplate({ template, notes }: IURLTemplateProps) {
  return !notes ? (
    !template ? (
      "Unknown"
    ) : (
      <Pre>{template}</Pre>
    )
  ) : (
    <Details
      summary={
        !template ? (
          "Unknown"
        ) : (
          <Pre className={styles.template}>{template}</Pre>
        )
      }
    >
      <p>{notes}</p>
    </Details>
  );
}
