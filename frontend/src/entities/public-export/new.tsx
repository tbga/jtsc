import { ProjectError } from "#lib/errors";
import { NEWLINE_SEPARATOR } from "#lib/strings";
import { isEmpty } from "#lib/util";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { fetchPublicExportCreate } from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { Link } from "#components/links";
import { type IProfile } from "#entities/profile";
import { type ISite } from "#entities/site";
import { PublicExportURL } from "#entities/public-export";
import { type IPublicExportInit } from "./types";

const FORM_FIELDS = {
  TITLE: {
    name: "title",
    label: "Title",
  },
  SITE_IDS: {
    name: "site_id",
    label: "Site IDs",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  site_ids?: ISite["id"][];
}

export const NewPublicExportForm = blockComponent(undefined, Component);

function Component({ id, site_ids, ...blockProps }: IProps) {
  const { language } = useLanguage();
  const parsedSiteIDs = !site_ids?.length
    ? undefined
    : site_ids.join(NEWLINE_SEPARATOR);

  return (
    <Form<IFieldName, IPublicExportInit, IEntityItem>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async (init) => fetchPublicExportCreate(init)}
      resultView={(_, { id, name }) => (
        <p>
          Created a new public export{" "}
          <Link href={new PublicExportURL(language, id)} target="_blank">
            &quot;{name}&quot; ({id})
          </Link>
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
          >
            Title of the export.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.SITE_IDS.name}`}
            form={formID}
            name={FORM_FIELDS.SITE_IDS.name}
            label={FORM_FIELDS.SITE_IDS.label}
            defaultValue={parsedSiteIDs}
          >
            Newline-separated list of published site IDs.
          </TextSection>
        </>
      )}
    </Form>
  );
}

function collectInit(
  init: IPublicExportInit,
  fieldName: IFieldName,
  value: string,
) {
  switch (fieldName) {
    case FORM_FIELDS.TITLE.name: {
      init[fieldName] = value;
      break;
    }

    case FORM_FIELDS.SITE_IDS.name: {
      const ids = collectIDs(value);

      if (isEmpty(ids)) {
        break;
      }

      init.sites = ids as [IProfile["id"], ...IProfile["id"][]];

      break;
    }

    default: {
      throw new ProjectError(
        `Unknown field name "${fieldName satisfies never}".`,
      );
    }
  }
}
