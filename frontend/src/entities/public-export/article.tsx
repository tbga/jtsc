import { useEffect, useState } from "react";
import { fetchPublicExportSitesPagination } from "#api/account/administrator/public-exports";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  type IArticleProps,
} from "#components";
import { EntityID } from "#components/entities";
import { DL, DS, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { Link, LinkButton } from "#components/links";
import { DateTime } from "#components/dates";
import {
  PublicExportEditURL,
  PublicExportSitesURL,
} from "#entities/public-export";
import {
  BIGINT_ZERO,
  type IBigIntegerPositive,
  type IBigSerialInteger,
} from "#types";
import { type IPublicExport } from "./types";

interface IProps extends IArticleProps {
  publicExport: IPublicExport;
}

export const PublicExportArticle = blockComponent<IProps>(undefined, Component);

function Component({ publicExport, headingLevel, ...blockProps }: IProps) {
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const { id, title, status, created_at, created_by, updated_at } =
    publicExport;

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{title}</Heading>
        <EntityID>{id}</EntityID>
      </ArticleHeader>
      <ArticleBody>
        <DL>
          <DS isHorizontal dKey="Status" dValue={status} />
          <DS isHorizontal dKey="Creator" dValue={created_by} />
          <DS
            isHorizontal
            dKey="Sites"
            dValue={<Sites public_export_id={publicExport.id} />}
          />
          <DS
            dKey="Creation date"
            dValue={<DateTime dateTime={created_at} />}
          />
          <DS dKey="Last updated" dValue={<DateTime dateTime={updated_at} />} />
        </DL>
      </ArticleBody>
      {isAdmin && publicExport.status !== "finished" && (
        <ArticleFooter>
          <ListButtons>
            <LinkButton href={new PublicExportEditURL(language, id)}>
              Edit
            </LinkButton>
          </ListButtons>
        </ArticleFooter>
      )}
    </Article>
  );
}

function Sites({ public_export_id }: { public_export_id: IBigSerialInteger }) {
  const { language } = useLanguage();
  const [totalCount, changeTotalCount] = useState<IBigIntegerPositive>();

  useEffect(() => {
    (async () => {
      const newPagination =
        await fetchPublicExportSitesPagination(public_export_id);

      changeTotalCount(newPagination.total_count);
    })();
  }, [public_export_id]);

  return !totalCount ? (
    <LoadingBar />
  ) : BigInt(totalCount) === BIGINT_ZERO ? (
    <span>{totalCount}</span>
  ) : (
    <Link href={new PublicExportSitesURL(language, public_export_id)}>
      {totalCount}
    </Link>
  );
}
