import { ProjectError } from "#lib/errors";
import { isEmpty } from "#lib/util";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { fetchPublicExportSitesCreate } from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { SiteURL, type ISite } from "#entities/site";
import { type IPublicExport } from "../types";

const FORM_FIELDS = {
  SITE_IDS: { name: "site_ids", label: "Site IDs" },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  public_export_id: IPublicExport["id"];
}

export const PublicExportSitesAddForm = blockComponent(undefined, Component);

function Component({ public_export_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();

  function collectInit(
    init: { ids: ISite["id"][] },
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.SITE_IDS.name: {
        const ids = collectIDs(value);

        if (isEmpty(ids)) {
          break;
        }

        init.ids = ids;

        break;
      }

      default: {
        throw new ProjectError(
          `Unknown input name "${fieldName satisfies never}".`,
        );
      }
    }
  }

  return (
    <Form<IFieldName, { ids: ISite["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) =>
        fetchPublicExportSitesCreate(public_export_id, ids)
      }
      resultView={(formID, result) => (
        <>
          <p>Added sites to the public export:</p>
          <ListLocal
            items={result.map((site) => (
              <EntityItem
                key={site.id}
                item={site}
                urlBuilder={(id) => new SiteURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.SITE_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.SITE_IDS.name}
          label={FORM_FIELDS.SITE_IDS.label}
        >
          Newline-separated list of site IDs.
        </TextSection>
      )}
    </Form>
  );
}
