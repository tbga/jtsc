import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Heading } from "#components";
import { Link } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  Item,
  List,
  type ICardProps,
} from "#components/lists";
import { EntityID } from "#components/entities";
import { PublicExportURL } from "./urls";
import { type IPublicExportPreview } from "./types";

import styles from "./card.module.scss";

interface IProps extends Omit<ICardProps, "id"> {
  publicExport: IPublicExportPreview;
}

export const PublicExportCard = blockComponent(styles.block, Component);

function Component({
  publicExport,
  headingLevel,
  children,
  ...blockProps
}: IProps) {
  const { language } = useLanguage();
  const { id, title, created_by, status, sites } = publicExport;

  return (
    <Card {...blockProps} id={`public-export-${id}`}>
      <CardHeader>
        <Heading>{title}</Heading>
      </CardHeader>

      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS isHorizontal dKey="Status" dValue={status} />
          <DS isHorizontal dKey="Creator" dValue={created_by} />
          <DS isHorizontal dKey="Sites" dValue={sites} />
        </DL>
      </CardBody>

      <CardFooter>
        <List>
          <Item className={styles.item}>
            <Link
              className={styles.details}
              href={new PublicExportURL(language, id)}
              target="_blank"
            >
              Details
            </Link>
          </Item>
        </List>
        {children}
      </CardFooter>
    </Card>
  );
}
