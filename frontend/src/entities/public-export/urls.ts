import { AdministratorURL, CreatePageURL } from "#lib/urls";
import { type IBigSerialInteger } from "#types";
import { type IPublicExport } from "./types";

export class PublicExportCreateURL extends CreatePageURL {
  constructor(language: string) {
    const pathname = "/public-exports";
    super({ language, pathname });
  }
}

export class PublicExportURL extends AdministratorURL {
  constructor(language: string, id: IPublicExport["id"]) {
    const pathname = `/public-export/${id}`;
    super({ language, pathname });
  }
}

/**
 * @TODO change the underlying page first
 */
export class PublicExportEditURL extends AdministratorURL {
  constructor(language: string, id: IPublicExport["id"]) {
    const pathname = `/public-export/${id}/edit`;
    super({ language, pathname });
  }
}

/**
 * @TODO change the underlying page first
 */
export class PublicExportSitesAddURL extends AdministratorURL {
  constructor(language: string, id: IPublicExport["id"]) {
    const pathname = `/public-export/${id}/edit/sites/add`;
    super({ language, pathname });
  }
}

/**
 * @TODO change the underlying page first
 */
export class PublicExportSitesRemoveURL extends AdministratorURL {
  constructor(language: string, id: IPublicExport["id"]) {
    const pathname = `/public-export/${id}/edit/sites/remove`;
    super({ language, pathname });
  }
}

export class PublicExportSitesURL extends AdministratorURL {
  constructor(
    language: string,
    id: IPublicExport["id"],
    page?: IBigSerialInteger,
  ) {
    const pathname = !page
      ? `/public-export/${id}/sites`
      : `/public-export/${id}/sites/${page}`;
    super({ language, pathname });
  }
}

export class PublicExportsURL extends AdministratorURL {
  constructor(language: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/public-exports" : `/public-exports/${page}`;
    super({ language, pathname });
  }
}
