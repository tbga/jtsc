import { ProjectError } from "#lib/errors";
import { fetchPublicExportUpdate } from "#api/account/administrator/public-exports";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { LinkInternal } from "#components/links";
import { PublicExportURL } from "./urls";
import { type IPublicExport, type IPublicExportUpdate } from "./types";

interface IProps extends IBaseFormProps {
  publicExport: IPublicExport;
  onUpdate?: (updatedPublicExport: IPublicExport) => Promise<void>;
}

const FORM_FIELDS = {
  TITLE: {
    name: "title",
    label: "Title",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

export const PublicExportEditForm = blockComponent(undefined, Component);

function Component({
  publicExport,
  id: formID,
  onUpdate,
  ...blockProps
}: IProps) {
  const { language } = useLanguage();
  const { id, title } = publicExport;

  function collectInit(
    init: IPublicExportUpdate,
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.TITLE.name: {
        if (publicExport[fieldName] !== value) {
          init[fieldName] = value;
        }
        break;
      }

      default: {
        throw new ProjectError(`Illegal key "${fieldName satisfies never}".`);
      }
    }
  }

  return (
    <Form<IFieldName, IPublicExportUpdate, IPublicExport>
      isClient
      id={formID}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async (update) => {
        const updatedPublicExport = await fetchPublicExportUpdate(id, update);

        if (onUpdate) {
          await onUpdate(updatedPublicExport);
        }

        return updatedPublicExport;
      }}
      resultView={(_, { id, title }) => (
        <p>
          Public export{" "}
          <LinkInternal href={new PublicExportURL(language, id)}>
            &quot;{title}&quot; ({id})
          </LinkInternal>{" "}
          was succesfully updated.
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${formID}-${FORM_FIELDS.TITLE.name}`}
            form={formID}
            name={FORM_FIELDS.TITLE.name}
            label={FORM_FIELDS.TITLE.label}
            defaultValue={title}
          >
            Original ID of the profile.
          </TextSection>
        </>
      )}
    </Form>
  );
}
