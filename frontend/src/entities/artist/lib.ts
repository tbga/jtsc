import {
  fetchArtist,
  fetchArtistProfiles,
  fetchArtistProfilesPagination,
  fetchArtists,
  fetchArtistsPagination,
} from "#api/artists";
import { type IBigSerialInteger } from "#types";
import { type IArtist } from "./types";

export {
  artistUpdateSchema,
  validateArtistUpdate,
} from "#codegen/schema/lib/entities/artist/update";

export function artistName(artist: IArtist) {
  return artist.name ?? "Unknown";
}

export async function getArtists(currentPage?: string) {
  if (!currentPage) {
    const pagination = await fetchArtistsPagination();

    currentPage = pagination.current_page;
  }

  return fetchArtists(currentPage);
}

export async function getArtist(artistID: IArtist["id"]) {
  return fetchArtist(artistID);
}

export async function getArtistProfiles(
  artistID: IBigSerialInteger,
  page?: IBigSerialInteger,
) {
  if (!page) {
    const pagination = await fetchArtistProfilesPagination(artistID);

    page = pagination.total_pages;
  }

  return fetchArtistProfiles(artistID, page);
}
