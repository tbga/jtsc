import { useState } from "react";
import { useLanguage } from "#hooks";
import { SearchForm } from "#components/forms";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  fetchArtistSearch,
  fetchArtistSearchPagination,
} from "#api/account/search";
import { ListInternal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { ButtonSwitch } from "#components/buttons";
import { type IAsyncReturnType } from "#types";
import { ArtistURL } from "./urls";

import styles from "./search.module.scss";

interface IProps extends IBlockProps<"div"> {}

/**
 * @TODO actual item search
 */
export const SearchArtistsForm = blockComponent(styles.block, Component);

function Component({ ...blockProps }: IProps) {
  const { language } = useLanguage();
  const [isOpened, switchOpened] = useState(false);
  const [query, changeQuery] = useState<string>();
  const [artistItems, changeArtistItems] =
    useState<IAsyncReturnType<typeof fetchArtistSearch>>();

  async function handleSearch(query: string) {
    const newSiteItems = await fetchArtistSearch(query, "1");
    changeArtistItems(newSiteItems);
    changeQuery(query);
  }

  return (
    <div {...blockProps}>
      <ButtonSwitch
        className={styles.switch}
        onClick={() => {
          switchOpened(!isOpened);
        }}
      >
        {(isOn) => (isOn ? "Close" : "Search Artists")}
      </ButtonSwitch>
      {!isOpened ? undefined : (
        <>
          <SearchForm
            id="search-artists-xnqVOZ2E-LoNSGsc90ExZ"
            paginationFetch={async ({ query }) =>
              fetchArtistSearchPagination(query)
            }
            getResult={async ({ query }) => handleSearch(query)}
          />
          {!artistItems ? undefined : (
            <ListInternal
              pagination={artistItems.pagination}
              onPageChange={async (page) => {
                const newSiteItems = await fetchArtistSearch(query!, page);
                changeArtistItems(newSiteItems);
              }}
            >
              {artistItems.artists.map((artist) => (
                <EntityItem
                  key={artist.id}
                  item={artist}
                  urlBuilder={(id) => new ArtistURL(language, id)}
                />
              ))}
            </ListInternal>
          )}
        </>
      )}
    </div>
  );
}
