import { useLanguage } from "#hooks";
import { Heading } from "#components";
import { EntityID } from "#components/entities";
import { LinkInternal } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  type ICardProps,
} from "#components/lists";
import { blockComponent } from "#components/meta";
import { ArtistURL } from "./urls";
import { type IArtistPreview } from "./types";

export interface IArtistCardProps extends ICardProps {
  artist: IArtistPreview;
}

export const ArtistCard = blockComponent(undefined, Component);

function Component({
  id: elementID,
  headingLevel,
  artist,
  children,
  ...blockProps
}: IArtistCardProps) {
  const { language } = useLanguage();
  const { id, sex, profiles, posts, releases, sites, name } = artist;
  const finalSex = sex === undefined ? "Unknown" : sex ? "Male" : "Female";

  return (
    <Card id={elementID ?? `artist-${id}`} {...blockProps}>
      <>
        <CardHeader>
          <Heading level={headingLevel}>{name ?? "Unknown"}</Heading>
        </CardHeader>
        <CardBody>
          <EntityID>{id}</EntityID>
          <DL>
            <DS isHorizontal dKey="Sex" dValue={finalSex} />
            <DS isHorizontal dKey="Sites" dValue={sites} />
            <DS isHorizontal dKey="Profiles" dValue={profiles} />
            <DS isHorizontal dKey="Posts" dValue={posts} />
            <DS isHorizontal dKey="Releases" dValue={releases} />
          </DL>
        </CardBody>
        <CardFooter>
          <LinkInternal href={new ArtistURL(language, id)} target="_blank">
            Details
          </LinkInternal>
          {children}
        </CardFooter>
      </>
    </Card>
  );
}
