import { ProjectError } from "#lib/errors";
import { useLanguage } from "#hooks";
import { fetchArtistEdit } from "#api/account/administrator/artists";
import { blockComponent } from "#components/meta";
import { Form, IBaseFormProps } from "#components/forms";
import { type IRadioGroupProps, RadioGroup } from "#components/forms/sections";
import { LinkInternal } from "#components/links";
import { ArtistURL } from "#entities/artist";
import { artistName, validateArtistUpdate } from "./lib";
import { type IArtist, type IArtistUpdate } from "./types";

const FIELD_NAMES = ["sex"] as const;
type IInputName = (typeof FIELD_NAMES)[number];

export interface IArtistEditFormProps extends Omit<IBaseFormProps, "id"> {
  artist: IArtist;
  id?: string;
}

export const ArtistEditForm = blockComponent(undefined, Component);

function Component({
  artist,
  id: formID,
  ...blockProps
}: IArtistEditFormProps) {
  const { language } = useLanguage();
  const { id, sex } = artist;
  const finalID = formID ?? `edit-artist-${artist.id}`;
  const sexOptions: (formID: string) => IRadioGroupProps["options"] = (
    formID: string,
  ) => [
    {
      id: `${formID}-sex-null`,
      title: "Unknown",
      defaultValue: "",
      defaultChecked: sex === undefined,
    },
    {
      id: `${formID}-sex-male`,
      title: "Male",
      defaultValue: "true",
      defaultChecked: sex === true,
    },
    {
      id: `${formID}-sex-female`,
      title: "Female",
      defaultValue: "false",
      defaultChecked: sex === false,
    },
  ];

  return (
    <Form<IInputName, IArtistUpdate, IArtist>
      isClient
      id={finalID}
      fieldNames={FIELD_NAMES}
      collectInit={(edit, fieldName, value) => {
        switch (fieldName) {
          case "sex":
            // @ts-expect-error validator will cast it.
            edit.sex = value;
            break;

          default: {
            throw new ProjectError(`Unknown field "${fieldName}".`);
          }
        }
      }}
      validateInit={validateArtistUpdate}
      getResult={async (edit) => {
        return fetchArtistEdit(artist.id, edit);
      }}
      resultView={(formID, editedArtist) => (
        <p>
          The artist{" "}
          <LinkInternal href={new ArtistURL(language, id)}>
            {artistName(artist)}
          </LinkInternal>{" "}
          was successfully updated.
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <RadioGroup
          form={formID}
          name="sex"
          legend={"Sex"}
          options={sexOptions(formID)}
        >
          Sex of the artist.
        </RadioGroup>
      )}
    </Form>
  );
}
