import { ProjectURL } from "#lib/urls";
import { ProfileCreateURL } from "#entities/profile";
import { type IBigIntegerPositive } from "#types";
import { type IArtist } from "./types";

export class ArtistURL extends ProjectURL {
  constructor(language: string, artistID: IArtist["id"]) {
    super({ language, pathname: `/artist/${artistID}` });
  }
}

export class NewArtistProfileURL extends ProfileCreateURL {
  constructor(language: string, artistID: IArtist["id"]) {
    const searchParams = new URLSearchParams([["artist_id", artistID]]);
    super(language, searchParams);
  }
}

export class ArtistProfilesURL extends ProjectURL {
  constructor(
    language: string,
    artistID: IArtist["id"],
    page?: IBigIntegerPositive,
  ) {
    const pathname = !page
      ? `/artist/${artistID}/profiles`
      : `/artist/${artistID}/profiles/${page}`;
    super({ language, pathname });
  }
}
