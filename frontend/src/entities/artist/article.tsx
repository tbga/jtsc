import { useEffect, useState } from "react";
import { EditURL } from "#lib/urls";
import { fetchArtistProfilesPagination } from "#api/artists";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  type IArticleProps,
} from "#components";
import { LinkButton, LinkInternal } from "#components/links";
import { DL, DS, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { DateTime } from "#components/dates";
import { EntityID } from "#components/entities";
import { BIGINT_ZERO } from "#types";
import { ArtistProfilesURL } from "./urls";
import { type IArtist } from "./types";

export interface IArtistArticleProps extends IArticleProps {
  artist: IArtist;
}

/**
 * Move manipulations to a separate edit page.
 */
export const ArtistArticle = blockComponent(undefined, Component);

function Component({
  artist,
  headingLevel,
  ...blockProps
}: IArtistArticleProps) {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const { id, sex, created_at, name } = artist;
  const resolvedSex =
    typeof sex === "boolean" ? (sex ? "Male" : "Female") : sex;

  return (
    <Article {...blockProps}>
      <>
        <ArticleHeader>
          <Heading level={headingLevel}>{name ?? "Unknown"}</Heading>
          <EntityID>{id}</EntityID>
        </ArticleHeader>

        <ArticleBody>
          <DL>
            <DS isHorizontal dKey="Sex" dValue={resolvedSex} />
            <DS isHorizontal dKey="Profiles" dValue={<Profiles id={id} />} />
            <DS dKey="Date added" dValue={<DateTime dateTime={created_at} />} />
          </DL>
        </ArticleBody>

        {isRegistered && (
          <ArticleFooter>
            <ListButtons>
              <LinkButton
                href={new EditURL({ language, pathname: `/artist/${id}` })}
              >
                Edit
              </LinkButton>
            </ListButtons>
          </ArticleFooter>
        )}
      </>
    </Article>
  );
}

function Profiles({ id }: { id: IArtist["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchArtistProfilesPagination>>>();

  useEffect(() => {
    (async () => {
      const newPagination = await fetchArtistProfilesPagination(id);

      changePagination(newPagination);
    })();
  }, [id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <>0</>
  ) : (
    <LinkInternal
      href={new ArtistProfilesURL(language, id, pagination.total_pages)}
    >
      {pagination.total_count}
    </LinkInternal>
  );
}
