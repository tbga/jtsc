export type { IArtist } from "#codegen/schema/lib/entities/artist/entity";
export type { IArtistInit } from "#codegen/schema/lib/entities/artist/init";
export type { IArtistPreview } from "#codegen/schema/lib/entities/artist/preview";
export type { IArtistUpdate } from "#codegen/schema/lib/entities/artist/update";
