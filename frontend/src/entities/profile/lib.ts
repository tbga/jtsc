import {
  fetchProfileCreate,
  fetchUpdateProfile,
} from "#api/account/administrator/profile";
import {
  fetchProfile,
  fetchProfiles,
  fetchProfileNames,
  fetchProfileNamesPagination,
} from "#api/profiles";
import {
  type IBigSerialInteger,
  validateBigSerialInteger,
  BIGINT_ZERO,
} from "#types";
import { validateProfileInit, validateProfileUpdate } from "./lib";
import { type IProfileInit, type IProfileUpdate, type IProfile } from "./types";

export {
  profileInitSchema,
  validateProfileInit,
} from "#codegen/schema/lib/entities/profile/init";
export {
  profileUpdateSchema,
  validateProfileUpdate,
} from "#codegen/schema/lib/entities/profile/update";

export async function getProfiles(...args: Parameters<typeof fetchProfiles>) {
  return fetchProfiles(...args);
}

export async function getProfile(profileID: IBigSerialInteger) {
  validateBigSerialInteger(profileID);
  const profile = await fetchProfile(profileID);
  const namesData = await getProfileNames(profileID);
  const name = namesData?.names[0].name;

  return {
    profile,
    namesData,
    name,
  };
}

export async function getProfileNames(
  profileID: IBigSerialInteger,
  page?: IBigSerialInteger,
) {
  validateBigSerialInteger(profileID);

  if (!page) {
    const pagination = await fetchProfileNamesPagination(profileID);

    if (BigInt(pagination.total_count) === BIGINT_ZERO) {
      return;
    }

    page = pagination.current_page;
  }

  validateBigSerialInteger(page);

  const namesData = await fetchProfileNames(profileID, page);

  return namesData;
}

export async function createNewProfile(profileInit: IProfileInit) {
  validateProfileInit(profileInit);

  return fetchProfileCreate(profileInit);
}

export async function updateProfile(
  profile_id: IProfile["id"],
  profileUpdate: IProfileUpdate,
) {
  validateProfileUpdate(profileUpdate);

  return fetchUpdateProfile(profile_id, profileUpdate);
}
