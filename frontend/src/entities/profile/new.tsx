import { useEffect, useState } from "react";
import { collectStrings, NEWLINE_SEPARATOR } from "#lib/strings";
import { collectIDs } from "#lib/entities";
import { ProjectError } from "#lib/errors";
import { isEmpty } from "#lib/util";
import { fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import { Details, Pre } from "#components";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { Link } from "#components/links";
import { IntegerSection, TextSection } from "#components/forms/sections";
import { DL, DS } from "#components/lists";
import { type IArtistInit, type IArtist } from "#entities/artist";
import { SiteSection, type ISite } from "#entities/site";
import { type INameInit } from "#entities/name";
import { OperationURL } from "#entities/operation";
import { type IRelease } from "#entities/release";
import { EMPTY_OBJECT, IAsyncReturnType } from "#types";
import { createNewProfile } from "./lib";
import { type IProfileInit, type IProfile } from "./types";

export interface INewProfileFormProps extends IBaseFormProps {
  artist_ids?: IArtist["id"][];
  /**
   * @TODO replace with site object
   * as it currently refetches twice on different levels
   */
  site_id?: ISite["id"];
  release_ids?: IRelease["id"][];
}

export const NewProfileForm = blockComponent(undefined, Component);

const FORM_FIELDS = {
  ORIGINAL_ID: {
    name: "original_id",
    label: "Original ID",
  },
  NAMES: {
    name: "names",
    label: "Profile names",
  },
  ORIGINAL_BIO: {
    name: "original_bio",
    label: "Original bio",
  },
  SITE_ID: {
    name: "site_id",
    label: "Site",
  },
  RELEASE_IDS: {
    name: "release_ids",
    label: "Release IDs",
  },
  ARTIST_IDS: {
    name: "artist_ids",
    label: "Artist IDs",
  },
  ARTIST_INITS: {
    name: "artist_inits",
    label: "New artists",
  },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

function Component({
  artist_ids,
  release_ids,
  site_id,
  id,
  ...blockProps
}: INewProfileFormProps) {
  const { language } = useLanguage();
  const [site, changeSite] = useState<IAsyncReturnType<typeof fetchSite>>();
  const parsedArtistIDs = artist_ids?.join(NEWLINE_SEPARATOR);
  const parsedReleaseIDs = release_ids?.join(NEWLINE_SEPARATOR);

  useEffect(() => {
    if (!site_id) {
      return;
    }

    (async () => {
      const newSite = await fetchSite(site_id);
      changeSite(newSite);
    })();
  }, [site_id]);

  return (
    <Form<IFieldName, IProfileInit, IProfile>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async (init) => createNewProfile(init)}
      resultView={(_, { id, name }) => (
        <p>
          New operation{" "}
          <Link href={new OperationURL(language, id)}>
            &quot;{name ?? "Unknown"}&quot; ({id})
          </Link>{" "}
          was successfully created.
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <>
          <SiteSection
            id={`${formID}-${FORM_FIELDS.SITE_ID.name}`}
            form={formID}
            name={FORM_FIELDS.SITE_ID.name}
            label={FORM_FIELDS.SITE_ID.label}
            defaultValue={site_id}
            onSiteChange={async (newSite) => changeSite(newSite)}
            onSiteClear={async () => changeSite(undefined)}
          >
            Site of the profile
          </SiteSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_ID.name}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_ID.name}
            label={FORM_FIELDS.ORIGINAL_ID.label}
          >
            {/* @TODO some loading state */}
            {site?.url_templates?.profile && (
              <Pre>{site.url_templates.profile}</Pre>
            )}
            <p>Original ID of the profile.</p>
            {site?.url_templates?.profile_notes && (
              <Details summary="Notes">
                {site.url_templates.profile_notes}
              </Details>
            )}
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.NAMES.name}`}
            form={formID}
            name={FORM_FIELDS.NAMES.name}
            label={FORM_FIELDS.NAMES.label}
          >
            Newline-separated list of profile names.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_BIO.name}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_BIO.name}
            label={FORM_FIELDS.ORIGINAL_BIO.label}
          >
            Original bio of the profile.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.RELEASE_IDS.name}`}
            form={formID}
            name={FORM_FIELDS.RELEASE_IDS.name}
            label={FORM_FIELDS.RELEASE_IDS.label}
            defaultValue={parsedReleaseIDs}
          >
            Newline-separated list of release IDs.
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ARTIST_IDS.name}`}
            form={formID}
            name={FORM_FIELDS.ARTIST_IDS.name}
            label={FORM_FIELDS.ARTIST_IDS.label}
            defaultValue={parsedArtistIDs}
          >
            Newline-separated list of artist IDs.
          </TextSection>

          <IntegerSection
            id={`${formID}-${FORM_FIELDS.ARTIST_INITS.name}`}
            form={formID}
            name={FORM_FIELDS.ARTIST_INITS.name}
            label={FORM_FIELDS.ARTIST_INITS.label}
            defaultValue={parsedArtistIDs ? undefined : 1}
            max={25}
          >
            Amount of new artists to create.
          </IntegerSection>
        </>
      )}
    </Form>
  );
}

function collectInit(init: IProfileInit, key: IFieldName, value: string) {
  switch (key) {
    case FORM_FIELDS.ORIGINAL_ID.name:
    case FORM_FIELDS.ORIGINAL_BIO.name:
    case FORM_FIELDS.SITE_ID.name: {
      init[key] = value;
      break;
    }

    case FORM_FIELDS.NAMES.name: {
      const fullNames = collectStrings(value);

      if (isEmpty(fullNames)) {
        break;
      }
      const names = fullNames.map<INameInit>((full_name) => {
        return {
          full_name,
        };
      });

      init[key] = names as [INameInit, ...INameInit[]];

      break;
    }

    case FORM_FIELDS.RELEASE_IDS.name: {
      const release_ids = collectIDs(value);

      if (isEmpty(release_ids)) {
        break;
      }

      if (!init.releases) {
        init.releases = {};
      }

      init.releases.ids = release_ids as [IRelease["id"], ...IRelease["id"][]];

      break;
    }

    case FORM_FIELDS.ARTIST_IDS.name: {
      const artist_ids = collectIDs(value);

      if (!artist_ids.length) {
        break;
      }

      if (!init.artists) {
        init.artists = {};
      }

      init.artists.ids = artist_ids as [IArtist["id"], ...IArtist["id"][]];

      break;
    }

    case FORM_FIELDS.ARTIST_INITS.name: {
      // for now only creating dummy artists
      const artist_inits = new Array<IArtistInit>(Number(value)).fill(
        EMPTY_OBJECT,
      );

      if (isEmpty(artist_inits)) {
        break;
      }

      if (!init.artists) {
        init.artists = {};
      }

      init.artists.inits = artist_inits as [IArtistInit, ...IArtistInit[]];

      break;
    }

    default: {
      throw new ProjectError(`Unknown field "${key satisfies never}".`);
    }
  }
}
