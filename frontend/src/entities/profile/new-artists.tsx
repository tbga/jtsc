import { ProjectError } from "#lib/errors";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { fetchCreateProfileArtists } from "#api/account/administrator/profile";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { ArtistURL, type IArtist } from "#entities/artist";
import { type IProfile } from "./types";

const FORM_FIELDS = {
  ARTIST_IDS: { name: "artist_ids", label: "Artist IDs" },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  profile_id: IProfile["id"];
}

export const NewProfileArtistsForm = blockComponent(undefined, Component);

function Component({ profile_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();
  function collectInit(
    init: { ids: IArtist["id"][] },
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.ARTIST_IDS.name: {
        const ids = collectIDs(value);

        if (!ids.length) {
          break;
        }

        init.ids = ids;

        break;
      }

      default: {
        throw new ProjectError(`Unknown input key "${fieldName}".`);
      }
    }
  }

  return (
    <Form<IFieldName, { ids: IArtist["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) => fetchCreateProfileArtists(profile_id, ids)}
      resultView={(formID, result) => (
        <>
          <p>Added artists to the profile:</p>
          <ListLocal
            items={result.map((artist) => (
              <EntityItem
                key={artist.id}
                item={artist}
                urlBuilder={(id) => new ArtistURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.ARTIST_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.ARTIST_IDS.name}
          label={FORM_FIELDS.ARTIST_IDS.label}
        >
          Newline-separated list of artist IDs.
        </TextSection>
      )}
    </Form>
  );
}
