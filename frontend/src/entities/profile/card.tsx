import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Heading } from "#components";
import { Link } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  Item,
  List,
  type ICardProps,
} from "#components/lists";
import { EntityID } from "#components/entities";
import { SiteURL } from "#entities/site";
import { ProfileURL } from "./urls";
import { type IProfilePreview } from "./types";

import styles from "./card.module.scss";

export interface IProfileCardProps extends Omit<ICardProps, "id"> {
  profile: IProfilePreview;
}

export const ProfileCard = blockComponent(styles.block, Component);

function Component({
  profile,
  headingLevel,
  children,
  ...blockProps
}: IProfileCardProps) {
  const { language } = useLanguage();
  const { id, name, site, artists, releases, original_id } = profile;

  return (
    <Card {...blockProps} id={`profile-${id}`}>
      <CardHeader>
        <Heading>{name ?? "Unknown"}</Heading>
      </CardHeader>
      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS
            dKey="Site"
            dValue={
              site ? (
                <Link href={new SiteURL(language, site.id)} target="_blank">
                  {site.name ?? "Unknown"} ({site.id})
                </Link>
              ) : (
                "Unknown"
              )
            }
          />
          <DS dKey="Original ID" dValue={original_id} />
          <DS isHorizontal dKey="Artists" dValue={artists} />
          <DS isHorizontal dKey="Releases" dValue={releases} />
        </DL>
      </CardBody>
      <CardFooter>
        <List>
          <Item className={styles.item}>
            <Link
              className={styles.details}
              href={new ProfileURL(language, id)}
              target="_blank"
            >
              Details
            </Link>
          </Item>
        </List>
        {children}
      </CardFooter>
    </Card>
  );
}
