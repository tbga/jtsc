import { useEffect, useState } from "react";
import { ProjectError } from "#lib/errors";
import { fetchSite } from "#api/sites";
import { useLanguage } from "#hooks";
import { Details, Pre } from "#components";
import { blockComponent } from "#components/meta";
import { Form, type IBaseFormProps } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { LinkInternal } from "#components/links";
import { SiteSection } from "#entities/site";
import { updateProfile } from "./lib";
import { ProfileURL } from "./urls";
import { type IProfile, type IProfileUpdate } from "./types";
import { type IAsyncReturnType } from "#types";

export interface IProfileUpdateFormProps extends IBaseFormProps {
  profile: IProfile;
}

const FORM_FIELDS = {
  SITE_ID: "site_id",
  ORIGINAL_ID: "original_id",
  ORIGINAL_BIO: "original_bio",
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS);
type IFieldName = (typeof FIELD_NAMES)[number];

/**
 * @TODO: artist update
 */
export const ProfileUpdateForm = blockComponent(undefined, Component);

function Component({
  profile: currentProfile,
  id: formID,
  ...blockProps
}: IProfileUpdateFormProps) {
  const { language } = useLanguage();
  const [siteEntity, changeSiteEntity] =
    useState<IAsyncReturnType<typeof fetchSite>>();
  const { id, site, original_bio, original_id, name } = currentProfile;
  const siteID = site?.id;

  useEffect(() => {
    if (siteID === siteEntity?.id || !siteID) {
      return;
    }

    (async () => {
      const newEntity = await fetchSite(siteID);
      changeSiteEntity(newEntity);
    })();
  }, [siteID]);

  function collectInit(
    init: IProfileUpdate,
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.ORIGINAL_BIO:
      case FORM_FIELDS.ORIGINAL_ID: {
        if (currentProfile[fieldName] !== value) {
          init[fieldName] = value;
        }
        break;
      }

      // special check for `site_id`
      case FORM_FIELDS.SITE_ID: {
        if (currentProfile.site?.id !== value) {
          init[fieldName] = value;
        }
        break;
      }

      default: {
        throw new ProjectError(`Illegal key "${fieldName satisfies never}".`);
      }
    }
  }

  return (
    <Form<IFieldName, IProfileUpdate, IProfile>
      isClient
      id={formID}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async (update) => updateProfile(id, update)}
      resultView={(_, { id, name }) => (
        <p>
          Profile{" "}
          <LinkInternal href={new ProfileURL(language, id)}>
            {`${name ?? "Unknown"} (${id})`}
          </LinkInternal>{" "}
          was succesfully updated.
        </p>
      )}
      {...blockProps}
    >
      {(formID) => (
        <>
          <SiteSection
            id={`${formID}-${FORM_FIELDS.SITE_ID}`}
            form={formID}
            name={FORM_FIELDS.SITE_ID}
            defaultValue={site?.id}
            onSiteChange={async (newSite) => changeSiteEntity(newSite)}
            onSiteClear={async () => changeSiteEntity(undefined)}
          >
            Site of the profile.
          </SiteSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_ID}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_ID}
            label="Original ID"
            defaultValue={original_id}
          >
            {siteEntity?.url_templates?.profile && (
              <Pre>{siteEntity.url_templates.profile}</Pre>
            )}
            Original ID of the profile.
            {siteEntity?.url_templates?.profile_notes && (
              <Details summary="Notes">
                <p>{siteEntity.url_templates.profile_notes}</p>
              </Details>
            )}
          </TextSection>

          <TextSection
            id={`${formID}-${FORM_FIELDS.ORIGINAL_BIO}`}
            form={formID}
            name={FORM_FIELDS.ORIGINAL_BIO}
            label="Original Bio"
            defaultValue={original_bio}
          >
            Original bio of the profile.
          </TextSection>
        </>
      )}
    </Form>
  );
}
