import { CreatePageURL, EditURL, ProjectURL, RemovePageURL } from "#lib/urls";
import { type IBigIntegerPositive } from "#types";
import { type IProfile } from "./types";

export class ProfileURL extends ProjectURL {
  constructor(
    language: string,
    profileID: IProfile["id"],
    pathName: string = "",
  ) {
    super({ language, pathname: `/profile/${profileID}${pathName}` });
  }
}

export class ProfileCreateURL extends CreatePageURL {
  constructor(language: string, searchParams?: URLSearchParams) {
    super({ language, pathname: "/profile", searchParams });
  }
}

export class ProfileEditURL extends EditURL {
  constructor(language: string, profileID: IProfile["id"], hash?: string) {
    super({ language, pathname: `/profile/${profileID}`, hash });
  }
}

export class NewProfileNamesURL extends CreatePageURL {
  constructor(language: string, profileID: IProfile["id"]) {
    super({ language, pathname: `/profile/${profileID}/names` });
  }
}

export class RemoveProfileNamesURL extends RemovePageURL {
  constructor(language: string, profileID: IProfile["id"]) {
    super({ language, pathname: `/profile/${profileID}/names` });
  }
}

export class NewProfileReleasesURL extends CreatePageURL {
  constructor(language: string, profileID: IProfile["id"]) {
    super({ language, pathname: `/profile/${profileID}/releases` });
  }
}

export class ProfileReleasesURL extends ProjectURL {
  constructor(
    language: string,
    profileID: IProfile["id"],
    page?: IBigIntegerPositive,
  ) {
    const pathname = !page
      ? `/profile/${profileID}/releases`
      : `/profile/${profileID}/releases/${page}`;
    super({ language, pathname });
  }
}

export class NewProfileArtistsURL extends CreatePageURL {
  constructor(language: string, profileID: IProfile["id"]) {
    super({ language, pathname: `/profile/${profileID}/artists` });
  }
}

export class RemoveProfileArtistsURL extends RemovePageURL {
  constructor(language: string, profileID: IProfile["id"]) {
    super({ language, pathname: `/profile/${profileID}/artists` });
  }
}

export class ProfileArtistsURL extends ProjectURL {
  constructor(
    language: string,
    profileID: IProfile["id"],
    page?: IBigIntegerPositive,
  ) {
    const pathname = !page
      ? `/profile/${profileID}/artists`
      : `/profile/${profileID}/artists/${page}`;
    super({ language, pathname });
  }
}
