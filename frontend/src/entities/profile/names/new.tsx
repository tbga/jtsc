import { NEWLINE_SEPARATOR } from "#lib/strings";
import { type IEntityItem } from "#lib/entities";
import { fetchProfileNamesCreate } from "#api/account/administrator/profile";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { type INameInit } from "#entities/name";
import { type IProfile } from "#entities/profile";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";

export interface INewProfileNameFormProps extends Omit<IBaseFormProps, "id"> {
  profile_id: IProfile["id"];
}

const FIELD_NAMES = ["full_names"] as const;
type IFieldName = (typeof FIELD_NAMES)[number];

export const NewProfileNamesForm = blockComponent(undefined, Component);

function Component({ profile_id, ...blockProps }: INewProfileNameFormProps) {
  const formID = `create-profile-${profile_id}-names`;

  function collectInit(
    init: { names: INameInit[] },
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case "full_names": {
        const inits = value
          .split(NEWLINE_SEPARATOR)
          .reduce<INameInit[]>((inits, name) => {
            const full_name = name.trim();

            if (!full_name) {
              return inits;
            }

            const init: INameInit = {
              full_name,
            };

            inits.push(init);

            return inits;
          }, []);

        init.names = inits;

        break;
      }

      default: {
        break;
      }
    }
  }

  return (
    <Form<IFieldName, { names: INameInit[] }, IEntityItem[]>
      isClient
      id={formID}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ names }) =>
        fetchProfileNamesCreate(profile_id, names)
      }
      resultView={(formID, names) => (
        <>
          <p>These names were successfully added to the profile:</p>
          <ListLocal
            items={names.map((name) => (
              <EntityItem key={name.id} item={name} />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${formID}-full_names`}
          form={formID}
          name="full_names"
          label="Names"
        >
          Newline-separated list of names.
        </TextSection>
      )}
    </Form>
  );
}
