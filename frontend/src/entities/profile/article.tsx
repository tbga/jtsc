import { useEffect, useState } from "react";
import { parseTemplate } from "url-template";
import {
  fetchProfileArtistItems,
  fetchProfileArtistPagination,
  fetchProfileNames,
  fetchProfileNamesPagination,
  fetchProfileReleasesPagination,
} from "#api/profiles";
import { fetchSite } from "#api/sites";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  Pre,
  type IArticleProps,
} from "#components";
import { blockComponent } from "#components/meta";
import { DL, DS, Item, ListButtons } from "#components/lists";
import { Link, LinkButton, LinkInternal } from "#components/links";
import { DateTime } from "#components/dates";
import { ListInternal } from "#components/lists/generic";
import { EntityID, EntityItem } from "#components/entities";
import { SiteURL } from "#entities/site";
import { ArtistURL } from "#entities/artist";
import { BIGINT_ONE, BIGINT_ZERO, IAsyncReturnType } from "#types";
import {
  NewProfileNamesURL,
  ProfileArtistsURL,
  ProfileEditURL,
  ProfileReleasesURL,
} from "./urls";
import { type IProfile } from "./types";

import styles from "./article.module.scss";

export interface IProfileArticleProps extends IArticleProps {
  profile: IProfile;
  namesData?: IAsyncReturnType<typeof fetchProfileNames>;
}

/**
 * @TODO move edit-specific links out
 */
export const ProfileArticle = blockComponent(styles.block, Component);

function Component({
  profile,
  namesData,
  headingLevel,
  ...blockProps
}: IProfileArticleProps) {
  const { language } = useLanguage();
  const { isRegistered } = useAccount();
  const [fullSite, changeFullSite] =
    useState<IAsyncReturnType<typeof fetchSite>>();
  const { id, site, original_bio, original_id, created_at } = profile;
  const siteID = site?.id;
  const siteURL = fullSite && new URL(fullSite.home_page);
  const originalProfileURL =
    !original_id || !siteURL || !fullSite?.url_templates?.profile
      ? undefined
      : decodeURIComponent(
          parseTemplate(fullSite.url_templates?.profile).expand({
            protocol: siteURL.protocol,
            host: siteURL.host,
            origin: siteURL.origin,
            profile_id: original_id,
          }),
        );
  const profileName = namesData?.names[0]?.name ?? "Unknown";

  useEffect(() => {
    if (!siteID) {
      return;
    }
    (async () => {
      const newFullSite = await fetchSite(siteID);
      changeFullSite(newFullSite);
    })();
  }, [siteID]);

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{profileName}</Heading>
        <EntityID>{id}</EntityID>
        <Names id={id} />
      </ArticleHeader>
      <ArticleBody>
        <DL>
          <DS
            isHorizontal
            dKey="Site"
            dValue={
              site && (
                <LinkInternal
                  href={new SiteURL(language, site.id)}
                  target="_blank"
                >
                  {site.name ? `"${site.name}" (${site.id})` : "Unknown"}
                </LinkInternal>
              )
            }
          />
          <DS dKey={"Original ID"} dValue={original_id} />
          <DS
            dKey={"Original URL"}
            dValue={originalProfileURL && <Link href={originalProfileURL} />}
          />
          <DS dKey="Bio" dValue={<Pre>{original_bio}</Pre>} />
          <DS
            dKey={
              <Link href={new ProfileArtistsURL(language, id)}>Artists</Link>
            }
            dValue={<Artists id={id} />}
          />
          <DS isHorizontal dKey="Releases" dValue={<Releases id={id} />} />
        </DL>
      </ArticleBody>
      <ArticleFooter>
        <DL>
          <DS dKey="Date added" dValue={<DateTime dateTime={created_at} />} />
        </DL>
        {isRegistered && (
          <ListButtons>
            <LinkButton href={new ProfileEditURL(language, id)}>
              Edit
            </LinkButton>
          </ListButtons>
        )}
      </ArticleFooter>
    </Article>
  );
}

function Names({ id }: { id: IProfile["id"] }) {
  const { language } = useLanguage();
  const [namesData, changeNamesData] =
    useState<IAsyncReturnType<typeof fetchProfileNames>>();

  useEffect(() => {
    (async () => {
      const pagination = await fetchProfileNamesPagination(id);

      if (BigInt(pagination.total_count) === BIGINT_ZERO) {
        return;
      }

      const newNames = await fetchProfileNames(id, String(BIGINT_ONE));
      changeNamesData(newNames);
    })();
  }, [id]);

  return !namesData ? (
    <p>
      No names available.{" "}
      <Link href={new NewProfileNamesURL(language, id)}>Add one</Link>
    </p>
  ) : (
    <ListInternal
      isHorizontal
      className={styles.nameList}
      pagination={namesData.pagination}
      onPageChange={async (page) => {
        const newNames = await fetchProfileNames(id, page);
        changeNamesData(newNames);
      }}
    >
      {namesData.names.map(({ id, name }) => (
        <Item key={id} className={styles.item}>
          {name}
        </Item>
      ))}
    </ListInternal>
  );
}

function Releases({ id }: { id: IProfile["id"] }) {
  const { language } = useLanguage();
  const [pagination, changePagination] =
    useState<Awaited<ReturnType<typeof fetchProfileReleasesPagination>>>();

  useEffect(() => {
    (async () => {
      const newPagination = await fetchProfileReleasesPagination(id);
      changePagination(newPagination);
    })();
  }, [id]);

  return !pagination ? (
    <LoadingBar />
  ) : BigInt(pagination.total_count) === BIGINT_ZERO ? (
    <span>{pagination.total_count}</span>
  ) : (
    <Link href={new ProfileReleasesURL(language, id)}>
      {pagination.total_count}
    </Link>
  );
}

function Artists({ id }: { id: IProfile["id"] }) {
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [artistsData, changeArtistsData] =
    useState<IAsyncReturnType<typeof fetchProfileArtistItems>>();

  useEffect(() => {
    (async () => {
      try {
        switchLoading(true);
        const pagination = await fetchProfileArtistPagination(id);

        if (BigInt(pagination.total_count) === BIGINT_ZERO) {
          return;
        }

        const newArtistsData = await fetchProfileArtistItems(id, "1");

        changeArtistsData(newArtistsData);
      } finally {
        switchLoading(false);
      }
    })();
  }, [id]);

  return (
    <div>
      {isLoading ? (
        <LoadingBar />
      ) : !artistsData ? (
        <p>No artists available.</p>
      ) : (
        <ListInternal
          isHorizontal
          pagination={artistsData.pagination}
          onPageChange={async (page) => {
            const newArtistsData = await fetchProfileArtistItems(id, page);
            changeArtistsData(newArtistsData);
          }}
        >
          {artistsData.artists.map((item) => (
            <EntityItem
              key={item.id}
              item={item}
              urlBuilder={(id) => new ArtistURL(language, id)}
            />
          ))}
        </ListInternal>
      )}
    </div>
  );
}
