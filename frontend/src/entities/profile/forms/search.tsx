import { fetchSearchProfilesPagination } from "#api/account/search";
import { useLanguage } from "#hooks";
import { FormSection, ISearchFormProps, SearchForm } from "#components/forms";
import { HiddenInput } from "#components/forms/inputs";
import { Link } from "#components/links";
import { DL, DS } from "#components/lists";
import { ISite, SiteSection, SiteURL } from "#entities/site";
import { toJSON } from "#lib/json";

const FIELD_NAMES = ["site_id"] as const;
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends Pick<ISearchFormProps, "id"> {
  searchQuery?: string;
  site?: ISite;
  onSearch: (params: ISearchParams) => Promise<void>;
}

interface ISearchParams {
  query: string;
  site_id?: string;
}

export function SearchProfilesForm({
  id,
  searchQuery,
  site,
  onSearch,
}: IProps) {
  const { language } = useLanguage();
  return (
    <SearchForm<IFieldName, ISearchParams>
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={(init, fieldName, value) => {
        console.log(toJSON([init, fieldName, value]));
        if (fieldName === "site_id") {
          init[fieldName] = value;
        }

        console.log(toJSON([init, fieldName, value]));
      }}
      defaultValue={searchQuery}
      paginationFetch={async (arg) => {
        return await fetchSearchProfilesPagination(arg);
      }}
      getResult={async (params) => await onSearch(params)}
      optionalExtra={(formID) =>
        site ? (
          <FormSection>
            <DL>
              <DS
                dKey="Site"
                dValue={
                  <Link href={new SiteURL(language, site.id)} target="_blank">
                    {site.title} ({site.id}) - {site.home_page}
                  </Link>
                }
              />
            </DL>
            <HiddenInput
              id={`${formID}-site_id`}
              form={formID}
              name="site_id"
              defaultValue={site.id}
            />
          </FormSection>
        ) : (
          <SiteSection id={`${formID}-site_id`} form={formID} name="site_id" />
        )
      }
    />
  );
}
