import { ProjectError } from "#lib/errors";
import { collectIDs, type IEntityItem } from "#lib/entities";
import { useLanguage } from "#hooks";
import { fetchCreateProfileReleases } from "#api/account/administrator/profile";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { TextSection } from "#components/forms/sections";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { type IRelease, ReleaseURL } from "#entities/release";
import { type IProfile } from "./types";
import { isEmpty } from "#lib/util";

const FORM_FIELDS = {
  RELEASE_IDS: { name: "release_ids", label: "Release IDs" },
} as const;
const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  profile_id: IProfile["id"];
}

export const NewProfileReleasesForm = blockComponent(undefined, Component);

function Component({ profile_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();
  function collectInit(
    init: { ids: IRelease["id"][] },
    fieldName: IFieldName,
    value: string,
  ) {
    switch (fieldName) {
      case FORM_FIELDS.RELEASE_IDS.name: {
        const ids = collectIDs(value);

        if (isEmpty(ids)) {
          break;
        }

        init.ids = ids;

        break;
      }

      default: {
        throw new ProjectError(
          `Unknown input name "${fieldName satisfies never}".`,
        );
      }
    }
  }

  return (
    <Form<IFieldName, { ids: IRelease["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) => fetchCreateProfileReleases(profile_id, ids)}
      resultView={(formID, result) => (
        <>
          <p>Added releases to the profile:</p>
          <ListLocal
            items={result.map((release) => (
              <EntityItem
                key={release.id}
                item={release}
                urlBuilder={(id) => new ReleaseURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.RELEASE_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.RELEASE_IDS.name}
          label={FORM_FIELDS.RELEASE_IDS.label}
        >
          Newline-separated list of release IDs.
        </TextSection>
      )}
    </Form>
  );
}
