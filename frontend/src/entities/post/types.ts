export type { IPost } from "#codegen/schema/lib/entities/post/entity";
export type { IPostInit } from "#codegen/schema/lib/entities/post/init";
export type { IPostPreview } from "#codegen/schema/lib/entities/post/preview";
