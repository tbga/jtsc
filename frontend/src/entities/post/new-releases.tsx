import { isEmpty } from "#lib/util";
import { collectIDs } from "#lib/entities";
import { ProjectError } from "#lib/errors";
import { useLanguage } from "#hooks";
import { fetchPostReleasesCreate } from "#api/account/administrator/posts";
import { blockComponent } from "#components/meta";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { TextSection } from "#components/forms/sections";
import { type IBaseFormProps, Form } from "#components/forms";
import {
  ReleaseURL,
  type IRelease,
  type IReleasePreview,
} from "#entities/release";
import { type IPost } from "./types";

const FORM_FIELDS = {
  RELEASE_IDS: {
    name: "release_ids",
    label: "Release IDs",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  post_id: IPost["id"];
}

export const NewPostReleasesForm = blockComponent(undefined, Component);

function Component({ post_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();

  return (
    <Form<IFieldName, { ids: IRelease["id"][] }, IReleasePreview[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) => fetchPostReleasesCreate(post_id, { ids })}
      resultView={(formID, result) => (
        <>
          <p>Added these releases to the post:</p>
          <ListLocal
            items={result.map((releases) => (
              <EntityItem
                key={releases.id}
                item={releases}
                urlBuilder={(id) => new ReleaseURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.RELEASE_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.RELEASE_IDS.name}
          label={FORM_FIELDS.RELEASE_IDS.label}
        >
          Newline-separated list of release IDs.
        </TextSection>
      )}
    </Form>
  );
}

function collectInit(
  init: { ids: IRelease["id"][] },
  fieldName: IFieldName,
  value: string,
) {
  switch (fieldName) {
    case FORM_FIELDS.RELEASE_IDS.name: {
      const ids = collectIDs(value);

      if (isEmpty(ids)) {
        break;
      }

      init.ids = ids;

      break;
    }

    default: {
      throw new ProjectError(`Unknown input key "${fieldName}".`);
    }
  }
}
