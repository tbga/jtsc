import {
  CreatePageURL,
  EditURL,
  ProjectURL,
  RemovePageURL,
  SearchURL,
} from "#lib/urls";
import { type IBigSerialInteger, type IBigIntegerPositive } from "#types";
import { type IPost } from "./types";

export class PostURL extends ProjectURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}` });
  }
}

export class PostEditURL extends EditURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}` });
  }
}

export class PostArtistsCreateURL extends CreatePageURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}/artists` });
  }
}

export class PostArtistsRemoveURL extends RemovePageURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}/artists` });
  }
}

export class PostArtistsURL extends ProjectURL {
  constructor(language: string, id: IPost["id"], page?: IBigIntegerPositive) {
    const pathname = !page
      ? `/post/${id}/artists`
      : `/post/${id}/artists/${page}`;
    super({
      language,
      pathname,
    });
  }
}

export class PostReleasesCreateURL extends CreatePageURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}/releases` });
  }
}

export class PostReleasesRemoveURL extends RemovePageURL {
  constructor(language: string, id: IPost["id"]) {
    super({ language, pathname: `/post/${id}/releases` });
  }
}

export class PostReleasesURL extends ProjectURL {
  constructor(language: string, id: IPost["id"], page?: IBigIntegerPositive) {
    const pathname = !page
      ? `/post/${id}/releases`
      : `/post/${id}/releases/${page}`;
    super({
      language,
      pathname,
    });
  }
}

export class PostsURL extends ProjectURL {
  constructor(language: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/posts" : `/posts/${page}`;
    super({ language, pathname });
  }
}

export class PostsSearchURL extends SearchURL {
  constructor(language: string, query: string, page?: IBigSerialInteger) {
    const pathname = !page ? "/posts" : `/posts/${page}`;
    const searchParams = new URLSearchParams([["query", query]]);
    super({ language, pathname, searchParams });
  }
}
