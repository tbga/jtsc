import { useEffect, useState } from "react";
import {
  fetchPostArtistsPagination,
  fetchPostReleasesPagination,
} from "#api/post";
import { useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  type IArticleProps,
} from "#components";
import { DateTime } from "#components/dates";
import { DL, DS, ListButtons } from "#components/lists";
import { blockComponent } from "#components/meta";
import { Link, LinkButton } from "#components/links";
import { EntityID } from "#components/entities";
import { PostArtistsURL, PostEditURL, PostReleasesURL } from "#entities/post";
import { BIGINT_ZERO, type IAsyncReturnType } from "#types";
import { type IPost } from "./types";

export interface IPostArticleProps extends IArticleProps {
  post: IPost;
}

export const PostArticle = blockComponent(undefined, Component);

function Component({ post, headingLevel, ...blockProps }: IPostArticleProps) {
  const { language } = useLanguage();
  const { id, created_at, updated_at, title } = post;

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{title ?? "Untitled"}</Heading>
        <EntityID>{id}</EntityID>
      </ArticleHeader>
      <ArticleBody>
        <DL>
          <DS isHorizontal dKey="Artists" dValue={<Artists id={id} />} />
          <DS isHorizontal dKey="Releases" dValue={<Releases id={id} />} />
          <DS dKey="Date created" dValue={<DateTime dateTime={created_at} />} />
          <DS dKey="Last update" dValue={<DateTime dateTime={updated_at} />} />
        </DL>
      </ArticleBody>
      <ArticleFooter>
        <ListButtons>
          <LinkButton href={new PostEditURL(language, id)}>Edit</LinkButton>
        </ListButtons>
      </ArticleFooter>
    </Article>
  );
}

function Artists({ id }: { id: IPost["id"] }) {
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [pagination, changePagination] =
    useState<IAsyncReturnType<typeof fetchPostArtistsPagination>>();

  useEffect(() => {
    (async () => {
      try {
        switchLoading(true);
        const pagination = await fetchPostArtistsPagination(id);
        changePagination(pagination);
      } finally {
        switchLoading(false);
      }
    })();
  }, [id]);

  return isLoading ? (
    <LoadingBar />
  ) : BigInt(pagination!.total_count) === BIGINT_ZERO ? (
    <>{pagination!.total_count}</>
  ) : (
    <Link href={new PostArtistsURL(language, id)}>
      {pagination!.total_count}
    </Link>
  );
}

function Releases({ id }: { id: IPost["id"] }) {
  const { language } = useLanguage();
  const [isLoading, switchLoading] = useState(true);
  const [pagination, changePagination] =
    useState<IAsyncReturnType<typeof fetchPostReleasesPagination>>();

  useEffect(() => {
    (async () => {
      try {
        switchLoading(true);
        const pagination = await fetchPostReleasesPagination(id);
        changePagination(pagination);
      } finally {
        switchLoading(false);
      }
    })();
  }, [id]);

  return isLoading ? (
    <LoadingBar />
  ) : BigInt(pagination!.total_count) === BIGINT_ZERO ? (
    <>{pagination!.total_count}</>
  ) : (
    <Link href={new PostReleasesURL(language, id)}>
      {pagination!.total_count}
    </Link>
  );
}
