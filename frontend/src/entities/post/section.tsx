import { useEffect, useRef, useState, type ReactNode } from "react";
import { fetchRelease } from "#api/releases";
import { fetchPost } from "#api/post";
import { LoadingBar } from "#components";
import { blockComponent } from "#components/meta";
import { Fieldset, IFieldsetProps, Legend } from "#components/forms";
import { IntegerInput } from "#components/forms/inputs";
import { Button } from "#components/buttons";
import { ListButtons } from "#components/lists";
import { type IAsyncReturnType } from "#types";
import { PostCard, type IPostCardProps } from "./card";
import { SearchPostsForm } from "./search";

import styles from "./section.module.scss";

interface IProps extends IFieldsetProps, Pick<IPostCardProps, "headingLevel"> {
  id: string;
  form: string;
  name: string;
  label?: ReactNode;
}

export const PostSection = blockComponent(styles.block, Component);

function Component({
  id,
  name,
  form,
  headingLevel,
  label,
  defaultValue,
  children,
  ...blockProps
}: IProps) {
  const [isLoading, switchLoading] = useState(Boolean(defaultValue));
  const [post, changePost] = useState<IAsyncReturnType<typeof fetchPost>>();
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!defaultValue) {
      return;
    }

    const parsedID = (
      Array.isArray(defaultValue)
        ? (defaultValue as string[])[0]
        : String(defaultValue)
    ).trim();

    if (!parsedID) {
      return;
    }

    (async () => {
      try {
        const newRelease = await fetchRelease(String(parsedID));
        changePost(newRelease);
        if (inputRef.current) {
          inputRef.current.value = newRelease.id;
        }
      } finally {
        switchLoading(false);
      }
    })();
  }, [defaultValue]);

  async function resetPost(isFull: boolean = false) {
    if (isLoading) {
      return;
    }

    try {
      switchLoading(true);

      if (isFull || !defaultValue) {
        changePost(undefined);
        if (inputRef.current) {
          inputRef.current.value = "";
        }

        return;
      }

      const parsedID = (
        Array.isArray(defaultValue)
          ? (defaultValue as string[])[0]
          : String(defaultValue)
      ).trim();

      if (!parsedID) {
        return;
      }

      const newRelease = await fetchPost(String(parsedID));
      changePost(newRelease);
      if (inputRef.current) {
        inputRef.current.value = newRelease.id;
      }
    } finally {
      switchLoading(false);
    }
  }

  return (
    <Fieldset {...blockProps}>
      <Legend>{label ?? "Post"}</Legend>

      {isLoading ? (
        <LoadingBar />
      ) : !post ? (
        <SearchPostsForm
          className={styles.search}
          onSelect={async ({ id }) => {
            const newSite = await fetchPost(id);
            changePost(newSite);
            if (inputRef.current) {
              inputRef.current.value = newSite.id;
            }
          }}
        />
      ) : (
        <PostCard
          className={styles.card}
          // @ts-expect-error @TODO generic card component
          release={post}
        >
          <ListButtons isSpread>
            <Button onClick={async () => resetPost()}>Reset</Button>
            {defaultValue && (
              <Button onClick={async () => resetPost(true)}>Clear</Button>
            )}
          </ListButtons>
        </PostCard>
      )}
      <IntegerInput
        ref={inputRef}
        id={id}
        className={styles.id}
        form={form}
        name={name}
        defaultValue={defaultValue}
      />
      {children}
    </Fieldset>
  );
}
