import { useLanguage } from "#hooks";
import { Heading } from "#components";
import { Link } from "#components/links";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  type ICardProps,
} from "#components/lists";
import { blockComponent } from "#components/meta";
import { EntityID } from "#components/entities";
import { PostURL } from "./urls";
import { type IPostPreview } from "./types";

import styles from "./card.module.scss";

export interface IPostCardProps extends ICardProps {
  post: IPostPreview;
}

export const PostCard = blockComponent(styles.block, Component);

function Component({
  post,
  headingLevel,
  children,
  ...blockProps
}: IPostCardProps) {
  const { language } = useLanguage();
  const { id, releases, artists, title } = post;

  return (
    <Card {...blockProps}>
      <CardHeader>
        <Heading level={headingLevel}>{title ?? "Untitled"}</Heading>
      </CardHeader>
      <CardBody>
        <EntityID>{id}</EntityID>
        <DL>
          <DS isHorizontal dKey="Artists" dValue={artists} />
          <DS isHorizontal dKey="Releases" dValue={releases} />
        </DL>
      </CardBody>
      <CardFooter>
        <Link
          className={styles.details}
          href={new PostURL(language, id)}
          target="_blank"
        >
          Details
        </Link>
        {children}
      </CardFooter>
    </Card>
  );
}
