import { useState } from "react";
import { type IEntityItem } from "#lib/entities";
import { useLanguage } from "#hooks";
import { SearchForm } from "#components/forms";
import { blockComponent, type IBlockProps } from "#components/meta";
import {
  fetchPostItemSearch,
  fetchPostSearchPagination,
} from "#api/account/search";
import { ListInternal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { Button, ButtonSwitch } from "#components/buttons";
import { BIGINT_ONE, type IAsyncReturnType } from "#types";
import { PostURL } from "./urls";

import styles from "./search.module.scss";

interface IProps extends Omit<IBlockProps<"div">, "onSelect"> {
  onSelect?: (site: IEntityItem) => Promise<void>;
}

/**
 * @TODO close on select
 */
export const SearchPostsForm = blockComponent(styles.block, Component);

function Component({ onSelect, ...blockProps }: IProps) {
  const { language } = useLanguage();
  const [isOpened, switchOpened] = useState(false);
  const [query, changeQuery] = useState<string>();
  const [postItems, changePostItems] =
    useState<IAsyncReturnType<typeof fetchPostItemSearch>>();

  async function handleSearch(query: string) {
    const newSiteItems = await fetchPostItemSearch(query, String(BIGINT_ONE));
    changePostItems(newSiteItems);
    changeQuery(query);
  }

  return (
    <div {...blockProps}>
      <ButtonSwitch
        className={styles.switch}
        isOn={isOpened}
        onClick={() => {
          switchOpened(!isOpened);
        }}
      >
        {(isOn) => (isOn ? "Close" : "Search Posts")}
      </ButtonSwitch>
      {!isOpened ? undefined : (
        <>
          <SearchForm
            id="search-posts-rR4Gqep8G_VhuCBbIt2Tx"
            paginationFetch={async ({ query }) =>
              fetchPostSearchPagination(query)
            }
            getResult={async ({ query }) => handleSearch(query)}
          />
          {!postItems ? undefined : (
            <ListInternal
              pagination={postItems.pagination}
              onPageChange={async (page) => {
                const newPostItems = await fetchPostItemSearch(query!, page);
                changePostItems(newPostItems);
              }}
            >
              {postItems.posts.map((post) => (
                <EntityItem
                  key={post.id}
                  item={post}
                  urlBuilder={(id) => new PostURL(language, id)}
                >
                  {onSelect && (
                    <Button
                      className={styles.select}
                      onClick={async () => {
                        onSelect(post);
                      }}
                    >
                      Select
                    </Button>
                  )}
                </EntityItem>
              ))}
            </ListInternal>
          )}
        </>
      )}
    </div>
  );
}
