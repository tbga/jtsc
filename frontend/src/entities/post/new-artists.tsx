import { NEWLINE_SEPARATOR } from "#lib/strings";
import { ProjectError } from "#lib/errors";
import { type IEntityItem } from "#lib/entities";
import { useLanguage } from "#hooks";
import { fetchPostArtistsCreate } from "#api/account/administrator/posts";
import { blockComponent } from "#components/meta";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { TextSection } from "#components/forms/sections";
import { type IBaseFormProps, Form } from "#components/forms";
import { ArtistURL, type IArtist } from "#entities/artist";
import { type IPost } from "./types";

const FORM_FIELDS = {
  ARTIST_IDS: {
    name: "artist_ids",
    label: "Artist IDs",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IProps extends IBaseFormProps {
  post_id: IPost["id"];
}

export const NewPostArtistsForm = blockComponent(undefined, Component);

function Component({ post_id, id, ...blockProps }: IProps) {
  const { language } = useLanguage();
  return (
    <Form<IFieldName, { ids: IArtist["id"][] }, IEntityItem[]>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={collectInit}
      getResult={async ({ ids }) => fetchPostArtistsCreate(post_id, ids)}
      resultView={(formID, result) => (
        <>
          <p>Added these artists to the post:</p>
          <ListLocal
            items={result.map((artist) => (
              <EntityItem
                key={artist.id}
                item={artist}
                urlBuilder={(id) => new ArtistURL(language, id)}
              />
            ))}
          />
        </>
      )}
      {...blockProps}
    >
      {(formID) => (
        <TextSection
          id={`${id}-${FORM_FIELDS.ARTIST_IDS.name}`}
          form={formID}
          name={FORM_FIELDS.ARTIST_IDS.name}
          label={FORM_FIELDS.ARTIST_IDS.label}
        >
          Newline-separated list of artist IDs.
        </TextSection>
      )}
    </Form>
  );
}

function collectInit(
  init: { ids: IArtist["id"][] },
  fieldName: IFieldName,
  value: string,
) {
  switch (fieldName) {
    case FORM_FIELDS.ARTIST_IDS.name: {
      const ids = value
        .split(NEWLINE_SEPARATOR)
        .reduce<IArtist["id"][]>((ids, id) => {
          const parsedID = id.trim();

          if (!parsedID) {
            return ids;
          }

          ids.push(id);

          return ids;
        }, []);

      if (!ids.length) {
        break;
      }

      init.ids = ids;

      break;
    }

    default: {
      throw new ProjectError(`Unknown input key "${fieldName}".`);
    }
  }
}
