import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import { Heading } from "#components";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  DL,
  DS,
  type ICardProps,
} from "#components/lists";
import { Link } from "#components/links";
import { DateTime } from "#components/dates";
import { OperationURL } from "./urls";
import { Status } from "./status";
import { type IOperationPreview } from "./types";

export interface IOperationCardProps extends ICardProps {
  operation: IOperationPreview;
}

export const OperationCard = blockComponent(undefined, Component);

function Component({
  operation,
  headingLevel,
  ...blockProps
}: IOperationCardProps) {
  const { language } = useLanguage();
  const { id, type, status, created_at, updated_at } = operation;

  return (
    <Card {...blockProps}>
      <CardHeader>
        <Heading level={headingLevel}>{id}</Heading>
      </CardHeader>
      <CardBody>
        <DL>
          <DS isHorizontal dKey="Type" dValue={type} />
          <DS isHorizontal dKey="Status" dValue={<Status status={status} />} />
          <DS dKey="Date Started" dValue={<DateTime dateTime={created_at} />} />
          <DS dKey="Last Updated" dValue={<DateTime dateTime={updated_at} />} />
        </DL>
      </CardBody>
      <CardFooter>
        <Link href={new OperationURL(language, id)}>Details</Link>
      </CardFooter>
    </Card>
  );
}
