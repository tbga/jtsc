import clsx from "clsx";
import { ProjectError } from "#lib/errors";
import { blockComponent, type IBlockProps } from "#components/meta";
import { type IOperationStatus } from "./types";

import styles from "./status.module.scss";

interface IStatusProps extends IBlockProps<"span"> {
  status: IOperationStatus;
}

export const Status = blockComponent(styles.block, Component);

function Component({
  className,
  status,
  children,
  ...blockProps
}: IStatusProps) {
  let statusClass;

  switch (status) {
    case "pending": {
      statusClass = status;
      break;
    }

    case "in-progress": {
      statusClass = "progress";
      break;
    }

    case "finished": {
      statusClass = status;
      break;
    }

    case "failed": {
      statusClass = status;
      break;
    }

    default: {
      throw new ProjectError(`Unknown operation status ${status}`);
    }
  }

  const finalClassname = clsx(className, styles[statusClass]);

  return (
    <span className={finalClassname} {...blockProps}>
      {children ?? status}
    </span>
  );
}
