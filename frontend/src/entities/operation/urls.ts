import { AdministratorURL } from "#lib/urls";
import { type IBigIntegerPositive } from "#types";
import { type IOperation } from "./types";

export class OperationsAllURL extends AdministratorURL {
  constructor(language: string, page?: IBigIntegerPositive) {
    super({
      language,
      pathname: !page ? "/operations/all" : `/operations/all/${page}`,
    });
  }
}

export class OperationURL extends AdministratorURL {
  constructor(language: string, id: IOperation["id"]) {
    super({ language, pathname: `/operation/${id}` });
  }
}
