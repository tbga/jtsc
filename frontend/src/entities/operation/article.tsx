import { useEffect, useState } from "react";
import { type IEntityItem } from "#lib/entities";
import {
  fetchCancelOperation,
  fetchOperation,
  retryOperation,
} from "#api/account/administrator/operations";
import { useAccount, useLanguage } from "#hooks";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  JSONView,
  type IArticleProps,
  DEFAULT_HEADING_LEVEL,
} from "#components";
import { blockComponent } from "#components/meta";
import { Button } from "#components/buttons";
import { DateTime } from "#components/dates";
import { Link } from "#components/links";
import { DL, DS, Item, List, ListButtons } from "#components/lists";
import { ListLocal } from "#components/lists/generic";
import { EntityItem } from "#components/entities";
import { OperationURL } from "#entities/operation";
import { type IRelease, ReleaseURL } from "#entities/release";
import { ProfileURL } from "#entities/profile";
import { PublicImportURL } from "#entities/public-import";
import { Status } from "./status";
import { type IOperation } from "./types";

export interface IOperationArticleProps extends IArticleProps {
  operation: IOperation;
}

export const OperationArticle = blockComponent(undefined, Component);

function Component({
  operation,
  headingLevel = DEFAULT_HEADING_LEVEL,
  ...blockProps
}: IOperationArticleProps) {
  const { language } = useLanguage();
  const { isAdmin } = useAccount();
  const [nextOperation, changeNextOperation] = useState<IOperation>();
  const {
    id,
    type,
    status,
    created_at,
    updated_at,
    input_data,
    result_data,
    errors,
  } = operation;

  useEffect(() => {
    changeNextOperation(undefined);
  }, [id]);

  return (
    <Article {...blockProps}>
      <ArticleHeader>
        <Heading level={headingLevel}>{id}</Heading>
      </ArticleHeader>
      <ArticleBody>
        <Heading
          // @ts-expect-error level types
          level={headingLevel + 1}
        >
          Stats
        </Heading>
        <DL>
          <DS isHorizontal dKey="Type" dValue={type} />
          <DS isHorizontal dKey="Status" dValue={<Status status={status} />} />
          <DS dKey="Date Started" dValue={<DateTime dateTime={created_at} />} />
          <DS dKey="Last Update" dValue={<DateTime dateTime={updated_at} />} />
        </DL>
        {isAdmin && (
          <ListButtons isSpread>
            <Button
              disabled={status !== "pending" && status !== "in-progress"}
              onClick={async () => {
                // @TODO rewrite after operations rework
                const updatedID = await fetchCancelOperation(id);
                const updatedOperation = await fetchOperation(updatedID);
                changeNextOperation(updatedOperation);
              }}
            >
              Cancel
            </Button>
            <Button
              disabled={!errors}
              onClick={async () => {
                const newOperation = await retryOperation(id);
                changeNextOperation(newOperation);
              }}
            >
              Retry
            </Button>
          </ListButtons>
        )}
        {nextOperation && (
          <p>
            The operation was{" "}
            <Link href={new OperationURL(language, nextOperation.id)}>
              retried
            </Link>
            .
          </p>
        )}
      </ArticleBody>
      <ArticleFooter>
        <Heading
          // @ts-expect-error level types
          level={headingLevel + 1}
        >
          Starting data
        </Heading>
        <JSONView json={input_data} />
        {result_data && (
          <>
            <Heading
              // @ts-expect-error level types
              level={headingLevel + 1}
            >
              Finished
            </Heading>
            <Result type={type} result_data={result_data} />
          </>
        )}
        {errors && (
          <>
            <Heading
              // @ts-expect-error level types
              level={headingLevel + 1}
            >
              Errors
            </Heading>
            <JSONView json={errors} />
          </>
        )}
      </ArticleFooter>
    </Article>
  );
}

interface IResultProps extends Pick<IOperation, "type" | "result_data"> {}

function Result({ type, result_data }: IResultProps) {
  const { language } = useLanguage();

  switch (type) {
    case "releases_create": {
      const id = (result_data as unknown as IRelease["id"][])[0];

      return (
        <List>
          <Item>
            <Link href={new ReleaseURL(language, id)} target={"_blank"}>
              Created release
            </Link>
          </Item>
        </List>
      );
    }

    case "profiles_create": {
      const items = result_data as unknown as IEntityItem[];

      return (
        <>
          <p>Created these profiles:</p>{" "}
          <List>
            {items.map((profile) => (
              <EntityItem
                key={profile.id}
                item={profile}
                urlBuilder={(id) => new ProfileURL(language, id)}
              />
            ))}
          </List>
        </>
      );
    }

    case "public_imports_create": {
      const importItems = result_data as [IEntityItem, ...IEntityItem[]];

      return (
        <>
          <p>Created these imports:</p>{" "}
          <List>
            {importItems.map((publicImport) => (
              <EntityItem
                key={publicImport.id}
                item={publicImport}
                urlBuilder={(id) => new PublicImportURL(language, id)}
              />
            ))}
          </List>
        </>
      );
    }

    default: {
      return <JSONView json={result_data} />;
    }
  }
}
