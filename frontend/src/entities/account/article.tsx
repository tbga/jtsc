import { ProjectError } from "#lib/errors";
import { AdministratorURL } from "#lib/urls";
import { useLanguage } from "#hooks";
import { blockComponent } from "#components/meta";
import {
  Article,
  ArticleBody,
  ArticleFooter,
  ArticleHeader,
  Heading,
  LoadingBar,
  type IArticleProps,
} from "#components";
import { DateTime } from "#components/dates";
import { Link } from "#components/links";
import { DL, DS } from "#components/lists";
import { type IUserAgent, type IAccountRole, type IAccount } from "./types";

interface IAccountArticleProps extends IArticleProps {
  account: IAccount;
  userAgent?: IUserAgent;
}

export const AccountArticle = blockComponent(undefined, Component);

function Component({
  account,
  userAgent,
  headingLevel = 2,
  ...blockProps
}: IAccountArticleProps) {
  const { language, isLoading } = useLanguage();
  const { role } = account;

  return (
    <Article {...blockProps}>
      {isLoading ? (
        <>
          <ArticleHeader>
            <LoadingBar />
          </ArticleHeader>
          <ArticleBody>
            <LoadingBar />
          </ArticleBody>
          <ArticleFooter>
            <LoadingBar />
          </ArticleFooter>
        </>
      ) : (
        <>
          <ArticleBody>
            <DL>
              <DS isHorizontal dKey="Role" dValue={<Role role={role} />} />
            </DL>
          </ArticleBody>

          <ArticleFooter>
            {userAgent && (
              <UserAgent
                userAgent={userAgent}
                // @ts-expect-error literal union
                headingLevel={headingLevel + 1}
              />
            )}
          </ArticleFooter>
        </>
      )}
    </Article>
  );
}

interface IUserAgentProps
  extends Pick<Required<IAccountArticleProps>, "userAgent" | "headingLevel"> {}

function UserAgent({ userAgent, headingLevel }: IUserAgentProps) {
  const { server, client } = userAgent;

  return (
    <>
      <Heading level={headingLevel}>User Agent</Heading>
      <DL>
        <DS dKey="Server" dValue={server} />
        <DS dKey="Client" dValue={client} />
      </DL>
    </>
  );
}

function Role({ role }: { role: IAccountRole }) {
  const { language } = useLanguage();

  switch (role) {
    case "user":
      return <>User</>;

    case "administrator":
      return (
        <Link href={new AdministratorURL({ language })}>Administrator</Link>
      );

    default:
      throw new ProjectError(`Unknown role \"${role}\".`);
  }
}
