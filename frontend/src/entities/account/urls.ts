import { ProjectURL } from "#lib/urls";

export class AccountRegistrationURL extends ProjectURL {
  constructor(language: string) {
    super({ language, pathname: "/authentication/register" });
  }
}

export class AccountLoginURL extends ProjectURL {
  constructor(language: string) {
    super({ language, pathname: "/authentication/login" });
  }
}
