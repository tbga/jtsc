import { IS_INVITE_ONLY } from "#environment/variables";
import { ProjectError } from "#lib/errors";
import { nanoIDSchema } from "#lib/strings";
import { blockComponent } from "#components/meta";
import { type IBaseFormProps, Form } from "#components/forms";
import { PasswordSection, TextSection } from "#components/forms/sections";
import { useAccount } from "#hooks";
import { validateAccountInit } from "./lib";
import { type IAccountInit } from "./types";

const FORM_FIELDS = {
  LOGIN: {
    name: "login",
    label: "Login",
  },
  PASSWORD: {
    name: "password",
    label: "Password",
  },
  INVITE: {
    name: "invitation_code",
    label: "Invitation Code",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface IRegisterAccountFormProps extends IBaseFormProps {}

export const RegisterAccountForm = blockComponent(undefined, Component);

function Component({ id, ...blockProps }: IRegisterAccountFormProps) {
  const { register } = useAccount();

  return (
    <Form<IFieldName, IAccountInit>
      id={id}
      isClient
      fieldNames={FIELD_NAMES}
      resultView={null}
      collectInit={(init, fieldName, value) => {
        switch (fieldName) {
          case FORM_FIELDS.LOGIN.name:
          case FORM_FIELDS.PASSWORD.name:
          case FORM_FIELDS.INVITE.name: {
            init[fieldName] = value;
            break;
          }

          default: {
            throw new ProjectError(`Unknown field "${fieldName}".`);
          }
        }
      }}
      validateInit={validateAccountInit}
      getResult={async (init) => {
        await register(init);
      }}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${id}-${FORM_FIELDS.LOGIN.name}`}
            form={formID}
            name={FORM_FIELDS.LOGIN.name}
            label={FORM_FIELDS.LOGIN.label}
            required
            minLength={5}
            maxLength={25}
          />
          <PasswordSection
            id={`${id}-${FORM_FIELDS.PASSWORD.name}`}
            form={formID}
            name={FORM_FIELDS.PASSWORD.name}
            label={FORM_FIELDS.PASSWORD.label}
            autoComplete="new-password"
            required
            minLength={10}
            maxLength={72}
          />
          <TextSection
            id={`${id}-${FORM_FIELDS.INVITE.name}`}
            form={formID}
            name={FORM_FIELDS.INVITE.name}
            label={FORM_FIELDS.INVITE.label}
            required={IS_INVITE_ONLY}
            minLength={nanoIDSchema.minLength}
            maxLength={nanoIDSchema.maxLength}
          />
        </>
      )}
    </Form>
  );
}
