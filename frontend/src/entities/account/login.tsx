import { Form, IBaseFormProps } from "#components/forms";
import { PasswordSection, TextSection } from "#components/forms/sections";
import { blockComponent } from "#components/meta";
import { useAccount } from "#hooks";
import { ProjectError } from "#lib/errors";
import { validateAccountLogin } from "./lib";
import { type IAccountLogin } from "./types";

const FORM_FIELDS = {
  LOGIN: {
    name: "login",
    label: "Login",
  },
  PASSWORD: {
    name: "password",
    label: "Password",
  },
} as const;

const FIELD_NAMES = Object.values(FORM_FIELDS).map(({ name }) => name);
type IFieldName = (typeof FIELD_NAMES)[number];

interface ILoginAccountFormProps extends IBaseFormProps {}

/**
 * @TODO fix it not resubmtting on fail
 */
export const LoginAccountForm = blockComponent(undefined, Component);

function Component({ id, ...blockProps }: ILoginAccountFormProps) {
  const { login } = useAccount();

  return (
    <Form<IFieldName, IAccountLogin>
      isClient
      id={id}
      fieldNames={FIELD_NAMES}
      collectInit={(accLogin, fieldName, value) => {
        switch (fieldName) {
          case FORM_FIELDS.LOGIN.name:
          case FORM_FIELDS.PASSWORD.name: {
            accLogin[fieldName] = value;
            break;
          }

          default: {
            throw new ProjectError(`Unknown field "${fieldName}".`);
          }
        }
      }}
      validateInit={validateAccountLogin}
      getResult={async (accLogin) => {
        await login(accLogin);
      }}
      resultView={null}
      {...blockProps}
    >
      {(formID) => (
        <>
          <TextSection
            id={`${id}-${FORM_FIELDS.LOGIN.name}`}
            form={formID}
            name={FORM_FIELDS.LOGIN.name}
            label={FORM_FIELDS.LOGIN.label}
            required
            minLength={5}
            maxLength={25}
          >
            Account login.
          </TextSection>
          <PasswordSection
            form={formID}
            id={`${id}-${FORM_FIELDS.PASSWORD.name}`}
            name={FORM_FIELDS.PASSWORD.name}
            label={FORM_FIELDS.PASSWORD.label}
            autoComplete="current-password"
            required
            minLength={10}
            maxLength={72}
          >
            Account password.
          </PasswordSection>
        </>
      )}
    </Form>
  );
}
