import { type INonEmptyString } from "#lib/strings";

export type { IAccount } from "#codegen/schema/lib/entities/account/entity";
export type { IAccountInit } from "#codegen/schema/lib/entities/account/init";
export type { IAccountLogin } from "#codegen/schema/lib/entities/account/login";
export type { IAccountPreview } from "#codegen/schema/lib/entities/account/preview";
export type { IAccountRole } from "#codegen/schema/lib/entities/account/role";

export interface IUserAgent {
  server: INonEmptyString;
  client: INonEmptyString;
}
