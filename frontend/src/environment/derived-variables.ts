import path from "path";
import { cwd } from "process";
import {
  NODE_ENV,
  API_ORIGIN,
  ORIGIN,
  PROJECT_FOLDER,
  SCHEMA_PATH,
  STORAGE_ORIGIN,
} from "./variables";

export const IS_DEVELOPMENT = NODE_ENV === "development";

export const API_URL = new URL(API_ORIGIN);
export const ORIGIN_URL = new URL(ORIGIN);
export const STORAGE_URL = new URL(STORAGE_ORIGIN);
export const PROJECT_ROOT = PROJECT_FOLDER ?? cwd();

export const SCHEMA_FOLDER =
  SCHEMA_PATH ?? path.join(PROJECT_ROOT, "..", "schema");
