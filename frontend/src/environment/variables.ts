import { fromJSON } from "#lib/json";
import { type ITheme } from "#lib/theme";

export const NODE_ENV = process.env.NODE_ENV;

export const TITLE = process.env.NEXT_PUBLIC_TITLE!;
export const API_ORIGIN = process.env.NEXT_PUBLIC_API_ORIGIN!;
export const STORAGE_ORIGIN = process.env.NEXT_PUBLIC_STORAGE_ORIGIN!;
export const ORIGIN = process.env.NEXT_PUBLIC_ORIGIN!;
export const REPO = process.env.NEXT_PUBLIC_REPO!;
export const DEFAULT_THEME = process.env.NEXT_PUBLIC_DEFAULT_THEME! as ITheme;
export const IS_PUBLIC = fromJSON<boolean>(process.env.NEXT_PUBLIC_IS_PUBLIC!);
export const IS_INVITE_ONLY = fromJSON<boolean>(
  process.env.NEXT_PUBLIC_IS_INVITE_ONLY!,
);
export const SUPPORTED_LANGUAGES = fromJSON<string[]>(
  process.env.NEXT_PUBLIC_SUPPORTED_LANGUAGES!,
);
export const DEFAULT_LANGUAGE = process.env.NEXT_PUBLIC_DEFAULT_LANGUAGE!;

export const PROJECT_FOLDER = process.env.FRONTEND_PATH;
export const SCHEMA_PATH = process.env.SCHEMA_PATH;
