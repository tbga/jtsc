import { platform } from "process";

export const IS_BROWSER = typeof window !== "undefined";
export const IS_WINDOWS = platform === "win32";

/**
 * In milliseconds.
 */
export const DURATION_MS = {
  SECOND: 1_000,
  // 1000 * 60
  MINUTE: 60_000,
  // 1000 * 60 * 60
  HOUR: 3_600_000,
  // 1000 * 60 * 60 * 24
  DAY: 86_400_000,
  // 1000 * 60 * 60 * 24 * 7
  WEEK: 604_800_000,
  /**
   * An average month length, for things like "next month"
   * manipulate the date string instead.
   */
  // 1000 * 60 * 60 * 24 * 30
  MONTH: 2_592_000_000,
  // 1000 * 60 * 60 * 24 * 365
  YEAR: 31_536_000_000,
} as const;
