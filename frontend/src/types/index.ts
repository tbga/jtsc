import { type ReactNode } from "react";

export { validateBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

export type { IBigInteger } from "#codegen/schema/lib/types/numbers/big-integer";
export type { IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
export type { IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
export type { ISerialInteger } from "#codegen/schema/lib/types/numbers/serial";

export type IValues<Type = Record<string, unknown>> = Type[keyof Type];

export type IActionResult = IActionSuccess | IActionFailure;

export interface IActionSuccess {
  is_successful: true;
}

export interface IActionFailure {
  is_successful: false;
}

export type IUnknownFunction = (
  ...args: unknown[]
) => Promise<unknown> | unknown;

export type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

/**
 * Infers a return type of an async function.
 */
export type IAsyncReturnType<Func extends (...args: any[]) => Promise<any>> =
  Awaited<ReturnType<Func>>;

export type IDynamicChildren<InputType extends unknown[] = unknown[]> = (
  ...args: InputType
) => ReactNode;

/**
 * Make selected keys optional.
 */
export type IPartialSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Partial<Type>, Key>;

/**
 * Make selected key required.
 */
export type IRequiredSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Required<Type>, Key>;

export const EMPTY_OBJECT = {} as const;

Object.freeze(EMPTY_OBJECT);

export const EMPTY_ARRAY = [] as const;

Object.freeze(EMPTY_ARRAY);

export function EMPTY_FUNCTION<Args extends unknown[]>(...args: Args) {}
export async function EMPTY_ASYNC_FUNCTION<Args extends unknown[]>(
  ...args: Args
) {}

Object.freeze(EMPTY_FUNCTION);

export const BIGINT_ZERO = BigInt(0);
export const BIGINT_ONE = BigInt(1);
