/*
 * This file was generated automatically, do not edit it by hand.
 */
export const statsSitesSchema = {
  $id: "https://jtsc-schemas.org/stats/sites.schema.json",
  title: "StatsSites",
  description: "Stats of the sites.",
  type: "object",
  additionalProperties: false,
  required: ["all", "published", "local"],
  properties: {
    all: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    published: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    local: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
