/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { statsSitesSchema } from "./schema";
import { validate } from "./validate";
import { IStatsSites } from "./types";
export const validateStatsSites = createValidator<IStatsSites>(
  validate as ValidateFunction<IStatsSites>,
  statsSitesSchema as unknown as IJSONSchema
);
