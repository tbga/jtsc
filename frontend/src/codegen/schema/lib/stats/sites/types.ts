/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * Stats of the sites.
 */
export interface IStatsSites {
  all: IBigIntegerPositive;
  published: IBigIntegerPositive;
  local: IBigIntegerPositive;
}
