/*
 * This file was generated automatically, do not edit it by hand.
 */
export { statsSitesSchema } from "./schema";
export { validateStatsSites } from "./lib";
export type { IStatsSites } from "./types";
