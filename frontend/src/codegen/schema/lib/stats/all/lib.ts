/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { statsAllSchema } from "./schema";
import { validate } from "./validate";
import { IStatsAll } from "./types";
export const validateStatsAll = createValidator<IStatsAll>(
  validate as ValidateFunction<IStatsAll>,
  statsAllSchema as unknown as IJSONSchema
);
