/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
import { type IStatsSites } from "#codegen/schema/lib/stats/sites";

/**
 * All stats of the application.
 */
export interface IStatsAll {
  artists: IBigIntegerPositive;
  posts: IBigIntegerPositive;
  sites: IStatsSites;
  profiles: IBigIntegerPositive;
  releases: IBigIntegerPositive;
}
