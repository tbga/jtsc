/*
 * This file was generated automatically, do not edit it by hand.
 */
export { statsAllSchema } from "./schema";
export { validateStatsAll } from "./lib";
export type { IStatsAll } from "./types";
