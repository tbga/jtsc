/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicSiteSchema = {
  $id: "http://jtsc-schemas.org/public-exports/entities/site.schema.json",
  title: "PublicSite",
  type: "object",
  additionalProperties: false,
  required: ["id", "home_page", "title"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    long_title: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
} as const;
