/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicSiteSchema } from "./schema";
import { validate } from "./validate";
import { IPublicSite } from "./types";
export const validatePublicSite = createValidator<IPublicSite>(
  validate as ValidateFunction<IPublicSite>,
  publicSiteSchema as unknown as IJSONSchema
);
