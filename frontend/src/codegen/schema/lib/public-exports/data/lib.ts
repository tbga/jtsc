/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportDataSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportData } from "./types";
export const validatePublicExportData = createValidator<IPublicExportData>(
  validate as ValidateFunction<IPublicExportData>,
  publicExportDataSchema as unknown as IJSONSchema
);
