/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicExportMetaSchema = {
  $id: "http://jtsc-schemas.org/public-exports/meta.schema.json",
  title: "PublicExportMeta",
  description: "The metadata of the public export.",
  type: "object",
  additionalProperties: false,
  required: ["id", "type", "version", "title"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    type: {
      const: "basic",
    },
    version: {
      const: 1,
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
