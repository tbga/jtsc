/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportMetaSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportMeta } from "./types";
export const validatePublicExportMeta = createValidator<IPublicExportMeta>(
  validate as ValidateFunction<IPublicExportMeta>,
  publicExportMetaSchema as unknown as IJSONSchema
);
