/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/public-exports/meta.schema.json",
  title: "PublicExportMeta",
  description: "The metadata of the public export.",
  type: "object",
  additionalProperties: false,
  required: ["id", "type", "version", "title"],
  properties: {
    id: { $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json" },
    type: { const: "basic" },
    version: { const: 1 },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/public-exports/meta.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.type === undefined && (missing0 = "type")) ||
        (data.version === undefined && (missing0 = "version")) ||
        (data.title === undefined && (missing0 = "title"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "type" ||
              key0 === "version" ||
              key0 === "title"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (errors === _errs3) {
                if (typeof data0 === "string") {
                  if (func1(data0) > 36) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 36 },
                        message: "must NOT have more than 36 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data0) < 36) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 36 },
                          message: "must NOT have fewer than 36 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (!formats0.test(data0)) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                            keyword: "format",
                            params: { format: "uuid" },
                            message: 'must match format "' + "uuid" + '"',
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.type !== undefined) {
              const _errs5 = errors;
              if ("basic" !== data.type) {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/type",
                    schemaPath: "#/properties/type/const",
                    keyword: "const",
                    params: { allowedValue: "basic" },
                    message: "must be equal to constant",
                  },
                ];
                return false;
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.version !== undefined) {
                const _errs6 = errors;
                if (1 !== data.version) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/version",
                      schemaPath: "#/properties/version/const",
                      keyword: "const",
                      params: { allowedValue: 1 },
                      message: "must be equal to constant",
                    },
                  ];
                  return false;
                }
                var valid0 = _errs6 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.title !== undefined) {
                  let data3 = data.title;
                  const _errs7 = errors;
                  const _errs8 = errors;
                  if (typeof data3 !== "string") {
                    let dataType1 = typeof data3;
                    let coerced1 = undefined;
                    if (!(coerced1 !== undefined)) {
                      if (dataType1 == "number" || dataType1 == "boolean") {
                        coerced1 = "" + data3;
                      } else if (data3 === null) {
                        coerced1 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/title",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced1 !== undefined) {
                      data3 = coerced1;
                      if (data !== undefined) {
                        data["title"] = coerced1;
                      }
                    }
                  }
                  if (errors === _errs8) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 216) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/title",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 216 },
                            message: "must NOT have more than 216 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs7 === errors;
                } else {
                  var valid0 = true;
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
