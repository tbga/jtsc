/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type INanoID } from "#codegen/schema/lib/types/strings/nanoid";

/**
 * Backend configuration options.
 */
export interface IBackendConfiguration {
  /**
   * A key to sign cookies.
   */
  SECRET_KEY: string;
  ADMIN_INVITE?: INanoID;
  /**
   * Visibility of the public API. If set to `false`, any access outside of register/login endpoints requires an account.
   */
  IS_PUBLIC: boolean;
  /**
   * Controls invitation code is required during registration or not.
   */
  IS_INVITE_ONLY: boolean;
  /**
   * Database connection url.
   */
  DATABASE_URL: string;
  /**
   * Database query logging.
   */
  IS_DATABASE_LOGGING_ENABLED?: boolean;
  /**
   * Run the server in https mode.
   */
  IS_HTTPS_ENABLED?: boolean;
  /**
   * The path to https sertificate.
   */
  SECURE_KEY_PATH?: string;
  /**
   * The path to https sertificate.
   */
  SECURE_CERTIFICATE_PATH?: string;
  /**
   * Port of the server.
   */
  BACKEND_PORT: number;
  /**
   * Local folder for the frontend files.
   */
  PUBLIC_PATH?: string;
  /**
   * Origin of the file storage.
   */
  STORAGE_ORIGIN: string;
  /**
   * The path of local storage on file system.
   */
  LOCAL_STORAGE_FOLDER?: string;
  /**
   * The path of public storage on file system.
   */
  PUBLIC_STORAGE_FOLDER?: string;
  /**
   * Local folder for the temporary files.
   */
  TEMPORARY_FOLDER?: string;
  /**
   * Local folder of the backend.
   */
  BACKEND_PATH?: string;
  /**
   * Local folder of schemas.
   */
  SCHEMA_PATH?: string;
  /**
   * Local folder of the database.
   */
  DATABASE_FOLDER?: string;
  /**
   * The path of public exports folder on file system.
   */
  PUBLIC_EXPORTS_FOLDER?: string;
}
