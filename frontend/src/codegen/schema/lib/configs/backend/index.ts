/*
 * This file was generated automatically, do not edit it by hand.
 */
export { backendConfigurationSchema } from "./schema";
export { validateBackendConfiguration } from "./lib";
export type { IBackendConfiguration } from "./types";
