/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/configs/backend.schema.json",
  title: "BackendConfiguration",
  description: "Backend configuration options.",
  type: "object",
  additionalProperties: false,
  required: [
    "DATABASE_URL",
    "STORAGE_ORIGIN",
    "BACKEND_PORT",
    "SECRET_KEY",
    "IS_PUBLIC",
    "IS_INVITE_ONLY",
  ],
  properties: {
    SECRET_KEY: {
      description: "A key to sign cookies.",
      type: "string",
      minLength: 32,
    },
    ADMIN_INVITE: {
      $ref: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
    },
    IS_PUBLIC: {
      description:
        "Visibility of the public API. If set to `false`, any access outside of register/login endpoints requires an account.",
      type: "boolean",
      default: false,
    },
    IS_INVITE_ONLY: {
      description:
        "Controls invitation code is required during registration or not.",
      type: "boolean",
      default: true,
    },
    DATABASE_URL: {
      description: "Database connection url.",
      type: "string",
      examples: [
        "postgres://<username>:<password>@<host>:<port>/<database-name>",
      ],
    },
    IS_DATABASE_LOGGING_ENABLED: {
      description: "Database query logging.",
      type: "boolean",
      default: false,
    },
    IS_HTTPS_ENABLED: {
      description: "Run the server in https mode.",
      type: "boolean",
      default: false,
    },
    SECURE_KEY_PATH: {
      description: "The path to https sertificate.",
      type: "string",
      default: "./configs/development/key.pem",
    },
    SECURE_CERTIFICATE_PATH: {
      description: "The path to https sertificate.",
      type: "string",
      default: "./configs/development/cert.pem",
    },
    BACKEND_PORT: {
      description: "Port of the server.",
      type: "number",
      default: 3499,
    },
    PUBLIC_PATH: {
      description: "Local folder for the frontend files.",
      type: "string",
      examples: ["/absolute/path/to/public/folder"],
    },
    STORAGE_ORIGIN: {
      description: "Origin of the file storage.",
      type: "string",
      examples: ["http://localhost:3499/storage"],
    },
    LOCAL_STORAGE_FOLDER: {
      description: "The path of local storage on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/storage/files",
      examples: ["/absolute/path/to/storage/folder"],
    },
    PUBLIC_STORAGE_FOLDER: {
      description: "The path of public storage on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/storage/public",
      examples: ["/absolute/path/to/storage/folder"],
    },
    TEMPORARY_FOLDER: {
      description: "Local folder for the temporary files.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/temporary",
      examples: ["/absolute/path/to/temp/folder"],
    },
    BACKEND_PATH: {
      description: "Local folder of the backend.",
      type: "string",
      examples: ["/absolute/path/to/backend/folder"],
    },
    SCHEMA_PATH: {
      description: "Local folder of schemas.",
      type: "string",
      examples: ["/absolute/path/to/schema/folder"],
    },
    DATABASE_FOLDER: {
      description: "Local folder of the database.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/database",
      examples: ["/absolute/path/to/database/folder"],
    },
    PUBLIC_EXPORTS_FOLDER: {
      description: "The path of public exports folder on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/public-exports",
      examples: ["/absolute/path/to/public-exports/folder"],
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
  title: "NanoID",
  description:
    "The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.",
  type: "string",
  minLength: 21,
  maxLength: 21,
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/configs/backend.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.DATABASE_URL === undefined && (missing0 = "DATABASE_URL")) ||
        (data.STORAGE_ORIGIN === undefined && (missing0 = "STORAGE_ORIGIN")) ||
        (data.BACKEND_PORT === undefined && (missing0 = "BACKEND_PORT")) ||
        (data.SECRET_KEY === undefined && (missing0 = "SECRET_KEY")) ||
        (data.IS_PUBLIC === undefined && (missing0 = "IS_PUBLIC")) ||
        (data.IS_INVITE_ONLY === undefined && (missing0 = "IS_INVITE_ONLY"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!func1.call(schema31.properties, key0)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.SECRET_KEY !== undefined) {
            let data0 = data.SECRET_KEY;
            const _errs2 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/SECRET_KEY",
                      schemaPath: "#/properties/SECRET_KEY/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["SECRET_KEY"] = coerced0;
                }
              }
            }
            if (errors === _errs2) {
              if (typeof data0 === "string") {
                if (func2(data0) < 32) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/SECRET_KEY",
                      schemaPath: "#/properties/SECRET_KEY/minLength",
                      keyword: "minLength",
                      params: { limit: 32 },
                      message: "must NOT have fewer than 32 characters",
                    },
                  ];
                  return false;
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.ADMIN_INVITE !== undefined) {
              let data1 = data.ADMIN_INVITE;
              const _errs4 = errors;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/ADMIN_INVITE",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/nanoid.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["ADMIN_INVITE"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func2(data1) > 21) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/ADMIN_INVITE",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/nanoid.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 21 },
                        message: "must NOT have more than 21 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func2(data1) < 21) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/ADMIN_INVITE",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/nanoid.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 21 },
                          message: "must NOT have fewer than 21 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.IS_PUBLIC !== undefined) {
                let data2 = data.IS_PUBLIC;
                const _errs7 = errors;
                if (typeof data2 !== "boolean") {
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (data2 === "false" || data2 === 0 || data2 === null) {
                      coerced2 = false;
                    } else if (data2 === "true" || data2 === 1) {
                      coerced2 = true;
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/IS_PUBLIC",
                          schemaPath: "#/properties/IS_PUBLIC/type",
                          keyword: "type",
                          params: { type: "boolean" },
                          message: "must be boolean",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["IS_PUBLIC"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.IS_INVITE_ONLY !== undefined) {
                  let data3 = data.IS_INVITE_ONLY;
                  const _errs9 = errors;
                  if (typeof data3 !== "boolean") {
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (data3 === "false" || data3 === 0 || data3 === null) {
                        coerced3 = false;
                      } else if (data3 === "true" || data3 === 1) {
                        coerced3 = true;
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/IS_INVITE_ONLY",
                            schemaPath: "#/properties/IS_INVITE_ONLY/type",
                            keyword: "type",
                            params: { type: "boolean" },
                            message: "must be boolean",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["IS_INVITE_ONLY"] = coerced3;
                      }
                    }
                  }
                  var valid0 = _errs9 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.DATABASE_URL !== undefined) {
                    let data4 = data.DATABASE_URL;
                    const _errs11 = errors;
                    if (typeof data4 !== "string") {
                      let dataType4 = typeof data4;
                      let coerced4 = undefined;
                      if (!(coerced4 !== undefined)) {
                        if (dataType4 == "number" || dataType4 == "boolean") {
                          coerced4 = "" + data4;
                        } else if (data4 === null) {
                          coerced4 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/DATABASE_URL",
                              schemaPath: "#/properties/DATABASE_URL/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced4 !== undefined) {
                        data4 = coerced4;
                        if (data !== undefined) {
                          data["DATABASE_URL"] = coerced4;
                        }
                      }
                    }
                    var valid0 = _errs11 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.IS_DATABASE_LOGGING_ENABLED !== undefined) {
                      let data5 = data.IS_DATABASE_LOGGING_ENABLED;
                      const _errs13 = errors;
                      if (typeof data5 !== "boolean") {
                        let coerced5 = undefined;
                        if (!(coerced5 !== undefined)) {
                          if (
                            data5 === "false" ||
                            data5 === 0 ||
                            data5 === null
                          ) {
                            coerced5 = false;
                          } else if (data5 === "true" || data5 === 1) {
                            coerced5 = true;
                          } else {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/IS_DATABASE_LOGGING_ENABLED",
                                schemaPath:
                                  "#/properties/IS_DATABASE_LOGGING_ENABLED/type",
                                keyword: "type",
                                params: { type: "boolean" },
                                message: "must be boolean",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced5 !== undefined) {
                          data5 = coerced5;
                          if (data !== undefined) {
                            data["IS_DATABASE_LOGGING_ENABLED"] = coerced5;
                          }
                        }
                      }
                      var valid0 = _errs13 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.IS_HTTPS_ENABLED !== undefined) {
                        let data6 = data.IS_HTTPS_ENABLED;
                        const _errs15 = errors;
                        if (typeof data6 !== "boolean") {
                          let coerced6 = undefined;
                          if (!(coerced6 !== undefined)) {
                            if (
                              data6 === "false" ||
                              data6 === 0 ||
                              data6 === null
                            ) {
                              coerced6 = false;
                            } else if (data6 === "true" || data6 === 1) {
                              coerced6 = true;
                            } else {
                              validate20.errors = [
                                {
                                  instancePath:
                                    instancePath + "/IS_HTTPS_ENABLED",
                                  schemaPath:
                                    "#/properties/IS_HTTPS_ENABLED/type",
                                  keyword: "type",
                                  params: { type: "boolean" },
                                  message: "must be boolean",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced6 !== undefined) {
                            data6 = coerced6;
                            if (data !== undefined) {
                              data["IS_HTTPS_ENABLED"] = coerced6;
                            }
                          }
                        }
                        var valid0 = _errs15 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.SECURE_KEY_PATH !== undefined) {
                          let data7 = data.SECURE_KEY_PATH;
                          const _errs17 = errors;
                          if (typeof data7 !== "string") {
                            let dataType7 = typeof data7;
                            let coerced7 = undefined;
                            if (!(coerced7 !== undefined)) {
                              if (
                                dataType7 == "number" ||
                                dataType7 == "boolean"
                              ) {
                                coerced7 = "" + data7;
                              } else if (data7 === null) {
                                coerced7 = "";
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/SECURE_KEY_PATH",
                                    schemaPath:
                                      "#/properties/SECURE_KEY_PATH/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced7 !== undefined) {
                              data7 = coerced7;
                              if (data !== undefined) {
                                data["SECURE_KEY_PATH"] = coerced7;
                              }
                            }
                          }
                          var valid0 = _errs17 === errors;
                        } else {
                          var valid0 = true;
                        }
                        if (valid0) {
                          if (data.SECURE_CERTIFICATE_PATH !== undefined) {
                            let data8 = data.SECURE_CERTIFICATE_PATH;
                            const _errs19 = errors;
                            if (typeof data8 !== "string") {
                              let dataType8 = typeof data8;
                              let coerced8 = undefined;
                              if (!(coerced8 !== undefined)) {
                                if (
                                  dataType8 == "number" ||
                                  dataType8 == "boolean"
                                ) {
                                  coerced8 = "" + data8;
                                } else if (data8 === null) {
                                  coerced8 = "";
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath +
                                        "/SECURE_CERTIFICATE_PATH",
                                      schemaPath:
                                        "#/properties/SECURE_CERTIFICATE_PATH/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced8 !== undefined) {
                                data8 = coerced8;
                                if (data !== undefined) {
                                  data["SECURE_CERTIFICATE_PATH"] = coerced8;
                                }
                              }
                            }
                            var valid0 = _errs19 === errors;
                          } else {
                            var valid0 = true;
                          }
                          if (valid0) {
                            if (data.BACKEND_PORT !== undefined) {
                              let data9 = data.BACKEND_PORT;
                              const _errs21 = errors;
                              if (
                                !(typeof data9 == "number" && isFinite(data9))
                              ) {
                                let dataType9 = typeof data9;
                                let coerced9 = undefined;
                                if (!(coerced9 !== undefined)) {
                                  if (
                                    dataType9 == "boolean" ||
                                    data9 === null ||
                                    (dataType9 == "string" &&
                                      data9 &&
                                      data9 == +data9)
                                  ) {
                                    coerced9 = +data9;
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/BACKEND_PORT",
                                        schemaPath:
                                          "#/properties/BACKEND_PORT/type",
                                        keyword: "type",
                                        params: { type: "number" },
                                        message: "must be number",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced9 !== undefined) {
                                  data9 = coerced9;
                                  if (data !== undefined) {
                                    data["BACKEND_PORT"] = coerced9;
                                  }
                                }
                              }
                              var valid0 = _errs21 === errors;
                            } else {
                              var valid0 = true;
                            }
                            if (valid0) {
                              if (data.PUBLIC_PATH !== undefined) {
                                let data10 = data.PUBLIC_PATH;
                                const _errs23 = errors;
                                if (typeof data10 !== "string") {
                                  let dataType10 = typeof data10;
                                  let coerced10 = undefined;
                                  if (!(coerced10 !== undefined)) {
                                    if (
                                      dataType10 == "number" ||
                                      dataType10 == "boolean"
                                    ) {
                                      coerced10 = "" + data10;
                                    } else if (data10 === null) {
                                      coerced10 = "";
                                    } else {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/PUBLIC_PATH",
                                          schemaPath:
                                            "#/properties/PUBLIC_PATH/type",
                                          keyword: "type",
                                          params: { type: "string" },
                                          message: "must be string",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                  if (coerced10 !== undefined) {
                                    data10 = coerced10;
                                    if (data !== undefined) {
                                      data["PUBLIC_PATH"] = coerced10;
                                    }
                                  }
                                }
                                var valid0 = _errs23 === errors;
                              } else {
                                var valid0 = true;
                              }
                              if (valid0) {
                                if (data.STORAGE_ORIGIN !== undefined) {
                                  let data11 = data.STORAGE_ORIGIN;
                                  const _errs25 = errors;
                                  if (typeof data11 !== "string") {
                                    let dataType11 = typeof data11;
                                    let coerced11 = undefined;
                                    if (!(coerced11 !== undefined)) {
                                      if (
                                        dataType11 == "number" ||
                                        dataType11 == "boolean"
                                      ) {
                                        coerced11 = "" + data11;
                                      } else if (data11 === null) {
                                        coerced11 = "";
                                      } else {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/STORAGE_ORIGIN",
                                            schemaPath:
                                              "#/properties/STORAGE_ORIGIN/type",
                                            keyword: "type",
                                            params: { type: "string" },
                                            message: "must be string",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                    if (coerced11 !== undefined) {
                                      data11 = coerced11;
                                      if (data !== undefined) {
                                        data["STORAGE_ORIGIN"] = coerced11;
                                      }
                                    }
                                  }
                                  var valid0 = _errs25 === errors;
                                } else {
                                  var valid0 = true;
                                }
                                if (valid0) {
                                  if (data.LOCAL_STORAGE_FOLDER !== undefined) {
                                    let data12 = data.LOCAL_STORAGE_FOLDER;
                                    const _errs27 = errors;
                                    if (typeof data12 !== "string") {
                                      let dataType12 = typeof data12;
                                      let coerced12 = undefined;
                                      if (!(coerced12 !== undefined)) {
                                        if (
                                          dataType12 == "number" ||
                                          dataType12 == "boolean"
                                        ) {
                                          coerced12 = "" + data12;
                                        } else if (data12 === null) {
                                          coerced12 = "";
                                        } else {
                                          validate20.errors = [
                                            {
                                              instancePath:
                                                instancePath +
                                                "/LOCAL_STORAGE_FOLDER",
                                              schemaPath:
                                                "#/properties/LOCAL_STORAGE_FOLDER/type",
                                              keyword: "type",
                                              params: { type: "string" },
                                              message: "must be string",
                                            },
                                          ];
                                          return false;
                                        }
                                      }
                                      if (coerced12 !== undefined) {
                                        data12 = coerced12;
                                        if (data !== undefined) {
                                          data["LOCAL_STORAGE_FOLDER"] =
                                            coerced12;
                                        }
                                      }
                                    }
                                    var valid0 = _errs27 === errors;
                                  } else {
                                    var valid0 = true;
                                  }
                                  if (valid0) {
                                    if (
                                      data.PUBLIC_STORAGE_FOLDER !== undefined
                                    ) {
                                      let data13 = data.PUBLIC_STORAGE_FOLDER;
                                      const _errs29 = errors;
                                      if (typeof data13 !== "string") {
                                        let dataType13 = typeof data13;
                                        let coerced13 = undefined;
                                        if (!(coerced13 !== undefined)) {
                                          if (
                                            dataType13 == "number" ||
                                            dataType13 == "boolean"
                                          ) {
                                            coerced13 = "" + data13;
                                          } else if (data13 === null) {
                                            coerced13 = "";
                                          } else {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/PUBLIC_STORAGE_FOLDER",
                                                schemaPath:
                                                  "#/properties/PUBLIC_STORAGE_FOLDER/type",
                                                keyword: "type",
                                                params: { type: "string" },
                                                message: "must be string",
                                              },
                                            ];
                                            return false;
                                          }
                                        }
                                        if (coerced13 !== undefined) {
                                          data13 = coerced13;
                                          if (data !== undefined) {
                                            data["PUBLIC_STORAGE_FOLDER"] =
                                              coerced13;
                                          }
                                        }
                                      }
                                      var valid0 = _errs29 === errors;
                                    } else {
                                      var valid0 = true;
                                    }
                                    if (valid0) {
                                      if (data.TEMPORARY_FOLDER !== undefined) {
                                        let data14 = data.TEMPORARY_FOLDER;
                                        const _errs31 = errors;
                                        if (typeof data14 !== "string") {
                                          let dataType14 = typeof data14;
                                          let coerced14 = undefined;
                                          if (!(coerced14 !== undefined)) {
                                            if (
                                              dataType14 == "number" ||
                                              dataType14 == "boolean"
                                            ) {
                                              coerced14 = "" + data14;
                                            } else if (data14 === null) {
                                              coerced14 = "";
                                            } else {
                                              validate20.errors = [
                                                {
                                                  instancePath:
                                                    instancePath +
                                                    "/TEMPORARY_FOLDER",
                                                  schemaPath:
                                                    "#/properties/TEMPORARY_FOLDER/type",
                                                  keyword: "type",
                                                  params: { type: "string" },
                                                  message: "must be string",
                                                },
                                              ];
                                              return false;
                                            }
                                          }
                                          if (coerced14 !== undefined) {
                                            data14 = coerced14;
                                            if (data !== undefined) {
                                              data["TEMPORARY_FOLDER"] =
                                                coerced14;
                                            }
                                          }
                                        }
                                        var valid0 = _errs31 === errors;
                                      } else {
                                        var valid0 = true;
                                      }
                                      if (valid0) {
                                        if (data.BACKEND_PATH !== undefined) {
                                          let data15 = data.BACKEND_PATH;
                                          const _errs33 = errors;
                                          if (typeof data15 !== "string") {
                                            let dataType15 = typeof data15;
                                            let coerced15 = undefined;
                                            if (!(coerced15 !== undefined)) {
                                              if (
                                                dataType15 == "number" ||
                                                dataType15 == "boolean"
                                              ) {
                                                coerced15 = "" + data15;
                                              } else if (data15 === null) {
                                                coerced15 = "";
                                              } else {
                                                validate20.errors = [
                                                  {
                                                    instancePath:
                                                      instancePath +
                                                      "/BACKEND_PATH",
                                                    schemaPath:
                                                      "#/properties/BACKEND_PATH/type",
                                                    keyword: "type",
                                                    params: { type: "string" },
                                                    message: "must be string",
                                                  },
                                                ];
                                                return false;
                                              }
                                            }
                                            if (coerced15 !== undefined) {
                                              data15 = coerced15;
                                              if (data !== undefined) {
                                                data["BACKEND_PATH"] =
                                                  coerced15;
                                              }
                                            }
                                          }
                                          var valid0 = _errs33 === errors;
                                        } else {
                                          var valid0 = true;
                                        }
                                        if (valid0) {
                                          if (data.SCHEMA_PATH !== undefined) {
                                            let data16 = data.SCHEMA_PATH;
                                            const _errs35 = errors;
                                            if (typeof data16 !== "string") {
                                              let dataType16 = typeof data16;
                                              let coerced16 = undefined;
                                              if (!(coerced16 !== undefined)) {
                                                if (
                                                  dataType16 == "number" ||
                                                  dataType16 == "boolean"
                                                ) {
                                                  coerced16 = "" + data16;
                                                } else if (data16 === null) {
                                                  coerced16 = "";
                                                } else {
                                                  validate20.errors = [
                                                    {
                                                      instancePath:
                                                        instancePath +
                                                        "/SCHEMA_PATH",
                                                      schemaPath:
                                                        "#/properties/SCHEMA_PATH/type",
                                                      keyword: "type",
                                                      params: {
                                                        type: "string",
                                                      },
                                                      message: "must be string",
                                                    },
                                                  ];
                                                  return false;
                                                }
                                              }
                                              if (coerced16 !== undefined) {
                                                data16 = coerced16;
                                                if (data !== undefined) {
                                                  data["SCHEMA_PATH"] =
                                                    coerced16;
                                                }
                                              }
                                            }
                                            var valid0 = _errs35 === errors;
                                          } else {
                                            var valid0 = true;
                                          }
                                          if (valid0) {
                                            if (
                                              data.DATABASE_FOLDER !== undefined
                                            ) {
                                              let data17 = data.DATABASE_FOLDER;
                                              const _errs37 = errors;
                                              if (typeof data17 !== "string") {
                                                let dataType17 = typeof data17;
                                                let coerced17 = undefined;
                                                if (
                                                  !(coerced17 !== undefined)
                                                ) {
                                                  if (
                                                    dataType17 == "number" ||
                                                    dataType17 == "boolean"
                                                  ) {
                                                    coerced17 = "" + data17;
                                                  } else if (data17 === null) {
                                                    coerced17 = "";
                                                  } else {
                                                    validate20.errors = [
                                                      {
                                                        instancePath:
                                                          instancePath +
                                                          "/DATABASE_FOLDER",
                                                        schemaPath:
                                                          "#/properties/DATABASE_FOLDER/type",
                                                        keyword: "type",
                                                        params: {
                                                          type: "string",
                                                        },
                                                        message:
                                                          "must be string",
                                                      },
                                                    ];
                                                    return false;
                                                  }
                                                }
                                                if (coerced17 !== undefined) {
                                                  data17 = coerced17;
                                                  if (data !== undefined) {
                                                    data["DATABASE_FOLDER"] =
                                                      coerced17;
                                                  }
                                                }
                                              }
                                              var valid0 = _errs37 === errors;
                                            } else {
                                              var valid0 = true;
                                            }
                                            if (valid0) {
                                              if (
                                                data.PUBLIC_EXPORTS_FOLDER !==
                                                undefined
                                              ) {
                                                let data18 =
                                                  data.PUBLIC_EXPORTS_FOLDER;
                                                const _errs39 = errors;
                                                if (
                                                  typeof data18 !== "string"
                                                ) {
                                                  let dataType18 =
                                                    typeof data18;
                                                  let coerced18 = undefined;
                                                  if (
                                                    !(coerced18 !== undefined)
                                                  ) {
                                                    if (
                                                      dataType18 == "number" ||
                                                      dataType18 == "boolean"
                                                    ) {
                                                      coerced18 = "" + data18;
                                                    } else if (
                                                      data18 === null
                                                    ) {
                                                      coerced18 = "";
                                                    } else {
                                                      validate20.errors = [
                                                        {
                                                          instancePath:
                                                            instancePath +
                                                            "/PUBLIC_EXPORTS_FOLDER",
                                                          schemaPath:
                                                            "#/properties/PUBLIC_EXPORTS_FOLDER/type",
                                                          keyword: "type",
                                                          params: {
                                                            type: "string",
                                                          },
                                                          message:
                                                            "must be string",
                                                        },
                                                      ];
                                                      return false;
                                                    }
                                                  }
                                                  if (coerced18 !== undefined) {
                                                    data18 = coerced18;
                                                    if (data !== undefined) {
                                                      data[
                                                        "PUBLIC_EXPORTS_FOLDER"
                                                      ] = coerced18;
                                                    }
                                                  }
                                                }
                                                var valid0 = _errs39 === errors;
                                              } else {
                                                var valid0 = true;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
