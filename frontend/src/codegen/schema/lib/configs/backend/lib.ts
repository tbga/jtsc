/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { backendConfigurationSchema } from "./schema";
import { validate } from "./validate";
import { IBackendConfiguration } from "./types";
export const validateBackendConfiguration =
  createValidator<IBackendConfiguration>(
    validate as ValidateFunction<IBackendConfiguration>,
    backendConfigurationSchema as unknown as IJSONSchema
  );
