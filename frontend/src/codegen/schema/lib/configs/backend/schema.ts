/*
 * This file was generated automatically, do not edit it by hand.
 */
export const backendConfigurationSchema = {
  $id: "https://jtsc-schemas.org/configs/backend.schema.json",
  title: "BackendConfiguration",
  description: "Backend configuration options.",
  type: "object",
  additionalProperties: false,
  required: [
    "DATABASE_URL",
    "STORAGE_ORIGIN",
    "BACKEND_PORT",
    "SECRET_KEY",
    "IS_PUBLIC",
    "IS_INVITE_ONLY",
  ],
  properties: {
    SECRET_KEY: {
      description: "A key to sign cookies.",
      type: "string",
      minLength: 32,
    },
    ADMIN_INVITE: {
      $ref: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
    },
    IS_PUBLIC: {
      description:
        "Visibility of the public API. If set to `false`, any access outside of register/login endpoints requires an account.",
      type: "boolean",
      default: false,
    },
    IS_INVITE_ONLY: {
      description:
        "Controls invitation code is required during registration or not.",
      type: "boolean",
      default: true,
    },
    DATABASE_URL: {
      description: "Database connection url.",
      type: "string",
      examples: [
        "postgres://<username>:<password>@<host>:<port>/<database-name>",
      ],
    },
    IS_DATABASE_LOGGING_ENABLED: {
      description: "Database query logging.",
      type: "boolean",
      default: false,
    },
    IS_HTTPS_ENABLED: {
      description: "Run the server in https mode.",
      type: "boolean",
      default: false,
    },
    SECURE_KEY_PATH: {
      description: "The path to https sertificate.",
      type: "string",
      default: "./configs/development/key.pem",
    },
    SECURE_CERTIFICATE_PATH: {
      description: "The path to https sertificate.",
      type: "string",
      default: "./configs/development/cert.pem",
    },
    BACKEND_PORT: {
      description: "Port of the server.",
      type: "number",
      default: 3499,
    },
    PUBLIC_PATH: {
      description: "Local folder for the frontend files.",
      type: "string",
      examples: ["/absolute/path/to/public/folder"],
    },
    STORAGE_ORIGIN: {
      description: "Origin of the file storage.",
      type: "string",
      examples: ["http://localhost:3499/storage"],
    },
    LOCAL_STORAGE_FOLDER: {
      description: "The path of local storage on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/storage/files",
      examples: ["/absolute/path/to/storage/folder"],
    },
    PUBLIC_STORAGE_FOLDER: {
      description: "The path of public storage on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/storage/public",
      examples: ["/absolute/path/to/storage/folder"],
    },
    TEMPORARY_FOLDER: {
      description: "Local folder for the temporary files.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/temporary",
      examples: ["/absolute/path/to/temp/folder"],
    },
    BACKEND_PATH: {
      description: "Local folder of the backend.",
      type: "string",
      examples: ["/absolute/path/to/backend/folder"],
    },
    SCHEMA_PATH: {
      description: "Local folder of schemas.",
      type: "string",
      examples: ["/absolute/path/to/schema/folder"],
    },
    DATABASE_FOLDER: {
      description: "Local folder of the database.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/database",
      examples: ["/absolute/path/to/database/folder"],
    },
    PUBLIC_EXPORTS_FOLDER: {
      description: "The path of public exports folder on file system.",
      type: "string",
      default: "${SYSTEM_FOLDER}/${PROJECT_ENV}/public-exports",
      examples: ["/absolute/path/to/public-exports/folder"],
    },
  },
} as const;
