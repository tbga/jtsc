/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { environmentConfigurationSchema } from "./schema";
import { validate } from "./validate";
import { IEnvironmentConfiguration } from "./types";
export const validateEnvironmentConfiguration =
  createValidator<IEnvironmentConfiguration>(
    validate as ValidateFunction<IEnvironmentConfiguration>,
    environmentConfigurationSchema as unknown as IJSONSchema
  );
