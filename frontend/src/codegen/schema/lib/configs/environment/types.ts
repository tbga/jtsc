/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Environment configuration options.
 */
export interface IEnvironmentConfiguration {
  /**
   * Node.js environment variable.
   */
  NODE_ENV: "development" | "production";
}
