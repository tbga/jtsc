/*
 * This file was generated automatically, do not edit it by hand.
 */
export const environmentConfigurationSchema = {
  $id: "https://jtsc-schemas.org/configs/environment.schema.json",
  title: "EnvironmentConfiguration",
  description: "Environment configuration options.",
  type: "object",
  additionalProperties: false,
  required: ["NODE_ENV"],
  properties: {
    NODE_ENV: {
      description: "Node.js environment variable.",
      type: "string",
      enum: ["development", "production"],
    },
  },
} as const;
