/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { projectEnvironmentSchema } from "./schema";
import { validate } from "./validate";
import { IProjectEnvironment } from "./types";
export const validateProjectEnvironment = createValidator<IProjectEnvironment>(
  validate as ValidateFunction<IProjectEnvironment>,
  projectEnvironmentSchema as unknown as IJSONSchema
);
