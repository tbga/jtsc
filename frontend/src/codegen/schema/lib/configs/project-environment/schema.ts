/*
 * This file was generated automatically, do not edit it by hand.
 */
export const projectEnvironmentSchema = {
  $id: "https://jtsc-schemas.org/configs/project-environment.schema.json",
  title: "ProjectEnvironment",
  description: "Project environment states.",
  type: "string",
  enum: ["development", "production"],
} as const;
