/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Project environment states.
 */
export type IProjectEnvironment = "development" | "production";
