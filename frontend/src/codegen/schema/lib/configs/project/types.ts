/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Project configuration options.
 */
export interface IProjectConfiguration {
  /**
   * A path to the project folder
   */
  PROJECT_PATH: string;
  /**
   * A path to build output folder
   */
  OUTPUT_PATH: string;
}
