/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { projectConfigurationSchema } from "./schema";
import { validate } from "./validate";
import { IProjectConfiguration } from "./types";
export const validateProjectConfiguration =
  createValidator<IProjectConfiguration>(
    validate as ValidateFunction<IProjectConfiguration>,
    projectConfigurationSchema as unknown as IJSONSchema
  );
