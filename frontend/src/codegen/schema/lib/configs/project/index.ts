/*
 * This file was generated automatically, do not edit it by hand.
 */
export { projectConfigurationSchema } from "./schema";
export { validateProjectConfiguration } from "./lib";
export type { IProjectConfiguration } from "./types";
