/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { entrySchema } from "./schema";
import { validate } from "./validate";
import { IEntry } from "./types";
export const validateEntry = createValidator<IEntry>(
  validate as ValidateFunction<IEntry>,
  entrySchema as unknown as IJSONSchema
);
