/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/local-file-system/entry.schema.json",
  title: "Entry",
  description: "An entry on the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["type", "name"],
  properties: {
    type: {
      description: "The type of the entry.",
      type: "string",
      enum: ["file", "folder"],
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
    size: { description: "Size of the file.", type: "number" },
    entries: {
      description: "The amount of entries in the folder.",
      type: "integer",
      minimum: 0,
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/local-file-system/entry.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.type === undefined && (missing0 = "type")) ||
        (data.name === undefined && (missing0 = "name"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "type" ||
              key0 === "name" ||
              key0 === "size" ||
              key0 === "entries"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.type !== undefined) {
            let data0 = data.type;
            const _errs2 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/type",
                      schemaPath: "#/properties/type/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["type"] = coerced0;
                }
              }
            }
            if (!(data0 === "file" || data0 === "folder")) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/type",
                  schemaPath: "#/properties/type/enum",
                  keyword: "enum",
                  params: { allowedValues: schema31.properties.type.enum },
                  message: "must be equal to one of the allowed values",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs4 = errors;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.size !== undefined) {
                let data2 = data.size;
                const _errs7 = errors;
                if (!(typeof data2 == "number" && isFinite(data2))) {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (
                      dataType2 == "boolean" ||
                      data2 === null ||
                      (dataType2 == "string" && data2 && data2 == +data2)
                    ) {
                      coerced2 = +data2;
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/size",
                          schemaPath: "#/properties/size/type",
                          keyword: "type",
                          params: { type: "number" },
                          message: "must be number",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["size"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.entries !== undefined) {
                  let data3 = data.entries;
                  const _errs9 = errors;
                  if (
                    !(
                      typeof data3 == "number" &&
                      !(data3 % 1) &&
                      !isNaN(data3) &&
                      isFinite(data3)
                    )
                  ) {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (
                        dataType3 === "boolean" ||
                        data3 === null ||
                        (dataType3 === "string" &&
                          data3 &&
                          data3 == +data3 &&
                          !(data3 % 1))
                      ) {
                        coerced3 = +data3;
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/entries",
                            schemaPath: "#/properties/entries/type",
                            keyword: "type",
                            params: { type: "integer" },
                            message: "must be integer",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["entries"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs9) {
                    if (typeof data3 == "number" && isFinite(data3)) {
                      if (data3 < 0 || isNaN(data3)) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/entries",
                            schemaPath: "#/properties/entries/minimum",
                            keyword: "minimum",
                            params: { comparison: ">=", limit: 0 },
                            message: "must be >= 0",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                  var valid0 = _errs9 === errors;
                } else {
                  var valid0 = true;
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
