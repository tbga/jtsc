/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";

/**
 * An entry on the local file system.
 */
export interface IEntry {
  /**
   * The type of the entry.
   */
  type: "file" | "folder";
  name: INonEmptyString;
  /**
   * Size of the file.
   */
  size?: number;
  /**
   * The amount of entries in the folder.
   */
  entries?: number;
}
