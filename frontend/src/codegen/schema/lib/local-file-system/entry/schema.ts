/*
 * This file was generated automatically, do not edit it by hand.
 */
export const entrySchema = {
  $id: "https://jtsc-schemas.org/local-file-system/entry.schema.json",
  title: "Entry",
  description: "An entry on the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["type", "name"],
  properties: {
    type: {
      description: "The type of the entry.",
      type: "string",
      enum: ["file", "folder"],
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    size: {
      description: "Size of the file.",
      type: "number",
    },
    entries: {
      description: "The amount of entries in the folder.",
      type: "integer",
      minimum: 0,
    },
  },
} as const;
