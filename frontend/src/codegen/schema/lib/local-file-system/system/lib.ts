/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { fileSystemSchema } from "./schema";
import { validate } from "./validate";
import { IFileSystem } from "./types";
export const validateFileSystem = createValidator<IFileSystem>(
  validate as ValidateFunction<IFileSystem>,
  fileSystemSchema as unknown as IJSONSchema
);
