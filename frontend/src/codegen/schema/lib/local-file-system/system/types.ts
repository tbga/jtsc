/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPlatform } from "#codegen/schema/lib/types/nodejs/platform";
import { type IPath } from "#codegen/schema/lib/local-file-system/path";

/**
 * Information about the local file system.
 */
export interface IFileSystem {
  platform: IPlatform;
  home_dir: IPath;
}
