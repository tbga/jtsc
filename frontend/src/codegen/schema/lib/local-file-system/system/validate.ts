/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/local-file-system/system.schema.json",
  title: "FileSystem",
  description: "Information about the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["platform", "home_dir"],
  properties: {
    platform: {
      $ref: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
    },
    home_dir: {
      $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
  title: "Platform",
  description: "Information about the local file system.",
  type: "string",
  enum: [
    "aix",
    "android",
    "darwin",
    "freebsd",
    "haiku",
    "linux",
    "openbsd",
    "sunos",
    "win32",
    "cygwin",
    "netbsd",
  ],
};
const schema33 = {
  $id: "https://jtsc-schemas.org/local-file-system/path.schema.json",
  title: "Path",
  description: "A `URL` file path.",
  type: "string",
  format: "uri",
  minLength: 1,
};
const formats0 = require("ajv-formats/dist/formats").fullFormats.uri;
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/local-file-system/system.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.platform === undefined && (missing0 = "platform")) ||
        (data.home_dir === undefined && (missing0 = "home_dir"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "platform" || key0 === "home_dir")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.platform !== undefined) {
            let data0 = data.platform;
            const _errs2 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/platform",
                      schemaPath:
                        "https://jtsc-schemas.org/types/nodejs/platform.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["platform"] = coerced0;
                }
              }
            }
            if (
              !(
                data0 === "aix" ||
                data0 === "android" ||
                data0 === "darwin" ||
                data0 === "freebsd" ||
                data0 === "haiku" ||
                data0 === "linux" ||
                data0 === "openbsd" ||
                data0 === "sunos" ||
                data0 === "win32" ||
                data0 === "cygwin" ||
                data0 === "netbsd"
              )
            ) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/platform",
                  schemaPath:
                    "https://jtsc-schemas.org/types/nodejs/platform.schema.json/enum",
                  keyword: "enum",
                  params: { allowedValues: schema32.enum },
                  message: "must be equal to one of the allowed values",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.home_dir !== undefined) {
              let data1 = data.home_dir;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/home_dir",
                        schemaPath:
                          "https://jtsc-schemas.org/local-file-system/path.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["home_dir"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func1(data1) < 1) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/home_dir",
                          schemaPath:
                            "https://jtsc-schemas.org/local-file-system/path.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (!formats0(data1)) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/home_dir",
                            schemaPath:
                              "https://jtsc-schemas.org/local-file-system/path.schema.json/format",
                            keyword: "format",
                            params: { format: "uri" },
                            message: 'must match format "' + "uri" + '"',
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
