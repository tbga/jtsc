/*
 * This file was generated automatically, do not edit it by hand.
 */
export { folderSchema } from "./schema";
export { validateFolder } from "./lib";
export type { IFolder } from "./types";
