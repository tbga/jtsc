/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { folderSchema } from "./schema";
import { validate } from "./validate";
import { IFolder } from "./types";
export const validateFolder = createValidator<IFolder>(
  validate as ValidateFunction<IFolder>,
  folderSchema as unknown as IJSONSchema
);
