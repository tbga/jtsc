/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IEntry } from "#codegen/schema/lib/local-file-system/entry";

/**
 * A folder on the local file system.
 */
export interface IFolder {
  name: INonEmptyString;
  /**
   * Entries within the folder
   */
  entries: IEntry[];
}
