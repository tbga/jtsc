/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { pathSchema } from "./schema";
import { validate } from "./validate";
import { IPath } from "./types";
export const validatePath = createValidator<IPath>(
  validate as ValidateFunction<IPath>,
  pathSchema as unknown as IJSONSchema
);
