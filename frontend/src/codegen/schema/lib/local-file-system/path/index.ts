/*
 * This file was generated automatically, do not edit it by hand.
 */
export { pathSchema } from "./schema";
export { validatePath } from "./lib";
export type { IPath } from "./types";
