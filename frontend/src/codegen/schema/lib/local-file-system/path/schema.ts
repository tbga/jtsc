/*
 * This file was generated automatically, do not edit it by hand.
 */
export const pathSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/path.schema.json",
  title: "Path",
  description: "A `URL` file path.",
  type: "string",
  format: "uri",
  minLength: 1,
} as const;
