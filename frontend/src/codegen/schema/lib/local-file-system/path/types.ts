/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * A `URL` file path.
 */
export type IPath = string;
