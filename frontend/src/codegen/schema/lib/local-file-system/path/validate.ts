/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/local-file-system/path.schema.json",
  title: "Path",
  description: "A `URL` file path.",
  type: "string",
  format: "uri",
  minLength: 1,
};
const formats0 = require("ajv-formats/dist/formats").fullFormats.uri;
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/local-file-system/path.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (typeof data !== "string") {
    let dataType0 = typeof data;
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (dataType0 == "number" || dataType0 == "boolean") {
        coerced0 = "" + data;
      } else if (data === null) {
        coerced0 = "";
      } else {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/type",
            keyword: "type",
            params: { type: "string" },
            message: "must be string",
          },
        ];
        return false;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  if (errors === 0) {
    if (errors === 0) {
      if (typeof data === "string") {
        if (func1(data) < 1) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/minLength",
              keyword: "minLength",
              params: { limit: 1 },
              message: "must NOT have fewer than 1 characters",
            },
          ];
          return false;
        } else {
          if (!formats0(data)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/format",
                keyword: "format",
                params: { format: "uri" },
                message: 'must match format "' + "uri" + '"',
              },
            ];
            return false;
          }
        }
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
