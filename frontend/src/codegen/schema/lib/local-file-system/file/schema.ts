/*
 * This file was generated automatically, do not edit it by hand.
 */
export const fileSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/file.schema.json",
  title: "File",
  description: "A file on the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["name", "size"],
  properties: {
    name: {
      description: "Name of the file.",
      type: "string",
      minLength: 1,
    },
    size: {
      description: "Size of the file.",
      type: "string",
      minLength: 1,
    },
  },
} as const;
