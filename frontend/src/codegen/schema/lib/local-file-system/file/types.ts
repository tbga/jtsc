/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * A file on the local file system.
 */
export interface IFile {
  /**
   * Name of the file.
   */
  name: string;
  /**
   * Size of the file.
   */
  size: string;
}
