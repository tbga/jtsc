/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { fileSchema } from "./schema";
import { validate } from "./validate";
import { IFile } from "./types";
export const validateFile = createValidator<IFile>(
  validate as ValidateFunction<IFile>,
  fileSchema as unknown as IJSONSchema
);
