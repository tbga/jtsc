/*
 * This file was generated automatically, do not edit it by hand.
 */
export { entitySchema } from "./schema";
export { validateEntity } from "./lib";
export type { IEntity } from "./types";
