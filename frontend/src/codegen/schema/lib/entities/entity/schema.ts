/*
 * This file was generated automatically, do not edit it by hand.
 */
export const entitySchema = {
  $id: "https://jtsc-schemas.org/entities/entity.schema.json",
  title: "Entity",
  description: "A baseline entity.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
  },
} as const;
