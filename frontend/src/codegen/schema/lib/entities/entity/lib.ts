/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { entitySchema } from "./schema";
import { validate } from "./validate";
import { IEntity } from "./types";
export const validateEntity = createValidator<IEntity>(
  validate as ValidateFunction<IEntity>,
  entitySchema as unknown as IJSONSchema
);
