/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IArtistInit } from "#codegen/schema/lib/entities/artist/init";
import { type IReleaseInit } from "#codegen/schema/lib/entities/release/init";

/**
 * Initializer for the post.
 */
export interface IPostInit {
  /**
   * Artists of the post.
   */
  artists?: {
    /**
     * IDs of existing artists.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
    /**
     * Initializers for new artists.
     *
     * @minItems 1
     */
    inits?: [IArtistInit, ...IArtistInit[]];
  };
  /**
   * Releases of the post.
   */
  releases?: {
    /**
     * IDs of existing releases.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
    /**
     * Initializers for new releases.
     *
     * @minItems 1
     */
    inits?: [IReleaseInit, ...IReleaseInit[]];
  };
}
