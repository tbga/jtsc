/*
 * This file was generated automatically, do not edit it by hand.
 */
export { postInitSchema } from "./schema";
export { validatePostInit } from "./lib";
export type { IPostInit } from "./types";
