/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { postInitSchema } from "./schema";
import { validate } from "./validate";
import { IPostInit } from "./types";
export const validatePostInit = createValidator<IPostInit>(
  validate as ValidateFunction<IPostInit>,
  postInitSchema as unknown as IJSONSchema
);
