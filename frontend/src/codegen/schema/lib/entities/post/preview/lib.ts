/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { postPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IPostPreview } from "./types";
export const validatePostPreview = createValidator<IPostPreview>(
  validate as ValidateFunction<IPostPreview>,
  postPreviewSchema as unknown as IJSONSchema
);
