/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { postSchema } from "./schema";
import { validate } from "./validate";
import { IPost } from "./types";
export const validatePost = createValidator<IPost>(
  validate as ValidateFunction<IPost>,
  postSchema as unknown as IJSONSchema
);
