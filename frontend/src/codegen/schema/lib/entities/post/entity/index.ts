/*
 * This file was generated automatically, do not edit it by hand.
 */
export { postSchema } from "./schema";
export { validatePost } from "./lib";
export type { IPost } from "./types";
