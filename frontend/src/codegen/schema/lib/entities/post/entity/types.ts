/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";

/**
 * The post details.
 */
export interface IPost {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  name?: ITitle;
  title?: ITitle;
}
