/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/operation/entity.schema.json",
  title: "Operation",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "created_by",
    "type",
    "status",
    "input_data",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/operation/status.schema.json",
    },
    type: {
      $ref: "https://jtsc-schemas.org/entities/operation/type.schema.json",
    },
    input_data: { $ref: "https://jtsc-schemas.org/types/json/any.schema.json" },
    result_data: {
      $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
    },
    errors: {
      description: "An array of error messages, if any.",
      type: "array",
      items: { type: "string" },
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema36 = {
  $id: "https://jtsc-schemas.org/entities/operation/status.schema.json",
  title: "OperationStatus",
  description: "The status of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    { const: "pending", description: "The operation is awaiting in queue." },
    {
      const: "in-progress",
      description: "The operation is currently being ran.",
    },
    {
      const: "finished",
      description: "The operation has been successfully finished.",
    },
    { const: "failed", description: "The operation failed with errors." },
  ],
};
const schema37 = {
  $id: "https://jtsc-schemas.org/entities/operation/type.schema.json",
  title: "OperationType",
  description: "The type of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    { const: "local_upload", description: "Upload of the files." },
    { const: "releases_create", description: "Creation of releases." },
    { const: "profiles_create", description: "Creation of profiles." },
    { const: "public_exports_finish", description: "Finish public exports." },
    { const: "public_imports_create", description: "Create public imports." },
    { const: "public_imports_consume", description: "Consume public imports." },
  ],
};
const schema38 = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    { type: "null" },
    { type: "boolean" },
    { type: "number" },
    { type: "string" },
    { type: "object" },
    { type: "array" },
    { type: "array", minItems: 1 },
  ],
};
const schema40 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/operation/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_at === undefined && (missing0 = "created_at")) ||
        (data.updated_at === undefined && (missing0 = "updated_at")) ||
        (data.created_by === undefined && (missing0 = "created_by")) ||
        (data.type === undefined && (missing0 = "type")) ||
        (data.status === undefined && (missing0 = "status")) ||
        (data.input_data === undefined && (missing0 = "input_data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!func1.call(schema31.properties, key0)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_at !== undefined) {
              let data1 = data.created_at;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_at",
                        schemaPath:
                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_at"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func2(data1) > 29) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/created_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 29 },
                          message: "must NOT have more than 29 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func2(data1) < 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 29 },
                            message: "must NOT have fewer than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (!formats0.validate(data1)) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/created_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                              keyword: "format",
                              params: { format: "date-time" },
                              message:
                                'must match format "' + "date-time" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.updated_at !== undefined) {
                let data2 = data.updated_at;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/updated_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["updated_at"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (errors === _errs9) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/updated_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 29 },
                            message: "must NOT have more than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 29) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/updated_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 29 },
                              message: "must NOT have fewer than 29 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.validate(data2)) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/updated_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                keyword: "format",
                                params: { format: "date-time" },
                                message:
                                  'must match format "' + "date-time" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.created_by !== undefined) {
                  let data3 = data.created_by;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_by",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["created_by"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func2(data3) > 19) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_by",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 19 },
                            message: "must NOT have more than 19 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data3) < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/created_by",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.status !== undefined) {
                    let data4 = data.status;
                    const _errs14 = errors;
                    const _errs15 = errors;
                    const _errs17 = errors;
                    let valid6 = false;
                    const _errs18 = errors;
                    if ("pending" !== data4) {
                      const err0 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/operation/status.schema.json/anyOf/0/const",
                        keyword: "const",
                        params: { allowedValue: "pending" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err0];
                      } else {
                        vErrors.push(err0);
                      }
                      errors++;
                    }
                    var _valid0 = _errs18 === errors;
                    valid6 = valid6 || _valid0;
                    const _errs19 = errors;
                    if ("in-progress" !== data4) {
                      const err1 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/operation/status.schema.json/anyOf/1/const",
                        keyword: "const",
                        params: { allowedValue: "in-progress" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err1];
                      } else {
                        vErrors.push(err1);
                      }
                      errors++;
                    }
                    var _valid0 = _errs19 === errors;
                    valid6 = valid6 || _valid0;
                    const _errs20 = errors;
                    if ("finished" !== data4) {
                      const err2 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/operation/status.schema.json/anyOf/2/const",
                        keyword: "const",
                        params: { allowedValue: "finished" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err2];
                      } else {
                        vErrors.push(err2);
                      }
                      errors++;
                    }
                    var _valid0 = _errs20 === errors;
                    valid6 = valid6 || _valid0;
                    const _errs21 = errors;
                    if ("failed" !== data4) {
                      const err3 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/operation/status.schema.json/anyOf/3/const",
                        keyword: "const",
                        params: { allowedValue: "failed" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err3];
                      } else {
                        vErrors.push(err3);
                      }
                      errors++;
                    }
                    var _valid0 = _errs21 === errors;
                    valid6 = valid6 || _valid0;
                    if (!valid6) {
                      const err4 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/operation/status.schema.json/anyOf",
                        keyword: "anyOf",
                        params: {},
                        message: "must match a schema in anyOf",
                      };
                      if (vErrors === null) {
                        vErrors = [err4];
                      } else {
                        vErrors.push(err4);
                      }
                      errors++;
                      validate20.errors = vErrors;
                      return false;
                    } else {
                      errors = _errs17;
                      if (vErrors !== null) {
                        if (_errs17) {
                          vErrors.length = _errs17;
                        } else {
                          vErrors = null;
                        }
                      }
                    }
                    if (errors === _errs15) {
                      if (
                        data4 &&
                        typeof data4 == "object" &&
                        !Array.isArray(data4)
                      ) {
                        for (const key1 in data4) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/status",
                              schemaPath:
                                "https://jtsc-schemas.org/entities/operation/status.schema.json/additionalProperties",
                              keyword: "additionalProperties",
                              params: { additionalProperty: key1 },
                              message: "must NOT have additional properties",
                            },
                          ];
                          return false;
                          break;
                        }
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/status",
                            schemaPath:
                              "https://jtsc-schemas.org/entities/operation/status.schema.json/type",
                            keyword: "type",
                            params: { type: "object" },
                            message: "must be object",
                          },
                        ];
                        return false;
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.type !== undefined) {
                      let data5 = data.type;
                      const _errs23 = errors;
                      const _errs24 = errors;
                      const _errs26 = errors;
                      let valid8 = false;
                      const _errs27 = errors;
                      if ("local_upload" !== data5) {
                        const err5 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/0/const",
                          keyword: "const",
                          params: { allowedValue: "local_upload" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err5];
                        } else {
                          vErrors.push(err5);
                        }
                        errors++;
                      }
                      var _valid1 = _errs27 === errors;
                      valid8 = valid8 || _valid1;
                      const _errs28 = errors;
                      if ("releases_create" !== data5) {
                        const err6 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/1/const",
                          keyword: "const",
                          params: { allowedValue: "releases_create" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err6];
                        } else {
                          vErrors.push(err6);
                        }
                        errors++;
                      }
                      var _valid1 = _errs28 === errors;
                      valid8 = valid8 || _valid1;
                      const _errs29 = errors;
                      if ("profiles_create" !== data5) {
                        const err7 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/2/const",
                          keyword: "const",
                          params: { allowedValue: "profiles_create" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err7];
                        } else {
                          vErrors.push(err7);
                        }
                        errors++;
                      }
                      var _valid1 = _errs29 === errors;
                      valid8 = valid8 || _valid1;
                      const _errs30 = errors;
                      if ("public_exports_finish" !== data5) {
                        const err8 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/3/const",
                          keyword: "const",
                          params: { allowedValue: "public_exports_finish" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err8];
                        } else {
                          vErrors.push(err8);
                        }
                        errors++;
                      }
                      var _valid1 = _errs30 === errors;
                      valid8 = valid8 || _valid1;
                      const _errs31 = errors;
                      if ("public_imports_create" !== data5) {
                        const err9 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/4/const",
                          keyword: "const",
                          params: { allowedValue: "public_imports_create" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err9];
                        } else {
                          vErrors.push(err9);
                        }
                        errors++;
                      }
                      var _valid1 = _errs31 === errors;
                      valid8 = valid8 || _valid1;
                      const _errs32 = errors;
                      if ("public_imports_consume" !== data5) {
                        const err10 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf/5/const",
                          keyword: "const",
                          params: { allowedValue: "public_imports_consume" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err10];
                        } else {
                          vErrors.push(err10);
                        }
                        errors++;
                      }
                      var _valid1 = _errs32 === errors;
                      valid8 = valid8 || _valid1;
                      if (!valid8) {
                        const err11 = {
                          instancePath: instancePath + "/type",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/operation/type.schema.json/anyOf",
                          keyword: "anyOf",
                          params: {},
                          message: "must match a schema in anyOf",
                        };
                        if (vErrors === null) {
                          vErrors = [err11];
                        } else {
                          vErrors.push(err11);
                        }
                        errors++;
                        validate20.errors = vErrors;
                        return false;
                      } else {
                        errors = _errs26;
                        if (vErrors !== null) {
                          if (_errs26) {
                            vErrors.length = _errs26;
                          } else {
                            vErrors = null;
                          }
                        }
                      }
                      if (errors === _errs24) {
                        if (
                          data5 &&
                          typeof data5 == "object" &&
                          !Array.isArray(data5)
                        ) {
                          for (const key2 in data5) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/type",
                                schemaPath:
                                  "https://jtsc-schemas.org/entities/operation/type.schema.json/additionalProperties",
                                keyword: "additionalProperties",
                                params: { additionalProperty: key2 },
                                message: "must NOT have additional properties",
                              },
                            ];
                            return false;
                            break;
                          }
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/type",
                              schemaPath:
                                "https://jtsc-schemas.org/entities/operation/type.schema.json/type",
                              keyword: "type",
                              params: { type: "object" },
                              message: "must be object",
                            },
                          ];
                          return false;
                        }
                      }
                      var valid0 = _errs23 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.input_data !== undefined) {
                        let data6 = data.input_data;
                        const _errs34 = errors;
                        const _errs36 = errors;
                        let valid10 = false;
                        const _errs37 = errors;
                        if (data6 !== null) {
                          let coerced4 = undefined;
                          if (!(coerced4 !== undefined)) {
                            if (
                              data6 === "" ||
                              data6 === 0 ||
                              data6 === false
                            ) {
                              coerced4 = null;
                            } else {
                              const err12 = {
                                instancePath: instancePath + "/input_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/0/type",
                                keyword: "type",
                                params: { type: "null" },
                                message: "must be null",
                              };
                              if (vErrors === null) {
                                vErrors = [err12];
                              } else {
                                vErrors.push(err12);
                              }
                              errors++;
                            }
                          }
                          if (coerced4 !== undefined) {
                            data6 = coerced4;
                            if (data !== undefined) {
                              data["input_data"] = coerced4;
                            }
                          }
                        }
                        var _valid2 = _errs37 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs39 = errors;
                        if (typeof data6 !== "boolean") {
                          let coerced5 = undefined;
                          if (!(coerced5 !== undefined)) {
                            if (
                              data6 === "false" ||
                              data6 === 0 ||
                              data6 === null
                            ) {
                              coerced5 = false;
                            } else if (data6 === "true" || data6 === 1) {
                              coerced5 = true;
                            } else {
                              const err13 = {
                                instancePath: instancePath + "/input_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/1/type",
                                keyword: "type",
                                params: { type: "boolean" },
                                message: "must be boolean",
                              };
                              if (vErrors === null) {
                                vErrors = [err13];
                              } else {
                                vErrors.push(err13);
                              }
                              errors++;
                            }
                          }
                          if (coerced5 !== undefined) {
                            data6 = coerced5;
                            if (data !== undefined) {
                              data["input_data"] = coerced5;
                            }
                          }
                        }
                        var _valid2 = _errs39 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs41 = errors;
                        if (!(typeof data6 == "number" && isFinite(data6))) {
                          let dataType6 = typeof data6;
                          let coerced6 = undefined;
                          if (!(coerced6 !== undefined)) {
                            if (
                              dataType6 == "boolean" ||
                              data6 === null ||
                              (dataType6 == "string" &&
                                data6 &&
                                data6 == +data6)
                            ) {
                              coerced6 = +data6;
                            } else {
                              const err14 = {
                                instancePath: instancePath + "/input_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/2/type",
                                keyword: "type",
                                params: { type: "number" },
                                message: "must be number",
                              };
                              if (vErrors === null) {
                                vErrors = [err14];
                              } else {
                                vErrors.push(err14);
                              }
                              errors++;
                            }
                          }
                          if (coerced6 !== undefined) {
                            data6 = coerced6;
                            if (data !== undefined) {
                              data["input_data"] = coerced6;
                            }
                          }
                        }
                        var _valid2 = _errs41 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs43 = errors;
                        if (typeof data6 !== "string") {
                          let dataType7 = typeof data6;
                          let coerced7 = undefined;
                          if (!(coerced7 !== undefined)) {
                            if (
                              dataType7 == "number" ||
                              dataType7 == "boolean"
                            ) {
                              coerced7 = "" + data6;
                            } else if (data6 === null) {
                              coerced7 = "";
                            } else {
                              const err15 = {
                                instancePath: instancePath + "/input_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/3/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              };
                              if (vErrors === null) {
                                vErrors = [err15];
                              } else {
                                vErrors.push(err15);
                              }
                              errors++;
                            }
                          }
                          if (coerced7 !== undefined) {
                            data6 = coerced7;
                            if (data !== undefined) {
                              data["input_data"] = coerced7;
                            }
                          }
                        }
                        var _valid2 = _errs43 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs45 = errors;
                        if (
                          !(
                            data6 &&
                            typeof data6 == "object" &&
                            !Array.isArray(data6)
                          )
                        ) {
                          const err16 = {
                            instancePath: instancePath + "/input_data",
                            schemaPath:
                              "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/4/type",
                            keyword: "type",
                            params: { type: "object" },
                            message: "must be object",
                          };
                          if (vErrors === null) {
                            vErrors = [err16];
                          } else {
                            vErrors.push(err16);
                          }
                          errors++;
                        }
                        var _valid2 = _errs45 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs47 = errors;
                        if (!Array.isArray(data6)) {
                          const err17 = {
                            instancePath: instancePath + "/input_data",
                            schemaPath:
                              "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/5/type",
                            keyword: "type",
                            params: { type: "array" },
                            message: "must be array",
                          };
                          if (vErrors === null) {
                            vErrors = [err17];
                          } else {
                            vErrors.push(err17);
                          }
                          errors++;
                        }
                        var _valid2 = _errs47 === errors;
                        valid10 = valid10 || _valid2;
                        const _errs49 = errors;
                        if (errors === _errs49) {
                          if (Array.isArray(data6)) {
                            if (data6.length < 1) {
                              const err18 = {
                                instancePath: instancePath + "/input_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/minItems",
                                keyword: "minItems",
                                params: { limit: 1 },
                                message: "must NOT have fewer than 1 items",
                              };
                              if (vErrors === null) {
                                vErrors = [err18];
                              } else {
                                vErrors.push(err18);
                              }
                              errors++;
                            }
                          } else {
                            const err19 = {
                              instancePath: instancePath + "/input_data",
                              schemaPath:
                                "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/type",
                              keyword: "type",
                              params: { type: "array" },
                              message: "must be array",
                            };
                            if (vErrors === null) {
                              vErrors = [err19];
                            } else {
                              vErrors.push(err19);
                            }
                            errors++;
                          }
                        }
                        var _valid2 = _errs49 === errors;
                        valid10 = valid10 || _valid2;
                        if (!valid10) {
                          const err20 = {
                            instancePath: instancePath + "/input_data",
                            schemaPath:
                              "https://jtsc-schemas.org/types/json/any.schema.json/anyOf",
                            keyword: "anyOf",
                            params: {},
                            message: "must match a schema in anyOf",
                          };
                          if (vErrors === null) {
                            vErrors = [err20];
                          } else {
                            vErrors.push(err20);
                          }
                          errors++;
                          validate20.errors = vErrors;
                          return false;
                        } else {
                          errors = _errs36;
                          if (vErrors !== null) {
                            if (_errs36) {
                              vErrors.length = _errs36;
                            } else {
                              vErrors = null;
                            }
                          }
                        }
                        var valid0 = _errs34 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.result_data !== undefined) {
                          let data7 = data.result_data;
                          const _errs51 = errors;
                          const _errs53 = errors;
                          let valid12 = false;
                          const _errs54 = errors;
                          if (data7 !== null) {
                            let coerced8 = undefined;
                            if (!(coerced8 !== undefined)) {
                              if (
                                data7 === "" ||
                                data7 === 0 ||
                                data7 === false
                              ) {
                                coerced8 = null;
                              } else {
                                const err21 = {
                                  instancePath: instancePath + "/result_data",
                                  schemaPath:
                                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/0/type",
                                  keyword: "type",
                                  params: { type: "null" },
                                  message: "must be null",
                                };
                                if (vErrors === null) {
                                  vErrors = [err21];
                                } else {
                                  vErrors.push(err21);
                                }
                                errors++;
                              }
                            }
                            if (coerced8 !== undefined) {
                              data7 = coerced8;
                              if (data !== undefined) {
                                data["result_data"] = coerced8;
                              }
                            }
                          }
                          var _valid3 = _errs54 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs56 = errors;
                          if (typeof data7 !== "boolean") {
                            let coerced9 = undefined;
                            if (!(coerced9 !== undefined)) {
                              if (
                                data7 === "false" ||
                                data7 === 0 ||
                                data7 === null
                              ) {
                                coerced9 = false;
                              } else if (data7 === "true" || data7 === 1) {
                                coerced9 = true;
                              } else {
                                const err22 = {
                                  instancePath: instancePath + "/result_data",
                                  schemaPath:
                                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/1/type",
                                  keyword: "type",
                                  params: { type: "boolean" },
                                  message: "must be boolean",
                                };
                                if (vErrors === null) {
                                  vErrors = [err22];
                                } else {
                                  vErrors.push(err22);
                                }
                                errors++;
                              }
                            }
                            if (coerced9 !== undefined) {
                              data7 = coerced9;
                              if (data !== undefined) {
                                data["result_data"] = coerced9;
                              }
                            }
                          }
                          var _valid3 = _errs56 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs58 = errors;
                          if (!(typeof data7 == "number" && isFinite(data7))) {
                            let dataType10 = typeof data7;
                            let coerced10 = undefined;
                            if (!(coerced10 !== undefined)) {
                              if (
                                dataType10 == "boolean" ||
                                data7 === null ||
                                (dataType10 == "string" &&
                                  data7 &&
                                  data7 == +data7)
                              ) {
                                coerced10 = +data7;
                              } else {
                                const err23 = {
                                  instancePath: instancePath + "/result_data",
                                  schemaPath:
                                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/2/type",
                                  keyword: "type",
                                  params: { type: "number" },
                                  message: "must be number",
                                };
                                if (vErrors === null) {
                                  vErrors = [err23];
                                } else {
                                  vErrors.push(err23);
                                }
                                errors++;
                              }
                            }
                            if (coerced10 !== undefined) {
                              data7 = coerced10;
                              if (data !== undefined) {
                                data["result_data"] = coerced10;
                              }
                            }
                          }
                          var _valid3 = _errs58 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs60 = errors;
                          if (typeof data7 !== "string") {
                            let dataType11 = typeof data7;
                            let coerced11 = undefined;
                            if (!(coerced11 !== undefined)) {
                              if (
                                dataType11 == "number" ||
                                dataType11 == "boolean"
                              ) {
                                coerced11 = "" + data7;
                              } else if (data7 === null) {
                                coerced11 = "";
                              } else {
                                const err24 = {
                                  instancePath: instancePath + "/result_data",
                                  schemaPath:
                                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/3/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                };
                                if (vErrors === null) {
                                  vErrors = [err24];
                                } else {
                                  vErrors.push(err24);
                                }
                                errors++;
                              }
                            }
                            if (coerced11 !== undefined) {
                              data7 = coerced11;
                              if (data !== undefined) {
                                data["result_data"] = coerced11;
                              }
                            }
                          }
                          var _valid3 = _errs60 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs62 = errors;
                          if (
                            !(
                              data7 &&
                              typeof data7 == "object" &&
                              !Array.isArray(data7)
                            )
                          ) {
                            const err25 = {
                              instancePath: instancePath + "/result_data",
                              schemaPath:
                                "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/4/type",
                              keyword: "type",
                              params: { type: "object" },
                              message: "must be object",
                            };
                            if (vErrors === null) {
                              vErrors = [err25];
                            } else {
                              vErrors.push(err25);
                            }
                            errors++;
                          }
                          var _valid3 = _errs62 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs64 = errors;
                          if (!Array.isArray(data7)) {
                            const err26 = {
                              instancePath: instancePath + "/result_data",
                              schemaPath:
                                "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/5/type",
                              keyword: "type",
                              params: { type: "array" },
                              message: "must be array",
                            };
                            if (vErrors === null) {
                              vErrors = [err26];
                            } else {
                              vErrors.push(err26);
                            }
                            errors++;
                          }
                          var _valid3 = _errs64 === errors;
                          valid12 = valid12 || _valid3;
                          const _errs66 = errors;
                          if (errors === _errs66) {
                            if (Array.isArray(data7)) {
                              if (data7.length < 1) {
                                const err27 = {
                                  instancePath: instancePath + "/result_data",
                                  schemaPath:
                                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/minItems",
                                  keyword: "minItems",
                                  params: { limit: 1 },
                                  message: "must NOT have fewer than 1 items",
                                };
                                if (vErrors === null) {
                                  vErrors = [err27];
                                } else {
                                  vErrors.push(err27);
                                }
                                errors++;
                              }
                            } else {
                              const err28 = {
                                instancePath: instancePath + "/result_data",
                                schemaPath:
                                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/type",
                                keyword: "type",
                                params: { type: "array" },
                                message: "must be array",
                              };
                              if (vErrors === null) {
                                vErrors = [err28];
                              } else {
                                vErrors.push(err28);
                              }
                              errors++;
                            }
                          }
                          var _valid3 = _errs66 === errors;
                          valid12 = valid12 || _valid3;
                          if (!valid12) {
                            const err29 = {
                              instancePath: instancePath + "/result_data",
                              schemaPath:
                                "https://jtsc-schemas.org/types/json/any.schema.json/anyOf",
                              keyword: "anyOf",
                              params: {},
                              message: "must match a schema in anyOf",
                            };
                            if (vErrors === null) {
                              vErrors = [err29];
                            } else {
                              vErrors.push(err29);
                            }
                            errors++;
                            validate20.errors = vErrors;
                            return false;
                          } else {
                            errors = _errs53;
                            if (vErrors !== null) {
                              if (_errs53) {
                                vErrors.length = _errs53;
                              } else {
                                vErrors = null;
                              }
                            }
                          }
                          var valid0 = _errs51 === errors;
                        } else {
                          var valid0 = true;
                        }
                        if (valid0) {
                          if (data.errors !== undefined) {
                            let data8 = data.errors;
                            const _errs68 = errors;
                            if (errors === _errs68) {
                              if (Array.isArray(data8)) {
                                var valid13 = true;
                                const len0 = data8.length;
                                for (let i0 = 0; i0 < len0; i0++) {
                                  let data9 = data8[i0];
                                  const _errs70 = errors;
                                  if (typeof data9 !== "string") {
                                    let dataType12 = typeof data9;
                                    let coerced12 = undefined;
                                    if (!(coerced12 !== undefined)) {
                                      if (
                                        dataType12 == "number" ||
                                        dataType12 == "boolean"
                                      ) {
                                        coerced12 = "" + data9;
                                      } else if (data9 === null) {
                                        coerced12 = "";
                                      } else {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/errors/" + i0,
                                            schemaPath:
                                              "#/properties/errors/items/type",
                                            keyword: "type",
                                            params: { type: "string" },
                                            message: "must be string",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                    if (coerced12 !== undefined) {
                                      data9 = coerced12;
                                      if (data8 !== undefined) {
                                        data8[i0] = coerced12;
                                      }
                                    }
                                  }
                                  var valid13 = _errs70 === errors;
                                  if (!valid13) {
                                    break;
                                  }
                                }
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/errors",
                                    schemaPath: "#/properties/errors/type",
                                    keyword: "type",
                                    params: { type: "array" },
                                    message: "must be array",
                                  },
                                ];
                                return false;
                              }
                            }
                            var valid0 = _errs68 === errors;
                          } else {
                            var valid0 = true;
                          }
                          if (valid0) {
                            if (data.name !== undefined) {
                              let data10 = data.name;
                              const _errs72 = errors;
                              const _errs73 = errors;
                              if (typeof data10 !== "string") {
                                let dataType13 = typeof data10;
                                let coerced13 = undefined;
                                if (!(coerced13 !== undefined)) {
                                  if (
                                    dataType13 == "number" ||
                                    dataType13 == "boolean"
                                  ) {
                                    coerced13 = "" + data10;
                                  } else if (data10 === null) {
                                    coerced13 = "";
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath: instancePath + "/name",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced13 !== undefined) {
                                  data10 = coerced13;
                                  if (data !== undefined) {
                                    data["name"] = coerced13;
                                  }
                                }
                              }
                              if (errors === _errs73) {
                                if (typeof data10 === "string") {
                                  if (func2(data10) < 1) {
                                    validate20.errors = [
                                      {
                                        instancePath: instancePath + "/name",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                                        keyword: "minLength",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 characters",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                              var valid0 = _errs72 === errors;
                            } else {
                              var valid0 = true;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
