/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IOperationStatus } from "#codegen/schema/lib/entities/operation/status";
import { type IOperationType } from "#codegen/schema/lib/entities/operation/type";
import { type IJSONAny } from "#codegen/schema/lib/types/json/any";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";

export interface IOperation {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  created_by: IBigSerialInteger;
  status: IOperationStatus;
  type: IOperationType;
  input_data: IJSONAny;
  result_data?: IJSONAny;
  /**
   * An array of error messages, if any.
   */
  errors?: string[];
  name?: INonEmptyString;
}
