/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationSchema } from "./schema";
import { validate } from "./validate";
import { IOperation } from "./types";
export const validateOperation = createValidator<IOperation>(
  validate as ValidateFunction<IOperation>,
  operationSchema as unknown as IJSONSchema
);
