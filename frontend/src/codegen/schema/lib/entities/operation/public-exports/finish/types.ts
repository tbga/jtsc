/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for finishing public exports.
 */
export interface IOperationPublicExportsFinish {
  type: "public_exports_finish";
  /**
   * A list of finalized public export IDs.
   *
   * @minItems 1
   */
  input_data: [IBigSerialInteger, ...IBigSerialInteger[]];
  /**
   * A list of finished public export IDs.
   *
   * @minItems 1
   */
  result_data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
