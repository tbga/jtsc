/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationPublicExportsFinishSchema } from "./schema";
import { validate } from "./validate";
import { IOperationPublicExportsFinish } from "./types";
export const validateOperationPublicExportsFinish =
  createValidator<IOperationPublicExportsFinish>(
    validate as ValidateFunction<IOperationPublicExportsFinish>,
    operationPublicExportsFinishSchema as unknown as IJSONSchema
  );
