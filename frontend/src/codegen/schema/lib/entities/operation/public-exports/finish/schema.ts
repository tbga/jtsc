/*
 * This file was generated automatically, do not edit it by hand.
 */
export const operationPublicExportsFinishSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/public-exports/finish.schema.json",
  title: "OperationPublicExportsFinish",
  description: "Operation for finishing public exports.",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "public_exports_finish",
    },
    input_data: {
      description: "A list of finalized public export IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
    result_data: {
      description: "A list of finished public export IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
