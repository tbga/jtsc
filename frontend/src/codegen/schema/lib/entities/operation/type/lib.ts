/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationTypeSchema } from "./schema";
import { validate } from "./validate";
import { IOperationType } from "./types";
export const validateOperationType = createValidator<IOperationType>(
  validate as ValidateFunction<IOperationType>,
  operationTypeSchema as unknown as IJSONSchema
);
