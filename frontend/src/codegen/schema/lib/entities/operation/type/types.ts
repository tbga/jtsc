/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * The type of operation.
 */
export type IOperationType =
  | "local_upload"
  | "releases_create"
  | "profiles_create"
  | "public_exports_finish"
  | "public_imports_create"
  | "public_imports_consume";
