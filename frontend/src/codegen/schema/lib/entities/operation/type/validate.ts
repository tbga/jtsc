/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/operation/type.schema.json",
  title: "OperationType",
  description: "The type of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    { const: "local_upload", description: "Upload of the files." },
    { const: "releases_create", description: "Creation of releases." },
    { const: "profiles_create", description: "Creation of profiles." },
    { const: "public_exports_finish", description: "Finish public exports." },
    { const: "public_imports_create", description: "Create public imports." },
    { const: "public_imports_consume", description: "Consume public imports." },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/operation/type.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  const _errs1 = errors;
  let valid0 = false;
  const _errs2 = errors;
  if ("local_upload" !== data) {
    const err0 = {
      instancePath,
      schemaPath: "#/anyOf/0/const",
      keyword: "const",
      params: { allowedValue: "local_upload" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err0];
    } else {
      vErrors.push(err0);
    }
    errors++;
  }
  var _valid0 = _errs2 === errors;
  valid0 = valid0 || _valid0;
  const _errs3 = errors;
  if ("releases_create" !== data) {
    const err1 = {
      instancePath,
      schemaPath: "#/anyOf/1/const",
      keyword: "const",
      params: { allowedValue: "releases_create" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err1];
    } else {
      vErrors.push(err1);
    }
    errors++;
  }
  var _valid0 = _errs3 === errors;
  valid0 = valid0 || _valid0;
  const _errs4 = errors;
  if ("profiles_create" !== data) {
    const err2 = {
      instancePath,
      schemaPath: "#/anyOf/2/const",
      keyword: "const",
      params: { allowedValue: "profiles_create" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err2];
    } else {
      vErrors.push(err2);
    }
    errors++;
  }
  var _valid0 = _errs4 === errors;
  valid0 = valid0 || _valid0;
  const _errs5 = errors;
  if ("public_exports_finish" !== data) {
    const err3 = {
      instancePath,
      schemaPath: "#/anyOf/3/const",
      keyword: "const",
      params: { allowedValue: "public_exports_finish" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err3];
    } else {
      vErrors.push(err3);
    }
    errors++;
  }
  var _valid0 = _errs5 === errors;
  valid0 = valid0 || _valid0;
  const _errs6 = errors;
  if ("public_imports_create" !== data) {
    const err4 = {
      instancePath,
      schemaPath: "#/anyOf/4/const",
      keyword: "const",
      params: { allowedValue: "public_imports_create" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err4];
    } else {
      vErrors.push(err4);
    }
    errors++;
  }
  var _valid0 = _errs6 === errors;
  valid0 = valid0 || _valid0;
  const _errs7 = errors;
  if ("public_imports_consume" !== data) {
    const err5 = {
      instancePath,
      schemaPath: "#/anyOf/5/const",
      keyword: "const",
      params: { allowedValue: "public_imports_consume" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err5];
    } else {
      vErrors.push(err5);
    }
    errors++;
  }
  var _valid0 = _errs7 === errors;
  valid0 = valid0 || _valid0;
  if (!valid0) {
    const err6 = {
      instancePath,
      schemaPath: "#/anyOf",
      keyword: "anyOf",
      params: {},
      message: "must match a schema in anyOf",
    };
    if (vErrors === null) {
      vErrors = [err6];
    } else {
      vErrors.push(err6);
    }
    errors++;
    validate20.errors = vErrors;
    return false;
  } else {
    errors = _errs1;
    if (vErrors !== null) {
      if (_errs1) {
        vErrors.length = _errs1;
      } else {
        vErrors = null;
      }
    }
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      for (const key0 in data) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/additionalProperties",
            keyword: "additionalProperties",
            params: { additionalProperty: key0 },
            message: "must NOT have additional properties",
          },
        ];
        return false;
        break;
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
