/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationProfilesCreateSchema } from "./schema";
import { validate } from "./validate";
import { IOperationProfilesCreate } from "./types";
export const validateOperationProfilesCreate =
  createValidator<IOperationProfilesCreate>(
    validate as ValidateFunction<IOperationProfilesCreate>,
    operationProfilesCreateSchema as unknown as IJSONSchema
  );
