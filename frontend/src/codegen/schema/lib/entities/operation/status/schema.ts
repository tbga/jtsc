/*
 * This file was generated automatically, do not edit it by hand.
 */
export const operationStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/status.schema.json",
  title: "OperationStatus",
  description: "The status of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    {
      const: "pending",
      description: "The operation is awaiting in queue.",
    },
    {
      const: "in-progress",
      description: "The operation is currently being ran.",
    },
    {
      const: "finished",
      description: "The operation has been successfully finished.",
    },
    {
      const: "failed",
      description: "The operation failed with errors.",
    },
  ],
} as const;
