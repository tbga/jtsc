/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationStatusSchema } from "./schema";
import { validate } from "./validate";
import { IOperationStatus } from "./types";
export const validateOperationStatus = createValidator<IOperationStatus>(
  validate as ValidateFunction<IOperationStatus>,
  operationStatusSchema as unknown as IJSONSchema
);
