/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/operation/status.schema.json",
  title: "OperationStatus",
  description: "The status of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    { const: "pending", description: "The operation is awaiting in queue." },
    {
      const: "in-progress",
      description: "The operation is currently being ran.",
    },
    {
      const: "finished",
      description: "The operation has been successfully finished.",
    },
    { const: "failed", description: "The operation failed with errors." },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/operation/status.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  const _errs1 = errors;
  let valid0 = false;
  const _errs2 = errors;
  if ("pending" !== data) {
    const err0 = {
      instancePath,
      schemaPath: "#/anyOf/0/const",
      keyword: "const",
      params: { allowedValue: "pending" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err0];
    } else {
      vErrors.push(err0);
    }
    errors++;
  }
  var _valid0 = _errs2 === errors;
  valid0 = valid0 || _valid0;
  const _errs3 = errors;
  if ("in-progress" !== data) {
    const err1 = {
      instancePath,
      schemaPath: "#/anyOf/1/const",
      keyword: "const",
      params: { allowedValue: "in-progress" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err1];
    } else {
      vErrors.push(err1);
    }
    errors++;
  }
  var _valid0 = _errs3 === errors;
  valid0 = valid0 || _valid0;
  const _errs4 = errors;
  if ("finished" !== data) {
    const err2 = {
      instancePath,
      schemaPath: "#/anyOf/2/const",
      keyword: "const",
      params: { allowedValue: "finished" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err2];
    } else {
      vErrors.push(err2);
    }
    errors++;
  }
  var _valid0 = _errs4 === errors;
  valid0 = valid0 || _valid0;
  const _errs5 = errors;
  if ("failed" !== data) {
    const err3 = {
      instancePath,
      schemaPath: "#/anyOf/3/const",
      keyword: "const",
      params: { allowedValue: "failed" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err3];
    } else {
      vErrors.push(err3);
    }
    errors++;
  }
  var _valid0 = _errs5 === errors;
  valid0 = valid0 || _valid0;
  if (!valid0) {
    const err4 = {
      instancePath,
      schemaPath: "#/anyOf",
      keyword: "anyOf",
      params: {},
      message: "must match a schema in anyOf",
    };
    if (vErrors === null) {
      vErrors = [err4];
    } else {
      vErrors.push(err4);
    }
    errors++;
    validate20.errors = vErrors;
    return false;
  } else {
    errors = _errs1;
    if (vErrors !== null) {
      if (_errs1) {
        vErrors.length = _errs1;
      } else {
        vErrors = null;
      }
    }
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      for (const key0 in data) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/additionalProperties",
            keyword: "additionalProperties",
            params: { additionalProperty: key0 },
            message: "must NOT have additional properties",
          },
        ];
        return false;
        break;
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
