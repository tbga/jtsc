/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * The status of operation.
 */
export type IOperationStatus =
  | "pending"
  | "in-progress"
  | "finished"
  | "failed";
