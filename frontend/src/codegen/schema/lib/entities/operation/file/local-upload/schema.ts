/*
 * This file was generated automatically, do not edit it by hand.
 */
export const operationLocalUploadSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/file/local-upload.schema.json",
  title: "OperationLocalUpload",
  description: "Operation for the local upload",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "local_upload",
    },
    input_data: {
      description: "A list of file initializers.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/file/init.schema.json",
      },
    },
    result_data: {
      description: "A list of added files IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
