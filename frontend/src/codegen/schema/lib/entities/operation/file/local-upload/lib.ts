/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationLocalUploadSchema } from "./schema";
import { validate } from "./validate";
import { IOperationLocalUpload } from "./types";
export const validateOperationLocalUpload =
  createValidator<IOperationLocalUpload>(
    validate as ValidateFunction<IOperationLocalUpload>,
    operationLocalUploadSchema as unknown as IJSONSchema
  );
