/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationReleasesCreateSchema } from "./schema";
import { validate } from "./validate";
import { IOperationReleasesCreate } from "./types";
export const validateOperationReleasesCreate =
  createValidator<IOperationReleasesCreate>(
    validate as ValidateFunction<IOperationReleasesCreate>,
    operationReleasesCreateSchema as unknown as IJSONSchema
  );
