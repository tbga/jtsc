/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/operation/release/create.schema.json",
  title: "OperationReleasesCreate",
  description: "Operation for the releases creation",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: { const: "releases_create" },
    input_data: {
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
      },
    },
    result_data: {
      description: "A list of added releases IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/release/init.schema.json",
  title: "ReleaseInit",
  description: "Release initializer.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    profile_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    original_release_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    released_at_original: {
      $ref: "http://jtsc-schemas.org/types/dates/generic.schema.json",
    },
    profiles: {
      description: "Related profiles.",
      type: "object",
      minProperties: 1,
      additionalProperties: false,
      properties: {
        ids: {
          description: "Existing profile IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
    posts: {
      description: "Related posts.",
      type: "object",
      minProperties: 1,
      additionalProperties: false,
      properties: {
        inits: {
          description: "Post initializers.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/init.schema.json",
          },
        },
        ids: {
          description: "Existing post IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
  },
};
const schema35 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema36 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema39 = {
  $id: "http://jtsc-schemas.org/types/dates/generic.schema.json",
  title: "DateTimeGeneric",
  description: "Plain text date of arbitrary format.",
  type: "string",
  minLength: 1,
  maxLength: 256,
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
const func0 = require("ajv/dist/runtime/equal").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
const schema41 = {
  $id: "https://jtsc-schemas.org/entities/post/init.schema.json",
  title: "PostInit",
  description: "Initializer for the post.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    artists: {
      description: "Artists of the post.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "IDs of existing artists.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "Initializers for new artists.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/init.schema.json",
          },
        },
      },
    },
    releases: {
      description: "Releases of the post.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "IDs of existing releases.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "Initializers for new releases.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
          },
        },
      },
    },
  },
};
const schema43 = {
  $id: "https://jtsc-schemas.org/entities/artist/init.schema.json",
  title: "ArtistInit",
  description: "Initializer for the artist.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
      description:
        "Is optional because the artist can hide it by using pseudonyms.",
    },
  },
};
const wrapper0 = { validate: validate21 };
function validate22(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/post/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate22.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!(key0 === "artists" || key0 === "releases")) {
          validate22.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.artists !== undefined) {
          let data0 = data.artists;
          const _errs2 = errors;
          if (errors === _errs2) {
            if (data0 && typeof data0 == "object" && !Array.isArray(data0)) {
              if (Object.keys(data0).length < 1) {
                validate22.errors = [
                  {
                    instancePath: instancePath + "/artists",
                    schemaPath: "#/properties/artists/minProperties",
                    keyword: "minProperties",
                    params: { limit: 1 },
                    message: "must NOT have fewer than 1 properties",
                  },
                ];
                return false;
              } else {
                const _errs4 = errors;
                for (const key1 in data0) {
                  if (!(key1 === "ids" || key1 === "inits")) {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/artists",
                        schemaPath: "#/properties/artists/additionalProperties",
                        keyword: "additionalProperties",
                        params: { additionalProperty: key1 },
                        message: "must NOT have additional properties",
                      },
                    ];
                    return false;
                    break;
                  }
                }
                if (_errs4 === errors) {
                  if (data0.ids !== undefined) {
                    let data1 = data0.ids;
                    const _errs5 = errors;
                    if (errors === _errs5) {
                      if (Array.isArray(data1)) {
                        if (data1.length < 1) {
                          validate22.errors = [
                            {
                              instancePath: instancePath + "/artists/ids",
                              schemaPath:
                                "#/properties/artists/properties/ids/minItems",
                              keyword: "minItems",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 items",
                            },
                          ];
                          return false;
                        } else {
                          var valid2 = true;
                          const len0 = data1.length;
                          for (let i0 = 0; i0 < len0; i0++) {
                            let data2 = data1[i0];
                            const _errs7 = errors;
                            const _errs8 = errors;
                            if (typeof data2 !== "string") {
                              let dataType0 = typeof data2;
                              let coerced0 = undefined;
                              if (!(coerced0 !== undefined)) {
                                if (
                                  dataType0 == "number" ||
                                  dataType0 == "boolean"
                                ) {
                                  coerced0 = "" + data2;
                                } else if (data2 === null) {
                                  coerced0 = "";
                                } else {
                                  validate22.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/artists/ids/" + i0,
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced0 !== undefined) {
                                data2 = coerced0;
                                if (data1 !== undefined) {
                                  data1[i0] = coerced0;
                                }
                              }
                            }
                            if (errors === _errs8) {
                              if (typeof data2 === "string") {
                                if (func2(data2) > 19) {
                                  validate22.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/artists/ids/" + i0,
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                      keyword: "maxLength",
                                      params: { limit: 19 },
                                      message:
                                        "must NOT have more than 19 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (func2(data2) < 1) {
                                    validate22.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/artists/ids/" + i0,
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                        keyword: "minLength",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 characters",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                            var valid2 = _errs7 === errors;
                            if (!valid2) {
                              break;
                            }
                          }
                          if (valid2) {
                            let i1 = data1.length;
                            let j0;
                            if (i1 > 1) {
                              outer0: for (; i1--; ) {
                                for (j0 = i1; j0--; ) {
                                  if (func0(data1[i1], data1[j0])) {
                                    validate22.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/artists/ids",
                                        schemaPath:
                                          "#/properties/artists/properties/ids/uniqueItems",
                                        keyword: "uniqueItems",
                                        params: { i: i1, j: j0 },
                                        message:
                                          "must NOT have duplicate items (items ## " +
                                          j0 +
                                          " and " +
                                          i1 +
                                          " are identical)",
                                      },
                                    ];
                                    return false;
                                    break outer0;
                                  }
                                }
                              }
                            }
                          }
                        }
                      } else {
                        validate22.errors = [
                          {
                            instancePath: instancePath + "/artists/ids",
                            schemaPath:
                              "#/properties/artists/properties/ids/type",
                            keyword: "type",
                            params: { type: "array" },
                            message: "must be array",
                          },
                        ];
                        return false;
                      }
                    }
                    var valid1 = _errs5 === errors;
                  } else {
                    var valid1 = true;
                  }
                  if (valid1) {
                    if (data0.inits !== undefined) {
                      let data3 = data0.inits;
                      const _errs10 = errors;
                      if (errors === _errs10) {
                        if (Array.isArray(data3)) {
                          if (data3.length < 1) {
                            validate22.errors = [
                              {
                                instancePath: instancePath + "/artists/inits",
                                schemaPath:
                                  "#/properties/artists/properties/inits/minItems",
                                keyword: "minItems",
                                params: { limit: 1 },
                                message: "must NOT have fewer than 1 items",
                              },
                            ];
                            return false;
                          } else {
                            var valid5 = true;
                            const len1 = data3.length;
                            for (let i2 = 0; i2 < len1; i2++) {
                              let data4 = data3[i2];
                              const _errs12 = errors;
                              const _errs13 = errors;
                              if (errors === _errs13) {
                                if (
                                  data4 &&
                                  typeof data4 == "object" &&
                                  !Array.isArray(data4)
                                ) {
                                  const _errs15 = errors;
                                  for (const key2 in data4) {
                                    if (!(key2 === "sex")) {
                                      validate22.errors = [
                                        {
                                          instancePath:
                                            instancePath +
                                            "/artists/inits/" +
                                            i2,
                                          schemaPath:
                                            "https://jtsc-schemas.org/entities/artist/init.schema.json/additionalProperties",
                                          keyword: "additionalProperties",
                                          params: { additionalProperty: key2 },
                                          message:
                                            "must NOT have additional properties",
                                        },
                                      ];
                                      return false;
                                      break;
                                    }
                                  }
                                  if (_errs15 === errors) {
                                    if (data4.sex !== undefined) {
                                      let data5 = data4.sex;
                                      if (typeof data5 !== "boolean") {
                                        let coerced1 = undefined;
                                        if (!(coerced1 !== undefined)) {
                                          if (
                                            data5 === "false" ||
                                            data5 === 0 ||
                                            data5 === null
                                          ) {
                                            coerced1 = false;
                                          } else if (
                                            data5 === "true" ||
                                            data5 === 1
                                          ) {
                                            coerced1 = true;
                                          } else {
                                            validate22.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/artists/inits/" +
                                                  i2 +
                                                  "/sex",
                                                schemaPath:
                                                  "https://jtsc-schemas.org/entities/artist/init.schema.json/properties/sex/type",
                                                keyword: "type",
                                                params: { type: "boolean" },
                                                message: "must be boolean",
                                              },
                                            ];
                                            return false;
                                          }
                                        }
                                        if (coerced1 !== undefined) {
                                          data5 = coerced1;
                                          if (data4 !== undefined) {
                                            data4["sex"] = coerced1;
                                          }
                                        }
                                      }
                                    }
                                  }
                                } else {
                                  validate22.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/artists/inits/" + i2,
                                      schemaPath:
                                        "https://jtsc-schemas.org/entities/artist/init.schema.json/type",
                                      keyword: "type",
                                      params: { type: "object" },
                                      message: "must be object",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              var valid5 = _errs12 === errors;
                              if (!valid5) {
                                break;
                              }
                            }
                          }
                        } else {
                          validate22.errors = [
                            {
                              instancePath: instancePath + "/artists/inits",
                              schemaPath:
                                "#/properties/artists/properties/inits/type",
                              keyword: "type",
                              params: { type: "array" },
                              message: "must be array",
                            },
                          ];
                          return false;
                        }
                      }
                      var valid1 = _errs10 === errors;
                    } else {
                      var valid1 = true;
                    }
                  }
                }
              }
            } else {
              validate22.errors = [
                {
                  instancePath: instancePath + "/artists",
                  schemaPath: "#/properties/artists/type",
                  keyword: "type",
                  params: { type: "object" },
                  message: "must be object",
                },
              ];
              return false;
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.releases !== undefined) {
            let data6 = data.releases;
            const _errs18 = errors;
            if (errors === _errs18) {
              if (data6 && typeof data6 == "object" && !Array.isArray(data6)) {
                if (Object.keys(data6).length < 1) {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/releases",
                      schemaPath: "#/properties/releases/minProperties",
                      keyword: "minProperties",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 properties",
                    },
                  ];
                  return false;
                } else {
                  const _errs20 = errors;
                  for (const key3 in data6) {
                    if (!(key3 === "ids" || key3 === "inits")) {
                      validate22.errors = [
                        {
                          instancePath: instancePath + "/releases",
                          schemaPath:
                            "#/properties/releases/additionalProperties",
                          keyword: "additionalProperties",
                          params: { additionalProperty: key3 },
                          message: "must NOT have additional properties",
                        },
                      ];
                      return false;
                      break;
                    }
                  }
                  if (_errs20 === errors) {
                    if (data6.ids !== undefined) {
                      let data7 = data6.ids;
                      const _errs21 = errors;
                      if (errors === _errs21) {
                        if (Array.isArray(data7)) {
                          if (data7.length < 1) {
                            validate22.errors = [
                              {
                                instancePath: instancePath + "/releases/ids",
                                schemaPath:
                                  "#/properties/releases/properties/ids/minItems",
                                keyword: "minItems",
                                params: { limit: 1 },
                                message: "must NOT have fewer than 1 items",
                              },
                            ];
                            return false;
                          } else {
                            var valid9 = true;
                            const len2 = data7.length;
                            for (let i3 = 0; i3 < len2; i3++) {
                              let data8 = data7[i3];
                              const _errs23 = errors;
                              const _errs24 = errors;
                              if (typeof data8 !== "string") {
                                let dataType2 = typeof data8;
                                let coerced2 = undefined;
                                if (!(coerced2 !== undefined)) {
                                  if (
                                    dataType2 == "number" ||
                                    dataType2 == "boolean"
                                  ) {
                                    coerced2 = "" + data8;
                                  } else if (data8 === null) {
                                    coerced2 = "";
                                  } else {
                                    validate22.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/releases/ids/" + i3,
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced2 !== undefined) {
                                  data8 = coerced2;
                                  if (data7 !== undefined) {
                                    data7[i3] = coerced2;
                                  }
                                }
                              }
                              if (errors === _errs24) {
                                if (typeof data8 === "string") {
                                  if (func2(data8) > 19) {
                                    validate22.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/releases/ids/" + i3,
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 19 },
                                        message:
                                          "must NOT have more than 19 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func2(data8) < 1) {
                                      validate22.errors = [
                                        {
                                          instancePath:
                                            instancePath +
                                            "/releases/ids/" +
                                            i3,
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                              var valid9 = _errs23 === errors;
                              if (!valid9) {
                                break;
                              }
                            }
                            if (valid9) {
                              let i4 = data7.length;
                              let j1;
                              if (i4 > 1) {
                                outer1: for (; i4--; ) {
                                  for (j1 = i4; j1--; ) {
                                    if (func0(data7[i4], data7[j1])) {
                                      validate22.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/releases/ids",
                                          schemaPath:
                                            "#/properties/releases/properties/ids/uniqueItems",
                                          keyword: "uniqueItems",
                                          params: { i: i4, j: j1 },
                                          message:
                                            "must NOT have duplicate items (items ## " +
                                            j1 +
                                            " and " +
                                            i4 +
                                            " are identical)",
                                        },
                                      ];
                                      return false;
                                      break outer1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        } else {
                          validate22.errors = [
                            {
                              instancePath: instancePath + "/releases/ids",
                              schemaPath:
                                "#/properties/releases/properties/ids/type",
                              keyword: "type",
                              params: { type: "array" },
                              message: "must be array",
                            },
                          ];
                          return false;
                        }
                      }
                      var valid8 = _errs21 === errors;
                    } else {
                      var valid8 = true;
                    }
                    if (valid8) {
                      if (data6.inits !== undefined) {
                        let data9 = data6.inits;
                        const _errs26 = errors;
                        if (errors === _errs26) {
                          if (Array.isArray(data9)) {
                            if (data9.length < 1) {
                              validate22.errors = [
                                {
                                  instancePath:
                                    instancePath + "/releases/inits",
                                  schemaPath:
                                    "#/properties/releases/properties/inits/minItems",
                                  keyword: "minItems",
                                  params: { limit: 1 },
                                  message: "must NOT have fewer than 1 items",
                                },
                              ];
                              return false;
                            } else {
                              var valid12 = true;
                              const len3 = data9.length;
                              for (let i5 = 0; i5 < len3; i5++) {
                                const _errs28 = errors;
                                if (
                                  !wrapper0.validate(data9[i5], {
                                    instancePath:
                                      instancePath + "/releases/inits/" + i5,
                                    parentData: data9,
                                    parentDataProperty: i5,
                                    rootData,
                                    dynamicAnchors,
                                  })
                                ) {
                                  vErrors =
                                    vErrors === null
                                      ? wrapper0.validate.errors
                                      : vErrors.concat(
                                          wrapper0.validate.errors
                                        );
                                  errors = vErrors.length;
                                }
                                var valid12 = _errs28 === errors;
                                if (!valid12) {
                                  break;
                                }
                              }
                            }
                          } else {
                            validate22.errors = [
                              {
                                instancePath: instancePath + "/releases/inits",
                                schemaPath:
                                  "#/properties/releases/properties/inits/type",
                                keyword: "type",
                                params: { type: "array" },
                                message: "must be array",
                              },
                            ];
                            return false;
                          }
                        }
                        var valid8 = _errs26 === errors;
                      } else {
                        var valid8 = true;
                      }
                    }
                  }
                }
              } else {
                validate22.errors = [
                  {
                    instancePath: instancePath + "/releases",
                    schemaPath: "#/properties/releases/type",
                    keyword: "type",
                    params: { type: "object" },
                    message: "must be object",
                  },
                ];
                return false;
              }
            }
            var valid0 = _errs18 === errors;
          } else {
            var valid0 = true;
          }
        }
      }
    } else {
      validate22.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate22.errors = vErrors;
  return errors === 0;
}
validate22.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/release/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!func1.call(schema32.properties, key0)) {
          validate21.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.profile_id !== undefined) {
          let data0 = data.profile_id;
          const _errs2 = errors;
          const _errs3 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/profile_id",
                    schemaPath:
                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["profile_id"] = coerced0;
              }
            }
          }
          if (errors === _errs3) {
            if (typeof data0 === "string") {
              if (func2(data0) > 19) {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/profile_id",
                    schemaPath:
                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                    keyword: "maxLength",
                    params: { limit: 19 },
                    message: "must NOT have more than 19 characters",
                  },
                ];
                return false;
              } else {
                if (func2(data0) < 1) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/profile_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                      keyword: "minLength",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 characters",
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.site_id !== undefined) {
            let data1 = data.site_id;
            const _errs5 = errors;
            const _errs6 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/site_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["site_id"] = coerced1;
                }
              }
            }
            if (errors === _errs6) {
              if (typeof data1 === "string") {
                if (func2(data1) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/site_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data1) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/site_id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs5 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.title !== undefined) {
              let data2 = data.title;
              const _errs8 = errors;
              const _errs9 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/title",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["title"] = coerced2;
                  }
                }
              }
              if (errors === _errs9) {
                if (typeof data2 === "string") {
                  if (func2(data2) > 216) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/title",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 216 },
                        message: "must NOT have more than 216 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func2(data2) < 1) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/title",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs8 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.description !== undefined) {
                let data3 = data.description;
                const _errs11 = errors;
                const _errs12 = errors;
                if (typeof data3 !== "string") {
                  let dataType3 = typeof data3;
                  let coerced3 = undefined;
                  if (!(coerced3 !== undefined)) {
                    if (dataType3 == "number" || dataType3 == "boolean") {
                      coerced3 = "" + data3;
                    } else if (data3 === null) {
                      coerced3 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/description",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced3 !== undefined) {
                    data3 = coerced3;
                    if (data !== undefined) {
                      data["description"] = coerced3;
                    }
                  }
                }
                if (errors === _errs12) {
                  if (typeof data3 === "string") {
                    if (func2(data3) < 1) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/description",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
                var valid0 = _errs11 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.original_release_id !== undefined) {
                  let data4 = data.original_release_id;
                  const _errs14 = errors;
                  const _errs15 = errors;
                  if (typeof data4 !== "string") {
                    let dataType4 = typeof data4;
                    let coerced4 = undefined;
                    if (!(coerced4 !== undefined)) {
                      if (dataType4 == "number" || dataType4 == "boolean") {
                        coerced4 = "" + data4;
                      } else if (data4 === null) {
                        coerced4 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/original_release_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced4 !== undefined) {
                      data4 = coerced4;
                      if (data !== undefined) {
                        data["original_release_id"] = coerced4;
                      }
                    }
                  }
                  if (errors === _errs15) {
                    if (typeof data4 === "string") {
                      if (func2(data4) < 1) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/original_release_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                  var valid0 = _errs14 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.released_at !== undefined) {
                    let data5 = data.released_at;
                    const _errs17 = errors;
                    const _errs18 = errors;
                    if (typeof data5 !== "string") {
                      let dataType5 = typeof data5;
                      let coerced5 = undefined;
                      if (!(coerced5 !== undefined)) {
                        if (dataType5 == "number" || dataType5 == "boolean") {
                          coerced5 = "" + data5;
                        } else if (data5 === null) {
                          coerced5 = "";
                        } else {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/released_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced5 !== undefined) {
                        data5 = coerced5;
                        if (data !== undefined) {
                          data["released_at"] = coerced5;
                        }
                      }
                    }
                    if (errors === _errs18) {
                      if (errors === _errs18) {
                        if (typeof data5 === "string") {
                          if (func2(data5) > 29) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/released_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 29 },
                                message:
                                  "must NOT have more than 29 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func2(data5) < 29) {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/released_at",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 29 },
                                  message:
                                    "must NOT have fewer than 29 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (!formats0.validate(data5)) {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/released_at",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                    keyword: "format",
                                    params: { format: "date-time" },
                                    message:
                                      'must match format "' + "date-time" + '"',
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                      }
                    }
                    var valid0 = _errs17 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.released_at_original !== undefined) {
                      let data6 = data.released_at_original;
                      const _errs20 = errors;
                      const _errs21 = errors;
                      if (typeof data6 !== "string") {
                        let dataType6 = typeof data6;
                        let coerced6 = undefined;
                        if (!(coerced6 !== undefined)) {
                          if (dataType6 == "number" || dataType6 == "boolean") {
                            coerced6 = "" + data6;
                          } else if (data6 === null) {
                            coerced6 = "";
                          } else {
                            validate21.errors = [
                              {
                                instancePath:
                                  instancePath + "/released_at_original",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/generic.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced6 !== undefined) {
                          data6 = coerced6;
                          if (data !== undefined) {
                            data["released_at_original"] = coerced6;
                          }
                        }
                      }
                      if (errors === _errs21) {
                        if (typeof data6 === "string") {
                          if (func2(data6) > 256) {
                            validate21.errors = [
                              {
                                instancePath:
                                  instancePath + "/released_at_original",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/generic.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 256 },
                                message:
                                  "must NOT have more than 256 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func2(data6) < 1) {
                              validate21.errors = [
                                {
                                  instancePath:
                                    instancePath + "/released_at_original",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/dates/generic.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs20 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.profiles !== undefined) {
                        let data7 = data.profiles;
                        const _errs23 = errors;
                        if (errors === _errs23) {
                          if (
                            data7 &&
                            typeof data7 == "object" &&
                            !Array.isArray(data7)
                          ) {
                            if (Object.keys(data7).length < 1) {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/profiles",
                                  schemaPath:
                                    "#/properties/profiles/minProperties",
                                  keyword: "minProperties",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 properties",
                                },
                              ];
                              return false;
                            } else {
                              const _errs25 = errors;
                              for (const key1 in data7) {
                                if (!(key1 === "ids")) {
                                  validate21.errors = [
                                    {
                                      instancePath: instancePath + "/profiles",
                                      schemaPath:
                                        "#/properties/profiles/additionalProperties",
                                      keyword: "additionalProperties",
                                      params: { additionalProperty: key1 },
                                      message:
                                        "must NOT have additional properties",
                                    },
                                  ];
                                  return false;
                                  break;
                                }
                              }
                              if (_errs25 === errors) {
                                if (data7.ids !== undefined) {
                                  let data8 = data7.ids;
                                  const _errs26 = errors;
                                  if (errors === _errs26) {
                                    if (Array.isArray(data8)) {
                                      if (data8.length < 1) {
                                        validate21.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/profiles/ids",
                                            schemaPath:
                                              "#/properties/profiles/properties/ids/minItems",
                                            keyword: "minItems",
                                            params: { limit: 1 },
                                            message:
                                              "must NOT have fewer than 1 items",
                                          },
                                        ];
                                        return false;
                                      } else {
                                        var valid9 = true;
                                        const len0 = data8.length;
                                        for (let i0 = 0; i0 < len0; i0++) {
                                          let data9 = data8[i0];
                                          const _errs28 = errors;
                                          const _errs29 = errors;
                                          if (typeof data9 !== "string") {
                                            let dataType7 = typeof data9;
                                            let coerced7 = undefined;
                                            if (!(coerced7 !== undefined)) {
                                              if (
                                                dataType7 == "number" ||
                                                dataType7 == "boolean"
                                              ) {
                                                coerced7 = "" + data9;
                                              } else if (data9 === null) {
                                                coerced7 = "";
                                              } else {
                                                validate21.errors = [
                                                  {
                                                    instancePath:
                                                      instancePath +
                                                      "/profiles/ids/" +
                                                      i0,
                                                    schemaPath:
                                                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                                    keyword: "type",
                                                    params: { type: "string" },
                                                    message: "must be string",
                                                  },
                                                ];
                                                return false;
                                              }
                                            }
                                            if (coerced7 !== undefined) {
                                              data9 = coerced7;
                                              if (data8 !== undefined) {
                                                data8[i0] = coerced7;
                                              }
                                            }
                                          }
                                          if (errors === _errs29) {
                                            if (typeof data9 === "string") {
                                              if (func2(data9) > 19) {
                                                validate21.errors = [
                                                  {
                                                    instancePath:
                                                      instancePath +
                                                      "/profiles/ids/" +
                                                      i0,
                                                    schemaPath:
                                                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                                    keyword: "maxLength",
                                                    params: { limit: 19 },
                                                    message:
                                                      "must NOT have more than 19 characters",
                                                  },
                                                ];
                                                return false;
                                              } else {
                                                if (func2(data9) < 1) {
                                                  validate21.errors = [
                                                    {
                                                      instancePath:
                                                        instancePath +
                                                        "/profiles/ids/" +
                                                        i0,
                                                      schemaPath:
                                                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                                      keyword: "minLength",
                                                      params: { limit: 1 },
                                                      message:
                                                        "must NOT have fewer than 1 characters",
                                                    },
                                                  ];
                                                  return false;
                                                }
                                              }
                                            }
                                          }
                                          var valid9 = _errs28 === errors;
                                          if (!valid9) {
                                            break;
                                          }
                                        }
                                        if (valid9) {
                                          let i1 = data8.length;
                                          let j0;
                                          if (i1 > 1) {
                                            outer0: for (; i1--; ) {
                                              for (j0 = i1; j0--; ) {
                                                if (
                                                  func0(data8[i1], data8[j0])
                                                ) {
                                                  validate21.errors = [
                                                    {
                                                      instancePath:
                                                        instancePath +
                                                        "/profiles/ids",
                                                      schemaPath:
                                                        "#/properties/profiles/properties/ids/uniqueItems",
                                                      keyword: "uniqueItems",
                                                      params: { i: i1, j: j0 },
                                                      message:
                                                        "must NOT have duplicate items (items ## " +
                                                        j0 +
                                                        " and " +
                                                        i1 +
                                                        " are identical)",
                                                    },
                                                  ];
                                                  return false;
                                                  break outer0;
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    } else {
                                      validate21.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/profiles/ids",
                                          schemaPath:
                                            "#/properties/profiles/properties/ids/type",
                                          keyword: "type",
                                          params: { type: "array" },
                                          message: "must be array",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                            }
                          } else {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/profiles",
                                schemaPath: "#/properties/profiles/type",
                                keyword: "type",
                                params: { type: "object" },
                                message: "must be object",
                              },
                            ];
                            return false;
                          }
                        }
                        var valid0 = _errs23 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.posts !== undefined) {
                          let data10 = data.posts;
                          const _errs31 = errors;
                          if (errors === _errs31) {
                            if (
                              data10 &&
                              typeof data10 == "object" &&
                              !Array.isArray(data10)
                            ) {
                              if (Object.keys(data10).length < 1) {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/posts",
                                    schemaPath:
                                      "#/properties/posts/minProperties",
                                    keyword: "minProperties",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 properties",
                                  },
                                ];
                                return false;
                              } else {
                                const _errs33 = errors;
                                for (const key2 in data10) {
                                  if (!(key2 === "inits" || key2 === "ids")) {
                                    validate21.errors = [
                                      {
                                        instancePath: instancePath + "/posts",
                                        schemaPath:
                                          "#/properties/posts/additionalProperties",
                                        keyword: "additionalProperties",
                                        params: { additionalProperty: key2 },
                                        message:
                                          "must NOT have additional properties",
                                      },
                                    ];
                                    return false;
                                    break;
                                  }
                                }
                                if (_errs33 === errors) {
                                  if (data10.inits !== undefined) {
                                    let data11 = data10.inits;
                                    const _errs34 = errors;
                                    if (errors === _errs34) {
                                      if (Array.isArray(data11)) {
                                        if (data11.length < 1) {
                                          validate21.errors = [
                                            {
                                              instancePath:
                                                instancePath + "/posts/inits",
                                              schemaPath:
                                                "#/properties/posts/properties/inits/minItems",
                                              keyword: "minItems",
                                              params: { limit: 1 },
                                              message:
                                                "must NOT have fewer than 1 items",
                                            },
                                          ];
                                          return false;
                                        } else {
                                          var valid13 = true;
                                          const len1 = data11.length;
                                          for (let i2 = 0; i2 < len1; i2++) {
                                            const _errs36 = errors;
                                            if (
                                              !validate22(data11[i2], {
                                                instancePath:
                                                  instancePath +
                                                  "/posts/inits/" +
                                                  i2,
                                                parentData: data11,
                                                parentDataProperty: i2,
                                                rootData,
                                                dynamicAnchors,
                                              })
                                            ) {
                                              vErrors =
                                                vErrors === null
                                                  ? validate22.errors
                                                  : vErrors.concat(
                                                      validate22.errors
                                                    );
                                              errors = vErrors.length;
                                            }
                                            var valid13 = _errs36 === errors;
                                            if (!valid13) {
                                              break;
                                            }
                                          }
                                        }
                                      } else {
                                        validate21.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/posts/inits",
                                            schemaPath:
                                              "#/properties/posts/properties/inits/type",
                                            keyword: "type",
                                            params: { type: "array" },
                                            message: "must be array",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                    var valid12 = _errs34 === errors;
                                  } else {
                                    var valid12 = true;
                                  }
                                  if (valid12) {
                                    if (data10.ids !== undefined) {
                                      let data13 = data10.ids;
                                      const _errs37 = errors;
                                      if (errors === _errs37) {
                                        if (Array.isArray(data13)) {
                                          if (data13.length < 1) {
                                            validate21.errors = [
                                              {
                                                instancePath:
                                                  instancePath + "/posts/ids",
                                                schemaPath:
                                                  "#/properties/posts/properties/ids/minItems",
                                                keyword: "minItems",
                                                params: { limit: 1 },
                                                message:
                                                  "must NOT have fewer than 1 items",
                                              },
                                            ];
                                            return false;
                                          } else {
                                            var valid14 = true;
                                            const len2 = data13.length;
                                            for (let i3 = 0; i3 < len2; i3++) {
                                              let data14 = data13[i3];
                                              const _errs39 = errors;
                                              const _errs40 = errors;
                                              if (typeof data14 !== "string") {
                                                let dataType8 = typeof data14;
                                                let coerced8 = undefined;
                                                if (!(coerced8 !== undefined)) {
                                                  if (
                                                    dataType8 == "number" ||
                                                    dataType8 == "boolean"
                                                  ) {
                                                    coerced8 = "" + data14;
                                                  } else if (data14 === null) {
                                                    coerced8 = "";
                                                  } else {
                                                    validate21.errors = [
                                                      {
                                                        instancePath:
                                                          instancePath +
                                                          "/posts/ids/" +
                                                          i3,
                                                        schemaPath:
                                                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                                        keyword: "type",
                                                        params: {
                                                          type: "string",
                                                        },
                                                        message:
                                                          "must be string",
                                                      },
                                                    ];
                                                    return false;
                                                  }
                                                }
                                                if (coerced8 !== undefined) {
                                                  data14 = coerced8;
                                                  if (data13 !== undefined) {
                                                    data13[i3] = coerced8;
                                                  }
                                                }
                                              }
                                              if (errors === _errs40) {
                                                if (
                                                  typeof data14 === "string"
                                                ) {
                                                  if (func2(data14) > 19) {
                                                    validate21.errors = [
                                                      {
                                                        instancePath:
                                                          instancePath +
                                                          "/posts/ids/" +
                                                          i3,
                                                        schemaPath:
                                                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                                        keyword: "maxLength",
                                                        params: { limit: 19 },
                                                        message:
                                                          "must NOT have more than 19 characters",
                                                      },
                                                    ];
                                                    return false;
                                                  } else {
                                                    if (func2(data14) < 1) {
                                                      validate21.errors = [
                                                        {
                                                          instancePath:
                                                            instancePath +
                                                            "/posts/ids/" +
                                                            i3,
                                                          schemaPath:
                                                            "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                                          keyword: "minLength",
                                                          params: { limit: 1 },
                                                          message:
                                                            "must NOT have fewer than 1 characters",
                                                        },
                                                      ];
                                                      return false;
                                                    }
                                                  }
                                                }
                                              }
                                              var valid14 = _errs39 === errors;
                                              if (!valid14) {
                                                break;
                                              }
                                            }
                                            if (valid14) {
                                              let i4 = data13.length;
                                              let j1;
                                              if (i4 > 1) {
                                                outer1: for (; i4--; ) {
                                                  for (j1 = i4; j1--; ) {
                                                    if (
                                                      func0(
                                                        data13[i4],
                                                        data13[j1]
                                                      )
                                                    ) {
                                                      validate21.errors = [
                                                        {
                                                          instancePath:
                                                            instancePath +
                                                            "/posts/ids",
                                                          schemaPath:
                                                            "#/properties/posts/properties/ids/uniqueItems",
                                                          keyword:
                                                            "uniqueItems",
                                                          params: {
                                                            i: i4,
                                                            j: j1,
                                                          },
                                                          message:
                                                            "must NOT have duplicate items (items ## " +
                                                            j1 +
                                                            " and " +
                                                            i4 +
                                                            " are identical)",
                                                        },
                                                      ];
                                                      return false;
                                                      break outer1;
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        } else {
                                          validate21.errors = [
                                            {
                                              instancePath:
                                                instancePath + "/posts/ids",
                                              schemaPath:
                                                "#/properties/posts/properties/ids/type",
                                              keyword: "type",
                                              params: { type: "array" },
                                              message: "must be array",
                                            },
                                          ];
                                          return false;
                                        }
                                      }
                                      var valid12 = _errs37 === errors;
                                    } else {
                                      var valid12 = true;
                                    }
                                  }
                                }
                              }
                            } else {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/posts",
                                  schemaPath: "#/properties/posts/type",
                                  keyword: "type",
                                  params: { type: "object" },
                                  message: "must be object",
                                },
                              ];
                              return false;
                            }
                          }
                          var valid0 = _errs31 === errors;
                        } else {
                          var valid0 = true;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/operation/release/create.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.type === undefined && (missing0 = "type")) ||
        (data.input_data === undefined && (missing0 = "input_data")) ||
        (data.result_data === undefined && (missing0 = "result_data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "type" ||
              key0 === "input_data" ||
              key0 === "result_data"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.type !== undefined) {
            const _errs2 = errors;
            if ("releases_create" !== data.type) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/type",
                  schemaPath: "#/properties/type/const",
                  keyword: "const",
                  params: { allowedValue: "releases_create" },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.input_data !== undefined) {
              let data1 = data.input_data;
              const _errs3 = errors;
              if (errors === _errs3) {
                if (Array.isArray(data1)) {
                  var valid1 = true;
                  const len0 = data1.length;
                  for (let i0 = 0; i0 < len0; i0++) {
                    const _errs5 = errors;
                    if (
                      !validate21(data1[i0], {
                        instancePath: instancePath + "/input_data/" + i0,
                        parentData: data1,
                        parentDataProperty: i0,
                        rootData,
                        dynamicAnchors,
                      })
                    ) {
                      vErrors =
                        vErrors === null
                          ? validate21.errors
                          : vErrors.concat(validate21.errors);
                      errors = vErrors.length;
                    }
                    var valid1 = _errs5 === errors;
                    if (!valid1) {
                      break;
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/input_data",
                      schemaPath: "#/properties/input_data/type",
                      keyword: "type",
                      params: { type: "array" },
                      message: "must be array",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs3 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.result_data !== undefined) {
                let data3 = data.result_data;
                const _errs6 = errors;
                if (errors === _errs6) {
                  if (Array.isArray(data3)) {
                    var valid2 = true;
                    const len1 = data3.length;
                    for (let i1 = 0; i1 < len1; i1++) {
                      let data4 = data3[i1];
                      const _errs8 = errors;
                      const _errs9 = errors;
                      if (typeof data4 !== "string") {
                        let dataType0 = typeof data4;
                        let coerced0 = undefined;
                        if (!(coerced0 !== undefined)) {
                          if (dataType0 == "number" || dataType0 == "boolean") {
                            coerced0 = "" + data4;
                          } else if (data4 === null) {
                            coerced0 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/result_data/" + i1,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced0 !== undefined) {
                          data4 = coerced0;
                          if (data3 !== undefined) {
                            data3[i1] = coerced0;
                          }
                        }
                      }
                      if (errors === _errs9) {
                        if (typeof data4 === "string") {
                          if (func2(data4) > 19) {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/result_data/" + i1,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 19 },
                                message:
                                  "must NOT have more than 19 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func2(data4) < 1) {
                              validate20.errors = [
                                {
                                  instancePath:
                                    instancePath + "/result_data/" + i1,
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid2 = _errs8 === errors;
                      if (!valid2) {
                        break;
                      }
                    }
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/result_data",
                        schemaPath: "#/properties/result_data/type",
                        keyword: "type",
                        params: { type: "array" },
                        message: "must be array",
                      },
                    ];
                    return false;
                  }
                }
                var valid0 = _errs6 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
