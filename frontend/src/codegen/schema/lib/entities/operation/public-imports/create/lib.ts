/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationPublicImportsCreateSchema } from "./schema";
import { validate } from "./validate";
import { IOperationPublicImportsCreate } from "./types";
export const validateOperationPublicImportsCreate =
  createValidator<IOperationPublicImportsCreate>(
    validate as ValidateFunction<IOperationPublicImportsCreate>,
    operationPublicImportsCreateSchema as unknown as IJSONSchema
  );
