/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationPublicImportsConsumeSchema } from "./schema";
import { validate } from "./validate";
import { IOperationPublicImportsConsume } from "./types";
export const validateOperationPublicImportsConsume =
  createValidator<IOperationPublicImportsConsume>(
    validate as ValidateFunction<IOperationPublicImportsConsume>,
    operationPublicImportsConsumeSchema as unknown as IJSONSchema
  );
