/*
 * This file was generated automatically, do not edit it by hand.
 */
export const operationPublicImportsCreateSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/public-imports/create.schema.json",
  title: "OperationPublicImportsCreate",
  description: "Operation for the creation of public imports.",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "public_imports_create",
    },
    input_data: {
      description: "A list of file urls to the import folders.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
      },
    },
    result_data: {
      description: "A list of added public import IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
