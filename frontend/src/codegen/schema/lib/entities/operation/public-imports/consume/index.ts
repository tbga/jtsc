/*
 * This file was generated automatically, do not edit it by hand.
 */
export { operationPublicImportsConsumeSchema } from "./schema";
export { validateOperationPublicImportsConsume } from "./lib";
export type { IOperationPublicImportsConsume } from "./types";
