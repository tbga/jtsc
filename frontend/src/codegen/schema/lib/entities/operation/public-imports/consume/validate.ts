/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/operation/public-imports/consume.schema.json",
  title: "OperationPublicImportsConsume",
  description: "Operation for the consumption of public imports.",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: { const: "public_imports_consume" },
    input_data: {
      description: "A list of public import IDs to consume.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
    result_data: {
      description: "A list of consumed public import IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/operation/public-imports/consume.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.type === undefined && (missing0 = "type")) ||
        (data.input_data === undefined && (missing0 = "input_data")) ||
        (data.result_data === undefined && (missing0 = "result_data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "type" ||
              key0 === "input_data" ||
              key0 === "result_data"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.type !== undefined) {
            const _errs2 = errors;
            if ("public_imports_consume" !== data.type) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/type",
                  schemaPath: "#/properties/type/const",
                  keyword: "const",
                  params: { allowedValue: "public_imports_consume" },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.input_data !== undefined) {
              let data1 = data.input_data;
              const _errs3 = errors;
              if (errors === _errs3) {
                if (Array.isArray(data1)) {
                  var valid1 = true;
                  const len0 = data1.length;
                  for (let i0 = 0; i0 < len0; i0++) {
                    let data2 = data1[i0];
                    const _errs5 = errors;
                    const _errs6 = errors;
                    if (typeof data2 !== "string") {
                      let dataType0 = typeof data2;
                      let coerced0 = undefined;
                      if (!(coerced0 !== undefined)) {
                        if (dataType0 == "number" || dataType0 == "boolean") {
                          coerced0 = "" + data2;
                        } else if (data2 === null) {
                          coerced0 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/input_data/" + i0,
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced0 !== undefined) {
                        data2 = coerced0;
                        if (data1 !== undefined) {
                          data1[i0] = coerced0;
                        }
                      }
                    }
                    if (errors === _errs6) {
                      if (typeof data2 === "string") {
                        if (func1(data2) > 19) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/input_data/" + i0,
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 19 },
                              message: "must NOT have more than 19 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data2) < 1) {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/input_data/" + i0,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid1 = _errs5 === errors;
                    if (!valid1) {
                      break;
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/input_data",
                      schemaPath: "#/properties/input_data/type",
                      keyword: "type",
                      params: { type: "array" },
                      message: "must be array",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs3 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.result_data !== undefined) {
                let data3 = data.result_data;
                const _errs8 = errors;
                if (errors === _errs8) {
                  if (Array.isArray(data3)) {
                    var valid3 = true;
                    const len1 = data3.length;
                    for (let i1 = 0; i1 < len1; i1++) {
                      let data4 = data3[i1];
                      const _errs10 = errors;
                      const _errs11 = errors;
                      if (typeof data4 !== "string") {
                        let dataType1 = typeof data4;
                        let coerced1 = undefined;
                        if (!(coerced1 !== undefined)) {
                          if (dataType1 == "number" || dataType1 == "boolean") {
                            coerced1 = "" + data4;
                          } else if (data4 === null) {
                            coerced1 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/result_data/" + i1,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced1 !== undefined) {
                          data4 = coerced1;
                          if (data3 !== undefined) {
                            data3[i1] = coerced1;
                          }
                        }
                      }
                      if (errors === _errs11) {
                        if (typeof data4 === "string") {
                          if (func1(data4) > 19) {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/result_data/" + i1,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 19 },
                                message:
                                  "must NOT have more than 19 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data4) < 1) {
                              validate20.errors = [
                                {
                                  instancePath:
                                    instancePath + "/result_data/" + i1,
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid3 = _errs10 === errors;
                      if (!valid3) {
                        break;
                      }
                    }
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/result_data",
                        schemaPath: "#/properties/result_data/type",
                        keyword: "type",
                        params: { type: "array" },
                        message: "must be array",
                      },
                    ];
                    return false;
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
