/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { operationPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IOperationPreview } from "./types";
export const validateOperationPreview = createValidator<IOperationPreview>(
  validate as ValidateFunction<IOperationPreview>,
  operationPreviewSchema as unknown as IJSONSchema
);
