/*
 * This file was generated automatically, do not edit it by hand.
 */
export { accountRoleSchema } from "./schema";
export { validateAccountRole } from "./lib";
export type { IAccountRole } from "./types";
