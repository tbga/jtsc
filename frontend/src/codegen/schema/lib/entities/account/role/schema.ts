/*
 * This file was generated automatically, do not edit it by hand.
 */
export const accountRoleSchema = {
  $id: "https://jtsc-schemas.org/entities/account/role.schema.json",
  title: "AccountRole",
  anyOf: [
    {
      const: "user",
    },
    {
      const: "administrator",
    },
  ],
} as const;
