/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { accountRoleSchema } from "./schema";
import { validate } from "./validate";
import { IAccountRole } from "./types";
export const validateAccountRole = createValidator<IAccountRole>(
  validate as ValidateFunction<IAccountRole>,
  accountRoleSchema as unknown as IJSONSchema
);
