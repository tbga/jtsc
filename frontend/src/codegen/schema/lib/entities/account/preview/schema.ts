/*
 * This file was generated automatically, do not edit it by hand.
 */
export const accountPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/account/preview.schema.json",
  title: "AccountPreview",
  description: "A preview of the account.",
  type: "object",
  additionalProperties: false,
  required: ["role"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    role: {
      $ref: "https://jtsc-schemas.org/entities/account/role.schema.json",
    },
  },
} as const;
