/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { accountPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IAccountPreview } from "./types";
export const validateAccountPreview = createValidator<IAccountPreview>(
  validate as ValidateFunction<IAccountPreview>,
  accountPreviewSchema as unknown as IJSONSchema
);
