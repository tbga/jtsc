/*
 * This file was generated automatically, do not edit it by hand.
 */
export const accountInitSchema = {
  $id: "https://jtsc-schemas.org/entities/account/init.schema.json",
  title: "AccountInit",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: {
      type: "string",
      minLength: 5,
      maxLength: 25,
    },
    password: {
      $comment:
        "https://github.com/kelektiv/node.bcrypt.js#security-issues-and-concerns",
      type: "string",
      minLength: 10,
      maxLength: 72,
    },
    invitation_code: {
      $ref: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
    },
  },
} as const;
