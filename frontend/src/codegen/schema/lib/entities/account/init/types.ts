/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type INanoID } from "#codegen/schema/lib/types/strings/nanoid";

/**
 * Account initializer.
 */
export interface IAccountInit {
  login: string;
  password: string;
  invitation_code?: INanoID;
}
