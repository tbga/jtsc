/*
 * This file was generated automatically, do not edit it by hand.
 */
export { accountInitSchema } from "./schema";
export { validateAccountInit } from "./lib";
export type { IAccountInit } from "./types";
