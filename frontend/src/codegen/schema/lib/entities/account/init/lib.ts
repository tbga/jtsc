/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { accountInitSchema } from "./schema";
import { validate } from "./validate";
import { IAccountInit } from "./types";
export const validateAccountInit = createValidator<IAccountInit>(
  validate as ValidateFunction<IAccountInit>,
  accountInitSchema as unknown as IJSONSchema
);
