/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/account/init.schema.json",
  title: "AccountInit",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: { type: "string", minLength: 5, maxLength: 25 },
    password: {
      $comment:
        "https://github.com/kelektiv/node.bcrypt.js#security-issues-and-concerns",
      type: "string",
      minLength: 10,
      maxLength: 72,
    },
    invitation_code: {
      $ref: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
  title: "NanoID",
  description:
    "The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.",
  type: "string",
  minLength: 21,
  maxLength: 21,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/account/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.login === undefined && (missing0 = "login")) ||
        (data.password === undefined && (missing0 = "password"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "login" ||
              key0 === "password" ||
              key0 === "invitation_code"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.login !== undefined) {
            let data0 = data.login;
            const _errs2 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/login",
                      schemaPath: "#/properties/login/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["login"] = coerced0;
                }
              }
            }
            if (errors === _errs2) {
              if (typeof data0 === "string") {
                if (func1(data0) > 25) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/login",
                      schemaPath: "#/properties/login/maxLength",
                      keyword: "maxLength",
                      params: { limit: 25 },
                      message: "must NOT have more than 25 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 5) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/login",
                        schemaPath: "#/properties/login/minLength",
                        keyword: "minLength",
                        params: { limit: 5 },
                        message: "must NOT have fewer than 5 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.password !== undefined) {
              let data1 = data.password;
              const _errs4 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/password",
                        schemaPath: "#/properties/password/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["password"] = coerced1;
                  }
                }
              }
              if (errors === _errs4) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 72) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/password",
                        schemaPath: "#/properties/password/maxLength",
                        keyword: "maxLength",
                        params: { limit: 72 },
                        message: "must NOT have more than 72 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 10) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/password",
                          schemaPath: "#/properties/password/minLength",
                          keyword: "minLength",
                          params: { limit: 10 },
                          message: "must NOT have fewer than 10 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.invitation_code !== undefined) {
                let data2 = data.invitation_code;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/invitation_code",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/nanoid.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["invitation_code"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (typeof data2 === "string") {
                    if (func1(data2) > 21) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/invitation_code",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/nanoid.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 21 },
                          message: "must NOT have more than 21 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data2) < 21) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/invitation_code",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/nanoid.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 21 },
                            message: "must NOT have fewer than 21 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
