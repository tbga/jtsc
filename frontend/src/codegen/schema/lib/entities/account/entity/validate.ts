/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/account/entity.schema.json",
  title: "Account",
  type: "object",
  additionalProperties: false,
  required: ["role"],
  properties: {
    role: {
      $ref: "https://jtsc-schemas.org/entities/account/role.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/account/role.schema.json",
  title: "AccountRole",
  anyOf: [{ const: "user" }, { const: "administrator" }],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/account/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.role === undefined && (missing0 = "role")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "role")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.role !== undefined) {
            let data0 = data.role;
            const _errs4 = errors;
            let valid2 = false;
            const _errs5 = errors;
            if ("user" !== data0) {
              const err0 = {
                instancePath: instancePath + "/role",
                schemaPath:
                  "https://jtsc-schemas.org/entities/account/role.schema.json/anyOf/0/const",
                keyword: "const",
                params: { allowedValue: "user" },
                message: "must be equal to constant",
              };
              if (vErrors === null) {
                vErrors = [err0];
              } else {
                vErrors.push(err0);
              }
              errors++;
            }
            var _valid0 = _errs5 === errors;
            valid2 = valid2 || _valid0;
            const _errs6 = errors;
            if ("administrator" !== data0) {
              const err1 = {
                instancePath: instancePath + "/role",
                schemaPath:
                  "https://jtsc-schemas.org/entities/account/role.schema.json/anyOf/1/const",
                keyword: "const",
                params: { allowedValue: "administrator" },
                message: "must be equal to constant",
              };
              if (vErrors === null) {
                vErrors = [err1];
              } else {
                vErrors.push(err1);
              }
              errors++;
            }
            var _valid0 = _errs6 === errors;
            valid2 = valid2 || _valid0;
            if (!valid2) {
              const err2 = {
                instancePath: instancePath + "/role",
                schemaPath:
                  "https://jtsc-schemas.org/entities/account/role.schema.json/anyOf",
                keyword: "anyOf",
                params: {},
                message: "must match a schema in anyOf",
              };
              if (vErrors === null) {
                vErrors = [err2];
              } else {
                vErrors.push(err2);
              }
              errors++;
              validate20.errors = vErrors;
              return false;
            } else {
              errors = _errs4;
              if (vErrors !== null) {
                if (_errs4) {
                  vErrors.length = _errs4;
                } else {
                  vErrors = null;
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
