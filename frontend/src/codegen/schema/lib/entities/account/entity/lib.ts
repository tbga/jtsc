/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { accountSchema } from "./schema";
import { validate } from "./validate";
import { IAccount } from "./types";
export const validateAccount = createValidator<IAccount>(
  validate as ValidateFunction<IAccount>,
  accountSchema as unknown as IJSONSchema
);
