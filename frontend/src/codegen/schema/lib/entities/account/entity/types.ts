/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IAccountRole } from "#codegen/schema/lib/entities/account/role";

export interface IAccount {
  role: IAccountRole;
}
