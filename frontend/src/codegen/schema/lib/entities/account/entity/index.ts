/*
 * This file was generated automatically, do not edit it by hand.
 */
export { accountSchema } from "./schema";
export { validateAccount } from "./lib";
export type { IAccount } from "./types";
