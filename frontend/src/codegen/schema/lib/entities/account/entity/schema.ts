/*
 * This file was generated automatically, do not edit it by hand.
 */
export const accountSchema = {
  $id: "https://jtsc-schemas.org/entities/account/entity.schema.json",
  title: "Account",
  type: "object",
  additionalProperties: false,
  required: ["role"],
  properties: {
    role: {
      $ref: "https://jtsc-schemas.org/entities/account/role.schema.json",
    },
  },
} as const;
