/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { accountLoginSchema } from "./schema";
import { validate } from "./validate";
import { IAccountLogin } from "./types";
export const validateAccountLogin = createValidator<IAccountLogin>(
  validate as ValidateFunction<IAccountLogin>,
  accountLoginSchema as unknown as IJSONSchema
);
