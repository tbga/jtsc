/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Account initializer.
 */
export interface IAccountLogin {
  login: string;
  password: string;
}
