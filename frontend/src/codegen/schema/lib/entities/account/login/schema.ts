/*
 * This file was generated automatically, do not edit it by hand.
 */
export const accountLoginSchema = {
  $id: "https://jtsc-schemas.org/entities/account/login.schema.json",
  title: "AccountLogin",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: {
      type: "string",
      minLength: 5,
      maxLength: 25,
    },
    password: {
      type: "string",
      minLength: 10,
      maxLength: 72,
    },
  },
} as const;
