/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { entityPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IEntityPreview } from "./types";
export const validateEntityPreview = createValidator<IEntityPreview>(
  validate as ValidateFunction<IEntityPreview>,
  entityPreviewSchema as unknown as IJSONSchema
);
