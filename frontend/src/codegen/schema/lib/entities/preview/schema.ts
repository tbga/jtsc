/*
 * This file was generated automatically, do not edit it by hand.
 */
export const entityPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/preview.schema.json",
  title: "EntityPreview",
  description: "A preview of the entity.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
} as const;
