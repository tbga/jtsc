/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profileNameSchema } from "./schema";
import { validate } from "./validate";
import { IProfileName } from "./types";
export const validateProfileName = createValidator<IProfileName>(
  validate as ValidateFunction<IProfileName>,
  profileNameSchema as unknown as IJSONSchema
);
