/*
 * This file was generated automatically, do not edit it by hand.
 */
export { profileNameInitSchema } from "./schema";
export { validateProfileNameInit } from "./lib";
export type { IProfileNameInit } from "./types";
