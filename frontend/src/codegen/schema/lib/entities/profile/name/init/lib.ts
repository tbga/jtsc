/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profileNameInitSchema } from "./schema";
import { validate } from "./validate";
import { IProfileNameInit } from "./types";
export const validateProfileNameInit = createValidator<IProfileNameInit>(
  validate as ValidateFunction<IProfileNameInit>,
  profileNameInitSchema as unknown as IJSONSchema
);
