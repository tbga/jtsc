/*
 * This file was generated automatically, do not edit it by hand.
 */
export const profileNameSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/name/entity.schema.json",
  $comment: "@TODO remove it",
  title: "ProfileName",
  description: "An artist's profile's name.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at", "profile_id", "name_id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    profile_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
