/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";

/**
 * An artist's profile's name.
 */
export interface IProfileName {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  profile_id: IBigSerialInteger;
  name_id: IBigSerialInteger;
}
