/*
 * This file was generated automatically, do not edit it by hand.
 */
export const profileNameInitSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/name/init.schema.json",
  $comment: "@TODO remove it",
  title: "ProfileNameInit",
  description: "Initializer for a profile name.",
  type: "object",
  additionalProperties: false,
  required: ["profile_id", "name_id"],
  properties: {
    profile_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
