/*
 * This file was generated automatically, do not edit it by hand.
 */
export const profileInitSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/init.schema.json",
  title: "ProfileInit",
  description: "Profile initializer.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    names: {
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/name/init.schema.json",
      },
    },
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    original_id: {
      type: "string",
      description: "The profile ID on the site.",
    },
    original_bio: {
      type: "string",
    },
    releases: {
      description: "Profile releases data.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "A list of existing release IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
    artists: {
      description: "Profile artists data.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "A list of existing artist IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "A list of artist initializers.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/init.schema.json",
          },
        },
      },
    },
  },
} as const;
