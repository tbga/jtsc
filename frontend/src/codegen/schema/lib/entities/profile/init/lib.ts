/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profileInitSchema } from "./schema";
import { validate } from "./validate";
import { IProfileInit } from "./types";
export const validateProfileInit = createValidator<IProfileInit>(
  validate as ValidateFunction<IProfileInit>,
  profileInitSchema as unknown as IJSONSchema
);
