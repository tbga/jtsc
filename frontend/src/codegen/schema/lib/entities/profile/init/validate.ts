/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/profile/init.schema.json",
  title: "ProfileInit",
  description: "Profile initializer.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    names: {
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/name/init.schema.json",
      },
    },
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    original_id: { type: "string", description: "The profile ID on the site." },
    original_bio: { type: "string" },
    releases: {
      description: "Profile releases data.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "A list of existing release IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
    artists: {
      description: "Profile artists data.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "A list of existing artist IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "A list of artist initializers.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/init.schema.json",
          },
        },
      },
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/name/init.schema.json",
  title: "NameInit",
  description: "Name initializer.",
  type: "object",
  additionalProperties: false,
  required: ["full_name"],
  properties: {
    full_name: {
      type: "string",
      minLength: 1,
      maxLength: 747,
      description:
        "The complete name of the name entry. The limit is the longest name as per [guinness](https://www.guinnessworldrecords.com/world-records/67285-longest-personal-name).",
      examples: [
        "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Zeus Wolfeschlegelsteinhausenbergerdorffwelchevoralternwarengewissenhaftschaferswessenschafewarenwohlgepflegeundsorgfaltigkeitbeschutzenvonangreifendurchihrraubgierigfeindewelchevoralternzwolftausendjahresvorandieerscheinenvanderersteerdemenschderraumschiffgebrauchlichtalsseinursprungvonkraftgestartseinlangefahrthinzwischensternartigraumaufdersuchenachdiesternwelchegehabtbewohnbarplanetenkreisedrehensichundwohinderneurassevonverstandigmenschlichkeitkonntefortpflanzenundsicherfreuenanlebenslanglichfreudeundruhemitnichteinfurchtvorangreifenvonandererintelligentgeschopfsvonhinzwischensternartigraum",
      ],
    },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema36 = {
  $id: "https://jtsc-schemas.org/entities/artist/init.schema.json",
  title: "ArtistInit",
  description: "Initializer for the artist.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
      description:
        "Is optional because the artist can hide it by using pseudonyms.",
    },
  },
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const func0 = require("ajv/dist/runtime/equal").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/profile/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (
          !(
            key0 === "names" ||
            key0 === "site_id" ||
            key0 === "original_id" ||
            key0 === "original_bio" ||
            key0 === "releases" ||
            key0 === "artists"
          )
        ) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.names !== undefined) {
          let data0 = data.names;
          const _errs2 = errors;
          if (errors === _errs2) {
            if (Array.isArray(data0)) {
              if (data0.length < 1) {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/names",
                    schemaPath: "#/properties/names/minItems",
                    keyword: "minItems",
                    params: { limit: 1 },
                    message: "must NOT have fewer than 1 items",
                  },
                ];
                return false;
              } else {
                var valid1 = true;
                const len0 = data0.length;
                for (let i0 = 0; i0 < len0; i0++) {
                  let data1 = data0[i0];
                  const _errs4 = errors;
                  const _errs5 = errors;
                  if (errors === _errs5) {
                    if (
                      data1 &&
                      typeof data1 == "object" &&
                      !Array.isArray(data1)
                    ) {
                      let missing0;
                      if (
                        data1.full_name === undefined &&
                        (missing0 = "full_name")
                      ) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/names/" + i0,
                            schemaPath:
                              "https://jtsc-schemas.org/entities/name/init.schema.json/required",
                            keyword: "required",
                            params: { missingProperty: missing0 },
                            message:
                              "must have required property '" + missing0 + "'",
                          },
                        ];
                        return false;
                      } else {
                        const _errs7 = errors;
                        for (const key1 in data1) {
                          if (!(key1 === "full_name")) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/names/" + i0,
                                schemaPath:
                                  "https://jtsc-schemas.org/entities/name/init.schema.json/additionalProperties",
                                keyword: "additionalProperties",
                                params: { additionalProperty: key1 },
                                message: "must NOT have additional properties",
                              },
                            ];
                            return false;
                            break;
                          }
                        }
                        if (_errs7 === errors) {
                          if (data1.full_name !== undefined) {
                            let data2 = data1.full_name;
                            const _errs8 = errors;
                            if (typeof data2 !== "string") {
                              let dataType0 = typeof data2;
                              let coerced0 = undefined;
                              if (!(coerced0 !== undefined)) {
                                if (
                                  dataType0 == "number" ||
                                  dataType0 == "boolean"
                                ) {
                                  coerced0 = "" + data2;
                                } else if (data2 === null) {
                                  coerced0 = "";
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath +
                                        "/names/" +
                                        i0 +
                                        "/full_name",
                                      schemaPath:
                                        "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced0 !== undefined) {
                                data2 = coerced0;
                                if (data1 !== undefined) {
                                  data1["full_name"] = coerced0;
                                }
                              }
                            }
                            if (errors === _errs8) {
                              if (typeof data2 === "string") {
                                if (func1(data2) > 747) {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath +
                                        "/names/" +
                                        i0 +
                                        "/full_name",
                                      schemaPath:
                                        "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/maxLength",
                                      keyword: "maxLength",
                                      params: { limit: 747 },
                                      message:
                                        "must NOT have more than 747 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (func1(data2) < 1) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath +
                                          "/names/" +
                                          i0 +
                                          "/full_name",
                                        schemaPath:
                                          "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/minLength",
                                        keyword: "minLength",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 characters",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/names/" + i0,
                          schemaPath:
                            "https://jtsc-schemas.org/entities/name/init.schema.json/type",
                          keyword: "type",
                          params: { type: "object" },
                          message: "must be object",
                        },
                      ];
                      return false;
                    }
                  }
                  var valid1 = _errs4 === errors;
                  if (!valid1) {
                    break;
                  }
                }
              }
            } else {
              validate20.errors = [
                {
                  instancePath: instancePath + "/names",
                  schemaPath: "#/properties/names/type",
                  keyword: "type",
                  params: { type: "array" },
                  message: "must be array",
                },
              ];
              return false;
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.site_id !== undefined) {
            let data3 = data.site_id;
            const _errs10 = errors;
            const _errs11 = errors;
            if (typeof data3 !== "string") {
              let dataType1 = typeof data3;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data3;
                } else if (data3 === null) {
                  coerced1 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/site_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data3 = coerced1;
                if (data !== undefined) {
                  data["site_id"] = coerced1;
                }
              }
            }
            if (errors === _errs11) {
              if (typeof data3 === "string") {
                if (func1(data3) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/site_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data3) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/site_id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs10 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.original_id !== undefined) {
              let data4 = data.original_id;
              const _errs13 = errors;
              if (typeof data4 !== "string") {
                let dataType2 = typeof data4;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data4;
                  } else if (data4 === null) {
                    coerced2 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/original_id",
                        schemaPath: "#/properties/original_id/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data4 = coerced2;
                  if (data !== undefined) {
                    data["original_id"] = coerced2;
                  }
                }
              }
              var valid0 = _errs13 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.original_bio !== undefined) {
                let data5 = data.original_bio;
                const _errs15 = errors;
                if (typeof data5 !== "string") {
                  let dataType3 = typeof data5;
                  let coerced3 = undefined;
                  if (!(coerced3 !== undefined)) {
                    if (dataType3 == "number" || dataType3 == "boolean") {
                      coerced3 = "" + data5;
                    } else if (data5 === null) {
                      coerced3 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/original_bio",
                          schemaPath: "#/properties/original_bio/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced3 !== undefined) {
                    data5 = coerced3;
                    if (data !== undefined) {
                      data["original_bio"] = coerced3;
                    }
                  }
                }
                var valid0 = _errs15 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.releases !== undefined) {
                  let data6 = data.releases;
                  const _errs17 = errors;
                  if (errors === _errs17) {
                    if (
                      data6 &&
                      typeof data6 == "object" &&
                      !Array.isArray(data6)
                    ) {
                      if (Object.keys(data6).length < 1) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/releases",
                            schemaPath: "#/properties/releases/minProperties",
                            keyword: "minProperties",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 properties",
                          },
                        ];
                        return false;
                      } else {
                        const _errs19 = errors;
                        for (const key2 in data6) {
                          if (!(key2 === "ids")) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/releases",
                                schemaPath:
                                  "#/properties/releases/additionalProperties",
                                keyword: "additionalProperties",
                                params: { additionalProperty: key2 },
                                message: "must NOT have additional properties",
                              },
                            ];
                            return false;
                            break;
                          }
                        }
                        if (_errs19 === errors) {
                          if (data6.ids !== undefined) {
                            let data7 = data6.ids;
                            const _errs20 = errors;
                            if (errors === _errs20) {
                              if (Array.isArray(data7)) {
                                if (data7.length < 1) {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/releases/ids",
                                      schemaPath:
                                        "#/properties/releases/properties/ids/minItems",
                                      keyword: "minItems",
                                      params: { limit: 1 },
                                      message:
                                        "must NOT have fewer than 1 items",
                                    },
                                  ];
                                  return false;
                                } else {
                                  var valid6 = true;
                                  const len1 = data7.length;
                                  for (let i1 = 0; i1 < len1; i1++) {
                                    let data8 = data7[i1];
                                    const _errs22 = errors;
                                    const _errs23 = errors;
                                    if (typeof data8 !== "string") {
                                      let dataType4 = typeof data8;
                                      let coerced4 = undefined;
                                      if (!(coerced4 !== undefined)) {
                                        if (
                                          dataType4 == "number" ||
                                          dataType4 == "boolean"
                                        ) {
                                          coerced4 = "" + data8;
                                        } else if (data8 === null) {
                                          coerced4 = "";
                                        } else {
                                          validate20.errors = [
                                            {
                                              instancePath:
                                                instancePath +
                                                "/releases/ids/" +
                                                i1,
                                              schemaPath:
                                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                              keyword: "type",
                                              params: { type: "string" },
                                              message: "must be string",
                                            },
                                          ];
                                          return false;
                                        }
                                      }
                                      if (coerced4 !== undefined) {
                                        data8 = coerced4;
                                        if (data7 !== undefined) {
                                          data7[i1] = coerced4;
                                        }
                                      }
                                    }
                                    if (errors === _errs23) {
                                      if (typeof data8 === "string") {
                                        if (func1(data8) > 19) {
                                          validate20.errors = [
                                            {
                                              instancePath:
                                                instancePath +
                                                "/releases/ids/" +
                                                i1,
                                              schemaPath:
                                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                              keyword: "maxLength",
                                              params: { limit: 19 },
                                              message:
                                                "must NOT have more than 19 characters",
                                            },
                                          ];
                                          return false;
                                        } else {
                                          if (func1(data8) < 1) {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/releases/ids/" +
                                                  i1,
                                                schemaPath:
                                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                                keyword: "minLength",
                                                params: { limit: 1 },
                                                message:
                                                  "must NOT have fewer than 1 characters",
                                              },
                                            ];
                                            return false;
                                          }
                                        }
                                      }
                                    }
                                    var valid6 = _errs22 === errors;
                                    if (!valid6) {
                                      break;
                                    }
                                  }
                                  if (valid6) {
                                    let i2 = data7.length;
                                    let j0;
                                    if (i2 > 1) {
                                      outer0: for (; i2--; ) {
                                        for (j0 = i2; j0--; ) {
                                          if (func0(data7[i2], data7[j0])) {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/releases/ids",
                                                schemaPath:
                                                  "#/properties/releases/properties/ids/uniqueItems",
                                                keyword: "uniqueItems",
                                                params: { i: i2, j: j0 },
                                                message:
                                                  "must NOT have duplicate items (items ## " +
                                                  j0 +
                                                  " and " +
                                                  i2 +
                                                  " are identical)",
                                              },
                                            ];
                                            return false;
                                            break outer0;
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/releases/ids",
                                    schemaPath:
                                      "#/properties/releases/properties/ids/type",
                                    keyword: "type",
                                    params: { type: "array" },
                                    message: "must be array",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                      }
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/releases",
                          schemaPath: "#/properties/releases/type",
                          keyword: "type",
                          params: { type: "object" },
                          message: "must be object",
                        },
                      ];
                      return false;
                    }
                  }
                  var valid0 = _errs17 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.artists !== undefined) {
                    let data9 = data.artists;
                    const _errs25 = errors;
                    if (errors === _errs25) {
                      if (
                        data9 &&
                        typeof data9 == "object" &&
                        !Array.isArray(data9)
                      ) {
                        if (Object.keys(data9).length < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/artists",
                              schemaPath: "#/properties/artists/minProperties",
                              keyword: "minProperties",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 properties",
                            },
                          ];
                          return false;
                        } else {
                          const _errs27 = errors;
                          for (const key3 in data9) {
                            if (!(key3 === "ids" || key3 === "inits")) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/artists",
                                  schemaPath:
                                    "#/properties/artists/additionalProperties",
                                  keyword: "additionalProperties",
                                  params: { additionalProperty: key3 },
                                  message:
                                    "must NOT have additional properties",
                                },
                              ];
                              return false;
                              break;
                            }
                          }
                          if (_errs27 === errors) {
                            if (data9.ids !== undefined) {
                              let data10 = data9.ids;
                              const _errs28 = errors;
                              if (errors === _errs28) {
                                if (Array.isArray(data10)) {
                                  if (data10.length < 1) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/artists/ids",
                                        schemaPath:
                                          "#/properties/artists/properties/ids/minItems",
                                        keyword: "minItems",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 items",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    var valid10 = true;
                                    const len2 = data10.length;
                                    for (let i3 = 0; i3 < len2; i3++) {
                                      let data11 = data10[i3];
                                      const _errs30 = errors;
                                      const _errs31 = errors;
                                      if (typeof data11 !== "string") {
                                        let dataType5 = typeof data11;
                                        let coerced5 = undefined;
                                        if (!(coerced5 !== undefined)) {
                                          if (
                                            dataType5 == "number" ||
                                            dataType5 == "boolean"
                                          ) {
                                            coerced5 = "" + data11;
                                          } else if (data11 === null) {
                                            coerced5 = "";
                                          } else {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/artists/ids/" +
                                                  i3,
                                                schemaPath:
                                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                                keyword: "type",
                                                params: { type: "string" },
                                                message: "must be string",
                                              },
                                            ];
                                            return false;
                                          }
                                        }
                                        if (coerced5 !== undefined) {
                                          data11 = coerced5;
                                          if (data10 !== undefined) {
                                            data10[i3] = coerced5;
                                          }
                                        }
                                      }
                                      if (errors === _errs31) {
                                        if (typeof data11 === "string") {
                                          if (func1(data11) > 19) {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/artists/ids/" +
                                                  i3,
                                                schemaPath:
                                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                                keyword: "maxLength",
                                                params: { limit: 19 },
                                                message:
                                                  "must NOT have more than 19 characters",
                                              },
                                            ];
                                            return false;
                                          } else {
                                            if (func1(data11) < 1) {
                                              validate20.errors = [
                                                {
                                                  instancePath:
                                                    instancePath +
                                                    "/artists/ids/" +
                                                    i3,
                                                  schemaPath:
                                                    "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                                  keyword: "minLength",
                                                  params: { limit: 1 },
                                                  message:
                                                    "must NOT have fewer than 1 characters",
                                                },
                                              ];
                                              return false;
                                            }
                                          }
                                        }
                                      }
                                      var valid10 = _errs30 === errors;
                                      if (!valid10) {
                                        break;
                                      }
                                    }
                                    if (valid10) {
                                      let i4 = data10.length;
                                      let j1;
                                      if (i4 > 1) {
                                        outer1: for (; i4--; ) {
                                          for (j1 = i4; j1--; ) {
                                            if (func0(data10[i4], data10[j1])) {
                                              validate20.errors = [
                                                {
                                                  instancePath:
                                                    instancePath +
                                                    "/artists/ids",
                                                  schemaPath:
                                                    "#/properties/artists/properties/ids/uniqueItems",
                                                  keyword: "uniqueItems",
                                                  params: { i: i4, j: j1 },
                                                  message:
                                                    "must NOT have duplicate items (items ## " +
                                                    j1 +
                                                    " and " +
                                                    i4 +
                                                    " are identical)",
                                                },
                                              ];
                                              return false;
                                              break outer1;
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/artists/ids",
                                      schemaPath:
                                        "#/properties/artists/properties/ids/type",
                                      keyword: "type",
                                      params: { type: "array" },
                                      message: "must be array",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              var valid9 = _errs28 === errors;
                            } else {
                              var valid9 = true;
                            }
                            if (valid9) {
                              if (data9.inits !== undefined) {
                                let data12 = data9.inits;
                                const _errs33 = errors;
                                if (errors === _errs33) {
                                  if (Array.isArray(data12)) {
                                    if (data12.length < 1) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/artists/inits",
                                          schemaPath:
                                            "#/properties/artists/properties/inits/minItems",
                                          keyword: "minItems",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 items",
                                        },
                                      ];
                                      return false;
                                    } else {
                                      var valid13 = true;
                                      const len3 = data12.length;
                                      for (let i5 = 0; i5 < len3; i5++) {
                                        let data13 = data12[i5];
                                        const _errs35 = errors;
                                        const _errs36 = errors;
                                        if (errors === _errs36) {
                                          if (
                                            data13 &&
                                            typeof data13 == "object" &&
                                            !Array.isArray(data13)
                                          ) {
                                            const _errs38 = errors;
                                            for (const key4 in data13) {
                                              if (!(key4 === "sex")) {
                                                validate20.errors = [
                                                  {
                                                    instancePath:
                                                      instancePath +
                                                      "/artists/inits/" +
                                                      i5,
                                                    schemaPath:
                                                      "https://jtsc-schemas.org/entities/artist/init.schema.json/additionalProperties",
                                                    keyword:
                                                      "additionalProperties",
                                                    params: {
                                                      additionalProperty: key4,
                                                    },
                                                    message:
                                                      "must NOT have additional properties",
                                                  },
                                                ];
                                                return false;
                                                break;
                                              }
                                            }
                                            if (_errs38 === errors) {
                                              if (data13.sex !== undefined) {
                                                let data14 = data13.sex;
                                                if (
                                                  typeof data14 !== "boolean"
                                                ) {
                                                  let coerced6 = undefined;
                                                  if (
                                                    !(coerced6 !== undefined)
                                                  ) {
                                                    if (
                                                      data14 === "false" ||
                                                      data14 === 0 ||
                                                      data14 === null
                                                    ) {
                                                      coerced6 = false;
                                                    } else if (
                                                      data14 === "true" ||
                                                      data14 === 1
                                                    ) {
                                                      coerced6 = true;
                                                    } else {
                                                      validate20.errors = [
                                                        {
                                                          instancePath:
                                                            instancePath +
                                                            "/artists/inits/" +
                                                            i5 +
                                                            "/sex",
                                                          schemaPath:
                                                            "https://jtsc-schemas.org/entities/artist/init.schema.json/properties/sex/type",
                                                          keyword: "type",
                                                          params: {
                                                            type: "boolean",
                                                          },
                                                          message:
                                                            "must be boolean",
                                                        },
                                                      ];
                                                      return false;
                                                    }
                                                  }
                                                  if (coerced6 !== undefined) {
                                                    data14 = coerced6;
                                                    if (data13 !== undefined) {
                                                      data13["sex"] = coerced6;
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          } else {
                                            validate20.errors = [
                                              {
                                                instancePath:
                                                  instancePath +
                                                  "/artists/inits/" +
                                                  i5,
                                                schemaPath:
                                                  "https://jtsc-schemas.org/entities/artist/init.schema.json/type",
                                                keyword: "type",
                                                params: { type: "object" },
                                                message: "must be object",
                                              },
                                            ];
                                            return false;
                                          }
                                        }
                                        var valid13 = _errs35 === errors;
                                        if (!valid13) {
                                          break;
                                        }
                                      }
                                    }
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/artists/inits",
                                        schemaPath:
                                          "#/properties/artists/properties/inits/type",
                                        keyword: "type",
                                        params: { type: "array" },
                                        message: "must be array",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                var valid9 = _errs33 === errors;
                              } else {
                                var valid9 = true;
                              }
                            }
                          }
                        }
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/artists",
                            schemaPath: "#/properties/artists/type",
                            keyword: "type",
                            params: { type: "object" },
                            message: "must be object",
                          },
                        ];
                        return false;
                      }
                    }
                    var valid0 = _errs25 === errors;
                  } else {
                    var valid0 = true;
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
