/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/profile/update.schema.json",
  title: "ProfileUpdate",
  description: "An update of artist's profile.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    original_id: {
      type: "string",
      minLength: 1,
      description:
        "The ID of the profile on the site it was registered. It is a string because the sites have a different opinion on what is an ID.\nIs optional because it's not always possible prove the existence of the profile (it is a private profile or the site is defunct etc...).",
    },
    original_bio: {
      type: "string",
      minLength: 1,
      description: "The profile's bio.",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/profile/update.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (
          !(
            key0 === "site_id" ||
            key0 === "original_id" ||
            key0 === "original_bio"
          )
        ) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.site_id !== undefined) {
          let data0 = data.site_id;
          const _errs2 = errors;
          const _errs3 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/site_id",
                    schemaPath:
                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["site_id"] = coerced0;
              }
            }
          }
          if (errors === _errs3) {
            if (typeof data0 === "string") {
              if (func1(data0) > 19) {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/site_id",
                    schemaPath:
                      "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                    keyword: "maxLength",
                    params: { limit: 19 },
                    message: "must NOT have more than 19 characters",
                  },
                ];
                return false;
              } else {
                if (func1(data0) < 1) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/site_id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                      keyword: "minLength",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 characters",
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.original_id !== undefined) {
            let data1 = data.original_id;
            const _errs5 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/original_id",
                      schemaPath: "#/properties/original_id/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["original_id"] = coerced1;
                }
              }
            }
            if (errors === _errs5) {
              if (typeof data1 === "string") {
                if (func1(data1) < 1) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/original_id",
                      schemaPath: "#/properties/original_id/minLength",
                      keyword: "minLength",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 characters",
                    },
                  ];
                  return false;
                }
              }
            }
            var valid0 = _errs5 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.original_bio !== undefined) {
              let data2 = data.original_bio;
              const _errs7 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/original_bio",
                        schemaPath: "#/properties/original_bio/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["original_bio"] = coerced2;
                  }
                }
              }
              if (errors === _errs7) {
                if (typeof data2 === "string") {
                  if (func1(data2) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/original_bio",
                        schemaPath: "#/properties/original_bio/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs7 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
