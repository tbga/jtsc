/*
 * This file was generated automatically, do not edit it by hand.
 */
export { profileUpdateSchema } from "./schema";
export { validateProfileUpdate } from "./lib";
export type { IProfileUpdate } from "./types";
