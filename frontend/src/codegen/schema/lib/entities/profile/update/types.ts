/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * An update of artist's profile.
 */
export interface IProfileUpdate {
  site_id?: IBigSerialInteger;
  /**
   * The ID of the profile on the site it was registered. It is a string because the sites have a different opinion on what is an ID.
   * Is optional because it's not always possible prove the existence of the profile (it is a private profile or the site is defunct etc...).
   */
  original_id?: string;
  /**
   * The profile's bio.
   */
  original_bio?: string;
}
