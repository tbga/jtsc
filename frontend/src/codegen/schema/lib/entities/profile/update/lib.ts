/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profileUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IProfileUpdate } from "./types";
export const validateProfileUpdate = createValidator<IProfileUpdate>(
  validate as ValidateFunction<IProfileUpdate>,
  profileUpdateSchema as unknown as IJSONSchema
);
