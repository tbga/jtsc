/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profilePreviewSchema } from "./schema";
import { validate } from "./validate";
import { IProfilePreview } from "./types";
export const validateProfilePreview = createValidator<IProfilePreview>(
  validate as ValidateFunction<IProfilePreview>,
  profilePreviewSchema as unknown as IJSONSchema
);
