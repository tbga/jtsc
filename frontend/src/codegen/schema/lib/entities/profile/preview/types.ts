/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * Preview of the profile.
 */
export interface IProfilePreview {
  id: IBigSerialInteger;
  site?: IEntityItem;
  original_id?: INonEmptyString;
  /**
   * Is a derived name for use in cards and titles.
   */
  name?: string;
  last_active_at?: IDateTime;
  artists: IBigIntegerPositive;
  releases: IBigIntegerPositive;
}
