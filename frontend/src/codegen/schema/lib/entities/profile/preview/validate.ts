/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/profile/preview.schema.json",
  title: "ProfilePreview",
  description: "Preview of the profile.",
  $comment: "@TODO Add `original_id` field.",
  type: "object",
  additionalProperties: false,
  required: ["id", "artists", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    site: { $ref: "https://jtsc-schemas.org/entities/item.schema.json" },
    original_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    name: {
      type: "string",
      description: "Is a derived name for use in cards and titles.",
    },
    last_active_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    artists: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema36 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const schema37 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const schema33 = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
const schema35 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/item.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.id === undefined && (missing0 = "id")) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "name" || key0 === "public_id")) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["public_id"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (errors === _errs8) {
                    if (typeof data2 === "string") {
                      if (func1(data2) > 36) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data2) < 36) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.test(data2)) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
const formats2 = require("ajv-formats/dist/formats").fullFormats["date-time"];
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/profile/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.artists === undefined && (missing0 = "artists")) ||
        (data.releases === undefined && (missing0 = "releases"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs2 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "site" ||
              key0 === "original_id" ||
              key0 === "name" ||
              key0 === "last_active_at" ||
              key0 === "artists" ||
              key0 === "releases"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs2 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs3 = errors;
            const _errs4 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs4) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs3 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.site !== undefined) {
              const _errs6 = errors;
              if (
                !validate21(data.site, {
                  instancePath: instancePath + "/site",
                  parentData: data,
                  parentDataProperty: "site",
                  rootData,
                  dynamicAnchors,
                })
              ) {
                vErrors =
                  vErrors === null
                    ? validate21.errors
                    : vErrors.concat(validate21.errors);
                errors = vErrors.length;
              }
              var valid0 = _errs6 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.original_id !== undefined) {
                let data2 = data.original_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType1 = typeof data2;
                  let coerced1 = undefined;
                  if (!(coerced1 !== undefined)) {
                    if (dataType1 == "number" || dataType1 == "boolean") {
                      coerced1 = "" + data2;
                    } else if (data2 === null) {
                      coerced1 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/original_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced1 !== undefined) {
                    data2 = coerced1;
                    if (data !== undefined) {
                      data["original_id"] = coerced1;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (typeof data2 === "string") {
                    if (func1(data2) < 1) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/original_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs10 = errors;
                  if (typeof data3 !== "string") {
                    let dataType2 = typeof data3;
                    let coerced2 = undefined;
                    if (!(coerced2 !== undefined)) {
                      if (dataType2 == "number" || dataType2 == "boolean") {
                        coerced2 = "" + data3;
                      } else if (data3 === null) {
                        coerced2 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath: "#/properties/name/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced2 !== undefined) {
                      data3 = coerced2;
                      if (data !== undefined) {
                        data["name"] = coerced2;
                      }
                    }
                  }
                  var valid0 = _errs10 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.last_active_at !== undefined) {
                    let data4 = data.last_active_at;
                    const _errs12 = errors;
                    const _errs13 = errors;
                    if (typeof data4 !== "string") {
                      let dataType3 = typeof data4;
                      let coerced3 = undefined;
                      if (!(coerced3 !== undefined)) {
                        if (dataType3 == "number" || dataType3 == "boolean") {
                          coerced3 = "" + data4;
                        } else if (data4 === null) {
                          coerced3 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/last_active_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced3 !== undefined) {
                        data4 = coerced3;
                        if (data !== undefined) {
                          data["last_active_at"] = coerced3;
                        }
                      }
                    }
                    if (errors === _errs13) {
                      if (errors === _errs13) {
                        if (typeof data4 === "string") {
                          if (func1(data4) > 29) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/last_active_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 29 },
                                message:
                                  "must NOT have more than 29 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data4) < 29) {
                              validate20.errors = [
                                {
                                  instancePath:
                                    instancePath + "/last_active_at",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 29 },
                                  message:
                                    "must NOT have fewer than 29 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (!formats2.validate(data4)) {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/last_active_at",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                    keyword: "format",
                                    params: { format: "date-time" },
                                    message:
                                      'must match format "' + "date-time" + '"',
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                      }
                    }
                    var valid0 = _errs12 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.artists !== undefined) {
                      let data5 = data.artists;
                      const _errs15 = errors;
                      const _errs16 = errors;
                      if (typeof data5 !== "string") {
                        let dataType4 = typeof data5;
                        let coerced4 = undefined;
                        if (!(coerced4 !== undefined)) {
                          if (dataType4 == "number" || dataType4 == "boolean") {
                            coerced4 = "" + data5;
                          } else if (data5 === null) {
                            coerced4 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/artists",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced4 !== undefined) {
                          data5 = coerced4;
                          if (data !== undefined) {
                            data["artists"] = coerced4;
                          }
                        }
                      }
                      if (errors === _errs16) {
                        if (typeof data5 === "string") {
                          if (func1(data5) > 20) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/artists",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 20 },
                                message:
                                  "must NOT have more than 20 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data5) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/artists",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs15 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.releases !== undefined) {
                        let data6 = data.releases;
                        const _errs18 = errors;
                        const _errs19 = errors;
                        if (typeof data6 !== "string") {
                          let dataType5 = typeof data6;
                          let coerced5 = undefined;
                          if (!(coerced5 !== undefined)) {
                            if (
                              dataType5 == "number" ||
                              dataType5 == "boolean"
                            ) {
                              coerced5 = "" + data6;
                            } else if (data6 === null) {
                              coerced5 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/releases",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced5 !== undefined) {
                            data6 = coerced5;
                            if (data !== undefined) {
                              data["releases"] = coerced5;
                            }
                          }
                        }
                        if (errors === _errs19) {
                          if (typeof data6 === "string") {
                            if (func1(data6) > 20) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/releases",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 20 },
                                  message:
                                    "must NOT have more than 20 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data6) < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/releases",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid0 = _errs18 === errors;
                      } else {
                        var valid0 = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
