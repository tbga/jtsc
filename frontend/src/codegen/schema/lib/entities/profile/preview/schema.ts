/*
 * This file was generated automatically, do not edit it by hand.
 */
export const profilePreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/preview.schema.json",
  title: "ProfilePreview",
  description: "Preview of the profile.",
  $comment: "@TODO Add `original_id` field.",
  type: "object",
  additionalProperties: false,
  required: ["id", "artists", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    original_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    name: {
      type: "string",
      description: "Is a derived name for use in cards and titles.",
    },
    last_active_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    artists: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
