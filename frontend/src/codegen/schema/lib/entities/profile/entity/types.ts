/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * An artist's profile.
 */
export interface IProfile {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  site?: IEntityItem;
  /**
   * Is a derived name for use in cards and titles.
   */
  name?: string;
  /**
   * The ID of the profile on the site it was registered. It is a string because the sites have a different opinion on what is an ID.
   * Is optional because it's not always possible prove the existence of the profile (it is a private profile or the site is defunct etc...).
   */
  original_id?: string;
  /**
   * The profile's bio.
   */
  original_bio?: string;
}
