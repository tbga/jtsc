/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { profileSchema } from "./schema";
import { validate } from "./validate";
import { IProfile } from "./types";
export const validateProfile = createValidator<IProfile>(
  validate as ValidateFunction<IProfile>,
  profileSchema as unknown as IJSONSchema
);
