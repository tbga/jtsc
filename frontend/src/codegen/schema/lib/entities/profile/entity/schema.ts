/*
 * This file was generated automatically, do not edit it by hand.
 */
export const profileSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/entity.schema.json",
  $comment: "@TODO cut down on extraneous derivative schemas.",
  title: "Profile",
  description: "An artist's profile.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    name: {
      type: "string",
      description: "Is a derived name for use in cards and titles.",
    },
    original_id: {
      type: "string",
      description:
        "The ID of the profile on the site it was registered. It is a string because the sites have a different opinion on what is an ID.\nIs optional because it's not always possible prove the existence of the profile (it is a private profile or the site is defunct etc...).",
    },
    original_bio: {
      type: "string",
      description: "The profile's bio.",
    },
  },
} as const;
