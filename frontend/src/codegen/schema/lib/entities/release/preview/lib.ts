/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { releasePreviewSchema } from "./schema";
import { validate } from "./validate";
import { IReleasePreview } from "./types";
export const validateReleasePreview = createValidator<IReleasePreview>(
  validate as ValidateFunction<IReleasePreview>,
  releasePreviewSchema as unknown as IJSONSchema
);
