/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { releaseSchema } from "./schema";
import { validate } from "./validate";
import { IRelease } from "./types";
export const validateRelease = createValidator<IRelease>(
  validate as ValidateFunction<IRelease>,
  releaseSchema as unknown as IJSONSchema
);
