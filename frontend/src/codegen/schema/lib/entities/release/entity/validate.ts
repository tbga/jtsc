/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/release/entity.schema.json",
  title: "Release",
  description: "A release of an artist's post on a profile.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
    site: { $ref: "https://jtsc-schemas.org/entities/item.schema.json" },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    original_release_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    released_at_original: {
      $ref: "http://jtsc-schemas.org/types/dates/generic.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema35 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const schema39 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema43 = {
  $id: "http://jtsc-schemas.org/types/dates/generic.schema.json",
  title: "DateTimeGeneric",
  description: "Plain text date of arbitrary format.",
  type: "string",
  minLength: 1,
  maxLength: 256,
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
const schema36 = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const formats4 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/item.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.id === undefined && (missing0 = "id")) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "name" || key0 === "public_id")) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func2(data1) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["public_id"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (errors === _errs8) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 36) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 36) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats4.test(data2)) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/release/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_at === undefined && (missing0 = "created_at")) ||
        (data.updated_at === undefined && (missing0 = "updated_at"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!func1.call(schema31.properties, key0)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_at !== undefined) {
              let data1 = data.created_at;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_at",
                        schemaPath:
                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_at"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func2(data1) > 29) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/created_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 29 },
                          message: "must NOT have more than 29 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func2(data1) < 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 29 },
                            message: "must NOT have fewer than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (!formats0.validate(data1)) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/created_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                              keyword: "format",
                              params: { format: "date-time" },
                              message:
                                'must match format "' + "date-time" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.updated_at !== undefined) {
                let data2 = data.updated_at;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/updated_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["updated_at"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (errors === _errs9) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/updated_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 29 },
                            message: "must NOT have more than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 29) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/updated_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 29 },
                              message: "must NOT have fewer than 29 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.validate(data2)) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/updated_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                keyword: "format",
                                params: { format: "date-time" },
                                message:
                                  'must match format "' + "date-time" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func2(data3) < 1) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.site !== undefined) {
                    const _errs14 = errors;
                    if (
                      !validate21(data.site, {
                        instancePath: instancePath + "/site",
                        parentData: data,
                        parentDataProperty: "site",
                        rootData,
                        dynamicAnchors,
                      })
                    ) {
                      vErrors =
                        vErrors === null
                          ? validate21.errors
                          : vErrors.concat(validate21.errors);
                      errors = vErrors.length;
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.title !== undefined) {
                      let data5 = data.title;
                      const _errs15 = errors;
                      const _errs16 = errors;
                      if (typeof data5 !== "string") {
                        let dataType4 = typeof data5;
                        let coerced4 = undefined;
                        if (!(coerced4 !== undefined)) {
                          if (dataType4 == "number" || dataType4 == "boolean") {
                            coerced4 = "" + data5;
                          } else if (data5 === null) {
                            coerced4 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced4 !== undefined) {
                          data5 = coerced4;
                          if (data !== undefined) {
                            data["title"] = coerced4;
                          }
                        }
                      }
                      if (errors === _errs16) {
                        if (typeof data5 === "string") {
                          if (func2(data5) > 216) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 216 },
                                message:
                                  "must NOT have more than 216 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func2(data5) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/title",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs15 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.description !== undefined) {
                        let data6 = data.description;
                        const _errs18 = errors;
                        const _errs19 = errors;
                        if (typeof data6 !== "string") {
                          let dataType5 = typeof data6;
                          let coerced5 = undefined;
                          if (!(coerced5 !== undefined)) {
                            if (
                              dataType5 == "number" ||
                              dataType5 == "boolean"
                            ) {
                              coerced5 = "" + data6;
                            } else if (data6 === null) {
                              coerced5 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/description",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced5 !== undefined) {
                            data6 = coerced5;
                            if (data !== undefined) {
                              data["description"] = coerced5;
                            }
                          }
                        }
                        if (errors === _errs19) {
                          if (typeof data6 === "string") {
                            if (func2(data6) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/description",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                        var valid0 = _errs18 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.original_release_id !== undefined) {
                          let data7 = data.original_release_id;
                          const _errs21 = errors;
                          const _errs22 = errors;
                          if (typeof data7 !== "string") {
                            let dataType6 = typeof data7;
                            let coerced6 = undefined;
                            if (!(coerced6 !== undefined)) {
                              if (
                                dataType6 == "number" ||
                                dataType6 == "boolean"
                              ) {
                                coerced6 = "" + data7;
                              } else if (data7 === null) {
                                coerced6 = "";
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/original_release_id",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced6 !== undefined) {
                              data7 = coerced6;
                              if (data !== undefined) {
                                data["original_release_id"] = coerced6;
                              }
                            }
                          }
                          if (errors === _errs22) {
                            if (typeof data7 === "string") {
                              if (func2(data7) < 1) {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/original_release_id",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                          var valid0 = _errs21 === errors;
                        } else {
                          var valid0 = true;
                        }
                        if (valid0) {
                          if (data.released_at !== undefined) {
                            let data8 = data.released_at;
                            const _errs24 = errors;
                            const _errs25 = errors;
                            if (typeof data8 !== "string") {
                              let dataType7 = typeof data8;
                              let coerced7 = undefined;
                              if (!(coerced7 !== undefined)) {
                                if (
                                  dataType7 == "number" ||
                                  dataType7 == "boolean"
                                ) {
                                  coerced7 = "" + data8;
                                } else if (data8 === null) {
                                  coerced7 = "";
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/released_at",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced7 !== undefined) {
                                data8 = coerced7;
                                if (data !== undefined) {
                                  data["released_at"] = coerced7;
                                }
                              }
                            }
                            if (errors === _errs25) {
                              if (errors === _errs25) {
                                if (typeof data8 === "string") {
                                  if (func2(data8) > 29) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/released_at",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 29 },
                                        message:
                                          "must NOT have more than 29 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func2(data8) < 29) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/released_at",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 29 },
                                          message:
                                            "must NOT have fewer than 29 characters",
                                        },
                                      ];
                                      return false;
                                    } else {
                                      if (!formats0.validate(data8)) {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/released_at",
                                            schemaPath:
                                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                            keyword: "format",
                                            params: { format: "date-time" },
                                            message:
                                              'must match format "' +
                                              "date-time" +
                                              '"',
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            var valid0 = _errs24 === errors;
                          } else {
                            var valid0 = true;
                          }
                          if (valid0) {
                            if (data.released_at_original !== undefined) {
                              let data9 = data.released_at_original;
                              const _errs27 = errors;
                              const _errs28 = errors;
                              if (typeof data9 !== "string") {
                                let dataType8 = typeof data9;
                                let coerced8 = undefined;
                                if (!(coerced8 !== undefined)) {
                                  if (
                                    dataType8 == "number" ||
                                    dataType8 == "boolean"
                                  ) {
                                    coerced8 = "" + data9;
                                  } else if (data9 === null) {
                                    coerced8 = "";
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath +
                                          "/released_at_original",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/dates/generic.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced8 !== undefined) {
                                  data9 = coerced8;
                                  if (data !== undefined) {
                                    data["released_at_original"] = coerced8;
                                  }
                                }
                              }
                              if (errors === _errs28) {
                                if (typeof data9 === "string") {
                                  if (func2(data9) > 256) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath +
                                          "/released_at_original",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/dates/generic.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 256 },
                                        message:
                                          "must NOT have more than 256 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func2(data9) < 1) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath +
                                            "/released_at_original",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/dates/generic.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                              var valid0 = _errs27 === errors;
                            } else {
                              var valid0 = true;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
