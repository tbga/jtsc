/*
 * This file was generated automatically, do not edit it by hand.
 */
export const releaseSchema = {
  $id: "https://jtsc-schemas.org/entities/release/entity.schema.json",
  title: "Release",
  description: "A release of an artist's post on a profile.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    original_release_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    released_at_original: {
      $ref: "http://jtsc-schemas.org/types/dates/generic.schema.json",
    },
  },
} as const;
