/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IDateTimeGeneric } from "#codegen/schema/lib/types/dates/generic";

/**
 * A release of an artist's post on a profile.
 */
export interface IRelease {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  name?: INonEmptyString;
  site?: IEntityItem;
  title?: ITitle;
  description?: INonEmptyString;
  original_release_id?: INonEmptyString;
  released_at?: IDateTime;
  released_at_original?: IDateTimeGeneric;
}
