/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { releaseInitSchema } from "./schema";
import { validate } from "./validate";
import { IReleaseInit } from "./types";
export const validateReleaseInit = createValidator<IReleaseInit>(
  validate as ValidateFunction<IReleaseInit>,
  releaseInitSchema as unknown as IJSONSchema
);
