/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportPreview } from "./types";
export const validatePublicExportPreview =
  createValidator<IPublicExportPreview>(
    validate as ValidateFunction<IPublicExportPreview>,
    publicExportPreviewSchema as unknown as IJSONSchema
  );
