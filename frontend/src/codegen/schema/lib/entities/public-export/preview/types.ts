/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IPublicExportStatus } from "#codegen/schema/lib/entities/public-export/status";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";

/**
 * A preview of the public export.
 */
export interface IPublicExportPreview {
  id: IBigSerialInteger;
  created_by: IBigSerialInteger;
  title: ITitle;
  name?: ITitle;
  status: IPublicExportStatus;
  sites: IBigIntegerPositive;
  public_id?: IUUID;
}
