/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-export/preview.schema.json",
  title: "PublicExportPreview",
  description: "A preview of the public export.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_by", "title", "sites", "status"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    name: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    status: {
      $ref: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema36 = {
  $id: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
  title: "PublicExportStatus",
  description: "A public export's status.",
  anyOf: [
    { const: "in-progress", description: "Public export is being worked on." },
    {
      const: "finalized",
      description: "Public export is ready to be exported.",
    },
    { const: "finished", description: "Public export has been exported." },
  ],
};
const schema37 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-export/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_by === undefined && (missing0 = "created_by")) ||
        (data.title === undefined && (missing0 = "title")) ||
        (data.sites === undefined && (missing0 = "sites")) ||
        (data.status === undefined && (missing0 = "status"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "created_by" ||
              key0 === "title" ||
              key0 === "name" ||
              key0 === "status" ||
              key0 === "sites" ||
              key0 === "public_id"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_by !== undefined) {
              let data1 = data.created_by;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_by",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_by"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 19) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_by",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 19 },
                        message: "must NOT have more than 19 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 1) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/created_by",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.title !== undefined) {
                let data2 = data.title;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/title",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["title"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (typeof data2 === "string") {
                    if (func1(data2) > 216) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/title",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 216 },
                          message: "must NOT have more than 216 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data2) < 1) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/title",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 216) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 216 },
                            message: "must NOT have more than 216 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/name",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.status !== undefined) {
                    let data4 = data.status;
                    const _errs14 = errors;
                    const _errs16 = errors;
                    let valid6 = false;
                    const _errs17 = errors;
                    if ("in-progress" !== data4) {
                      const err0 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-export/status.schema.json/anyOf/0/const",
                        keyword: "const",
                        params: { allowedValue: "in-progress" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err0];
                      } else {
                        vErrors.push(err0);
                      }
                      errors++;
                    }
                    var _valid0 = _errs17 === errors;
                    valid6 = valid6 || _valid0;
                    const _errs18 = errors;
                    if ("finalized" !== data4) {
                      const err1 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-export/status.schema.json/anyOf/1/const",
                        keyword: "const",
                        params: { allowedValue: "finalized" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err1];
                      } else {
                        vErrors.push(err1);
                      }
                      errors++;
                    }
                    var _valid0 = _errs18 === errors;
                    valid6 = valid6 || _valid0;
                    const _errs19 = errors;
                    if ("finished" !== data4) {
                      const err2 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-export/status.schema.json/anyOf/2/const",
                        keyword: "const",
                        params: { allowedValue: "finished" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err2];
                      } else {
                        vErrors.push(err2);
                      }
                      errors++;
                    }
                    var _valid0 = _errs19 === errors;
                    valid6 = valid6 || _valid0;
                    if (!valid6) {
                      const err3 = {
                        instancePath: instancePath + "/status",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-export/status.schema.json/anyOf",
                        keyword: "anyOf",
                        params: {},
                        message: "must match a schema in anyOf",
                      };
                      if (vErrors === null) {
                        vErrors = [err3];
                      } else {
                        vErrors.push(err3);
                      }
                      errors++;
                      validate20.errors = vErrors;
                      return false;
                    } else {
                      errors = _errs16;
                      if (vErrors !== null) {
                        if (_errs16) {
                          vErrors.length = _errs16;
                        } else {
                          vErrors = null;
                        }
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.sites !== undefined) {
                      let data5 = data.sites;
                      const _errs20 = errors;
                      const _errs21 = errors;
                      if (typeof data5 !== "string") {
                        let dataType4 = typeof data5;
                        let coerced4 = undefined;
                        if (!(coerced4 !== undefined)) {
                          if (dataType4 == "number" || dataType4 == "boolean") {
                            coerced4 = "" + data5;
                          } else if (data5 === null) {
                            coerced4 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/sites",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced4 !== undefined) {
                          data5 = coerced4;
                          if (data !== undefined) {
                            data["sites"] = coerced4;
                          }
                        }
                      }
                      if (errors === _errs21) {
                        if (typeof data5 === "string") {
                          if (func1(data5) > 20) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/sites",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 20 },
                                message:
                                  "must NOT have more than 20 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data5) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/sites",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs20 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.public_id !== undefined) {
                        let data6 = data.public_id;
                        const _errs23 = errors;
                        const _errs24 = errors;
                        if (typeof data6 !== "string") {
                          let dataType5 = typeof data6;
                          let coerced5 = undefined;
                          if (!(coerced5 !== undefined)) {
                            if (
                              dataType5 == "number" ||
                              dataType5 == "boolean"
                            ) {
                              coerced5 = "" + data6;
                            } else if (data6 === null) {
                              coerced5 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/public_id",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced5 !== undefined) {
                            data6 = coerced5;
                            if (data !== undefined) {
                              data["public_id"] = coerced5;
                            }
                          }
                        }
                        if (errors === _errs24) {
                          if (errors === _errs24) {
                            if (typeof data6 === "string") {
                              if (func1(data6) > 36) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/public_id",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 36 },
                                    message:
                                      "must NOT have more than 36 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func1(data6) < 36) {
                                  validate20.errors = [
                                    {
                                      instancePath: instancePath + "/public_id",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 36 },
                                      message:
                                        "must NOT have fewer than 36 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (!formats0.test(data6)) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/public_id",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                        keyword: "format",
                                        params: { format: "uuid" },
                                        message:
                                          'must match format "' + "uuid" + '"',
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                          }
                        }
                        var valid0 = _errs23 === errors;
                      } else {
                        var valid0 = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
