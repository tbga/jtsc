/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicExportInitSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/init.schema.json",
  title: "PublicExportInit",
  description: "Public export initializer.",
  type: "object",
  additionalProperties: false,
  required: ["title"],
  properties: {
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    sites: {
      description: "A list of published site IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
