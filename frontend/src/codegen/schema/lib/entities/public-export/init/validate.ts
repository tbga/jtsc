/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-export/init.schema.json",
  title: "PublicExportInit",
  description: "Public export initializer.",
  type: "object",
  additionalProperties: false,
  required: ["title"],
  properties: {
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    sites: {
      description: "A list of published site IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const func0 = require("ajv/dist/runtime/equal").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-export/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.title === undefined && (missing0 = "title")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "title" || key0 === "sites")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.title !== undefined) {
            let data0 = data.title;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/title",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["title"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 216) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/title",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 216 },
                      message: "must NOT have more than 216 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/title",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.sites !== undefined) {
              let data1 = data.sites;
              const _errs5 = errors;
              if (errors === _errs5) {
                if (Array.isArray(data1)) {
                  if (data1.length < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/sites",
                        schemaPath: "#/properties/sites/minItems",
                        keyword: "minItems",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 items",
                      },
                    ];
                    return false;
                  } else {
                    var valid2 = true;
                    const len0 = data1.length;
                    for (let i0 = 0; i0 < len0; i0++) {
                      let data2 = data1[i0];
                      const _errs7 = errors;
                      const _errs8 = errors;
                      if (typeof data2 !== "string") {
                        let dataType1 = typeof data2;
                        let coerced1 = undefined;
                        if (!(coerced1 !== undefined)) {
                          if (dataType1 == "number" || dataType1 == "boolean") {
                            coerced1 = "" + data2;
                          } else if (data2 === null) {
                            coerced1 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/sites/" + i0,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced1 !== undefined) {
                          data2 = coerced1;
                          if (data1 !== undefined) {
                            data1[i0] = coerced1;
                          }
                        }
                      }
                      if (errors === _errs8) {
                        if (typeof data2 === "string") {
                          if (func1(data2) > 19) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/sites/" + i0,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 19 },
                                message:
                                  "must NOT have more than 19 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data2) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/sites/" + i0,
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid2 = _errs7 === errors;
                      if (!valid2) {
                        break;
                      }
                    }
                    if (valid2) {
                      let i1 = data1.length;
                      let j0;
                      if (i1 > 1) {
                        outer0: for (; i1--; ) {
                          for (j0 = i1; j0--; ) {
                            if (func0(data1[i1], data1[j0])) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/sites",
                                  schemaPath: "#/properties/sites/uniqueItems",
                                  keyword: "uniqueItems",
                                  params: { i: i1, j: j0 },
                                  message:
                                    "must NOT have duplicate items (items ## " +
                                    j0 +
                                    " and " +
                                    i1 +
                                    " are identical)",
                                },
                              ];
                              return false;
                              break outer0;
                            }
                          }
                        }
                      }
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/sites",
                      schemaPath: "#/properties/sites/type",
                      keyword: "type",
                      params: { type: "array" },
                      message: "must be array",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
