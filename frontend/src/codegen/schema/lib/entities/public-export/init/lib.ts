/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportInitSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportInit } from "./types";
export const validatePublicExportInit = createValidator<IPublicExportInit>(
  validate as ValidateFunction<IPublicExportInit>,
  publicExportInitSchema as unknown as IJSONSchema
);
