/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * A public export's status.
 */
export type IPublicExportStatus = "in-progress" | "finalized" | "finished";
