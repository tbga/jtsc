/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportStatusSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportStatus } from "./types";
export const validatePublicExportStatus = createValidator<IPublicExportStatus>(
  validate as ValidateFunction<IPublicExportStatus>,
  publicExportStatusSchema as unknown as IJSONSchema
);
