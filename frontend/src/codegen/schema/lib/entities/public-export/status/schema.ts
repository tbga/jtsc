/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicExportStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
  title: "PublicExportStatus",
  description: "A public export's status.",
  anyOf: [
    {
      const: "in-progress",
      description: "Public export is being worked on.",
    },
    {
      const: "finalized",
      description: "Public export is ready to be exported.",
    },
    {
      const: "finished",
      description: "Public export has been exported.",
    },
  ],
} as const;
