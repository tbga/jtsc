/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExport } from "./types";
export const validatePublicExport = createValidator<IPublicExport>(
  validate as ValidateFunction<IPublicExport>,
  publicExportSchema as unknown as IJSONSchema
);
