/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IPublicExportStatus } from "#codegen/schema/lib/entities/public-export/status";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";

/**
 * A public export entity.
 */
export interface IPublicExport {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  created_by: IBigSerialInteger;
  title: ITitle;
  name?: ITitle;
  status: IPublicExportStatus;
  public_id?: IUUID;
}
