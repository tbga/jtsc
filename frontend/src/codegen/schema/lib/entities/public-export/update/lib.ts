/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicExportUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IPublicExportUpdate } from "./types";
export const validatePublicExportUpdate = createValidator<IPublicExportUpdate>(
  validate as ValidateFunction<IPublicExportUpdate>,
  publicExportUpdateSchema as unknown as IJSONSchema
);
