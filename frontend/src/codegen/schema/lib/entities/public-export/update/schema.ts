/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicExportUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
  title: "PublicExportUpdate",
  description: "An update schema for an entity.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
