/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ITitle } from "#codegen/schema/lib/types/strings/title";

/**
 * An update schema for an entity.
 */
export interface IPublicExportUpdate {
  title?: ITitle;
}
