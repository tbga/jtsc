/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";

/**
 * An entity's item.
 */
export interface IEntityItem {
  id: IBigSerialInteger;
  /**
   * A human-readable name for the entity
   */
  name?: string;
  public_id?: IUUID;
}
