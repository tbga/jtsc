/*
 * This file was generated automatically, do not edit it by hand.
 */
export { entityItemSchema } from "./schema";
export { validateEntityItem } from "./lib";
export type { IEntityItem } from "./types";
