/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { entityItemSchema } from "./schema";
import { validate } from "./validate";
import { IEntityItem } from "./types";
export const validateEntityItem = createValidator<IEntityItem>(
  validate as ValidateFunction<IEntityItem>,
  entityItemSchema as unknown as IJSONSchema
);
