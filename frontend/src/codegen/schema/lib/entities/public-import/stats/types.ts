/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * The stats of the public import.
 */
export interface IPublicImportStats {
  id: IBigSerialInteger;
  /**
   * Site stats.
   */
  sites: {
    all: IBigIntegerPositive;
    new: IBigIntegerPositive;
    same: IBigIntegerPositive;
    different: IBigIntegerPositive;
  };
}
