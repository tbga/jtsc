/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportStatsSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportStats } from "./types";
export const validatePublicImportStats = createValidator<IPublicImportStats>(
  validate as ValidateFunction<IPublicImportStats>,
  publicImportStatsSchema as unknown as IJSONSchema
);
