/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportStatsSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/stats.schema.json",
  title: "PublicImportStats",
  description: "The stats of the public import.",
  type: "object",
  additionalProperties: false,
  required: ["id", "sites"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    sites: {
      description: "Site stats.",
      type: "object",
      additionalProperties: false,
      required: ["all", "new", "same", "different"],
      properties: {
        all: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        new: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        same: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        different: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
      },
    },
  },
} as const;
