/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type IPublicImportStatus } from "#codegen/schema/lib/entities/public-import/status";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * A preview of the public import.
 */
export interface IPublicImportPreview {
  id: IBigSerialInteger;
  created_by: IBigSerialInteger;
  public_id: IUUID;
  status: IPublicImportStatus;
  title: ITitle;
  name?: ITitle;
  sites: IBigIntegerPositive;
}
