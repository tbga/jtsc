/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportPreview } from "./types";
export const validatePublicImportPreview =
  createValidator<IPublicImportPreview>(
    validate as ValidateFunction<IPublicImportPreview>,
    publicImportPreviewSchema as unknown as IJSONSchema
  );
