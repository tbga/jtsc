/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImport } from "./types";
export const validatePublicImport = createValidator<IPublicImport>(
  validate as ValidateFunction<IPublicImport>,
  publicImportSchema as unknown as IJSONSchema
);
