/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportSiteCategorySchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportSiteCategory } from "./types";
export const validatePublicImportSiteCategory =
  createValidator<IPublicImportSiteCategory>(
    validate as ValidateFunction<IPublicImportSiteCategory>,
    publicImportSiteCategorySchema as unknown as IJSONSchema
  );
