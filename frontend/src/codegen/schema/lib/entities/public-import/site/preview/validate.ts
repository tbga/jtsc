/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/preview.schema.json",
  title: "PublicImportSitePreview",
  description: "A preview of the public import site.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "public_import",
    "category",
    "approval_status",
    "public_id",
    "title",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    public_import: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    local_site: { $ref: "https://jtsc-schemas.org/entities/item.schema.json" },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    category: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
    },
    approval_status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema35 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const schema37 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema38 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
  title: "PublicImportSiteCategory",
  anyOf: [
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
};
const schema39 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
  title: "PublicImportSiteApprovalStatus",
  description: "Approval status of public import's site.",
  anyOf: [
    { const: "undecided", description: "There was no approval decision yet." },
    {
      const: "approved",
      description: "The site is approved for public import.",
    },
    {
      const: "rejected",
      description: "The site is rejected from public import.",
    },
  ],
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const schema33 = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/item.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.id === undefined && (missing0 = "id")) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "name" || key0 === "public_id")) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["public_id"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (errors === _errs8) {
                    if (typeof data2 === "string") {
                      if (func1(data2) > 36) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data2) < 36) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.test(data2)) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-import/site/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.public_import === undefined && (missing0 = "public_import")) ||
        (data.category === undefined && (missing0 = "category")) ||
        (data.approval_status === undefined &&
          (missing0 = "approval_status")) ||
        (data.public_id === undefined && (missing0 = "public_id")) ||
        (data.title === undefined && (missing0 = "title"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "public_import" ||
              key0 === "public_id" ||
              key0 === "local_site" ||
              key0 === "title" ||
              key0 === "category" ||
              key0 === "approval_status" ||
              key0 === "name"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.public_import !== undefined) {
              const _errs5 = errors;
              if (
                !validate21(data.public_import, {
                  instancePath: instancePath + "/public_import",
                  parentData: data,
                  parentDataProperty: "public_import",
                  rootData,
                  dynamicAnchors,
                })
              ) {
                vErrors =
                  vErrors === null
                    ? validate21.errors
                    : vErrors.concat(validate21.errors);
                errors = vErrors.length;
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs6 = errors;
                const _errs7 = errors;
                if (typeof data2 !== "string") {
                  let dataType1 = typeof data2;
                  let coerced1 = undefined;
                  if (!(coerced1 !== undefined)) {
                    if (dataType1 == "number" || dataType1 == "boolean") {
                      coerced1 = "" + data2;
                    } else if (data2 === null) {
                      coerced1 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced1 !== undefined) {
                    data2 = coerced1;
                    if (data !== undefined) {
                      data["public_id"] = coerced1;
                    }
                  }
                }
                if (errors === _errs7) {
                  if (errors === _errs7) {
                    if (typeof data2 === "string") {
                      if (func1(data2) > 36) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data2) < 36) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.test(data2)) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs6 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.local_site !== undefined) {
                  const _errs9 = errors;
                  if (
                    !validate21(data.local_site, {
                      instancePath: instancePath + "/local_site",
                      parentData: data,
                      parentDataProperty: "local_site",
                      rootData,
                      dynamicAnchors,
                    })
                  ) {
                    vErrors =
                      vErrors === null
                        ? validate21.errors
                        : vErrors.concat(validate21.errors);
                    errors = vErrors.length;
                  }
                  var valid0 = _errs9 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.title !== undefined) {
                    let data4 = data.title;
                    const _errs10 = errors;
                    const _errs11 = errors;
                    if (typeof data4 !== "string") {
                      let dataType2 = typeof data4;
                      let coerced2 = undefined;
                      if (!(coerced2 !== undefined)) {
                        if (dataType2 == "number" || dataType2 == "boolean") {
                          coerced2 = "" + data4;
                        } else if (data4 === null) {
                          coerced2 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced2 !== undefined) {
                        data4 = coerced2;
                        if (data !== undefined) {
                          data["title"] = coerced2;
                        }
                      }
                    }
                    if (errors === _errs11) {
                      if (typeof data4 === "string") {
                        if (func1(data4) > 216) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 216 },
                              message: "must NOT have more than 216 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data4) < 1) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs10 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.category !== undefined) {
                      let data5 = data.category;
                      const _errs13 = errors;
                      const _errs15 = errors;
                      let valid5 = false;
                      const _errs16 = errors;
                      if ("new" !== data5) {
                        const err0 = {
                          instancePath: instancePath + "/category",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/0/const",
                          keyword: "const",
                          params: { allowedValue: "new" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err0];
                        } else {
                          vErrors.push(err0);
                        }
                        errors++;
                      }
                      var _valid0 = _errs16 === errors;
                      valid5 = valid5 || _valid0;
                      const _errs17 = errors;
                      if ("same" !== data5) {
                        const err1 = {
                          instancePath: instancePath + "/category",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/1/const",
                          keyword: "const",
                          params: { allowedValue: "same" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err1];
                        } else {
                          vErrors.push(err1);
                        }
                        errors++;
                      }
                      var _valid0 = _errs17 === errors;
                      valid5 = valid5 || _valid0;
                      const _errs18 = errors;
                      if ("different" !== data5) {
                        const err2 = {
                          instancePath: instancePath + "/category",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/2/const",
                          keyword: "const",
                          params: { allowedValue: "different" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err2];
                        } else {
                          vErrors.push(err2);
                        }
                        errors++;
                      }
                      var _valid0 = _errs18 === errors;
                      valid5 = valid5 || _valid0;
                      if (!valid5) {
                        const err3 = {
                          instancePath: instancePath + "/category",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf",
                          keyword: "anyOf",
                          params: {},
                          message: "must match a schema in anyOf",
                        };
                        if (vErrors === null) {
                          vErrors = [err3];
                        } else {
                          vErrors.push(err3);
                        }
                        errors++;
                        validate20.errors = vErrors;
                        return false;
                      } else {
                        errors = _errs15;
                        if (vErrors !== null) {
                          if (_errs15) {
                            vErrors.length = _errs15;
                          } else {
                            vErrors = null;
                          }
                        }
                      }
                      var valid0 = _errs13 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.approval_status !== undefined) {
                        let data6 = data.approval_status;
                        const _errs19 = errors;
                        const _errs21 = errors;
                        let valid7 = false;
                        const _errs22 = errors;
                        if ("undecided" !== data6) {
                          const err4 = {
                            instancePath: instancePath + "/approval_status",
                            schemaPath:
                              "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/0/const",
                            keyword: "const",
                            params: { allowedValue: "undecided" },
                            message: "must be equal to constant",
                          };
                          if (vErrors === null) {
                            vErrors = [err4];
                          } else {
                            vErrors.push(err4);
                          }
                          errors++;
                        }
                        var _valid1 = _errs22 === errors;
                        valid7 = valid7 || _valid1;
                        const _errs23 = errors;
                        if ("approved" !== data6) {
                          const err5 = {
                            instancePath: instancePath + "/approval_status",
                            schemaPath:
                              "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/1/const",
                            keyword: "const",
                            params: { allowedValue: "approved" },
                            message: "must be equal to constant",
                          };
                          if (vErrors === null) {
                            vErrors = [err5];
                          } else {
                            vErrors.push(err5);
                          }
                          errors++;
                        }
                        var _valid1 = _errs23 === errors;
                        valid7 = valid7 || _valid1;
                        const _errs24 = errors;
                        if ("rejected" !== data6) {
                          const err6 = {
                            instancePath: instancePath + "/approval_status",
                            schemaPath:
                              "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/2/const",
                            keyword: "const",
                            params: { allowedValue: "rejected" },
                            message: "must be equal to constant",
                          };
                          if (vErrors === null) {
                            vErrors = [err6];
                          } else {
                            vErrors.push(err6);
                          }
                          errors++;
                        }
                        var _valid1 = _errs24 === errors;
                        valid7 = valid7 || _valid1;
                        if (!valid7) {
                          const err7 = {
                            instancePath: instancePath + "/approval_status",
                            schemaPath:
                              "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf",
                            keyword: "anyOf",
                            params: {},
                            message: "must match a schema in anyOf",
                          };
                          if (vErrors === null) {
                            vErrors = [err7];
                          } else {
                            vErrors.push(err7);
                          }
                          errors++;
                          validate20.errors = vErrors;
                          return false;
                        } else {
                          errors = _errs21;
                          if (vErrors !== null) {
                            if (_errs21) {
                              vErrors.length = _errs21;
                            } else {
                              vErrors = null;
                            }
                          }
                        }
                        var valid0 = _errs19 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.name !== undefined) {
                          let data7 = data.name;
                          const _errs25 = errors;
                          const _errs26 = errors;
                          if (typeof data7 !== "string") {
                            let dataType3 = typeof data7;
                            let coerced3 = undefined;
                            if (!(coerced3 !== undefined)) {
                              if (
                                dataType3 == "number" ||
                                dataType3 == "boolean"
                              ) {
                                coerced3 = "" + data7;
                              } else if (data7 === null) {
                                coerced3 = "";
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/name",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced3 !== undefined) {
                              data7 = coerced3;
                              if (data !== undefined) {
                                data["name"] = coerced3;
                              }
                            }
                          }
                          if (errors === _errs26) {
                            if (typeof data7 === "string") {
                              if (func1(data7) > 216) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/name",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 216 },
                                    message:
                                      "must NOT have more than 216 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func1(data7) < 1) {
                                  validate20.errors = [
                                    {
                                      instancePath: instancePath + "/name",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 1 },
                                      message:
                                        "must NOT have fewer than 1 characters",
                                    },
                                  ];
                                  return false;
                                }
                              }
                            }
                          }
                          var valid0 = _errs25 === errors;
                        } else {
                          var valid0 = true;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
