/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/entity.schema.json",
  title: "PublicImportSite",
  description: "A public import site entity.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "category",
    "approval_status",
    "public_import",
    "public_id",
    "home_page",
    "title",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    public_import: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    category: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
    },
    approval_status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    local_site: { $ref: "https://jtsc-schemas.org/entities/item.schema.json" },
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    name: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    long_title: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema38 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
  title: "PublicImportSiteCategory",
  anyOf: [
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
};
const schema39 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
  title: "PublicImportSiteApprovalStatus",
  description: "Approval status of public import's site.",
  anyOf: [
    { const: "undecided", description: "There was no approval decision yet." },
    {
      const: "approved",
      description: "The site is approved for public import.",
    },
    {
      const: "rejected",
      description: "The site is rejected from public import.",
    },
  ],
};
const schema37 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const schema41 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema43 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
const formats4 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
const formats8 = require("ajv-formats/dist/formats").fullFormats.uri;
const schema35 = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/item.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.id === undefined && (missing0 = "id")) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "name" || key0 === "public_id")) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func2(data1) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["public_id"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (errors === _errs8) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 36) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 36) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats4.test(data2)) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
const schema45 = {
  $id: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
  title: "SiteURLTemplates",
  description:
    "[URL templates](tools.ietf.org/html/rfc6570) for a given site. They must include the same data as the site uses for its frontend, even if it's not needed for a successful URL resolution.\nFor example some sites append human-readable text to an ID and the url resolves without the append just fine too. But the text must still be included in the original value of the ID or the template, depending on use case.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    profile_list: {
      description: "A template for the url to the list of profiles.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profiles",
        "{origin}/profiles/{profile_page}",
        "{origin}/profiles{?profile_page}",
      ],
    },
    profile_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    profile: {
      description: "A template for the url to the profile.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profile/{profile_id}",
        "{origin}/profile{?profile_id}",
      ],
    },
    profile_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release_list: {
      description: "A template for the url to the list of releases.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/releases",
        "{origin}/releases/{release_page}",
        "{origin}/releases{?release_page}",
        "{origin}/profile/{profile_id}/releases/{release_page}",
        "{origin}/profile/{profile_id}/releases{?release_page}",
      ],
    },
    release_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release: {
      description: "A template for the url to the release.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/release/{release_id}",
        "{origin}/release{?release_id}",
        "{origin}/{profile_id}/{release_id}",
        "{origin}{?profile_id,release_id}",
        "{origin}/profile/{profile_id}/release/{release_id}",
        "{origin}/profile/{profile_id}/release{?release_id}",
      ],
    },
    release_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
  },
};
const schema46 = {
  $id: "http://jtsc-schemas.org/types/strings/description.schema.json",
  title: "Description",
  type: "string",
  minLength: 1,
  maxLength: 512,
  examples: [
    "Eum omnis vel quod quasi. A aut et eveniet saepe dolor aliquam nulla ea. Et corporis veniam dolorem eos et voluptatem. Voluptatum cupiditate provident provident qui.",
  ],
};
const formats10 =
  /^(?:(?:[^\x00-\x20"'<>%\\^`{|}]|%[0-9a-f]{2})|\{[+#./;?&=,!@|]?(?:[a-z0-9_]|%[0-9a-f]{2})+(?::[1-9][0-9]{0,3}|\*)?(?:,(?:[a-z0-9_]|%[0-9a-f]{2})+(?::[1-9][0-9]{0,3}|\*)?)*\})*$/i;
function validate24(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/site/url-templates.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate24.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (
          !(
            key0 === "profile_list" ||
            key0 === "profile_list_notes" ||
            key0 === "profile" ||
            key0 === "profile_notes" ||
            key0 === "release_list" ||
            key0 === "release_list_notes" ||
            key0 === "release" ||
            key0 === "release_notes"
          )
        ) {
          validate24.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.profile_list !== undefined) {
          let data0 = data.profile_list;
          const _errs2 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate24.errors = [
                  {
                    instancePath: instancePath + "/profile_list",
                    schemaPath: "#/properties/profile_list/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["profile_list"] = coerced0;
              }
            }
          }
          if (errors === _errs2) {
            if (errors === _errs2) {
              if (typeof data0 === "string") {
                if (!formats10.test(data0)) {
                  validate24.errors = [
                    {
                      instancePath: instancePath + "/profile_list",
                      schemaPath: "#/properties/profile_list/format",
                      keyword: "format",
                      params: { format: "uri-template" },
                      message: 'must match format "' + "uri-template" + '"',
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.profile_list_notes !== undefined) {
            let data1 = data.profile_list_notes;
            const _errs4 = errors;
            const _errs5 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate24.errors = [
                    {
                      instancePath: instancePath + "/profile_list_notes",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["profile_list_notes"] = coerced1;
                }
              }
            }
            if (errors === _errs5) {
              if (typeof data1 === "string") {
                if (func2(data1) > 512) {
                  validate24.errors = [
                    {
                      instancePath: instancePath + "/profile_list_notes",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 512 },
                      message: "must NOT have more than 512 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data1) < 1) {
                    validate24.errors = [
                      {
                        instancePath: instancePath + "/profile_list_notes",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs4 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.profile !== undefined) {
              let data2 = data.profile;
              const _errs7 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate24.errors = [
                      {
                        instancePath: instancePath + "/profile",
                        schemaPath: "#/properties/profile/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["profile"] = coerced2;
                  }
                }
              }
              if (errors === _errs7) {
                if (errors === _errs7) {
                  if (typeof data2 === "string") {
                    if (!formats10.test(data2)) {
                      validate24.errors = [
                        {
                          instancePath: instancePath + "/profile",
                          schemaPath: "#/properties/profile/format",
                          keyword: "format",
                          params: { format: "uri-template" },
                          message: 'must match format "' + "uri-template" + '"',
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs7 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.profile_notes !== undefined) {
                let data3 = data.profile_notes;
                const _errs9 = errors;
                const _errs10 = errors;
                if (typeof data3 !== "string") {
                  let dataType3 = typeof data3;
                  let coerced3 = undefined;
                  if (!(coerced3 !== undefined)) {
                    if (dataType3 == "number" || dataType3 == "boolean") {
                      coerced3 = "" + data3;
                    } else if (data3 === null) {
                      coerced3 = "";
                    } else {
                      validate24.errors = [
                        {
                          instancePath: instancePath + "/profile_notes",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced3 !== undefined) {
                    data3 = coerced3;
                    if (data !== undefined) {
                      data["profile_notes"] = coerced3;
                    }
                  }
                }
                if (errors === _errs10) {
                  if (typeof data3 === "string") {
                    if (func2(data3) > 512) {
                      validate24.errors = [
                        {
                          instancePath: instancePath + "/profile_notes",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 512 },
                          message: "must NOT have more than 512 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func2(data3) < 1) {
                        validate24.errors = [
                          {
                            instancePath: instancePath + "/profile_notes",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs9 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.release_list !== undefined) {
                  let data4 = data.release_list;
                  const _errs12 = errors;
                  if (typeof data4 !== "string") {
                    let dataType4 = typeof data4;
                    let coerced4 = undefined;
                    if (!(coerced4 !== undefined)) {
                      if (dataType4 == "number" || dataType4 == "boolean") {
                        coerced4 = "" + data4;
                      } else if (data4 === null) {
                        coerced4 = "";
                      } else {
                        validate24.errors = [
                          {
                            instancePath: instancePath + "/release_list",
                            schemaPath: "#/properties/release_list/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced4 !== undefined) {
                      data4 = coerced4;
                      if (data !== undefined) {
                        data["release_list"] = coerced4;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (errors === _errs12) {
                      if (typeof data4 === "string") {
                        if (!formats10.test(data4)) {
                          validate24.errors = [
                            {
                              instancePath: instancePath + "/release_list",
                              schemaPath: "#/properties/release_list/format",
                              keyword: "format",
                              params: { format: "uri-template" },
                              message:
                                'must match format "' + "uri-template" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs12 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.release_list_notes !== undefined) {
                    let data5 = data.release_list_notes;
                    const _errs14 = errors;
                    const _errs15 = errors;
                    if (typeof data5 !== "string") {
                      let dataType5 = typeof data5;
                      let coerced5 = undefined;
                      if (!(coerced5 !== undefined)) {
                        if (dataType5 == "number" || dataType5 == "boolean") {
                          coerced5 = "" + data5;
                        } else if (data5 === null) {
                          coerced5 = "";
                        } else {
                          validate24.errors = [
                            {
                              instancePath:
                                instancePath + "/release_list_notes",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced5 !== undefined) {
                        data5 = coerced5;
                        if (data !== undefined) {
                          data["release_list_notes"] = coerced5;
                        }
                      }
                    }
                    if (errors === _errs15) {
                      if (typeof data5 === "string") {
                        if (func2(data5) > 512) {
                          validate24.errors = [
                            {
                              instancePath:
                                instancePath + "/release_list_notes",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 512 },
                              message: "must NOT have more than 512 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func2(data5) < 1) {
                            validate24.errors = [
                              {
                                instancePath:
                                  instancePath + "/release_list_notes",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.release !== undefined) {
                      let data6 = data.release;
                      const _errs17 = errors;
                      if (typeof data6 !== "string") {
                        let dataType6 = typeof data6;
                        let coerced6 = undefined;
                        if (!(coerced6 !== undefined)) {
                          if (dataType6 == "number" || dataType6 == "boolean") {
                            coerced6 = "" + data6;
                          } else if (data6 === null) {
                            coerced6 = "";
                          } else {
                            validate24.errors = [
                              {
                                instancePath: instancePath + "/release",
                                schemaPath: "#/properties/release/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced6 !== undefined) {
                          data6 = coerced6;
                          if (data !== undefined) {
                            data["release"] = coerced6;
                          }
                        }
                      }
                      if (errors === _errs17) {
                        if (errors === _errs17) {
                          if (typeof data6 === "string") {
                            if (!formats10.test(data6)) {
                              validate24.errors = [
                                {
                                  instancePath: instancePath + "/release",
                                  schemaPath: "#/properties/release/format",
                                  keyword: "format",
                                  params: { format: "uri-template" },
                                  message:
                                    'must match format "' +
                                    "uri-template" +
                                    '"',
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs17 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.release_notes !== undefined) {
                        let data7 = data.release_notes;
                        const _errs19 = errors;
                        const _errs20 = errors;
                        if (typeof data7 !== "string") {
                          let dataType7 = typeof data7;
                          let coerced7 = undefined;
                          if (!(coerced7 !== undefined)) {
                            if (
                              dataType7 == "number" ||
                              dataType7 == "boolean"
                            ) {
                              coerced7 = "" + data7;
                            } else if (data7 === null) {
                              coerced7 = "";
                            } else {
                              validate24.errors = [
                                {
                                  instancePath: instancePath + "/release_notes",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced7 !== undefined) {
                            data7 = coerced7;
                            if (data !== undefined) {
                              data["release_notes"] = coerced7;
                            }
                          }
                        }
                        if (errors === _errs20) {
                          if (typeof data7 === "string") {
                            if (func2(data7) > 512) {
                              validate24.errors = [
                                {
                                  instancePath: instancePath + "/release_notes",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 512 },
                                  message:
                                    "must NOT have more than 512 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func2(data7) < 1) {
                                validate24.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/release_notes",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid0 = _errs19 === errors;
                      } else {
                        var valid0 = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate24.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate24.errors = vErrors;
  return errors === 0;
}
validate24.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-import/site/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_at === undefined && (missing0 = "created_at")) ||
        (data.updated_at === undefined && (missing0 = "updated_at")) ||
        (data.category === undefined && (missing0 = "category")) ||
        (data.approval_status === undefined &&
          (missing0 = "approval_status")) ||
        (data.public_import === undefined && (missing0 = "public_import")) ||
        (data.public_id === undefined && (missing0 = "public_id")) ||
        (data.home_page === undefined && (missing0 = "home_page")) ||
        (data.title === undefined && (missing0 = "title"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!func1.call(schema31.properties, key0)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_at !== undefined) {
              let data1 = data.created_at;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_at",
                        schemaPath:
                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_at"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func2(data1) > 29) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/created_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 29 },
                          message: "must NOT have more than 29 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func2(data1) < 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 29 },
                            message: "must NOT have fewer than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (!formats0.validate(data1)) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/created_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                              keyword: "format",
                              params: { format: "date-time" },
                              message:
                                'must match format "' + "date-time" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.updated_at !== undefined) {
                let data2 = data.updated_at;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/updated_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["updated_at"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (errors === _errs9) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/updated_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 29 },
                            message: "must NOT have more than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 29) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/updated_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 29 },
                              message: "must NOT have fewer than 29 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.validate(data2)) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/updated_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                keyword: "format",
                                params: { format: "date-time" },
                                message:
                                  'must match format "' + "date-time" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.public_import !== undefined) {
                  const _errs11 = errors;
                  if (
                    !validate21(data.public_import, {
                      instancePath: instancePath + "/public_import",
                      parentData: data,
                      parentDataProperty: "public_import",
                      rootData,
                      dynamicAnchors,
                    })
                  ) {
                    vErrors =
                      vErrors === null
                        ? validate21.errors
                        : vErrors.concat(validate21.errors);
                    errors = vErrors.length;
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.category !== undefined) {
                    let data4 = data.category;
                    const _errs12 = errors;
                    const _errs14 = errors;
                    let valid5 = false;
                    const _errs15 = errors;
                    if ("new" !== data4) {
                      const err0 = {
                        instancePath: instancePath + "/category",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/0/const",
                        keyword: "const",
                        params: { allowedValue: "new" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err0];
                      } else {
                        vErrors.push(err0);
                      }
                      errors++;
                    }
                    var _valid0 = _errs15 === errors;
                    valid5 = valid5 || _valid0;
                    const _errs16 = errors;
                    if ("same" !== data4) {
                      const err1 = {
                        instancePath: instancePath + "/category",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/1/const",
                        keyword: "const",
                        params: { allowedValue: "same" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err1];
                      } else {
                        vErrors.push(err1);
                      }
                      errors++;
                    }
                    var _valid0 = _errs16 === errors;
                    valid5 = valid5 || _valid0;
                    const _errs17 = errors;
                    if ("different" !== data4) {
                      const err2 = {
                        instancePath: instancePath + "/category",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf/2/const",
                        keyword: "const",
                        params: { allowedValue: "different" },
                        message: "must be equal to constant",
                      };
                      if (vErrors === null) {
                        vErrors = [err2];
                      } else {
                        vErrors.push(err2);
                      }
                      errors++;
                    }
                    var _valid0 = _errs17 === errors;
                    valid5 = valid5 || _valid0;
                    if (!valid5) {
                      const err3 = {
                        instancePath: instancePath + "/category",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/public-import/site/category.schema.json/anyOf",
                        keyword: "anyOf",
                        params: {},
                        message: "must match a schema in anyOf",
                      };
                      if (vErrors === null) {
                        vErrors = [err3];
                      } else {
                        vErrors.push(err3);
                      }
                      errors++;
                      validate20.errors = vErrors;
                      return false;
                    } else {
                      errors = _errs14;
                      if (vErrors !== null) {
                        if (_errs14) {
                          vErrors.length = _errs14;
                        } else {
                          vErrors = null;
                        }
                      }
                    }
                    var valid0 = _errs12 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.approval_status !== undefined) {
                      let data5 = data.approval_status;
                      const _errs18 = errors;
                      const _errs20 = errors;
                      let valid7 = false;
                      const _errs21 = errors;
                      if ("undecided" !== data5) {
                        const err4 = {
                          instancePath: instancePath + "/approval_status",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/0/const",
                          keyword: "const",
                          params: { allowedValue: "undecided" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err4];
                        } else {
                          vErrors.push(err4);
                        }
                        errors++;
                      }
                      var _valid1 = _errs21 === errors;
                      valid7 = valid7 || _valid1;
                      const _errs22 = errors;
                      if ("approved" !== data5) {
                        const err5 = {
                          instancePath: instancePath + "/approval_status",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/1/const",
                          keyword: "const",
                          params: { allowedValue: "approved" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err5];
                        } else {
                          vErrors.push(err5);
                        }
                        errors++;
                      }
                      var _valid1 = _errs22 === errors;
                      valid7 = valid7 || _valid1;
                      const _errs23 = errors;
                      if ("rejected" !== data5) {
                        const err6 = {
                          instancePath: instancePath + "/approval_status",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf/2/const",
                          keyword: "const",
                          params: { allowedValue: "rejected" },
                          message: "must be equal to constant",
                        };
                        if (vErrors === null) {
                          vErrors = [err6];
                        } else {
                          vErrors.push(err6);
                        }
                        errors++;
                      }
                      var _valid1 = _errs23 === errors;
                      valid7 = valid7 || _valid1;
                      if (!valid7) {
                        const err7 = {
                          instancePath: instancePath + "/approval_status",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json/anyOf",
                          keyword: "anyOf",
                          params: {},
                          message: "must match a schema in anyOf",
                        };
                        if (vErrors === null) {
                          vErrors = [err7];
                        } else {
                          vErrors.push(err7);
                        }
                        errors++;
                        validate20.errors = vErrors;
                        return false;
                      } else {
                        errors = _errs20;
                        if (vErrors !== null) {
                          if (_errs20) {
                            vErrors.length = _errs20;
                          } else {
                            vErrors = null;
                          }
                        }
                      }
                      var valid0 = _errs18 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.public_id !== undefined) {
                        let data6 = data.public_id;
                        const _errs24 = errors;
                        const _errs25 = errors;
                        if (typeof data6 !== "string") {
                          let dataType3 = typeof data6;
                          let coerced3 = undefined;
                          if (!(coerced3 !== undefined)) {
                            if (
                              dataType3 == "number" ||
                              dataType3 == "boolean"
                            ) {
                              coerced3 = "" + data6;
                            } else if (data6 === null) {
                              coerced3 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/public_id",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced3 !== undefined) {
                            data6 = coerced3;
                            if (data !== undefined) {
                              data["public_id"] = coerced3;
                            }
                          }
                        }
                        if (errors === _errs25) {
                          if (errors === _errs25) {
                            if (typeof data6 === "string") {
                              if (func2(data6) > 36) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/public_id",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 36 },
                                    message:
                                      "must NOT have more than 36 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func2(data6) < 36) {
                                  validate20.errors = [
                                    {
                                      instancePath: instancePath + "/public_id",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 36 },
                                      message:
                                        "must NOT have fewer than 36 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (!formats4.test(data6)) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/public_id",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                        keyword: "format",
                                        params: { format: "uuid" },
                                        message:
                                          'must match format "' + "uuid" + '"',
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                          }
                        }
                        var valid0 = _errs24 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.local_site !== undefined) {
                          const _errs27 = errors;
                          if (
                            !validate21(data.local_site, {
                              instancePath: instancePath + "/local_site",
                              parentData: data,
                              parentDataProperty: "local_site",
                              rootData,
                              dynamicAnchors,
                            })
                          ) {
                            vErrors =
                              vErrors === null
                                ? validate21.errors
                                : vErrors.concat(validate21.errors);
                            errors = vErrors.length;
                          }
                          var valid0 = _errs27 === errors;
                        } else {
                          var valid0 = true;
                        }
                        if (valid0) {
                          if (data.home_page !== undefined) {
                            let data8 = data.home_page;
                            const _errs28 = errors;
                            if (typeof data8 !== "string") {
                              let dataType4 = typeof data8;
                              let coerced4 = undefined;
                              if (!(coerced4 !== undefined)) {
                                if (
                                  dataType4 == "number" ||
                                  dataType4 == "boolean"
                                ) {
                                  coerced4 = "" + data8;
                                } else if (data8 === null) {
                                  coerced4 = "";
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath: instancePath + "/home_page",
                                      schemaPath: "#/properties/home_page/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced4 !== undefined) {
                                data8 = coerced4;
                                if (data !== undefined) {
                                  data["home_page"] = coerced4;
                                }
                              }
                            }
                            if (errors === _errs28) {
                              if (errors === _errs28) {
                                if (typeof data8 === "string") {
                                  if (!formats8(data8)) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/home_page",
                                        schemaPath:
                                          "#/properties/home_page/format",
                                        keyword: "format",
                                        params: { format: "uri" },
                                        message:
                                          'must match format "' + "uri" + '"',
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                            var valid0 = _errs28 === errors;
                          } else {
                            var valid0 = true;
                          }
                          if (valid0) {
                            if (data.title !== undefined) {
                              let data9 = data.title;
                              const _errs30 = errors;
                              const _errs31 = errors;
                              if (typeof data9 !== "string") {
                                let dataType5 = typeof data9;
                                let coerced5 = undefined;
                                if (!(coerced5 !== undefined)) {
                                  if (
                                    dataType5 == "number" ||
                                    dataType5 == "boolean"
                                  ) {
                                    coerced5 = "" + data9;
                                  } else if (data9 === null) {
                                    coerced5 = "";
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath: instancePath + "/title",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced5 !== undefined) {
                                  data9 = coerced5;
                                  if (data !== undefined) {
                                    data["title"] = coerced5;
                                  }
                                }
                              }
                              if (errors === _errs31) {
                                if (typeof data9 === "string") {
                                  if (func2(data9) > 216) {
                                    validate20.errors = [
                                      {
                                        instancePath: instancePath + "/title",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 216 },
                                        message:
                                          "must NOT have more than 216 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func2(data9) < 1) {
                                      validate20.errors = [
                                        {
                                          instancePath: instancePath + "/title",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                              var valid0 = _errs30 === errors;
                            } else {
                              var valid0 = true;
                            }
                            if (valid0) {
                              if (data.name !== undefined) {
                                let data10 = data.name;
                                const _errs33 = errors;
                                const _errs34 = errors;
                                if (typeof data10 !== "string") {
                                  let dataType6 = typeof data10;
                                  let coerced6 = undefined;
                                  if (!(coerced6 !== undefined)) {
                                    if (
                                      dataType6 == "number" ||
                                      dataType6 == "boolean"
                                    ) {
                                      coerced6 = "" + data10;
                                    } else if (data10 === null) {
                                      coerced6 = "";
                                    } else {
                                      validate20.errors = [
                                        {
                                          instancePath: instancePath + "/name",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                                          keyword: "type",
                                          params: { type: "string" },
                                          message: "must be string",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                  if (coerced6 !== undefined) {
                                    data10 = coerced6;
                                    if (data !== undefined) {
                                      data["name"] = coerced6;
                                    }
                                  }
                                }
                                if (errors === _errs34) {
                                  if (typeof data10 === "string") {
                                    if (func2(data10) > 216) {
                                      validate20.errors = [
                                        {
                                          instancePath: instancePath + "/name",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                                          keyword: "maxLength",
                                          params: { limit: 216 },
                                          message:
                                            "must NOT have more than 216 characters",
                                        },
                                      ];
                                      return false;
                                    } else {
                                      if (func2(data10) < 1) {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/name",
                                            schemaPath:
                                              "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                            keyword: "minLength",
                                            params: { limit: 1 },
                                            message:
                                              "must NOT have fewer than 1 characters",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                  }
                                }
                                var valid0 = _errs33 === errors;
                              } else {
                                var valid0 = true;
                              }
                              if (valid0) {
                                if (data.long_title !== undefined) {
                                  let data11 = data.long_title;
                                  const _errs36 = errors;
                                  const _errs37 = errors;
                                  if (typeof data11 !== "string") {
                                    let dataType7 = typeof data11;
                                    let coerced7 = undefined;
                                    if (!(coerced7 !== undefined)) {
                                      if (
                                        dataType7 == "number" ||
                                        dataType7 == "boolean"
                                      ) {
                                        coerced7 = "" + data11;
                                      } else if (data11 === null) {
                                        coerced7 = "";
                                      } else {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/long_title",
                                            schemaPath:
                                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                                            keyword: "type",
                                            params: { type: "string" },
                                            message: "must be string",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                    if (coerced7 !== undefined) {
                                      data11 = coerced7;
                                      if (data !== undefined) {
                                        data["long_title"] = coerced7;
                                      }
                                    }
                                  }
                                  if (errors === _errs37) {
                                    if (typeof data11 === "string") {
                                      if (func2(data11) < 1) {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/long_title",
                                            schemaPath:
                                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                                            keyword: "minLength",
                                            params: { limit: 1 },
                                            message:
                                              "must NOT have fewer than 1 characters",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                  }
                                  var valid0 = _errs36 === errors;
                                } else {
                                  var valid0 = true;
                                }
                                if (valid0) {
                                  if (data.description !== undefined) {
                                    let data12 = data.description;
                                    const _errs39 = errors;
                                    const _errs40 = errors;
                                    if (typeof data12 !== "string") {
                                      let dataType8 = typeof data12;
                                      let coerced8 = undefined;
                                      if (!(coerced8 !== undefined)) {
                                        if (
                                          dataType8 == "number" ||
                                          dataType8 == "boolean"
                                        ) {
                                          coerced8 = "" + data12;
                                        } else if (data12 === null) {
                                          coerced8 = "";
                                        } else {
                                          validate20.errors = [
                                            {
                                              instancePath:
                                                instancePath + "/description",
                                              schemaPath:
                                                "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                                              keyword: "type",
                                              params: { type: "string" },
                                              message: "must be string",
                                            },
                                          ];
                                          return false;
                                        }
                                      }
                                      if (coerced8 !== undefined) {
                                        data12 = coerced8;
                                        if (data !== undefined) {
                                          data["description"] = coerced8;
                                        }
                                      }
                                    }
                                    if (errors === _errs40) {
                                      if (typeof data12 === "string") {
                                        if (func2(data12) < 1) {
                                          validate20.errors = [
                                            {
                                              instancePath:
                                                instancePath + "/description",
                                              schemaPath:
                                                "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                                              keyword: "minLength",
                                              params: { limit: 1 },
                                              message:
                                                "must NOT have fewer than 1 characters",
                                            },
                                          ];
                                          return false;
                                        }
                                      }
                                    }
                                    var valid0 = _errs39 === errors;
                                  } else {
                                    var valid0 = true;
                                  }
                                  if (valid0) {
                                    if (data.url_templates !== undefined) {
                                      const _errs42 = errors;
                                      if (
                                        !validate24(data.url_templates, {
                                          instancePath:
                                            instancePath + "/url_templates",
                                          parentData: data,
                                          parentDataProperty: "url_templates",
                                          rootData,
                                          dynamicAnchors,
                                        })
                                      ) {
                                        vErrors =
                                          vErrors === null
                                            ? validate24.errors
                                            : vErrors.concat(validate24.errors);
                                        errors = vErrors.length;
                                      }
                                      var valid0 = _errs42 === errors;
                                    } else {
                                      var valid0 = true;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
