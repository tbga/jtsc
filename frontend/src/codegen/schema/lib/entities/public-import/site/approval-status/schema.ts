/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportSiteApprovalStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
  title: "PublicImportSiteApprovalStatus",
  description: "Approval status of public import's site.",
  anyOf: [
    {
      const: "undecided",
      description: "There was no approval decision yet.",
    },
    {
      const: "approved",
      description: "The site is approved for public import.",
    },
    {
      const: "rejected",
      description: "The site is rejected from public import.",
    },
  ],
} as const;
