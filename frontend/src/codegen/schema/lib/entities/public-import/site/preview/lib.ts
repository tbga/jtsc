/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportSitePreviewSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportSitePreview } from "./types";
export const validatePublicImportSitePreview =
  createValidator<IPublicImportSitePreview>(
    validate as ValidateFunction<IPublicImportSitePreview>,
    publicImportSitePreviewSchema as unknown as IJSONSchema
  );
