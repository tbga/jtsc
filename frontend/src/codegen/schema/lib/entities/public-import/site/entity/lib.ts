/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportSiteSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportSite } from "./types";
export const validatePublicImportSite = createValidator<IPublicImportSite>(
  validate as ValidateFunction<IPublicImportSite>,
  publicImportSiteSchema as unknown as IJSONSchema
);
