/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Approval status of public import's site.
 */
export type IPublicImportSiteApprovalStatus =
  | "undecided"
  | "approved"
  | "rejected";
