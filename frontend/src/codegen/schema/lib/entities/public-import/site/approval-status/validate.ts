/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
  title: "PublicImportSiteApprovalStatus",
  description: "Approval status of public import's site.",
  anyOf: [
    { const: "undecided", description: "There was no approval decision yet." },
    {
      const: "approved",
      description: "The site is approved for public import.",
    },
    {
      const: "rejected",
      description: "The site is rejected from public import.",
    },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  const _errs0 = errors;
  let valid0 = false;
  const _errs1 = errors;
  if ("undecided" !== data) {
    const err0 = {
      instancePath,
      schemaPath: "#/anyOf/0/const",
      keyword: "const",
      params: { allowedValue: "undecided" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err0];
    } else {
      vErrors.push(err0);
    }
    errors++;
  }
  var _valid0 = _errs1 === errors;
  valid0 = valid0 || _valid0;
  const _errs2 = errors;
  if ("approved" !== data) {
    const err1 = {
      instancePath,
      schemaPath: "#/anyOf/1/const",
      keyword: "const",
      params: { allowedValue: "approved" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err1];
    } else {
      vErrors.push(err1);
    }
    errors++;
  }
  var _valid0 = _errs2 === errors;
  valid0 = valid0 || _valid0;
  const _errs3 = errors;
  if ("rejected" !== data) {
    const err2 = {
      instancePath,
      schemaPath: "#/anyOf/2/const",
      keyword: "const",
      params: { allowedValue: "rejected" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err2];
    } else {
      vErrors.push(err2);
    }
    errors++;
  }
  var _valid0 = _errs3 === errors;
  valid0 = valid0 || _valid0;
  if (!valid0) {
    const err3 = {
      instancePath,
      schemaPath: "#/anyOf",
      keyword: "anyOf",
      params: {},
      message: "must match a schema in anyOf",
    };
    if (vErrors === null) {
      vErrors = [err3];
    } else {
      vErrors.push(err3);
    }
    errors++;
    validate20.errors = vErrors;
    return false;
  } else {
    errors = _errs0;
    if (vErrors !== null) {
      if (_errs0) {
        vErrors.length = _errs0;
      } else {
        vErrors = null;
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
