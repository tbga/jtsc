/*
 * This file was generated automatically, do not edit it by hand.
 */

export type IPublicImportSiteCategoryAPI = "all" | "new" | "same" | "different";
