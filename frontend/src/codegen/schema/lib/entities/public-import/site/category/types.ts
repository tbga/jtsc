/*
 * This file was generated automatically, do not edit it by hand.
 */

export type IPublicImportSiteCategory = "new" | "same" | "different";
