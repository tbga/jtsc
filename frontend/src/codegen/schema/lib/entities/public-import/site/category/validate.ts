/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
  title: "PublicImportSiteCategory",
  anyOf: [
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-import/site/category.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  const _errs0 = errors;
  let valid0 = false;
  const _errs1 = errors;
  if ("new" !== data) {
    const err0 = {
      instancePath,
      schemaPath: "#/anyOf/0/const",
      keyword: "const",
      params: { allowedValue: "new" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err0];
    } else {
      vErrors.push(err0);
    }
    errors++;
  }
  var _valid0 = _errs1 === errors;
  valid0 = valid0 || _valid0;
  const _errs2 = errors;
  if ("same" !== data) {
    const err1 = {
      instancePath,
      schemaPath: "#/anyOf/1/const",
      keyword: "const",
      params: { allowedValue: "same" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err1];
    } else {
      vErrors.push(err1);
    }
    errors++;
  }
  var _valid0 = _errs2 === errors;
  valid0 = valid0 || _valid0;
  const _errs3 = errors;
  if ("different" !== data) {
    const err2 = {
      instancePath,
      schemaPath: "#/anyOf/2/const",
      keyword: "const",
      params: { allowedValue: "different" },
      message: "must be equal to constant",
    };
    if (vErrors === null) {
      vErrors = [err2];
    } else {
      vErrors.push(err2);
    }
    errors++;
  }
  var _valid0 = _errs3 === errors;
  valid0 = valid0 || _valid0;
  if (!valid0) {
    const err3 = {
      instancePath,
      schemaPath: "#/anyOf",
      keyword: "anyOf",
      params: {},
      message: "must match a schema in anyOf",
    };
    if (vErrors === null) {
      vErrors = [err3];
    } else {
      vErrors.push(err3);
    }
    errors++;
    validate20.errors = vErrors;
    return false;
  } else {
    errors = _errs0;
    if (vErrors !== null) {
      if (_errs0) {
        vErrors.length = _errs0;
      } else {
        vErrors = null;
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
