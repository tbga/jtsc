/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportSitePreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/preview.schema.json",
  title: "PublicImportSitePreview",
  description: "A preview of the public import site.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "public_import",
    "category",
    "approval_status",
    "public_id",
    "title",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    public_import: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    local_site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    category: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
    },
    approval_status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
