/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportSiteCategorySchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
  title: "PublicImportSiteCategory",
  anyOf: [
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
} as const;
