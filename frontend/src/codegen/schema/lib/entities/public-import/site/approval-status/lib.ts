/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportSiteApprovalStatusSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportSiteApprovalStatus } from "./types";
export const validatePublicImportSiteApprovalStatus =
  createValidator<IPublicImportSiteApprovalStatus>(
    validate as ValidateFunction<IPublicImportSiteApprovalStatus>,
    publicImportSiteApprovalStatusSchema as unknown as IJSONSchema
  );
