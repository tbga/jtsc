/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportSiteCategoryAPISchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category-api.schema.json",
  title: "PublicImportSiteCategoryAPI",
  anyOf: [
    {
      description: "All sites of the import.",
      const: "all",
    },
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
} as const;
