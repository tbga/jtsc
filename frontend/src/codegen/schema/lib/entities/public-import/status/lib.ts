/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { publicImportStatusSchema } from "./schema";
import { validate } from "./validate";
import { IPublicImportStatus } from "./types";
export const validatePublicImportStatus = createValidator<IPublicImportStatus>(
  validate as ValidateFunction<IPublicImportStatus>,
  publicImportStatusSchema as unknown as IJSONSchema
);
