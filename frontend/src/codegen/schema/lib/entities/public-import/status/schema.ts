/*
 * This file was generated automatically, do not edit it by hand.
 */
export const publicImportStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/status.schema.json",
  title: "PublicImportStatus",
  description: "Status of the public import.",
  anyOf: [
    {
      const: "pending",
      description: "Public import is pending.",
    },
    {
      const: "in-progress",
      description: "Public import is being consumed.",
    },
    {
      const: "consumed",
      description: "Public import is consumed.",
    },
  ],
} as const;
