/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Status of the public import.
 */
export type IPublicImportStatus = "pending" | "in-progress" | "consumed";
