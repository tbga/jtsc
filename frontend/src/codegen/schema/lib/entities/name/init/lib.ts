/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { nameInitSchema } from "./schema";
import { validate } from "./validate";
import { INameInit } from "./types";
export const validateNameInit = createValidator<INameInit>(
  validate as ValidateFunction<INameInit>,
  nameInitSchema as unknown as IJSONSchema
);
