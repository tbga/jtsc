/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Name initializer.
 */
export interface INameInit {
  /**
   * The complete name of the name entry. The limit is the longest name as per [guinness](https://www.guinnessworldrecords.com/world-records/67285-longest-personal-name).
   */
  full_name: string;
}
