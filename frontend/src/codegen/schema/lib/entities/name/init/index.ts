/*
 * This file was generated automatically, do not edit it by hand.
 */
export { nameInitSchema } from "./schema";
export { validateNameInit } from "./lib";
export type { INameInit } from "./types";
