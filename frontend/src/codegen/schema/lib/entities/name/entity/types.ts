/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";

/**
 * A container to hold as much name information as possible.
 */
export interface IName {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  /**
   * The complete name of the name entry.
   */
  full_name: string;
}
