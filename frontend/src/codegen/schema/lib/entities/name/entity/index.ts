/*
 * This file was generated automatically, do not edit it by hand.
 */
export { nameSchema } from "./schema";
export { validateName } from "./lib";
export type { IName } from "./types";
