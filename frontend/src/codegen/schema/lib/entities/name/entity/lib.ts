/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { nameSchema } from "./schema";
import { validate } from "./validate";
import { IName } from "./types";
export const validateName = createValidator<IName>(
  validate as ValidateFunction<IName>,
  nameSchema as unknown as IJSONSchema
);
