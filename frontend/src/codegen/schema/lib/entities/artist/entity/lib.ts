/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { artistSchema } from "./schema";
import { validate } from "./validate";
import { IArtist } from "./types";
export const validateArtist = createValidator<IArtist>(
  validate as ValidateFunction<IArtist>,
  artistSchema as unknown as IJSONSchema
);
