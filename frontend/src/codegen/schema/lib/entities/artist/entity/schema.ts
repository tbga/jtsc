/*
 * This file was generated automatically, do not edit it by hand.
 */
export const artistSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/entity.schema.json",
  title: "Artist",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    sex: {
      type: "boolean",
    },
  },
} as const;
