/*
 * This file was generated automatically, do not edit it by hand.
 */
export { artistSchema } from "./schema";
export { validateArtist } from "./lib";
export type { IArtist } from "./types";
