/*
 * This file was generated automatically, do not edit it by hand.
 */
export const artistUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/update.schema.json",
  title: "ArtistUpdate",
  description: "An update on the artist entry.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
    },
  },
} as const;
