/*
 * This file was generated automatically, do not edit it by hand.
 */
export { artistUpdateSchema } from "./schema";
export { validateArtistUpdate } from "./lib";
export type { IArtistUpdate } from "./types";
