/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * An update on the artist entry.
 */
export interface IArtistUpdate {
  sex?: boolean;
}
