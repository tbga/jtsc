/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { artistUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IArtistUpdate } from "./types";
export const validateArtistUpdate = createValidator<IArtistUpdate>(
  validate as ValidateFunction<IArtistUpdate>,
  artistUpdateSchema as unknown as IJSONSchema
);
