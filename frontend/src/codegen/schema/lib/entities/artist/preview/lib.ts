/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { artistPreviewSchema } from "./schema";
import { validate } from "./validate";
import { IArtistPreview } from "./types";
export const validateArtistPreview = createValidator<IArtistPreview>(
  validate as ValidateFunction<IArtistPreview>,
  artistPreviewSchema as unknown as IJSONSchema
);
