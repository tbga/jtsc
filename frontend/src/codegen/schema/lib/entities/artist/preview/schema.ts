/*
 * This file was generated automatically, do not edit it by hand.
 */
export const artistPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/preview.schema.json",
  title: "ArtistPreview",
  type: "object",
  additionalProperties: false,
  required: ["id", "sites", "profiles", "posts", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    sex: {
      type: "boolean",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
