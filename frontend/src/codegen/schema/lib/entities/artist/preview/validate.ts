/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/artist/preview.schema.json",
  title: "ArtistPreview",
  type: "object",
  additionalProperties: false,
  required: ["id", "sites", "profiles", "posts", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
    sex: { type: "boolean" },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/artist/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.sites === undefined && (missing0 = "sites")) ||
        (data.profiles === undefined && (missing0 = "profiles")) ||
        (data.posts === undefined && (missing0 = "posts")) ||
        (data.releases === undefined && (missing0 = "releases"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "name" ||
              key0 === "sex" ||
              key0 === "sites" ||
              key0 === "profiles" ||
              key0 === "posts" ||
              key0 === "releases"
            )
          ) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.sex !== undefined) {
                let data2 = data.sex;
                const _errs8 = errors;
                if (typeof data2 !== "boolean") {
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (data2 === "false" || data2 === 0 || data2 === null) {
                      coerced2 = false;
                    } else if (data2 === "true" || data2 === 1) {
                      coerced2 = true;
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/sex",
                          schemaPath: "#/properties/sex/type",
                          keyword: "type",
                          params: { type: "boolean" },
                          message: "must be boolean",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["sex"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.sites !== undefined) {
                  let data3 = data.sites;
                  const _errs10 = errors;
                  const _errs11 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/sites",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["sites"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs11) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 20) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/sites",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 20 },
                            message: "must NOT have more than 20 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/sites",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs10 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.profiles !== undefined) {
                    let data4 = data.profiles;
                    const _errs13 = errors;
                    const _errs14 = errors;
                    if (typeof data4 !== "string") {
                      let dataType4 = typeof data4;
                      let coerced4 = undefined;
                      if (!(coerced4 !== undefined)) {
                        if (dataType4 == "number" || dataType4 == "boolean") {
                          coerced4 = "" + data4;
                        } else if (data4 === null) {
                          coerced4 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/profiles",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced4 !== undefined) {
                        data4 = coerced4;
                        if (data !== undefined) {
                          data["profiles"] = coerced4;
                        }
                      }
                    }
                    if (errors === _errs14) {
                      if (typeof data4 === "string") {
                        if (func1(data4) > 20) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/profiles",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 20 },
                              message: "must NOT have more than 20 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data4) < 1) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/profiles",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs13 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.posts !== undefined) {
                      let data5 = data.posts;
                      const _errs16 = errors;
                      const _errs17 = errors;
                      if (typeof data5 !== "string") {
                        let dataType5 = typeof data5;
                        let coerced5 = undefined;
                        if (!(coerced5 !== undefined)) {
                          if (dataType5 == "number" || dataType5 == "boolean") {
                            coerced5 = "" + data5;
                          } else if (data5 === null) {
                            coerced5 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/posts",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced5 !== undefined) {
                          data5 = coerced5;
                          if (data !== undefined) {
                            data["posts"] = coerced5;
                          }
                        }
                      }
                      if (errors === _errs17) {
                        if (typeof data5 === "string") {
                          if (func1(data5) > 20) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/posts",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 20 },
                                message:
                                  "must NOT have more than 20 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data5) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/posts",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs16 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.releases !== undefined) {
                        let data6 = data.releases;
                        const _errs19 = errors;
                        const _errs20 = errors;
                        if (typeof data6 !== "string") {
                          let dataType6 = typeof data6;
                          let coerced6 = undefined;
                          if (!(coerced6 !== undefined)) {
                            if (
                              dataType6 == "number" ||
                              dataType6 == "boolean"
                            ) {
                              coerced6 = "" + data6;
                            } else if (data6 === null) {
                              coerced6 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/releases",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced6 !== undefined) {
                            data6 = coerced6;
                            if (data !== undefined) {
                              data["releases"] = coerced6;
                            }
                          }
                        }
                        if (errors === _errs20) {
                          if (typeof data6 === "string") {
                            if (func1(data6) > 20) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/releases",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 20 },
                                  message:
                                    "must NOT have more than 20 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data6) < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/releases",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid0 = _errs19 === errors;
                      } else {
                        var valid0 = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
