/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { artistInitSchema } from "./schema";
import { validate } from "./validate";
import { IArtistInit } from "./types";
export const validateArtistInit = createValidator<IArtistInit>(
  validate as ValidateFunction<IArtistInit>,
  artistInitSchema as unknown as IJSONSchema
);
