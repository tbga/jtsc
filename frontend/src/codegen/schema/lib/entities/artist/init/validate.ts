/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/artist/init.schema.json",
  title: "ArtistInit",
  description: "Initializer for the artist.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
      description:
        "Is optional because the artist can hide it by using pseudonyms.",
    },
  },
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/artist/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!(key0 === "sex")) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.sex !== undefined) {
          let data0 = data.sex;
          if (typeof data0 !== "boolean") {
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (data0 === "false" || data0 === 0 || data0 === null) {
                coerced0 = false;
              } else if (data0 === "true" || data0 === 1) {
                coerced0 = true;
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/sex",
                    schemaPath: "#/properties/sex/type",
                    keyword: "type",
                    params: { type: "boolean" },
                    message: "must be boolean",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["sex"] = coerced0;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
