/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Initializer for the artist.
 */
export interface IArtistInit {
  /**
   * Is optional because the artist can hide it by using pseudonyms.
   */
  sex?: boolean;
}
