/*
 * This file was generated automatically, do not edit it by hand.
 */
export { artistInitSchema } from "./schema";
export { validateArtistInit } from "./lib";
export type { IArtistInit } from "./types";
