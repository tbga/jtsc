/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";

export interface ISitePreview {
  id: IBigSerialInteger;
  /**
   * The site's home page.
   */
  home_page: string;
  /**
   * The title used by the site. Most likely prepended/appended in the <title> tag.
   */
  title: string;
  name: INonEmptyString;
  profiles: IBigIntegerPositive;
  releases: IBigIntegerPositive;
  public_id?: IUUID;
  published_at?: IDateTime;
}
