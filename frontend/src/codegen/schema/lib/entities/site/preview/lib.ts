/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { sitePreviewSchema } from "./schema";
import { validate } from "./validate";
import { ISitePreview } from "./types";
export const validateSitePreview = createValidator<ISitePreview>(
  validate as ValidateFunction<ISitePreview>,
  sitePreviewSchema as unknown as IJSONSchema
);
