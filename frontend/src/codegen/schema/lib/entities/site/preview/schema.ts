/*
 * This file was generated automatically, do not edit it by hand.
 */
export const sitePreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/site/preview.schema.json",
  title: "SitePreview",
  type: "object",
  additionalProperties: false,
  required: ["id", "home_page", "title", "name", "profiles", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      type: "string",
      description:
        "The title used by the site. Most likely prepended/appended in the <title> tag.",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    published_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
  },
} as const;
