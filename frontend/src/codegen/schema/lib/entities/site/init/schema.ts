/*
 * This file was generated automatically, do not edit it by hand.
 */
export const siteInitSchema = {
  $id: "https://jtsc-schemas.org/entities/site/init.schema.json",
  title: "SiteInit",
  description: "Site initializer.",
  type: "object",
  additionalProperties: false,
  required: ["home_page", "title"],
  properties: {
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      type: "string",
      description:
        "The title used by the site. Most likely prepended/appended in the <title> tag.",
    },
    long_title: {
      type: "string",
      description:
        "Long title, most likely a follow up to the main title on the home page.",
    },
    description: {
      type: "string",
      description:
        "A description of the site, most likely to be a follow up to the long title on the home page.",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
} as const;
