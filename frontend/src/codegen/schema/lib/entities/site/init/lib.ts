/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { siteInitSchema } from "./schema";
import { validate } from "./validate";
import { ISiteInit } from "./types";
export const validateSiteInit = createValidator<ISiteInit>(
  validate as ValidateFunction<ISiteInit>,
  siteInitSchema as unknown as IJSONSchema
);
