/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type ISiteURLTemplates } from "#codegen/schema/lib/entities/site/url-templates";

export interface ISite {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  /**
   * The site's home page.
   */
  home_page: string;
  title: INonEmptyString;
  name: INonEmptyString;
  /**
   * Long title, most likely a follow up to the main title on the home page.
   */
  long_title?: string;
  /**
   * A description of the site, most likely to be a follow up to the long title on the home page.
   */
  description?: string;
  public_id?: IUUID;
  published_at?: IDateTime;
  published_by?: IBigSerialInteger;
  url_templates?: ISiteURLTemplates;
}
