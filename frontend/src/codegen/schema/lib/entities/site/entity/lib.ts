/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { siteSchema } from "./schema";
import { validate } from "./validate";
import { ISite } from "./types";
export const validateSite = createValidator<ISite>(
  validate as ValidateFunction<ISite>,
  siteSchema as unknown as IJSONSchema
);
