/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { siteUpdateSchema } from "./schema";
import { validate } from "./validate";
import { ISiteUpdate } from "./types";
export const validateSiteUpdate = createValidator<ISiteUpdate>(
  validate as ValidateFunction<ISiteUpdate>,
  siteUpdateSchema as unknown as IJSONSchema
);
