/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISiteURLTemplates } from "#codegen/schema/lib/entities/site/url-templates";

export interface ISiteUpdate {
  /**
   * The site's home page.
   */
  home_page?: string;
  /**
   * The title used by the site. Most likely prepended/appended in the <title> tag.
   */
  title?: string;
  /**
   * Long title, most likely a follow up to the main title on the home page.
   */
  long_title?: string;
  /**
   * A description of the site, most likely to be a follow up to the long title on the home page.
   */
  description?: string;
  url_templates?: ISiteURLTemplates;
}
