/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { siteURLTemplatesSchema } from "./schema";
import { validate } from "./validate";
import { ISiteURLTemplates } from "./types";
export const validateSiteURLTemplates = createValidator<ISiteURLTemplates>(
  validate as ValidateFunction<ISiteURLTemplates>,
  siteURLTemplatesSchema as unknown as IJSONSchema
);
