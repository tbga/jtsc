/*
 * This file was generated automatically, do not edit it by hand.
 */
export { siteURLTemplatesSchema } from "./schema";
export { validateSiteURLTemplates } from "./lib";
export type { ISiteURLTemplates } from "./types";
