/*
 * This file was generated automatically, do not edit it by hand.
 */
export const fileSchema = {
  $id: "https://jtsc-schemas.org/entities/file/entity.schema.json",
  title: "File",
  description: "The file on the resource.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "url",
    "name",
    "type",
    "subtype",
    "size",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      description: "The name of the file.",
      type: "string",
      minLength: 1,
    },
    url: {
      description: "A url of the file.",
      type: "string",
      minLength: 1,
    },
    type: {
      description: "Media type.",
      type: "string",
      enum: [
        "application",
        "audio",
        "font",
        "image",
        "message",
        "model",
        "multipart",
        "text",
        "video",
      ],
    },
    subtype: {
      description: "Media subtype.",
      $comment: '@TODO make it dependant off the `"type"` key.',
      type: "string",
      minLength: 1,
    },
    size: {
      type: "string",
      description: "Size of the file in bytes.",
      minLength: 1,
    },
    hashes: {
      $ref: "https://jtsc-schemas.org/entities/file/hashes.schema.json",
    },
  },
} as const;
