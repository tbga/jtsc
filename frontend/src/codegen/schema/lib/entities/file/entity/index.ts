/*
 * This file was generated automatically, do not edit it by hand.
 */
export { fileSchema } from "./schema";
export { validateFile } from "./lib";
export type { IFile } from "./types";
