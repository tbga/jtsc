/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/file/entity.schema.json",
  title: "File",
  description: "The file on the resource.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "url",
    "name",
    "type",
    "subtype",
    "size",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      description: "The name of the file.",
      type: "string",
      minLength: 1,
    },
    url: { description: "A url of the file.", type: "string", minLength: 1 },
    type: {
      description: "Media type.",
      type: "string",
      enum: [
        "application",
        "audio",
        "font",
        "image",
        "message",
        "model",
        "multipart",
        "text",
        "video",
      ],
    },
    subtype: {
      description: "Media subtype.",
      $comment: '@TODO make it dependant off the `"type"` key.',
      type: "string",
      minLength: 1,
    },
    size: {
      type: "string",
      description: "Size of the file in bytes.",
      minLength: 1,
    },
    hashes: {
      $ref: "https://jtsc-schemas.org/entities/file/hashes.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const func1 = Object.prototype.hasOwnProperty;
const func2 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
const schema35 = {
  $id: "https://jtsc-schemas.org/entities/file/hashes.schema.json",
  title: "FileHashes",
  description: "The hashes of a file.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sha256: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
    },
    sha1: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
    },
    md5: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
    },
  },
};
const schema36 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
  title: "HashSHA256",
  description: "SHA256 hash string.",
  type: "string",
  minLength: 64,
  maxLength: 64,
};
const schema37 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
  title: "HashSHA1",
  description: "SHA1 hash string.",
  type: "string",
  minLength: 40,
  maxLength: 40,
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
  title: "HashMD5",
  description: "MD5 hash string.",
  type: "string",
  minLength: 32,
  maxLength: 32,
};
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/file/hashes.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!(key0 === "sha256" || key0 === "sha1" || key0 === "md5")) {
          validate21.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.sha256 !== undefined) {
          let data0 = data.sha256;
          const _errs2 = errors;
          const _errs3 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/sha256",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["sha256"] = coerced0;
              }
            }
          }
          if (errors === _errs3) {
            if (typeof data0 === "string") {
              if (func2(data0) > 64) {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/sha256",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/maxLength",
                    keyword: "maxLength",
                    params: { limit: 64 },
                    message: "must NOT have more than 64 characters",
                  },
                ];
                return false;
              } else {
                if (func2(data0) < 64) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/sha256",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/minLength",
                      keyword: "minLength",
                      params: { limit: 64 },
                      message: "must NOT have fewer than 64 characters",
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.sha1 !== undefined) {
            let data1 = data.sha1;
            const _errs5 = errors;
            const _errs6 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/sha1",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["sha1"] = coerced1;
                }
              }
            }
            if (errors === _errs6) {
              if (typeof data1 === "string") {
                if (func2(data1) > 40) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/sha1",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 40 },
                      message: "must NOT have more than 40 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data1) < 40) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/sha1",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 40 },
                        message: "must NOT have fewer than 40 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs5 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.md5 !== undefined) {
              let data2 = data.md5;
              const _errs8 = errors;
              const _errs9 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/md5",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["md5"] = coerced2;
                  }
                }
              }
              if (errors === _errs9) {
                if (typeof data2 === "string") {
                  if (func2(data2) > 32) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/md5",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 32 },
                        message: "must NOT have more than 32 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func2(data2) < 32) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/md5",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 32 },
                          message: "must NOT have fewer than 32 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs8 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/file/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_at === undefined && (missing0 = "created_at")) ||
        (data.updated_at === undefined && (missing0 = "updated_at")) ||
        (data.url === undefined && (missing0 = "url")) ||
        (data.name === undefined && (missing0 = "name")) ||
        (data.type === undefined && (missing0 = "type")) ||
        (data.subtype === undefined && (missing0 = "subtype")) ||
        (data.size === undefined && (missing0 = "size"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!func1.call(schema31.properties, key0)) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func2(data0) > 19) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func2(data0) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_at !== undefined) {
              let data1 = data.created_at;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/created_at",
                        schemaPath:
                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_at"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func2(data1) > 29) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/created_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 29 },
                          message: "must NOT have more than 29 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func2(data1) < 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/created_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 29 },
                            message: "must NOT have fewer than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (!formats0.validate(data1)) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/created_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                              keyword: "format",
                              params: { format: "date-time" },
                              message:
                                'must match format "' + "date-time" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.updated_at !== undefined) {
                let data2 = data.updated_at;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/updated_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["updated_at"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (errors === _errs9) {
                    if (typeof data2 === "string") {
                      if (func2(data2) > 29) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/updated_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 29 },
                            message: "must NOT have more than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func2(data2) < 29) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/updated_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 29 },
                              message: "must NOT have fewer than 29 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.validate(data2)) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/updated_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                keyword: "format",
                                params: { format: "date-time" },
                                message:
                                  'must match format "' + "date-time" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath: "#/properties/name/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs11) {
                    if (typeof data3 === "string") {
                      if (func2(data3) < 1) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath: "#/properties/name/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.url !== undefined) {
                    let data4 = data.url;
                    const _errs13 = errors;
                    if (typeof data4 !== "string") {
                      let dataType4 = typeof data4;
                      let coerced4 = undefined;
                      if (!(coerced4 !== undefined)) {
                        if (dataType4 == "number" || dataType4 == "boolean") {
                          coerced4 = "" + data4;
                        } else if (data4 === null) {
                          coerced4 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/url",
                              schemaPath: "#/properties/url/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced4 !== undefined) {
                        data4 = coerced4;
                        if (data !== undefined) {
                          data["url"] = coerced4;
                        }
                      }
                    }
                    if (errors === _errs13) {
                      if (typeof data4 === "string") {
                        if (func2(data4) < 1) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/url",
                              schemaPath: "#/properties/url/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                    var valid0 = _errs13 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.type !== undefined) {
                      let data5 = data.type;
                      const _errs15 = errors;
                      if (typeof data5 !== "string") {
                        let dataType5 = typeof data5;
                        let coerced5 = undefined;
                        if (!(coerced5 !== undefined)) {
                          if (dataType5 == "number" || dataType5 == "boolean") {
                            coerced5 = "" + data5;
                          } else if (data5 === null) {
                            coerced5 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/type",
                                schemaPath: "#/properties/type/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced5 !== undefined) {
                          data5 = coerced5;
                          if (data !== undefined) {
                            data["type"] = coerced5;
                          }
                        }
                      }
                      if (
                        !(
                          data5 === "application" ||
                          data5 === "audio" ||
                          data5 === "font" ||
                          data5 === "image" ||
                          data5 === "message" ||
                          data5 === "model" ||
                          data5 === "multipart" ||
                          data5 === "text" ||
                          data5 === "video"
                        )
                      ) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/type",
                            schemaPath: "#/properties/type/enum",
                            keyword: "enum",
                            params: {
                              allowedValues: schema31.properties.type.enum,
                            },
                            message:
                              "must be equal to one of the allowed values",
                          },
                        ];
                        return false;
                      }
                      var valid0 = _errs15 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.subtype !== undefined) {
                        let data6 = data.subtype;
                        const _errs17 = errors;
                        if (typeof data6 !== "string") {
                          let dataType6 = typeof data6;
                          let coerced6 = undefined;
                          if (!(coerced6 !== undefined)) {
                            if (
                              dataType6 == "number" ||
                              dataType6 == "boolean"
                            ) {
                              coerced6 = "" + data6;
                            } else if (data6 === null) {
                              coerced6 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/subtype",
                                  schemaPath: "#/properties/subtype/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced6 !== undefined) {
                            data6 = coerced6;
                            if (data !== undefined) {
                              data["subtype"] = coerced6;
                            }
                          }
                        }
                        if (errors === _errs17) {
                          if (typeof data6 === "string") {
                            if (func2(data6) < 1) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/subtype",
                                  schemaPath: "#/properties/subtype/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                        var valid0 = _errs17 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.size !== undefined) {
                          let data7 = data.size;
                          const _errs20 = errors;
                          if (typeof data7 !== "string") {
                            let dataType7 = typeof data7;
                            let coerced7 = undefined;
                            if (!(coerced7 !== undefined)) {
                              if (
                                dataType7 == "number" ||
                                dataType7 == "boolean"
                              ) {
                                coerced7 = "" + data7;
                              } else if (data7 === null) {
                                coerced7 = "";
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/size",
                                    schemaPath: "#/properties/size/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced7 !== undefined) {
                              data7 = coerced7;
                              if (data !== undefined) {
                                data["size"] = coerced7;
                              }
                            }
                          }
                          if (errors === _errs20) {
                            if (typeof data7 === "string") {
                              if (func2(data7) < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/size",
                                    schemaPath: "#/properties/size/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                          var valid0 = _errs20 === errors;
                        } else {
                          var valid0 = true;
                        }
                        if (valid0) {
                          if (data.hashes !== undefined) {
                            const _errs22 = errors;
                            if (
                              !validate21(data.hashes, {
                                instancePath: instancePath + "/hashes",
                                parentData: data,
                                parentDataProperty: "hashes",
                                rootData,
                                dynamicAnchors,
                              })
                            ) {
                              vErrors =
                                vErrors === null
                                  ? validate21.errors
                                  : vErrors.concat(validate21.errors);
                              errors = vErrors.length;
                            }
                            var valid0 = _errs22 === errors;
                          } else {
                            var valid0 = true;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
