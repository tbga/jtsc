/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { filePreviewSchema } from "./schema";
import { validate } from "./validate";
import { IFilePreview } from "./types";
export const validateFilePreview = createValidator<IFilePreview>(
  validate as ValidateFunction<IFilePreview>,
  filePreviewSchema as unknown as IJSONSchema
);
