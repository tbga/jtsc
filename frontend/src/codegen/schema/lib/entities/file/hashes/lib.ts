/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { fileHashesSchema } from "./schema";
import { validate } from "./validate";
import { IFileHashes } from "./types";
export const validateFileHashes = createValidator<IFileHashes>(
  validate as ValidateFunction<IFileHashes>,
  fileHashesSchema as unknown as IJSONSchema
);
