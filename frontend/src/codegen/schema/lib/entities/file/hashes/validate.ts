/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/entities/file/hashes.schema.json",
  title: "FileHashes",
  description: "The hashes of a file.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sha256: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
    },
    sha1: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
    },
    md5: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
  title: "HashSHA256",
  description: "SHA256 hash string.",
  type: "string",
  minLength: 64,
  maxLength: 64,
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
  title: "HashSHA1",
  description: "SHA1 hash string.",
  type: "string",
  minLength: 40,
  maxLength: 40,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
  title: "HashMD5",
  description: "MD5 hash string.",
  type: "string",
  minLength: 32,
  maxLength: 32,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/file/hashes.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!(key0 === "sha256" || key0 === "sha1" || key0 === "md5")) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.sha256 !== undefined) {
          let data0 = data.sha256;
          const _errs2 = errors;
          const _errs3 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/sha256",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["sha256"] = coerced0;
              }
            }
          }
          if (errors === _errs3) {
            if (typeof data0 === "string") {
              if (func1(data0) > 64) {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/sha256",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/maxLength",
                    keyword: "maxLength",
                    params: { limit: 64 },
                    message: "must NOT have more than 64 characters",
                  },
                ];
                return false;
              } else {
                if (func1(data0) < 64) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/sha256",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json/minLength",
                      keyword: "minLength",
                      params: { limit: 64 },
                      message: "must NOT have fewer than 64 characters",
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.sha1 !== undefined) {
            let data1 = data.sha1;
            const _errs5 = errors;
            const _errs6 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/sha1",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["sha1"] = coerced1;
                }
              }
            }
            if (errors === _errs6) {
              if (typeof data1 === "string") {
                if (func1(data1) > 40) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/sha1",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 40 },
                      message: "must NOT have more than 40 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data1) < 40) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/sha1",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 40 },
                        message: "must NOT have fewer than 40 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs5 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.md5 !== undefined) {
              let data2 = data.md5;
              const _errs8 = errors;
              const _errs9 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/md5",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["md5"] = coerced2;
                  }
                }
              }
              if (errors === _errs9) {
                if (typeof data2 === "string") {
                  if (func1(data2) > 32) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/md5",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 32 },
                        message: "must NOT have more than 32 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data2) < 32) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/md5",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 32 },
                          message: "must NOT have fewer than 32 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs8 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
