/*
 * This file was generated automatically, do not edit it by hand.
 */
export const fileHashesSchema = {
  $id: "https://jtsc-schemas.org/entities/file/hashes.schema.json",
  title: "FileHashes",
  description: "The hashes of a file.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sha256: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
    },
    sha1: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
    },
    md5: {
      $ref: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
    },
  },
} as const;
