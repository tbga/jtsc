/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { fileInitSchema } from "./schema";
import { validate } from "./validate";
import { IFileInit } from "./types";
export const validateFileInit = createValidator<IFileInit>(
  validate as ValidateFunction<IFileInit>,
  fileInitSchema as unknown as IJSONSchema
);
