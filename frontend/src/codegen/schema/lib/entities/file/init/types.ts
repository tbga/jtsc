/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPath } from "#codegen/schema/lib/local-file-system/path";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";

/**
 * File initializer.
 */
export interface IFileInit {
  local_path: IPath;
  name?: INonEmptyString;
}
