/*
 * This file was generated automatically, do not edit it by hand.
 */
export { fileInitSchema } from "./schema";
export { validateFileInit } from "./lib";
export type { IFileInit } from "./types";
