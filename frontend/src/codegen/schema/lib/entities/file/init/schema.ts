/*
 * This file was generated automatically, do not edit it by hand.
 */
export const fileInitSchema = {
  $id: "https://jtsc-schemas.org/entities/file/init.schema.json",
  title: "FileInit",
  description: "File initializer.",
  type: "object",
  additionalProperties: false,
  required: ["local_path"],
  properties: {
    local_path: {
      $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
  },
} as const;
