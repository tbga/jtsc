/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * The update of the file.
 */
export interface IFileUpdate {
  /**
   * The name of the file.
   */
  name: string;
}
