/*
 * This file was generated automatically, do not edit it by hand.
 */
export const fileUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/file/update.schema.json",
  title: "FileUpdate",
  description: "The update of the file.",
  type: "object",
  additionalProperties: false,
  required: ["name"],
  properties: {
    name: {
      description: "The name of the file.",
      type: "string",
    },
  },
} as const;
