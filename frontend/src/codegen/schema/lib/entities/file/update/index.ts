/*
 * This file was generated automatically, do not edit it by hand.
 */
export { fileUpdateSchema } from "./schema";
export { validateFileUpdate } from "./lib";
export type { IFileUpdate } from "./types";
