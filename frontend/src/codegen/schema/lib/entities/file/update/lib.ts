/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { fileUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IFileUpdate } from "./types";
export const validateFileUpdate = createValidator<IFileUpdate>(
  validate as ValidateFunction<IFileUpdate>,
  fileUpdateSchema as unknown as IJSONSchema
);
