/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/dates/datetime.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (typeof data !== "string") {
    let dataType0 = typeof data;
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (dataType0 == "number" || dataType0 == "boolean") {
        coerced0 = "" + data;
      } else if (data === null) {
        coerced0 = "";
      } else {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/type",
            keyword: "type",
            params: { type: "string" },
            message: "must be string",
          },
        ];
        return false;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  if (errors === 0) {
    if (errors === 0) {
      if (typeof data === "string") {
        if (func1(data) > 29) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/maxLength",
              keyword: "maxLength",
              params: { limit: 29 },
              message: "must NOT have more than 29 characters",
            },
          ];
          return false;
        } else {
          if (func1(data) < 29) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/minLength",
                keyword: "minLength",
                params: { limit: 29 },
                message: "must NOT have fewer than 29 characters",
              },
            ];
            return false;
          } else {
            if (!formats0.validate(data)) {
              validate20.errors = [
                {
                  instancePath,
                  schemaPath: "#/format",
                  keyword: "format",
                  params: { format: "date-time" },
                  message: 'must match format "' + "date-time" + '"',
                },
              ];
              return false;
            }
          }
        }
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
