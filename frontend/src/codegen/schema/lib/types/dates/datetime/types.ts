/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * [RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).
 */
export type IDateTime = string;
