/*
 * This file was generated automatically, do not edit it by hand.
 */
export const dateTimeGenericSchema = {
  $id: "http://jtsc-schemas.org/types/dates/generic.schema.json",
  title: "DateTimeGeneric",
  description: "Plain text date of arbitrary format.",
  type: "string",
  minLength: 1,
  maxLength: 256,
} as const;
