/*
 * This file was generated automatically, do not edit it by hand.
 */
export { dateTimeGenericSchema } from "./schema";
export { validateDateTimeGeneric } from "./lib";
export type { IDateTimeGeneric } from "./types";
