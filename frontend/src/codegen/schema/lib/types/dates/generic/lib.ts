/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { dateTimeGenericSchema } from "./schema";
import { validate } from "./validate";
import { IDateTimeGeneric } from "./types";
export const validateDateTimeGeneric = createValidator<IDateTimeGeneric>(
  validate as ValidateFunction<IDateTimeGeneric>,
  dateTimeGenericSchema as unknown as IJSONSchema
);
