/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Plain text date of arbitrary format.
 */
export type IDateTimeGeneric = string;
