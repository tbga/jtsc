/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { titleSchema } from "./schema";
import { validate } from "./validate";
import { ITitle } from "./types";
export const validateTitle = createValidator<ITitle>(
  validate as ValidateFunction<ITitle>,
  titleSchema as unknown as IJSONSchema
);
