/*
 * This file was generated automatically, do not edit it by hand.
 */
export { titleSchema } from "./schema";
export { validateTitle } from "./lib";
export type { ITitle } from "./types";
