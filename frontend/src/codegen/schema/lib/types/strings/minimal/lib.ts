/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { nonEmptyStringSchema } from "./schema";
import { validate } from "./validate";
import { INonEmptyString } from "./types";
export const validateNonEmptyString = createValidator<INonEmptyString>(
  validate as ValidateFunction<INonEmptyString>,
  nonEmptyStringSchema as unknown as IJSONSchema
);
