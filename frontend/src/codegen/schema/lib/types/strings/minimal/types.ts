/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * A string with at least 1 character.
 */
export type INonEmptyString = string;
