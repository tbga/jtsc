/*
 * This file was generated automatically, do not edit it by hand.
 */
export const nonEmptyStringSchema = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
} as const;
