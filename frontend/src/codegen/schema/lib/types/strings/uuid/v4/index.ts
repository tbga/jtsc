/*
 * This file was generated automatically, do not edit it by hand.
 */
export { uUIDSchema } from "./schema";
export { validateUUID } from "./lib";
export type { IUUID } from "./types";
