/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { uUIDSchema } from "./schema";
import { validate } from "./validate";
import { IUUID } from "./types";
export const validateUUID = createValidator<IUUID>(
  validate as ValidateFunction<IUUID>,
  uUIDSchema as unknown as IJSONSchema
);
