/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/strings/uuid/v4.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (typeof data !== "string") {
    let dataType0 = typeof data;
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (dataType0 == "number" || dataType0 == "boolean") {
        coerced0 = "" + data;
      } else if (data === null) {
        coerced0 = "";
      } else {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/type",
            keyword: "type",
            params: { type: "string" },
            message: "must be string",
          },
        ];
        return false;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  if (errors === 0) {
    if (errors === 0) {
      if (typeof data === "string") {
        if (func1(data) > 36) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/maxLength",
              keyword: "maxLength",
              params: { limit: 36 },
              message: "must NOT have more than 36 characters",
            },
          ];
          return false;
        } else {
          if (func1(data) < 36) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/minLength",
                keyword: "minLength",
                params: { limit: 36 },
                message: "must NOT have fewer than 36 characters",
              },
            ];
            return false;
          } else {
            if (!formats0.test(data)) {
              validate20.errors = [
                {
                  instancePath,
                  schemaPath: "#/format",
                  keyword: "format",
                  params: { format: "uuid" },
                  message: 'must match format "' + "uuid" + '"',
                },
              ];
              return false;
            }
          }
        }
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
