/*
 * This file was generated automatically, do not edit it by hand.
 */
export const nanoIDSchema = {
  $id: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
  title: "NanoID",
  description:
    "The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.",
  type: "string",
  minLength: 21,
  maxLength: 21,
} as const;
