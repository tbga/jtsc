/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.
 */
export type INanoID = string;
