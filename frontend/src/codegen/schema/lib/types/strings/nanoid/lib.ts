/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { nanoIDSchema } from "./schema";
import { validate } from "./validate";
import { INanoID } from "./types";
export const validateNanoID = createValidator<INanoID>(
  validate as ValidateFunction<INanoID>,
  nanoIDSchema as unknown as IJSONSchema
);
