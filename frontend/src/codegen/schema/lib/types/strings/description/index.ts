/*
 * This file was generated automatically, do not edit it by hand.
 */
export { descriptionSchema } from "./schema";
export { validateDescription } from "./lib";
export type { IDescription } from "./types";
