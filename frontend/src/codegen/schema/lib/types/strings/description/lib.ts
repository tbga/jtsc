/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { descriptionSchema } from "./schema";
import { validate } from "./validate";
import { IDescription } from "./types";
export const validateDescription = createValidator<IDescription>(
  validate as ValidateFunction<IDescription>,
  descriptionSchema as unknown as IJSONSchema
);
