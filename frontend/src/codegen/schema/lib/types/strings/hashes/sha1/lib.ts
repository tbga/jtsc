/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { hashSHA1Schema } from "./schema";
import { validate } from "./validate";
import { IHashSHA1 } from "./types";
export const validateHashSHA1 = createValidator<IHashSHA1>(
  validate as ValidateFunction<IHashSHA1>,
  hashSHA1Schema as unknown as IJSONSchema
);
