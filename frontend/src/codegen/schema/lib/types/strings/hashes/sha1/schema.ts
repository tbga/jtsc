/*
 * This file was generated automatically, do not edit it by hand.
 */
export const hashSHA1Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
  title: "HashSHA1",
  description: "SHA1 hash string.",
  type: "string",
  minLength: 40,
  maxLength: 40,
} as const;
