/*
 * This file was generated automatically, do not edit it by hand.
 */
export const hashMD5Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
  title: "HashMD5",
  description: "MD5 hash string.",
  type: "string",
  minLength: 32,
  maxLength: 32,
} as const;
