/*
 * This file was generated automatically, do not edit it by hand.
 */
export const hashSHA256Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
  title: "HashSHA256",
  description: "SHA256 hash string.",
  type: "string",
  minLength: 64,
  maxLength: 64,
} as const;
