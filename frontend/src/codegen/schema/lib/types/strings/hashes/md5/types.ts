/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * MD5 hash string.
 */
export type IHashMD5 = string;
