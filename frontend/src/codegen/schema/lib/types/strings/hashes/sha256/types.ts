/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * SHA256 hash string.
 */
export type IHashSHA256 = string;
