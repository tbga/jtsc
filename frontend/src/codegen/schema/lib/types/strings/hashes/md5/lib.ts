/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { hashMD5Schema } from "./schema";
import { validate } from "./validate";
import { IHashMD5 } from "./types";
export const validateHashMD5 = createValidator<IHashMD5>(
  validate as ValidateFunction<IHashMD5>,
  hashMD5Schema as unknown as IJSONSchema
);
