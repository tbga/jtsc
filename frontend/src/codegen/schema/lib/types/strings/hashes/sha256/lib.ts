/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { hashSHA256Schema } from "./schema";
import { validate } from "./validate";
import { IHashSHA256 } from "./types";
export const validateHashSHA256 = createValidator<IHashSHA256>(
  validate as ValidateFunction<IHashSHA256>,
  hashSHA256Schema as unknown as IJSONSchema
);
