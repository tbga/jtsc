/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * SHA1 hash string.
 */
export type IHashSHA1 = string;
