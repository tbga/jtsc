/*
 * This file was generated automatically, do not edit it by hand.
 */
export const paginationSchema = {
  $id: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
  title: "Pagination",
  description: "Pagination information for the collection.",
  type: "object",
  required: ["total_count", "limit", "total_pages", "current_page"],
  additionalProperties: false,
  properties: {
    current_page: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    total_count: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    limit: {
      type: "integer",
    },
    total_pages: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
