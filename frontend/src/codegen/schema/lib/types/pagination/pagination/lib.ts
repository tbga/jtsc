/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { paginationSchema } from "./schema";
import { validate } from "./validate";
import { IPagination } from "./types";
export const validatePagination = createValidator<IPagination>(
  validate as ValidateFunction<IPagination>,
  paginationSchema as unknown as IJSONSchema
);
