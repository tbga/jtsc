/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Pagination information for the collection.
 */
export interface IPagination {
  current_page: IBigSerialInteger;
  total_count: IBigSerialInteger;
  limit: number;
  total_pages: IBigSerialInteger;
}
