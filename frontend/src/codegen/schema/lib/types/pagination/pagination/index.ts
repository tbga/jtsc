/*
 * This file was generated automatically, do not edit it by hand.
 */
export { paginationSchema } from "./schema";
export { validatePagination } from "./lib";
export type { IPagination } from "./types";
