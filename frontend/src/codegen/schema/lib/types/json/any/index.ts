/*
 * This file was generated automatically, do not edit it by hand.
 */
export { jSONAnySchema } from "./schema";
export { validateJSONAny } from "./lib";
export type { IJSONAny } from "./types";
