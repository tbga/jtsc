/*
 * This file was generated automatically, do not edit it by hand.
 */
export const jSONAnySchema = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    {
      type: "null",
    },
    {
      type: "boolean",
    },
    {
      type: "number",
    },
    {
      type: "string",
    },
    {
      type: "object",
    },
    {
      type: "array",
    },
    {
      type: "array",
      minItems: 1,
    },
  ],
} as const;
