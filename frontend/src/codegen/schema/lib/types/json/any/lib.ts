/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { jSONAnySchema } from "./schema";
import { validate } from "./validate";
import { IJSONAny } from "./types";
export const validateJSONAny = createValidator<IJSONAny>(
  validate as ValidateFunction<IJSONAny>,
  jSONAnySchema as unknown as IJSONSchema
);
