/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    { type: "null" },
    { type: "boolean" },
    { type: "number" },
    { type: "string" },
    { type: "object" },
    { type: "array" },
    { type: "array", minItems: 1 },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/types/json/any.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  const _errs0 = errors;
  let valid0 = false;
  const _errs1 = errors;
  if (data !== null) {
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (data === "" || data === 0 || data === false) {
        coerced0 = null;
      } else {
        const err0 = {
          instancePath,
          schemaPath: "#/anyOf/0/type",
          keyword: "type",
          params: { type: "null" },
          message: "must be null",
        };
        if (vErrors === null) {
          vErrors = [err0];
        } else {
          vErrors.push(err0);
        }
        errors++;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  var _valid0 = _errs1 === errors;
  valid0 = valid0 || _valid0;
  const _errs3 = errors;
  if (typeof data !== "boolean") {
    let coerced1 = undefined;
    if (!(coerced1 !== undefined)) {
      if (data === "false" || data === 0 || data === null) {
        coerced1 = false;
      } else if (data === "true" || data === 1) {
        coerced1 = true;
      } else {
        const err1 = {
          instancePath,
          schemaPath: "#/anyOf/1/type",
          keyword: "type",
          params: { type: "boolean" },
          message: "must be boolean",
        };
        if (vErrors === null) {
          vErrors = [err1];
        } else {
          vErrors.push(err1);
        }
        errors++;
      }
    }
    if (coerced1 !== undefined) {
      data = coerced1;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced1;
      }
    }
  }
  var _valid0 = _errs3 === errors;
  valid0 = valid0 || _valid0;
  const _errs5 = errors;
  if (!(typeof data == "number" && isFinite(data))) {
    let dataType2 = typeof data;
    let coerced2 = undefined;
    if (!(coerced2 !== undefined)) {
      if (
        dataType2 == "boolean" ||
        data === null ||
        (dataType2 == "string" && data && data == +data)
      ) {
        coerced2 = +data;
      } else {
        const err2 = {
          instancePath,
          schemaPath: "#/anyOf/2/type",
          keyword: "type",
          params: { type: "number" },
          message: "must be number",
        };
        if (vErrors === null) {
          vErrors = [err2];
        } else {
          vErrors.push(err2);
        }
        errors++;
      }
    }
    if (coerced2 !== undefined) {
      data = coerced2;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced2;
      }
    }
  }
  var _valid0 = _errs5 === errors;
  valid0 = valid0 || _valid0;
  const _errs7 = errors;
  if (typeof data !== "string") {
    let dataType3 = typeof data;
    let coerced3 = undefined;
    if (!(coerced3 !== undefined)) {
      if (dataType3 == "number" || dataType3 == "boolean") {
        coerced3 = "" + data;
      } else if (data === null) {
        coerced3 = "";
      } else {
        const err3 = {
          instancePath,
          schemaPath: "#/anyOf/3/type",
          keyword: "type",
          params: { type: "string" },
          message: "must be string",
        };
        if (vErrors === null) {
          vErrors = [err3];
        } else {
          vErrors.push(err3);
        }
        errors++;
      }
    }
    if (coerced3 !== undefined) {
      data = coerced3;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced3;
      }
    }
  }
  var _valid0 = _errs7 === errors;
  valid0 = valid0 || _valid0;
  const _errs9 = errors;
  if (!(data && typeof data == "object" && !Array.isArray(data))) {
    const err4 = {
      instancePath,
      schemaPath: "#/anyOf/4/type",
      keyword: "type",
      params: { type: "object" },
      message: "must be object",
    };
    if (vErrors === null) {
      vErrors = [err4];
    } else {
      vErrors.push(err4);
    }
    errors++;
  }
  var _valid0 = _errs9 === errors;
  valid0 = valid0 || _valid0;
  const _errs11 = errors;
  if (!Array.isArray(data)) {
    const err5 = {
      instancePath,
      schemaPath: "#/anyOf/5/type",
      keyword: "type",
      params: { type: "array" },
      message: "must be array",
    };
    if (vErrors === null) {
      vErrors = [err5];
    } else {
      vErrors.push(err5);
    }
    errors++;
  }
  var _valid0 = _errs11 === errors;
  valid0 = valid0 || _valid0;
  const _errs13 = errors;
  if (errors === _errs13) {
    if (Array.isArray(data)) {
      if (data.length < 1) {
        const err6 = {
          instancePath,
          schemaPath: "#/anyOf/6/minItems",
          keyword: "minItems",
          params: { limit: 1 },
          message: "must NOT have fewer than 1 items",
        };
        if (vErrors === null) {
          vErrors = [err6];
        } else {
          vErrors.push(err6);
        }
        errors++;
      }
    } else {
      const err7 = {
        instancePath,
        schemaPath: "#/anyOf/6/type",
        keyword: "type",
        params: { type: "array" },
        message: "must be array",
      };
      if (vErrors === null) {
        vErrors = [err7];
      } else {
        vErrors.push(err7);
      }
      errors++;
    }
  }
  var _valid0 = _errs13 === errors;
  valid0 = valid0 || _valid0;
  if (!valid0) {
    const err8 = {
      instancePath,
      schemaPath: "#/anyOf",
      keyword: "anyOf",
      params: {},
      message: "must match a schema in anyOf",
    };
    if (vErrors === null) {
      vErrors = [err8];
    } else {
      vErrors.push(err8);
    }
    errors++;
    validate20.errors = vErrors;
    return false;
  } else {
    errors = _errs0;
    if (vErrors !== null) {
      if (_errs0) {
        vErrors.length = _errs0;
      } else {
        vErrors = null;
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
