/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
  title: "Platform",
  description: "Information about the local file system.",
  type: "string",
  enum: [
    "aix",
    "android",
    "darwin",
    "freebsd",
    "haiku",
    "linux",
    "openbsd",
    "sunos",
    "win32",
    "cygwin",
    "netbsd",
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/types/nodejs/platform.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (typeof data !== "string") {
    let dataType0 = typeof data;
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (dataType0 == "number" || dataType0 == "boolean") {
        coerced0 = "" + data;
      } else if (data === null) {
        coerced0 = "";
      } else {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/type",
            keyword: "type",
            params: { type: "string" },
            message: "must be string",
          },
        ];
        return false;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  if (
    !(
      data === "aix" ||
      data === "android" ||
      data === "darwin" ||
      data === "freebsd" ||
      data === "haiku" ||
      data === "linux" ||
      data === "openbsd" ||
      data === "sunos" ||
      data === "win32" ||
      data === "cygwin" ||
      data === "netbsd"
    )
  ) {
    validate20.errors = [
      {
        instancePath,
        schemaPath: "#/enum",
        keyword: "enum",
        params: { allowedValues: schema31.enum },
        message: "must be equal to one of the allowed values",
      },
    ];
    return false;
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
