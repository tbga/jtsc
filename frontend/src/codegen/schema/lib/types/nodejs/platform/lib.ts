/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { platformSchema } from "./schema";
import { validate } from "./validate";
import { IPlatform } from "./types";
export const validatePlatform = createValidator<IPlatform>(
  validate as ValidateFunction<IPlatform>,
  platformSchema as unknown as IJSONSchema
);
