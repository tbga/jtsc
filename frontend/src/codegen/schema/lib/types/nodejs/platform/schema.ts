/*
 * This file was generated automatically, do not edit it by hand.
 */
export const platformSchema = {
  $id: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
  title: "Platform",
  description: "Information about the local file system.",
  type: "string",
  enum: [
    "aix",
    "android",
    "darwin",
    "freebsd",
    "haiku",
    "linux",
    "openbsd",
    "sunos",
    "win32",
    "cygwin",
    "netbsd",
  ],
} as const;
