/*
 * This file was generated automatically, do not edit it by hand.
 */
export { platformSchema } from "./schema";
export { validatePlatform } from "./lib";
export type { IPlatform } from "./types";
