/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { nonEmptyStringArraySchema } from "./schema";
import { validate } from "./validate";
import { INonEmptyStringArray } from "./types";
export const validateNonEmptyStringArray =
  createValidator<INonEmptyStringArray>(
    validate as ValidateFunction<INonEmptyStringArray>,
    nonEmptyStringArraySchema as unknown as IJSONSchema
  );
