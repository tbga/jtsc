/*
 * This file was generated automatically, do not edit it by hand.
 */
export const nonEmptyStringArraySchema = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-string-array.schema.json",
  title: "NonEmptyStringArray",
  description: "A non-empty array of non-empty strings.",
  type: "array",
  minItems: 1,
  items: {
    $comment:
      "For some reason [`json-schema-to-typescript`](https://github.com/bcherny/json-schema-to-typescript) redeclares `http://jtsc-schemas.org/types/strings/minimal.schema.json` $ref, so using literal instead.",
    type: "string",
    minLength: 1,
  },
} as const;
