/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-string-array.schema.json",
  title: "NonEmptyStringArray",
  description: "A non-empty array of non-empty strings.",
  type: "array",
  minItems: 1,
  items: {
    $comment:
      "For some reason [`json-schema-to-typescript`](https://github.com/bcherny/json-schema-to-typescript) redeclares `http://jtsc-schemas.org/types/strings/minimal.schema.json` $ref, so using literal instead.",
    type: "string",
    minLength: 1,
  },
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/arrays/non-empty-string-array.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (Array.isArray(data)) {
      if (data.length < 1) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/minItems",
            keyword: "minItems",
            params: { limit: 1 },
            message: "must NOT have fewer than 1 items",
          },
        ];
        return false;
      } else {
        var valid0 = true;
        const len0 = data.length;
        for (let i0 = 0; i0 < len0; i0++) {
          let data0 = data[i0];
          const _errs1 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/" + i0,
                    schemaPath: "#/items/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data[i0] = coerced0;
              }
            }
          }
          if (errors === _errs1) {
            if (typeof data0 === "string") {
              if (func1(data0) < 1) {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/" + i0,
                    schemaPath: "#/items/minLength",
                    keyword: "minLength",
                    params: { limit: 1 },
                    message: "must NOT have fewer than 1 characters",
                  },
                ];
                return false;
              }
            }
          }
          var valid0 = _errs1 === errors;
          if (!valid0) {
            break;
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "array" },
          message: "must be array",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  items: true,
  dynamicProps: false,
  dynamicItems: false,
};
