/*
 * This file was generated automatically, do not edit it by hand.
 */
export { nonEmptyStringArraySchema } from "./schema";
export { validateNonEmptyStringArray } from "./lib";
export type { INonEmptyStringArray } from "./types";
