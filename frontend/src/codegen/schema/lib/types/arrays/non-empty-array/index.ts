/*
 * This file was generated automatically, do not edit it by hand.
 */
export { arrayNonEmptySchema } from "./schema";
export { validateArrayNonEmpty } from "./lib";
export type { IArrayNonEmpty } from "./types";
