/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { arrayNonEmptySchema } from "./schema";
import { validate } from "./validate";
import { IArrayNonEmpty } from "./types";
export const validateArrayNonEmpty = createValidator<IArrayNonEmpty>(
  validate as ValidateFunction<IArrayNonEmpty>,
  arrayNonEmptySchema as unknown as IJSONSchema
);
