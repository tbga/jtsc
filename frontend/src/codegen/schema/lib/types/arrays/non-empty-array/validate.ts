/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-array.schema.json",
  title: "ArrayNonEmpty",
  description: "An array with at least one element",
  type: "array",
  minItems: 1,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/arrays/non-empty-array.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (Array.isArray(data)) {
      if (data.length < 1) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/minItems",
            keyword: "minItems",
            params: { limit: 1 },
            message: "must NOT have fewer than 1 items",
          },
        ];
        return false;
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "array" },
          message: "must be array",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
