/*
 * This file was generated automatically, do not edit it by hand.
 */
export const arrayNonEmptySchema = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-array.schema.json",
  title: "ArrayNonEmpty",
  description: "An array with at least one element",
  type: "array",
  minItems: 1,
} as const;
