/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { bigIntegerSchema } from "./schema";
import { validate } from "./validate";
import { IBigInteger } from "./types";
export const validateBigInteger = createValidator<IBigInteger>(
  validate as ValidateFunction<IBigInteger>,
  bigIntegerSchema as unknown as IJSONSchema
);
