/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Big integer
 */
export type IBigInteger = string;
