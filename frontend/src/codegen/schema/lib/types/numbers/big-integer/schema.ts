/*
 * This file was generated automatically, do not edit it by hand.
 */
export const bigIntegerSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer.schema.json",
  title: "BigInteger",
  description: "Big integer",
  type: "string",
  minLength: 1,
  maxLength: 20,
} as const;
