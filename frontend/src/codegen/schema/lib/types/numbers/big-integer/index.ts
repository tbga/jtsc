/*
 * This file was generated automatically, do not edit it by hand.
 */
export { bigIntegerSchema } from "./schema";
export { validateBigInteger } from "./lib";
export type { IBigInteger } from "./types";
