/*
 * This file was generated automatically, do not edit it by hand.
 */
export const serialIntegerSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/serial.schema.json",
  title: "SerialInteger",
  description:
    "Integer equivalent of `SERIAL` type.\nMax length is 10 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "integer",
  minimum: 1,
  maximum: 10,
} as const;
