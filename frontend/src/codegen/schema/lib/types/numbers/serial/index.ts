/*
 * This file was generated automatically, do not edit it by hand.
 */
export { serialIntegerSchema } from "./schema";
export { validateSerialInteger } from "./lib";
export type { ISerialInteger } from "./types";
