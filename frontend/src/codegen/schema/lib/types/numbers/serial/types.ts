/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Integer equivalent of `SERIAL` type.
 * Max length is 10 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).
 */
export type ISerialInteger = number;
