/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { serialIntegerSchema } from "./schema";
import { validate } from "./validate";
import { ISerialInteger } from "./types";
export const validateSerialInteger = createValidator<ISerialInteger>(
  validate as ValidateFunction<ISerialInteger>,
  serialIntegerSchema as unknown as IJSONSchema
);
