/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "http://jtsc-schemas.org/types/numbers/serial.schema.json",
  title: "SerialInteger",
  description:
    "Integer equivalent of `SERIAL` type.\nMax length is 10 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "integer",
  minimum: 1,
  maximum: 10,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/numbers/serial.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (
    !(typeof data == "number" && !(data % 1) && !isNaN(data) && isFinite(data))
  ) {
    let dataType0 = typeof data;
    let coerced0 = undefined;
    if (!(coerced0 !== undefined)) {
      if (
        dataType0 === "boolean" ||
        data === null ||
        (dataType0 === "string" && data && data == +data && !(data % 1))
      ) {
        coerced0 = +data;
      } else {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/type",
            keyword: "type",
            params: { type: "integer" },
            message: "must be integer",
          },
        ];
        return false;
      }
    }
    if (coerced0 !== undefined) {
      data = coerced0;
      if (parentData !== undefined) {
        parentData[parentDataProperty] = coerced0;
      }
    }
  }
  if (errors === 0) {
    if (typeof data == "number" && isFinite(data)) {
      if (data > 10 || isNaN(data)) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/maximum",
            keyword: "maximum",
            params: { comparison: "<=", limit: 10 },
            message: "must be <= 10",
          },
        ];
        return false;
      } else {
        if (data < 1 || isNaN(data)) {
          validate20.errors = [
            {
              instancePath,
              schemaPath: "#/minimum",
              keyword: "minimum",
              params: { comparison: ">=", limit: 1 },
              message: "must be >= 1",
            },
          ];
          return false;
        }
      }
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = { dynamicProps: false, dynamicItems: false };
