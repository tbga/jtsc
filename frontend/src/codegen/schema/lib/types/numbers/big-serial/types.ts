/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Integer equivalent of `BIG SERIAL` type.
 * Max length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).
 */
export type IBigSerialInteger = string;
