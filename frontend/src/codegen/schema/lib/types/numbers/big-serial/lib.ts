/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { bigSerialIntegerSchema } from "./schema";
import { validate } from "./validate";
import { IBigSerialInteger } from "./types";
export const validateBigSerialInteger = createValidator<IBigSerialInteger>(
  validate as ValidateFunction<IBigSerialInteger>,
  bigSerialIntegerSchema as unknown as IJSONSchema
);
