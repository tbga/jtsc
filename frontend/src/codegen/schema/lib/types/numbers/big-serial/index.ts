/*
 * This file was generated automatically, do not edit it by hand.
 */
export { bigSerialIntegerSchema } from "./schema";
export { validateBigSerialInteger } from "./lib";
export type { IBigSerialInteger } from "./types";
