/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Big integer but only positive values and zero.
 */
export type IBigIntegerPositive = string;
