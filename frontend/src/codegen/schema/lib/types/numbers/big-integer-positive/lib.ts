/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { bigIntegerPositiveSchema } from "./schema";
import { validate } from "./validate";
import { IBigIntegerPositive } from "./types";
export const validateBigIntegerPositive = createValidator<IBigIntegerPositive>(
  validate as ValidateFunction<IBigIntegerPositive>,
  bigIntegerPositiveSchema as unknown as IJSONSchema
);
