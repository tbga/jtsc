/*
 * This file was generated automatically, do not edit it by hand.
 */
export const bigIntegerPositiveSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
} as const;
