/*
 * This file was generated automatically, do not edit it by hand.
 */
export { bigIntegerPositiveSchema } from "./schema";
export { validateBigIntegerPositive } from "./lib";
export type { IBigIntegerPositive } from "./types";
