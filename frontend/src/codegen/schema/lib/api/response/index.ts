/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSchema } from "./schema";
export { validateAPIResponse } from "./lib";
export type { IAPIResponse } from "./types";
