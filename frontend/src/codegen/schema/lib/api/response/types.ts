/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IAPIResponseFailure } from "#codegen/schema/lib/api/response-failure";
import { type IAPIResponseSuccess } from "#codegen/schema/lib/api/response-success";

/**
 * Body of the API response.
 */
export type IAPIResponse = IAPIResponseFailure | IAPIResponseSuccess;
