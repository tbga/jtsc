/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponse } from "./types";
export const validateAPIResponse = createValidator<IAPIResponse>(
  validate as ValidateFunction<IAPIResponse>,
  aPIResponseSchema as unknown as IJSONSchema
);
