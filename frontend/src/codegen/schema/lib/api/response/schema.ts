/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSchema = {
  $id: "https://jtsc-schemas.org/api/response.schema.json",
  title: "APIResponse",
  description: "Body of the API response.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    {
      $ref: "https://jtsc-schemas.org/api/response-failure.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/api/response-success.schema.json",
    },
  ],
} as const;
