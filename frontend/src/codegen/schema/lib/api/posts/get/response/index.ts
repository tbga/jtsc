/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostsGetSchema } from "./schema";
export { validateAPIResponsePostsGet } from "./lib";
export type { IAPIResponsePostsGet } from "./types";
