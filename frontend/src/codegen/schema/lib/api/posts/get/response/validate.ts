/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/posts/get/response.schema.json",
  title: "APIResponsePostsGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "posts"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        posts: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/preview.schema.json",
          },
        },
      },
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
  title: "Pagination",
  description: "Pagination information for the collection.",
  type: "object",
  required: ["total_count", "limit", "total_pages", "current_page"],
  additionalProperties: false,
  properties: {
    current_page: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    total_count: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    limit: { type: "integer" },
    total_pages: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/pagination/pagination.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.total_count === undefined && (missing0 = "total_count")) ||
        (data.limit === undefined && (missing0 = "limit")) ||
        (data.total_pages === undefined && (missing0 = "total_pages")) ||
        (data.current_page === undefined && (missing0 = "current_page"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "current_page" ||
              key0 === "total_count" ||
              key0 === "limit" ||
              key0 === "total_pages"
            )
          ) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.current_page !== undefined) {
            let data0 = data.current_page;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/current_page",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["current_page"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/current_page",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/current_page",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.total_count !== undefined) {
              let data1 = data.total_count;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/total_count",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["total_count"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 19) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/total_count",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 19 },
                        message: "must NOT have more than 19 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 1) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/total_count",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.limit !== undefined) {
                let data2 = data.limit;
                const _errs8 = errors;
                if (
                  !(
                    typeof data2 == "number" &&
                    !(data2 % 1) &&
                    !isNaN(data2) &&
                    isFinite(data2)
                  )
                ) {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (
                      dataType2 === "boolean" ||
                      data2 === null ||
                      (dataType2 === "string" &&
                        data2 &&
                        data2 == +data2 &&
                        !(data2 % 1))
                    ) {
                      coerced2 = +data2;
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/limit",
                          schemaPath: "#/properties/limit/type",
                          keyword: "type",
                          params: { type: "integer" },
                          message: "must be integer",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["limit"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.total_pages !== undefined) {
                  let data3 = data.total_pages;
                  const _errs10 = errors;
                  const _errs11 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/total_pages",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["total_pages"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs11) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 19) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/total_pages",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 19 },
                            message: "must NOT have more than 19 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/total_pages",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs10 === errors;
                } else {
                  var valid0 = true;
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
const schema36 = {
  $id: "https://jtsc-schemas.org/entities/post/preview.schema.json",
  title: "PostPreview",
  description: "Preview of the post.",
  type: "object",
  additionalProperties: false,
  required: ["id", "releases", "artists"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    artists: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
  },
};
const schema38 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const schema40 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
function validate23(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/post/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate23.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.releases === undefined && (missing0 = "releases")) ||
        (data.artists === undefined && (missing0 = "artists"))
      ) {
        validate23.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "releases" ||
              key0 === "artists" ||
              key0 === "name" ||
              key0 === "title"
            )
          ) {
            validate23.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate23.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate23.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate23.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.releases !== undefined) {
              let data1 = data.releases;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate23.errors = [
                      {
                        instancePath: instancePath + "/releases",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["releases"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 20) {
                    validate23.errors = [
                      {
                        instancePath: instancePath + "/releases",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 20 },
                        message: "must NOT have more than 20 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 1) {
                      validate23.errors = [
                        {
                          instancePath: instancePath + "/releases",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.artists !== undefined) {
                let data2 = data.artists;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate23.errors = [
                        {
                          instancePath: instancePath + "/artists",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["artists"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (typeof data2 === "string") {
                    if (func1(data2) > 20) {
                      validate23.errors = [
                        {
                          instancePath: instancePath + "/artists",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 20 },
                          message: "must NOT have more than 20 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data2) < 1) {
                        validate23.errors = [
                          {
                            instancePath: instancePath + "/artists",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate23.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 216) {
                        validate23.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 216 },
                            message: "must NOT have more than 216 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate23.errors = [
                            {
                              instancePath: instancePath + "/name",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.title !== undefined) {
                    let data4 = data.title;
                    const _errs14 = errors;
                    const _errs15 = errors;
                    if (typeof data4 !== "string") {
                      let dataType4 = typeof data4;
                      let coerced4 = undefined;
                      if (!(coerced4 !== undefined)) {
                        if (dataType4 == "number" || dataType4 == "boolean") {
                          coerced4 = "" + data4;
                        } else if (data4 === null) {
                          coerced4 = "";
                        } else {
                          validate23.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced4 !== undefined) {
                        data4 = coerced4;
                        if (data !== undefined) {
                          data["title"] = coerced4;
                        }
                      }
                    }
                    if (errors === _errs15) {
                      if (typeof data4 === "string") {
                        if (func1(data4) > 216) {
                          validate23.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 216 },
                              message: "must NOT have more than 216 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data4) < 1) {
                            validate23.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate23.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate23.errors = vErrors;
  return errors === 0;
}
validate23.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/posts/get/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              if (errors === _errs4) {
                if (
                  data1 &&
                  typeof data1 == "object" &&
                  !Array.isArray(data1)
                ) {
                  let missing1;
                  if (
                    (data1.pagination === undefined &&
                      (missing1 = "pagination")) ||
                    (data1.posts === undefined && (missing1 = "posts"))
                  ) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath: "#/properties/data/required",
                        keyword: "required",
                        params: { missingProperty: missing1 },
                        message:
                          "must have required property '" + missing1 + "'",
                      },
                    ];
                    return false;
                  } else {
                    const _errs6 = errors;
                    for (const key1 in data1) {
                      if (!(key1 === "pagination" || key1 === "posts")) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/data",
                            schemaPath:
                              "#/properties/data/additionalProperties",
                            keyword: "additionalProperties",
                            params: { additionalProperty: key1 },
                            message: "must NOT have additional properties",
                          },
                        ];
                        return false;
                        break;
                      }
                    }
                    if (_errs6 === errors) {
                      if (data1.pagination !== undefined) {
                        const _errs7 = errors;
                        if (
                          !validate21(data1.pagination, {
                            instancePath: instancePath + "/data/pagination",
                            parentData: data1,
                            parentDataProperty: "pagination",
                            rootData,
                            dynamicAnchors,
                          })
                        ) {
                          vErrors =
                            vErrors === null
                              ? validate21.errors
                              : vErrors.concat(validate21.errors);
                          errors = vErrors.length;
                        }
                        var valid1 = _errs7 === errors;
                      } else {
                        var valid1 = true;
                      }
                      if (valid1) {
                        if (data1.posts !== undefined) {
                          let data3 = data1.posts;
                          const _errs8 = errors;
                          if (errors === _errs8) {
                            if (Array.isArray(data3)) {
                              if (data3.length < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/data/posts",
                                    schemaPath:
                                      "#/properties/data/properties/posts/minItems",
                                    keyword: "minItems",
                                    params: { limit: 1 },
                                    message: "must NOT have fewer than 1 items",
                                  },
                                ];
                                return false;
                              } else {
                                var valid2 = true;
                                const len0 = data3.length;
                                for (let i0 = 0; i0 < len0; i0++) {
                                  const _errs10 = errors;
                                  if (
                                    !validate23(data3[i0], {
                                      instancePath:
                                        instancePath + "/data/posts/" + i0,
                                      parentData: data3,
                                      parentDataProperty: i0,
                                      rootData,
                                      dynamicAnchors,
                                    })
                                  ) {
                                    vErrors =
                                      vErrors === null
                                        ? validate23.errors
                                        : vErrors.concat(validate23.errors);
                                    errors = vErrors.length;
                                  }
                                  var valid2 = _errs10 === errors;
                                  if (!valid2) {
                                    break;
                                  }
                                }
                              }
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/posts",
                                  schemaPath:
                                    "#/properties/data/properties/posts/type",
                                  keyword: "type",
                                  params: { type: "array" },
                                  message: "must be array",
                                },
                              ];
                              return false;
                            }
                          }
                          var valid1 = _errs8 === errors;
                        } else {
                          var valid1 = true;
                        }
                      }
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/type",
                      keyword: "type",
                      params: { type: "object" },
                      message: "must be object",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
