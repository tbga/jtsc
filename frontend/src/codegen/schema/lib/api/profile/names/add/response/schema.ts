/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileNamesAddSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/add/response.schema.json",
  title: "APIResponseProfileNamesAdd",
  description: "Added profile names.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added names.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/entity.schema.json",
      },
    },
  },
} as const;
