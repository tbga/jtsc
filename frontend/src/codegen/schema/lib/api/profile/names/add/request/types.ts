/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type INameInit } from "#codegen/schema/lib/entities/name/init";

/**
 * Add profile names request.
 */
export interface IAPIRequestProfileNamesAdd {
  /**
   * A list of name initializers.
   *
   * @minItems 1
   */
  data: [INameInit, ...INameInit[]];
}
