/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileNamesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/remove/request.schema.json",
  title: "APIRequestProfileNamesRemove",
  description: "Remove profile names request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of name IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
