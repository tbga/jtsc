/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileNamesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileNamesRemove } from "./types";
export const validateAPIResponseProfileNamesRemove =
  createValidator<IAPIResponseProfileNamesRemove>(
    validate as ValidateFunction<IAPIResponseProfileNamesRemove>,
    aPIResponseProfileNamesRemoveSchema as unknown as IJSONSchema
  );
