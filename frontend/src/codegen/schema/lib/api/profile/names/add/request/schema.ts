/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileNamesAddSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/add/request.schema.json",
  title: "APIRequestProfileNamesAdd",
  description: "Add profile names request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of name initializers.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/name/init.schema.json",
      },
    },
  },
} as const;
