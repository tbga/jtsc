/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileNameItemsGetSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileNameItemsGet } from "./types";
export const validateAPIResponseProfileNameItemsGet =
  createValidator<IAPIResponseProfileNameItemsGet>(
    validate as ValidateFunction<IAPIResponseProfileNameItemsGet>,
    aPIResponseProfileNameItemsGetSchema as unknown as IJSONSchema
  );
