/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileNamesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileNamesRemove } from "./types";
export const validateAPIRequestProfileNamesRemove =
  createValidator<IAPIRequestProfileNamesRemove>(
    validate as ValidateFunction<IAPIRequestProfileNamesRemove>,
    aPIRequestProfileNamesRemoveSchema as unknown as IJSONSchema
  );
