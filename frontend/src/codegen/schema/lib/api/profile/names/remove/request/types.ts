/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Remove profile names request.
 */
export interface IAPIRequestProfileNamesRemove {
  /**
   * A list of name IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
