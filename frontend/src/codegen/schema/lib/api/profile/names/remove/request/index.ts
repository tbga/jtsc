/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestProfileNamesRemoveSchema } from "./schema";
export { validateAPIRequestProfileNamesRemove } from "./lib";
export type { IAPIRequestProfileNamesRemove } from "./types";
