/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileNameItemsGetSchema } from "./schema";
export { validateAPIResponseProfileNameItemsGet } from "./lib";
export type { IAPIResponseProfileNameItemsGet } from "./types";
