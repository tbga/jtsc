/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IEntity } from "#codegen/schema/lib/entities/entity";

/**
 * Added profile names.
 */
export interface IAPIResponseProfileNamesAdd {
  is_successful: true;
  /**
   * A list of added names.
   */
  data: IEntity[];
}
