/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileNamesAddSchema } from "./schema";
export { validateAPIResponseProfileNamesAdd } from "./lib";
export type { IAPIResponseProfileNamesAdd } from "./types";
