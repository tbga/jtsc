/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileNamesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileNamesAdd } from "./types";
export const validateAPIRequestProfileNamesAdd =
  createValidator<IAPIRequestProfileNamesAdd>(
    validate as ValidateFunction<IAPIRequestProfileNamesAdd>,
    aPIRequestProfileNamesAddSchema as unknown as IJSONSchema
  );
