/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileNameItemsGetSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/items/response.schema.json",
  title: "APIResponseProfileNameItemsGet",
  description: "A successful response for profile names.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "names"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        names: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
