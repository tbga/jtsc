/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileNamesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileNamesAdd } from "./types";
export const validateAPIResponseProfileNamesAdd =
  createValidator<IAPIResponseProfileNamesAdd>(
    validate as ValidateFunction<IAPIResponseProfileNamesAdd>,
    aPIResponseProfileNamesAddSchema as unknown as IJSONSchema
  );
