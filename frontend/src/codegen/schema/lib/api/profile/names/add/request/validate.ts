/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/profile/names/add/request.schema.json",
  title: "APIRequestProfileNamesAdd",
  description: "Add profile names request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of name initializers.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/name/init.schema.json",
      },
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/name/init.schema.json",
  title: "NameInit",
  description: "Name initializer.",
  type: "object",
  additionalProperties: false,
  required: ["full_name"],
  properties: {
    full_name: {
      type: "string",
      minLength: 1,
      maxLength: 747,
      description:
        "The complete name of the name entry. The limit is the longest name as per [guinness](https://www.guinnessworldrecords.com/world-records/67285-longest-personal-name).",
      examples: [
        "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Zeus Wolfeschlegelsteinhausenbergerdorffwelchevoralternwarengewissenhaftschaferswessenschafewarenwohlgepflegeundsorgfaltigkeitbeschutzenvonangreifendurchihrraubgierigfeindewelchevoralternzwolftausendjahresvorandieerscheinenvanderersteerdemenschderraumschiffgebrauchlichtalsseinursprungvonkraftgestartseinlangefahrthinzwischensternartigraumaufdersuchenachdiesternwelchegehabtbewohnbarplanetenkreisedrehensichundwohinderneurassevonverstandigmenschlichkeitkonntefortpflanzenundsicherfreuenanlebenslanglichfreudeundruhemitnichteinfurchtvorangreifenvonandererintelligentgeschopfsvonhinzwischensternartigraum",
      ],
    },
  },
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/profile/names/add/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs2 = errors;
            if (errors === _errs2) {
              if (Array.isArray(data0)) {
                if (data0.length < 1) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/minItems",
                      keyword: "minItems",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 items",
                    },
                  ];
                  return false;
                } else {
                  var valid1 = true;
                  const len0 = data0.length;
                  for (let i0 = 0; i0 < len0; i0++) {
                    let data1 = data0[i0];
                    const _errs4 = errors;
                    const _errs5 = errors;
                    if (errors === _errs5) {
                      if (
                        data1 &&
                        typeof data1 == "object" &&
                        !Array.isArray(data1)
                      ) {
                        let missing1;
                        if (
                          data1.full_name === undefined &&
                          (missing1 = "full_name")
                        ) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/data/" + i0,
                              schemaPath:
                                "https://jtsc-schemas.org/entities/name/init.schema.json/required",
                              keyword: "required",
                              params: { missingProperty: missing1 },
                              message:
                                "must have required property '" +
                                missing1 +
                                "'",
                            },
                          ];
                          return false;
                        } else {
                          const _errs7 = errors;
                          for (const key1 in data1) {
                            if (!(key1 === "full_name")) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/" + i0,
                                  schemaPath:
                                    "https://jtsc-schemas.org/entities/name/init.schema.json/additionalProperties",
                                  keyword: "additionalProperties",
                                  params: { additionalProperty: key1 },
                                  message:
                                    "must NOT have additional properties",
                                },
                              ];
                              return false;
                              break;
                            }
                          }
                          if (_errs7 === errors) {
                            if (data1.full_name !== undefined) {
                              let data2 = data1.full_name;
                              const _errs8 = errors;
                              if (typeof data2 !== "string") {
                                let dataType0 = typeof data2;
                                let coerced0 = undefined;
                                if (!(coerced0 !== undefined)) {
                                  if (
                                    dataType0 == "number" ||
                                    dataType0 == "boolean"
                                  ) {
                                    coerced0 = "" + data2;
                                  } else if (data2 === null) {
                                    coerced0 = "";
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath +
                                          "/data/" +
                                          i0 +
                                          "/full_name",
                                        schemaPath:
                                          "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced0 !== undefined) {
                                  data2 = coerced0;
                                  if (data1 !== undefined) {
                                    data1["full_name"] = coerced0;
                                  }
                                }
                              }
                              if (errors === _errs8) {
                                if (typeof data2 === "string") {
                                  if (func1(data2) > 747) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath +
                                          "/data/" +
                                          i0 +
                                          "/full_name",
                                        schemaPath:
                                          "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 747 },
                                        message:
                                          "must NOT have more than 747 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func1(data2) < 1) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath +
                                            "/data/" +
                                            i0 +
                                            "/full_name",
                                          schemaPath:
                                            "https://jtsc-schemas.org/entities/name/init.schema.json/properties/full_name/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      } else {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/data/" + i0,
                            schemaPath:
                              "https://jtsc-schemas.org/entities/name/init.schema.json/type",
                            keyword: "type",
                            params: { type: "object" },
                            message: "must be object",
                          },
                        ];
                        return false;
                      }
                    }
                    var valid1 = _errs4 === errors;
                    if (!valid1) {
                      break;
                    }
                  }
                }
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/data",
                    schemaPath: "#/properties/data/type",
                    keyword: "type",
                    params: { type: "array" },
                    message: "must be array",
                  },
                ];
                return false;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
