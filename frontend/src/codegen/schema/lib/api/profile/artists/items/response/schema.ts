/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileArtistItemsGetSchema = {
  $id: "https://jtsc-schemas.org/api/profile/artists/items/response.schema.json",
  title: "APIResponseProfileArtistItemsGet",
  description: "A successful response for profile artists.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "artists"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        artists: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
