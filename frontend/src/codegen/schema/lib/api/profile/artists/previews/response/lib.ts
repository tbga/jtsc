/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileArtistPreviewsGetSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileArtistPreviewsGet } from "./types";
export const validateAPIResponseProfileArtistPreviewsGet =
  createValidator<IAPIResponseProfileArtistPreviewsGet>(
    validate as ValidateFunction<IAPIResponseProfileArtistPreviewsGet>,
    aPIResponseProfileArtistPreviewsGetSchema as unknown as IJSONSchema
  );
