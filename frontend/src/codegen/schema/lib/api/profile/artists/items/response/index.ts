/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileArtistItemsGetSchema } from "./schema";
export { validateAPIResponseProfileArtistItemsGet } from "./lib";
export type { IAPIResponseProfileArtistItemsGet } from "./types";
