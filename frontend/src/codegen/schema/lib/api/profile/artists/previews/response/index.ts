/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileArtistPreviewsGetSchema } from "./schema";
export { validateAPIResponseProfileArtistPreviewsGet } from "./lib";
export type { IAPIResponseProfileArtistPreviewsGet } from "./types";
