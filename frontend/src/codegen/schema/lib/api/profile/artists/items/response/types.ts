/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * A successful response for profile artists.
 */
export interface IAPIResponseProfileArtistItemsGet {
  is_successful: true;
  data: {
    pagination: IPagination;
    artists: IEntityItem[];
  };
}
