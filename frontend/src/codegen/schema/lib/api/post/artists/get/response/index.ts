/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostArtistsPreviewsSchema } from "./schema";
export { validateAPIResponsePostArtistsPreviews } from "./lib";
export type { IAPIResponsePostArtistsPreviews } from "./types";
