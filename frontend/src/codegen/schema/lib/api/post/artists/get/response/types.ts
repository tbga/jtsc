/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IArtistPreview } from "#codegen/schema/lib/entities/artist/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePostArtistsPreviews {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * @minItems 1
     */
    artists: [IArtistPreview, ...IArtistPreview[]];
  };
}
