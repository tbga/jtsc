/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostArtistsPreviewsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostArtistsPreviews } from "./types";
export const validateAPIResponsePostArtistsPreviews =
  createValidator<IAPIResponsePostArtistsPreviews>(
    validate as ValidateFunction<IAPIResponsePostArtistsPreviews>,
    aPIResponsePostArtistsPreviewsSchema as unknown as IJSONSchema
  );
