/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostReleasePreviewsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostReleasePreviews } from "./types";
export const validateAPIResponsePostReleasePreviews =
  createValidator<IAPIResponsePostReleasePreviews>(
    validate as ValidateFunction<IAPIResponsePostReleasePreviews>,
    aPIResponsePostReleasePreviewsSchema as unknown as IJSONSchema
  );
