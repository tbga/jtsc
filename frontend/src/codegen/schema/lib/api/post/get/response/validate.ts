/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/post/get/response.schema.json",
  title: "APIResponsePostGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: { $ref: "https://jtsc-schemas.org/entities/post/entity.schema.json" },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/post/entity.schema.json",
  title: "Post",
  description: "The post details.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const schema36 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const formats0 = require("ajv-formats/dist/formats").fullFormats["date-time"];
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/post/entity.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.created_at === undefined && (missing0 = "created_at")) ||
        (data.updated_at === undefined && (missing0 = "updated_at"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "created_at" ||
              key0 === "updated_at" ||
              key0 === "name" ||
              key0 === "title"
            )
          ) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.created_at !== undefined) {
              let data1 = data.created_at;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/created_at",
                        schemaPath:
                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["created_at"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (errors === _errs6) {
                  if (typeof data1 === "string") {
                    if (func1(data1) > 29) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/created_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 29 },
                          message: "must NOT have more than 29 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data1) < 29) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/created_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 29 },
                            message: "must NOT have fewer than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (!formats0.validate(data1)) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/created_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                              keyword: "format",
                              params: { format: "date-time" },
                              message:
                                'must match format "' + "date-time" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.updated_at !== undefined) {
                let data2 = data.updated_at;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/updated_at",
                          schemaPath:
                            "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["updated_at"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (errors === _errs9) {
                    if (typeof data2 === "string") {
                      if (func1(data2) > 29) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/updated_at",
                            schemaPath:
                              "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 29 },
                            message: "must NOT have more than 29 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data2) < 29) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/updated_at",
                              schemaPath:
                                "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 29 },
                              message: "must NOT have fewer than 29 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.validate(data2)) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/updated_at",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                keyword: "format",
                                params: { format: "date-time" },
                                message:
                                  'must match format "' + "date-time" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 216) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 216 },
                            message: "must NOT have more than 216 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/name",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.title !== undefined) {
                    let data4 = data.title;
                    const _errs14 = errors;
                    const _errs15 = errors;
                    if (typeof data4 !== "string") {
                      let dataType4 = typeof data4;
                      let coerced4 = undefined;
                      if (!(coerced4 !== undefined)) {
                        if (dataType4 == "number" || dataType4 == "boolean") {
                          coerced4 = "" + data4;
                        } else if (data4 === null) {
                          coerced4 = "";
                        } else {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced4 !== undefined) {
                        data4 = coerced4;
                        if (data !== undefined) {
                          data["title"] = coerced4;
                        }
                      }
                    }
                    if (errors === _errs15) {
                      if (typeof data4 === "string") {
                        if (func1(data4) > 216) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/title",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 216 },
                              message: "must NOT have more than 216 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data4) < 1) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/post/get/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              const _errs4 = errors;
              if (
                !validate21(data.data, {
                  instancePath: instancePath + "/data",
                  parentData: data,
                  parentDataProperty: "data",
                  rootData,
                  dynamicAnchors,
                })
              ) {
                vErrors =
                  vErrors === null
                    ? validate21.errors
                    : vErrors.concat(validate21.errors);
                errors = vErrors.length;
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
