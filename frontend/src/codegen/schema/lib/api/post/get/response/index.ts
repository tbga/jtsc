/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostGetSchema } from "./schema";
export { validateAPIResponsePostGet } from "./lib";
export type { IAPIResponsePostGet } from "./types";
