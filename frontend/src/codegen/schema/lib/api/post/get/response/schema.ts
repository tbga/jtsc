/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePostGetSchema = {
  $id: "https://jtsc-schemas.org/api/post/get/response.schema.json",
  title: "APIResponsePostGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/post/entity.schema.json",
    },
  },
} as const;
