/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/paginated-response.schema.json",
  title: "APIResponsePaginated",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "items"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        items: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
          },
        },
      },
    },
  },
};
const schema36 = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    { type: "null" },
    { type: "boolean" },
    { type: "number" },
    { type: "string" },
    { type: "object" },
    { type: "array" },
    { type: "array", minItems: 1 },
  ],
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
  title: "Pagination",
  description: "Pagination information for the collection.",
  type: "object",
  required: ["total_count", "limit", "total_pages", "current_page"],
  additionalProperties: false,
  properties: {
    current_page: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    total_count: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    limit: { type: "integer" },
    total_pages: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="http://jtsc-schemas.org/types/pagination/pagination.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.total_count === undefined && (missing0 = "total_count")) ||
        (data.limit === undefined && (missing0 = "limit")) ||
        (data.total_pages === undefined && (missing0 = "total_pages")) ||
        (data.current_page === undefined && (missing0 = "current_page"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "current_page" ||
              key0 === "total_count" ||
              key0 === "limit" ||
              key0 === "total_pages"
            )
          ) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.current_page !== undefined) {
            let data0 = data.current_page;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/current_page",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["current_page"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/current_page",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/current_page",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.total_count !== undefined) {
              let data1 = data.total_count;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/total_count",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["total_count"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 19) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/total_count",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 19 },
                        message: "must NOT have more than 19 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 1) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/total_count",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.limit !== undefined) {
                let data2 = data.limit;
                const _errs8 = errors;
                if (
                  !(
                    typeof data2 == "number" &&
                    !(data2 % 1) &&
                    !isNaN(data2) &&
                    isFinite(data2)
                  )
                ) {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (
                      dataType2 === "boolean" ||
                      data2 === null ||
                      (dataType2 === "string" &&
                        data2 &&
                        data2 == +data2 &&
                        !(data2 % 1))
                    ) {
                      coerced2 = +data2;
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/limit",
                          schemaPath: "#/properties/limit/type",
                          keyword: "type",
                          params: { type: "integer" },
                          message: "must be integer",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["limit"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.total_pages !== undefined) {
                  let data3 = data.total_pages;
                  const _errs10 = errors;
                  const _errs11 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/total_pages",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["total_pages"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs11) {
                    if (typeof data3 === "string") {
                      if (func1(data3) > 19) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/total_pages",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 19 },
                            message: "must NOT have more than 19 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data3) < 1) {
                          validate21.errors = [
                            {
                              instancePath: instancePath + "/total_pages",
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 1 },
                              message: "must NOT have fewer than 1 characters",
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs10 === errors;
                } else {
                  var valid0 = true;
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/paginated-response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              if (errors === _errs4) {
                if (
                  data1 &&
                  typeof data1 == "object" &&
                  !Array.isArray(data1)
                ) {
                  let missing1;
                  if (
                    (data1.pagination === undefined &&
                      (missing1 = "pagination")) ||
                    (data1.items === undefined && (missing1 = "items"))
                  ) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath: "#/properties/data/required",
                        keyword: "required",
                        params: { missingProperty: missing1 },
                        message:
                          "must have required property '" + missing1 + "'",
                      },
                    ];
                    return false;
                  } else {
                    const _errs6 = errors;
                    for (const key1 in data1) {
                      if (!(key1 === "pagination" || key1 === "items")) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/data",
                            schemaPath:
                              "#/properties/data/additionalProperties",
                            keyword: "additionalProperties",
                            params: { additionalProperty: key1 },
                            message: "must NOT have additional properties",
                          },
                        ];
                        return false;
                        break;
                      }
                    }
                    if (_errs6 === errors) {
                      if (data1.pagination !== undefined) {
                        const _errs7 = errors;
                        if (
                          !validate21(data1.pagination, {
                            instancePath: instancePath + "/data/pagination",
                            parentData: data1,
                            parentDataProperty: "pagination",
                            rootData,
                            dynamicAnchors,
                          })
                        ) {
                          vErrors =
                            vErrors === null
                              ? validate21.errors
                              : vErrors.concat(validate21.errors);
                          errors = vErrors.length;
                        }
                        var valid1 = _errs7 === errors;
                      } else {
                        var valid1 = true;
                      }
                      if (valid1) {
                        if (data1.items !== undefined) {
                          let data3 = data1.items;
                          const _errs8 = errors;
                          if (errors === _errs8) {
                            if (Array.isArray(data3)) {
                              if (data3.length < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/data/items",
                                    schemaPath:
                                      "#/properties/data/properties/items/minItems",
                                    keyword: "minItems",
                                    params: { limit: 1 },
                                    message: "must NOT have fewer than 1 items",
                                  },
                                ];
                                return false;
                              } else {
                                var valid2 = true;
                                const len0 = data3.length;
                                for (let i0 = 0; i0 < len0; i0++) {
                                  let data4 = data3[i0];
                                  const _errs10 = errors;
                                  const _errs12 = errors;
                                  let valid4 = false;
                                  const _errs13 = errors;
                                  if (data4 !== null) {
                                    let coerced1 = undefined;
                                    if (!(coerced1 !== undefined)) {
                                      if (
                                        data4 === "" ||
                                        data4 === 0 ||
                                        data4 === false
                                      ) {
                                        coerced1 = null;
                                      } else {
                                        const err0 = {
                                          instancePath:
                                            instancePath + "/data/items/" + i0,
                                          schemaPath:
                                            "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/0/type",
                                          keyword: "type",
                                          params: { type: "null" },
                                          message: "must be null",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err0];
                                        } else {
                                          vErrors.push(err0);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (coerced1 !== undefined) {
                                      data4 = coerced1;
                                      if (data3 !== undefined) {
                                        data3[i0] = coerced1;
                                      }
                                    }
                                  }
                                  var _valid0 = _errs13 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs15 = errors;
                                  if (typeof data4 !== "boolean") {
                                    let coerced2 = undefined;
                                    if (!(coerced2 !== undefined)) {
                                      if (
                                        data4 === "false" ||
                                        data4 === 0 ||
                                        data4 === null
                                      ) {
                                        coerced2 = false;
                                      } else if (
                                        data4 === "true" ||
                                        data4 === 1
                                      ) {
                                        coerced2 = true;
                                      } else {
                                        const err1 = {
                                          instancePath:
                                            instancePath + "/data/items/" + i0,
                                          schemaPath:
                                            "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/1/type",
                                          keyword: "type",
                                          params: { type: "boolean" },
                                          message: "must be boolean",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err1];
                                        } else {
                                          vErrors.push(err1);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (coerced2 !== undefined) {
                                      data4 = coerced2;
                                      if (data3 !== undefined) {
                                        data3[i0] = coerced2;
                                      }
                                    }
                                  }
                                  var _valid0 = _errs15 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs17 = errors;
                                  if (
                                    !(
                                      typeof data4 == "number" &&
                                      isFinite(data4)
                                    )
                                  ) {
                                    let dataType3 = typeof data4;
                                    let coerced3 = undefined;
                                    if (!(coerced3 !== undefined)) {
                                      if (
                                        dataType3 == "boolean" ||
                                        data4 === null ||
                                        (dataType3 == "string" &&
                                          data4 &&
                                          data4 == +data4)
                                      ) {
                                        coerced3 = +data4;
                                      } else {
                                        const err2 = {
                                          instancePath:
                                            instancePath + "/data/items/" + i0,
                                          schemaPath:
                                            "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/2/type",
                                          keyword: "type",
                                          params: { type: "number" },
                                          message: "must be number",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err2];
                                        } else {
                                          vErrors.push(err2);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (coerced3 !== undefined) {
                                      data4 = coerced3;
                                      if (data3 !== undefined) {
                                        data3[i0] = coerced3;
                                      }
                                    }
                                  }
                                  var _valid0 = _errs17 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs19 = errors;
                                  if (typeof data4 !== "string") {
                                    let dataType4 = typeof data4;
                                    let coerced4 = undefined;
                                    if (!(coerced4 !== undefined)) {
                                      if (
                                        dataType4 == "number" ||
                                        dataType4 == "boolean"
                                      ) {
                                        coerced4 = "" + data4;
                                      } else if (data4 === null) {
                                        coerced4 = "";
                                      } else {
                                        const err3 = {
                                          instancePath:
                                            instancePath + "/data/items/" + i0,
                                          schemaPath:
                                            "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/3/type",
                                          keyword: "type",
                                          params: { type: "string" },
                                          message: "must be string",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err3];
                                        } else {
                                          vErrors.push(err3);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (coerced4 !== undefined) {
                                      data4 = coerced4;
                                      if (data3 !== undefined) {
                                        data3[i0] = coerced4;
                                      }
                                    }
                                  }
                                  var _valid0 = _errs19 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs21 = errors;
                                  if (
                                    !(
                                      data4 &&
                                      typeof data4 == "object" &&
                                      !Array.isArray(data4)
                                    )
                                  ) {
                                    const err4 = {
                                      instancePath:
                                        instancePath + "/data/items/" + i0,
                                      schemaPath:
                                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/4/type",
                                      keyword: "type",
                                      params: { type: "object" },
                                      message: "must be object",
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err4];
                                    } else {
                                      vErrors.push(err4);
                                    }
                                    errors++;
                                  }
                                  var _valid0 = _errs21 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs23 = errors;
                                  if (!Array.isArray(data4)) {
                                    const err5 = {
                                      instancePath:
                                        instancePath + "/data/items/" + i0,
                                      schemaPath:
                                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/5/type",
                                      keyword: "type",
                                      params: { type: "array" },
                                      message: "must be array",
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err5];
                                    } else {
                                      vErrors.push(err5);
                                    }
                                    errors++;
                                  }
                                  var _valid0 = _errs23 === errors;
                                  valid4 = valid4 || _valid0;
                                  const _errs25 = errors;
                                  if (errors === _errs25) {
                                    if (Array.isArray(data4)) {
                                      if (data4.length < 1) {
                                        const err6 = {
                                          instancePath:
                                            instancePath + "/data/items/" + i0,
                                          schemaPath:
                                            "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/minItems",
                                          keyword: "minItems",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 items",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err6];
                                        } else {
                                          vErrors.push(err6);
                                        }
                                        errors++;
                                      }
                                    } else {
                                      const err7 = {
                                        instancePath:
                                          instancePath + "/data/items/" + i0,
                                        schemaPath:
                                          "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/type",
                                        keyword: "type",
                                        params: { type: "array" },
                                        message: "must be array",
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err7];
                                      } else {
                                        vErrors.push(err7);
                                      }
                                      errors++;
                                    }
                                  }
                                  var _valid0 = _errs25 === errors;
                                  valid4 = valid4 || _valid0;
                                  if (!valid4) {
                                    const err8 = {
                                      instancePath:
                                        instancePath + "/data/items/" + i0,
                                      schemaPath:
                                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf",
                                      keyword: "anyOf",
                                      params: {},
                                      message: "must match a schema in anyOf",
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err8];
                                    } else {
                                      vErrors.push(err8);
                                    }
                                    errors++;
                                    validate20.errors = vErrors;
                                    return false;
                                  } else {
                                    errors = _errs12;
                                    if (vErrors !== null) {
                                      if (_errs12) {
                                        vErrors.length = _errs12;
                                      } else {
                                        vErrors = null;
                                      }
                                    }
                                  }
                                  var valid2 = _errs10 === errors;
                                  if (!valid2) {
                                    break;
                                  }
                                }
                              }
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/items",
                                  schemaPath:
                                    "#/properties/data/properties/items/type",
                                  keyword: "type",
                                  params: { type: "array" },
                                  message: "must be array",
                                },
                              ];
                              return false;
                            }
                          }
                          var valid1 = _errs8 === errors;
                        } else {
                          var valid1 = true;
                        }
                      }
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/type",
                      keyword: "type",
                      params: { type: "object" },
                      message: "must be object",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
