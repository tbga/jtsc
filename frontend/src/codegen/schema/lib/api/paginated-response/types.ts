/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IJSONAny } from "#codegen/schema/lib/types/json/any";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePaginated {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * @minItems 1
     */
    items: [IJSONAny, ...IJSONAny[]];
  };
}
