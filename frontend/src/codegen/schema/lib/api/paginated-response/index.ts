/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePaginatedSchema } from "./schema";
export { validateAPIResponsePaginated } from "./lib";
export type { IAPIResponsePaginated } from "./types";
