/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePaginatedSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePaginated } from "./types";
export const validateAPIResponsePaginated =
  createValidator<IAPIResponsePaginated>(
    validate as ValidateFunction<IAPIResponsePaginated>,
    aPIResponsePaginatedSchema as unknown as IJSONSchema
  );
