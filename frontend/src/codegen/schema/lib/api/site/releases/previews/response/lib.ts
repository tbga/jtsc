/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSiteReleasesSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSiteReleases } from "./types";
export const validateAPIResponseSiteReleases =
  createValidator<IAPIResponseSiteReleases>(
    validate as ValidateFunction<IAPIResponseSiteReleases>,
    aPIResponseSiteReleasesSchema as unknown as IJSONSchema
  );
