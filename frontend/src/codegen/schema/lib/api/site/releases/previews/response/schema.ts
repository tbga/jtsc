/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSiteReleasesSchema = {
  $id: "https://jtsc-schemas.org/api/site/releases/previews/response.schema.json",
  title: "APIResponseSiteReleases",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "releases"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        releases: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
