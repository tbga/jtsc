/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSiteGetSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSiteGet } from "./types";
export const validateAPIResponseSiteGet = createValidator<IAPIResponseSiteGet>(
  validate as ValidateFunction<IAPIResponseSiteGet>,
  aPIResponseSiteGetSchema as unknown as IJSONSchema
);
