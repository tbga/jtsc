/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISite } from "#codegen/schema/lib/entities/site/entity";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSiteGet {
  is_successful: true;
  data: ISite;
}
