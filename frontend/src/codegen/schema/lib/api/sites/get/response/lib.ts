/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSitesGetSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSitesGet } from "./types";
export const validateAPIResponseSitesGet =
  createValidator<IAPIResponseSitesGet>(
    validate as ValidateFunction<IAPIResponseSitesGet>,
    aPIResponseSitesGetSchema as unknown as IJSONSchema
  );
