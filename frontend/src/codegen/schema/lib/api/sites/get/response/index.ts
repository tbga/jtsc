/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSitesGetSchema } from "./schema";
export { validateAPIResponseSitesGet } from "./lib";
export type { IAPIResponseSitesGet } from "./types";
