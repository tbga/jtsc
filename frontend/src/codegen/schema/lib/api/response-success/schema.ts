/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSuccessSchema = {
  $id: "https://jtsc-schemas.org/api/response-success.schema.json",
  title: "APIResponseSuccess",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
    },
  },
} as const;
