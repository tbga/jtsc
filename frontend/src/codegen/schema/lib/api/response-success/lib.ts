/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSuccessSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSuccess } from "./types";
export const validateAPIResponseSuccess = createValidator<IAPIResponseSuccess>(
  validate as ValidateFunction<IAPIResponseSuccess>,
  aPIResponseSuccessSchema as unknown as IJSONSchema
);
