/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/response-success.schema.json",
  title: "APIResponseSuccess",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: { $ref: "https://jtsc-schemas.org/types/json/any.schema.json" },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    { type: "null" },
    { type: "boolean" },
    { type: "number" },
    { type: "string" },
    { type: "object" },
    { type: "array" },
    { type: "array", minItems: 1 },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/response-success.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              const _errs6 = errors;
              let valid2 = false;
              const _errs7 = errors;
              if (data1 !== null) {
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (data1 === "" || data1 === 0 || data1 === false) {
                    coerced1 = null;
                  } else {
                    const err0 = {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/0/type",
                      keyword: "type",
                      params: { type: "null" },
                      message: "must be null",
                    };
                    if (vErrors === null) {
                      vErrors = [err0];
                    } else {
                      vErrors.push(err0);
                    }
                    errors++;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["data"] = coerced1;
                  }
                }
              }
              var _valid0 = _errs7 === errors;
              valid2 = valid2 || _valid0;
              const _errs9 = errors;
              if (typeof data1 !== "boolean") {
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (data1 === "false" || data1 === 0 || data1 === null) {
                    coerced2 = false;
                  } else if (data1 === "true" || data1 === 1) {
                    coerced2 = true;
                  } else {
                    const err1 = {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/1/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    };
                    if (vErrors === null) {
                      vErrors = [err1];
                    } else {
                      vErrors.push(err1);
                    }
                    errors++;
                  }
                }
                if (coerced2 !== undefined) {
                  data1 = coerced2;
                  if (data !== undefined) {
                    data["data"] = coerced2;
                  }
                }
              }
              var _valid0 = _errs9 === errors;
              valid2 = valid2 || _valid0;
              const _errs11 = errors;
              if (!(typeof data1 == "number" && isFinite(data1))) {
                let dataType3 = typeof data1;
                let coerced3 = undefined;
                if (!(coerced3 !== undefined)) {
                  if (
                    dataType3 == "boolean" ||
                    data1 === null ||
                    (dataType3 == "string" && data1 && data1 == +data1)
                  ) {
                    coerced3 = +data1;
                  } else {
                    const err2 = {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/2/type",
                      keyword: "type",
                      params: { type: "number" },
                      message: "must be number",
                    };
                    if (vErrors === null) {
                      vErrors = [err2];
                    } else {
                      vErrors.push(err2);
                    }
                    errors++;
                  }
                }
                if (coerced3 !== undefined) {
                  data1 = coerced3;
                  if (data !== undefined) {
                    data["data"] = coerced3;
                  }
                }
              }
              var _valid0 = _errs11 === errors;
              valid2 = valid2 || _valid0;
              const _errs13 = errors;
              if (typeof data1 !== "string") {
                let dataType4 = typeof data1;
                let coerced4 = undefined;
                if (!(coerced4 !== undefined)) {
                  if (dataType4 == "number" || dataType4 == "boolean") {
                    coerced4 = "" + data1;
                  } else if (data1 === null) {
                    coerced4 = "";
                  } else {
                    const err3 = {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/3/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    };
                    if (vErrors === null) {
                      vErrors = [err3];
                    } else {
                      vErrors.push(err3);
                    }
                    errors++;
                  }
                }
                if (coerced4 !== undefined) {
                  data1 = coerced4;
                  if (data !== undefined) {
                    data["data"] = coerced4;
                  }
                }
              }
              var _valid0 = _errs13 === errors;
              valid2 = valid2 || _valid0;
              const _errs15 = errors;
              if (
                !(data1 && typeof data1 == "object" && !Array.isArray(data1))
              ) {
                const err4 = {
                  instancePath: instancePath + "/data",
                  schemaPath:
                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/4/type",
                  keyword: "type",
                  params: { type: "object" },
                  message: "must be object",
                };
                if (vErrors === null) {
                  vErrors = [err4];
                } else {
                  vErrors.push(err4);
                }
                errors++;
              }
              var _valid0 = _errs15 === errors;
              valid2 = valid2 || _valid0;
              const _errs17 = errors;
              if (!Array.isArray(data1)) {
                const err5 = {
                  instancePath: instancePath + "/data",
                  schemaPath:
                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/5/type",
                  keyword: "type",
                  params: { type: "array" },
                  message: "must be array",
                };
                if (vErrors === null) {
                  vErrors = [err5];
                } else {
                  vErrors.push(err5);
                }
                errors++;
              }
              var _valid0 = _errs17 === errors;
              valid2 = valid2 || _valid0;
              const _errs19 = errors;
              if (errors === _errs19) {
                if (Array.isArray(data1)) {
                  if (data1.length < 1) {
                    const err6 = {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/minItems",
                      keyword: "minItems",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 items",
                    };
                    if (vErrors === null) {
                      vErrors = [err6];
                    } else {
                      vErrors.push(err6);
                    }
                    errors++;
                  }
                } else {
                  const err7 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/type",
                    keyword: "type",
                    params: { type: "array" },
                    message: "must be array",
                  };
                  if (vErrors === null) {
                    vErrors = [err7];
                  } else {
                    vErrors.push(err7);
                  }
                  errors++;
                }
              }
              var _valid0 = _errs19 === errors;
              valid2 = valid2 || _valid0;
              if (!valid2) {
                const err8 = {
                  instancePath: instancePath + "/data",
                  schemaPath:
                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf",
                  keyword: "anyOf",
                  params: {},
                  message: "must match a schema in anyOf",
                };
                if (vErrors === null) {
                  vErrors = [err8];
                } else {
                  vErrors.push(err8);
                }
                errors++;
                validate20.errors = vErrors;
                return false;
              } else {
                errors = _errs6;
                if (vErrors !== null) {
                  if (_errs6) {
                    vErrors.length = _errs6;
                  } else {
                    vErrors = null;
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
