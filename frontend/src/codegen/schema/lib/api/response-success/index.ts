/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSuccessSchema } from "./schema";
export { validateAPIResponseSuccess } from "./lib";
export type { IAPIResponseSuccess } from "./types";
