/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseStatsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseStats } from "./types";
export const validateAPIResponseStats = createValidator<IAPIResponseStats>(
  validate as ValidateFunction<IAPIResponseStats>,
  aPIResponseStatsSchema as unknown as IJSONSchema
);
