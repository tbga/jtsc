/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/stats/request.schema.json",
  title: "APIRequestStats",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: { $ref: "https://jtsc-schemas.org/types/json/any.schema.json" },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    { type: "null" },
    { type: "boolean" },
    { type: "number" },
    { type: "string" },
    { type: "object" },
    { type: "array" },
    { type: "array", minItems: 1 },
  ],
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/stats/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs4 = errors;
            let valid2 = false;
            const _errs5 = errors;
            if (data0 !== null) {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "" || data0 === 0 || data0 === false) {
                  coerced0 = null;
                } else {
                  const err0 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/0/type",
                    keyword: "type",
                    params: { type: "null" },
                    message: "must be null",
                  };
                  if (vErrors === null) {
                    vErrors = [err0];
                  } else {
                    vErrors.push(err0);
                  }
                  errors++;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["data"] = coerced0;
                }
              }
            }
            var _valid0 = _errs5 === errors;
            valid2 = valid2 || _valid0;
            const _errs7 = errors;
            if (typeof data0 !== "boolean") {
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced1 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced1 = true;
                } else {
                  const err1 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/1/type",
                    keyword: "type",
                    params: { type: "boolean" },
                    message: "must be boolean",
                  };
                  if (vErrors === null) {
                    vErrors = [err1];
                  } else {
                    vErrors.push(err1);
                  }
                  errors++;
                }
              }
              if (coerced1 !== undefined) {
                data0 = coerced1;
                if (data !== undefined) {
                  data["data"] = coerced1;
                }
              }
            }
            var _valid0 = _errs7 === errors;
            valid2 = valid2 || _valid0;
            const _errs9 = errors;
            if (!(typeof data0 == "number" && isFinite(data0))) {
              let dataType2 = typeof data0;
              let coerced2 = undefined;
              if (!(coerced2 !== undefined)) {
                if (
                  dataType2 == "boolean" ||
                  data0 === null ||
                  (dataType2 == "string" && data0 && data0 == +data0)
                ) {
                  coerced2 = +data0;
                } else {
                  const err2 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/2/type",
                    keyword: "type",
                    params: { type: "number" },
                    message: "must be number",
                  };
                  if (vErrors === null) {
                    vErrors = [err2];
                  } else {
                    vErrors.push(err2);
                  }
                  errors++;
                }
              }
              if (coerced2 !== undefined) {
                data0 = coerced2;
                if (data !== undefined) {
                  data["data"] = coerced2;
                }
              }
            }
            var _valid0 = _errs9 === errors;
            valid2 = valid2 || _valid0;
            const _errs11 = errors;
            if (typeof data0 !== "string") {
              let dataType3 = typeof data0;
              let coerced3 = undefined;
              if (!(coerced3 !== undefined)) {
                if (dataType3 == "number" || dataType3 == "boolean") {
                  coerced3 = "" + data0;
                } else if (data0 === null) {
                  coerced3 = "";
                } else {
                  const err3 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/3/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  };
                  if (vErrors === null) {
                    vErrors = [err3];
                  } else {
                    vErrors.push(err3);
                  }
                  errors++;
                }
              }
              if (coerced3 !== undefined) {
                data0 = coerced3;
                if (data !== undefined) {
                  data["data"] = coerced3;
                }
              }
            }
            var _valid0 = _errs11 === errors;
            valid2 = valid2 || _valid0;
            const _errs13 = errors;
            if (!(data0 && typeof data0 == "object" && !Array.isArray(data0))) {
              const err4 = {
                instancePath: instancePath + "/data",
                schemaPath:
                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/4/type",
                keyword: "type",
                params: { type: "object" },
                message: "must be object",
              };
              if (vErrors === null) {
                vErrors = [err4];
              } else {
                vErrors.push(err4);
              }
              errors++;
            }
            var _valid0 = _errs13 === errors;
            valid2 = valid2 || _valid0;
            const _errs15 = errors;
            if (!Array.isArray(data0)) {
              const err5 = {
                instancePath: instancePath + "/data",
                schemaPath:
                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/5/type",
                keyword: "type",
                params: { type: "array" },
                message: "must be array",
              };
              if (vErrors === null) {
                vErrors = [err5];
              } else {
                vErrors.push(err5);
              }
              errors++;
            }
            var _valid0 = _errs15 === errors;
            valid2 = valid2 || _valid0;
            const _errs17 = errors;
            if (errors === _errs17) {
              if (Array.isArray(data0)) {
                if (data0.length < 1) {
                  const err6 = {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/minItems",
                    keyword: "minItems",
                    params: { limit: 1 },
                    message: "must NOT have fewer than 1 items",
                  };
                  if (vErrors === null) {
                    vErrors = [err6];
                  } else {
                    vErrors.push(err6);
                  }
                  errors++;
                }
              } else {
                const err7 = {
                  instancePath: instancePath + "/data",
                  schemaPath:
                    "https://jtsc-schemas.org/types/json/any.schema.json/anyOf/6/type",
                  keyword: "type",
                  params: { type: "array" },
                  message: "must be array",
                };
                if (vErrors === null) {
                  vErrors = [err7];
                } else {
                  vErrors.push(err7);
                }
                errors++;
              }
            }
            var _valid0 = _errs17 === errors;
            valid2 = valid2 || _valid0;
            if (!valid2) {
              const err8 = {
                instancePath: instancePath + "/data",
                schemaPath:
                  "https://jtsc-schemas.org/types/json/any.schema.json/anyOf",
                keyword: "anyOf",
                params: {},
                message: "must match a schema in anyOf",
              };
              if (vErrors === null) {
                vErrors = [err8];
              } else {
                vErrors.push(err8);
              }
              errors++;
              validate20.errors = vErrors;
              return false;
            } else {
              errors = _errs4;
              if (vErrors !== null) {
                if (_errs4) {
                  vErrors.length = _errs4;
                } else {
                  vErrors = null;
                }
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
