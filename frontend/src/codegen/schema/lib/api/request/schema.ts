/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestStatsSchema = {
  $id: "https://jtsc-schemas.org/api/stats/request.schema.json",
  title: "APIRequestStats",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
    },
  },
} as const;
