/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestStatsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestStats } from "./types";
export const validateAPIRequestStats = createValidator<IAPIRequestStats>(
  validate as ValidateFunction<IAPIRequestStats>,
  aPIRequestStatsSchema as unknown as IJSONSchema
);
