/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IJSONAny } from "#codegen/schema/lib/types/json/any";

/**
 * Body of the API request.
 */
export interface IAPIRequestStats {
  data: IJSONAny;
}
