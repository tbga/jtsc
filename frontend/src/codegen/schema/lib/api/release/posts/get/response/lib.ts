/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleasePostsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleasePosts } from "./types";
export const validateAPIResponseReleasePosts =
  createValidator<IAPIResponseReleasePosts>(
    validate as ValidateFunction<IAPIResponseReleasePosts>,
    aPIResponseReleasePostsSchema as unknown as IJSONSchema
  );
