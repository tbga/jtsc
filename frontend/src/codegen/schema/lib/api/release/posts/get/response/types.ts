/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IPostPreview } from "#codegen/schema/lib/entities/post/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseReleasePosts {
  is_successful: true;
  data: {
    pagination: IPagination;
    posts: IPostPreview[];
  };
}
