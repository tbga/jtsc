/*
 * This file was generated automatically, do not edit it by hand.
 */
export const getReleaseResponseSchema = {
  $id: "https://jtsc-schemas.org/api/release/get/response.schema.json",
  title: "GetReleaseResponse",
  description: "Release info.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/entity.schema.json",
    },
  },
} as const;
