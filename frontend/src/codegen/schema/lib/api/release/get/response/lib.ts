/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { getReleaseResponseSchema } from "./schema";
import { validate } from "./validate";
import { IGetReleaseResponse } from "./types";
export const validateGetReleaseResponse = createValidator<IGetReleaseResponse>(
  validate as ValidateFunction<IGetReleaseResponse>,
  getReleaseResponseSchema as unknown as IJSONSchema
);
