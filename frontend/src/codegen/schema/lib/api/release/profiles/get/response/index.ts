/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseReleaseProfilesSchema } from "./schema";
export { validateAPIResponseReleaseProfiles } from "./lib";
export type { IAPIResponseReleaseProfiles } from "./types";
