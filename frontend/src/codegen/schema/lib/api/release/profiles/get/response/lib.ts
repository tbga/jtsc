/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleaseProfilesSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleaseProfiles } from "./types";
export const validateAPIResponseReleaseProfiles =
  createValidator<IAPIResponseReleaseProfiles>(
    validate as ValidateFunction<IAPIResponseReleaseProfiles>,
    aPIResponseReleaseProfilesSchema as unknown as IJSONSchema
  );
