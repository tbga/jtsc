/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseFailureSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseFailure } from "./types";
export const validateAPIResponseFailure = createValidator<IAPIResponseFailure>(
  validate as ValidateFunction<IAPIResponseFailure>,
  aPIResponseFailureSchema as unknown as IJSONSchema
);
