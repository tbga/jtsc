/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Body of the failed API response.
 */
export interface IAPIResponseFailure {
  is_successful: false;
  errors: string[];
}
