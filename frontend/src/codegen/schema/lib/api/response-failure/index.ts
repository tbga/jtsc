/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseFailureSchema } from "./schema";
export { validateAPIResponseFailure } from "./lib";
export type { IAPIResponseFailure } from "./types";
