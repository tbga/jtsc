/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/authentication/login/request.schema.json",
  title: "APIRequestAccountLogin",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/login.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/account/login.schema.json",
  title: "AccountLogin",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: { type: "string", minLength: 5, maxLength: 25 },
    password: { type: "string", minLength: 10, maxLength: 72 },
  },
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/authentication/login/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs3 = errors;
            if (errors === _errs3) {
              if (data0 && typeof data0 == "object" && !Array.isArray(data0)) {
                let missing1;
                if (
                  (data0.login === undefined && (missing1 = "login")) ||
                  (data0.password === undefined && (missing1 = "password"))
                ) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath:
                        "https://jtsc-schemas.org/entities/account/login.schema.json/required",
                      keyword: "required",
                      params: { missingProperty: missing1 },
                      message: "must have required property '" + missing1 + "'",
                    },
                  ];
                  return false;
                } else {
                  const _errs5 = errors;
                  for (const key1 in data0) {
                    if (!(key1 === "login" || key1 === "password")) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/data",
                          schemaPath:
                            "https://jtsc-schemas.org/entities/account/login.schema.json/additionalProperties",
                          keyword: "additionalProperties",
                          params: { additionalProperty: key1 },
                          message: "must NOT have additional properties",
                        },
                      ];
                      return false;
                      break;
                    }
                  }
                  if (_errs5 === errors) {
                    if (data0.login !== undefined) {
                      let data1 = data0.login;
                      const _errs6 = errors;
                      if (typeof data1 !== "string") {
                        let dataType0 = typeof data1;
                        let coerced0 = undefined;
                        if (!(coerced0 !== undefined)) {
                          if (dataType0 == "number" || dataType0 == "boolean") {
                            coerced0 = "" + data1;
                          } else if (data1 === null) {
                            coerced0 = "";
                          } else {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/data/login",
                                schemaPath:
                                  "https://jtsc-schemas.org/entities/account/login.schema.json/properties/login/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced0 !== undefined) {
                          data1 = coerced0;
                          if (data0 !== undefined) {
                            data0["login"] = coerced0;
                          }
                        }
                      }
                      if (errors === _errs6) {
                        if (typeof data1 === "string") {
                          if (func1(data1) > 25) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/data/login",
                                schemaPath:
                                  "https://jtsc-schemas.org/entities/account/login.schema.json/properties/login/maxLength",
                                keyword: "maxLength",
                                params: { limit: 25 },
                                message:
                                  "must NOT have more than 25 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data1) < 5) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/login",
                                  schemaPath:
                                    "https://jtsc-schemas.org/entities/account/login.schema.json/properties/login/minLength",
                                  keyword: "minLength",
                                  params: { limit: 5 },
                                  message:
                                    "must NOT have fewer than 5 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid2 = _errs6 === errors;
                    } else {
                      var valid2 = true;
                    }
                    if (valid2) {
                      if (data0.password !== undefined) {
                        let data2 = data0.password;
                        const _errs8 = errors;
                        if (typeof data2 !== "string") {
                          let dataType1 = typeof data2;
                          let coerced1 = undefined;
                          if (!(coerced1 !== undefined)) {
                            if (
                              dataType1 == "number" ||
                              dataType1 == "boolean"
                            ) {
                              coerced1 = "" + data2;
                            } else if (data2 === null) {
                              coerced1 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/password",
                                  schemaPath:
                                    "https://jtsc-schemas.org/entities/account/login.schema.json/properties/password/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced1 !== undefined) {
                            data2 = coerced1;
                            if (data0 !== undefined) {
                              data0["password"] = coerced1;
                            }
                          }
                        }
                        if (errors === _errs8) {
                          if (typeof data2 === "string") {
                            if (func1(data2) > 72) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/password",
                                  schemaPath:
                                    "https://jtsc-schemas.org/entities/account/login.schema.json/properties/password/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 72 },
                                  message:
                                    "must NOT have more than 72 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data2) < 10) {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/data/password",
                                    schemaPath:
                                      "https://jtsc-schemas.org/entities/account/login.schema.json/properties/password/minLength",
                                    keyword: "minLength",
                                    params: { limit: 10 },
                                    message:
                                      "must NOT have fewer than 10 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid2 = _errs8 === errors;
                      } else {
                        var valid2 = true;
                      }
                    }
                  }
                }
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/entities/account/login.schema.json/type",
                    keyword: "type",
                    params: { type: "object" },
                    message: "must be object",
                  },
                ];
                return false;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
