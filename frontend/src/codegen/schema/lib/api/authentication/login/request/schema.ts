/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestAccountLoginSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/login/request.schema.json",
  title: "APIRequestAccountLogin",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/login.schema.json",
    },
  },
} as const;
