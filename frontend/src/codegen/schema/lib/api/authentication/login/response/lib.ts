/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseAccountLoginSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseAccountLogin } from "./types";
export const validateAPIResponseAccountLogin =
  createValidator<IAPIResponseAccountLogin>(
    validate as ValidateFunction<IAPIResponseAccountLogin>,
    aPIResponseAccountLoginSchema as unknown as IJSONSchema
  );
