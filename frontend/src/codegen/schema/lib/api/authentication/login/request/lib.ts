/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestAccountLoginSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestAccountLogin } from "./types";
export const validateAPIRequestAccountLogin =
  createValidator<IAPIRequestAccountLogin>(
    validate as ValidateFunction<IAPIRequestAccountLogin>,
    aPIRequestAccountLoginSchema as unknown as IJSONSchema
  );
