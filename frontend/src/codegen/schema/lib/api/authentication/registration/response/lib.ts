/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseAccountRegistrationSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseAccountRegistration } from "./types";
export const validateAPIResponseAccountRegistration =
  createValidator<IAPIResponseAccountRegistration>(
    validate as ValidateFunction<IAPIResponseAccountRegistration>,
    aPIResponseAccountRegistrationSchema as unknown as IJSONSchema
  );
