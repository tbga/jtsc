/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestAccountRegistrationSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/registration/request.schema.json",
  title: "APIRequestAccountRegistration",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/init.schema.json",
    },
  },
} as const;
