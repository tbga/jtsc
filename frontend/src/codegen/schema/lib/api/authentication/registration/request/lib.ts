/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestAccountRegistrationSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestAccountRegistration } from "./types";
export const validateAPIRequestAccountRegistration =
  createValidator<IAPIRequestAccountRegistration>(
    validate as ValidateFunction<IAPIRequestAccountRegistration>,
    aPIRequestAccountRegistrationSchema as unknown as IJSONSchema
  );
