/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestAccountRegistrationSchema } from "./schema";
export { validateAPIRequestAccountRegistration } from "./lib";
export type { IAPIRequestAccountRegistration } from "./types";
