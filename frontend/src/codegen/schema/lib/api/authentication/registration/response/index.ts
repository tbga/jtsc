/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseAccountRegistrationSchema } from "./schema";
export { validateAPIResponseAccountRegistration } from "./lib";
export type { IAPIResponseAccountRegistration } from "./types";
