/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IAccountInit } from "#codegen/schema/lib/entities/account/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestAccountRegistration {
  data: IAccountInit;
}
