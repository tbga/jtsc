/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseAccountLogoutSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseAccountLogout } from "./types";
export const validateAPIResponseAccountLogout =
  createValidator<IAPIResponseAccountLogout>(
    validate as ValidateFunction<IAPIResponseAccountLogout>,
    aPIResponseAccountLogoutSchema as unknown as IJSONSchema
  );
