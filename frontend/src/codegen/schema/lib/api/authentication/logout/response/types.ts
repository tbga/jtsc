/*
 * This file was generated automatically, do not edit it by hand.
 */

/**
 * Body of the successful API response.
 */
export interface IAPIResponseAccountLogout {
  is_successful: true;
  data: null;
}
