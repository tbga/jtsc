/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseAccountLogoutSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/logout/response.schema.json",
  title: "APIResponseAccountLogout",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      const: null,
    },
  },
} as const;
