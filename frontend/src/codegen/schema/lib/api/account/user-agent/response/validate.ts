/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/user-agent/response.schema.json",
  title: "APIResponseUserAgent",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/user-agent/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["data"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
