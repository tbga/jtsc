/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseUserAgentSchema } from "./schema";
export { validateAPIResponseUserAgent } from "./lib";
export type { IAPIResponseUserAgent } from "./types";
