/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseUserAgentSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseUserAgent } from "./types";
export const validateAPIResponseUserAgent =
  createValidator<IAPIResponseUserAgent>(
    validate as ValidateFunction<IAPIResponseUserAgent>,
    aPIResponseUserAgentSchema as unknown as IJSONSchema
  );
