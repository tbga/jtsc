/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseUserAgentSchema = {
  $id: "https://jtsc-schemas.org/api/account/user-agent/response.schema.json",
  title: "APIResponseUserAgent",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
  },
} as const;
