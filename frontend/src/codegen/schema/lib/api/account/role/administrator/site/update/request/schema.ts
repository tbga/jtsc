/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/update/request.schema.json",
  title: "APIRequestSiteUpdate",
  description: "Site update request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/update.schema.json",
    },
  },
} as const;
