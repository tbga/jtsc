/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/create/request.schema.json",
  title: "APIRequestProfileCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/profile/init.schema.json",
    },
  },
} as const;
