/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/site/update/request.schema.json",
  title: "APIRequestPublicImportSiteUpdate",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: { is_approved: { type: "boolean" } },
    },
  },
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/public-import/site/update/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs2 = errors;
            if (errors === _errs2) {
              if (data0 && typeof data0 == "object" && !Array.isArray(data0)) {
                if (Object.keys(data0).length < 1) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/minProperties",
                      keyword: "minProperties",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 properties",
                    },
                  ];
                  return false;
                } else {
                  const _errs4 = errors;
                  for (const key1 in data0) {
                    if (!(key1 === "is_approved")) {
                      validate20.errors = [
                        {
                          instancePath: instancePath + "/data",
                          schemaPath: "#/properties/data/additionalProperties",
                          keyword: "additionalProperties",
                          params: { additionalProperty: key1 },
                          message: "must NOT have additional properties",
                        },
                      ];
                      return false;
                      break;
                    }
                  }
                  if (_errs4 === errors) {
                    if (data0.is_approved !== undefined) {
                      let data1 = data0.is_approved;
                      if (typeof data1 !== "boolean") {
                        let coerced0 = undefined;
                        if (!(coerced0 !== undefined)) {
                          if (
                            data1 === "false" ||
                            data1 === 0 ||
                            data1 === null
                          ) {
                            coerced0 = false;
                          } else if (data1 === "true" || data1 === 1) {
                            coerced0 = true;
                          } else {
                            validate20.errors = [
                              {
                                instancePath:
                                  instancePath + "/data/is_approved",
                                schemaPath:
                                  "#/properties/data/properties/is_approved/type",
                                keyword: "type",
                                params: { type: "boolean" },
                                message: "must be boolean",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced0 !== undefined) {
                          data1 = coerced0;
                          if (data0 !== undefined) {
                            data0["is_approved"] = coerced0;
                          }
                        }
                      }
                    }
                  }
                }
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/data",
                    schemaPath: "#/properties/data/type",
                    keyword: "type",
                    params: { type: "object" },
                    message: "must be object",
                  },
                ];
                return false;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
