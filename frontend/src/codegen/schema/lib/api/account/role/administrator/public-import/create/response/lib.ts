/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicImportCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicImportCreate } from "./types";
export const validateAPIResponsePublicImportCreate =
  createValidator<IAPIResponsePublicImportCreate>(
    validate as ValidateFunction<IAPIResponsePublicImportCreate>,
    aPIResponsePublicImportCreateSchema as unknown as IJSONSchema
  );
