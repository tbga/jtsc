/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPostReleasesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPostReleasesAdd } from "./types";
export const validateAPIRequestPostReleasesAdd =
  createValidator<IAPIRequestPostReleasesAdd>(
    validate as ValidateFunction<IAPIRequestPostReleasesAdd>,
    aPIRequestPostReleasesAddSchema as unknown as IJSONSchema
  );
