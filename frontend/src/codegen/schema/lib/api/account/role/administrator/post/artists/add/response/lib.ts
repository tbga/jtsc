/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostArtistsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostArtistsAdd } from "./types";
export const validateAPIResponsePostArtistsAdd =
  createValidator<IAPIResponsePostArtistsAdd>(
    validate as ValidateFunction<IAPIResponsePostArtistsAdd>,
    aPIResponsePostArtistsAddSchema as unknown as IJSONSchema
  );
