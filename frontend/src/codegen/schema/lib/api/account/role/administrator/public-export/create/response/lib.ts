/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportCreate } from "./types";
export const validateAPIResponsePublicExportCreate =
  createValidator<IAPIResponsePublicExportCreate>(
    validate as ValidateFunction<IAPIResponsePublicExportCreate>,
    aPIResponsePublicExportCreateSchema as unknown as IJSONSchema
  );
