/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/add/request.schema.json",
  title: "APIRequestProfileArtistsAdd",
  description: "Add profile artists request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
