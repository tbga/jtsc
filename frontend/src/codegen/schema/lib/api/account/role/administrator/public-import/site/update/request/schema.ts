/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPublicImportSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/site/update/request.schema.json",
  title: "APIRequestPublicImportSiteUpdate",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        is_approved: {
          type: "boolean",
        },
      },
    },
  },
} as const;
