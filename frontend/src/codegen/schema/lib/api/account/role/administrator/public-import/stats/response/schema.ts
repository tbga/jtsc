/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicImportStatsSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/stats/response.schema.json",
  title: "APIResponsePublicImportStats",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-import/stats.schema.json",
    },
  },
} as const;
