/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/update/response.schema.json",
  title: "APIResponseSiteUpdate",
  description: "Updated site response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/entity.schema.json",
    },
  },
} as const;
