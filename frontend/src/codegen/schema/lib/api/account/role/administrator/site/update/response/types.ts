/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISite } from "#codegen/schema/lib/entities/site/entity";

/**
 * Updated site response.
 */
export interface IAPIResponseSiteUpdate {
  is_successful: true;
  data: ISite;
}
