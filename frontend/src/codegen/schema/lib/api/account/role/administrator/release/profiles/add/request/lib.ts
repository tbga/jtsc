/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleaseProfilesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleaseProfilesAdd } from "./types";
export const validateAPIRequestReleaseProfilesAdd =
  createValidator<IAPIRequestReleaseProfilesAdd>(
    validate as ValidateFunction<IAPIRequestReleaseProfilesAdd>,
    aPIRequestReleaseProfilesAddSchema as unknown as IJSONSchema
  );
