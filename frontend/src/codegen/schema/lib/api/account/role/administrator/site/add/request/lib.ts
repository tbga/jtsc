/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestSiteAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestSiteAdd } from "./types";
export const validateAPIRequestSiteAdd = createValidator<IAPIRequestSiteAdd>(
  validate as ValidateFunction<IAPIRequestSiteAdd>,
  aPIRequestSiteAddSchema as unknown as IJSONSchema
);
