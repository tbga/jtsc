/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleasePostsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleasePostsAdd } from "./types";
export const validateAPIResponseReleasePostsAdd =
  createValidator<IAPIResponseReleasePostsAdd>(
    validate as ValidateFunction<IAPIResponseReleasePostsAdd>,
    aPIResponseReleasePostsAddSchema as unknown as IJSONSchema
  );
