/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IProfileInit } from "#codegen/schema/lib/entities/profile/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestProfileCreate {
  data: IProfileInit;
}
