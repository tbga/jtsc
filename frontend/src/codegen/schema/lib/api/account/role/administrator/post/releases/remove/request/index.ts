/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestPostReleasesRemoveSchema } from "./schema";
export { validateAPIRequestPostReleasesRemove } from "./lib";
export type { IAPIRequestPostReleasesRemove } from "./types";
