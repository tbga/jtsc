/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPublicExportSitesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/add/request.schema.json",
  title: "APIRequestPublicExportSitesAdd",
  description: "Add published sites to the public export.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of site IDs.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
