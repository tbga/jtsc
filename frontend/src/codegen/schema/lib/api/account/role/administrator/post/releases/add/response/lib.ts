/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostReleasesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostReleasesAdd } from "./types";
export const validateAPIResponsePostReleasesAdd =
  createValidator<IAPIResponsePostReleasesAdd>(
    validate as ValidateFunction<IAPIResponsePostReleasesAdd>,
    aPIResponsePostReleasesAddSchema as unknown as IJSONSchema
  );
