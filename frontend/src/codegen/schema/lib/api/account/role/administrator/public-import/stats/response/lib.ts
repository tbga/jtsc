/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicImportStatsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicImportStats } from "./types";
export const validateAPIResponsePublicImportStats =
  createValidator<IAPIResponsePublicImportStats>(
    validate as ValidateFunction<IAPIResponsePublicImportStats>,
    aPIResponsePublicImportStatsSchema as unknown as IJSONSchema
  );
