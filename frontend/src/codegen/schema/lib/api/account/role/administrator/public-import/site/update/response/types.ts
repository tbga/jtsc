/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPublicImportSite } from "#codegen/schema/lib/entities/public-import/site/entity";

export interface IAPIResponsePublicImportSiteUpdate {
  is_successful: true;
  data: IPublicImportSite;
}
