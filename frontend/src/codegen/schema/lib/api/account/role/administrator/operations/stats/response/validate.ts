/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/operations/stats/response.schema.json",
  title: "APIResponseOperationsStats",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: {
      description: "Operations overview.",
      type: "object",
      additionalProperties: false,
      required: ["all", "pending", "in_progress", "finished", "failed"],
      properties: {
        all: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        pending: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        in_progress: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        finished: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        failed: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
      },
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/operations/stats/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              if (errors === _errs4) {
                if (
                  data1 &&
                  typeof data1 == "object" &&
                  !Array.isArray(data1)
                ) {
                  let missing1;
                  if (
                    (data1.all === undefined && (missing1 = "all")) ||
                    (data1.pending === undefined && (missing1 = "pending")) ||
                    (data1.in_progress === undefined &&
                      (missing1 = "in_progress")) ||
                    (data1.finished === undefined && (missing1 = "finished")) ||
                    (data1.failed === undefined && (missing1 = "failed"))
                  ) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath: "#/properties/data/required",
                        keyword: "required",
                        params: { missingProperty: missing1 },
                        message:
                          "must have required property '" + missing1 + "'",
                      },
                    ];
                    return false;
                  } else {
                    const _errs6 = errors;
                    for (const key1 in data1) {
                      if (
                        !(
                          key1 === "all" ||
                          key1 === "pending" ||
                          key1 === "in_progress" ||
                          key1 === "finished" ||
                          key1 === "failed"
                        )
                      ) {
                        validate20.errors = [
                          {
                            instancePath: instancePath + "/data",
                            schemaPath:
                              "#/properties/data/additionalProperties",
                            keyword: "additionalProperties",
                            params: { additionalProperty: key1 },
                            message: "must NOT have additional properties",
                          },
                        ];
                        return false;
                        break;
                      }
                    }
                    if (_errs6 === errors) {
                      if (data1.all !== undefined) {
                        let data2 = data1.all;
                        const _errs7 = errors;
                        const _errs8 = errors;
                        if (typeof data2 !== "string") {
                          let dataType1 = typeof data2;
                          let coerced1 = undefined;
                          if (!(coerced1 !== undefined)) {
                            if (
                              dataType1 == "number" ||
                              dataType1 == "boolean"
                            ) {
                              coerced1 = "" + data2;
                            } else if (data2 === null) {
                              coerced1 = "";
                            } else {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/all",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced1 !== undefined) {
                            data2 = coerced1;
                            if (data1 !== undefined) {
                              data1["all"] = coerced1;
                            }
                          }
                        }
                        if (errors === _errs8) {
                          if (typeof data2 === "string") {
                            if (func1(data2) > 20) {
                              validate20.errors = [
                                {
                                  instancePath: instancePath + "/data/all",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 20 },
                                  message:
                                    "must NOT have more than 20 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data2) < 1) {
                                validate20.errors = [
                                  {
                                    instancePath: instancePath + "/data/all",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid1 = _errs7 === errors;
                      } else {
                        var valid1 = true;
                      }
                      if (valid1) {
                        if (data1.pending !== undefined) {
                          let data3 = data1.pending;
                          const _errs10 = errors;
                          const _errs11 = errors;
                          if (typeof data3 !== "string") {
                            let dataType2 = typeof data3;
                            let coerced2 = undefined;
                            if (!(coerced2 !== undefined)) {
                              if (
                                dataType2 == "number" ||
                                dataType2 == "boolean"
                              ) {
                                coerced2 = "" + data3;
                              } else if (data3 === null) {
                                coerced2 = "";
                              } else {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/data/pending",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced2 !== undefined) {
                              data3 = coerced2;
                              if (data1 !== undefined) {
                                data1["pending"] = coerced2;
                              }
                            }
                          }
                          if (errors === _errs11) {
                            if (typeof data3 === "string") {
                              if (func1(data3) > 20) {
                                validate20.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/data/pending",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 20 },
                                    message:
                                      "must NOT have more than 20 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func1(data3) < 1) {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/data/pending",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 1 },
                                      message:
                                        "must NOT have fewer than 1 characters",
                                    },
                                  ];
                                  return false;
                                }
                              }
                            }
                          }
                          var valid1 = _errs10 === errors;
                        } else {
                          var valid1 = true;
                        }
                        if (valid1) {
                          if (data1.in_progress !== undefined) {
                            let data4 = data1.in_progress;
                            const _errs13 = errors;
                            const _errs14 = errors;
                            if (typeof data4 !== "string") {
                              let dataType3 = typeof data4;
                              let coerced3 = undefined;
                              if (!(coerced3 !== undefined)) {
                                if (
                                  dataType3 == "number" ||
                                  dataType3 == "boolean"
                                ) {
                                  coerced3 = "" + data4;
                                } else if (data4 === null) {
                                  coerced3 = "";
                                } else {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/data/in_progress",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced3 !== undefined) {
                                data4 = coerced3;
                                if (data1 !== undefined) {
                                  data1["in_progress"] = coerced3;
                                }
                              }
                            }
                            if (errors === _errs14) {
                              if (typeof data4 === "string") {
                                if (func1(data4) > 20) {
                                  validate20.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/data/in_progress",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                      keyword: "maxLength",
                                      params: { limit: 20 },
                                      message:
                                        "must NOT have more than 20 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (func1(data4) < 1) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/data/in_progress",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                        keyword: "minLength",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 characters",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                            var valid1 = _errs13 === errors;
                          } else {
                            var valid1 = true;
                          }
                          if (valid1) {
                            if (data1.finished !== undefined) {
                              let data5 = data1.finished;
                              const _errs16 = errors;
                              const _errs17 = errors;
                              if (typeof data5 !== "string") {
                                let dataType4 = typeof data5;
                                let coerced4 = undefined;
                                if (!(coerced4 !== undefined)) {
                                  if (
                                    dataType4 == "number" ||
                                    dataType4 == "boolean"
                                  ) {
                                    coerced4 = "" + data5;
                                  } else if (data5 === null) {
                                    coerced4 = "";
                                  } else {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/data/finished",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced4 !== undefined) {
                                  data5 = coerced4;
                                  if (data1 !== undefined) {
                                    data1["finished"] = coerced4;
                                  }
                                }
                              }
                              if (errors === _errs17) {
                                if (typeof data5 === "string") {
                                  if (func1(data5) > 20) {
                                    validate20.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/data/finished",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 20 },
                                        message:
                                          "must NOT have more than 20 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func1(data5) < 1) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/data/finished",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                              var valid1 = _errs16 === errors;
                            } else {
                              var valid1 = true;
                            }
                            if (valid1) {
                              if (data1.failed !== undefined) {
                                let data6 = data1.failed;
                                const _errs19 = errors;
                                const _errs20 = errors;
                                if (typeof data6 !== "string") {
                                  let dataType5 = typeof data6;
                                  let coerced5 = undefined;
                                  if (!(coerced5 !== undefined)) {
                                    if (
                                      dataType5 == "number" ||
                                      dataType5 == "boolean"
                                    ) {
                                      coerced5 = "" + data6;
                                    } else if (data6 === null) {
                                      coerced5 = "";
                                    } else {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/data/failed",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                          keyword: "type",
                                          params: { type: "string" },
                                          message: "must be string",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                  if (coerced5 !== undefined) {
                                    data6 = coerced5;
                                    if (data1 !== undefined) {
                                      data1["failed"] = coerced5;
                                    }
                                  }
                                }
                                if (errors === _errs20) {
                                  if (typeof data6 === "string") {
                                    if (func1(data6) > 20) {
                                      validate20.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/data/failed",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                          keyword: "maxLength",
                                          params: { limit: 20 },
                                          message:
                                            "must NOT have more than 20 characters",
                                        },
                                      ];
                                      return false;
                                    } else {
                                      if (func1(data6) < 1) {
                                        validate20.errors = [
                                          {
                                            instancePath:
                                              instancePath + "/data/failed",
                                            schemaPath:
                                              "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                            keyword: "minLength",
                                            params: { limit: 1 },
                                            message:
                                              "must NOT have fewer than 1 characters",
                                          },
                                        ];
                                        return false;
                                      }
                                    }
                                  }
                                }
                                var valid1 = _errs19 === errors;
                              } else {
                                var valid1 = true;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/type",
                      keyword: "type",
                      params: { type: "object" },
                      message: "must be object",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
