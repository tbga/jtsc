/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublishSiteSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/publish/site/response.schema.json",
  title: "APIResponsePublishSite",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/entity.schema.json",
    },
  },
} as const;
