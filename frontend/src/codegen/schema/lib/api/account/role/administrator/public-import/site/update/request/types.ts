/*
 * This file was generated automatically, do not edit it by hand.
 */

export interface IAPIRequestPublicImportSiteUpdate {
  data: {
    is_approved?: boolean;
  };
}
