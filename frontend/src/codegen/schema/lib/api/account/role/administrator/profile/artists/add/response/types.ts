/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Added profile artists.
 */
export interface IAPIResponseProfileArtistsAdd {
  is_successful: true;
  /**
   * A list of added artists.
   */
  data: IEntityItem[];
}
