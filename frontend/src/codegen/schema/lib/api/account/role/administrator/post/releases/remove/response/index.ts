/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostReleasesRemoveSchema } from "./schema";
export { validateAPIResponsePostReleasesRemove } from "./lib";
export type { IAPIResponsePostReleasesRemove } from "./types";
