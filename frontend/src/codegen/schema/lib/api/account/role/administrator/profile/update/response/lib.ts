/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileUpdate } from "./types";
export const validateAPIResponseProfileUpdate =
  createValidator<IAPIResponseProfileUpdate>(
    validate as ValidateFunction<IAPIResponseProfileUpdate>,
    aPIResponseProfileUpdateSchema as unknown as IJSONSchema
  );
