/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/update/response.schema.json",
  title: "APIResponseProfileUpdate",
  description: "Added profile releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/entity.schema.json",
    },
  },
} as const;
