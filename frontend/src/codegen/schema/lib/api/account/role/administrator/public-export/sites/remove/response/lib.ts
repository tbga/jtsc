/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportSitesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportSitesRemove } from "./types";
export const validateAPIResponsePublicExportSitesRemove =
  createValidator<IAPIResponsePublicExportSitesRemove>(
    validate as ValidateFunction<IAPIResponsePublicExportSitesRemove>,
    aPIResponsePublicExportSitesRemoveSchema as unknown as IJSONSchema
  );
