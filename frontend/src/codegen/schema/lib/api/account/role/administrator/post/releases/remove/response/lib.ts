/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostReleasesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostReleasesRemove } from "./types";
export const validateAPIResponsePostReleasesRemove =
  createValidator<IAPIResponsePostReleasesRemove>(
    validate as ValidateFunction<IAPIResponsePostReleasesRemove>,
    aPIResponsePostReleasesRemoveSchema as unknown as IJSONSchema
  );
