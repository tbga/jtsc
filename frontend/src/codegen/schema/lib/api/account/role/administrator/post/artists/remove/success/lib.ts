/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePostArtistsRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePostArtistsRemove } from "./types";
export const validateAPIResponsePostArtistsRemove =
  createValidator<IAPIResponsePostArtistsRemove>(
    validate as ValidateFunction<IAPIResponsePostArtistsRemove>,
    aPIResponsePostArtistsRemoveSchema as unknown as IJSONSchema
  );
