/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseArtistUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/artist/update/response.schema.json",
  title: "APIResponseArtistUpdate",
  description: "Updated artist.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/artist/entity.schema.json",
    },
  },
} as const;
