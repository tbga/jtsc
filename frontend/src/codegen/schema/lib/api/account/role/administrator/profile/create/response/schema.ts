/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/create/response.schema.json",
  title: "APIResponseProfileCreate",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
