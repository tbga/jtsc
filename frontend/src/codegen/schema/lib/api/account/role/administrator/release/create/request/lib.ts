/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleaseCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleaseCreate } from "./types";
export const validateAPIRequestReleaseCreate =
  createValidator<IAPIRequestReleaseCreate>(
    validate as ValidateFunction<IAPIRequestReleaseCreate>,
    aPIRequestReleaseCreateSchema as unknown as IJSONSchema
  );
