/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestReleaseProfilesRemoveSchema } from "./schema";
export { validateAPIRequestReleaseProfilesRemove } from "./lib";
export type { IAPIRequestReleaseProfilesRemove } from "./types";
