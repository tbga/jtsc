/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IProfileUpdate } from "#codegen/schema/lib/entities/profile/update";

/**
 * Profile's update
 */
export interface IAPIRequestProfileUpdate {
  data: IProfileUpdate;
}
