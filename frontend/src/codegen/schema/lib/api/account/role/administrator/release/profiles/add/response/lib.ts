/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleaseProfilesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleaseProfilesAdd } from "./types";
export const validateAPIResponseReleaseProfilesAdd =
  createValidator<IAPIResponseReleaseProfilesAdd>(
    validate as ValidateFunction<IAPIResponseReleaseProfilesAdd>,
    aPIResponseReleaseProfilesAddSchema as unknown as IJSONSchema
  );
