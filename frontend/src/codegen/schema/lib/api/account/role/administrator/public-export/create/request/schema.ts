/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPublicExportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/create/request.schema.json",
  title: "APIRequestPublicExportCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/init.schema.json",
    },
  },
} as const;
