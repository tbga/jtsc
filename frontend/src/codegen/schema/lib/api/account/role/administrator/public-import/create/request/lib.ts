/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPublicImportCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPublicImportCreate } from "./types";
export const validateAPIRequestPublicImportCreate =
  createValidator<IAPIRequestPublicImportCreate>(
    validate as ValidateFunction<IAPIRequestPublicImportCreate>,
    aPIRequestPublicImportCreateSchema as unknown as IJSONSchema
  );
