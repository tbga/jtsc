/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileArtistsRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileArtistsRemove } from "./types";
export const validateAPIResponseProfileArtistsRemove =
  createValidator<IAPIResponseProfileArtistsRemove>(
    validate as ValidateFunction<IAPIResponseProfileArtistsRemove>,
    aPIResponseProfileArtistsRemoveSchema as unknown as IJSONSchema
  );
