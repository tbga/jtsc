/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileArtistsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileArtistsAdd } from "./types";
export const validateAPIRequestProfileArtistsAdd =
  createValidator<IAPIRequestProfileArtistsAdd>(
    validate as ValidateFunction<IAPIRequestProfileArtistsAdd>,
    aPIRequestProfileArtistsAddSchema as unknown as IJSONSchema
  );
