/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IArtistUpdate } from "#codegen/schema/lib/entities/artist/update";

/**
 * Update artist.
 */
export interface IAPIRequestArtistUpdate {
  data: IArtistUpdate;
}
