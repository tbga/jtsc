/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSiteUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSiteUpdate } from "./types";
export const validateAPIResponseSiteUpdate =
  createValidator<IAPIResponseSiteUpdate>(
    validate as ValidateFunction<IAPIResponseSiteUpdate>,
    aPIResponseSiteUpdateSchema as unknown as IJSONSchema
  );
