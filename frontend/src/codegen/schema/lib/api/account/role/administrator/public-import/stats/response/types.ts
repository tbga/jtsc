/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPublicImportStats } from "#codegen/schema/lib/entities/public-import/stats";

export interface IAPIResponsePublicImportStats {
  is_successful: true;
  data: IPublicImportStats;
}
