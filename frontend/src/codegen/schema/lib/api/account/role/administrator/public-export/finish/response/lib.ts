/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportFinishSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportFinish } from "./types";
export const validateAPIResponsePublicExportFinish =
  createValidator<IAPIResponsePublicExportFinish>(
    validate as ValidateFunction<IAPIResponsePublicExportFinish>,
    aPIResponsePublicExportFinishSchema as unknown as IJSONSchema
  );
