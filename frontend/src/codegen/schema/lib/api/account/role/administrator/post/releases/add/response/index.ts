/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostReleasesAddSchema } from "./schema";
export { validateAPIResponsePostReleasesAdd } from "./lib";
export type { IAPIResponsePostReleasesAdd } from "./types";
