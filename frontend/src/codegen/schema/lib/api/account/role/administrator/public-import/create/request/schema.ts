/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPublicImportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/create/request.schema.json",
  title: "APIRequestPublicImportCreate",
  description: "A path to the public export folder.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
    },
  },
} as const;
