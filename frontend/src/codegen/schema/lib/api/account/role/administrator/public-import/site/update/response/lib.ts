/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicImportSiteUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicImportSiteUpdate } from "./types";
export const validateAPIResponsePublicImportSiteUpdate =
  createValidator<IAPIResponsePublicImportSiteUpdate>(
    validate as ValidateFunction<IAPIResponsePublicImportSiteUpdate>,
    aPIResponsePublicImportSiteUpdateSchema as unknown as IJSONSchema
  );
