/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportFinalizeSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportFinalize } from "./types";
export const validateAPIResponsePublicExportFinalize =
  createValidator<IAPIResponsePublicExportFinalize>(
    validate as ValidateFunction<IAPIResponsePublicExportFinalize>,
    aPIResponsePublicExportFinalizeSchema as unknown as IJSONSchema
  );
