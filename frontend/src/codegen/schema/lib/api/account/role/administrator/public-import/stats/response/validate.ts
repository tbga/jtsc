/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/stats/response.schema.json",
  title: "APIResponsePublicImportStats",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-import/stats.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/public-import/stats.schema.json",
  title: "PublicImportStats",
  description: "The stats of the public import.",
  type: "object",
  additionalProperties: false,
  required: ["id", "sites"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    sites: {
      description: "Site stats.",
      type: "object",
      additionalProperties: false,
      required: ["all", "new", "same", "different"],
      properties: {
        all: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        new: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        same: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        different: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
      },
    },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-import/stats.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.sites === undefined && (missing0 = "sites"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "sites")) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.sites !== undefined) {
              let data1 = data.sites;
              const _errs5 = errors;
              if (errors === _errs5) {
                if (
                  data1 &&
                  typeof data1 == "object" &&
                  !Array.isArray(data1)
                ) {
                  let missing1;
                  if (
                    (data1.all === undefined && (missing1 = "all")) ||
                    (data1.new === undefined && (missing1 = "new")) ||
                    (data1.same === undefined && (missing1 = "same")) ||
                    (data1.different === undefined && (missing1 = "different"))
                  ) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/sites",
                        schemaPath: "#/properties/sites/required",
                        keyword: "required",
                        params: { missingProperty: missing1 },
                        message:
                          "must have required property '" + missing1 + "'",
                      },
                    ];
                    return false;
                  } else {
                    const _errs7 = errors;
                    for (const key1 in data1) {
                      if (
                        !(
                          key1 === "all" ||
                          key1 === "new" ||
                          key1 === "same" ||
                          key1 === "different"
                        )
                      ) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/sites",
                            schemaPath:
                              "#/properties/sites/additionalProperties",
                            keyword: "additionalProperties",
                            params: { additionalProperty: key1 },
                            message: "must NOT have additional properties",
                          },
                        ];
                        return false;
                        break;
                      }
                    }
                    if (_errs7 === errors) {
                      if (data1.all !== undefined) {
                        let data2 = data1.all;
                        const _errs8 = errors;
                        const _errs9 = errors;
                        if (typeof data2 !== "string") {
                          let dataType1 = typeof data2;
                          let coerced1 = undefined;
                          if (!(coerced1 !== undefined)) {
                            if (
                              dataType1 == "number" ||
                              dataType1 == "boolean"
                            ) {
                              coerced1 = "" + data2;
                            } else if (data2 === null) {
                              coerced1 = "";
                            } else {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/sites/all",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced1 !== undefined) {
                            data2 = coerced1;
                            if (data1 !== undefined) {
                              data1["all"] = coerced1;
                            }
                          }
                        }
                        if (errors === _errs9) {
                          if (typeof data2 === "string") {
                            if (func1(data2) > 20) {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/sites/all",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 20 },
                                  message:
                                    "must NOT have more than 20 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data2) < 1) {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/sites/all",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid2 = _errs8 === errors;
                      } else {
                        var valid2 = true;
                      }
                      if (valid2) {
                        if (data1.new !== undefined) {
                          let data3 = data1.new;
                          const _errs11 = errors;
                          const _errs12 = errors;
                          if (typeof data3 !== "string") {
                            let dataType2 = typeof data3;
                            let coerced2 = undefined;
                            if (!(coerced2 !== undefined)) {
                              if (
                                dataType2 == "number" ||
                                dataType2 == "boolean"
                              ) {
                                coerced2 = "" + data3;
                              } else if (data3 === null) {
                                coerced2 = "";
                              } else {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/sites/new",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced2 !== undefined) {
                              data3 = coerced2;
                              if (data1 !== undefined) {
                                data1["new"] = coerced2;
                              }
                            }
                          }
                          if (errors === _errs12) {
                            if (typeof data3 === "string") {
                              if (func1(data3) > 20) {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/sites/new",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 20 },
                                    message:
                                      "must NOT have more than 20 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func1(data3) < 1) {
                                  validate21.errors = [
                                    {
                                      instancePath: instancePath + "/sites/new",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 1 },
                                      message:
                                        "must NOT have fewer than 1 characters",
                                    },
                                  ];
                                  return false;
                                }
                              }
                            }
                          }
                          var valid2 = _errs11 === errors;
                        } else {
                          var valid2 = true;
                        }
                        if (valid2) {
                          if (data1.same !== undefined) {
                            let data4 = data1.same;
                            const _errs14 = errors;
                            const _errs15 = errors;
                            if (typeof data4 !== "string") {
                              let dataType3 = typeof data4;
                              let coerced3 = undefined;
                              if (!(coerced3 !== undefined)) {
                                if (
                                  dataType3 == "number" ||
                                  dataType3 == "boolean"
                                ) {
                                  coerced3 = "" + data4;
                                } else if (data4 === null) {
                                  coerced3 = "";
                                } else {
                                  validate21.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/sites/same",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                      keyword: "type",
                                      params: { type: "string" },
                                      message: "must be string",
                                    },
                                  ];
                                  return false;
                                }
                              }
                              if (coerced3 !== undefined) {
                                data4 = coerced3;
                                if (data1 !== undefined) {
                                  data1["same"] = coerced3;
                                }
                              }
                            }
                            if (errors === _errs15) {
                              if (typeof data4 === "string") {
                                if (func1(data4) > 20) {
                                  validate21.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/sites/same",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                      keyword: "maxLength",
                                      params: { limit: 20 },
                                      message:
                                        "must NOT have more than 20 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (func1(data4) < 1) {
                                    validate21.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/sites/same",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                        keyword: "minLength",
                                        params: { limit: 1 },
                                        message:
                                          "must NOT have fewer than 1 characters",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                            var valid2 = _errs14 === errors;
                          } else {
                            var valid2 = true;
                          }
                          if (valid2) {
                            if (data1.different !== undefined) {
                              let data5 = data1.different;
                              const _errs17 = errors;
                              const _errs18 = errors;
                              if (typeof data5 !== "string") {
                                let dataType4 = typeof data5;
                                let coerced4 = undefined;
                                if (!(coerced4 !== undefined)) {
                                  if (
                                    dataType4 == "number" ||
                                    dataType4 == "boolean"
                                  ) {
                                    coerced4 = "" + data5;
                                  } else if (data5 === null) {
                                    coerced4 = "";
                                  } else {
                                    validate21.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/sites/different",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                                        keyword: "type",
                                        params: { type: "string" },
                                        message: "must be string",
                                      },
                                    ];
                                    return false;
                                  }
                                }
                                if (coerced4 !== undefined) {
                                  data5 = coerced4;
                                  if (data1 !== undefined) {
                                    data1["different"] = coerced4;
                                  }
                                }
                              }
                              if (errors === _errs18) {
                                if (typeof data5 === "string") {
                                  if (func1(data5) > 20) {
                                    validate21.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/sites/different",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                                        keyword: "maxLength",
                                        params: { limit: 20 },
                                        message:
                                          "must NOT have more than 20 characters",
                                      },
                                    ];
                                    return false;
                                  } else {
                                    if (func1(data5) < 1) {
                                      validate21.errors = [
                                        {
                                          instancePath:
                                            instancePath + "/sites/different",
                                          schemaPath:
                                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                                          keyword: "minLength",
                                          params: { limit: 1 },
                                          message:
                                            "must NOT have fewer than 1 characters",
                                        },
                                      ];
                                      return false;
                                    }
                                  }
                                }
                              }
                              var valid2 = _errs17 === errors;
                            } else {
                              var valid2 = true;
                            }
                          }
                        }
                      }
                    }
                  }
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/sites",
                      schemaPath: "#/properties/sites/type",
                      keyword: "type",
                      params: { type: "object" },
                      message: "must be object",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/public-import/stats/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              const _errs4 = errors;
              if (
                !validate21(data.data, {
                  instancePath: instancePath + "/data",
                  parentData: data,
                  parentDataProperty: "data",
                  rootData,
                  dynamicAnchors,
                })
              ) {
                vErrors =
                  vErrors === null
                    ? validate21.errors
                    : vErrors.concat(validate21.errors);
                errors = vErrors.length;
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
