/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPostArtistsRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPostArtistsRemove } from "./types";
export const validateAPIRequestPostArtistsRemove =
  createValidator<IAPIRequestPostArtistsRemove>(
    validate as ValidateFunction<IAPIRequestPostArtistsRemove>,
    aPIRequestPostArtistsRemoveSchema as unknown as IJSONSchema
  );
