/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostArtistsRemoveSchema } from "./schema";
export { validateAPIResponsePostArtistsRemove } from "./lib";
export type { IAPIResponsePostArtistsRemove } from "./types";
