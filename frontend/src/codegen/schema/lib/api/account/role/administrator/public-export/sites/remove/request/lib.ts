/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPublicExportSitesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPublicExportSitesRemove } from "./types";
export const validateAPIRequestPublicExportSitesRemove =
  createValidator<IAPIRequestPublicExportSitesRemove>(
    validate as ValidateFunction<IAPIRequestPublicExportSitesRemove>,
    aPIRequestPublicExportSitesRemoveSchema as unknown as IJSONSchema
  );
