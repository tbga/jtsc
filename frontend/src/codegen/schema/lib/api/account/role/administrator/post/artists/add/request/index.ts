/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestPostArtistsAddSchema } from "./schema";
export { validateAPIRequestPostArtistsAdd } from "./lib";
export type { IAPIRequestPostArtistsAdd } from "./types";
