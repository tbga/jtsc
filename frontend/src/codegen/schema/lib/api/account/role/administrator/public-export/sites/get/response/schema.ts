/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicExportSitesSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/get/response.schema.json",
  title: "APIResponsePublicExportSites",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "sites"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        sites: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/site/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
