/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseReleasePostsAddSchema } from "./schema";
export { validateAPIResponseReleasePostsAdd } from "./lib";
export type { IAPIResponseReleasePostsAdd } from "./types";
