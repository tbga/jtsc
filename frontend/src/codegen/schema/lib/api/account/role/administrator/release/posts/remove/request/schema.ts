/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestReleasePostsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/remove/request.schema.json",
  title: "APIRequestReleasePostsRemove",
  description: "Remove posts from a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of post IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
