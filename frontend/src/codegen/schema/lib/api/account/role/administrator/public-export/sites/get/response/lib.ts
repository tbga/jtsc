/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportSitesSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportSites } from "./types";
export const validateAPIResponsePublicExportSites =
  createValidator<IAPIResponsePublicExportSites>(
    validate as ValidateFunction<IAPIResponsePublicExportSites>,
    aPIResponsePublicExportSitesSchema as unknown as IJSONSchema
  );
