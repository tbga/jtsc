/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestReleasePostsRemoveSchema } from "./schema";
export { validateAPIRequestReleasePostsRemove } from "./lib";
export type { IAPIRequestReleasePostsRemove } from "./types";
