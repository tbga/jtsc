/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPath } from "#codegen/schema/lib/local-file-system/path";

/**
 * A path to the public export folder.
 */
export interface IAPIRequestPublicImportCreate {
  data: IPath;
}
