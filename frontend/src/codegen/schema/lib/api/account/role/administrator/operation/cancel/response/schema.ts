/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseOperationCancelSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/operation/cancel/response.schema.json",
  $comment: "@TODO change data into a proper operation entity after rework.",
  title: "APIResponseOperationCancel",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
