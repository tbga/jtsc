/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileCreate } from "./types";
export const validateAPIResponseProfileCreate =
  createValidator<IAPIResponseProfileCreate>(
    validate as ValidateFunction<IAPIResponseProfileCreate>,
    aPIResponseProfileCreateSchema as unknown as IJSONSchema
  );
