/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseProfileArtistsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/remove/response.schema.json",
  title: "APIResponseProfileArtistsRemove",
  description: "Removed profile artists.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed artists.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
