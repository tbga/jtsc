/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISitePreview } from "#codegen/schema/lib/entities/site/preview";

/**
 * Added public export sites.
 */
export interface IAPIResponsePublicExportSitesAdd {
  is_successful: true;
  /**
   * A list of added sites.
   *
   * @minItems 1
   */
  data: [ISitePreview, ...ISitePreview[]];
}
