/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleasePostsRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleasePostsRemove } from "./types";
export const validateAPIRequestReleasePostsRemove =
  createValidator<IAPIRequestReleasePostsRemove>(
    validate as ValidateFunction<IAPIRequestReleasePostsRemove>,
    aPIRequestReleasePostsRemoveSchema as unknown as IJSONSchema
  );
