/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseReleasePostsSchema } from "./schema";
export { validateAPIResponseReleasePosts } from "./lib";
export type { IAPIResponseReleasePosts } from "./types";
