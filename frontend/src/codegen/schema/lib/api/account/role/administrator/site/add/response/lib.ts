/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSiteAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSiteAdd } from "./types";
export const validateAPIResponseSiteAdd = createValidator<IAPIResponseSiteAdd>(
  validate as ValidateFunction<IAPIResponseSiteAdd>,
  aPIResponseSiteAddSchema as unknown as IJSONSchema
);
