/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/artist/update/request.schema.json",
  title: "APIRequestArtistUpdate",
  description: "Update artist.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/artist/update.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/artist/update.schema.json",
  title: "ArtistUpdate",
  description: "An update on the artist entry.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: { sex: { type: "boolean" } },
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/artist/update/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs3 = errors;
            if (errors === _errs3) {
              if (data0 && typeof data0 == "object" && !Array.isArray(data0)) {
                const _errs5 = errors;
                for (const key1 in data0) {
                  if (!(key1 === "sex")) {
                    validate20.errors = [
                      {
                        instancePath: instancePath + "/data",
                        schemaPath:
                          "https://jtsc-schemas.org/entities/artist/update.schema.json/additionalProperties",
                        keyword: "additionalProperties",
                        params: { additionalProperty: key1 },
                        message: "must NOT have additional properties",
                      },
                    ];
                    return false;
                    break;
                  }
                }
                if (_errs5 === errors) {
                  if (data0.sex !== undefined) {
                    let data1 = data0.sex;
                    if (typeof data1 !== "boolean") {
                      let coerced0 = undefined;
                      if (!(coerced0 !== undefined)) {
                        if (
                          data1 === "false" ||
                          data1 === 0 ||
                          data1 === null
                        ) {
                          coerced0 = false;
                        } else if (data1 === "true" || data1 === 1) {
                          coerced0 = true;
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/data/sex",
                              schemaPath:
                                "https://jtsc-schemas.org/entities/artist/update.schema.json/properties/sex/type",
                              keyword: "type",
                              params: { type: "boolean" },
                              message: "must be boolean",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced0 !== undefined) {
                        data1 = coerced0;
                        if (data0 !== undefined) {
                          data0["sex"] = coerced0;
                        }
                      }
                    }
                  }
                }
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/data",
                    schemaPath:
                      "https://jtsc-schemas.org/entities/artist/update.schema.json/type",
                    keyword: "type",
                    params: { type: "object" },
                    message: "must be object",
                  },
                ];
                return false;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
