/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPostReleasesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/releases/add/request.schema.json",
  title: "APIRequestPostReleasesAdd",
  description: "Add post releases request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "Post releases init data.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "A list of existing release IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "A list of release initializers.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
          },
        },
      },
    },
  },
} as const;
