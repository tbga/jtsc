/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/releases/add/response.schema.json",
  title: "APIResponsePostReleasesAdd",
  description: "Added post releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: { type: "boolean", const: true },
    data: {
      description: "A list of added releases.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
      },
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/release/preview.schema.json",
  title: "ReleasePreview",
  description: "Preview of the release.",
  type: "object",
  additionalProperties: false,
  required: ["id", "profiles", "posts"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    name: { $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json" },
    site: { $ref: "https://jtsc-schemas.org/entities/item.schema.json" },
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    original_release_id: { type: "string" },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
};
const schema36 = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
};
const schema40 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const schema41 = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const schema37 = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
};
const schema39 = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
};
const formats0 = /^(?:urn:uuid:)?[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$/i;
function validate22(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/item.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate22.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.id === undefined && (missing0 = "id")) {
        validate22.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "id" || key0 === "name" || key0 === "public_id")) {
            validate22.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.name !== undefined) {
              let data1 = data.name;
              const _errs5 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["name"] = coerced1;
                  }
                }
              }
              if (errors === _errs5) {
                if (typeof data1 === "string") {
                  if (func1(data1) < 1) {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/name",
                        schemaPath: "#/properties/name/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.public_id !== undefined) {
                let data2 = data.public_id;
                const _errs7 = errors;
                const _errs8 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate22.errors = [
                        {
                          instancePath: instancePath + "/public_id",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["public_id"] = coerced2;
                    }
                  }
                }
                if (errors === _errs8) {
                  if (errors === _errs8) {
                    if (typeof data2 === "string") {
                      if (func1(data2) > 36) {
                        validate22.errors = [
                          {
                            instancePath: instancePath + "/public_id",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/maxLength",
                            keyword: "maxLength",
                            params: { limit: 36 },
                            message: "must NOT have more than 36 characters",
                          },
                        ];
                        return false;
                      } else {
                        if (func1(data2) < 36) {
                          validate22.errors = [
                            {
                              instancePath: instancePath + "/public_id",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/minLength",
                              keyword: "minLength",
                              params: { limit: 36 },
                              message: "must NOT have fewer than 36 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (!formats0.test(data2)) {
                            validate22.errors = [
                              {
                                instancePath: instancePath + "/public_id",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json/format",
                                keyword: "format",
                                params: { format: "uuid" },
                                message: 'must match format "' + "uuid" + '"',
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                  }
                }
                var valid0 = _errs7 === errors;
              } else {
                var valid0 = true;
              }
            }
          }
        }
      }
    } else {
      validate22.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate22.errors = vErrors;
  return errors === 0;
}
validate22.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
const formats2 = require("ajv-formats/dist/formats").fullFormats["date-time"];
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/release/preview.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.id === undefined && (missing0 = "id")) ||
        (data.profiles === undefined && (missing0 = "profiles")) ||
        (data.posts === undefined && (missing0 = "posts"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "id" ||
              key0 === "profiles" ||
              key0 === "posts" ||
              key0 === "name" ||
              key0 === "site" ||
              key0 === "title" ||
              key0 === "released_at" ||
              key0 === "original_release_id"
            )
          ) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.id !== undefined) {
            let data0 = data.id;
            const _errs2 = errors;
            const _errs3 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["id"] = coerced0;
                }
              }
            }
            if (errors === _errs3) {
              if (typeof data0 === "string") {
                if (func1(data0) > 19) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/id",
                      schemaPath:
                        "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 19 },
                      message: "must NOT have more than 19 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data0) < 1) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/id",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.profiles !== undefined) {
              let data1 = data.profiles;
              const _errs5 = errors;
              const _errs6 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/profiles",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["profiles"] = coerced1;
                  }
                }
              }
              if (errors === _errs6) {
                if (typeof data1 === "string") {
                  if (func1(data1) > 20) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/profiles",
                        schemaPath:
                          "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                        keyword: "maxLength",
                        params: { limit: 20 },
                        message: "must NOT have more than 20 characters",
                      },
                    ];
                    return false;
                  } else {
                    if (func1(data1) < 1) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/profiles",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                          keyword: "minLength",
                          params: { limit: 1 },
                          message: "must NOT have fewer than 1 characters",
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs5 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.posts !== undefined) {
                let data2 = data.posts;
                const _errs8 = errors;
                const _errs9 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/posts",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["posts"] = coerced2;
                    }
                  }
                }
                if (errors === _errs9) {
                  if (typeof data2 === "string") {
                    if (func1(data2) > 20) {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/posts",
                          schemaPath:
                            "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 20 },
                          message: "must NOT have more than 20 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data2) < 1) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/posts",
                            schemaPath:
                              "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs8 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.name !== undefined) {
                  let data3 = data.name;
                  const _errs11 = errors;
                  const _errs12 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["name"] = coerced3;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (typeof data3 === "string") {
                      if (func1(data3) < 1) {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/name",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/minimal.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                  var valid0 = _errs11 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.site !== undefined) {
                    const _errs14 = errors;
                    if (
                      !validate22(data.site, {
                        instancePath: instancePath + "/site",
                        parentData: data,
                        parentDataProperty: "site",
                        rootData,
                        dynamicAnchors,
                      })
                    ) {
                      vErrors =
                        vErrors === null
                          ? validate22.errors
                          : vErrors.concat(validate22.errors);
                      errors = vErrors.length;
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.title !== undefined) {
                      let data5 = data.title;
                      const _errs15 = errors;
                      const _errs16 = errors;
                      if (typeof data5 !== "string") {
                        let dataType4 = typeof data5;
                        let coerced4 = undefined;
                        if (!(coerced4 !== undefined)) {
                          if (dataType4 == "number" || dataType4 == "boolean") {
                            coerced4 = "" + data5;
                          } else if (data5 === null) {
                            coerced4 = "";
                          } else {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced4 !== undefined) {
                          data5 = coerced4;
                          if (data !== undefined) {
                            data["title"] = coerced4;
                          }
                        }
                      }
                      if (errors === _errs16) {
                        if (typeof data5 === "string") {
                          if (func1(data5) > 216) {
                            validate21.errors = [
                              {
                                instancePath: instancePath + "/title",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                                keyword: "maxLength",
                                params: { limit: 216 },
                                message:
                                  "must NOT have more than 216 characters",
                              },
                            ];
                            return false;
                          } else {
                            if (func1(data5) < 1) {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/title",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                                  keyword: "minLength",
                                  params: { limit: 1 },
                                  message:
                                    "must NOT have fewer than 1 characters",
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs15 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.released_at !== undefined) {
                        let data6 = data.released_at;
                        const _errs18 = errors;
                        const _errs19 = errors;
                        if (typeof data6 !== "string") {
                          let dataType5 = typeof data6;
                          let coerced5 = undefined;
                          if (!(coerced5 !== undefined)) {
                            if (
                              dataType5 == "number" ||
                              dataType5 == "boolean"
                            ) {
                              coerced5 = "" + data6;
                            } else if (data6 === null) {
                              coerced5 = "";
                            } else {
                              validate21.errors = [
                                {
                                  instancePath: instancePath + "/released_at",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/dates/datetime.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced5 !== undefined) {
                            data6 = coerced5;
                            if (data !== undefined) {
                              data["released_at"] = coerced5;
                            }
                          }
                        }
                        if (errors === _errs19) {
                          if (errors === _errs19) {
                            if (typeof data6 === "string") {
                              if (func1(data6) > 29) {
                                validate21.errors = [
                                  {
                                    instancePath: instancePath + "/released_at",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/dates/datetime.schema.json/maxLength",
                                    keyword: "maxLength",
                                    params: { limit: 29 },
                                    message:
                                      "must NOT have more than 29 characters",
                                  },
                                ];
                                return false;
                              } else {
                                if (func1(data6) < 29) {
                                  validate21.errors = [
                                    {
                                      instancePath:
                                        instancePath + "/released_at",
                                      schemaPath:
                                        "http://jtsc-schemas.org/types/dates/datetime.schema.json/minLength",
                                      keyword: "minLength",
                                      params: { limit: 29 },
                                      message:
                                        "must NOT have fewer than 29 characters",
                                    },
                                  ];
                                  return false;
                                } else {
                                  if (!formats2.validate(data6)) {
                                    validate21.errors = [
                                      {
                                        instancePath:
                                          instancePath + "/released_at",
                                        schemaPath:
                                          "http://jtsc-schemas.org/types/dates/datetime.schema.json/format",
                                        keyword: "format",
                                        params: { format: "date-time" },
                                        message:
                                          'must match format "' +
                                          "date-time" +
                                          '"',
                                      },
                                    ];
                                    return false;
                                  }
                                }
                              }
                            }
                          }
                        }
                        var valid0 = _errs18 === errors;
                      } else {
                        var valid0 = true;
                      }
                      if (valid0) {
                        if (data.original_release_id !== undefined) {
                          let data7 = data.original_release_id;
                          const _errs21 = errors;
                          if (typeof data7 !== "string") {
                            let dataType6 = typeof data7;
                            let coerced6 = undefined;
                            if (!(coerced6 !== undefined)) {
                              if (
                                dataType6 == "number" ||
                                dataType6 == "boolean"
                              ) {
                                coerced6 = "" + data7;
                              } else if (data7 === null) {
                                coerced6 = "";
                              } else {
                                validate21.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/original_release_id",
                                    schemaPath:
                                      "#/properties/original_release_id/type",
                                    keyword: "type",
                                    params: { type: "string" },
                                    message: "must be string",
                                  },
                                ];
                                return false;
                              }
                            }
                            if (coerced6 !== undefined) {
                              data7 = coerced6;
                              if (data !== undefined) {
                                data["original_release_id"] = coerced6;
                              }
                            }
                          }
                          var valid0 = _errs21 === errors;
                        } else {
                          var valid0 = true;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/post/releases/add/response.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.is_successful === undefined && (missing0 = "is_successful")) ||
        (data.data === undefined && (missing0 = "data"))
      ) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "is_successful" || key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.is_successful !== undefined) {
            let data0 = data.is_successful;
            const _errs2 = errors;
            if (typeof data0 !== "boolean") {
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (data0 === "false" || data0 === 0 || data0 === null) {
                  coerced0 = false;
                } else if (data0 === "true" || data0 === 1) {
                  coerced0 = true;
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/is_successful",
                      schemaPath: "#/properties/is_successful/type",
                      keyword: "type",
                      params: { type: "boolean" },
                      message: "must be boolean",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["is_successful"] = coerced0;
                }
              }
            }
            if (true !== data0) {
              validate20.errors = [
                {
                  instancePath: instancePath + "/is_successful",
                  schemaPath: "#/properties/is_successful/const",
                  keyword: "const",
                  params: { allowedValue: true },
                  message: "must be equal to constant",
                },
              ];
              return false;
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.data !== undefined) {
              let data1 = data.data;
              const _errs4 = errors;
              if (errors === _errs4) {
                if (Array.isArray(data1)) {
                  var valid1 = true;
                  const len0 = data1.length;
                  for (let i0 = 0; i0 < len0; i0++) {
                    const _errs6 = errors;
                    if (
                      !validate21(data1[i0], {
                        instancePath: instancePath + "/data/" + i0,
                        parentData: data1,
                        parentDataProperty: i0,
                        rootData,
                        dynamicAnchors,
                      })
                    ) {
                      vErrors =
                        vErrors === null
                          ? validate21.errors
                          : vErrors.concat(validate21.errors);
                      errors = vErrors.length;
                    }
                    var valid1 = _errs6 === errors;
                    if (!valid1) {
                      break;
                    }
                  }
                } else {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/type",
                      keyword: "type",
                      params: { type: "array" },
                      message: "must be array",
                    },
                  ];
                  return false;
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
