/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestReleasePostsAddSchema } from "./schema";
export { validateAPIRequestReleasePostsAdd } from "./lib";
export type { IAPIRequestReleasePostsAdd } from "./types";
