/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponsePostArtistsAddSchema } from "./schema";
export { validateAPIResponsePostArtistsAdd } from "./lib";
export type { IAPIResponsePostArtistsAdd } from "./types";
