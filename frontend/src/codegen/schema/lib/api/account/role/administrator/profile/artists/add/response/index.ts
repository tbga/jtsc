/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileArtistsAddSchema } from "./schema";
export { validateAPIResponseProfileArtistsAdd } from "./lib";
export type { IAPIResponseProfileArtistsAdd } from "./types";
