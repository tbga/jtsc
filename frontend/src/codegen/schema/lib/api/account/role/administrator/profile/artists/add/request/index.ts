/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestProfileArtistsAddSchema } from "./schema";
export { validateAPIRequestProfileArtistsAdd } from "./lib";
export type { IAPIRequestProfileArtistsAdd } from "./types";
