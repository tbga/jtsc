/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseArtistUpdateSchema } from "./schema";
export { validateAPIResponseArtistUpdate } from "./lib";
export type { IAPIResponseArtistUpdate } from "./types";
