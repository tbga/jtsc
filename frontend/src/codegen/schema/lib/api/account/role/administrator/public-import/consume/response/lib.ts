/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicImportConsumeSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicImportConsume } from "./types";
export const validateAPIResponsePublicImportConsume =
  createValidator<IAPIResponsePublicImportConsume>(
    validate as ValidateFunction<IAPIResponsePublicImportConsume>,
    aPIResponsePublicImportConsumeSchema as unknown as IJSONSchema
  );
