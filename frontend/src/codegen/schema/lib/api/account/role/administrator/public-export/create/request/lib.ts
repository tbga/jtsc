/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPublicExportCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPublicExportCreate } from "./types";
export const validateAPIRequestPublicExportCreate =
  createValidator<IAPIRequestPublicExportCreate>(
    validate as ValidateFunction<IAPIRequestPublicExportCreate>,
    aPIRequestPublicExportCreateSchema as unknown as IJSONSchema
  );
