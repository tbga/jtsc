/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestReleaseCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/create/request.schema.json",
  title: "APIRequestReleaseCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
    },
  },
} as const;
