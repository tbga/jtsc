/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicImportSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/site/update/response.schema.json",
  title: "APIResponsePublicImportSiteUpdate",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/entity.schema.json",
    },
  },
} as const;
