/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportSitesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExportSitesAdd } from "./types";
export const validateAPIResponsePublicExportSitesAdd =
  createValidator<IAPIResponsePublicExportSitesAdd>(
    validate as ValidateFunction<IAPIResponsePublicExportSitesAdd>,
    aPIResponsePublicExportSitesAddSchema as unknown as IJSONSchema
  );
