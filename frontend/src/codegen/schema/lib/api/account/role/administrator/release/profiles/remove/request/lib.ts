/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleaseProfilesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleaseProfilesRemove } from "./types";
export const validateAPIRequestReleaseProfilesRemove =
  createValidator<IAPIRequestReleaseProfilesRemove>(
    validate as ValidateFunction<IAPIRequestReleaseProfilesRemove>,
    aPIRequestReleaseProfilesRemoveSchema as unknown as IJSONSchema
  );
