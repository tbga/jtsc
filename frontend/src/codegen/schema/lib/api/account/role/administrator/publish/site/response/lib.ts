/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublishSiteSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublishSite } from "./types";
export const validateAPIResponsePublishSite =
  createValidator<IAPIResponsePublishSite>(
    validate as ValidateFunction<IAPIResponsePublishSite>,
    aPIResponsePublishSiteSchema as unknown as IJSONSchema
  );
