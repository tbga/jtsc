/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPostReleasesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPostReleasesRemove } from "./types";
export const validateAPIRequestPostReleasesRemove =
  createValidator<IAPIRequestPostReleasesRemove>(
    validate as ValidateFunction<IAPIRequestPostReleasesRemove>,
    aPIRequestPostReleasesRemoveSchema as unknown as IJSONSchema
  );
