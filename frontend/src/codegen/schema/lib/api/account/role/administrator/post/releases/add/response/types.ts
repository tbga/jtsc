/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * Added post releases.
 */
export interface IAPIResponsePostReleasesAdd {
  is_successful: true;
  /**
   * A list of added releases.
   */
  data: IReleasePreview[];
}
