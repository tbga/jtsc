/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestArtistUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/artist/update/request.schema.json",
  title: "APIRequestArtistUpdate",
  description: "Update artist.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/artist/update.schema.json",
    },
  },
} as const;
