/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileArtistsRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileArtistsRemove } from "./types";
export const validateAPIRequestProfileArtistsRemove =
  createValidator<IAPIRequestProfileArtistsRemove>(
    validate as ValidateFunction<IAPIRequestProfileArtistsRemove>,
    aPIRequestProfileArtistsRemoveSchema as unknown as IJSONSchema
  );
