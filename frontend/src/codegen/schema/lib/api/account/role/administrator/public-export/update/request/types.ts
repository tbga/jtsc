/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPublicExportUpdate } from "#codegen/schema/lib/entities/public-export/update";

/**
 * Public export update.
 */
export interface IAPIRequestPublicExportUpdate {
  data: IPublicExportUpdate;
}
