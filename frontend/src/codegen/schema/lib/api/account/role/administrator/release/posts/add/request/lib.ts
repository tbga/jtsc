/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleasePostsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleasePostsAdd } from "./types";
export const validateAPIRequestReleasePostsAdd =
  createValidator<IAPIRequestReleasePostsAdd>(
    validate as ValidateFunction<IAPIRequestReleasePostsAdd>,
    aPIRequestReleasePostsAddSchema as unknown as IJSONSchema
  );
