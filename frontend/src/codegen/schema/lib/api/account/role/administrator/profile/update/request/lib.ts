/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileUpdate } from "./types";
export const validateAPIRequestProfileUpdate =
  createValidator<IAPIRequestProfileUpdate>(
    validate as ValidateFunction<IAPIRequestProfileUpdate>,
    aPIRequestProfileUpdateSchema as unknown as IJSONSchema
  );
