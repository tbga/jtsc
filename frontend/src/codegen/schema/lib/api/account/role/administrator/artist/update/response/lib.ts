/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseArtistUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseArtistUpdate } from "./types";
export const validateAPIResponseArtistUpdate =
  createValidator<IAPIResponseArtistUpdate>(
    validate as ValidateFunction<IAPIResponseArtistUpdate>,
    aPIResponseArtistUpdateSchema as unknown as IJSONSchema
  );
