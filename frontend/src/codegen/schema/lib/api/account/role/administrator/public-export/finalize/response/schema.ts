/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicExportFinalizeSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/finalize/response.schema.json",
  title: "APIResponsePublicExportFinalize",
  description: "Response of the public export finalization.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/entity.schema.json",
    },
  },
} as const;
