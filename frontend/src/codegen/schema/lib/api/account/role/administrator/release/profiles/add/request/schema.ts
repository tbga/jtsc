/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestReleaseProfilesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/profiles/add/request.schema.json",
  title: "APIRequestReleaseProfilesAdd",
  description: "Add profiles to a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of profile IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
