/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestReleaseUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/update/request.schema.json",
  title: "APIRequestReleaseUpdate",
  description: "Update release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/update.schema.json",
    },
  },
} as const;
