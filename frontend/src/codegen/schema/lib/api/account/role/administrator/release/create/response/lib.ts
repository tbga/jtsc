/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleaseCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleaseCreate } from "./types";
export const validateAPIResponseReleaseCreate =
  createValidator<IAPIResponseReleaseCreate>(
    validate as ValidateFunction<IAPIResponseReleaseCreate>,
    aPIResponseReleaseCreateSchema as unknown as IJSONSchema
  );
