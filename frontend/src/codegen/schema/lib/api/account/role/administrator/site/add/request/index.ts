/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestSiteAddSchema } from "./schema";
export { validateAPIRequestSiteAdd } from "./lib";
export type { IAPIRequestSiteAdd } from "./types";
