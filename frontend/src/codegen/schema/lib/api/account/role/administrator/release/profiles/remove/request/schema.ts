/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestReleaseProfilesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/release/profiles/remove/request.schema.json",
  title: "APIRequestReleaseProfilesRemove",
  description: "Remove profiles from a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of profile IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
