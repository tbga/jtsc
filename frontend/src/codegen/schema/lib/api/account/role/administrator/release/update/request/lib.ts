/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestReleaseUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestReleaseUpdate } from "./types";
export const validateAPIRequestReleaseUpdate =
  createValidator<IAPIRequestReleaseUpdate>(
    validate as ValidateFunction<IAPIRequestReleaseUpdate>,
    aPIRequestReleaseUpdateSchema as unknown as IJSONSchema
  );
