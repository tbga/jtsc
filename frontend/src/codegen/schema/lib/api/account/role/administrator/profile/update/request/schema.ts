/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/update/request.schema.json",
  title: "APIRequestProfileUpdate",
  description: "Profile's update",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/profile/update.schema.json",
    },
  },
} as const;
