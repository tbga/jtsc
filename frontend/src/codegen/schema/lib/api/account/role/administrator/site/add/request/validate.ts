/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/add/request.schema.json",
  title: "APIRequestSiteAdd",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: { $ref: "https://jtsc-schemas.org/entities/site/init.schema.json" },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/site/init.schema.json",
  title: "SiteInit",
  description: "Site initializer.",
  type: "object",
  additionalProperties: false,
  required: ["home_page", "title"],
  properties: {
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      type: "string",
      description:
        "The title used by the site. Most likely prepended/appended in the <title> tag.",
    },
    long_title: {
      type: "string",
      description:
        "Long title, most likely a follow up to the main title on the home page.",
    },
    description: {
      type: "string",
      description:
        "A description of the site, most likely to be a follow up to the long title on the home page.",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
};
const formats0 = require("ajv-formats/dist/formats").fullFormats.uri;
const schema33 = {
  $id: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
  title: "SiteURLTemplates",
  description:
    "[URL templates](tools.ietf.org/html/rfc6570) for a given site. They must include the same data as the site uses for its frontend, even if it's not needed for a successful URL resolution.\nFor example some sites append human-readable text to an ID and the url resolves without the append just fine too. But the text must still be included in the original value of the ID or the template, depending on use case.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    profile_list: {
      description: "A template for the url to the list of profiles.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profiles",
        "{origin}/profiles/{profile_page}",
        "{origin}/profiles{?profile_page}",
      ],
    },
    profile_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    profile: {
      description: "A template for the url to the profile.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profile/{profile_id}",
        "{origin}/profile{?profile_id}",
      ],
    },
    profile_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release_list: {
      description: "A template for the url to the list of releases.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/releases",
        "{origin}/releases/{release_page}",
        "{origin}/releases{?release_page}",
        "{origin}/profile/{profile_id}/releases/{release_page}",
        "{origin}/profile/{profile_id}/releases{?release_page}",
      ],
    },
    release_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release: {
      description: "A template for the url to the release.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/release/{release_id}",
        "{origin}/release{?release_id}",
        "{origin}/{profile_id}/{release_id}",
        "{origin}{?profile_id,release_id}",
        "{origin}/profile/{profile_id}/release/{release_id}",
        "{origin}/profile/{profile_id}/release{?release_id}",
      ],
    },
    release_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
  },
};
const schema34 = {
  $id: "http://jtsc-schemas.org/types/strings/description.schema.json",
  title: "Description",
  type: "string",
  minLength: 1,
  maxLength: 512,
  examples: [
    "Eum omnis vel quod quasi. A aut et eveniet saepe dolor aliquam nulla ea. Et corporis veniam dolorem eos et voluptatem. Voluptatum cupiditate provident provident qui.",
  ],
};
const formats2 =
  /^(?:(?:[^\x00-\x20"'<>%\\^`{|}]|%[0-9a-f]{2})|\{[+#./;?&=,!@|]?(?:[a-z0-9_]|%[0-9a-f]{2})+(?::[1-9][0-9]{0,3}|\*)?(?:,(?:[a-z0-9_]|%[0-9a-f]{2})+(?::[1-9][0-9]{0,3}|\*)?)*\})*$/i;
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate22(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/site/url-templates.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate22.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (
          !(
            key0 === "profile_list" ||
            key0 === "profile_list_notes" ||
            key0 === "profile" ||
            key0 === "profile_notes" ||
            key0 === "release_list" ||
            key0 === "release_list_notes" ||
            key0 === "release" ||
            key0 === "release_notes"
          )
        ) {
          validate22.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.profile_list !== undefined) {
          let data0 = data.profile_list;
          const _errs2 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate22.errors = [
                  {
                    instancePath: instancePath + "/profile_list",
                    schemaPath: "#/properties/profile_list/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["profile_list"] = coerced0;
              }
            }
          }
          if (errors === _errs2) {
            if (errors === _errs2) {
              if (typeof data0 === "string") {
                if (!formats2.test(data0)) {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/profile_list",
                      schemaPath: "#/properties/profile_list/format",
                      keyword: "format",
                      params: { format: "uri-template" },
                      message: 'must match format "' + "uri-template" + '"',
                    },
                  ];
                  return false;
                }
              }
            }
          }
          var valid0 = _errs2 === errors;
        } else {
          var valid0 = true;
        }
        if (valid0) {
          if (data.profile_list_notes !== undefined) {
            let data1 = data.profile_list_notes;
            const _errs4 = errors;
            const _errs5 = errors;
            if (typeof data1 !== "string") {
              let dataType1 = typeof data1;
              let coerced1 = undefined;
              if (!(coerced1 !== undefined)) {
                if (dataType1 == "number" || dataType1 == "boolean") {
                  coerced1 = "" + data1;
                } else if (data1 === null) {
                  coerced1 = "";
                } else {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/profile_list_notes",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced1 !== undefined) {
                data1 = coerced1;
                if (data !== undefined) {
                  data["profile_list_notes"] = coerced1;
                }
              }
            }
            if (errors === _errs5) {
              if (typeof data1 === "string") {
                if (func1(data1) > 512) {
                  validate22.errors = [
                    {
                      instancePath: instancePath + "/profile_list_notes",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                      keyword: "maxLength",
                      params: { limit: 512 },
                      message: "must NOT have more than 512 characters",
                    },
                  ];
                  return false;
                } else {
                  if (func1(data1) < 1) {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/profile_list_notes",
                        schemaPath:
                          "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                        keyword: "minLength",
                        params: { limit: 1 },
                        message: "must NOT have fewer than 1 characters",
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs4 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.profile !== undefined) {
              let data2 = data.profile;
              const _errs7 = errors;
              if (typeof data2 !== "string") {
                let dataType2 = typeof data2;
                let coerced2 = undefined;
                if (!(coerced2 !== undefined)) {
                  if (dataType2 == "number" || dataType2 == "boolean") {
                    coerced2 = "" + data2;
                  } else if (data2 === null) {
                    coerced2 = "";
                  } else {
                    validate22.errors = [
                      {
                        instancePath: instancePath + "/profile",
                        schemaPath: "#/properties/profile/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced2 !== undefined) {
                  data2 = coerced2;
                  if (data !== undefined) {
                    data["profile"] = coerced2;
                  }
                }
              }
              if (errors === _errs7) {
                if (errors === _errs7) {
                  if (typeof data2 === "string") {
                    if (!formats2.test(data2)) {
                      validate22.errors = [
                        {
                          instancePath: instancePath + "/profile",
                          schemaPath: "#/properties/profile/format",
                          keyword: "format",
                          params: { format: "uri-template" },
                          message: 'must match format "' + "uri-template" + '"',
                        },
                      ];
                      return false;
                    }
                  }
                }
              }
              var valid0 = _errs7 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.profile_notes !== undefined) {
                let data3 = data.profile_notes;
                const _errs9 = errors;
                const _errs10 = errors;
                if (typeof data3 !== "string") {
                  let dataType3 = typeof data3;
                  let coerced3 = undefined;
                  if (!(coerced3 !== undefined)) {
                    if (dataType3 == "number" || dataType3 == "boolean") {
                      coerced3 = "" + data3;
                    } else if (data3 === null) {
                      coerced3 = "";
                    } else {
                      validate22.errors = [
                        {
                          instancePath: instancePath + "/profile_notes",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced3 !== undefined) {
                    data3 = coerced3;
                    if (data !== undefined) {
                      data["profile_notes"] = coerced3;
                    }
                  }
                }
                if (errors === _errs10) {
                  if (typeof data3 === "string") {
                    if (func1(data3) > 512) {
                      validate22.errors = [
                        {
                          instancePath: instancePath + "/profile_notes",
                          schemaPath:
                            "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                          keyword: "maxLength",
                          params: { limit: 512 },
                          message: "must NOT have more than 512 characters",
                        },
                      ];
                      return false;
                    } else {
                      if (func1(data3) < 1) {
                        validate22.errors = [
                          {
                            instancePath: instancePath + "/profile_notes",
                            schemaPath:
                              "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                            keyword: "minLength",
                            params: { limit: 1 },
                            message: "must NOT have fewer than 1 characters",
                          },
                        ];
                        return false;
                      }
                    }
                  }
                }
                var valid0 = _errs9 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.release_list !== undefined) {
                  let data4 = data.release_list;
                  const _errs12 = errors;
                  if (typeof data4 !== "string") {
                    let dataType4 = typeof data4;
                    let coerced4 = undefined;
                    if (!(coerced4 !== undefined)) {
                      if (dataType4 == "number" || dataType4 == "boolean") {
                        coerced4 = "" + data4;
                      } else if (data4 === null) {
                        coerced4 = "";
                      } else {
                        validate22.errors = [
                          {
                            instancePath: instancePath + "/release_list",
                            schemaPath: "#/properties/release_list/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced4 !== undefined) {
                      data4 = coerced4;
                      if (data !== undefined) {
                        data["release_list"] = coerced4;
                      }
                    }
                  }
                  if (errors === _errs12) {
                    if (errors === _errs12) {
                      if (typeof data4 === "string") {
                        if (!formats2.test(data4)) {
                          validate22.errors = [
                            {
                              instancePath: instancePath + "/release_list",
                              schemaPath: "#/properties/release_list/format",
                              keyword: "format",
                              params: { format: "uri-template" },
                              message:
                                'must match format "' + "uri-template" + '"',
                            },
                          ];
                          return false;
                        }
                      }
                    }
                  }
                  var valid0 = _errs12 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.release_list_notes !== undefined) {
                    let data5 = data.release_list_notes;
                    const _errs14 = errors;
                    const _errs15 = errors;
                    if (typeof data5 !== "string") {
                      let dataType5 = typeof data5;
                      let coerced5 = undefined;
                      if (!(coerced5 !== undefined)) {
                        if (dataType5 == "number" || dataType5 == "boolean") {
                          coerced5 = "" + data5;
                        } else if (data5 === null) {
                          coerced5 = "";
                        } else {
                          validate22.errors = [
                            {
                              instancePath:
                                instancePath + "/release_list_notes",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced5 !== undefined) {
                        data5 = coerced5;
                        if (data !== undefined) {
                          data["release_list_notes"] = coerced5;
                        }
                      }
                    }
                    if (errors === _errs15) {
                      if (typeof data5 === "string") {
                        if (func1(data5) > 512) {
                          validate22.errors = [
                            {
                              instancePath:
                                instancePath + "/release_list_notes",
                              schemaPath:
                                "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 512 },
                              message: "must NOT have more than 512 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data5) < 1) {
                            validate22.errors = [
                              {
                                instancePath:
                                  instancePath + "/release_list_notes",
                                schemaPath:
                                  "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid0 = _errs14 === errors;
                  } else {
                    var valid0 = true;
                  }
                  if (valid0) {
                    if (data.release !== undefined) {
                      let data6 = data.release;
                      const _errs17 = errors;
                      if (typeof data6 !== "string") {
                        let dataType6 = typeof data6;
                        let coerced6 = undefined;
                        if (!(coerced6 !== undefined)) {
                          if (dataType6 == "number" || dataType6 == "boolean") {
                            coerced6 = "" + data6;
                          } else if (data6 === null) {
                            coerced6 = "";
                          } else {
                            validate22.errors = [
                              {
                                instancePath: instancePath + "/release",
                                schemaPath: "#/properties/release/type",
                                keyword: "type",
                                params: { type: "string" },
                                message: "must be string",
                              },
                            ];
                            return false;
                          }
                        }
                        if (coerced6 !== undefined) {
                          data6 = coerced6;
                          if (data !== undefined) {
                            data["release"] = coerced6;
                          }
                        }
                      }
                      if (errors === _errs17) {
                        if (errors === _errs17) {
                          if (typeof data6 === "string") {
                            if (!formats2.test(data6)) {
                              validate22.errors = [
                                {
                                  instancePath: instancePath + "/release",
                                  schemaPath: "#/properties/release/format",
                                  keyword: "format",
                                  params: { format: "uri-template" },
                                  message:
                                    'must match format "' +
                                    "uri-template" +
                                    '"',
                                },
                              ];
                              return false;
                            }
                          }
                        }
                      }
                      var valid0 = _errs17 === errors;
                    } else {
                      var valid0 = true;
                    }
                    if (valid0) {
                      if (data.release_notes !== undefined) {
                        let data7 = data.release_notes;
                        const _errs19 = errors;
                        const _errs20 = errors;
                        if (typeof data7 !== "string") {
                          let dataType7 = typeof data7;
                          let coerced7 = undefined;
                          if (!(coerced7 !== undefined)) {
                            if (
                              dataType7 == "number" ||
                              dataType7 == "boolean"
                            ) {
                              coerced7 = "" + data7;
                            } else if (data7 === null) {
                              coerced7 = "";
                            } else {
                              validate22.errors = [
                                {
                                  instancePath: instancePath + "/release_notes",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/description.schema.json/type",
                                  keyword: "type",
                                  params: { type: "string" },
                                  message: "must be string",
                                },
                              ];
                              return false;
                            }
                          }
                          if (coerced7 !== undefined) {
                            data7 = coerced7;
                            if (data !== undefined) {
                              data["release_notes"] = coerced7;
                            }
                          }
                        }
                        if (errors === _errs20) {
                          if (typeof data7 === "string") {
                            if (func1(data7) > 512) {
                              validate22.errors = [
                                {
                                  instancePath: instancePath + "/release_notes",
                                  schemaPath:
                                    "http://jtsc-schemas.org/types/strings/description.schema.json/maxLength",
                                  keyword: "maxLength",
                                  params: { limit: 512 },
                                  message:
                                    "must NOT have more than 512 characters",
                                },
                              ];
                              return false;
                            } else {
                              if (func1(data7) < 1) {
                                validate22.errors = [
                                  {
                                    instancePath:
                                      instancePath + "/release_notes",
                                    schemaPath:
                                      "http://jtsc-schemas.org/types/strings/description.schema.json/minLength",
                                    keyword: "minLength",
                                    params: { limit: 1 },
                                    message:
                                      "must NOT have fewer than 1 characters",
                                  },
                                ];
                                return false;
                              }
                            }
                          }
                        }
                        var valid0 = _errs19 === errors;
                      } else {
                        var valid0 = true;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate22.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate22.errors = vErrors;
  return errors === 0;
}
validate22.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/site/init.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (
        (data.home_page === undefined && (missing0 = "home_page")) ||
        (data.title === undefined && (missing0 = "title"))
      ) {
        validate21.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (
            !(
              key0 === "home_page" ||
              key0 === "title" ||
              key0 === "long_title" ||
              key0 === "description" ||
              key0 === "url_templates"
            )
          ) {
            validate21.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.home_page !== undefined) {
            let data0 = data.home_page;
            const _errs2 = errors;
            if (typeof data0 !== "string") {
              let dataType0 = typeof data0;
              let coerced0 = undefined;
              if (!(coerced0 !== undefined)) {
                if (dataType0 == "number" || dataType0 == "boolean") {
                  coerced0 = "" + data0;
                } else if (data0 === null) {
                  coerced0 = "";
                } else {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/home_page",
                      schemaPath: "#/properties/home_page/type",
                      keyword: "type",
                      params: { type: "string" },
                      message: "must be string",
                    },
                  ];
                  return false;
                }
              }
              if (coerced0 !== undefined) {
                data0 = coerced0;
                if (data !== undefined) {
                  data["home_page"] = coerced0;
                }
              }
            }
            if (errors === _errs2) {
              if (errors === _errs2) {
                if (typeof data0 === "string") {
                  if (!formats0(data0)) {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/home_page",
                        schemaPath: "#/properties/home_page/format",
                        keyword: "format",
                        params: { format: "uri" },
                        message: 'must match format "' + "uri" + '"',
                      },
                    ];
                    return false;
                  }
                }
              }
            }
            var valid0 = _errs2 === errors;
          } else {
            var valid0 = true;
          }
          if (valid0) {
            if (data.title !== undefined) {
              let data1 = data.title;
              const _errs4 = errors;
              if (typeof data1 !== "string") {
                let dataType1 = typeof data1;
                let coerced1 = undefined;
                if (!(coerced1 !== undefined)) {
                  if (dataType1 == "number" || dataType1 == "boolean") {
                    coerced1 = "" + data1;
                  } else if (data1 === null) {
                    coerced1 = "";
                  } else {
                    validate21.errors = [
                      {
                        instancePath: instancePath + "/title",
                        schemaPath: "#/properties/title/type",
                        keyword: "type",
                        params: { type: "string" },
                        message: "must be string",
                      },
                    ];
                    return false;
                  }
                }
                if (coerced1 !== undefined) {
                  data1 = coerced1;
                  if (data !== undefined) {
                    data["title"] = coerced1;
                  }
                }
              }
              var valid0 = _errs4 === errors;
            } else {
              var valid0 = true;
            }
            if (valid0) {
              if (data.long_title !== undefined) {
                let data2 = data.long_title;
                const _errs6 = errors;
                if (typeof data2 !== "string") {
                  let dataType2 = typeof data2;
                  let coerced2 = undefined;
                  if (!(coerced2 !== undefined)) {
                    if (dataType2 == "number" || dataType2 == "boolean") {
                      coerced2 = "" + data2;
                    } else if (data2 === null) {
                      coerced2 = "";
                    } else {
                      validate21.errors = [
                        {
                          instancePath: instancePath + "/long_title",
                          schemaPath: "#/properties/long_title/type",
                          keyword: "type",
                          params: { type: "string" },
                          message: "must be string",
                        },
                      ];
                      return false;
                    }
                  }
                  if (coerced2 !== undefined) {
                    data2 = coerced2;
                    if (data !== undefined) {
                      data["long_title"] = coerced2;
                    }
                  }
                }
                var valid0 = _errs6 === errors;
              } else {
                var valid0 = true;
              }
              if (valid0) {
                if (data.description !== undefined) {
                  let data3 = data.description;
                  const _errs8 = errors;
                  if (typeof data3 !== "string") {
                    let dataType3 = typeof data3;
                    let coerced3 = undefined;
                    if (!(coerced3 !== undefined)) {
                      if (dataType3 == "number" || dataType3 == "boolean") {
                        coerced3 = "" + data3;
                      } else if (data3 === null) {
                        coerced3 = "";
                      } else {
                        validate21.errors = [
                          {
                            instancePath: instancePath + "/description",
                            schemaPath: "#/properties/description/type",
                            keyword: "type",
                            params: { type: "string" },
                            message: "must be string",
                          },
                        ];
                        return false;
                      }
                    }
                    if (coerced3 !== undefined) {
                      data3 = coerced3;
                      if (data !== undefined) {
                        data["description"] = coerced3;
                      }
                    }
                  }
                  var valid0 = _errs8 === errors;
                } else {
                  var valid0 = true;
                }
                if (valid0) {
                  if (data.url_templates !== undefined) {
                    const _errs10 = errors;
                    if (
                      !validate22(data.url_templates, {
                        instancePath: instancePath + "/url_templates",
                        parentData: data,
                        parentDataProperty: "url_templates",
                        rootData,
                        dynamicAnchors,
                      })
                    ) {
                      vErrors =
                        vErrors === null
                          ? validate22.errors
                          : vErrors.concat(validate22.errors);
                      errors = vErrors.length;
                    }
                    var valid0 = _errs10 === errors;
                  } else {
                    var valid0 = true;
                  }
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/site/add/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            if (
              !validate21(data.data, {
                instancePath: instancePath + "/data",
                parentData: data,
                parentDataProperty: "data",
                rootData,
                dynamicAnchors,
              })
            ) {
              vErrors =
                vErrors === null
                  ? validate21.errors
                  : vErrors.concat(validate21.errors);
              errors = vErrors.length;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
