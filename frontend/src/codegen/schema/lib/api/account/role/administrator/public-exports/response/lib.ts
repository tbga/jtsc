/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponsePublicExportsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponsePublicExports } from "./types";
export const validateAPIResponsePublicExports =
  createValidator<IAPIResponsePublicExports>(
    validate as ValidateFunction<IAPIResponsePublicExports>,
    aPIResponsePublicExportsSchema as unknown as IJSONSchema
  );
