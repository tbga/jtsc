/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseReleaseProfilesRemoveSchema } from "./schema";
export { validateAPIResponseReleaseProfilesRemove } from "./lib";
export type { IAPIResponseReleaseProfilesRemove } from "./types";
