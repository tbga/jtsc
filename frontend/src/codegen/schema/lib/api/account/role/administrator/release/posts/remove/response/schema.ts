/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseReleasePostsSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/remove/response.schema.json",
  title: "APIResponseReleasePosts",
  description: "Removed release posts.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed posts.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
