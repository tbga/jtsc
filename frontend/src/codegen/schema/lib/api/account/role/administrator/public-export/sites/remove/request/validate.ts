/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/remove/request.schema.json",
  title: "APIRequestPublicExportSitesRemove",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of site IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
};
const schema32 = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
const func0 = require("ajv/dist/runtime/equal").default;
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/remove/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            let data0 = data.data;
            const _errs2 = errors;
            if (errors === _errs2) {
              if (Array.isArray(data0)) {
                if (data0.length < 1) {
                  validate20.errors = [
                    {
                      instancePath: instancePath + "/data",
                      schemaPath: "#/properties/data/minItems",
                      keyword: "minItems",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 items",
                    },
                  ];
                  return false;
                } else {
                  var valid1 = true;
                  const len0 = data0.length;
                  for (let i0 = 0; i0 < len0; i0++) {
                    let data1 = data0[i0];
                    const _errs4 = errors;
                    const _errs5 = errors;
                    if (typeof data1 !== "string") {
                      let dataType0 = typeof data1;
                      let coerced0 = undefined;
                      if (!(coerced0 !== undefined)) {
                        if (dataType0 == "number" || dataType0 == "boolean") {
                          coerced0 = "" + data1;
                        } else if (data1 === null) {
                          coerced0 = "";
                        } else {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/data/" + i0,
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/type",
                              keyword: "type",
                              params: { type: "string" },
                              message: "must be string",
                            },
                          ];
                          return false;
                        }
                      }
                      if (coerced0 !== undefined) {
                        data1 = coerced0;
                        if (data0 !== undefined) {
                          data0[i0] = coerced0;
                        }
                      }
                    }
                    if (errors === _errs5) {
                      if (typeof data1 === "string") {
                        if (func1(data1) > 19) {
                          validate20.errors = [
                            {
                              instancePath: instancePath + "/data/" + i0,
                              schemaPath:
                                "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/maxLength",
                              keyword: "maxLength",
                              params: { limit: 19 },
                              message: "must NOT have more than 19 characters",
                            },
                          ];
                          return false;
                        } else {
                          if (func1(data1) < 1) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/data/" + i0,
                                schemaPath:
                                  "http://jtsc-schemas.org/types/numbers/big-serial.schema.json/minLength",
                                keyword: "minLength",
                                params: { limit: 1 },
                                message:
                                  "must NOT have fewer than 1 characters",
                              },
                            ];
                            return false;
                          }
                        }
                      }
                    }
                    var valid1 = _errs4 === errors;
                    if (!valid1) {
                      break;
                    }
                  }
                  if (valid1) {
                    let i1 = data0.length;
                    let j0;
                    if (i1 > 1) {
                      outer0: for (; i1--; ) {
                        for (j0 = i1; j0--; ) {
                          if (func0(data0[i1], data0[j0])) {
                            validate20.errors = [
                              {
                                instancePath: instancePath + "/data",
                                schemaPath: "#/properties/data/uniqueItems",
                                keyword: "uniqueItems",
                                params: { i: i1, j: j0 },
                                message:
                                  "must NOT have duplicate items (items ## " +
                                  j0 +
                                  " and " +
                                  i1 +
                                  " are identical)",
                              },
                            ];
                            return false;
                            break outer0;
                          }
                        }
                      }
                    }
                  }
                }
              } else {
                validate20.errors = [
                  {
                    instancePath: instancePath + "/data",
                    schemaPath: "#/properties/data/type",
                    keyword: "type",
                    params: { type: "array" },
                    message: "must be array",
                  },
                ];
                return false;
              }
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
