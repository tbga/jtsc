/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IReleaseInit } from "#codegen/schema/lib/entities/release/init";

/**
 * Add post releases request.
 */
export interface IAPIRequestPostReleasesAdd {
  /**
   * Post releases init data.
   */
  data: {
    /**
     * A list of existing release IDs.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
    /**
     * A list of release initializers.
     *
     * @minItems 1
     */
    inits?: [IReleaseInit, ...IReleaseInit[]];
  };
}
