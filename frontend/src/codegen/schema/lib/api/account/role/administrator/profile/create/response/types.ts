/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseProfileCreate {
  is_successful: true;
  data: IEntityItem;
}
