/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicExportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/create/response.schema.json",
  title: "APIResponsePublicExportCreate",
  description: "Response of the public export creation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
