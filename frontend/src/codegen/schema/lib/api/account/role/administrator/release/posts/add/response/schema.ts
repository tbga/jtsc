/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseReleasePostsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/add/response.schema.json",
  title: "APIResponseReleasePostsAdd",
  description: "Added release posts.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added posts.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
