/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestProfileArtistsRemoveSchema } from "./schema";
export { validateAPIRequestProfileArtistsRemove } from "./lib";
export type { IAPIRequestProfileArtistsRemove } from "./types";
