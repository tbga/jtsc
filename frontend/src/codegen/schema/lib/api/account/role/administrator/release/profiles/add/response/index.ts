/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseReleaseProfilesAddSchema } from "./schema";
export { validateAPIResponseReleaseProfilesAdd } from "./lib";
export type { IAPIResponseReleaseProfilesAdd } from "./types";
