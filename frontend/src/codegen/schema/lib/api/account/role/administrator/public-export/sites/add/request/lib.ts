/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPublicExportSitesAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPublicExportSitesAdd } from "./types";
export const validateAPIRequestPublicExportSitesAdd =
  createValidator<IAPIRequestPublicExportSitesAdd>(
    validate as ValidateFunction<IAPIRequestPublicExportSitesAdd>,
    aPIRequestPublicExportSitesAddSchema as unknown as IJSONSchema
  );
