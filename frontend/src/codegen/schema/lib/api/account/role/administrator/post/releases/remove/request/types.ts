/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Remove post releases request.
 */
export interface IAPIRequestPostReleasesRemove {
  /**
   * A list of release IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
