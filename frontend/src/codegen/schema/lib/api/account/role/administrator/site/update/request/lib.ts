/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestSiteUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestSiteUpdate } from "./types";
export const validateAPIRequestSiteUpdate =
  createValidator<IAPIRequestSiteUpdate>(
    validate as ValidateFunction<IAPIRequestSiteUpdate>,
    aPIRequestSiteUpdateSchema as unknown as IJSONSchema
  );
