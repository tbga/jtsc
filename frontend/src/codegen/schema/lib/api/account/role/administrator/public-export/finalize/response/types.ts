/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPublicExport } from "#codegen/schema/lib/entities/public-export/entity";

/**
 * Response of the public export finalization.
 */
export interface IAPIResponsePublicExportFinalize {
  is_successful: true;
  data: IPublicExport;
}
