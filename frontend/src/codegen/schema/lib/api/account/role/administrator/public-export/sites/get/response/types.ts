/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type ISitePreview } from "#codegen/schema/lib/entities/site/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePublicExportSites {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * @minItems 1
     */
    sites: [ISitePreview, ...ISitePreview[]];
  };
}
