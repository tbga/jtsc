/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleaseProfilesRemoveSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleaseProfilesRemove } from "./types";
export const validateAPIResponseReleaseProfilesRemove =
  createValidator<IAPIResponseReleaseProfilesRemove>(
    validate as ValidateFunction<IAPIResponseReleaseProfilesRemove>,
    aPIResponseReleaseProfilesRemoveSchema as unknown as IJSONSchema
  );
