/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Removed profile artists.
 */
export interface IAPIResponseProfileArtistsRemove {
  is_successful: true;
  /**
   * A list of removed artists.
   */
  data: IEntityItem[];
}
