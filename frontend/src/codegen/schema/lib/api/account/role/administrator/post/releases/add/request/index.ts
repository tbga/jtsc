/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestPostReleasesAddSchema } from "./schema";
export { validateAPIRequestPostReleasesAdd } from "./lib";
export type { IAPIRequestPostReleasesAdd } from "./types";
