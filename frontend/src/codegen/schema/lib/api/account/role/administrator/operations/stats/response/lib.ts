/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseOperationsStatsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseOperationsStats } from "./types";
export const validateAPIResponseOperationsStats =
  createValidator<IAPIResponseOperationsStats>(
    validate as ValidateFunction<IAPIResponseOperationsStats>,
    aPIResponseOperationsStatsSchema as unknown as IJSONSchema
  );
