/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISiteInit } from "#codegen/schema/lib/entities/site/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestSiteAdd {
  data: ISiteInit;
}
