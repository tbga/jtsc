/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseReleaseUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseReleaseUpdate } from "./types";
export const validateAPIResponseReleaseUpdate =
  createValidator<IAPIResponseReleaseUpdate>(
    validate as ValidateFunction<IAPIResponseReleaseUpdate>,
    aPIResponseReleaseUpdateSchema as unknown as IJSONSchema
  );
