/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestArtistUpdateSchema } from "./schema";
export { validateAPIRequestArtistUpdate } from "./lib";
export type { IAPIRequestArtistUpdate } from "./types";
