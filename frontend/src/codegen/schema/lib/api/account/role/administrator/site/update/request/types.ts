/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ISiteUpdate } from "#codegen/schema/lib/entities/site/update";

/**
 * Site update request.
 */
export interface IAPIRequestSiteUpdate {
  data: ISiteUpdate;
}
