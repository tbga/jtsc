/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestPostArtistsRemoveSchema } from "./schema";
export { validateAPIRequestPostArtistsRemove } from "./lib";
export type { IAPIRequestPostArtistsRemove } from "./types";
