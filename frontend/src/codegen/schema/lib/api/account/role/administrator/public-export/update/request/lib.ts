/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPublicExportUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPublicExportUpdate } from "./types";
export const validateAPIRequestPublicExportUpdate =
  createValidator<IAPIRequestPublicExportUpdate>(
    validate as ValidateFunction<IAPIRequestPublicExportUpdate>,
    aPIRequestPublicExportUpdateSchema as unknown as IJSONSchema
  );
