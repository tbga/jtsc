/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IPublicExportPreview } from "#codegen/schema/lib/entities/public-export/preview";

/**
 * A list of public exports.
 */
export interface IAPIResponsePublicExports {
  is_successful: true;
  data: {
    pagination: IPagination;
    public_exports: IPublicExportPreview[];
  };
}
