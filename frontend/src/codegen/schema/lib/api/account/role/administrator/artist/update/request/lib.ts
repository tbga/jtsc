/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestArtistUpdateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestArtistUpdate } from "./types";
export const validateAPIRequestArtistUpdate =
  createValidator<IAPIRequestArtistUpdate>(
    validate as ValidateFunction<IAPIRequestArtistUpdate>,
    aPIRequestArtistUpdateSchema as unknown as IJSONSchema
  );
