/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseOperationsStatsSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/operations/stats/response.schema.json",
  title: "APIResponseOperationsStats",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "Operations overview.",
      type: "object",
      additionalProperties: false,
      required: ["all", "pending", "in_progress", "finished", "failed"],
      properties: {
        all: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        pending: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        in_progress: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        finished: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
        failed: {
          $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
        },
      },
    },
  },
} as const;
