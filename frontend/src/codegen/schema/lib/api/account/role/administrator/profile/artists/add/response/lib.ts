/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseProfileArtistsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseProfileArtistsAdd } from "./types";
export const validateAPIResponseProfileArtistsAdd =
  createValidator<IAPIResponseProfileArtistsAdd>(
    validate as ValidateFunction<IAPIResponseProfileArtistsAdd>,
    aPIResponseProfileArtistsAddSchema as unknown as IJSONSchema
  );
