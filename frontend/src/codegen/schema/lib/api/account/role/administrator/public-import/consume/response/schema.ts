/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePublicImportConsumeSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/consume/response.schema.json",
  title: "APIResponsePublicImportConsume",
  description: "An entity item for consumption operation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
