/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseReleaseCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/create/response.schema.json",
  title: "APIResponseReleaseCreate",
  description: "Response of the release creation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
