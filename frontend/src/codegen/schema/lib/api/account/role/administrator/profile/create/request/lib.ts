/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestProfileCreateSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestProfileCreate } from "./types";
export const validateAPIRequestProfileCreate =
  createValidator<IAPIRequestProfileCreate>(
    validate as ValidateFunction<IAPIRequestProfileCreate>,
    aPIRequestProfileCreateSchema as unknown as IJSONSchema
  );
