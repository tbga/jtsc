/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * An entity item for creaton operation.
 */
export interface IAPIResponsePublicImportCreate {
  is_successful: true;
  data: IEntityItem;
}
