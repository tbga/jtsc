/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPostArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/artists/add/request.schema.json",
  title: "APIRequestPostArtistsAdd",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
