/*
 * This file was generated automatically, do not edit it by hand.
 */
// @ts-nocheck
"use strict";
export const validate = validate20;
export default validate20;
const schema31 = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/update/request.schema.json",
  title: "APIRequestPublicExportUpdate",
  description: "Public export update.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
    },
  },
};
const schema32 = {
  $id: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
  title: "PublicExportUpdate",
  description: "An update schema for an entity.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    title: { $ref: "http://jtsc-schemas.org/types/strings/title.schema.json" },
  },
};
const schema33 = {
  $id: "http://jtsc-schemas.org/types/strings/title.schema.json",
  title: "Title",
  description:
    "A title string. Maximum known length is of [that release](https://en.wikipedia.org/wiki/Night_of_the_Day_of_the_Dawn_of_the_Son_of_the_Bride_of_the_Return_of_the_Revenge_of_the_Terror_of_the_Attack_of_the_Evil,_Mutant,_Alien,_Flesh_Eating,_Hellbound,_Zombified_Living_Dead#Part_5).",
  type: "string",
  minLength: 1,
  maxLength: 216,
  examples: [
    "Night Of The Day Of The Dawn Of The Son Of The Bride Of The Return Of The Revenge Of The Terror Of The Attack Of The Evil, Mutant, Hellbound, Flesh-Eating, Crawling, Alien, Zombified, Subhumanoid Living Dead — Part 5",
  ],
};
const func1 = require("ajv/dist/runtime/ucs2length").default;
function validate21(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/entities/public-export/update.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate21.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      const _errs1 = errors;
      for (const key0 in data) {
        if (!(key0 === "title")) {
          validate21.errors = [
            {
              instancePath,
              schemaPath: "#/additionalProperties",
              keyword: "additionalProperties",
              params: { additionalProperty: key0 },
              message: "must NOT have additional properties",
            },
          ];
          return false;
          break;
        }
      }
      if (_errs1 === errors) {
        if (data.title !== undefined) {
          let data0 = data.title;
          const _errs3 = errors;
          if (typeof data0 !== "string") {
            let dataType0 = typeof data0;
            let coerced0 = undefined;
            if (!(coerced0 !== undefined)) {
              if (dataType0 == "number" || dataType0 == "boolean") {
                coerced0 = "" + data0;
              } else if (data0 === null) {
                coerced0 = "";
              } else {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/title",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/title.schema.json/type",
                    keyword: "type",
                    params: { type: "string" },
                    message: "must be string",
                  },
                ];
                return false;
              }
            }
            if (coerced0 !== undefined) {
              data0 = coerced0;
              if (data !== undefined) {
                data["title"] = coerced0;
              }
            }
          }
          if (errors === _errs3) {
            if (typeof data0 === "string") {
              if (func1(data0) > 216) {
                validate21.errors = [
                  {
                    instancePath: instancePath + "/title",
                    schemaPath:
                      "http://jtsc-schemas.org/types/strings/title.schema.json/maxLength",
                    keyword: "maxLength",
                    params: { limit: 216 },
                    message: "must NOT have more than 216 characters",
                  },
                ];
                return false;
              } else {
                if (func1(data0) < 1) {
                  validate21.errors = [
                    {
                      instancePath: instancePath + "/title",
                      schemaPath:
                        "http://jtsc-schemas.org/types/strings/title.schema.json/minLength",
                      keyword: "minLength",
                      params: { limit: 1 },
                      message: "must NOT have fewer than 1 characters",
                    },
                  ];
                  return false;
                }
              }
            }
          }
        }
      }
    } else {
      validate21.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate21.errors = vErrors;
  return errors === 0;
}
validate21.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
function validate20(
  data,
  {
    instancePath = "",
    parentData,
    parentDataProperty,
    rootData = data,
    dynamicAnchors = {},
  } = {}
) {
  /*# sourceURL="https://jtsc-schemas.org/api/account/role/administrator/public-export/update/request.schema.json" */ let vErrors =
    null;
  let errors = 0;
  const evaluated0 = validate20.evaluated;
  if (evaluated0.dynamicProps) {
    evaluated0.props = undefined;
  }
  if (evaluated0.dynamicItems) {
    evaluated0.items = undefined;
  }
  if (errors === 0) {
    if (data && typeof data == "object" && !Array.isArray(data)) {
      let missing0;
      if (data.data === undefined && (missing0 = "data")) {
        validate20.errors = [
          {
            instancePath,
            schemaPath: "#/required",
            keyword: "required",
            params: { missingProperty: missing0 },
            message: "must have required property '" + missing0 + "'",
          },
        ];
        return false;
      } else {
        const _errs1 = errors;
        for (const key0 in data) {
          if (!(key0 === "data")) {
            validate20.errors = [
              {
                instancePath,
                schemaPath: "#/additionalProperties",
                keyword: "additionalProperties",
                params: { additionalProperty: key0 },
                message: "must NOT have additional properties",
              },
            ];
            return false;
            break;
          }
        }
        if (_errs1 === errors) {
          if (data.data !== undefined) {
            if (
              !validate21(data.data, {
                instancePath: instancePath + "/data",
                parentData: data,
                parentDataProperty: "data",
                rootData,
                dynamicAnchors,
              })
            ) {
              vErrors =
                vErrors === null
                  ? validate21.errors
                  : vErrors.concat(validate21.errors);
              errors = vErrors.length;
            }
          }
        }
      }
    } else {
      validate20.errors = [
        {
          instancePath,
          schemaPath: "#/type",
          keyword: "type",
          params: { type: "object" },
          message: "must be object",
        },
      ];
      return false;
    }
  }
  validate20.errors = vErrors;
  return errors === 0;
}
validate20.evaluated = {
  props: true,
  dynamicProps: false,
  dynamicItems: false,
};
