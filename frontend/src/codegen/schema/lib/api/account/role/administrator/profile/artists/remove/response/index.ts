/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseProfileArtistsRemoveSchema } from "./schema";
export { validateAPIResponseProfileArtistsRemove } from "./lib";
export type { IAPIResponseProfileArtistsRemove } from "./types";
