/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestProfileArtistsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/remove/request.schema.json",
  title: "APIRequestProfileArtistsRemove",
  description: "Remove profile artists request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
