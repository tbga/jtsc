/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPublicExportInit } from "#codegen/schema/lib/entities/public-export/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestPublicExportCreate {
  data: IPublicExportInit;
}
