/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestSiteAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/add/request.schema.json",
  title: "APIRequestSiteAdd",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/init.schema.json",
    },
  },
} as const;
