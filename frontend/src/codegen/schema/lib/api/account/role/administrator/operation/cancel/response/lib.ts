/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseOperationCancelSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseOperationCancel } from "./types";
export const validateAPIResponseOperationCancel =
  createValidator<IAPIResponseOperationCancel>(
    validate as ValidateFunction<IAPIResponseOperationCancel>,
    aPIResponseOperationCancelSchema as unknown as IJSONSchema
  );
