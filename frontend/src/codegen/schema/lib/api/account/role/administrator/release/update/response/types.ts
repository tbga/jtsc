/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IRelease } from "#codegen/schema/lib/entities/release/entity";

/**
 * Updated release.
 */
export interface IAPIResponseReleaseUpdate {
  is_successful: true;
  data: IRelease;
}
