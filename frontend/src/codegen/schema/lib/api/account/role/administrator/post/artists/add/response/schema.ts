/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponsePostArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/artists/add/response.schema.json",
  title: "APIResponsePostArtistsAdd",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added artists.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
