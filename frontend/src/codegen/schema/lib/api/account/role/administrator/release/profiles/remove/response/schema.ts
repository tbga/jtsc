/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseReleaseProfilesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/release/profiles/remove/response.schema.json",
  title: "APIResponseReleaseProfilesRemove",
  description: "Removed release profiles.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed profiles.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
