/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIRequestReleaseProfilesAddSchema } from "./schema";
export { validateAPIRequestReleaseProfilesAdd } from "./lib";
export type { IAPIRequestReleaseProfilesAdd } from "./types";
