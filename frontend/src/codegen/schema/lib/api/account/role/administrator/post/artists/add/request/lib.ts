/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIRequestPostArtistsAddSchema } from "./schema";
import { validate } from "./validate";
import { IAPIRequestPostArtistsAdd } from "./types";
export const validateAPIRequestPostArtistsAdd =
  createValidator<IAPIRequestPostArtistsAdd>(
    validate as ValidateFunction<IAPIRequestPostArtistsAdd>,
    aPIRequestPostArtistsAddSchema as unknown as IJSONSchema
  );
