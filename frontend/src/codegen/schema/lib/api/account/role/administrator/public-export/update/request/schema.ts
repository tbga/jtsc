/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIRequestPublicExportUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/update/request.schema.json",
  title: "APIRequestPublicExportUpdate",
  description: "Public export update.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
    },
  },
} as const;
