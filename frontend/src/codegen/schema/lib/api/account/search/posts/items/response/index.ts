/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSearchPostItemsSchema } from "./schema";
export { validateAPIResponseSearchPostItems } from "./lib";
export type { IAPIResponseSearchPostItems } from "./types";
