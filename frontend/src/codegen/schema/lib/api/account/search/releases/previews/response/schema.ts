/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSearchReleasePreviewsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/releases/previews/response.schema.json",
  title: "APIResponseSearchReleasePreviews",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "releases"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        releases: {
          description: "A list of release previews.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
