/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSearchReleaseItemsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSearchReleaseItems } from "./types";
export const validateAPIResponseSearchReleaseItems =
  createValidator<IAPIResponseSearchReleaseItems>(
    validate as ValidateFunction<IAPIResponseSearchReleaseItems>,
    aPIResponseSearchReleaseItemsSchema as unknown as IJSONSchema
  );
