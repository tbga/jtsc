/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSearchPostPreviewsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSearchPostPreviews } from "./types";
export const validateAPIResponseSearchPostPreviews =
  createValidator<IAPIResponseSearchPostPreviews>(
    validate as ValidateFunction<IAPIResponseSearchPostPreviews>,
    aPIResponseSearchPostPreviewsSchema as unknown as IJSONSchema
  );
