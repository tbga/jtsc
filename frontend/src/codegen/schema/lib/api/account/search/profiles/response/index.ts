/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPISearchProfilesSchema } from "./schema";
export { validateAPISearchProfiles } from "./lib";
export type { IAPISearchProfiles } from "./types";
