/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPISearchSiteItemsSchema } from "./schema";
import { validate } from "./validate";
import { IAPISearchSiteItems } from "./types";
export const validateAPISearchSiteItems = createValidator<IAPISearchSiteItems>(
  validate as ValidateFunction<IAPISearchSiteItems>,
  aPISearchSiteItemsSchema as unknown as IJSONSchema
);
