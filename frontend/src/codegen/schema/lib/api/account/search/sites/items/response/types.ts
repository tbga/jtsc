/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Body of the successful API response.
 */
export interface IAPISearchSiteItems {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * A list of file items.
     *
     * @minItems 1
     */
    files: [IEntityItem, ...IEntityItem[]];
  };
}
