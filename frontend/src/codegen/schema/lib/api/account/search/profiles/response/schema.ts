/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPISearchProfilesSchema = {
  $id: "https://jtsc-schemas.org/api/search/profiles/response.schema.json",
  title: "APISearchProfiles",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "profiles"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        profiles: {
          description: "A list of profile previews.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/profile/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
