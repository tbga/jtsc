/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPISearchArtistsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/artists/response.schema.json",
  title: "APISearchArtists",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "artists"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        artists: {
          description: "A list of artist previews.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
