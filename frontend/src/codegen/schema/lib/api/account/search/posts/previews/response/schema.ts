/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseSearchPostPreviewsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/posts/previews/response.schema.json",
  title: "APIResponseSearchPostPreviews",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "posts"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        posts: {
          description: "A list of post previews.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
