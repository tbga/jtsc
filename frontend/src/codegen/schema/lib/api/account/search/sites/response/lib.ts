/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPISearchSitesResponseSchema } from "./schema";
import { validate } from "./validate";
import { IAPISearchSitesResponse } from "./types";
export const validateAPISearchSitesResponse =
  createValidator<IAPISearchSitesResponse>(
    validate as ValidateFunction<IAPISearchSitesResponse>,
    aPISearchSitesResponseSchema as unknown as IJSONSchema
  );
