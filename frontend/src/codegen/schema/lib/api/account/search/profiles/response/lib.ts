/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPISearchProfilesSchema } from "./schema";
import { validate } from "./validate";
import { IAPISearchProfiles } from "./types";
export const validateAPISearchProfiles = createValidator<IAPISearchProfiles>(
  validate as ValidateFunction<IAPISearchProfiles>,
  aPISearchProfilesSchema as unknown as IJSONSchema
);
