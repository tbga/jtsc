/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSearchReleasePreviewsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSearchReleasePreviews } from "./types";
export const validateAPIResponseSearchReleasePreviews =
  createValidator<IAPIResponseSearchReleasePreviews>(
    validate as ValidateFunction<IAPIResponseSearchReleasePreviews>,
    aPIResponseSearchReleasePreviewsSchema as unknown as IJSONSchema
  );
