/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPISearchArtistsSchema } from "./schema";
export { validateAPISearchArtists } from "./lib";
export type { IAPISearchArtists } from "./types";
