/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSearchPostPreviewsSchema } from "./schema";
export { validateAPIResponseSearchPostPreviews } from "./lib";
export type { IAPIResponseSearchPostPreviews } from "./types";
