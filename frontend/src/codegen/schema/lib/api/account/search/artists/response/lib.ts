/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPISearchArtistsSchema } from "./schema";
import { validate } from "./validate";
import { IAPISearchArtists } from "./types";
export const validateAPISearchArtists = createValidator<IAPISearchArtists>(
  validate as ValidateFunction<IAPISearchArtists>,
  aPISearchArtistsSchema as unknown as IJSONSchema
);
