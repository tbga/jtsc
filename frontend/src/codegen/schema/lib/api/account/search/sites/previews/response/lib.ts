/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPISearchSitePreviewsSchema } from "./schema";
import { validate } from "./validate";
import { IAPISearchSitePreviews } from "./types";
export const validateAPISearchSitePreviews =
  createValidator<IAPISearchSitePreviews>(
    validate as ValidateFunction<IAPISearchSitePreviews>,
    aPISearchSitePreviewsSchema as unknown as IJSONSchema
  );
