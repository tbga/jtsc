/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPISearchSitesResponseSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/sites/response.schema.json",
  title: "APISearchSitesResponse",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
    },
  },
} as const;
