/*
 * This file was generated automatically, do not edit it by hand.
 */
export { aPIResponseSearchReleaseItemsSchema } from "./schema";
export { validateAPIResponseSearchReleaseItems } from "./lib";
export type { IAPIResponseSearchReleaseItems } from "./types";
