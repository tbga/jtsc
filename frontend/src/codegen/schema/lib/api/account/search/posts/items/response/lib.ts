/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseSearchPostItemsSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseSearchPostItems } from "./types";
export const validateAPIResponseSearchPostItems =
  createValidator<IAPIResponseSearchPostItems>(
    validate as ValidateFunction<IAPIResponseSearchPostItems>,
    aPIResponseSearchPostItemsSchema as unknown as IJSONSchema
  );
