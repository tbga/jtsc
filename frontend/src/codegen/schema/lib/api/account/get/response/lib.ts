/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { aPIResponseAccountGetSchema } from "./schema";
import { validate } from "./validate";
import { IAPIResponseAccountGet } from "./types";
export const validateAPIResponseAccountGet =
  createValidator<IAPIResponseAccountGet>(
    validate as ValidateFunction<IAPIResponseAccountGet>,
    aPIResponseAccountGetSchema as unknown as IJSONSchema
  );
