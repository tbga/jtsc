/*
 * This file was generated automatically, do not edit it by hand.
 */
export const aPIResponseAccountGetSchema = {
  $id: "https://jtsc-schemas.org/api/account/get/response.schema.json",
  title: "APIResponseAccountGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/entity.schema.json",
    },
  },
} as const;
