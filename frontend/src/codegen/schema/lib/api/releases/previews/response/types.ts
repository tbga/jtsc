/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * List of releases.
 */
export interface IGetReleasesResponse {
  is_successful: true;
  data: {
    pagination: IPagination;
    releases: IReleasePreview[];
  };
}
