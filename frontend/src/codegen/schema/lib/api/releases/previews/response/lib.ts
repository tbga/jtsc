/*
 * This file was generated automatically, do not edit it by hand.
 */
import { type ValidateFunction } from "ajv/dist/2020";
import { createValidator, type IJSONSchema } from "#lib/json-schema";
import { getReleasesResponseSchema } from "./schema";
import { validate } from "./validate";
import { IGetReleasesResponse } from "./types";
export const validateGetReleasesResponse =
  createValidator<IGetReleasesResponse>(
    validate as ValidateFunction<IGetReleasesResponse>,
    getReleasesResponseSchema as unknown as IJSONSchema
  );
