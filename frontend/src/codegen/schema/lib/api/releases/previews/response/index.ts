/*
 * This file was generated automatically, do not edit it by hand.
 */
export { getReleasesResponseSchema } from "./schema";
export { validateGetReleasesResponse } from "./lib";
export type { IGetReleasesResponse } from "./types";
