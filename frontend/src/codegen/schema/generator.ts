import path from "path";
import { isNativeError } from "util/types";
import stringifyObject from "stringify-object";
// import { SCHEMA_FOLDER } from "#environment/derived-variables";
import { IS_WINDOWS } from "#environment/constants";
import { decapitalizeString, multilineString } from "#lib/strings";
import {
  createInterfaceFromSchema,
  type ICodegenNode,
  type ICodegenFunc,
  type ICodegenGraph,
  schemaFileEnd,
  CodegenError,
  type IJSONSchemaCollection,
  type IModule,
  type IExport,
  getSchemaCollection,
  generateValidationCode,
  createCodegenAJV,
} from "#server/codegen";
import { collectRefs, IJSONSchema, IRefSet } from "#lib/json-schema";

const generateJSONSchemas: ICodegenFunc<ICodegenGraph> = async () => {
  const schemaCollection = await getSchemaCollection();
  const root = "lib";
  const nodes: ICodegenNode[] = [];

  for await (const { localPath, schema } of Object.values(schemaCollection)) {
    // exclude media schemas as they cause
    // call stack exceded error
    if (schema.$id.startsWith("https://jtsc-schemas.org/media")) {
      continue;
    }

    try {
      // @TODO a better way to detect and match multi-extensions
      const nodePath = localPath.replace(schemaFileEnd, "");
      const refs = collectRefs(schema);
      const schemaTypes = await createSchemaTypes(schema, {
        refs,
        root,
        schemaCollection,
      });
      const schemaModule = await createSchemaModule(schema);
      const validatorModule = await createValidatorModule(schema);
      const libModule = await createLibModule(schema);

      const nodeModules = {
        schema: schemaModule,
        lib: libModule,
        validate: validatorModule,
        types: schemaTypes,
      };

      const node: ICodegenNode = {
        nodePath,
        modules: nodeModules,
      };

      nodes.push(node);
    } catch (error) {
      if (!isNativeError(error)) {
        throw error;
      }

      throw new CodegenError(
        `Failed to run codegen for schema "${schema.$id}"`,
        {
          cause: error,
        }
      );
    }
  }

  const codegen: ICodegenGraph = {
    root,
    type: "graph",
    nodes,
  };

  return codegen;
};

interface ICreateSchemaTypesMeta {
  refs: IRefSet;
  schemaCollection: IJSONSchemaCollection;
  root: string;
}

async function createSchemaTypes(
  schema: IJSONSchema,
  { refs, root, schemaCollection }: ICreateSchemaTypesMeta
): Promise<IModule> {
  const typeExports: IExport[] = [];
  const imports: string[] = [];

  const interfaceCode = await createInterface(schema, {
    imports,
    refs,
    root,
    schemaCollection,
    typeExports,
  });

  const typesModule: IModule = {
    exports: typeExports,
    result: multilineString(...imports, interfaceCode),
  };
  return typesModule;
}

interface ICreateSchemaInterfaceMeta {
  refs: IRefSet;
  imports: string[];
  typeExports: IExport[];
  schemaCollection: IJSONSchemaCollection;
  root: string;
}

async function createInterface(
  schema: IJSONSchema,
  {
    imports,
    refs,
    typeExports,
    schemaCollection,
    root,
  }: ICreateSchemaInterfaceMeta
): Promise<string> {
  const schemaInterface = await createInterfaceFromSchema(schema, {
    declareExternallyReferenced: false,
  });
  const { code, inputSchema } = schemaInterface;
  const importStatements = Array.from(refs).map((ref) => {
    const { localPath, schema } = schemaCollection[ref];
    const symbolName = `I${schema.title}`;
    const normalizedPath = (
      IS_WINDOWS ? localPath.split(path.sep).join(path.posix.sep) : localPath
    ).replace(schemaFileEnd, "");
    const modulePath = `#codegen/schema/${root}/${normalizedPath}`;
    const importStatement = `import { type ${symbolName} } from "${modulePath}"`;

    return importStatement;
  });

  typeExports.push({ symbolName: inputSchema.title, isAbstract: true });
  imports.push(...importStatements, "\n");

  return code;
}

async function createSchemaModule(schema: IJSONSchema): Promise<IModule> {
  const concreteExports: IExport[] = [];
  const inlineSchemaCode = await createInlineSchema(schema, {
    exports: concreteExports,
  });
  const result = inlineSchemaCode;
  const libModule: IModule = {
    exports: concreteExports,
    result,
  };
  return libModule;
}

interface ICreateInlineSchemaMeta {
  exports: IExport[];
}

async function createInlineSchema(
  schema: IJSONSchema,
  { exports }: ICreateInlineSchemaMeta
) {
  const schemaName = `${decapitalizeString(schema.title!)}Schema`;
  const schemaObj = multilineString(
    `export const ${schemaName} = ${stringifyObject(schema)} as const`
  );

  // add schema name to the index exports
  exports.push({ symbolName: schemaName });

  return schemaObj;
}

async function createValidatorModule(schema: IJSONSchema): Promise<IModule> {
  const ajv = await createCodegenAJV(schema.$id);
  const validationCode = await generateValidationCode(schema, ajv);
  const result = multilineString("// @ts-nocheck", validationCode);
  const moduleData: IModule = {
    result,
    exports: [],
  };

  return moduleData;
}

async function createLibModule(schema: IJSONSchema): Promise<IModule> {
  const interfaceName = `I${schema.title}`;
  const schemaName = `${decapitalizeString(schema.title!)}Schema`;
  const funcName = `validate${schema.title}`;
  const exports: IExport[] = [{ symbolName: funcName }];
  const imports: string[] = [
    `import { type ValidateFunction } from "ajv/dist/2020"`,
    `import { createValidator, type IJSONSchema } from "#lib/json-schema"`,
    `import { ${schemaName} } from "./schema"`,
    `import { validate } from "./validate"`,
    `import { ${interfaceName} } from "./types"`,
  ];
  const validatorFunction = `createValidator<${interfaceName}>(validate as ValidateFunction<${interfaceName}>, ${schemaName} as unknown as IJSONSchema)`;
  const result = multilineString(
    ...imports,
    `export const ${funcName} = ${validatorFunction};`
  );
  const moduleData: IModule = {
    result,
    exports,
  };

  return moduleData;
}

export default generateJSONSchemas;
