import { ProjectError } from "#lib/errors";

export class StoreError extends ProjectError {}
