import Cookies from "js-cookie";
import type { CookieAttributes } from "js-cookie";
import { StoreError } from "./errors";

export const COOKIE = {
  THEME: "theme",
  LANGUAGE: "lang",
} as const;

/**
 * Only includes client-accessible cookies.
 */
type ICookieKey = (typeof COOKIE)[keyof typeof COOKIE];

const defaultOptions: CookieAttributes = {
  expires: 365,
  secure: false,
  sameSite: "Lax",
  // sameSite: "strict",
};

/**
 * @param name The name of the cookie.
 * @returns The value of the cookie.
 */
export function getCookie(name: ICookieKey) {
  const value = Cookies.get(name);

  return value;
}

/**
 * @param name The name of the cookie.
 * @param value The value of the cookie.
 * @param options Cookie options.
 */
export function setCookie(
  name: ICookieKey,
  value: string,
  options: Partial<CookieAttributes> = defaultOptions,
) {
  const finalOptions = options
    ? { ...defaultOptions, ...options }
    : defaultOptions;
  const result = Cookies.set(name, value, finalOptions);

  if (!result) {
    throw new StoreError(`Failed to set the value for the cookie "${name}".`);
  }
}
