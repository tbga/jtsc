export async function copyToClipBoard(text: string) {
  await navigator.clipboard.writeText(text);
}
