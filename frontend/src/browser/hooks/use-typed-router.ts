import { useEffect, useState } from "react";
import { type NextRouter, useRouter } from "next/router";
import { type IQueryTypes, IQueryValue, resolveQuery } from "#lib/util";
import { EMPTY_OBJECT } from "#types";

export interface ITypedRouter<Keys extends string | number> extends NextRouter {
  query: Record<Keys, IQueryValue>;
}

/**
 * @TODO better type inference
 * @TODO make it work at all
 * @param types A dictionary of query keys
 * and the resolution types.
 */
export function useTypedRouter<QueryKeys extends string>(
  types: IQueryTypes<QueryKeys>,
): ITypedRouter<QueryKeys> {
  const router = useRouter();
  const [resolvedQuery, changeResolvedQuery] =
    useState<ReturnType<typeof resolveQuery>>(EMPTY_OBJECT);
  const { query, isReady } = router;

  useEffect(() => {
    if (!isReady) {
      return;
    }

    const newResolvedQuery = resolveQuery(query, types);
    changeResolvedQuery(newResolvedQuery);
  }, [query, isReady, types]);

  return { ...router, query: resolvedQuery };
}
