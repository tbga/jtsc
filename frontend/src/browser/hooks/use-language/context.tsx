import {
  createContext,
  type ReactNode,
  useEffect,
  useState,
  useCallback,
} from "react";
import { useRouter } from "next/router";
import { DEFAULT_LANGUAGE, SUPPORTED_LANGUAGES } from "#environment/variables";
import { resolveQueryValue } from "#lib/util";
import { ProjectError } from "#lib/errors";
import { EMPTY_ASYNC_FUNCTION } from "#types";
import { type ILanguageContext } from "./types";

const defaultContext: ILanguageContext = {
  isLoading: true,
  language: "",
  changeLanguage: EMPTY_ASYNC_FUNCTION,
};

export const LanguageContext = createContext<ILanguageContext>(defaultContext);

export function LanguageProvider({ children }: { children: ReactNode }) {
  const router = useRouter();
  const [isLoading, switchLoading] = useState(true);
  const [lang, changeLang] = useState<string>();
  const { isReady: isRouterReady, query } = router;
  const langQuery = resolveQueryValue(query.lang);
  const isContextLoading = !isRouterReady || isLoading;

  const changeLanguage = useCallback(
    async (language: string) => {
      try {
        switchLoading(true);
        if (!SUPPORTED_LANGUAGES.includes(language)) {
          throw new ProjectError(`Unsupported language "${language}"`);
        }

        if (language === lang) {
          return;
        }

        changeLang(language);
      } finally {
        switchLoading(false);
      }
    },
    [lang],
  );

  useEffect(() => {
    if (!isRouterReady) {
      return;
    }

    (async () => {
      try {
        switchLoading(true);

        const newLang =
          !langQuery || !SUPPORTED_LANGUAGES.includes(langQuery)
            ? DEFAULT_LANGUAGE
            : langQuery;
        await changeLanguage(newLang);
      } catch (error) {
        console.error(error);
      } finally {
        switchLoading(false);
      }
    })();
  }, [isRouterReady, langQuery, changeLanguage]);

  return (
    <LanguageContext.Provider
      value={
        {
          isLoading: isContextLoading,
          language: lang,
          changeLanguage,
        } as ILanguageContext
      }
    >
      {children}
    </LanguageContext.Provider>
  );
}
