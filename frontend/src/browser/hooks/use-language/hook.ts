import { useContext } from "react";
import { LanguageContext } from "./context";

export function useLanguage() {
  const ctx = useContext(LanguageContext);

  return ctx;
}
