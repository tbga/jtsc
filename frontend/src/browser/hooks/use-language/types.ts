export interface ILanguageContext {
  isLoading: boolean;
  language: string;
  changeLanguage: (language: string) => Promise<void>;
}
