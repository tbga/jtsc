import { useEffect, useRef } from "react";

/**
 * Stolen from
 * [https://overreacted.io/making-setinterval-declarative-with-react-hooks](https://overreacted.io/making-setinterval-declarative-with-react-hooks).
 */
export function useInterval<FuncType extends () => void>(
  func: FuncType,
  delay?: number,
) {
  const savedFunc = useRef<typeof func>();

  useEffect(() => {
    savedFunc.current = func;
  }, [func]);

  useEffect(() => {
    if (!delay) {
      return;
    }

    const id = setInterval(tick, delay);

    function tick() {
      savedFunc?.current?.();
    }

    return () => clearInterval(id);
  }, [delay]);
}
