import { createContext, useState, type ReactNode } from "react";
import { getSystem } from "#lib/local-file-system";
import { EMPTY_FUNCTION } from "#types";
import { type ILocalFileSystemContext } from "./types";

const defaultContext: ILocalFileSystemContext = {
  errors: [],
  changeSystemInfo: EMPTY_FUNCTION,
  changeCurrentFolder: EMPTY_FUNCTION,
  changeErrors: EMPTY_FUNCTION,
};

export const LocalFileSystemContext =
  createContext<ILocalFileSystemContext>(defaultContext);

export function LocalFileSystemProvider({ children }: { children: ReactNode }) {
  const [systemInfo, changeSystemInfo] =
    useState<Awaited<ReturnType<typeof getSystem>>>();
  const [currentFolder, changeCurrentFolder] =
    useState<ILocalFileSystemContext["currentFolder"]>();
  const [errors, changeErrors] = useState<ILocalFileSystemContext["errors"]>(
    [],
  );

  return (
    <LocalFileSystemContext.Provider
      value={{
        systemInfo,
        changeSystemInfo,
        currentFolder,
        changeCurrentFolder,
        errors,
        changeErrors,
      }}
    >
      {children}
    </LocalFileSystemContext.Provider>
  );
}
