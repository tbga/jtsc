import { isError } from "#lib/errors";
import { getPath, getSystem, LocalPath } from "#lib/local-file-system";
import { useCallback, useContext, useEffect } from "react";
import { LocalFileSystemContext } from "./context";
import { type IUseLocalFileSystem } from "./types";

export function useLocalFileSystem(): IUseLocalFileSystem {
  const {
    currentFolder,
    systemInfo,
    errors,
    changeCurrentFolder,
    changeErrors,
    changeSystemInfo,
  } = useContext(LocalFileSystemContext);

  const getNextFolder = useCallback(
    async (localPath: LocalPath) => {
      try {
        const path = localPath.toString();
        const folder = await getPath(path, "folder");
        const folderInfo = { ...folder, path };
        changeCurrentFolder(folderInfo);
        changeErrors([]);
      } catch (error) {
        if (!isError(error)) {
          throw error;
        }

        changeErrors([error]);
      }
    },
    [changeCurrentFolder, changeErrors],
  );
  const isReady = Boolean(systemInfo && currentFolder);

  useEffect(() => {
    (async () => {
      try {
        const newSystemInfo = await getSystem();
        changeSystemInfo(newSystemInfo);
        const newLocalPath = new LocalPath(newSystemInfo.home_dir);
        await getNextFolder(newLocalPath);
      } catch (error) {
        if (!isError(error)) {
          throw error;
        }

        changeErrors([error]);
      }
    })();
  }, [changeSystemInfo, getNextFolder, changeErrors]);

  return {
    // @ts-expect-error ensured by `isReady`
    systemInfo,
    // @ts-expect-error ensured by `isReady`
    currentFolder,
    getNextFolder,
    errors,
    isReady,
  };
}
