import { type Dispatch, type SetStateAction } from "react";
import {
  type IFolder,
  type IFileSystem,
  type IPath,
  LocalPath,
} from "#lib/local-file-system";

export interface IPathInfo extends IFolder {
  path: IPath;
}

export interface ILocalFileSystemContext {
  currentFolder?: IPathInfo;
  systemInfo?: IFileSystem;
  errors: Error[];
  changeSystemInfo: Dispatch<SetStateAction<IFileSystem | undefined>>;
  changeCurrentFolder: Dispatch<SetStateAction<IPathInfo | undefined>>;
  changeErrors: Dispatch<SetStateAction<Error[]>>;
}

export interface IUseLocalFileSystem {
  currentFolder: IPathInfo;
  systemInfo: IFileSystem;
  errors: Error[];
  isReady: boolean;
  getNextFolder: (localPath: LocalPath) => Promise<void>;
}
