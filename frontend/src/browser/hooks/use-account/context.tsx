import { createContext, ReactNode, useEffect, useState } from "react";
import {
  getAccount,
  loginAccount,
  logoutAccount,
  registerAccount,
} from "#lib/authentication";
import { EMPTY_ASYNC_FUNCTION, type IAsyncReturnType } from "#types";
import { type IAccountContext } from "./types";

const defaultContext: IAccountContext = {
  isReady: false,
  isLoading: true,
  register: EMPTY_ASYNC_FUNCTION,
  login: EMPTY_ASYNC_FUNCTION,
  logout: EMPTY_ASYNC_FUNCTION,
};

export const AccountContext = createContext<IAccountContext>(defaultContext);

export function AccountProvider({ children }: { children: ReactNode }) {
  const [isReady, switchReady] = useState(false);
  const [isLoading, switchLoading] = useState(true);
  const [account, changeAccount] =
    useState<IAsyncReturnType<typeof getAccount>>();

  useEffect(() => {
    (async () => {
      try {
        const newAccount = await getAccount();

        if (!newAccount) {
          return;
        }

        changeAccount(newAccount);
      } catch (error) {
        console.error(error);
      } finally {
        switchReady(true);
        switchLoading(false);
      }
    })();
  }, []);

  async function register(...args: Parameters<typeof registerAccount>) {
    if (isLoading) {
      return;
    }

    switchLoading(true);
    const newAccount = await registerAccount(...args);
    changeAccount(newAccount);
    switchLoading(false);
  }

  async function login(...args: Parameters<typeof loginAccount>) {
    if (isLoading) {
      return;
    }

    switchLoading(true);
    const newAccount = await loginAccount(...args);
    changeAccount(newAccount);
    switchLoading(false);
  }

  async function logout() {
    if (isLoading) {
      return;
    }

    switchLoading(true);
    await logoutAccount();
    changeAccount(undefined);
    switchLoading(false);
  }

  return (
    <AccountContext.Provider
      value={{ isReady, isLoading, account, register, login, logout }}
    >
      {children}
    </AccountContext.Provider>
  );
}
