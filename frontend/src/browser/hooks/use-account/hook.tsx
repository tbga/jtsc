import { useContext } from "react";
import { AccountContext } from "./context";
import { type IUseAccount } from "./types";

export function useAccount(): IUseAccount {
  const { isReady, account, ...ctx } = useContext(AccountContext);

  const isRegistered = Boolean(isReady && account);
  const isAdmin = account?.role === "administrator";

  return {
    ...ctx,
    isReady,
    // @ts-expect-error `isRegistered` checks for its existence
    account,
    isRegistered,
    isAdmin,
  };
}
