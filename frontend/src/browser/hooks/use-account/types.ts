import {
  type IAccount,
  type IAccountInit,
  type IAccountLogin,
} from "#entities/account";

export interface IAccountContext {
  isReady: boolean;
  isLoading: boolean;
  account?: IAccount;
  register: (init: IAccountInit) => Promise<void>;
  login: (login: IAccountLogin) => Promise<void>;
  logout: () => Promise<void>;
}

export interface IUseAccount extends IAccountContext {
  isRegistered: boolean;
  account: IAccount;
  isAdmin: boolean;
}
