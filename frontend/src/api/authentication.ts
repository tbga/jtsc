import { apiFetch, APIURL } from "#lib/api";
import {
  type IAccountLogin,
  type IAccount,
  type IAccountInit,
} from "#entities/account";

export async function fetchRegisterAccount(init: IAccountInit) {
  const apiBody = await apiFetch<IAccount>({
    url: new APIURL("/authentication/register"),
    method: "POST",
    body: init,
  });

  return apiBody.data;
}

export async function fetchLoginAccount(login: IAccountLogin) {
  const apiBody = await apiFetch<IAccount>({
    url: new APIURL("/authentication/login"),
    method: "POST",
    body: login,
  });

  return apiBody.data;
}

export async function fetchLogoutAccount() {
  const apiBody = await apiFetch<null>({
    url: new APIURL("/authentication/logout"),
    method: "POST",
    body: null,
  });

  return apiBody.data;
}
