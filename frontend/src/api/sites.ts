import { apiFetch, APIURL } from "#lib/api";
import { type IBigSerialInteger } from "#types";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type ISite, type ISitePreview } from "#entities/site";
import { type IProfilePreview } from "#entities/profile";
import { type IReleasePreview } from "#entities/release";

export async function fetchSitesPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/sites"),
    method: "GET",
  });

  return apiBody.data;
}

export interface ISitesData {
  pagination: IPagination;
  sites: ISitePreview[];
}

export async function fetchSites(page: IBigSerialInteger) {
  const apiBody = await apiFetch<ISitesData>({
    url: new APIURL(`/sites/${page}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSite(site_id: ISite["id"]) {
  const apiBody = await apiFetch<ISite>({
    url: new APIURL(`/site/${site_id}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteProfilesPagination(site_id: IBigSerialInteger) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/site/${site_id}/profiles`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteProfiles(
  site_id: IBigSerialInteger,
  page: IBigSerialInteger,
) {
  const apiBody = await apiFetch<
    IPaginatedData<{ profiles: IProfilePreview[] }>
  >({
    url: new APIURL(`/site/${site_id}/profiles/${page}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteReleasesPagination(site_id: IBigSerialInteger) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/site/${site_id}/releases`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteReleases(
  site_id: IBigSerialInteger,
  page: IBigSerialInteger,
) {
  const apiBody = await apiFetch<
    IPaginatedData<{ releases: IReleasePreview[] }>
  >({
    url: new APIURL(`/site/${site_id}/releases/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
