import { apiFetch, APIURL } from "#lib/api";
import { type IPagination } from "#lib/pagination";
import {
  type IArtistPreview,
  type IArtistUpdate,
  type IArtist,
} from "#entities/artist";
import { type IProfilePreview } from "#entities/profile";
import { type IBigSerialInteger, type IBigIntegerPositive } from "#types";

export interface IArtistsData {
  pagination: IPagination;
  artists: IArtistPreview[];
}

export async function fetchArtistsPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/artists"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchArtists(currentPage: IBigIntegerPositive) {
  const apiBody = await apiFetch<IArtistsData>({
    url: new APIURL(`/artists/${currentPage}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchArtist(artistID: IArtist["id"]) {
  const apiBody = await apiFetch<IArtist>({
    url: new APIURL(`/artist/${artistID}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchArtistProfilesPagination(
  artistID: IBigSerialInteger,
) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/artist/${artistID}/profiles`),
    method: "GET",
  });

  return apiBody.data;
}

export interface IArtistProfilesData {
  pagination: IPagination;
  profiles: IProfilePreview[];
}

export async function fetchArtistProfiles(
  artistID: IBigSerialInteger,
  page: IBigSerialInteger,
) {
  const apiBody = await apiFetch<IArtistProfilesData>({
    url: new APIURL(`/artist/${artistID}/profiles/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
