import { apiFetch, AdministratorAPIURL } from "#lib/api";
import { type ISiteInit, type ISite, type ISiteUpdate } from "#entities/site";

export async function fetchNewSite(siteInit: ISiteInit) {
  const apiBody = await apiFetch<ISite>({
    url: new AdministratorAPIURL("/site"),
    method: "PUT",
    body: siteInit,
  });

  return apiBody.data;
}

export async function fetchSiteUpdate(
  site_id: ISite["id"],
  update: ISiteUpdate,
) {
  const apiBody = await apiFetch<ISite>({
    url: new AdministratorAPIURL(`/site/${site_id}`),
    method: "PATCH",
    body: update,
  });

  return apiBody.data;
}
