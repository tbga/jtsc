import { apiFetch, AdministratorAPIURL } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import {
  type IRelease,
  type IReleaseInit,
  type IReleaseUpdate,
} from "#entities/release";
import { type IOperation } from "#entities/operation";
import { type IPost } from "#entities/post";
import { type IProfile } from "#entities/profile";

export async function fetchCreateRelease(init: IReleaseInit) {
  const apiBody = await apiFetch<IEntityItem>({
    url: new AdministratorAPIURL("/release"),
    method: "PUT",
    body: init,
  });

  return apiBody.data;
}

export async function fetchAddReleases(inits: IReleaseInit[]) {
  const apiBody = await apiFetch<IOperation>({
    url: new AdministratorAPIURL("/releases"),
    method: "PUT",
    body: inits,
  });

  return apiBody.data;
}

export async function fetchEditRelease(
  release_id: IRelease["id"],
  update: IReleaseUpdate,
) {
  const apiBody = await apiFetch<IRelease>({
    url: new AdministratorAPIURL(`/release/${release_id}`),
    method: "PATCH",
    body: update,
  });

  return apiBody.data;
}

export async function fetchCreateReleasePosts(
  releaseID: IRelease["id"],
  postIDs: IPost["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/release/${releaseID}/posts`),
    method: "PUT",
    body: postIDs,
  });

  return apiBody.data;
}

export async function fetchRemoveReleasePosts(
  releaseID: IRelease["id"],
  postIDs: IPost["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/release/${releaseID}/posts`),
    method: "DELETE",
    body: postIDs,
  });

  return apiBody.data;
}

export async function fetchReleaseProfilesCreate(
  releaseID: IRelease["id"],
  profileIDs: IProfile["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/release/${releaseID}/profiles`),
    method: "PUT",
    body: profileIDs,
  });

  return apiBody.data;
}

export async function fetchReleaseProfilesRemove(
  releaseID: IRelease["id"],
  profileIDs: IProfile["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/release/${releaseID}/profiles`),
    method: "DELETE",
    body: profileIDs,
  });

  return apiBody.data;
}
