import { apiFetch, AdministratorAPIURL } from "#lib/api";
import {
  type IEntry,
  type IFolder,
  type IPath,
  type IFileSystem,
  type IFile,
} from "#lib/local-file-system";

export async function fetchSystem() {
  const apiBody = await apiFetch<IFileSystem>({
    url: new AdministratorAPIURL("/file-system"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchEntry(path: IPath) {
  const url = new AdministratorAPIURL("/file-system/entry");

  url.searchParams.set("path", path);

  const apiBody = await apiFetch<IEntry>({
    url,
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchFolder(path: IPath) {
  const url = new AdministratorAPIURL("/file-system/folder");

  url.searchParams.set("path", path);

  const apiBody = await apiFetch<IFolder>({
    url,
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchFile(path: IPath) {
  const url = new AdministratorAPIURL("/file-system/file");

  url.searchParams.set("path", path);

  const apiBody = await apiFetch<IFile>({
    url,
    method: "GET",
  });

  return apiBody.data;
}
