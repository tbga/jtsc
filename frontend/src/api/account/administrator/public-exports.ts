import { AdministratorAPIURL, apiFetch } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import {
  type IPublicExportPreview,
  type IPublicExportInit,
  type IPublicExport,
  type IPublicExportUpdate,
} from "#entities/public-export";
import { type ISite, type ISitePreview } from "#entities/site";
import { type IBigSerialInteger } from "#types";

export async function fetchPublicExportCreate(init: IPublicExportInit) {
  const resBody = await apiFetch<IEntityItem>({
    url: new AdministratorAPIURL("/public-export"),
    method: "PUT",
    body: init,
  });

  return resBody.data;
}

export async function fetchPublicExport(id: IPublicExport["id"]) {
  const resBody = await apiFetch<IPublicExport>({
    url: new AdministratorAPIURL(`/public-export/${id}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicExportUpdate(
  id: IPublicExport["id"],
  update: IPublicExportUpdate,
) {
  const resBody = await apiFetch<IPublicExport>({
    url: new AdministratorAPIURL(`/public-export/${id}`),
    method: "PATCH",
    body: update,
  });

  return resBody.data;
}

export async function fetchPublicExportFinalize(id: IPublicExport["id"]) {
  const resBody = await apiFetch<IPublicExport>({
    url: new AdministratorAPIURL(`/public-export/${id}/finalize`),
    method: "POST",
    body: null,
  });

  return resBody.data;
}

export async function fetchPublicExportFinish(id: IPublicExport["id"]) {
  const resBody = await apiFetch<IEntityItem>({
    url: new AdministratorAPIURL(`/public-export/${id}/finish`),
    method: "POST",
    body: null,
  });

  return resBody.data;
}

export async function fetchPublicExportSitesCreate(
  id: IPublicExport["id"],
  site_ids: ISite["id"][],
) {
  const resBody = await apiFetch<ISitePreview[]>({
    url: new AdministratorAPIURL(`/public-export/${id}/sites`),
    method: "PUT",
    body: site_ids,
  });

  return resBody.data;
}

export async function fetchPublicExportSitesPagination(
  id: IPublicExport["id"],
) {
  const resBody = await apiFetch<IPagination>({
    url: new AdministratorAPIURL(`/public-export/${id}/sites`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicExportSites(
  id: IPublicExport["id"],
  page: IBigSerialInteger,
) {
  const resBody = await apiFetch<IPaginatedData<{ sites: ISitePreview[] }>>({
    url: new AdministratorAPIURL(`/public-export/${id}/sites/${page}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicExportSitesRemove(
  id: IPublicExport["id"],
  site_ids: ISite["id"][],
) {
  const resBody = await apiFetch<ISite["id"][]>({
    url: new AdministratorAPIURL(`/public-export/${id}/sites`),
    method: "DELETE",
    body: site_ids,
  });

  return resBody.data;
}

export async function fetchPublicExportsPagination() {
  const resBody = await apiFetch<IPagination>({
    url: new AdministratorAPIURL("/public-exports"),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicExports(page: IBigSerialInteger) {
  const resBody = await apiFetch<
    IPaginatedData<{ public_exports: IPublicExportPreview[] }>
  >({
    url: new AdministratorAPIURL(`/public-exports/${page}`),
    method: "GET",
  });

  return resBody.data;
}
