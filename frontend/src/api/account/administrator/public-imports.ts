import {
  publicImportSiteCategoryAPISchema,
  type IPublicImportSiteCategoryAPI,
} from "#codegen/schema/lib/entities/public-import/site/category-api";
import { type IAPIRequestPublicImportSiteUpdate } from "#codegen/schema/lib/api/account/role/administrator/public-import/site/update/request";
import { AdministratorAPIURL, apiFetch } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type IPath } from "#lib/local-file-system";
import { type IBigSerialInteger } from "#types";
import {
  type IPublicImportPreview,
  type IPublicImport,
  type IPublicImportSitePreview,
  type IPublicImportSite,
  type IPublicImportStats,
} from "#entities/public-import";

export type { IPublicImportSiteCategoryAPI } from "#codegen/schema/lib/entities/public-import/site/category-api";
export const publicImportSiteCategoriesAPI =
  publicImportSiteCategoryAPISchema.anyOf.map((value) => value.const);

/**
 * @returns An entity item for public import creation operation.
 */
export async function fetchPublicImportCreate(fileURL: IPath) {
  const resBody = await apiFetch<IEntityItem>({
    url: new AdministratorAPIURL("/public-import"),
    method: "PUT",
    body: fileURL,
  });

  return resBody.data;
}

export async function fetchPublicImport(id: IPublicImport["id"]) {
  const resBody = await apiFetch<IPublicImport>({
    url: new AdministratorAPIURL(`/public-import/${id}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicImportStats(id: IPublicImport["id"]) {
  const resBody = await apiFetch<IPublicImportStats>({
    url: new AdministratorAPIURL(`/public-import/${id}/stats`),
    method: "GET",
  });

  return resBody.data;
}

/**
 * @returns An entity item for public import consumption operation.
 */
export async function fetchPublicImportConsume(id: IPublicImport["id"]) {
  const resBody = await apiFetch<IEntityItem>({
    url: new AdministratorAPIURL(`/public-import/${id}/consume`),
    method: "POST",
    body: null,
  });

  return resBody.data;
}

export async function fetchPublicImportSite(
  public_import_id: IPublicImport["id"],
  site_id: IPublicImportSite["id"],
) {
  const url = new AdministratorAPIURL(
    `/public-import/${public_import_id}/site/${site_id}`,
  );

  const resBody = await apiFetch<IPublicImportSite>({
    url,
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicImportSiteApprove(
  public_import_id: IPublicImport["id"],
  site_id: IPublicImportSite["id"],
) {
  const body: IAPIRequestPublicImportSiteUpdate["data"] = {
    is_approved: true,
  };
  const url = new AdministratorAPIURL(
    `/public-import/${public_import_id}/site/${site_id}`,
  );

  const resBody = await apiFetch<IPublicImportSite>({
    url,
    method: "PATCH",
    body,
  });

  return resBody.data;
}

export async function fetchPublicImportSiteReject(
  public_import_id: IPublicImport["id"],
  site_id: IPublicImportSite["id"],
) {
  const body: IAPIRequestPublicImportSiteUpdate["data"] = {
    is_approved: false,
  };
  const url = new AdministratorAPIURL(
    `/public-import/${public_import_id}/site/${site_id}`,
  );

  const resBody = await apiFetch<IPublicImportSite>({
    url,
    method: "PATCH",
    body,
  });

  return resBody.data;
}

export async function fetchPublicImportSitesPagination(
  id: IPublicImport["id"],
  category: IPublicImportSiteCategoryAPI,
) {
  const resBody = await apiFetch<IPagination>({
    url: new AdministratorAPIURL(`/public-import/${id}/sites/${category}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicImportSites(
  id: IPublicImport["id"],
  category: IPublicImportSiteCategoryAPI,
  page: IBigSerialInteger,
) {
  const resBody = await apiFetch<
    IPaginatedData<{ sites: IPublicImportSitePreview[] }>
  >({
    url: new AdministratorAPIURL(
      `/public-import/${id}/sites/${category}/${page}`,
    ),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicImportsPagination() {
  const resBody = await apiFetch<IPagination>({
    url: new AdministratorAPIURL("/public-imports"),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchPublicImports(page: IBigSerialInteger) {
  const resBody = await apiFetch<
    IPaginatedData<{ public_imports: IPublicImportPreview[] }>
  >({
    url: new AdministratorAPIURL(`/public-imports/${page}`),
    method: "GET",
  });

  return resBody.data;
}
