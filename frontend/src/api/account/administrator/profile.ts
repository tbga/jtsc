import { apiFetch, AdministratorAPIURL } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import {
  type IProfile,
  type IProfileInit,
  type IProfileUpdate,
} from "#entities/profile";
import { type INameInit, type IName } from "#entities/name";
import { type IReleaseInit, type IRelease } from "#entities/release";
import { type IArtist } from "#entities/artist";
import { type IBigSerialInteger } from "#types";

export async function fetchProfileCreate(profileInit: IProfileInit) {
  const resBody = await apiFetch<IProfile>({
    url: new AdministratorAPIURL("/profile"),
    method: "PUT",
    body: profileInit,
  });

  return resBody.data;
}

export async function fetchUpdateProfile(
  profile_id: IProfile["id"],
  profileUpdate: IProfileUpdate,
) {
  const resBody = await apiFetch<IProfile>({
    url: new AdministratorAPIURL(`/profile/${profile_id}`),
    method: "PATCH",
    body: profileUpdate,
  });

  return resBody.data;
}

export async function fetchProfileNamesCreate(
  profileID: IBigSerialInteger,
  inits: INameInit[],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/names`),
    method: "PUT",
    body: inits,
  });

  return resBody.data;
}

export async function fetchProfileNamesRemove(
  profileID: IBigSerialInteger,
  ids: IName["id"][],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/names`),
    method: "DELETE",
    body: ids,
  });

  return resBody.data;
}

export async function fetchAddProfileReleases(
  profileID: IProfile["id"],
  release_inits: IReleaseInit[],
) {
  const resBody = await apiFetch<IReleaseInit[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/releases`),
    method: "PUT",
    body: release_inits,
  });

  return resBody.data;
}

export async function fetchCreateProfileReleases(
  profileID: IProfile["id"],
  releaseIDs: IRelease["id"][],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/releases`),
    method: "PUT",
    body: releaseIDs,
  });

  return resBody.data;
}

export async function fetchRemoveProfileReleases(
  profileID: IProfile["id"],
  releaseIDs: IRelease["id"][],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/releases`),
    method: "DELETE",
    body: releaseIDs,
  });

  return resBody.data;
}

export async function fetchCreateProfileArtists(
  profileID: IProfile["id"],
  artistIDs: IArtist["id"][],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/artists`),
    method: "PUT",
    body: artistIDs,
  });

  return resBody.data;
}

export async function fetchRemoveProfileArtists(
  profileID: IProfile["id"],
  artistIDs: IArtist["id"][],
) {
  const resBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/profile/${profileID}/artists`),
    method: "DELETE",
    body: artistIDs,
  });

  return resBody.data;
}
