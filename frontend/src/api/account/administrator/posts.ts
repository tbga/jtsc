import { apiFetch, AdministratorAPIURL } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import { type IPost } from "#entities/post";
import { type IArtist } from "#entities/artist";
import {
  type IRelease,
  type IReleaseInit,
  type IReleasePreview,
} from "#entities/release";

export async function fetchPostArtistsCreate(
  post_id: IPost["id"],
  artist_ids: IArtist["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/post/${post_id}/artists`),
    method: "PUT",
    body: artist_ids,
  });

  return apiBody.data;
}

export async function fetchPostArtistsRemove(
  post_id: IPost["id"],
  artist_ids: IArtist["id"][],
) {
  const apiBody = await apiFetch<IEntityItem[]>({
    url: new AdministratorAPIURL(`/post/${post_id}/artists`),
    method: "DELETE",
    body: artist_ids,
  });

  return apiBody.data;
}

export async function fetchPostReleasesCreate(
  post_id: IPost["id"],
  data: {
    ids?: IRelease["id"][];
    inits?: IReleaseInit[];
  },
) {
  const apiBody = await apiFetch<IReleasePreview[]>({
    url: new AdministratorAPIURL(`/post/${post_id}/releases`),
    method: "PUT",
    body: data,
  });

  return apiBody.data;
}

export async function fetchPostReleasesRemove(
  post_id: IPost["id"],
  release_ids: IRelease["id"][],
) {
  const apiBody = await apiFetch<IReleasePreview[]>({
    url: new AdministratorAPIURL(`/post/${post_id}/releases`),
    method: "DELETE",
    body: release_ids,
  });

  return apiBody.data;
}
