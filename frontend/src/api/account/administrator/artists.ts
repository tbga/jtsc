import { apiFetch, AdministratorAPIURL } from "#lib/api";
import { type IArtistUpdate, type IArtist } from "#entities/artist";

export async function fetchArtistEdit(
  artist_id: IArtist["id"],
  artist_update: IArtistUpdate,
) {
  const apiBody = await apiFetch<IArtist>({
    url: new AdministratorAPIURL(`/artist/${artist_id}`),
    method: "PATCH",
    body: artist_update,
  });

  return apiBody.data;
}
