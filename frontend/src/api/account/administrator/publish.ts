import { apiFetch, APIURL } from "#lib/api";
import { type ISite } from "#entities/site";

export async function fetchPublishSite(id: ISite["id"]) {
  const apiBody = await apiFetch<ISite>({
    url: new APIURL(`/account/role/administrator/publish/site/${id}`),
    method: "POST",
    body: null,
  });

  return apiBody.data;
}
