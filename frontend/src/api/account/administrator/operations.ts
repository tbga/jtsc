import { type IAPIResponseOperationsStats } from "#codegen/schema/lib/api/account/role/administrator/operations/stats/response";
import { apiFetch, APIURL } from "#lib/api";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type IOperation, type IOperationPreview } from "#entities/operation";
import { type IBigIntegerPositive } from "#types";

export async function fetchOperationsStats() {
  const apiBody = await apiFetch<IAPIResponseOperationsStats["data"]>({
    url: new APIURL("/account/role/administrator/operations/stats"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchOperationsPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/account/role/administrator/operations/all"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchOperations(page: IBigIntegerPositive) {
  const apiBody = await apiFetch<
    IPaginatedData<{ operations: IOperationPreview[] }>
  >({
    url: new APIURL(`/account/role/administrator/operations/all/${page}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchOperation(id: IOperation["id"]) {
  const apiBody = await apiFetch<IOperation>({
    url: new APIURL(`/account/role/administrator/operation/${id}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function retryOperation(id: IOperation["id"]) {
  const apiBody = await apiFetch<IOperation>({
    url: new APIURL(`/account/role/administrator/operation/${id}/retry`),
    method: "POST",
    body: null,
  });

  return apiBody.data;
}

export async function fetchCancelOperation(id: IOperation["id"]) {
  const apiBody = await apiFetch<IOperation["id"]>({
    url: new APIURL(`/account/role/administrator/operation/${id}/cancel`),
    method: "POST",
    body: null,
  });

  return apiBody.data;
}
