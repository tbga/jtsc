import { apiFetch, SearchAPIURL } from "#lib/api";
import { type IEntityItem } from "#lib/entities";
import { type IArtistPreview } from "#entities/artist";
import { type IPaginatedData, IPagination } from "#lib/pagination";
import { type IBigSerialInteger, type IBigIntegerPositive } from "#types";
import { type IProfilePreview } from "#entities/profile";
import { ISite, type ISitePreview } from "#entities/site";
import { type IReleasePreview } from "#entities/release";
import { type IPostPreview } from "#entities/post";

export async function fetchNamesSearchPagination(
  query: string,
): Promise<IPagination> {
  const searchParams = new URLSearchParams([["query", query]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/names/items", searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function searchNameItems(query: string, page: IBigSerialInteger) {
  const searchParams = new URLSearchParams([["query", query]]);

  const apiBody = await apiFetch<IPaginatedData<{ names: IEntityItem[] }>>({
    url: new SearchAPIURL(`/names/items/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchArtistSearchPagination(searchQuery: string) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/artists", searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchArtistSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ artists: IArtistPreview[] }>>(
    {
      url: new SearchAPIURL(`/artists/${page}`, searchParams),
      method: "GET",
    },
  );

  return apiBody.data;
}

interface IProfileSearchArg {
  query: string;
  site_id?: ISite["id"];
}

export async function fetchSearchProfilesPagination({
  query,
  site_id,
}: IProfileSearchArg) {
  const searchParams = new URLSearchParams([["query", query]]);

  if (site_id) {
    searchParams.set("site_id", site_id);
  }

  const resBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/profiles", searchParams),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchSearchProfiles(
  { query, site_id }: IProfileSearchArg,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", query]]);

  if (site_id) {
    searchParams.set("site_id", site_id);
  }

  const resBody = await apiFetch<
    IPaginatedData<{ profiles: IProfilePreview[] }>
  >({
    url: new SearchAPIURL(`/profiles/${page}`, searchParams),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchFileSearchPagination(searchQuery: string) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/files", searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchFileItemSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ files: IEntityItem[] }>>({
    url: new SearchAPIURL(`/files/items/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteSearchPagination(searchQuery: string) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/sites", searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchSiteSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ sites: ISitePreview[] }>>({
    url: new SearchAPIURL(`/sites/previews/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}
export async function fetchSiteItemSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ sites: IEntityItem[] }>>({
    url: new SearchAPIURL(`/sites/items/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleaseSearchPagination(searchQuery: string) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/releases", searchParams),
    method: "GET",
  });

  return apiBody.data;
}
export async function fetchReleaseSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<
    IPaginatedData<{ releases: IReleasePreview[] }>
  >({
    url: new SearchAPIURL(`/releases/previews/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleaseItemSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ releases: IEntityItem[] }>>({
    url: new SearchAPIURL(`/releases/items/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostSearchPagination(searchQuery: string) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPagination>({
    url: new SearchAPIURL("/posts", searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ posts: IPostPreview[] }>>({
    url: new SearchAPIURL(`/posts/previews/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostItemSearch(
  searchQuery: string,
  page: IBigIntegerPositive,
) {
  const searchParams = new URLSearchParams([["query", searchQuery]]);

  const apiBody = await apiFetch<IPaginatedData<{ posts: IEntityItem[] }>>({
    url: new SearchAPIURL(`/posts/items/${page}`, searchParams),
    method: "GET",
  });

  return apiBody.data;
}
