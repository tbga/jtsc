import { apiFetch, APIURL } from "#lib/api";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type IPost } from "#entities/post";
import { type IArtistPreview } from "#entities/artist";
import { type IBigIntegerPositive } from "#types";
import { type IReleasePreview } from "#entities/release";

export async function fetchPost(post_id: IPost["id"]) {
  const apiBody = await apiFetch<IPost>({
    url: new APIURL(`/post/${post_id}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostArtistsPagination(post_id: IPost["id"]) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/post/${post_id}/artists`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostArtists(
  post_id: IPost["id"],
  page: IBigIntegerPositive,
) {
  const apiBody = await apiFetch<IPaginatedData<{ artists: IArtistPreview[] }>>(
    {
      url: new APIURL(`/post/${post_id}/artists/${page}`),
      method: "GET",
    },
  );

  return apiBody.data;
}

export async function fetchPostReleasesPagination(post_id: IPost["id"]) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/post/${post_id}/releases`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchPostReleases(
  post_id: IPost["id"],
  page: IBigIntegerPositive,
) {
  const apiBody = await apiFetch<
    IPaginatedData<{ releases: IReleasePreview[] }>
  >({
    url: new APIURL(`/post/${post_id}/releases/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
