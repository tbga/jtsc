import { type IName } from "#entities/name";
import { apiFetch, APIURL } from "#lib/api";
import { type IBigSerialInteger } from "#types";
import { type IPaginatedData, type IPagination } from "#lib/pagination";

export async function fetchNamesPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/names"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchNames(page: IBigSerialInteger) {
  const apiBody = await apiFetch<IPaginatedData<{ names: IName[] }>>({
    url: new APIURL(`/names/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
