import { type IProfile, type IProfilePreview } from "#entities/profile";
import { apiFetch, APIURL } from "#lib/api";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type IEntityItem } from "#lib/entities";
import { type IReleasePreview } from "#entities/release";
import { type IArtistPreview } from "#entities/artist";
import { type IBigIntegerPositive, type IBigSerialInteger } from "#types";

export async function fetchProfilesPagination() {
  const resBody = await apiFetch<IPagination>({
    url: new APIURL("/profiles"),
    method: "GET",
  });

  return resBody.data;
}

export interface IProfilesData {
  pagination: IPagination;
  profiles: IProfilePreview[];
}

export async function fetchProfiles(page: IBigSerialInteger) {
  const resBody = await apiFetch<IProfilesData>({
    url: new APIURL(`/profiles/${page}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfile(profileID: IBigSerialInteger) {
  const resBody = await apiFetch<IProfile>({
    url: new APIURL(`/profile/${profileID}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileNamesPagination(
  profileID: IBigSerialInteger,
) {
  const resBody = await apiFetch<IPagination>({
    url: new APIURL(`/profile/${profileID}/names`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileNames(
  profileID: IBigSerialInteger,
  page: IBigSerialInteger,
) {
  const resBody = await apiFetch<IPaginatedData<{ names: IEntityItem[] }>>({
    url: new APIURL(`/profile/${profileID}/names/${page}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileReleasesPagination(
  profileID: IProfile["id"],
) {
  const resBody = await apiFetch<IPagination>({
    url: new APIURL(`/profile/${profileID}/releases`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileReleases(
  profileID: IProfile["id"],
  page: IBigIntegerPositive,
) {
  const resBody = await apiFetch<
    IPaginatedData<{ releases: IReleasePreview[] }>
  >({
    url: new APIURL(`/profile/${profileID}/releases/${page}`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileArtistPagination(profileID: IProfile["id"]) {
  const resBody = await apiFetch<IPagination>({
    url: new APIURL(`/profile/${profileID}/artists`),
    method: "GET",
  });

  return resBody.data;
}

export async function fetchProfileArtists(
  profileID: IProfile["id"],
  page: IBigIntegerPositive,
) {
  const resBody = await apiFetch<IPaginatedData<{ artists: IArtistPreview[] }>>(
    {
      url: new APIURL(`/profile/${profileID}/artists/previews/${page}`),
      method: "GET",
    },
  );

  return resBody.data;
}

export async function fetchProfileArtistItems(
  profileID: IProfile["id"],
  page: IBigIntegerPositive,
) {
  const resBody = await apiFetch<IPaginatedData<{ artists: IEntityItem[] }>>({
    url: new APIURL(`/profile/${profileID}/artists/items/${page}`),
    method: "GET",
  });

  return resBody.data;
}
