import { apiFetch, APIURL } from "#lib/api";
import { type IPagination } from "#lib/pagination";
import { type IPostPreview } from "#entities/post";
import { type IBigSerialInteger } from "#types";

export async function fetchPostsPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/posts"),
    method: "GET",
  });

  return apiBody.data;
}

export interface IPostsData {
  pagination: IPagination;
  posts: IPostPreview[];
}

export async function fetchPosts(page: IBigSerialInteger) {
  const apiBody = await apiFetch<IPostsData>({
    url: new APIURL(`/posts/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
