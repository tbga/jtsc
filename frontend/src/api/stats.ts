import { apiFetch, APIURL } from "#lib/api";
import { type IAPIResponseStats } from "#codegen/schema/lib/api/stats/response";

export async function fetchStats() {
  const apiBody = await apiFetch<IAPIResponseStats["data"]>({
    url: new APIURL("/stats"),
    method: "GET",
  });

  return apiBody.data;
}
