import { apiFetch, APIURL } from "#lib/api";
import { type INonEmptyString } from "#lib/strings";
import { type IAccount } from "#entities/account";

export async function fetchAccount() {
  const apiBody = await apiFetch<IAccount>({
    url: new APIURL("/account"),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchUserAgent() {
  const apiBody = await apiFetch<INonEmptyString>({
    url: new APIURL("/account/user-agent"),
    method: "GET",
  });

  return apiBody.data;
}
