import { apiFetch, APIURL } from "#lib/api";
import { type IPaginatedData, type IPagination } from "#lib/pagination";
import { type IReleasePreview, type IRelease } from "#entities/release";
import { type IPostPreview } from "#entities/post";
import { type IProfilePreview } from "#entities/profile";
import { type IBigIntegerPositive, type IBigSerialInteger } from "#types";

export async function fetchReleasesPagination() {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL("/releases"),
    method: "GET",
  });

  return apiBody.data;
}

export interface IReleasesData {
  pagination: IPagination;
  releases: IReleasePreview[];
}

export async function fetchReleases(page: IBigSerialInteger) {
  const apiBody = await apiFetch<IReleasesData>({
    url: new APIURL(`/releases/${page}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchRelease(release_id: IRelease["id"]) {
  const apiBody = await apiFetch<IRelease>({
    url: new APIURL(`/release/${release_id}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleasePostsPagination(releaseID: IRelease["id"]) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/release/${releaseID}/posts`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleasePosts(
  releaseID: IRelease["id"],
  page: IBigIntegerPositive,
) {
  const apiBody = await apiFetch<IPaginatedData<{ posts: IPostPreview[] }>>({
    url: new APIURL(`/release/${releaseID}/posts/${page}`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleaseProfilesPagination(
  releaseID: IRelease["id"],
) {
  const apiBody = await apiFetch<IPagination>({
    url: new APIURL(`/release/${releaseID}/profiles`),
    method: "GET",
  });

  return apiBody.data;
}

export async function fetchReleaseProfiles(
  releaseID: IRelease["id"],
  page: IBigIntegerPositive,
) {
  const apiBody = await apiFetch<
    IPaginatedData<{ profiles: IProfilePreview[] }>
  >({
    url: new APIURL(`/release/${releaseID}/profiles/${page}`),
    method: "GET",
  });

  return apiBody.data;
}
