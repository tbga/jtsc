import { logoutAccount } from "#lib/authentication";
import { FetchError, specializedFetch, type IFetchArgs } from "#lib/fetch";
import { toJSON } from "#lib/json";
import { APIError } from "./errors";
import { APIRequest } from "./request";
import { type IAPIResponse, IAPIResponseFailure } from "./types";

const { json, raw } = specializedFetch();

interface IAPIFetchArgs extends IFetchArgs {
  body?: any;
}

export const rawFetch = raw;

/**
 * @TODO move `APIURL` call there.
 */
export async function apiFetch<DataShape = unknown>({
  body,
  ...args
}: IAPIFetchArgs) {
  try {
    let responseBody: IAPIResponse<DataShape>;

    if (!args.credentials) {
      args.credentials = "include";
    }

    switch (args.method) {
      case "GET": {
        responseBody = await json<IAPIResponse<DataShape>>(args);
        break;
      }

      default: {
        responseBody = await json<IAPIResponse<DataShape>>({
          ...args,
          body: toJSON(new APIRequest(body)),
        });
        break;
      }
    }

    if (!responseBody.is_successful) {
      const error = new APIError([
        "Failed to fetch from API, Reason:",
        ...responseBody.errors,
      ]);

      throw error;
    }

    return responseBody;
  } catch (error) {
    if (!FetchError.isError(error)) {
      throw error;
    }

    const { res } = error;
    if (res.status === 401) {
      await logoutAccount();
    }
    const resBody: IAPIResponseFailure = await res.json();
    const apiError = new APIError(
      ["Failed to fetch from API, Reason:", ...resBody.errors],
      {
        cause: error,
      },
    );

    throw apiError;
  }
}
