import { type IActionFailure, type IActionSuccess } from "#types";

export interface IAPIRequest<DataShape> {
  data: DataShape;
}

export type IAPIResponse<DataShape> =
  | IAPIResponseSuccess<DataShape>
  | IAPIResponseFailure;

export interface IAPIResponseSuccess<DataShape> extends IActionSuccess {
  data: DataShape;
}
export interface IAPIResponseFailure extends IActionFailure {
  errors: string[];
}
