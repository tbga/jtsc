import { ProjectError } from "#lib/errors";

export class APIError extends ProjectError {}
