import { type IAPIRequest } from "./types";

export class APIRequest<DataShape> implements IAPIRequest<DataShape> {
  data: DataShape;

  constructor(data: DataShape) {
    this.data = data;
  }

  toJSON() {
    return {
      data: this.data,
    };
  }
}
