import { API_URL } from "#environment/derived-variables";

export class APIURL extends URL {
  /**
   * @param searchParams Overwrites search params in the `pathName` argument.
   */
  constructor(pathName: string, searchParams?: URLSearchParams) {
    super(`/api/v1${pathName}`, API_URL);

    if (searchParams) {
      this.search = searchParams.toString();
    }
  }
}

export class AccountAPIURL extends APIURL {
  constructor(pathName: string, searchParams?: URLSearchParams) {
    const finalPath = `/account${pathName}`;
    super(finalPath, searchParams);
  }
}

export class SearchAPIURL extends AccountAPIURL {
  constructor(pathName: string, searchParams?: URLSearchParams) {
    const finalPath = `/search${pathName}`;
    super(finalPath, searchParams);
  }
}

export class AdministratorAPIURL extends AccountAPIURL {
  constructor(pathName: string, searchParams?: URLSearchParams) {
    const finalPath = `/role/administrator${pathName}`;
    super(finalPath, searchParams);
  }
}
