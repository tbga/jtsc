import { STORAGE_URL } from "#environment/derived-variables";

export class StorageURL extends URL {
  static isStorageURL(url: string) {
    return url.startsWith("/");
  }

  constructor(pathname: string) {
    super(`${STORAGE_URL.pathname}${pathname}`, STORAGE_URL.origin);

    if (this.host !== STORAGE_URL.host) {
      this.host = STORAGE_URL.host;
    }
  }
}
