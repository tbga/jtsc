export type { INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";

export type IMultilineString = string[];

export const NEWLINE_SEPARATOR = "\n";
