import { NEWLINE_SEPARATOR } from "./types";

export { validateNonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
export { nanoIDSchema } from "#codegen/schema/lib/types/strings/nanoid";

/**
 * Joins all arguments with a newline.
 * Falsy arguments do not get included into the output.
 */
export function multilineString(
  ...lines: Array<string | number | boolean | undefined>
) {
  const newLines = lines.filter((line) => line);

  return newLines.join("\n");
}

/**
 * Changes the first letter of a string to lower case.
 * @returns Lower-cased string.
 */
export function decapitalizeString(inputString: string) {
  const firstLetter = inputString.charAt(0).toLowerCase();
  const rest = inputString.slice(1);
  const result = [firstLetter, rest].join("");
  return result;
}

export function commentString(...lines: Array<string>) {
  const commentLines = lines.map((line) => ` * ${line}`);
  const comment = multilineString("/*", ...commentLines, " */");

  return comment;
}

/**
 * Splits a given string by `separator`.
 * Both input string and substrings are trimmed beforehand.
 *
 * @TODO
 * - return `undefined` on empty output array.
 * - return a tuple with at least one element on non-empty output
 * @returns An array of non-empty strings
 */
export function collectStrings(
  value: string,
  separator: string = NEWLINE_SEPARATOR,
): string[] {
  const strings = value
    .trim()
    .split(separator)
    .reduce<string[]>((strings, str) => {
      const parsedStr = str.trim();

      if (!parsedStr) {
        return strings;
      }

      strings.push(str);

      return strings;
    }, []);

  return strings;
}

/**
 * Uppercases the character at the start of the string.
 * @returns A new string with uppercased first character.
 */
export function capitalizeString(inputString: string): string {
  const outputString = `${inputString
    .charAt(0)
    .toUpperCase()}${inputString.slice(1)}`;

  return outputString;
}
