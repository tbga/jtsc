import { multilineString } from "#lib/strings";
import { isError } from "./lib";
import { type IErrorMessage } from "./types";

/**
 * The error thrown by the code in the project.
 */
export class ProjectError extends Error {
  static isError<ErrorType extends ProjectError>(
    this: new (...args: any[]) => ErrorType,
    error: unknown,
  ): error is ErrorType {
    return error instanceof this;
  }

  constructor(message: IErrorMessage, options?: ErrorOptions) {
    super(
      Array.isArray(message) ? multilineString(...message) : message,
      options,
    );

    this.name = this.constructor.name;

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if ("captureStackTrace" in Error) {
      Error.captureStackTrace(this, this.constructor);
    }
  }

  getMessageStack() {
    const messages = [this.message];
    let currentCause = this.cause;

    while (true) {
      if (!isError(currentCause)) {
        break;
      }

      messages.push(currentCause.message);
      currentCause = currentCause.cause;
    }

    messages.reverse();

    return multilineString(...messages);
  }
}
