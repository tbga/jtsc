export type IErrorMessage = string[] | string;

export interface IErrorOptions extends ErrorOptions {}
