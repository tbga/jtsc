export { isError } from "./lib";
export { ProjectError } from "./project";
export type { IErrorMessage, IErrorOptions } from "./types";
