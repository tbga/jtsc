import { LinkProps } from "next/link";
import { API_URL, ORIGIN_URL } from "#environment/derived-variables";
import { ProjectURL } from "./project";
import { type ILinkType, LINK_TYPE } from "./types";

export function guessURLType(
  inputLink: URL | ProjectURL | LinkProps["href"],
): ILinkType {
  return typeof inputLink === "string"
    ? guessURLStringType(inputLink)
    : guessURLInstanceType(inputLink);
}

function guessURLStringType(inputLink: string) {
  return inputLink.startsWith("/") ||
    inputLink.startsWith(API_URL.origin) ||
    inputLink.startsWith(ORIGIN_URL.origin)
    ? LINK_TYPE.INTERNAL
    : LINK_TYPE.EXTERNAL;
}

function guessURLInstanceType(inputLink: URL | LinkProps["href"]) {
  return !(inputLink instanceof URL) || inputLink instanceof ProjectURL
    ? LINK_TYPE.INTERNAL
    : LINK_TYPE.EXTERNAL;
}
