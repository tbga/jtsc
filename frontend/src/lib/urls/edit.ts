import { AdministratorURL } from "./account";

export class EditURL extends AdministratorURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof AdministratorURL>[0]) {
    const mergePathname = `/edit${pathname}`;
    super({ ...args, pathname: mergePathname });
  }
}
