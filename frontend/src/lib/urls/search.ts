import { AccountURL } from "./account";

export class SearchURL extends AccountURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof AccountURL>[0]) {
    const searchPathname = `/search${pathname}`;
    super({ ...args, pathname: searchPathname });
  }
}
