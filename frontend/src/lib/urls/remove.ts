import { AdministratorURL } from "./account";

export class RemovePageURL extends AdministratorURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof AdministratorURL>[0]) {
    const removePathname = `/remove${pathname}`;
    super({ ...args, pathname: removePathname });
  }
}
