import { ProjectURL } from "./project";

export class AccountURL extends ProjectURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof ProjectURL>[0]) {
    const mergePathname = `/account${pathname}`;
    super({ ...args, pathname: mergePathname });
  }
}

export class AdministratorURL extends AccountURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof AccountURL>[0]) {
    const mergePathname = `/role/administrator${pathname}`;
    super({ ...args, pathname: mergePathname });
  }
}
