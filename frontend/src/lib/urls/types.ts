import { type IValues } from "#types";

export interface IProjectURL {
  language: string;
  pathname: string;
  searchParams: URLSearchParams;
  hash: string;
}

export const URL_PROTOCOL = {
  FILE: "file://",
} as const;

export const LINK_TYPE = {
  INTERNAL: "internal",
  EXTERNAL: "external",
} as const;

export type ILinkType = IValues<typeof LINK_TYPE>;

export const URL_PATH_SEPARATOR = "/";
