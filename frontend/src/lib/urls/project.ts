import { ORIGIN_URL } from "#environment/derived-variables";
import { type IPartialSome } from "#types";
import { URL_PATH_SEPARATOR, type IProjectURL } from "./types";

/**
 * Does not perform the validation of `language` string.
 */
export class ProjectURL extends URL implements IProjectURL {
  language: string;

  constructor({
    language,
    pathname = "",
    searchParams,
    hash,
  }: IPartialSome<IProjectURL, "pathname" | "searchParams" | "hash">) {
    const joinedPath = `/${language}${pathname}`;
    // adding trailing slash to avoid hydration errors
    const finalPath = joinedPath.endsWith(URL_PATH_SEPARATOR)
      ? joinedPath
      : `${joinedPath}/`;

    super(finalPath, ORIGIN_URL.origin);

    this.host = ORIGIN_URL.host;
    this.language = language;

    if (searchParams) {
      this.search = searchParams.toString();
    }

    if (hash) {
      this.hash = hash;
    }
  }
}
