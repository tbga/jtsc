import { AdministratorURL } from "./account";

export class CreatePageURL extends AdministratorURL {
  constructor({
    pathname = "",
    ...args
  }: ConstructorParameters<typeof AdministratorURL>[0]) {
    const createPathname = `/create${pathname}`;
    super({ ...args, pathname: createPathname });
  }
}
