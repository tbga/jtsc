export { ProjectURL } from "./project";
export { AccountURL, AdministratorURL } from "./account";
export { CreatePageURL } from "./create";
export { EditURL } from "./edit";
export { SearchURL } from "./search";
export { RemovePageURL } from "./remove";
export { guessURLType } from "./guess-type";
export { URL_PROTOCOL, LINK_TYPE, URL_PATH_SEPARATOR } from "./types";
export type { ILinkType } from "./types";
