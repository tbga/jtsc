export function bigIntMax(...args: bigint[]) {
  const max = args.reduce((max, current) => {
    return current > max ? current : max;
  });

  return max;
}

export function bigIntMin(...args: bigint[]) {
  const min = args.reduce((min, current) => {
    return current < min ? current : min;
  });

  return min;
}
export function bigIntMinAndMax(...args: bigint[]): [bigint, bigint] {
  let currentMin = args[0];
  let currentMax = args[0];
  const minMax = [currentMin, currentMax] as [bigint, bigint];

  return args.reduce((minMax, current) => {
    const [min, max] = minMax;
    currentMin = current < min ? current : min;
    currentMax = current > max ? current : max;

    if (currentMin !== min) {
      minMax[0] = currentMin;
    }

    if (currentMax !== max) {
      minMax[1] = currentMax;
    }

    return minMax;
  }, minMax);
}
