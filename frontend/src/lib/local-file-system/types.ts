export type { IFileSystem } from "#codegen/schema/lib/local-file-system/system";
export type { IPath } from "#codegen/schema/lib/local-file-system/path";
export type { IEntry } from "#codegen/schema/lib/local-file-system/entry";
export type { IFolder } from "#codegen/schema/lib/local-file-system/folder";
export type { IFile } from "#codegen/schema/lib/local-file-system/file";

export const FILE_PROTOCOL = "file://";
export const SEPARATOR_LINUX = "/";
export const SEPARATOR_WINDOWS = "\\";

export type ISeparator = typeof SEPARATOR_LINUX | typeof SEPARATOR_WINDOWS;
