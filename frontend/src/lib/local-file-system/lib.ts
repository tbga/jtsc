import { ProjectError } from "#lib/errors";
import {
  fetchEntry,
  fetchFile,
  fetchFolder,
  fetchSystem,
} from "#api/account/administrator/local-file-system";
import { type IPath, type IEntry, type IFile, type IFolder } from "./types";

export async function getSystem() {
  return await fetchSystem();
}

export async function getPath(path: IPath, type: "file"): Promise<IFile>;
export async function getPath(path: IPath, type: "folder"): Promise<IFolder>;
export async function getPath(path: IPath, type?: IEntry["type"]) {
  if (!type) {
    const entry = await fetchEntry(path);
    type = entry.type;
  }

  switch (type) {
    case "folder": {
      return fetchFolder(path);
    }
    case "file": {
      return fetchFile(path);
    }

    default: {
      throw new ProjectError(`Unsupported entry type "${type}".`);
    }
  }
}
