export { getSystem, getPath } from "./lib";
export { LocalPath } from "./path";
export type { IFileSystem, IPath, IEntry, IFolder, IFile } from "./types";
