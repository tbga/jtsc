import { truthy } from "#lib/std/array/known-filters";
import {
  FILE_PROTOCOL,
  SEPARATOR_WINDOWS,
  SEPARATOR_LINUX,
  type IPath,
  type ISeparator,
} from "./types";

export class LocalPath extends URL {
  separator: ISeparator;

  constructor(
    pathname: IPath | LocalPath,
    base: IPath | LocalPath = FILE_PROTOCOL,
  ) {
    super(pathname, base);

    if (this.protocol !== FILE_PROTOCOL) {
      this.protocol = FILE_PROTOCOL;
    }

    this.separator = this.pathname.includes(SEPARATOR_WINDOWS)
      ? SEPARATOR_WINDOWS
      : SEPARATOR_LINUX;

    // remove trailing slash
    if (this.pathname.endsWith(this.separator)) {
      this.pathname = this.pathname.slice(
        0,
        this.pathname.length - this.separator.length,
      );
    }
  }

  /**
   * @returns A new instance of `LocalPath`.
   */
  dirName() {
    return new LocalPath("./", this);
  }

  /**
   * @param segments A list of strings without separators.
   * @returns A new instance of `LocalPath`.
   */
  join(...segments: string[]) {
    const newSegments = this.pathname.split(this.separator);
    const joinedPath = [...newSegments, ...segments].join(this.separator);

    return new LocalPath(joinedPath, this);
  }

  segments(isFiltered = true) {
    const segments = this.pathname.split(this.separator);

    return !isFiltered ? segments : segments.filter(truthy);
  }
}
