import { addYears as addYearsFunc } from "date-fns";
import { fromDateTime, toDateTime } from "./lib";
import { type IDateTime } from "./types";

export function addYears(date: IDateTime, amount: number) {
  const parsedDate = fromDateTime(date);
  const newDate = addYearsFunc(parsedDate, amount);
  const newParsedDate = toDateTime(newDate);

  return newParsedDate;
}
