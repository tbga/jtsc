import { format } from "date-fns";
import { fromDateTime } from "./lib";
import { type IDateTime } from "./types";

export const defaultFormat = "do MMMM yyyy GGGG";

export function formatDate(date: IDateTime) {
  return format(fromDateTime(date), defaultFormat);
}
