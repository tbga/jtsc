import {
  formatRFC3339,
  getDate as getDateFunc,
  getDaysInMonth as getDaysInMonthFunc,
  getHours as getHoursFunc,
  getMilliseconds as getMillisecondsFunc,
  getMinutes as getMinutesFunc,
  getMonth as getMonthFunc,
  getSeconds as getSecondsFunc,
  getYear as getYearFunc,
  parseISO,
  startOfToday,
} from "date-fns";
import { type IDateTime } from "./types";

export { validateDateTime } from "#codegen/schema/lib/types/dates/datetime";

const defaultOptions: Parameters<typeof formatRFC3339>["1"] = {
  fractionDigits: 3,
};

export function toDateTime(date: Date): IDateTime {
  return formatRFC3339(date, defaultOptions);
}

export function fromDateTime(date: IDateTime): Date {
  return parseISO(date);
}

export function nowDateTime() {
  return toDateTime(new Date());
}

export function todayDateTime() {
  return toDateTime(startOfToday());
}

export function getYear(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const year = getYearFunc(parsedDate);

  return year;
}

export function getMonth(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const month = getMonthFunc(parsedDate);

  return month;
}

export function getDate(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const day = getDateFunc(parsedDate);

  return day;
}

export function getHours(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const hour = getHoursFunc(parsedDate);

  return hour;
}

export function getMinutes(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const minute = getMinutesFunc(parsedDate);

  return minute;
}

export function getSeconds(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const second = getSecondsFunc(parsedDate);

  return second;
}

export function getMilliseconds(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const millisecond = getMillisecondsFunc(parsedDate);

  return millisecond;
}

/**
 * @TODO minutes offset
 */
export function getTimezone(date: IDateTime): string {
  const parsedDate = fromDateTime(date);
  const offsetInMinutes = parsedDate.getTimezoneOffset();
  const offsetHours = Math.floor(offsetInMinutes / 60);
  const hours = String(offsetHours * -1).padStart(2, "0");
  const timezone = `${offsetHours < 0 ? "+" : ""}${hours}:00`;

  return timezone;
}

export function getDaysInMonth(date: IDateTime): number {
  const parsedDate = fromDateTime(date);
  const days = getDaysInMonthFunc(parsedDate);

  return days;
}
