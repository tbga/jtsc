export type { IDateTime } from "#codegen/schema/lib/types/dates/datetime";

export const UNIX_ERA_START = "1970-01-01T00:00:00.000+00:00";
export const ZERO_DATETIME = "0000-01-01T00:00:00.000+00:00";
