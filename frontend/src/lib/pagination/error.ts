import { ProjectError, type IErrorOptions } from "#lib/errors";
import { type IPagination } from "./types";

export interface IPaginationErrorOptions extends IErrorOptions {
  pagination?: IPagination;
}

export class PaginationError extends ProjectError {
  pagination: IPaginationErrorOptions["pagination"];

  constructor(message: string, options?: IPaginationErrorOptions) {
    super(message, options);

    if (options?.pagination) {
      this.pagination = options.pagination;
    }
  }
}
