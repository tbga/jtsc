import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IBigSerialInteger } from "#types";

export type { IPagination } from "#codegen/schema/lib/types/pagination/pagination";

export type IURLBuilder = (page: string | number) => string | URL;

export type IPaginatedData<DataShape = unknown> = {
  pagination: IPagination;
} & DataShape;

export interface IPaginationNavigational extends IPagination {
  urlBuilder: IURLBuilder;
}

export interface IPaginationInternal extends IPagination {
  onPageChange: (page: IBigSerialInteger) => Promise<void>;
}

export const DEFAULT_LIMIT = 25;
