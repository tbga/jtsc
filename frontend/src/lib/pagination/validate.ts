import { PaginationError } from "#lib/pagination";
import { BIGINT_ONE, type IBigIntegerPositive } from "#types";

export function validatePageNumber(page: IBigIntegerPositive) {
  const parsedPage = BigInt(page);

  if (parsedPage < BIGINT_ONE) {
    throw new PaginationError(`Page number ${page} is less than 1.`);
  }
}
