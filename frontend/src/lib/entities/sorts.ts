import { type IEntityItem } from "#lib/entities";

export function sortByIDAsc(prev: IEntityItem, next: IEntityItem) {
  const prevID = BigInt(prev.id);
  const nextID = BigInt(next.id);

  const result = prevID === nextID ? 0 : prevID < nextID ? -1 : 1;

  return result;
}
