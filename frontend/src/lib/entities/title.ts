import type { IEntityItem } from "./types";

export function entityTitle(
  id: IEntityItem["id"],
  name: IEntityItem["name"],
): string {
  const trimmedName = name?.trim();
  // ensure that an entity with name "Unknown" is formatted differently
  // from entity without a name
  const finalName = trimmedName ? `"${trimmedName}"` : "Unknown";
  const title = `${finalName} (${id})`;

  return title;
}
