import { collectStrings } from "#lib/strings";

export function collectIDs(value: string): string[] {
  return collectStrings(value);
}
