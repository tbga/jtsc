/*
  Stolen from:
  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set#implementing_basic_set_operations
*/

export function isSuperset<ItemType>(
  set: Set<ItemType>,
  subset: Set<ItemType>,
) {
  for (const elem of subset) {
    if (!set.has(elem)) {
      return false;
    }
  }

  return true;
}

export function union<ItemType>(setA: Set<ItemType>, setB: Set<ItemType>) {
  const unionSet = new Set(setA);

  for (const elem of setB) {
    unionSet.add(elem);
  }

  return unionSet;
}

export function intersection<ItemType>(
  setA: Set<ItemType>,
  setB: Set<ItemType>,
) {
  const intersectionSet = new Set();

  for (const elem of setB) {
    if (setA.has(elem)) {
      intersectionSet.add(elem);
    }
  }

  return intersectionSet;
}

export function symmetricDifference<ItemType>(
  setA: Set<ItemType>,
  setB: Set<ItemType>,
) {
  const differenceSet = new Set(setA);

  for (const elem of setB) {
    if (differenceSet.has(elem)) {
      differenceSet.delete(elem);
    } else {
      differenceSet.add(elem);
    }
  }

  return differenceSet;
}

export function difference<ItemType>(setA: Set<ItemType>, setB: Set<ItemType>) {
  const differenceSet = new Set(setA);

  for (const elem of setB) {
    differenceSet.delete(elem);
  }

  return differenceSet;
}
