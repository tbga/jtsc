export function isIncludedInArray<
  ArrayElement extends ValueType,
  ValueType extends unknown,
>(arr: ReadonlyArray<ArrayElement>, value: ValueType): value is ArrayElement {
  return arr.includes(value as ArrayElement);
}
