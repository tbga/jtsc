import { type ValidateFunction, type DefinedError } from "ajv/dist/2020";
import { ValidationError } from "./errors";
import { type IJSONSchema } from "./types";

export function createValidator<SchemaInterface>(
  validate: ValidateFunction<SchemaInterface>,
  schema: IJSONSchema,
) {
  return (inputJSON: unknown): inputJSON is SchemaInterface => {
    const result = validate(inputJSON);

    if (!result) {
      // `errors` key is always an array when validation is failed
      const errors = [...(validate.errors! as DefinedError[])];
      throw new ValidationError(errors, schema.$id);
    }

    return true;
  };
}
