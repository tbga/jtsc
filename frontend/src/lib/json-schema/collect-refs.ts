import { type IJSONSubSchema } from "./types";

export interface IRefSet extends Set<IRefKey> {}
type IRefKey = Required<IJSONSubSchema>["$ref"];

export function collectRefs(
  schema: IJSONSubSchema,
  refs: IRefSet = new Set<IRefKey>(),
): IRefSet {
  if ("type" in schema && schema.type === "array") {
    // @ts-expect-error narrowed type
    collectArraySchemaRefs(schema, refs);
  }

  if ("$ref" in schema && typeof schema.$ref === "string") {
    refs.add(schema.$ref);
  }

  if ("allOf" in schema && schema.allOf) {
    schema.allOf.forEach((allOfSchema) => {
      if (typeof allOfSchema === "boolean") {
        return;
      }
      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(allOfSchema, refs);
    });
  }

  if ("anyOf" in schema && schema.anyOf) {
    schema.anyOf.forEach((anyOfSchema) => {
      if (typeof anyOfSchema === "boolean") {
        return;
      }
      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(anyOfSchema, refs);
    });
  }

  if ("oneOf" in schema && schema.oneOf) {
    schema.oneOf.forEach((oneOfSchema) => {
      if (typeof oneOfSchema === "boolean") {
        return;
      }
      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(oneOfSchema, refs);
    });
  }

  if ("propertyNames" in schema && schema.propertyNames) {
    collectRefs(schema.propertyNames, refs);
  }

  if ("properties" in schema && schema.properties) {
    // collect refs from propery schemas
    Object.values(schema.properties).forEach((schema) => {
      if (typeof schema === "boolean") {
        return;
      }
      collectRefs(schema, refs);
    });
  }

  if (
    "additionalProperties" in schema &&
    typeof schema.additionalProperties !== "boolean" &&
    schema.additionalProperties
  ) {
    collectRefs(schema.additionalProperties, refs);
  }

  // remove self-references
  refs.delete("#");

  return refs;
}

interface IArraySchema extends IJSONSubSchema {
  type: "array";
}

function collectArraySchemaRefs(schema: IArraySchema, refs: IRefSet) {
  if ("items" in schema && schema.items !== undefined) {
    const items = schema.items;

    !Array.isArray(items)
      ? collectRefs(items, refs)
      : items.forEach((schema) => {
          collectRefs(schema, refs);
        });
  }

  return refs;
}
