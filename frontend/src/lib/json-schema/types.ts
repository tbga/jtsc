import { type JSONSchema7 } from "json-schema";

export interface ISchemaMap
  extends Record<Required<IJSONSchema>["$id"], IJSONSchema> {}

export interface IJSONSchema extends IJSONSubSchema {
  $id: string;
  title: string;
}

export interface IJSONSubSchema extends JSONSchema7 {
  propertyNames?: IJSONSubSchema;
  properties?: {
    [key: string]: IJSONSubSchema;
  };

  additionalProperties?: boolean | IJSONSubSchema;
  items?: IJSONSubSchema | IJSONSubSchema[];
}
