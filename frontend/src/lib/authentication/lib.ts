import {
  fetchLoginAccount,
  fetchLogoutAccount,
  fetchRegisterAccount,
} from "#api/authentication";
import { fetchAccount } from "#api/account";
import {
  deleteLocaleStoreItem,
  getLocalStoreItem,
  LOCAL_STORAGE_KEYS,
  setLocalStoreItem,
} from "#store/local";
import {
  type IAccountLogin,
  type IAccountInit,
  type IAccount,
} from "#entities/account";

export async function getAccount(): Promise<IAccount | undefined> {
  const isRegistered = getLocalStoreItem<boolean>(
    LOCAL_STORAGE_KEYS.IS_REGISTERED,
  );

  if (!isRegistered) {
    return;
  }

  let account = getLocalStoreItem<IAccount>(LOCAL_STORAGE_KEYS.ACCOUNT);

  if (account) {
    return account;
  }

  account = await fetchAccount();

  setLocalStoreItem(LOCAL_STORAGE_KEYS.IS_REGISTERED, true);
  setLocalStoreItem(LOCAL_STORAGE_KEYS.ACCOUNT, account);

  return account;
}

export async function registerAccount(init: IAccountInit) {
  const account = await fetchRegisterAccount(init);

  setLocalStoreItem(LOCAL_STORAGE_KEYS.IS_REGISTERED, true);
  setLocalStoreItem(LOCAL_STORAGE_KEYS.ACCOUNT, account);

  return account;
}

export async function loginAccount(login: IAccountLogin) {
  const account = await fetchLoginAccount(login);

  setLocalStoreItem(LOCAL_STORAGE_KEYS.IS_REGISTERED, true);
  setLocalStoreItem(LOCAL_STORAGE_KEYS.ACCOUNT, account);

  return account;
}

export async function logoutAccount(isLocal: boolean = false) {
  if (!isLocal) {
    await fetchLogoutAccount();
  }

  deleteLocaleStoreItem(LOCAL_STORAGE_KEYS.IS_REGISTERED);
  deleteLocaleStoreItem(LOCAL_STORAGE_KEYS.ACCOUNT);
}
