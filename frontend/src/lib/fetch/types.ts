export interface IFetchArgs extends RequestInit {
  url: URL;
  headers?: Headers;
  method?: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
}

export interface ISpecializedFetch {
  raw: (args: IFetchArgs) => Promise<Response>;
  json: <BodyType>(
    ...args: Parameters<ISpecializedFetch["raw"]>
  ) => Promise<BodyType>;
}
