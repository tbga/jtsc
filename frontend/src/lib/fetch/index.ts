export { specializedFetch } from "./specialized";
export { FetchError } from "./error";
export type { IFetchArgs, ISpecializedFetch } from "./types";
