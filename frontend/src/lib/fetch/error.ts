import { ProjectError } from "#lib/errors";
import { multilineString } from "#lib/strings";
import { type IFetchArgs } from "./types";

export class FetchError extends ProjectError {
  req: IFetchArgs;
  res: Response;

  constructor(args: IFetchArgs, res: Response) {
    const message = multilineString(
      `Failed to fetch a response from ${args.method} - "${args.url}"`,
      `Message: ${res.status} - ${res.statusText}`,
    );
    super(message);

    this.req = args;
    this.res = res;
  }
}
