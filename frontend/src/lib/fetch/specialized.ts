import { FetchError } from "./error";
import { type ISpecializedFetch, type IFetchArgs } from "./types";

const jsonHeaders = new Headers([
  ["Accept", "application/json"],
  ["Content-Type", "application/json"],
]);

export function specializedFetch(): ISpecializedFetch {
  async function rawFetch(args: IFetchArgs) {
    // https://github.com/sindresorhus/ky#kyinput-options
    if (!args.credentials) {
      args.credentials = "same-origin";
    }

    const response = await fetch(args.url, args);

    return response;
  }

  async function jsonFetch<BodyType = unknown>(
    ...args: Parameters<typeof rawFetch>
  ) {
    const [fetchArgs] = args;
    let headers = fetchArgs.headers;

    if (!headers) {
      headers = jsonHeaders;
    } else {
      Array.from(jsonHeaders).forEach(([key, value]) => {
        // @ts-expect-error it is defined though
        headers.append(key, value);
      });
    }

    const response = await rawFetch({ ...fetchArgs, headers });

    if (!response.ok) {
      throw new FetchError(fetchArgs, response);
    }

    const data: BodyType = await response.json();

    return data;
  }

  return {
    raw: rawFetch,
    json: jsonFetch,
  };
}
