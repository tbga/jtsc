import { ProjectError } from "#lib/errors";
import { toJSONPretty } from "#lib/json";

export function isEmpty<ItemType extends unknown = unknown>(
  data: ItemType[] | ItemType,
) {
  if (data === undefined) {
    return false;
  }

  if (Array.isArray(data)) {
    return !data.length;
  }

  throw new ProjectError(["Unsupported type for data:", toJSONPretty(data)]);
}
