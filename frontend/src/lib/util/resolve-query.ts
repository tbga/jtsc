import { ProjectError } from "#lib/errors";
import { type ParsedUrlQuery } from "querystring";

export const QUERY_TYPE = {
  /**
   * Only the first value is used.
   * Is the default type.
   */
  FIRST: "first",
  /**
   * Only the last value is used.
   */
  LAST: "last",
  /**
   * All values are used.
   */
  ALL: "all",
} as const;
export type IQueryValue = ParsedUrlQuery[keyof ParsedUrlQuery];
export type IQueryType = (typeof QUERY_TYPE)[keyof typeof QUERY_TYPE];
export type IQueryTypes<QueryKeys extends string> = Record<
  QueryKeys,
  IQueryType
>;

/**
 * Resolves all query keys declared in the `types` argument
 * @param queryObject The parsed query object.
 * @param types A dictionary of the query keys and their resolution types.
 */
export function resolveQuery<QueryKeys extends string>(
  queryObject: ParsedUrlQuery,
  types: IQueryTypes<QueryKeys>,
): Record<QueryKeys, IQueryValue> {
  const resolvedQuery = Object.entries<IQueryType>(types).reduce<
    Record<keyof typeof types, IQueryValue>
  >(
    (resolvedQuery, [key, type]) => {
      const value = queryObject[key];
      const resolvedValue = resolveQueryValue(value, type);

      Object.defineProperty(resolvedQuery, key, {
        enumerable: true,
        value: resolvedValue,
      });

      return resolvedQuery;
    },
    {} as Record<keyof typeof types, IQueryValue>,
  );

  return resolvedQuery;
}

/**
 * @TODO rewrite properly
 */
export function resolveQueryValue(
  queryValue: ParsedUrlQuery[keyof ParsedUrlQuery],
  type: typeof QUERY_TYPE.ALL,
): [string, ...string[]] | undefined;
export function resolveQueryValue(
  queryValue: ParsedUrlQuery[keyof ParsedUrlQuery],
  type?: IQueryType,
): string | undefined;
export function resolveQueryValue(
  queryValue: ParsedUrlQuery[keyof ParsedUrlQuery],
  type: IQueryType = QUERY_TYPE.FIRST,
) {
  switch (type) {
    case QUERY_TYPE.FIRST: {
      return queryValue === undefined
        ? queryValue
        : Array.isArray(queryValue)
          ? queryValue[0]
          : queryValue;
    }

    case QUERY_TYPE.LAST: {
      return queryValue === undefined
        ? queryValue
        : !Array.isArray(queryValue)
          ? queryValue
          : queryValue[queryValue.length - 1];
    }

    case QUERY_TYPE.ALL: {
      return !queryValue
        ? undefined
        : !Array.isArray(queryValue)
          ? [queryValue]
          : queryValue;
    }

    default: {
      throw new ProjectError(`Unknown query type "${type}"`);
    }
  }
}
