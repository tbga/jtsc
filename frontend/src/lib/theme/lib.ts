import { DEFAULT_THEME } from "#environment/variables";
import { COOKIE, getCookie, setCookie } from "#store/cookie";
import { THEMES, type ITheme } from "./types";

// @TODO value syncing

export function getTheme(): ITheme {
  let theme: string | undefined;

  // check for the presence in document
  theme = document.documentElement.dataset.theme;

  if (theme && isTheme(theme)) {
    return theme;
  }

  // check for presence in the cookie
  theme = getCookie(COOKIE.THEME);

  if (theme && isTheme(theme)) {
    return theme;
  }

  return DEFAULT_THEME;
}

export function setTheme(nextTheme: ITheme): ITheme {
  setCookie(COOKIE.THEME, nextTheme);
  document.documentElement.dataset.theme = nextTheme;

  return nextTheme;
}

function isTheme(inputString: string): inputString is ITheme {
  // @ts-expect-error const array
  return THEMES.includes(inputString);
}
