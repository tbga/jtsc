export { getTheme, setTheme } from "./lib";
export { THEME } from "./types";
export type { ITheme } from "./types";
