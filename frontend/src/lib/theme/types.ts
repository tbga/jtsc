export const THEME = {
  LIGHT: "light",
  DARK: "dark",
} as const;
export const THEMES = Object.values(THEME);
export type ITheme = (typeof THEME)[keyof typeof THEME];
