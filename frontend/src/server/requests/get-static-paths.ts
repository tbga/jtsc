import { type GetStaticPathsResult, type GetStaticPaths } from "next";
import { SUPPORTED_LANGUAGES } from "#environment/variables";
import { type IBaseURLQuery } from "./types";

/**
 * @TODO extendable paths
 */
export function createStaticPaths(): GetStaticPaths<IBaseURLQuery> {
  async function getStaticPaths(): Promise<
    GetStaticPathsResult<IBaseURLQuery>
  > {
    const paths = SUPPORTED_LANGUAGES.map((lang) => {
      return {
        params: {
          lang,
        },
      };
    });

    const result = {
      paths,
      fallback: false,
    };

    return result;
  }

  return getStaticPaths;
}
