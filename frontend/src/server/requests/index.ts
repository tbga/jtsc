export { createStaticPaths } from "./get-static-paths";
export { createStaticProps } from "./get-static-props";
