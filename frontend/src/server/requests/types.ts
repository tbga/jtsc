import { type ParsedUrlQuery } from "querystring";

export interface IBaseURLQuery extends ParsedUrlQuery {
  lang: string;
}

export interface IBaseProps {
  lang: string;
}
