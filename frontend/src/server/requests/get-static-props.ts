import { EMPTY_ARRAY } from "#types";
import {
  type GetStaticPropsContext,
  type GetStaticProps,
  type GetStaticPropsResult,
} from "next";
import { type IBaseURLQuery, type IBaseProps } from "./types";

interface IOptions {
  nameSpaces?: readonly string[];
}

const defaultOptions: Required<IOptions> = {
  nameSpaces: EMPTY_ARRAY,
};

export function createStaticProps(
  options?: IOptions,
): GetStaticProps<IBaseProps, IBaseURLQuery> {
  const resolvedOptions = !options
    ? defaultOptions
    : { ...defaultOptions, ...options };

  async function getSaticProps({
    params,
  }: GetStaticPropsContext<IBaseURLQuery>): Promise<
    GetStaticPropsResult<IBaseProps>
  > {
    const { lang } = params!;
    const props: IBaseProps = {
      lang,
    };
    const result: GetStaticPropsResult<IBaseProps> = {
      props,
    };
    return result;
  }

  return getSaticProps;
}
