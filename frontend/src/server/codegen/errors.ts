import { ProjectError } from "#lib/errors";

export class CodegenError extends ProjectError {}
