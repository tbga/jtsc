export interface ICodegenModule extends Record<string, unknown> {
  default: ICodegenFunc;
}

export interface ICodegenFunc<
  ReturnShape extends ICodegen | ICodegenGraph = ICodegen,
> {
  (): Promise<ReturnShape>;
}

export interface ICodegen {
  /**
   * Export info used for creating index file.
   */
  exports: ICodegenExports;
  imports?: ICodegenImport[];
  result: string;
}

export interface ICodegenImport {
  names: string[];
  path: string;
  isType?: boolean;
}

export interface ICodegenExports {
  types?: string[];
  concrete?: string[];
}

/**
 * Exports of the module.
 */
export interface ICodegenGraph {
  type: "graph";
  root: string;
  nodes: ICodegenNode[];
}

export interface ICodegenNode {
  /**
   * A path to the module relative to the graph root.
   */
  nodePath: string;
  /**
   * The dictionary of module names and their data.
   */
  modules: Record<string, IModule>;
}

export interface IModule {
  /**
   * Information on the exported symbols of the module.
   */
  exports: IExport[];
  /**
   * The code of the module.
   */
  result: string;
}

export interface IExport {
  symbolName: string;
  /**
   * An alias for the exported member.
   */
  symbolAlias?: string;
  /**
   * Is it a `type` export or not.
   */
  isAbstract?: boolean;
}

export const codegenFolder = ["src", "codegen"];
export const generatorFilename = "generator";
export const generatorModule = `${generatorFilename}.ts`;
export const resultFilename = "result";
export const resultModule = `${resultFilename}.ts`;
export const indexFilename = "index";
export const indexModule = `${indexFilename}.ts`;
export const libFilename = "lib";
export const libModule = `${libFilename}.ts`;
export const typesFilename = "types";
