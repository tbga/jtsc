import path from "path";
import fs from "fs/promises";
import { multilineString } from "#lib/strings";
import { CodegenError } from "./errors";
import {
  indexFilename,
  type ICodegenGraph,
  type ICodegenNode,
  type IExport,
} from "./types";
import { saveToFile } from "./save-to-file";

export async function runCodegenGraph(
  moduleFolder: string,
  { root, nodes }: ICodegenGraph,
) {
  const rootFolder = path.join(moduleFolder, root);

  // @TODO more intelligent cleanup
  await fs.rm(rootFolder, { recursive: true, force: true });

  console.log(
    `Generating ${nodes.length} nodes for the graph "${path.basename(
      moduleFolder,
    )}"...`,
  );

  for await (const [index, node] of nodes.entries()) {
    console.log(`Generating node ${index + 1} "${node.nodePath}".`);
    await createNode(rootFolder, node);
  }

  console.log(
    `Finished generating nodes for the graph "${path.basename(moduleFolder)}".`,
  );
}

export async function createNode(
  rootFolder: string,
  { nodePath, modules }: ICodegenNode,
) {
  const nodeFolder = path.join(rootFolder, nodePath);
  const indexExports = new Map<string, IExport[]>();
  const moduleEntries = Object.entries(modules);

  if (!moduleEntries.length) {
    throw new CodegenError(
      `The codegen node at "${nodeFolder}" doesn't produce modules.`,
    );
  }

  for await (const [moduleName, nodeModule] of moduleEntries) {
    const nodePath = path.join(nodeFolder, `${moduleName}.ts`);
    const { exports, result } = nodeModule;

    await saveToFile(nodePath, result);

    indexExports.set(moduleName, exports);
  }

  await createIndex(nodeFolder, indexExports);
}

export async function createIndex(
  nodeFolder: string,
  exportMap: Map<string, IExport[]>,
) {
  const indexPath = path.join(nodeFolder, `${indexFilename}.ts`);
  const lines = Array.from(exportMap).reduce<string[]>(
    (lines, [moduleName, exports]) => {
      const types: string[] = [];
      const concrete: string[] = [];

      exports.forEach(({ symbolName, isAbstract, symbolAlias }) => {
        const name = symbolAlias
          ? `${symbolName} as ${symbolAlias}`
          : symbolName;
        isAbstract ? types.push(name) : concrete.push(name);
      });

      if (concrete.length) {
        lines.push(`export {${concrete.join(", ")}} from "./${moduleName}";`);
      }

      if (types.length) {
        lines.push(`export type {${types.join(", ")}} from "./${moduleName}";`);
      }

      return lines;
    },
    [],
  );

  const content = multilineString(...lines);

  await saveToFile(indexPath, content);
}
