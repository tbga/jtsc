import { cwd } from "process";
import { readFile } from "fs/promises";
import path from "path";
import Ajv from "ajv/dist/2020";
import standaloneCode from "ajv/dist/standalone";
import { default as addFormats } from "ajv-formats";
import {
  compile as compileSchemaInterface,
  type Options as JS2TSOptions,
} from "json-schema-to-typescript";
import { collectRefs, IRefSet, type IJSONSchema } from "#lib/json-schema";
import { reduceFolder } from "#server/lib/fs";
import { fromJSON, toJSON } from "#lib/json";
import { isNativeError } from "util/types";
import { CodegenError } from "./errors";

export interface IJSONSchemaCollection
  extends Record<IJSONSchema["$id"], IJSONSchemaInfo> {}

export interface IJSONSchemaInfo {
  localPath: string;
  schema: IJSONSchema;
}

export const schemaFileEnd = ".schema.json";
const metaSchemaFolder = "meta";
let collection: IJSONSchemaCollection | undefined = undefined;

export async function collectJSONSchemas() {
  if (collection) {
    return collection;
  }
  const schemaFolder = path.join(cwd(), "..", "schema");

  collection = await reduceFolder<IJSONSchemaCollection>(
    schemaFolder,
    {},
    async (schemaCollection, filePath, entry) => {
      // for now exclude meta schemas
      const isSchema =
        entry.isFile() &&
        filePath.dir !== path.join(schemaFolder, metaSchemaFolder) &&
        entry.name.endsWith(schemaFileEnd);

      if (!isSchema) {
        return schemaCollection;
      }

      const schemaPath = path.format(filePath);
      const schemaJSON = await readFile(schemaPath, { encoding: "utf-8" });
      const jsonSchema = fromJSON<IJSONSchema>(schemaJSON);

      if (!jsonSchema.$id || !jsonSchema.title) {
        throw new Error(
          `JSON schema file should have "$id" and "title" properties at the top level and the schema at "${filePath}" misses either of them.`,
        );
      }

      if (schemaCollection[jsonSchema.$id]) {
        throw new Error(
          `JSON schema file with ID "${jsonSchema.$id}" already exists, the schema at "${schemaPath}" must have a different "$id" value.`,
        );
      }

      schemaCollection[jsonSchema.$id] = {
        localPath: path.relative(schemaFolder, schemaPath),
        schema: jsonSchema,
      };

      return schemaCollection;
    },
  );

  return collection;
}

export async function getSchemaCollection() {
  return collection ?? collectJSONSchemas();
}

interface ISchemaInterface {
  inputSchema: IJSONSchema;
  code: string;
}

const parserOptions: JS2TSOptions["$refOptions"] = {
  resolve: {
    http: {
      // @ts-expect-error @TODO fix codegen
      async read(file) {
        const schemaCopy = transformSchema(collection![file.url].schema);

        return schemaCopy;
      },
    },
  },
};

export async function createInterfaceFromSchema(
  inputSchema: IJSONSchema,
  options?: Partial<Omit<JS2TSOptions, "$refOptions" | "bannerComment">>,
): Promise<ISchemaInterface> {
  const schemaCopy = transformSchema(inputSchema);
  const interfaceCode = await compileSchemaInterface(
    // @ts-expect-error draft4 type
    schemaCopy,
    schemaCopy.$id,
    { ...options, bannerComment: "", $refOptions: parserOptions },
  );

  const schemaInterface: ISchemaInterface = {
    inputSchema: schemaCopy,
    code: interfaceCode,
  };

  return schemaInterface;
}

function transformSchema(inputSchema: IJSONSchema): IJSONSchema {
  const modifedSchema = fromJSON<IJSONSchema>(toJSON(inputSchema));
  modifedSchema.title = `I${modifedSchema.title}`;

  return modifedSchema;
}

/**
 * Codegen version of AJV.
 */
export async function generateValidationCode(
  inputSchema: IJSONSchema,
  ajv: Ajv,
) {
  try {
    const validate = ajv.getSchema(inputSchema.$id);
    const moduleCode = standaloneCode(ajv, validate);

    return moduleCode;
  } catch (error) {
    if (!isNativeError(error)) {
      throw error;
    }

    throw new CodegenError(
      `Failed to create validation code for schema "${inputSchema.$id}".`,
      { cause: error },
    );
  }
}

export async function createCodegenAJV(
  schemaID: IJSONSchema["$id"],
  isStrict: boolean = false,
) {
  const collection = await getSchemaCollection();
  const schema = collection[schemaID].schema;
  const refs = await collectAllRefs(schema);
  const schemas = Array.from(refs)
    .sort()
    .map((ref) => collection[ref].schema);
  const newAJV = new Ajv({
    code: { source: true, esm: true },
    coerceTypes: !isStrict,
    schemas,
  });

  addFormats(newAJV);

  return newAJV;
}

/**
 * Collect all direct and transitive dependencies
 * for a given schema ID.
 */
async function collectAllRefs(
  schema: IJSONSchema,
  collectedRefs?: IRefSet,
  refs?: IRefSet,
) {
  if (!collectedRefs) {
    collectedRefs = new Set();
  }

  if (!refs) {
    refs = new Set();
  }

  const collection = await getSchemaCollection();
  const directRefs = collectRefs(schema);

  refs.add(schema.$id);
  collectedRefs.add(schema.$id);

  for await (const ref of directRefs) {
    // skip if it's already collected
    if (collectedRefs.has(ref)) {
      continue;
    }

    const refSchema = collection[ref].schema;
    await collectAllRefs(refSchema, collectedRefs, refs);
  }

  return refs;
}
