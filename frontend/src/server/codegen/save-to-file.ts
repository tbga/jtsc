import { type PathLike } from "fs";
import prettier from "prettier";
import { commentString, multilineString } from "#lib/strings";
import { writeToFile } from "#server/lib/fs";

export async function saveToFile(filePath: PathLike, content: string) {
  const codeGenContent = multilineString(
    commentString(
      "This file was generated automatically, do not edit it by hand.",
    ),
    content,
  );

  let formattedCode = codeGenContent;

  try {
    formattedCode = await prettier.format(codeGenContent, {
      parser: "typescript",
    });
  } catch (error) {
    console.warn(
      `Failed to format the file at "${filePath}". Continue unformatted.`,
    );
  }

  await writeToFile(filePath, formattedCode, { createNew: true });
}
