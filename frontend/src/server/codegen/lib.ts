import path from "path";
import fs from "fs/promises";
import { cwd } from "process";
import { isNativeError } from "util/types";
import prettier from "prettier";
import { dynamicImport, reduceFolder } from "#server/lib/fs";
import { multilineString } from "#lib/strings";

import { CodegenError } from "./errors";
import { runCodegenGraph } from "./graph";
import {
  ICodegenImport,
  type ICodegen,
  type ICodegenModule,
  codegenFolder,
  generatorModule,
  resultModule,
  indexModule,
  resultFilename,
  ICodegenFunc,
  ICodegenGraph,
} from "./types";

/**
 * @TODO `prettier` to pick up on its config
 */
export async function runCodegen() {
  const codegenPath = path.join(cwd(), ...codegenFolder);
  const codegenDirs = await reduceFolder<string[]>(
    codegenPath,
    [],
    async (codegenDirs, entryPath, dirEntry) => {
      try {
        if (!dirEntry.isFile() || dirEntry.name !== generatorModule) {
          return codegenDirs;
        }
        const modulePath = path.format(entryPath);
        const { default: codegen }: ICodegenModule = await import(modulePath);
        const codegenInfo = await codegen();

        if (isCodegenGraph(codegenInfo)) {
          await runCodegenGraph(entryPath.dir, codegenInfo);
        } else {
          const { imports, exports, result } = codegenInfo;
          const resultPath = path.join(entryPath.dir, resultModule);
          const finalResult = multilineString(
            "/*",
            "  This module was created by the codegen, do not edit it manually.",
            "*/",
            imports && parseImports(imports),
            result,
            "\n",
          );
          const indexPath = path.join(entryPath.dir, indexModule);
          const indexContent = createIndex(exports);
          const codegenModule = {
            index: indexContent,
            result: finalResult,
          };

          Object.entries(codegenModule).forEach(([key, code]) => {
            try {
              // @TODO: figure out why the config file isn't picked up.
              const formattedCode = prettier.format(code, {
                parser: "typescript",
              });
              // @ts-expect-error typescript is crying
              codegenModule[key] = formattedCode;
            } catch (error) {
              console.warn(
                `Failed to format the "${key}" at "${modulePath}"`,
                "Continue unformatted",
              );
            }
          });

          await fs.writeFile(resultPath, codegenModule.result);
          await fs.writeFile(indexPath, codegenModule.index);

          codegenDirs.push(path.relative(codegenPath, entryPath.dir));
        }

        return codegenDirs;
      } catch (error) {
        if (!isNativeError(error)) {
          throw error;
        }

        throw new CodegenError("Failed to run codegen", { cause: error });
      }
    },
  );
  return codegenDirs;
}

function parseImports(imports: ICodegenImport[]): string {
  const importsStrings = imports.reduce<string[]>((imports, currentImport) => {
    const { names, path, isType } = currentImport;
    const importString = `import ${isType ? "type" : ""} { ${names.join(
      ", ",
    )} } from "${path}";`;

    imports.push(importString);

    return imports;
  }, []);

  return multilineString(...importsStrings);
}

function createIndex(exports: ICodegen["exports"]): string {
  const concreteExports = [
    "export {",
    (exports.concrete || []).join(", "),
    `} from "./${resultFilename}"`,
  ].join(" ");
  const typeExports = [
    "export type {",
    (exports.types || []).join(", "),
    `} from "./${resultFilename}"`,
  ].join(" ");
  const indexContent = multilineString(concreteExports, typeExports);
  return indexContent;
}

function isCodegenGraph(
  info: Awaited<ReturnType<ICodegenFunc>>,
  // @ts-expect-error `"type"` is a dicriminator key
): info is ICodegenGraph {
  return "type" in info;
}
