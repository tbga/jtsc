export {
  collectPagePathInfo,
  getPagePathMap,
  savePagePathInfoToFile,
} from "./lib";
