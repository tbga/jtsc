import path from "path";
import { cwd } from "process";
import { defineProperty } from "#lib/std";
import { reduceFolder, writeJSONFile } from "#server/lib/fs";
import { convertWindowsPathToLinux } from "#server/lib/path";
import { IS_WINDOWS } from "#environment/constants";

/**
 * A mapping of nextjs route paths and their file system paths.
 */
interface IPagePathMap extends Record<string, string> {}

let pagePathmap: IPagePathMap | undefined = undefined;

export async function collectPagePathInfo() {
  if (pagePathmap) {
    return pagePathmap;
  }

  const pagesFolder = path.join(cwd(), "dist", "public");
  const pageMap = await reduceFolder<IPagePathMap>(
    pagesFolder,
    {},
    async (pageMap, parsedPath, entry) => {
      const isValidFile = entry.isFile() && parsedPath.ext === ".html";

      if (!isValidFile) {
        return pageMap;
      }

      const absolutePath = path.format(parsedPath);
      const { publicPath, routePath } = createRoutePath(
        pagesFolder,
        absolutePath,
      );

      defineProperty(pageMap, routePath, {
        value: publicPath,
        enumerable: true,
      });

      return pageMap;
    },
  );

  pagePathmap = pageMap;

  return pageMap;
}

export async function getPagePathMap() {
  return pagePathmap ?? collectPagePathInfo();
}

export async function savePagePathInfoToFile(filePath: string) {
  const pathMap = await getPagePathMap();
  await writeJSONFile(filePath, pathMap);
}

function createRoutePath(pagesFolder: string, absolutePath: string) {
  const publicPath = `/${
    IS_WINDOWS
      ? convertWindowsPathToLinux(path.relative(pagesFolder, absolutePath))
      : path.relative(pagesFolder, absolutePath)
  }`;
  const normalizedPath =
    path.basename(absolutePath) === "index.html"
      ? path.dirname(absolutePath)
      : absolutePath;
  const relativePath = IS_WINDOWS
    ? convertWindowsPathToLinux(path.relative(pagesFolder, normalizedPath))
    : path.relative(pagesFolder, normalizedPath);

  return {
    routePath: `/${relativePath}`,
    publicPath: publicPath,
  };
}
