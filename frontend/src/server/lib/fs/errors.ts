import { ProjectError } from "#lib/errors";

/**
 * Node.js has the class listed in the docs:
 * https://nodejs.org/docs/latest-v16.x/api/errors.html#class-systemerror
 * But doesn't export it to public API.
 */
export interface ISystemError extends Error {
  /**
   * The string error code.
   */
  code: string;

  /**
   * The name of the system call that triggered the error.
   */
  syscall: string;

  /**
   * The system-provided error number.
   */
  errno: number;

  /**
   * If present, extra details about the error condition.
   */
  info?: Object;

  /**
   * A system-provided human-readable description of the error.
   */
  message: string;

  /**
   * If present, the file path when reporting a file system error.
   */
  path?: string;
  /**
   * If present, the file path destination when reporting a file system error.
   */
  dest?: string;

  /**
   * If present, the network connection port that is not available.
   */
  port?: number;

  /**
   * If present, the address to which a network connection failed.
   */
  address?: string;
}

export function isSystemError(error: Error): error is ISystemError {
  return "errno" in error;
}

export class FileSystemError extends ProjectError {}

export class FolderReductionError extends FileSystemError {}
