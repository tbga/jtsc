import fs from "fs/promises";
import path from "path";
import { getSystemErrorName } from "util";
import { isNativeError } from "util/types";
import { FileSystemError, isSystemError } from "./errors";

interface IWriteToFileOptions {
  /**
   * Create the file at the path if doesn't exist.
   * @default true
   */
  createNew?: boolean;
}

const defaultOptions: Required<IWriteToFileOptions> = {
  createNew: true,
};

export async function writeToFile(
  filePath: Parameters<typeof fs.writeFile>["0"],
  content: Parameters<typeof fs.writeFile>["1"],
  options?: Partial<IWriteToFileOptions>,
) {
  const finalOptions =
    options && !Object.is(options, defaultOptions)
      ? { ...defaultOptions, options }
      : defaultOptions;

  try {
    await fs.writeFile(filePath, content);
  } catch (error) {
    if (!isNativeError(error)) {
      throw error;
    }

    const isMissingFileError =
      finalOptions.createNew &&
      isSystemError(error) &&
      getSystemErrorName(error.errno) === "ENOENT";

    if (isMissingFileError) {
      await fs.mkdir(path.dirname(String(filePath)), { recursive: true });
      await fs.appendFile(filePath, "");
      await writeToFile(filePath, content, finalOptions);
      return;
    }

    throw new FileSystemError(`Failed to write a file to "${filePath}".`, {
      cause: error,
    });
  }
}
