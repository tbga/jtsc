import { pathToFileURL } from "url";

/**
 * Need to wrap it into a separate function because
 * dynamic import on Windows requires `file:` `URL`.
 */
export async function dynamicImport(modulePath: string) {
  const fileURL = pathToFileURL(modulePath);

  return import(fileURL.toString());
}
