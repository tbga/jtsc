import path from "path";

export function convertWindowsPathToLinux(winPath: string) {
  return winPath.split(path.sep).join(path.posix.sep);
}
