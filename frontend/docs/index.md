# Frontend

## Build output

The result of the build can be separated into these categories:

- `static` - `/public/_next`<br>
  All paths in these folder incorporate unique IDs of sorts, which get busted on content change at build time.<br>
  Therefore they can be safely set `immutable` and `max-age` of 1 year.
- `translations` - `/public/locales`<br>
  Translation files change only on build, but they also do not have unique identifiers.<br>
  Therefore they need a `max-age` header between 10 minutes and 1 hour.
- `pages` - `**/*.html`<br>
  Same as `translations.`
- `rest`<br>
  Outside of `favicon.ico`, there shouldn't by any other files.<br>
  The same rules as `translations` applies.

## Pages

### Initialization Order

1. Initialization.<br>
   At this point nothing is known about the page, therefore it should show a `PUBLIC_TITLE` as a title of the page,`PUBLIC_TITLE` as a heading (if the page has it) and language-agnostic loading state.
   1. Resolve language info from the URL.
   2. Resolve common and page translations for the language.
   3. Resolve authentication info.
2. Page props fetching.
   All basics are resolved, therefore language-specific title and heading placeholders can be shown, along with language-specific loading states.
3. Page props fetched.
   Language-specific and page-specific title, heading and data can be shown with no loading state.

## HTML

- look into [`<search>` tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/search) and how to conditionally incorporate it into codebase.
