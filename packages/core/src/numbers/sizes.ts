export const KILOBYTE = 1024;
export const MEGABYTE = 1024 * 1024;
export const GIGABYTE = 1024 * 1024 * 1024;
export const TERABYTE = 1024 * 1024 * 1024 * 1024;
export const PETABYTE = 1024 * 1024 * 1024 * 1024 * 1024;
