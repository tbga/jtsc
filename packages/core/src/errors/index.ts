/**
 * Stolen from [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#custom_error_types).
 */
export class CustomError extends Error {
  constructor(...args: ConstructorParameters<typeof Error>) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...args);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    // @ts-ignore it's v8-only thing
    // typescript doesn't like it because it's a part of nodejs types only
    if (Error.captureStackTrace) {
      // @ts-ignore it's v8-only thing
      // typescript doesn't like it because it's a part of nodejs types only
      Error.captureStackTrace(this, this.constructor);
    }

    this.name = this.constructor.name;
  }
}

export class NotImplementedError extends CustomError {
  constructor() {
    super("Not Implemented.");
  }
}

export function validateError(input: unknown): asserts input is Error {
  const isError = input instanceof Error;

  if (!isError) {
    throw input;
  }
}
