export interface IProgress<DataShape = undefined> {
  current: number;
  total: number;
  data?: DataShape;
}
