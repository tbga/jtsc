import { MEGABYTE } from "@repo/core/numbers";
import { createSHA256 } from "hash-wasm";

const chunkSize = 64 * MEGABYTE;
let hasher: Awaited<ReturnType<typeof createSHA256>> | undefined = undefined;
let cache: WeakMap<File, string> | undefined = undefined;

export async function getHashSHA256(
  file: File,
  onProgress?: (currentSize: number, totalSize: number) => Promise<void>
) {
  if (!cache) {
    cache = new WeakMap();
  }

  const isCached = cache.has(file);

  if (isCached) {
    const hash = cache.get(file)!;

    if (onProgress) {
      await onProgress(file.size, file.size);
    }

    return hash;
  }

  if (hasher) {
    hasher.init();
  } else {
    hasher = await createSHA256();
  }

  const chunkNumber = Math.floor(file.size / chunkSize);

  for (let index = 0; index <= chunkNumber; index++) {
    const currentSize = chunkSize * index;
    const nextSize = Math.min(chunkSize * (index + 1), file.size);
    const chunk = file.slice(currentSize, nextSize);
    const buffer = await chunk.arrayBuffer();
    const view = new Uint8Array(buffer);

    hasher.update(view);

    if (onProgress) {
      await onProgress(currentSize, file.size);
    }
  }

  const hash = hasher.digest();

  return hash;
}
