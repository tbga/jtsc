/**
 * `URL` but trailing slash removed.
 */
export class NormalizedURL extends URL {
  constructor(...args: ConstructorParameters<typeof URL>) {
    super(...args);

    if (this.pathname.endsWith("/")) {
      this.pathname = this.pathname.slice(0, -1);
    }
  }
}

export function getPathnameSegments(url: NormalizedURL) {
  return url.pathname.slice(1).split("/");
}

export function getPathnameAndSearchParams(url: NormalizedURL) {
  const search = url.searchParams.size === 0 ? "" : url.search;

  return `${url.pathname}${search}`;
}
