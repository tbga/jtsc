export function isMixedCase(input: string) {
  return input.toLowerCase() !== input;
}
