export function countOccurencesOfCharacter(input: string, value: string): number {
  let count = 0;

  for (const character of input) {
    if (character !== value) {
      continue
    }

    count++
  }

  return count
}
