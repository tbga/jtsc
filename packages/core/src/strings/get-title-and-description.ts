export function getTitleAndDescription(inputString: string): {
  title?: string;
  description?: string;
} {
  const content = inputString.trim();
  let result: ReturnType<typeof getTitleAndDescription> = {};

  if (!content || content.length === 0) {
    return result;
  }

  // it's assumed the first line is a title and the rest is description
  const firstNewLineIndex = content.indexOf("\n");

  // `Array.slice()` supports negative indices in the arguments
  // but it has a different semantics from return of `Array.indexOf()`.
  const isNewline = firstNewLineIndex !== -1;
  const title = !isNewline
    ? content
    : content.slice(0, firstNewLineIndex).trim() || undefined;
  const description = !isNewline
    ? undefined
    : content.slice(firstNewLineIndex).trim() || undefined;

  result.title = title;
  result.description = description;

  return result;
}
