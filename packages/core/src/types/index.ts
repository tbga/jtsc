/**
 * Make selected keys optional.
 */
export type IPartialSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Partial<Type>, Key>;

/**
 * Make selected keys required.
 */
export type IRequiredSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Required<Type>, Key>;

export type IElementType<T extends Iterable<any>> = T extends Iterable<infer E>
  ? E
  : never;
