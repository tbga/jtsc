import {
  KILOBYTE,
  MEGABYTE,
  GIGABYTE,
  TERABYTE,
  PETABYTE,
} from "@repo/core/numbers";

export function createSizeFormatter(locale: string) {
  const formatBytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "byte",
    unitDisplay: "long",
    maximumFractionDigits: 3,
  }).format;
  const formatKilobytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "kilobyte",
    unitDisplay: "long",
    maximumFractionDigits: 3,
  }).format;
  const formatMegabytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "megabyte",
    unitDisplay: "long",
    maximumFractionDigits: 1,
  }).format;
  const formatGigabytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "gigabyte",
    unitDisplay: "long",
    maximumFractionDigits: 1,
  }).format;
  const formatTerabytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "terabyte",
    unitDisplay: "long",
    maximumFractionDigits: 1,
  }).format;
  const formatPetabytes = new Intl.NumberFormat(locale, {
    style: "unit",
    unit: "petabyte",
    unitDisplay: "long",
    maximumFractionDigits: 1,
  }).format;

  return (value: number) => {
    if (value > PETABYTE) {
      return formatPetabytes(value / PETABYTE);
    }

    if (value > TERABYTE) {
      return formatTerabytes(value / TERABYTE);
    }

    if (value > GIGABYTE) {
      return formatGigabytes(value / GIGABYTE);
    }

    if (value > MEGABYTE) {
      return formatMegabytes(value / MEGABYTE);
    }

    if (value > KILOBYTE) {
      return formatKilobytes(value / KILOBYTE);
    }

    return formatBytes(value);
  };
}
