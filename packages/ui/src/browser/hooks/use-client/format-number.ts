export function createNumberFormatter(locale: string) {
  const formatBig = new Intl.NumberFormat(locale, {
    notation: "engineering",
  }).format;
  const formatSmall = new Intl.NumberFormat(locale, { compactDisplay: "long" })
    .format;

  return (value: number) => {
    if (value > 1_000_000) {
      return formatBig(value);
    }

    return formatSmall(value);
  };
}
