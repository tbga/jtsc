import { isWebKitDirectorySupported } from "#browser/compatibility";
import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { createSizeFormatter } from "./format-size";
import { createNumberFormatter } from "./format-number";

type IClientContext =
  | {
      isClient: false;
    }
  | { isClient: true; clientData: IClientData };

interface IClientData {
  isWebkitDirectorySupported: boolean;
  formatNumber: (value: number) => string;
  formatSize: (value: number) => string;
}

const defaultContext: IClientContext = {
  isClient: false,
};

const ClientContext = createContext<IClientContext>(defaultContext);

export function ClientProvider({ children }: { children: ReactNode }) {
  const [clientData, setClientData] = useState<IClientData>();

  useEffect(() => {
    const locale = Intl.NumberFormat().resolvedOptions().locale;
    const clientData: IClientData = {
      isWebkitDirectorySupported: isWebKitDirectorySupported(),
      formatNumber: createNumberFormatter(locale),
      formatSize: createSizeFormatter(locale),
    };

    setClientData(clientData);
  }, []);

  return (
    <ClientContext.Provider
      value={
        !clientData
          ? {
              isClient: false,
            }
          : { isClient: true, clientData }
      }
    >
      {children}
    </ClientContext.Provider>
  );
}

export function useClient(): IClientContext {
  const context = useContext(ClientContext);

  return context;
}
