/**
 * Stolen from
 * https://stackoverflow.com/a/47872423
 */
export function isWebKitDirectorySupported() {
  const input = document.createElement("input");

  return typeof input.webkitdirectory === "boolean";
}
