import { createBlockComponent, IBlockProps } from "#components/meta";

export interface ILoadingBarProps extends IBlockProps<"div"> {}

export const LoadingBar = createBlockComponent(undefined, Component);

function Component({ children, ...blockProps }: ILoadingBarProps) {
  return <div {...blockProps}>{children ?? "Loading..."}</div>;
}
