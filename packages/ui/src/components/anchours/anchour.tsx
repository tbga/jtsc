import { forwardRef, LegacyRef } from "react";
import { createBlockComponent, IBlockProps } from "#components/meta";

import styles from "./anchour.module.scss";

export interface IAnchourBaseProps extends Omit<IBlockProps<"a">, "href"> {
  href?: URL | string;
}

export const AnchourBase = forwardRef<
  HTMLElementTagNameMap["a"],
  IAnchourBaseProps
>(createBlockComponent(styles, Component));

function Component(
  { href, children, ...props }: IAnchourBaseProps,
  ref?: LegacyRef<HTMLAnchorElement>
) {
  const finalHref = href instanceof URL ? href.toString() : href;

  return (
    <a href={finalHref} {...props} ref={ref}>
      {children ?? finalHref}
    </a>
  );
}
