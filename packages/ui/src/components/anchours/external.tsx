import { createBlockComponent } from "#components/meta";
import { AnchourBase, IAnchourBaseProps } from "./anchour";

import styles from "./external.module.scss";

export interface IAnchourExternalProps
  extends Omit<IAnchourBaseProps, "referrerPolicy" | "target"> {}

export const AnchourExternal = createBlockComponent(styles, Component);

function Component({ children, ...blockProps }: IAnchourExternalProps) {
  return (
    <AnchourBase {...blockProps} target="_blank" referrerPolicy="no-referrer">
      {children}
    </AnchourBase>
  );
}
