import { useClient } from "#browser/hooks";
import { LoadingBar } from "#components/loading";
import { createBlockComponent } from "#components/meta";
import { IPreProps, Pre } from "./pre";

import styles from "./size.module.scss";

interface IProps extends IPreProps {
  value: number;
}

export const SizeFormatted = createBlockComponent(styles, Component);

function Component({ value, ...props }: IProps) {
  const client = useClient();

  return (
    <Pre data-value={value} {...props}>
      {client.isClient === false ? (
        <LoadingBar />
      ) : (
        client.clientData.formatSize(value)
      )}
    </Pre>
  );
}
