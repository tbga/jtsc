import { useState } from "react";
import { createBlockComponent } from "#components/meta";
import { Details } from "#components/details";
import { Code } from "./code";
import { IPreProps, Pre } from "./pre";
import { DescriptionList, DescriptionSection } from "#components/lists";

import styles from "./json.module.scss";

interface IProps extends IPreProps {
  value: unknown;
}

export const JSONView = createBlockComponent(undefined, Component);

function Component({ value, ...props }: IProps) {
  return (
    <Pre {...props}>
      <Code>
        <JSONValue value={value} />
      </Code>
    </Pre>
  );
}

function JSONValue({ value }: Pick<IProps, "value">) {
  if (Array.isArray(value)) {
    return <JSONArray values={value} />;
  }

  if (typeof value === "object" && value !== null) {
    return (
      <>
        {"{"}
        <DescriptionList className={styles.object}>
          {Object.entries(value)
            .sort()
            .map(([key, value]) => (
              <DescriptionSection
                key={key}
                className={styles.keyValue}
                dKey={key}
                isHorizontal
                dValue={<JSONValue value={value} />}
              />
            ))}
        </DescriptionList>
        {"}"}
      </>
    );
  }

  return JSON.stringify(value, undefined, 2);
}

function JSONArray({ values }: { values: unknown[] }) {
  const [isOpen, switchOpen] = useState(false);

  return (
    <Details
      summary={<>[ {values.length} ]</>}
      open={isOpen}
      onToggle={(event) => {
        switchOpen(event.currentTarget.open);
      }}
    >
      <ol>
        {isOpen &&
          values.map((value, index) => (
            <li key={index}>
              <JSONValue value={value} />
            </li>
          ))}
      </ol>
    </Details>
  );
}
