import { createBlockComponent, IBlockProps } from "#components/meta";

import styles from "./pre.module.scss";

export interface IPreProps extends IBlockProps<"pre"> {}

export const Pre = createBlockComponent(styles.block, Component);

function Component({ ...blockProps }: IPreProps) {
  return <pre {...blockProps} />;
}
