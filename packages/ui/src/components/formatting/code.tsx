import { createBlockComponent, IBlockProps } from "#components/meta";

export interface ICodeProps extends IBlockProps<"pre"> {}

export const Code = createBlockComponent(undefined, Component);

function Component({ ...props }: ICodeProps) {
  return <code {...props} />;
}
