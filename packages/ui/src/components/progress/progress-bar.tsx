import { ReactNode } from "react";
import { IProgress } from "@repo/core/progress";
import { IRequiredSome } from "@repo/core/types";
import { createBlockComponent, IBlockProps } from "#components/meta";

import styles from "./progress-bar.module.scss";

interface IProps<DataShape = undefined>
  extends IRequiredSome<IBlockProps<"div">, "id"> {
  label?:
    | ReactNode
    | ((
        current: IProgress<DataShape>["current"],
        total: IProgress<DataShape>["total"],
        data: IProgress<DataShape>["data"]
      ) => ReactNode);
  progress: IProgress<DataShape>;
}

export const ProgressBar = createBlockComponent(styles, Component);

function Component<DataShape = undefined>({
  id,
  label,
  progress,
  ...props
}: IProps<DataShape>) {
  const { current, total, data } = progress;

  return (
    <div {...props}>
      {label && (
        <label htmlFor={id}>
          {typeof label === "function" ? label(current, total, data) : label}
        </label>
      )}

      <progress
        id={id}
        className={styles.progress}
        value={current}
        max={total}
      />
    </div>
  );
}
