import { createBlockComponent, IBlockProps } from "#components/meta";

import styles from "./standard.module.scss";

interface IListUnorderedProps extends IBlockProps<"ul"> {}
interface IListItemProps extends IBlockProps<"li"> {}

export const ListUnordered = createBlockComponent(
  styles,
  ListUnorderedComponent
);

export const ListItem = createBlockComponent(styles.item, ListItemComponent);

function ListUnorderedComponent({ ...props }: IListUnorderedProps) {
  return <ul {...props} />;
}

function ListItemComponent({ ...props }: IListItemProps) {
  return <li {...props} />;
}
