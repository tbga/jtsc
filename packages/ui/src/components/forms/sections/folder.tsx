import { ChangeEvent, useState } from "react";
import { IProgress } from "@repo/core/progress";
import { createBlockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { InputFolder, IInputFolderProps } from "#components/forms/inputs";
import { type IInputSectionProps } from "./types";
import { Details } from "#components/details";
import {
  DescriptionList,
  DescriptionSection,
  ListItem,
  ListUnordered,
} from "#components/lists";
import { Pre, SizeFormatted } from "#components/formatting";
import { ProgressBar } from "#components/progress";

interface IProps extends IInputSectionProps {}

interface IFolderStat {
  totalSize: number;
  files: File[];
}

export const FormSectionFolder = createBlockComponent(undefined, Component);

function Component({
  id,
  form,
  name,
  label,
  required,
  children,
  ...blockProps
}: IProps) {
  const [progress, changeCurrentProgress] = useState<IProgress>();
  const [folderStats, changeFolderStats] = useState<IFolderStat>();
  const progressID = `${id}-progress`;

  async function handleChange(event: ChangeEvent<HTMLInputElement>) {
    const fileList = event.currentTarget.files as FileList;

    if (!fileList || fileList.length === 0) {
      changeFolderStats(undefined);
      return;
    }

    const files: File[] = [];

    try {
      let totalSize = 0;
      let current = 0;
      changeCurrentProgress({ current, total: fileList.length });

      for await (const file of fileList) {
        files.push(file);
        totalSize = totalSize + file.size;
        current = current + 1;
        changeCurrentProgress({ current, total: fileList.length });
      }

      changeFolderStats({ totalSize, files: files });
    } finally {
      changeCurrentProgress(undefined);
    }
  }

  return (
    <FormSection {...blockProps}>
      <Label htmlFor={id}>{label}</Label>
      <InputFolder
        id={id}
        form={form}
        name={name}
        required={required}
        onChange={handleChange}
      />

      {children && <div>{children}</div>}

      {progress ? (
        <ProgressBar
          id={progressID}
          progress={progress}
          label="Analyzing files..."
        />
      ) : !folderStats ? (
        <p>No folder is selected.</p>
      ) : (
        <DescriptionList>
          <DescriptionSection
            dKey="Total size"
            dValue={<SizeFormatted value={folderStats.totalSize} />}
            isHorizontal
          />

          <DescriptionSection
            dKey="Files"
            dValue={
              <Details summary={<Pre>{folderStats.files.length}</Pre>}>
                <ListUnordered>
                  {folderStats.files.map(
                    ({ webkitRelativePath, name, type, size }) => (
                      <ListItem key={webkitRelativePath}>
                        <Details summary={<Pre>{name}</Pre>}>
                          <DescriptionList>
                            <DescriptionSection
                              dKey="Path"
                              dValue={<Pre>{webkitRelativePath}</Pre>}
                            />
                            <DescriptionSection
                              dKey="Type"
                              dValue={<Pre>{type}</Pre>}
                              isHorizontal
                            />
                            <DescriptionSection
                              dKey="Size"
                              dValue={<SizeFormatted value={size} />}
                              isHorizontal
                            />
                          </DescriptionList>
                        </Details>
                      </ListItem>
                    )
                  )}
                </ListUnordered>
              </Details>
            }
          />
        </DescriptionList>
      )}
    </FormSection>
  );
}
