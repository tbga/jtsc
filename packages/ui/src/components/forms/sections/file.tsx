import { ChangeEvent, useState } from "react";
import { getHashSHA256 } from "@repo/core/hashing";
import { IProgress } from "@repo/core/progress";
import { createBlockComponent } from "#components/meta";
import { FormSection, Label } from "#components/forms";
import { InputFile, IInputFileProps } from "#components/forms/inputs";
import {
  DescriptionList,
  DescriptionSection,
  ListItem,
  ListUnordered,
} from "#components/lists";
import { Pre, SizeFormatted } from "#components/formatting";
import { Details } from "#components/details";
import { type IInputSectionProps } from "./types";
import { ProgressBar } from "#components/progress";

import styles from "./file.module.scss";

interface IProps
  extends IInputSectionProps,
    Pick<IInputFileProps, "accept" | "multiple" | "capture"> {}

interface IFile {
  data: File;
  hashSHA256: string;
}

export const FormSectionFile = createBlockComponent(styles.block, Component);

function Component({
  id,
  form,
  name,
  label,
  accept,
  multiple,
  capture,
  children,
  required,
  ...blockProps
}: IProps) {
  const [files, changeFiles] = useState<IFile[]>([]);
  const [progress, changeCurrentProgress] = useState<IProgress>();
  const progressID = `${id}-progress`;

  async function handleProgress(currentSize: number, totalSize: number) {
    changeCurrentProgress({ current: currentSize, total: totalSize });
  }

  async function handleChange(event: ChangeEvent<HTMLInputElement>) {
    const files = event.currentTarget.files as FileList;

    if (files.length === 0) {
      changeFiles([]);
      return;
    }

    const isOneFile = multiple !== true;
    const nextFiles: IFile[] = [];

    try {
      if (isOneFile) {
        const file = files[0] as File;

        changeCurrentProgress({ current: 0, total: file.size });

        const hashSHA256 = await getHashSHA256(file, handleProgress);

        nextFiles.push({ hashSHA256, data: file });
      } else {
        let current = 0;

        changeCurrentProgress({ current: 0, total: files.length });
        for await (const file of files) {
          const hashSHA256 = await getHashSHA256(file);

          nextFiles.push({ hashSHA256, data: file });
          current = current + 1;
          changeCurrentProgress({ current, total: files.length });
        }
      }
    } finally {
      changeCurrentProgress(undefined);
    }

    changeFiles(nextFiles);
  }

  return (
    <FormSection {...blockProps}>
      <Label className={styles.label} htmlFor={id}>
        {label}
      </Label>
      <InputFile
        id={id}
        form={form}
        className={styles.input}
        name={name}
        accept={accept}
        multiple={multiple}
        capture={capture}
        required={required}
        onChange={handleChange}
      />

      {children && <div>{children}</div>}

      <ListUnordered>
        {progress ? (
          <ListItem>
            <ProgressBar
              id={progressID}
              progress={progress}
              label={"Analyzing files..."}
            />
          </ListItem>
        ) : !files.length ? (
          <ListItem>No files are selected for upload</ListItem>
        ) : (
          files.map(({ hashSHA256, data }, index) => (
            <ListItem key={`${data.name}-${data.type}-${index}`}>
              <Details summary={<Pre>{data.name}</Pre>}>
                <DescriptionList>
                  <DescriptionSection
                    dKey="SHA256 Hash"
                    dValue={<Pre>{hashSHA256}</Pre>}
                  />
                  <DescriptionSection
                    dKey="Media Type"
                    dValue={<Pre>{data.type}</Pre>}
                    isHorizontal
                  />
                  <DescriptionSection
                    dKey="Size"
                    dValue={<SizeFormatted value={data.size} />}
                    isHorizontal
                  />
                </DescriptionList>
              </Details>
            </ListItem>
          ))
        )}
      </ListUnordered>
    </FormSection>
  );
}
