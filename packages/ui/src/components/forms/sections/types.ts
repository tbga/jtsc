import { ReactNode } from "react";
import { IInputBaseProps } from "#components/forms/inputs";
import { type IFormSectionProps } from "../section";

export interface IInputSectionProps
  extends Omit<IFormSectionProps, "id">,
    Pick<
      IInputBaseProps,
      "name" | "required" | "defaultValue" | "readOnly" | "disabled"
    >,
    Pick<Required<IInputBaseProps>, "id" | "form"> {
      label: ReactNode
    }
