import type { IRequiredSome } from "@repo/core/types";
import { createBlockComponent, IBlockProps } from "#components/meta";

interface IProps extends IRequiredSome<IBlockProps<"output">, "form"> {}

export const Output = createBlockComponent(undefined, Component);

function Component({ ...props }: IProps) {
  return <output {...props} />;
}
