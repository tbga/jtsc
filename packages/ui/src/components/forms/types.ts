import { type FormEvent } from "react";
import { type IChildlessBlockProps } from "#components/meta";

/**
 * The form props for components extending client form.
 */
export interface IBaseFormProps
  extends Omit<IChildlessBlockProps<"div">, "id" | "onReset" | "onSubmit">,
    Pick<Required<IChildlessBlockProps<"div">>, "id"> {}

export interface ISubmitEvent<InputName extends string = string>
  extends FormEvent<HTMLFormElement> {
  currentTarget: FormEvent<HTMLFormElement>["currentTarget"] & {
    elements: IFormElements<InputName>;
  };
}

export type IFormElements<InputName extends string> =
  HTMLFormControlsCollection & Record<InputName, HTMLInputElement>;
