import { useState } from "react";
import type { IRequiredSome } from "@repo/core/types";
import { createBlockComponent } from "#components/meta";
import { LoadingBar } from "#components/loading";
import { ButtonReset, ButtonSubmit } from "#components/buttons";
import { FormSection, type ISubmitEvent } from "#components/forms";
import { FormHTML, type IFormHTMLProps } from "./form";

import styles from "./client.module.scss";

export interface IFormClientProps
  extends IRequiredSome<IFormHTMLProps, "onSubmit"> {}

export const FormClient = createBlockComponent(undefined, Component);

function Component({
  submitButton,
  resetButton,
  onSubmit,
  onReset,
  children,
  isNested,
  ...blockProps
}: IFormClientProps) {
  const [isSubmitting, switchSubmitting] = useState(false);
  const [errors, changeErrors] = useState<string[]>([]);

  async function handleSubmit(event: ISubmitEvent) {
    // prevent form submit
    event.preventDefault();

    // do not submit if the current submit is in progress
    if (isSubmitting) {
      return;
    }

    // handle submit
    try {
      switchSubmitting(true);

      await onSubmit(event);
      await onReset?.(event);

      if (errors.length) {
        changeErrors([]);
      }
    } catch (error) {
      changeErrors([String(error)]);
    } finally {
      switchSubmitting(false);
    }
  }

  async function handleReset(event: ISubmitEvent) {
    if (isSubmitting) {
      return;
    }

    try {
      switchSubmitting(true);
      changeErrors([]);

      if (onReset) {
        await onReset(event);
      }
    } finally {
      switchSubmitting(false);
    }
  }

  return (
    <FormHTML
      submitButton={null}
      resetButton={null}
      onSubmit={handleSubmit}
      onReset={handleReset}
      isNested={isNested}
      {...blockProps}
    >
      {(formID) => (
        <>
          {children?.(formID)}

          <Status isSubmitting={isSubmitting} errors={errors} />

          {submitButton === null && resetButton === null ? undefined : (
            <FormSection className={styles.buttons}>
              {submitButton !== null && (
                <ButtonSubmit
                  form={formID}
                  visualType={isNested ? "button" : "submit"}
                  disabled={isSubmitting}
                >
                  {submitButton}
                </ButtonSubmit>
              )}

              {resetButton !== null && (
                <ButtonReset form={formID} disabled={isSubmitting}>{resetButton}</ButtonReset>
              )}
            </FormSection>
          )}
        </>
      )}
    </FormHTML>
  );
}

interface IStatusProps {
  isSubmitting: boolean;
  errors: string[];
}

function Status({ isSubmitting, errors }: IStatusProps) {
  const isErrored = Boolean(errors.length);

  return (
    <FormSection>
      {isSubmitting ? (
        <LoadingBar>Submit is in progress...</LoadingBar>
      ) : (
        isErrored && <Errors errors={errors} />
      )}
    </FormSection>
  );
}

function Errors({ errors }: { errors: string[] }) {
  return (
    <ul>
      {errors.map((message, index) => (
        <li key={index}>{message}</li>
      ))}
    </ul>
  );
}
