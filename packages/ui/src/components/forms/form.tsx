import clsx from "clsx";
import { ReactNode } from "react";
import {
  createBlockComponent,
  type IChildlessBlockProps,
} from "#components/meta";
import { FormSection } from "#components/forms";
import { ButtonReset, ButtonSubmit } from "#components/buttons";

import styles from "./form.module.scss";

interface IBaseProps
  extends Omit<IChildlessBlockProps<"div">, "onSubmit" | "onReset">,
    Pick<IChildlessBlockProps<"form">, "onSubmit" | "onReset"> {}

export interface IFormHTMLProps extends IBaseProps {
  /**
   * ID of the root element.
   */
  id: string;
  isNested?: boolean;

  /**
   * Configure the submit button.
   * If `null` is passed, no button will be rendered.
   */
  submitButton?: null | string;
  /**
   * Configure the reset button.
   * If `null` is passed, no button will be rendered.
   */
  resetButton?: null | string;
  children?: (formID: string) => ReactNode;
}

/**
 * A baseline form component.
 * Must be server-side friendly,
 * therefore has no state and handlers are optional.
 */
export const FormHTML = createBlockComponent(styles.block, Component);

function Component({
  id,
  isNested,
  submitButton,
  resetButton,
  onSubmit,
  onReset,
  className,
  children,
  ...blockProps
}: IFormHTMLProps) {
  const finalClassname = clsx(className, isNested && styles.nested);

  return (
    <div className={finalClassname} {...blockProps}>
      {children?.(id)}

      {submitButton === null && resetButton === null ? undefined : (
        <FormSection className={styles.buttons}>
          {submitButton !== null && (
            <ButtonSubmit form={id} visualType={isNested ? "button" : "submit"}>
              {submitButton}
            </ButtonSubmit>
          )}

          {resetButton !== null && (
            <ButtonReset form={id}>{resetButton}</ButtonReset>
          )}
        </FormSection>
      )}

      <form
        id={id}
        className={styles.form}
        onSubmit={onSubmit}
        onReset={onReset}
      />
    </div>
  );
}
