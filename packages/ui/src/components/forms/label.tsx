import { createBlockComponent, type IBlockProps } from "#components/meta";

import styles from "./label.module.scss";

export interface ILabelProps extends IBlockProps<"label"> {
  htmlFor: string;
}

export const Label = createBlockComponent(styles.block, Component);

function Component({ children, ...blockProps }: ILabelProps) {
  return <label {...blockProps}>{children}</label>;
}
