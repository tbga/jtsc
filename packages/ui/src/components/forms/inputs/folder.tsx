import { createBlockComponent } from "#components/meta";
import { type IInputFileProps, InputFile } from "./file";

export interface IInputFolderProps extends Omit<IInputFileProps, "accept"> {}

export const InputFolder = createBlockComponent(undefined, Component);

function Component({ ...blockProps }: IInputFolderProps) {
  return (
    <InputFile
      {...blockProps}
      // @ts-expect-error typing autism
      webkitdirectory="true"
    />
  );
}
