import { forwardRef, type Ref } from "react";
import type { IRequiredSome } from "@repo/core/types";
import {
  createBlockComponent,
  type IChildlessBlockProps,
} from "#components/meta";

import styles from "./base.module.scss";

export interface IInputBaseProps
  extends IRequiredSome<
    IChildlessBlockProps<"input">,
    "id" | "name" | "form"
  > {}

export const InputBase = forwardRef<HTMLInputElement, IInputBaseProps>(
  createBlockComponent(styles.block, Component)
);

function Component(
  { ...blockProps }: IInputBaseProps,
  ref?: Ref<HTMLInputElement>
) {
  return <input {...blockProps} ref={ref} />;
}
