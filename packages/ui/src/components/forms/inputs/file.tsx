import { createBlockComponent } from "#components/meta";
import { type IInputBaseProps, InputBase } from "./base";

export interface IInputFileProps
  extends Omit<IInputBaseProps, "type" | "accept"> {
  accept?: readonly string[];
}

export const InputFile = createBlockComponent(undefined, Component);

function Component({ accept, ...blockProps }: IInputFileProps) {
  return <InputBase accept={accept?.join(",")} {...blockProps} type="file" />;
}
