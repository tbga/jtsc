import { createBlockComponent, type IBlockProps } from "#components/meta";

import styles from "./fieldset.module.scss";

export interface IFieldsetProps extends IBlockProps<"fieldset"> {}
export interface ILegendProps extends IBlockProps<"legend"> {}

export const Fieldset = createBlockComponent(styles.block, FieldsetComponent);
export const Legend = createBlockComponent(styles.legend, LegendComponent);

function FieldsetComponent({ children, ...blockProps }: IFieldsetProps) {
  return <fieldset {...blockProps}>{children}</fieldset>;
}

function LegendComponent({ children, ...blockProps }: ILegendProps) {
  return <legend {...blockProps}>{children}</legend>;
}
