import { ReactNode } from "react";
import {
  createBlockComponent,
  type IBlockProps,
  IChildlessBlockProps,
} from "#components/meta";
import { type IHeadingLevel } from "#components/headings";

import styles from "./article.module.scss";

export interface IArticleProps extends IChildlessBlockProps<"article"> {
  headingLevel: IHeadingLevel;
  children?: (headingLevel: IHeadingLevel) => ReactNode;
}
export interface IArticleHeaderProps extends IBlockProps<"header"> {}
export interface IArticleBodyProps extends IBlockProps<"section"> {}
export interface IArticleFooterProps extends IBlockProps<"footer"> {}

export const Article = createBlockComponent(styles.block, ArticleComponent);
export const ArticleHeader = createBlockComponent(
  styles.header,
  ArticleHeaderComponent
);
export const ArticleBody = createBlockComponent(
  styles.body,
  ArticleBodyComponent
);
export const ArticleFooter = createBlockComponent(
  styles.footer,
  ArticleFooterComponent
);

function ArticleComponent({
  headingLevel,
  children,
  ...blockProps
}: IArticleProps) {
  return <article {...blockProps}>{children?.(headingLevel)}</article>;
}
function ArticleHeaderComponent({
  children,
  ...blockProps
}: IArticleHeaderProps) {
  return <header {...blockProps}>{children}</header>;
}
function ArticleBodyComponent({ children, ...blockProps }: IArticleBodyProps) {
  return <section {...blockProps}>{children}</section>;
}
function ArticleFooterComponent({
  children,
  ...blockProps
}: IArticleFooterProps) {
  return <footer {...blockProps}>{children}</footer>;
}
