import clsx from "clsx";
import { createBlockComponent } from "#components/meta";
import { type IButtonHTMLProps, ButtonHTML } from "./base";
import { type IVisualType } from "./types";

export interface IButtonSubmitProps
  extends Omit<IButtonHTMLProps, "type" | "form" | "visualType">,
    Pick<Required<IButtonHTMLProps>, "form"> {
  visualType?: Exclude<IVisualType, "reset" | "negative" | "positive">;
}

export const ButtonSubmit = createBlockComponent(undefined, Component);
function Component({
  visualType = "submit",
  className,
  children,
  ...blockProps
}: IButtonSubmitProps) {
  const blockClass = clsx(className);

  return (
    <ButtonHTML
      className={blockClass}
      {...blockProps}
      type="submit"
      visualType={visualType}
    >
      {children ?? "Submit"}
    </ButtonHTML>
  );
}
