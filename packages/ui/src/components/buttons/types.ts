import { type MouseEvent as IReactMouseEvent } from "react";

export type IVisualType =
  | "button"
  | "submit"
  | "reset"
  | "negative"
  | "positive";
export interface IClickEvent
  extends IReactMouseEvent<HTMLButtonElement, MouseEvent> {}
