import { createBlockComponent } from "#components/meta";
import { type IButtonHTMLProps, ButtonHTML } from "./base";

interface IBaseProps
  extends Omit<IButtonHTMLProps, "type" | "form">,
    Pick<Required<IButtonHTMLProps>, "form"> {}

export interface IButtonResetProps extends IBaseProps {}

export const ButtonReset = createBlockComponent(undefined, Component);

function Component({ children, ...blockProps }: IButtonResetProps) {
  return (
    <ButtonHTML {...blockProps} type="reset">
      {children ?? "Reset"}
    </ButtonHTML>
  );
}
