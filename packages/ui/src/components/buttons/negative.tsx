import { createBlockComponent } from "#components/meta";
import { Button, type IButtonProps } from "./button";

interface IButtonNegativeProps extends Omit<IButtonProps, "visualType"> {}

export const ButtonNegative = createBlockComponent(undefined, Component);

function Component({ ...blockProps }: IButtonNegativeProps) {
  return <Button {...blockProps} visualType="negative" />;
}
