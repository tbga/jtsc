// this file is for extra type declarations
declare module '*.scss' {
  const classes: { [key: string]: string };
  export = classes;
}
