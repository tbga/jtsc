# JTSC Backend

Create new migration:
```sh
npx node-pg-migrate create <migration-name> --migrations-dir ./src/database/migrations --migration-filename-format utc
```


## Dependencies
`typescript` is locked to `4.8.2` because at `4.8.3` the default exports are broken.
