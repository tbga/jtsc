import type { IEntityItem } from "#lib/entities";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectNameCount,
  selectNameIDs,
  selectNameItems,
} from "#database/queries/names";
import {
  deleteProfileNames,
  insertProfileNames,
  type IProfileNameDBInit,
  type IProfileNameDBDelete,
} from "#database/queries/profiles";
import type { IName, INameInit } from "#entities/name";
import { EMPTY_OBJECT, type IBigIntegerPositive } from "#types";
import type { IProfile, IProfileArgs } from "./types.js";

interface ICreateProfileNamesArgs extends IProfileArgs {
  inits: INameInit[];
}

export const createProfileNames = dbQuery<
  ICreateProfileNamesArgs,
  IEntityItem[]
>(async ({ profile_id, inits }, ctx) => {
  const dbInits = inits.map<IProfileNameDBInit>((name_init) => {
    return {
      profile_id,
      name_init,
    };
  });

  const { names: name_ids } = await insertProfileNames(dbInits, ctx);

  const names = await selectNameItems(name_ids, ctx);

  return names;
});

export const countProfileNames = dbQuery<IProfile["id"], IBigIntegerPositive>(
  async (profile_id, ctx) => {
    return selectNameCount({ profile_ids: [profile_id] }, ctx);
  },
);

interface IGetProfileNamesArgs extends IProfileArgs, IPaginatedQueryArgs {}

export const getProfileNames = dbQuery<IGetProfileNamesArgs, IEntityItem[]>(
  async ({ profile_id, pagination }, ctx) => {
    const ids = await selectNameIDs(
      { profile_ids: [profile_id], pagination },
      ctx,
    );
    const items = await selectNameItems(ids, ctx);

    return items;
  },
);

interface IRemoveProfileNamesArgs extends IProfileArgs {
  name_ids: IName["id"][];
}

export const removeProfileNames = dbQuery<
  IRemoveProfileNamesArgs,
  IName["id"][]
>(async ({ profile_id, name_ids }, ctx) => {
  const dbDeletes = name_ids.map<IProfileNameDBDelete>((name_id) => {
    return {
      profile_id,
      name_id,
    };
  });
  const { names } = await deleteProfileNames(dbDeletes, ctx);

  return names;
});
