import { Pagination } from "#lib/pagination";
import { dbQuery } from "#database";
import { createReleases, type IReleaseInit } from "#entities/release";
import type { IBigIntegerPositive } from "#types";
import type { IProfile } from "./types.js";
import {
  selectReleaseCount,
  selectReleaseIDs,
  selectReleasePreviews,
} from "#database/queries/releases";
import { validateClientPageNumber } from "#middleware";

interface IAddProfileReleasesArgs {
  profile_id: IProfile["id"];
  release_inits: IReleaseInit[];
}

export const addProfileReleases = dbQuery(
  async ({ profile_id, release_inits }: IAddProfileReleasesArgs, ctx) => {
    const profileReleaseInits = release_inits.map<IReleaseInit>((init) => {
      return { ...init, profile_id };
    });

    return createReleases(profileReleaseInits, ctx);
  },
);

export const countProfileReleases = dbQuery(
  async (profile_id: IProfile["id"], ctx) => {
    const count = await selectReleaseCount({ profile_ids: [profile_id] }, ctx);
    return count;
  },
);

interface IGetProfileReleasesArgs {
  profile_id: IProfile["id"];
  page: IBigIntegerPositive;
}

export const getProfileReleases = dbQuery(
  async ({ profile_id, page }: IGetProfileReleasesArgs, ctx) => {
    const count = await countProfileReleases(profile_id, ctx);
    const pagination = new Pagination(count, page);

    validateClientPageNumber(page, pagination.total_pages);

    const ids = await selectReleaseIDs(
      { profile_ids: [profile_id], pagination },
      ctx,
    );
    const releases = await selectReleasePreviews(ids, ctx);

    return {
      pagination,
      releases,
    };
  },
);
