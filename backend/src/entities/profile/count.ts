import { dbQuery } from "#database";
import { selectProfileCount } from "#database/queries/profiles";
import type { ISearchQueryArgs } from "#lib/entities";
import type { IArtist } from "#entities/artist";
import type { ISite } from "#entities/site";
import type { IBigIntegerPositive } from "#types";

interface ICountProfilesArgs extends ISearchQueryArgs {}

export const countProfiles = dbQuery<ICountProfilesArgs, IBigIntegerPositive>(
  async ({ search_query }, ctx) => {
    const count = await selectProfileCount({ search_query }, ctx);

    return count;
  },
);

interface ICountArtistProfiles extends ISearchQueryArgs {
  artist_id: IArtist["id"];
}

export const countArtistProfiles = dbQuery<
  ICountArtistProfiles,
  IBigIntegerPositive
>(async ({ artist_id, search_query }, ctx) => {
  const count = await selectProfileCount(
    { artist_ids: [artist_id], search_query },
    ctx,
  );

  return count;
});

interface ICountSiteProfiles extends ISearchQueryArgs {
  site_id: ISite["id"];
}

export const countSiteProfiles = dbQuery<
  ICountSiteProfiles,
  IBigIntegerPositive
>(async ({ site_id, search_query }, ctx) => {
  const count = await selectProfileCount(
    { site_ids: [site_id], search_query },
    ctx,
  );

  return count;
});
