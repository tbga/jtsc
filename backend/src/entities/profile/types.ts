import type { IProfile } from "#codegen/schema/lib/entities/profile/entity";
import type { IProfileInit } from "#codegen/schema/lib/entities/profile/init";

export type { IProfile } from "#codegen/schema/lib/entities/profile/entity";
export type { IProfileInit } from "#codegen/schema/lib/entities/profile/init";
export type { IProfilePreview } from "#codegen/schema/lib/entities/profile/preview";
export type { IProfileUpdate } from "#codegen/schema/lib/entities/profile/update";
export type { IProfileName } from "#codegen/schema/lib/entities/profile/name/entity";
export type { IProfileNameInit } from "#codegen/schema/lib/entities/profile/name/init";

export interface IProfileArgs {
  profile_id: IProfile["id"];
}

export interface IProfileInitUnknown
  extends Omit<IProfileInit, "artists" | "site_id"> {}
