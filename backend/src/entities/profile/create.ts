import type { IEntityItem } from "#lib/entities";
import { dbQuery } from "#database";
import {
  insertProfiles,
  selectProfileItems,
  type IProfileDBInit,
} from "#database/queries/profiles";
import type { IProfileInit } from "./types.js";

export const createProfiles = dbQuery<IProfileInit[], IEntityItem[]>(
  async (inits, ctx) => {
    const dbInits = await toProfileDBInits(inits);
    const ids = await insertProfiles(dbInits, ctx);
    const newProfiles = await selectProfileItems(ids, ctx);

    return newProfiles;
  },
);

export async function toProfileDBInits(
  inits: IProfileInit[],
): Promise<IProfileDBInit[]> {
  const dbInits: IProfileDBInit[] = [];

  for await (const { artists, releases, ...init } of inits) {
    const dbInit: IProfileDBInit = {
      ...init,
      release_ids: releases?.ids,
      artist_ids: artists?.ids,
      artist_inits: artists?.inits,
    };

    dbInits.push(dbInit);
  }

  return dbInits;
}
