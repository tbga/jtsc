import { dbQuery } from "#database";
import {
  selectProfileCount,
  selectProfileIDs,
  selectProfilePreviews,
} from "#database/queries/profiles";
import type { IProfilePreview } from "#entities/profile";
import type { ISite } from "#entities/site";
import type { ISearchQueryArgs } from "#lib/entities";
import type { IPaginationDB } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";

interface IArgs extends ISearchQueryArgs {
  pagination: IPaginationDB;
  site_id?: ISite["id"];
}
export const searchProfilesCount = dbQuery<
  Omit<IArgs, "pagination">,
  IBigIntegerPositive
>(async ({ search_query, site_id }, ctx) => {
  const site_ids = site_id ? [site_id] : undefined;
  const count = await selectProfileCount({ search_query, site_ids }, ctx);

  return count;
});

export const searchProfiles = dbQuery<IArgs, IProfilePreview[]>(
  async ({ pagination, search_query, site_id }, ctx) => {
    const site_ids = site_id ? [site_id] : undefined;
    const ids = await selectProfileIDs(
      { pagination, search_query, site_ids },
      ctx,
    );
    const profiles = await selectProfilePreviews(ids, ctx);

    return profiles;
  },
);
