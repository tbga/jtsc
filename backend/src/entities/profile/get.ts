import { NotFoundError } from "#lib/server";
import type { IPaginationDB } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectProfileEntities,
  selectProfileIDs,
  selectProfilePreviews,
} from "#database/queries/profiles";
import type { IProfile, IProfilePreview } from "./types.js";

export const getProfile = dbQuery<IProfile["id"], IProfile>(
  async (profile_id, ctx) => {
    const result = await selectProfileEntities([profile_id], ctx);

    if (!result.length) {
      throw new NotFoundError(`Profile with id "${profile_id}" doesn't exist.`);
    }

    return result[0];
  },
);

export const getProfiles = dbQuery<IPaginationDB, IProfilePreview[]>(
  async (pagination, ctx) => {
    const ids = await selectProfileIDs({ pagination }, ctx);
    const previews = await selectProfilePreviews(ids, ctx);

    return previews;
  },
);
