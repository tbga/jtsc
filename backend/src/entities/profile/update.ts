import { dbQuery } from "#database";
import {
  type IProfileDBUpdate,
  updateProfiles,
} from "#database/queries/profiles";
import { getProfile } from "#entities/profile";
import type { IProfileUpdate, IProfile, IProfileArgs } from "./types.js";

interface IArgs extends IProfileArgs {
  update: IProfileUpdate;
}

export const updateProfile = dbQuery<IArgs, IProfile>(
  async ({ profile_id, update }, ctx) => {
    const dbUpdate: IProfileDBUpdate = {
      id: profile_id,
      ...update,
    };

    const [id] = await updateProfiles([dbUpdate], ctx);
    const profile = await getProfile(id, ctx);

    return profile;
  },
);
