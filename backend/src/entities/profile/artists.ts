import { dbQuery } from "#database";
import {
  selectArtistCount,
  selectArtistIDs,
  selectArtistItems,
  selectArtistPreviews,
} from "#database/queries/artists";
import {
  deleteProfileArtists,
  insertProfileArtists,
} from "#database/queries/profiles";
import type { IArtist, IArtistPreview } from "#entities/artist";
import type { IEntityItem } from "#lib/entities";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import type { IArrayElement, IBigIntegerPositive } from "#types";
import type { IProfile } from "./types.js";

interface IArgs extends IPaginatedQueryArgs {
  profile_id: IProfile["id"];
}

interface IEditArgs {
  profile_id: IProfile["id"];
  artist_ids: IArtist["id"][];
}

export const addProfileArtists = dbQuery<IEditArgs, IEntityItem[]>(
  async ({ profile_id, artist_ids }, ctx) => {
    const inits = artist_ids.map<
      IArrayElement<Parameters<typeof insertProfileArtists>["0"]>
    >((artist_id) => {
      return {
        profile_id,
        artist_id,
      };
    });
    const { artists } = await insertProfileArtists(inits, ctx);
    const artistIDs = artists.map(({ id }) => id);

    const items = await selectArtistItems(artistIDs, ctx);

    return items;
  },
);

export const countProfileArtists = dbQuery<IProfile["id"], IBigIntegerPositive>(
  async (profile_id, ctx) => {
    const count = await selectArtistCount({ profile_ids: [profile_id] }, ctx);

    return count;
  },
);

export const getProfileArtistPreviews = dbQuery<IArgs, IArtistPreview[]>(
  async ({ profile_id, pagination }, ctx) => {
    const ids = await selectArtistIDs(
      { profile_ids: [profile_id], pagination },
      ctx,
    );
    const previews = await selectArtistPreviews(ids, ctx);

    return previews;
  },
);

export const getProfileArtistItems = dbQuery<IArgs, IEntityItem[]>(
  async ({ profile_id, pagination }, ctx) => {
    const ids = await selectArtistIDs(
      { profile_ids: [profile_id], pagination },
      ctx,
    );
    const items = await selectArtistItems(ids, ctx);

    return items;
  },
);

export const removeProfileArtists = dbQuery<IEditArgs, IEntityItem[]>(
  async ({ profile_id, artist_ids }, ctx) => {
    const deletes = artist_ids.map<
      IArrayElement<Parameters<typeof deleteProfileArtists>["0"]>
    >((artist_id) => {
      return {
        profile_id,
        artist_id,
      };
    });

    const { artists } = await deleteProfileArtists(deletes, ctx);
    const artistIDs = artists.map(({ id }) => id);
    const items = await selectArtistItems(artistIDs, ctx);

    return items;
  },
);
