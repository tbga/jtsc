import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  type IPublicExportSiteRelation,
  insertPublicExportSites,
  deletePublicExportSites,
} from "#database/queries/public-exports";
import {
  selectSiteCount,
  selectSiteIDs,
  selectSitePreviews,
} from "#database/queries/sites";
import type { ISitePreview, ISite } from "#entities/site";
import type { IBigIntegerPositive } from "#types";
import type { IPublicExportArgs, IPublicExport } from "./types.js";

interface IPublicExportSitesArgs extends IPublicExportArgs {
  site_ids: ISite["id"][];
}

export const addPublicExportSites = dbQuery<
  IPublicExportSitesArgs,
  ISitePreview[]
>(async ({ public_export_id, site_ids }, ctx) => {
  const relations = site_ids.map<IPublicExportSiteRelation>((site_id) => {
    return {
      site_id,
      public_export_id,
    };
  });
  const { sites: siteIDs } = await insertPublicExportSites(relations, ctx);
  const sites = await selectSitePreviews(siteIDs, ctx);

  return sites;
});

export const countPublicExportSites = dbQuery<
  IPublicExport["id"],
  IBigIntegerPositive
>(async (public_export_id, ctx) => {
  const count = await selectSiteCount(
    {
      public_export_ids: [public_export_id],
    },
    ctx,
  );

  return count;
});

export const getPublicExportSites = dbQuery<
  IPublicExportArgs & IPaginatedQueryArgs,
  ISitePreview[]
>(async ({ public_export_id, pagination }, ctx) => {
  const siteIDs = await selectSiteIDs(
    { public_export_ids: [public_export_id], pagination },
    ctx,
  );

  const sites = await selectSitePreviews(siteIDs, ctx);

  return sites;
});

export const removePublicExportSites = dbQuery<
  IPublicExportSitesArgs,
  ISite["id"][]
>(async ({ public_export_id, site_ids }, ctx) => {
  const relations = site_ids.map<IPublicExportSiteRelation>((site_id) => {
    return {
      site_id,
      public_export_id,
    };
  });
  const { sites: siteIDs } = await deletePublicExportSites(relations, ctx);

  return siteIDs;
});
