import { dbQuery } from "#database";
import {
  updatePublicExports,
  type IPublicExportDBUpdate,
  selectPublicExportEntities,
} from "#database/queries/public-exports";
import { ProjectError } from "#lib/errors";

import type { IPublicExportUpdate, IPublicExport } from "./types.js";

interface IArgs {
  public_export_id: IPublicExport["id"];
  update: IPublicExportUpdate & Pick<Partial<IPublicExport>, "status">;
}

export const editPublicExport = dbQuery<IArgs, IPublicExport>(
  async ({ public_export_id, update }, ctx) => {
    const dbUpdate: IPublicExportDBUpdate = { ...update, id: public_export_id };
    const ids = await updatePublicExports([dbUpdate], ctx);
    const results = await selectPublicExportEntities(ids, ctx);

    if (results.length !== 1) {
      throw new ProjectError(
        `Expected 1 public update result for public export ${public_export_id} but returned ${ids.length}`,
      );
    }

    return results[0];
  },
);
