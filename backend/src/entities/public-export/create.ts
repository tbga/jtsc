import { dbQuery } from "#database";
import type { IEntityItem } from "#lib/entities";
import {
  type IPublicExportDBInit,
  insertPublicExports,
  selectPublicExportItems,
} from "#database/queries/public-exports";
import type { IAccountDB } from "#database/queries/accounts";
import type { IPublicExportInit } from "./types.js";

interface IArgs extends IPublicExportInit {
  account_id: IAccountDB["id"];
}

export const createPublicExport = dbQuery<IArgs, IEntityItem>(
  async ({ title, sites, account_id }, ctx) => {
    const dbInit: IPublicExportDBInit = {
      title,
      sites,
      created_by: account_id,
    };
    const publicExportIDs = await insertPublicExports([dbInit], ctx);
    const [publicExport] = await selectPublicExportItems(publicExportIDs, ctx);

    return publicExport;
  },
);
