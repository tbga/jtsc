import type { IPublicExport } from "#codegen/schema/lib/entities/public-export/entity";

export type { IPublicExport } from "#codegen/schema/lib/entities/public-export/entity";
export type { IPublicExportInit } from "#codegen/schema/lib/entities/public-export/init";
export type { IPublicExportPreview } from "#codegen/schema/lib/entities/public-export/preview";
export type { IPublicExportStatus } from "#codegen/schema/lib/entities/public-export/status";
export type { IPublicExportUpdate } from "#codegen/schema/lib/entities/public-export/update";

export interface IPublicExportArgs {
  public_export_id: IPublicExport["id"];
}
