import { dbQuery } from "#database";
import {
  selectPublicExportCount,
  selectPublicExportEntities,
  selectPublicExportIDs,
  selectPublicExportPreviews,
} from "#database/queries/public-exports";
import type { IBigIntegerPositive } from "#types";
import type { IPublicExportPreview, IPublicExport } from "./types.js";

export const countPublicExports = dbQuery<
  Parameters<typeof selectPublicExportCount>[0],
  IBigIntegerPositive
>(async (args, ctx) => {
  const count = await selectPublicExportCount(args, ctx);

  return count;
});

interface IGetPublicExportArgs {
  public_export_id: IPublicExport["id"];
}

export const getPublicExport = dbQuery<IGetPublicExportArgs, IPublicExport>(
  async ({ public_export_id }, ctx) => {
    const [publicExport] = await selectPublicExportEntities(
      [public_export_id],
      ctx,
    );

    return publicExport;
  },
);

export const getPublicExports = dbQuery<
  Parameters<typeof selectPublicExportIDs>[0],
  IPublicExportPreview[]
>(async (args, ctx) => {
  const ids = await selectPublicExportIDs(args, ctx);
  const previews = await selectPublicExportPreviews(ids, ctx);

  return previews;
});
