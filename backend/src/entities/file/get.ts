import { dbQuery } from "#database";
import type { IPaginationDB } from "#lib/pagination";
import { NotFoundError } from "#lib/server";
import {
  selectFileIDs,
  selectFileEntities,
  selectFilePreviews,
} from "#database/queries/files";
import type { IFile, IFilePreview } from "./types.js";

export const getFile = dbQuery<IFile["id"], IFile>(async (file_id, ctx) => {
  const result = await selectFileEntities([file_id], ctx);

  if (!result.length) {
    throw new NotFoundError(`Profile with id "${file_id}" doesn't exist.`);
  }

  return result[0];
});

export const getFiles = dbQuery<IPaginationDB, IFilePreview[]>(
  async (pagination, ctx) => {
    const ids = await selectFileIDs({ pagination }, ctx);
    const previews = await selectFilePreviews(ids, ctx);
    return previews;
  },
);
