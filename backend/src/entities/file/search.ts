import { dbQuery } from "#database";
import {
  selectFileIDs,
  selectFilePreviews,
  selectFileItems,
} from "#database/queries/files";
import type { IEntityItem, IPaginatedSearchQueryArgs } from "#lib/entities";
import type { IFilePreview } from "./types.js";

export const searchFiles = dbQuery<IPaginatedSearchQueryArgs, IFilePreview[]>(
  async ({ pagination, search_query }, ctx) => {
    const ids = await selectFileIDs({ search_query, pagination }, ctx);
    const previews = await selectFilePreviews(ids, ctx);

    return previews;
  },
);

export const searchFileItems = dbQuery<
  IPaginatedSearchQueryArgs,
  IEntityItem[]
>(async ({ pagination, search_query }, ctx) => {
  const ids = await selectFileIDs({ search_query, pagination }, ctx);
  const items = await selectFileItems(ids, ctx);

  return items;
});
