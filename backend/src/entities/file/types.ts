export type { IFile } from "#codegen/schema/lib/entities/file/entity";
export type { IFileInit } from "#codegen/schema/lib/entities/file/init";
export type { IFilePreview } from "#codegen/schema/lib/entities/file/preview";
export type { IFileUpdate } from "#codegen/schema/lib/entities/file/update";
export type { IFileHashes } from "#codegen/schema/lib/entities/file/hashes";
