import type { IPaginationDB } from "#lib/pagination";
import {
  selectProfilePreviews,
  selectProfileCount,
  selectProfileIDs,
} from "#database/queries/profiles";
import { dbQuery } from "#database";
import { type IPublishInit, publishEntities } from "#database/queries/entities";
import { selectSiteEntities, sitesTable } from "#database/queries/sites";
import type { IAccountArgs } from "#entities/account";
import type { IProfilePreview } from "#entities/profile";
import type { IBigIntegerPositive } from "#types";
import type { ISite, ISiteArgs } from "./types.js";

export const getSiteProfilesPagination = dbQuery<
  ISite["id"],
  IBigIntegerPositive
>(async (site_id, ctx) => {
  return selectProfileCount({ site_ids: [site_id] }, ctx);
});

export const getSiteProfiles = dbQuery<
  ISiteArgs & { pagination: IPaginationDB },
  IProfilePreview[]
>(async ({ site_id, pagination }, ctx) => {
  const ids = await selectProfileIDs({ site_ids: [site_id], pagination }, ctx);
  const profiles = await selectProfilePreviews(ids, ctx);

  return profiles;
});

interface IPublishSiteArgs extends IAccountArgs, ISiteArgs {}

export const publishSite = dbQuery<IPublishSiteArgs, ISite>(
  async ({ account_id, site_id }, ctx) => {
    const [site] = await publishSites({ account_id, ids: [site_id] }, ctx);

    return site;
  },
);

interface IPublishSitesArgs extends IAccountArgs {
  ids: ISite["id"][];
}

export const publishSites = dbQuery<IPublishSitesArgs, ISite[]>(
  async ({ account_id, ids }, ctx) => {
    const init: IPublishInit = {
      account_id,
      entity_ids: ids,
    };
    const site_ids = await publishEntities(
      { inits: [init], table: sitesTable },
      ctx,
    );
    const sites = await selectSiteEntities(site_ids, ctx);

    return sites;
  },
);
