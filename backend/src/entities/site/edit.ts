import { dbQuery } from "#database";
import { type ISiteDBUpdate, updateSites } from "#database/queries/sites";
import { getSite } from "#entities/site";
import type { ISite, ISiteUpdate } from "./types.js";

interface IArgs {
  id: ISite["id"];
  update: ISiteUpdate;
}

export const editSite = dbQuery<IArgs, ISite>(async ({ id, update }, ctx) => {
  const dbUpdate = toSiteDBUpdate(update, id);
  const [updatedID] = await updateSites([dbUpdate], ctx);
  const updatedSite = await getSite(updatedID, ctx);

  return updatedSite;
});

function toSiteDBUpdate(update: ISiteUpdate, id: ISite["id"]): ISiteDBUpdate {
  const { url_templates, ...updateFields } = update;
  const dbUpdate: ISiteDBUpdate = {
    ...updateFields,
    url_template_profile_list: url_templates?.profile_list,
    url_template_profile_list_notes: url_templates?.profile_list_notes,
    url_template_profile: url_templates?.profile,
    url_template_profile_notes: url_templates?.profile_notes,
    url_template_release_list: url_templates?.release_list,
    url_template_release_list_notes: url_templates?.release_list_notes,
    url_template_release: url_templates?.release,
    url_template_release_notes: url_templates?.release_notes,
    id,
  };

  return dbUpdate;
}
