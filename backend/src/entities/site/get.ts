import { ProjectError } from "#lib/errors";
import type { IPaginationDB } from "#lib/pagination";
import { NotFoundError } from "#lib/server";
import { dbQuery } from "#database";
import {
  selectSiteCount,
  selectSiteEntities,
  selectSiteIDs,
  selectSitePreviews,
} from "#database/queries/sites";
import { EMPTY_OBJECT, type IBigIntegerPositive } from "#types";
import type { ISitePreview, ISite } from "./types.js";

export const countSites = dbQuery<undefined, IBigIntegerPositive>(
  async (_, ctx) => {
    const count = await selectSiteCount(EMPTY_OBJECT, ctx);

    return count;
  },
);

export const getSites = dbQuery<IPaginationDB, ISitePreview[]>(
  async (pagination, ctx) => {
    const ids = await selectSiteIDs({ pagination }, ctx);
    const sites = await selectSitePreviews(ids, ctx);

    return sites;
  },
);

export const getSite = dbQuery<ISite["id"], ISite>(async (site_id, ctx) => {
  const result = await selectSiteEntities([site_id], ctx);

  if (!result.length) {
    throw new NotFoundError(`Site with id "${site_id}" doesn't exist.`);
  }

  if (result.length > 1) {
    throw new ProjectError(
      `Expected to return one site but got ${result.length}.`,
    );
  }

  return result[0];
});
