import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectReleaseCount,
  selectReleaseIDs,
  selectReleasePreviews,
} from "#database/queries/releases";
import type { IReleasePreview } from "#entities/release";
import type { IBigIntegerPositive } from "#types";
import type { ISiteArgs, ISite } from "./types.js";

export const countSiteReleases = dbQuery<ISite["id"], IBigIntegerPositive>(
  async (site_id, ctx) => {
    const count = await selectReleaseCount({ site_ids: [site_id] }, ctx);

    return count;
  },
);
export const getSiteReleases = dbQuery<
  ISiteArgs & IPaginatedQueryArgs,
  IReleasePreview[]
>(async ({ site_id, pagination }, ctx) => {
  const ids = await selectReleaseIDs({ site_ids: [site_id], pagination }, ctx);
  const releases = await selectReleasePreviews(ids, ctx);

  return releases;
});
