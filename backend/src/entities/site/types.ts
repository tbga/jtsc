import type { ISite } from "#codegen/schema/lib/entities/site/entity";
import type { IRequiredSome } from "#types";

export type { ISite } from "#codegen/schema/lib/entities/site/entity";
export type { ISiteInit } from "#codegen/schema/lib/entities/site/init";
export type { ISitePreview } from "#codegen/schema/lib/entities/site/preview";
export type { ISiteUpdate } from "#codegen/schema/lib/entities/site/update";
export type { ISiteURLTemplates } from "#codegen/schema/lib/entities/site/url-templates";

export interface ISiteArgs {
  site_id: ISite["id"];
}

export interface ISitePublished extends IRequiredSome<ISite, "public_id"> {}
