import { dbQuery } from "#database";
import {
  selectSiteCount,
  selectSiteIDs,
  selectSiteItems,
  selectSitePreviews,
} from "#database/queries/sites";
import type { ISearchQueryArgs, IEntityItem } from "#lib/entities";
import type { IPaginationDB } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";
import type { ISitePreview } from "./types.js";

interface ISearchArgs
  extends Pick<Required<ISearchQueryArgs>, "search_query"> {}
interface IPaginatedSearchArgs extends ISearchArgs {
  pagination: IPaginationDB;
}

export const countSiteSearch = dbQuery<ISearchArgs, IBigIntegerPositive>(
  async ({ search_query }, ctx) => {
    const count = await selectSiteCount({ search_query }, ctx);

    return count;
  },
);

export const searchSites = dbQuery<IPaginatedSearchArgs, ISitePreview[]>(
  async ({ pagination, search_query }, ctx) => {
    const ids = await selectSiteIDs({ search_query, pagination }, ctx);
    const previews = await selectSitePreviews(ids, ctx);

    return previews;
  },
);

export const searchSiteItems = dbQuery<IPaginatedSearchArgs, IEntityItem[]>(
  async ({ pagination, search_query }, ctx) => {
    const ids = await selectSiteIDs({ search_query, pagination }, ctx);
    const items = await selectSiteItems(ids, ctx);

    return items;
  },
);
