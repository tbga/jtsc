import { dbQuery } from "#database";
import {
  insertSites,
  type ISiteDBInit,
  selectSitePreviews,
} from "#database/queries/sites";
import type { ISiteInit, ISitePreview } from "./types.js";

export const createSites = dbQuery<ISiteInit[], ISitePreview[]>(
  async (inits, ctx) => {
    const dbInits = toSiteDBInits(inits);
    const site_ids = await insertSites(dbInits, ctx);
    const previews = await selectSitePreviews(site_ids, ctx);

    return previews;
  },
);

export function toSiteDBInits(inits: ISiteInit[]): ISiteDBInit[] {
  const dbInits = inits.map<ISiteDBInit>((init) => {
    const dbInit = toSiteInit(init);

    return dbInit;
  });

  return dbInits;
}

export function toSiteInit({ url_templates, ...init }: ISiteInit): ISiteDBInit {
  const dbInit: ISiteDBInit = {
    ...init,
    url_template_profile_list: url_templates?.profile_list,
    url_template_profile_list_notes: url_templates?.profile_list_notes,
    url_template_profile: url_templates?.profile,
    url_template_profile_notes: url_templates?.profile_notes,
    url_template_release_list: url_templates?.release_list,
    url_template_release_list_notes: url_templates?.release_list_notes,
    url_template_release: url_templates?.release,
    url_template_release_notes: url_templates?.release_notes,
  };

  return dbInit;
}
