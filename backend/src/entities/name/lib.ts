export {
  nameSchema,
  validateName,
} from "#codegen/schema/lib/entities/name/entity";
export {
  nameInitSchema,
  validateNameInit,
} from "#codegen/schema/lib/entities/name/init";
