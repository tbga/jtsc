export { validateName, validateNameInit } from "./lib.js";
export { searchNames } from "./search.js";
export type { IName, INameInit } from "./types.js";
