import type { IEntityItem } from "#lib/entities";
import type { IPaginationDB } from "#lib/pagination";
import { dbQuery } from "#database";
import { selectNameIDs, selectNameItems } from "#database/queries/names";

interface IArgs {
  query: string;
  pagination: IPaginationDB;
}

export const searchNames = dbQuery<IArgs, IEntityItem[]>(
  async ({ pagination, query }, ctx) => {
    const ids = await selectNameIDs({ pagination, search_query: query }, ctx);
    const items = await selectNameItems(ids, ctx);

    return items;
  },
);
