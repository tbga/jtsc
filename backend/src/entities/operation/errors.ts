import { ProjectError, type IErrorMessage } from "#lib/errors";
import type { IOperation } from "./types.js";

interface IOperationErrorOptions extends ErrorOptions {
  operation_id: IOperation["id"];
}

export class OperationError
  extends ProjectError
  implements IOperationErrorOptions
{
  operation_id;

  constructor(message: IErrorMessage, options: IOperationErrorOptions) {
    super(message, options);

    const { operation_id } = options;

    this.operation_id = operation_id;
  }
}
