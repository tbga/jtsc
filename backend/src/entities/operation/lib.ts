import type { IEntityItem } from "#lib/entities";
import { NotFoundError } from "#lib/server";
import { BIGINT_ZERO } from "#lib/numbers/bigint";
import { DatabaseError, dbQuery } from "#database";
import {
  insertOperations,
  selectOperationEntities,
  type IOperationDBInit,
  updateOperations,
  selectOperationItems,
  selectOperationIDs,
  selectOperationCount,
} from "#database/queries/operations";
import type { IAccountArgs } from "#entities/account";
import { OperationError } from "#entities/operation";
import type { IOperationArgs, IOperation } from "./types.js";

interface IArgs extends IAccountArgs, Pick<IOperation, "type" | "input_data"> {
  type: IOperation["type"];
}

export const createOperation = dbQuery<IArgs, IEntityItem>(
  async ({ account_id, type, input_data }, ctx) => {
    const operationInit: IOperationDBInit = {
      created_by: account_id,
      type,
      status: "pending",
      input_data,
    };

    const operation_ids = await insertOperations([operationInit], ctx);
    const items = await selectOperationItems(operation_ids, ctx);

    if (items.length > 1) {
      throw new DatabaseError(
        `\`get_one()\` query returned more than 1 result for operation id "${operation_ids[0]}".`,
      );
    }

    return items[0];
  },
);

export const getOperation = dbQuery<IOperation["id"], IOperation>(
  async (operaton_id, ctx) => {
    const result = await selectOperationEntities([operaton_id], ctx);

    if (!result.length) {
      throw new NotFoundError(`No operation exists with id "${operaton_id}".`);
    }

    if (result.length > 1) {
      throw new DatabaseError(
        `\`get_one()\` query returned more than 1 result for operation id "${operaton_id}".`,
      );
    }

    return result[0];
  },
);

const pagination = {
  limit: 1,
};

export const getNextPendingOperation = dbQuery<
  undefined,
  IOperation | undefined
>(async (_, ctx) => {
  const count = await selectOperationCount({ status: "pending" }, ctx);

  if (BigInt(count) === BIGINT_ZERO) {
    return;
  }

  const ids = await selectOperationIDs({ status: "pending", pagination }, ctx);
  const [nextOperation] = await selectOperationEntities(ids, ctx);

  return nextOperation;
});

interface IFinishOperationArgs extends IOperationArgs {
  results: Required<IOperation>["result_data"];
}

export const finishOperation = dbQuery<IFinishOperationArgs, IOperation>(
  async ({ operation_id, results }, ctx) => {
    const operationFinish = [
      {
        id: operation_id,
        status: "finished" as const,
        result_data: results,
      },
    ];
    const ids = await updateOperations(operationFinish, ctx);
    const updatedOperations = await selectOperationEntities(ids, ctx);

    return updatedOperations[0];
  },
);

interface IFailOperationArgs
  extends IOperationArgs,
    Pick<Required<IOperation>, "errors"> {}

export const failOperation = dbQuery<IFailOperationArgs, IOperation>(
  async ({ operation_id, errors }, ctx) => {
    const operationFail = [
      {
        id: operation_id,
        status: "failed" as const,
        errors,
      },
    ];
    const ids = await updateOperations(operationFail, ctx);
    const [finishedOperation] = await selectOperationEntities(ids, ctx);

    return finishedOperation;
  },
);

interface IRetryOperationArgs extends IAccountArgs, IOperationArgs {}

/**
 * Creates a new operation with the same type and initializer data
 * as the current one.
 */
export const retryOperation = dbQuery<IRetryOperationArgs, IEntityItem>(
  async ({ account_id, operation_id }, ctx) => {
    const [{ type, input_data }] = await selectOperationEntities(
      [operation_id],
      ctx,
    );
    const newOperation = await createOperation(
      { account_id: account_id, type, input_data },
      ctx,
    );

    return newOperation;
  },
);

/**
 * @TODO also cancel currently running operation in the runner
 */
export const cancelOperation = dbQuery<IOperation["id"], IOperation>(
  async (id, ctx) => {
    const operation = await getOperation(id, ctx);

    const status = operation.status;

    if (status !== "pending" && status !== "in-progress") {
      throw new OperationError(
        `Cannot cancel operation "${id}" because its status "${status}" is not "pending" or "in-progress".`,
        { operation_id: id },
      );
    }

    const errors = [
      String(
        new OperationError("Operation was canceled", { operation_id: id }),
      ),
    ];
    const failedOperation = await failOperation(
      { operation_id: id, errors },
      ctx,
    );

    return failedOperation;
  },
);
