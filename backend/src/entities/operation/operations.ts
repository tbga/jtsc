import { fileURLToPath } from "node:url";
import { log } from "#lib/logs";
import type { IEntityItem } from "#lib/entities";
import { type IDBContext, startTransaction } from "#database";
import { insertFiles } from "#database/queries/files";
import {
  type IPublicImportDBFinisher,
  type IPublicImportDBUpdate,
  finishPublicImports,
  selectPublicImportItems,
  updatePublicImports,
  selectPublicImportEntities,
} from "#database/queries/public-imports";
import { uploadFiles } from "#storage";
import { createReleases } from "#entities/release";
import { createProfiles } from "#entities/profile";
import { createPublicImport } from "#entities/public-import";
import { finishPublicExport } from "#public-exports";
import { failOperation, finishOperation } from "./lib.js";
import { OperationError } from "./errors.js";
import type {
  IOperation,
  IOperationLocalUpload,
  IOperationReleasesCreate,
  IOperationProfilesCreate,
  IOperationPublicExportsFinish,
  IOperationPublicImportsCreate,
  IOperationPublicImportsConsume,
} from "./types.js";

export async function resolveOperation(operation: IOperation) {
  try {
    await startTransaction(async (ctx) => {
      const results = await runOperation(operation, ctx);
      await finishOperation({ operation_id: operation.id, results }, ctx);
    });
  } catch (error) {
    log(`Failed operation "${operation.type}" (${operation.id}).`);
    // @TODO a better way to dump a complete log
    // since errors get serialized into `Error.message` without a stack.
    console.log(error);

    await startTransaction(async (ctx) => {
      await failOperation(
        { operation_id: operation.id, errors: [String(error)] },
        ctx,
      );
    });
  }
}

export async function runOperation(
  operation: IOperation,
  ctx: IDBContext,
): Promise<Required<IOperation>["result_data"]> {
  const { id, type, created_by: account_id } = operation;

  switch (type) {
    case "local_upload": {
      const { input_data } = operation as IOperationLocalUpload;
      const fileDBInits = await uploadFiles(input_data);
      const newFilesIDs = await insertFiles(fileDBInits, ctx);

      return newFilesIDs;
    }

    case "releases_create": {
      const { input_data } = operation as IOperationReleasesCreate;
      const newReleases = await createReleases(input_data, ctx);
      const newReleasesIDs = newReleases.map(({ id }) => id);

      return newReleasesIDs;
    }

    case "profiles_create": {
      const { input_data } = operation as IOperationProfilesCreate;
      const newReleases = await createProfiles(input_data, ctx);

      return newReleases;
    }

    case "public_exports_finish": {
      const { input_data: publicExportIDs } =
        operation as IOperationPublicExportsFinish;

      for await (const public_export_id of publicExportIDs) {
        await finishPublicExport(public_export_id, ctx);
      }

      return publicExportIDs;
    }

    case "public_imports_create": {
      const { input_data: publicImportURLs } =
        operation as IOperationPublicImportsCreate;
      const importItems: IEntityItem[] =
        [] satisfies IOperationPublicImportsCreate["result_data"];

      for await (const publicImportURL of publicImportURLs) {
        const importPath = fileURLToPath(publicImportURL);
        const item = await createPublicImport({ account_id, importPath }, ctx);

        importItems.push(item);
      }

      return importItems;
    }

    case "public_imports_consume": {
      const { input_data: publicImportIDs } =
        operation as IOperationPublicImportsConsume;

      const finishers = publicImportIDs.map<IPublicImportDBFinisher>((id) => {
        return { id, account_id };
      });

      try {
        const ids = await finishPublicImports(finishers, ctx);
        const importItems = await selectPublicImportItems(ids, ctx);

        return importItems;
      } catch (error) {
        // revert import status in case of errors
        try {
          const updates = publicImportIDs.map<IPublicImportDBUpdate>((id) => {
            return { id, status: "pending" };
          });
          await updatePublicImports(updates);
        } catch (error) {
          log(
            `Failed to recover from errors while consuming these public imports: ${publicImportIDs
              .map((id) => `"${id}"`)
              .join(", ")}.`,
          );
        }

        throw error;
      }
    }

    default: {
      throw new OperationError(
        `Unknown operation type "${type satisfies never}".`,
        {
          operation_id: id,
        },
      );
    }
  }
}
