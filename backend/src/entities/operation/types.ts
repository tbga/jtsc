import type { IOperation } from "#codegen/schema/lib/entities/operation/entity";
import { operationTypeSchema } from "#codegen/schema/lib/entities/operation/type";

export type { IOperation } from "#codegen/schema/lib/entities/operation/entity";
export type { IOperationPreview } from "#codegen/schema/lib/entities/operation/preview";
export type { IOperationType } from "#codegen/schema/lib/entities/operation/type";
export type { IOperationStatus } from "#codegen/schema/lib/entities/operation/status";
export type { IOperationLocalUpload } from "#codegen/schema/lib/entities/operation/file/local-upload";
export type { IOperationReleasesCreate } from "#codegen/schema/lib/entities/operation/release/create";
export type { IOperationProfilesCreate } from "#codegen/schema/lib/entities/operation/profile/create";
export type { IOperationPublicExportsFinish } from "#codegen/schema/lib/entities/operation/public-exports/finish";
export type { IOperationPublicImportsCreate } from "#codegen/schema/lib/entities/operation/public-imports/create";
export type { IOperationPublicImportsConsume } from "#codegen/schema/lib/entities/operation/public-imports/consume";

export interface IOperationArgs {
  operation_id: IOperation["id"];
}

export const RUNNER_STATUS = {
  RUNNING: "running",
  STOPPED: "stopped",
  PAUSED: "paused",
  MANUAL_STOPPED: "stopped_manual",
  MANUAL_PAUSED: "paused_manual",
} as const;

export const OPERATION_TYPES = operationTypeSchema.anyOf.map(
  (type) => type.const,
);
