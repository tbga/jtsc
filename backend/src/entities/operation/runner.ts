import { scheduler } from "node:timers/promises";
import { log } from "#lib/logs";
import { getNextPendingOperation } from "./lib.js";
import { resolveOperation } from "./operations.js";
import { RUNNER_STATUS } from "./types.js";
import { ProjectError } from "#lib/errors";

let status: (typeof RUNNER_STATUS)[keyof typeof RUNNER_STATUS] =
  RUNNER_STATUS.STOPPED;

launchRunner();

async function launchRunner() {
  while (true) {
    switch (status) {
      case RUNNER_STATUS.MANUAL_PAUSED:
      case RUNNER_STATUS.MANUAL_STOPPED:
      case RUNNER_STATUS.PAUSED:
      case RUNNER_STATUS.STOPPED: {
        await scheduler.wait(30_000);
        break;
      }

      case RUNNER_STATUS.RUNNING: {
        const nextOperation = await getNextPendingOperation(undefined);

        if (!nextOperation) {
          await scheduler.wait(30_000);
          break;
        }

        log(
          `Running operation "${nextOperation.type}" (${nextOperation.id})...`,
        );

        await resolveOperation(nextOperation);

        log(
          `Resolved operation "${nextOperation.type}" (${nextOperation.id}).`,
        );

        break;
      }

      default: {
        throw new ProjectError(`Unsupported status "${status}".`);
      }
    }
  }
}

export function startRunner() {
  if (status === RUNNER_STATUS.STOPPED) {
    status = RUNNER_STATUS.RUNNING;
  }
}

export function stopRunner() {
  if (status === RUNNER_STATUS.RUNNING) {
    status = RUNNER_STATUS.STOPPED;
  }
}
