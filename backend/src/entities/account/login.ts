import bcrypt from "bcrypt";
import { dbQuery } from "#database";
import {
  type IAccountDB,
  selectAccountAuth,
  selectAccountEntities,
} from "#database/queries/accounts";
import type { IAccountLogin } from "./types.js";

export const loginAccount = dbQuery<IAccountLogin, IAccountDB>(
  async ({ login, password }, ctx) => {
    const auth = await selectAccountAuth(login, ctx);

    await bcrypt.compare(password, auth.password);

    const [account] = await selectAccountEntities([auth.id], ctx);

    return account;
  },
);
