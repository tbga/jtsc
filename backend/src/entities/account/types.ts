import type { IAccountLogin } from "#codegen/schema/lib/entities/account/login";
import type { IBigSerialInteger } from "#types";

export type { IAccount } from "#codegen/schema/lib/entities/account/entity";
export { accountInitSchema } from "#codegen/schema/lib/entities/account/init";
export type { IAccountInit } from "#codegen/schema/lib/entities/account/init";
export type { IAccountPreview } from "#codegen/schema/lib/entities/account/preview";
export type { IAccountRole } from "#codegen/schema/lib/entities/account/role";
export type { IAccountLogin } from "#codegen/schema/lib/entities/account/login";

export interface IAccountArgs {
  account_id: IBigSerialInteger;
}

export interface IAccountAuth extends IAccountLogin {
  id: IBigSerialInteger;
}
