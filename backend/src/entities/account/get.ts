import { dbQuery } from "#database";
import { selectAccountEntities } from "#database/queries/accounts";
import type { IAccountAuth, IAccount } from "./types.js";

export const getAccount = dbQuery<IAccountAuth["id"], IAccount>(
  async (account_id, ctx) => {
    const [account] = await selectAccountEntities([account_id], ctx);

    return account;
  },
);
