import bcrypt from "bcrypt";
import { dbQuery } from "#database";
import {
  insertAccounts,
  type IAccountDBInit,
  selectAccountEntities,
  type IAccountDB,
} from "#database/queries/accounts";
import type { IAccountInit } from "#entities/account";
import type { IAccount, IAccountRole } from "./types.js";

interface IArgs {
  role?: IAccountRole;
  init: IAccountInit;
}

const saltRounds = 10;

export const registerAccount = dbQuery<IArgs, IAccountDB>(
  async ({ init, role = "user" }, ctx) => {
    const { login, password } = init;
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    const dbInit: IAccountDBInit = {
      login,
      password: hashedPassword,
      role,
    };
    const account_ids = await insertAccounts([dbInit], ctx);
    const [account] = await selectAccountEntities(account_ids, ctx);

    return account;
  },
);

export const registerAdministrator = dbQuery<IAccountInit, IAccountDB>(
  async (init, ctx) => {
    const account = await registerAccount({ init, role: "administrator" }, ctx);

    return account;
  },
);

export function toPublicAccount({ role }: IAccount): IAccount {
  return { role };
}
