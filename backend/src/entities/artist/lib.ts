export {
  artistSchema,
  validateArtist,
} from "#codegen/schema/lib/entities/artist/entity";
export {
  artistInitSchema,
  validateArtistInit,
} from "#codegen/schema/lib/entities/artist/init";
export {
  artistPreviewSchema,
  validateArtistPreview,
} from "#codegen/schema/lib/entities/artist/preview";
export {
  artistUpdateSchema,
  validateArtistUpdate,
} from "#codegen/schema/lib/entities/artist/update";
