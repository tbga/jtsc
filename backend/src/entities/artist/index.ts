export {
  artistInitSchema,
  artistPreviewSchema,
  artistSchema,
  artistUpdateSchema,
  validateArtist,
  validateArtistInit,
  validateArtistPreview,
  validateArtistUpdate,
} from "./lib.js";
export { getArtist, getArtists } from "./get.js";
export { countArtistSearch, searchArtists } from "./search.js";
export { updateArtist } from "./update.js";
export type {
  IArtist,
  IArtistInit,
  IArtistPreview,
  IArtistUpdate,
} from "./types.js";
