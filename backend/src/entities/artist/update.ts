import { dbQuery } from "#database";
import { type IArtistDBUpdate, updateArtists } from "#database/queries/artists";
import { getArtist } from "./get.js";
import type { IArtist } from "./types.js";

export const updateArtist = dbQuery<IArtistDBUpdate, IArtist>(
  async (update, ctx) => {
    const [id] = await updateArtists([update], ctx);
    const artist = await getArtist(id, ctx);

    return artist;
  },
);
