import { dbQuery } from "#database";
import {
  selectArtistEntities,
  selectArtistIDs,
  selectArtistPreviews,
} from "#database/queries/artists";
import type { IPaginationDB } from "#lib/pagination";
import { NotFoundError } from "#lib/server";
import type { IArtist, IArtistPreview } from "./types.js";

export const getArtist = dbQuery<IArtist["id"], IArtist>(
  async (artist_id, ctx) => {
    const result = await selectArtistEntities([artist_id], ctx);

    if (!result.length) {
      throw new NotFoundError(`The artist "${artist_id}" doesn't exist.`);
    }

    return result[0];
  },
);

export const getArtists = dbQuery<IPaginationDB, IArtistPreview[]>(
  async (pagination, ctx) => {
    const ids = await selectArtistIDs({ pagination }, ctx);
    const previews = await selectArtistPreviews(ids, ctx);

    return previews;
  },
);
