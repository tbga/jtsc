import type {
  ISearchQueryArgs,
  IPaginatedSearchQueryArgs,
} from "#lib/entities";
import { dbQuery } from "#database";
import {
  selectArtistCount,
  selectArtistIDs,
  selectArtistPreviews,
} from "#database/queries/artists";
import type { IArtistPreview } from "#entities/artist";
import type { IBigIntegerPositive } from "#types";

interface ICountArgs extends Required<ISearchQueryArgs> {}

export const countArtistSearch = dbQuery<ICountArgs, IBigIntegerPositive>(
  async ({ search_query }, ctx) => {
    const count = await selectArtistCount({ search_query }, ctx);

    return count;
  },
);

interface IArgs extends Required<IPaginatedSearchQueryArgs> {}

export const searchArtists = dbQuery<IArgs, IArtistPreview[]>(
  async ({ search_query, pagination }, ctx) => {
    const ids = await selectArtistIDs({ search_query, pagination }, ctx);
    const previews = await selectArtistPreviews(ids, ctx);

    return previews;
  },
);
