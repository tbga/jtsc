import type { IEntityItem } from "#lib/entities";
import type { Pagination } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectPostEntities,
  selectPostIDs,
  selectPostItems,
  selectPostPreviews,
} from "#database/queries/posts";
import type { IPostPreview, IPost } from "./types.js";

export const getPost = dbQuery<IPost["id"], IPost>(async (post_id, ctx) => {
  const [post] = await selectPostEntities([post_id], ctx);

  return post;
});

export const getPosts = dbQuery<Pagination, IPostPreview[]>(
  async (pagination, ctx) => {
    const ids = await selectPostIDs({ pagination }, ctx);
    const posts = await selectPostPreviews(ids, ctx);

    return posts;
  },
);

export const getPostItems = dbQuery<Pagination, IEntityItem[]>(
  async (pagination, ctx) => {
    const ids = await selectPostIDs({ pagination }, ctx);
    const posts = await selectPostItems(ids, ctx);

    return posts;
  },
);
