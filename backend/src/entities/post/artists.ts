import type { IEntityItem } from "#lib/entities";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectArtistCount,
  selectArtistIDs,
  selectArtistItems,
  selectArtistPreviews,
} from "#database/queries/artists";
import {
  deletePostArtists,
  insertPostArtists,
  type IPostArtistDBInit,
} from "#database/queries/posts";
import type { IArtistPreview, IArtist } from "#entities/artist";
import type { IPost } from "#entities/post";
import type { IBigIntegerPositive } from "#types";

interface IArgs {
  post_id: IPost["id"];
  artist_ids: IArtist["id"][];
}

interface IGetArgs extends IPaginatedQueryArgs {
  post_id: IPost["id"];
}

export const createPostArtists = dbQuery<IArgs, IEntityItem[]>(
  async ({ post_id, artist_ids }, ctx) => {
    const dbInits = artist_ids.map<IPostArtistDBInit>((artist_id) => {
      return {
        post_id,
        artist_id,
      };
    });
    const { artists: artistIDs } = await insertPostArtists(dbInits, ctx);
    const items = await selectArtistItems(artistIDs, ctx);

    return items;
  },
);

export const countPostArtists = dbQuery<IPost["id"], IBigIntegerPositive>(
  async (post_id, ctx) => {
    return selectArtistCount({ post_ids: [post_id] }, ctx);
  },
);

export const getPostArtists = dbQuery<IGetArgs, IArtistPreview[]>(
  async ({ post_id, pagination }, ctx) => {
    const ids = await selectArtistIDs({ post_ids: [post_id], pagination }, ctx);
    const previews = await selectArtistPreviews(ids, ctx);

    return previews;
  },
);

export const removePostArtists = dbQuery<IArgs, IEntityItem[]>(
  async ({ post_id, artist_ids }, ctx) => {
    const dbDeletes = artist_ids.map<IPostArtistDBInit>((artist_id) => {
      return {
        post_id,
        artist_id,
      };
    });
    const { artists: artistIDs } = await deletePostArtists(dbDeletes, ctx);
    const items = await selectArtistItems(artistIDs, ctx);

    return items;
  },
);
