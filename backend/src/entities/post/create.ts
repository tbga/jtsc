import type { IEntityItem } from "#lib/entities";
import { dbQuery } from "#database";
import {
  insertPosts,
  type IPostDBInit,
  selectPostItems,
} from "#database/queries/posts";
import type { IPostInit } from "./types.js";

export const createPost = dbQuery<IPostInit, IEntityItem>(async (init, ctx) => {
  const [item] = await createPosts([init], ctx);

  return item;
});

export const createPosts = dbQuery<IPostInit[], IEntityItem[]>(
  async (inits, ctx) => {
    const dbInits = toPostDBInits(inits);
    const ids = await insertPosts(dbInits, ctx);
    const items = await selectPostItems(ids, ctx);

    return items;
  },
);

export function toPostDBInits(inits: IPostInit[]): IPostDBInit[] {
  const dbInits = inits.map<IPostDBInit>(({ artists, ...init }) => {
    return { ...init, artist_ids: artists?.ids, artist_inits: artists?.inits };
  });

  return dbInits;
}
