import { dbQuery } from "#database";
import {
  type IPostReleaseDBInit,
  insertPostReleases,
  deletePostReleases,
  type IPostReleaseDBDelete,
} from "#database/queries/posts";
import {
  selectReleaseCount,
  selectReleaseIDs,
  selectReleasePreviews,
} from "#database/queries/releases";
import type { IRelease, IReleasePreview } from "#entities/release";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";
import type { IPostInit, IPostArgs, IPost } from "./types.js";

interface ICreatePostReleasesArgs
  extends IPostArgs,
    Pick<Required<IPostInit>["releases"], "ids" | "inits"> {}

export const createPostReleases = dbQuery<
  ICreatePostReleasesArgs,
  IReleasePreview[]
>(async ({ post_id, ids, inits }, ctx) => {
  const dbInits: IPostReleaseDBInit[] = [
    {
      post_id,
      release_ids: ids,
      release_inits: inits,
    },
  ];

  const { releases: release_ids } = await insertPostReleases(dbInits, ctx);
  const previews = await selectReleasePreviews(release_ids, ctx);

  return previews;
});

export const countPostReleases = dbQuery<IPost["id"], IBigIntegerPositive>(
  async (post_id, ctx) => {
    const count = await selectReleaseCount({ post_ids: [post_id] }, ctx);

    return count;
  },
);

interface IGetPostReleases extends IPostArgs, IPaginatedQueryArgs {}

export const getPostReleases = dbQuery<IGetPostReleases, IReleasePreview[]>(
  async ({ post_id, pagination }, ctx) => {
    const ids = await selectReleaseIDs(
      { post_ids: [post_id], pagination },
      ctx,
    );
    const previews = await selectReleasePreviews(ids, ctx);

    return previews;
  },
);

interface IRemovePostReleasesArgs extends IPostArgs {
  release_ids: IRelease["id"][];
}

export const removePostReleases = dbQuery<
  IRemovePostReleasesArgs,
  IReleasePreview[]
>(async ({ post_id, release_ids }, ctx) => {
  const dbDeletes = release_ids.map<IPostReleaseDBDelete>((release_id) => {
    return {
      post_id,
      release_id,
    };
  });
  const { releases: releaseIDs } = await deletePostReleases(dbDeletes, ctx);
  const previews = await selectReleasePreviews(releaseIDs, ctx);

  return previews;
});
