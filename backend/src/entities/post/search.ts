import type {
  IEntityItem,
  IPaginatedSearchQueryArgs,
  ISearchQueryArgs,
} from "#lib/entities";
import { dbQuery } from "#database";
import {
  selectPostCount,
  selectPostIDs,
  selectPostItems,
  selectPostPreviews,
} from "#database/queries/posts";
import type { IBigIntegerPositive } from "#types";
import type { IPostPreview } from "./types.js";

export const searchPostCount = dbQuery<
  Omit<ISearchQueryArgs, "is_merged_included">,
  IBigIntegerPositive
>(async ({ search_query }, ctx) => {
  const count = await selectPostCount({ search_query }, ctx);

  return count;
});

export const searchPostPreviews = dbQuery<
  IPaginatedSearchQueryArgs,
  IPostPreview[]
>(async ({ pagination, search_query }, ctx) => {
  const ids = await selectPostIDs({ pagination, search_query }, ctx);
  const previews = await selectPostPreviews(ids, ctx);

  return previews;
});

export const searchPostItems = dbQuery<
  IPaginatedSearchQueryArgs,
  IEntityItem[]
>(async ({ pagination, search_query }, ctx) => {
  const ids = await selectPostIDs({ pagination, search_query }, ctx);
  const items = await selectPostItems(ids, ctx);

  return items;
});
