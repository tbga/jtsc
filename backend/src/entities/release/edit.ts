import { dbQuery } from "#database";
import {
  selectReleaseEntities,
  updateReleases,
} from "#database/queries/releases";
import type { IRelease, IReleaseUpdate } from "./types.js";

interface IArgs {
  release_id: IRelease["id"];
  update: IReleaseUpdate;
}

export const editRelease = dbQuery<IArgs, IRelease>(
  async ({ release_id, update }, ctx) => {
    const dbUpdate = { ...update, id: release_id };
    const releaseIDs = await updateReleases([dbUpdate], ctx);
    const [release] = await selectReleaseEntities(releaseIDs, ctx);

    return release;
  },
);
