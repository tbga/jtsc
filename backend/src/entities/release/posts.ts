import type { IEntityItem } from "#lib/entities";
import type { IPaginationDB } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectPostCount,
  selectPostIDs,
  selectPostItems,
  selectPostPreviews,
} from "#database/queries/posts";
import {
  deleteReleasePosts,
  insertReleasePosts,
  type IReleasePostDBInit,
} from "#database/queries/releases";
import type { IPost, IPostPreview } from "#entities/post";
import type { IBigIntegerPositive } from "#types";
import type { IReleaseArgs, IRelease } from "./types.js";

interface ICreateReleasePostsArgs extends IReleaseArgs {
  post_ids: IPost["id"][];
}

export const createReleasePosts = dbQuery<
  ICreateReleasePostsArgs,
  IEntityItem[]
>(async ({ release_id, post_ids }, ctx) => {
  const dbInits = post_ids.map<IReleasePostDBInit>((post_id) => {
    return {
      release_id,
      post_id,
    };
  });
  const { posts: postIDs } = await insertReleasePosts(dbInits, ctx);

  const posts = await selectPostItems(postIDs, ctx);

  return posts;
});

export const countReleasePosts = dbQuery<IRelease["id"], IBigIntegerPositive>(
  async (release_id, ctx) => {
    const count = await selectPostCount({ release_ids: [release_id] }, ctx);

    return count;
  },
);

interface IGetReleasePostsArgs extends IReleaseArgs {
  pagination: IPaginationDB;
}

export const getReleasePosts = dbQuery<IGetReleasePostsArgs, IPostPreview[]>(
  async ({ release_id, pagination }, ctx) => {
    const ids = await selectPostIDs(
      {
        release_ids: [release_id],
        pagination,
      },
      ctx,
    );
    const previews = await selectPostPreviews(ids, ctx);

    return previews;
  },
);

export const removeReleasePosts = dbQuery<
  ICreateReleasePostsArgs,
  IEntityItem[]
>(async ({ release_id, post_ids }, ctx) => {
  const dbInits = post_ids.map<IReleasePostDBInit>((post_id) => {
    return {
      release_id,
      post_id,
    };
  });
  const { posts: postIDs } = await deleteReleasePosts(dbInits, ctx);
  const posts = await selectPostItems(postIDs, ctx);

  return posts;
});
