import { NotFoundError } from "#lib/server";
import type { IPaginationDB } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectReleaseIDs,
  selectReleaseEntities,
  selectReleasePreviews,
} from "#database/queries/releases";
import { EMPTY_OBJECT } from "#types";
import type { IRelease, IReleasePreview } from "./types.js";

export const getRelease = dbQuery<IRelease["id"], IRelease>(
  async (release_id, ctx) => {
    const ids = await selectReleaseIDs(
      {
        release_ids: [release_id],
        pagination: EMPTY_OBJECT,
      },
      ctx,
    );
    const result = await selectReleaseEntities(ids, ctx);

    if (!result.length) {
      throw new NotFoundError(`The release "${release_id}" doesn't exist.`);
    }

    return result[0];
  },
);

export const getReleases = dbQuery<IPaginationDB, IReleasePreview[]>(
  async (pagination, ctx) => {
    const ids = await selectReleaseIDs({ pagination }, ctx);
    const releases = await selectReleasePreviews(ids, ctx);

    return releases;
  },
);
