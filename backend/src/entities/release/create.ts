import type { IEntityItem } from "#lib/entities";
import { dbQuery } from "#database";
import {
  insertReleases,
  selectReleaseItems,
  type IReleaseDBInit,
} from "#database/queries/releases";
import type { IReleaseInit } from "./types.js";

export const createRelease = dbQuery<IReleaseInit, IEntityItem>(
  async (init, ctx) => {
    const [item] = await createReleases([init], ctx);

    return item;
  },
);

export const createReleases = dbQuery<IReleaseInit[], IEntityItem[]>(
  async (releaseInits, ctx) => {
    const dbInits = await toReleaseDBInits(releaseInits);
    const ids = await insertReleases(dbInits, ctx);
    const releases = await selectReleaseItems(ids, ctx);

    return releases;
  },
);

export async function toReleaseDBInits(
  inits: IReleaseInit[],
): Promise<IReleaseDBInit[]> {
  const dbInits: IReleaseDBInit[] = [];

  for await (const releaseInit of inits) {
    const { posts, profiles, ...init } = releaseInit;

    const dbInit: IReleaseDBInit = {
      ...init,
      profile_ids: profiles?.ids,
      post_ids: posts?.ids,
      post_inits: posts?.inits,
    };

    dbInits.push(dbInit);
  }

  return dbInits;
}
