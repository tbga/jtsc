import { dbQuery } from "#database";
import {
  selectReleaseCount,
  selectReleaseIDs,
  selectReleasePreviews,
  selectReleaseItems,
} from "#database/queries/releases";
import type { IReleasePreview } from "#entities/release";
import type {
  IEntityItem,
  IPaginatedSearchQueryArgs,
  ISearchQueryArgs,
} from "#lib/entities";
import type { IBigIntegerPositive } from "#types";

export const searchReleaseCount = dbQuery<
  Omit<ISearchQueryArgs, "is_merged_included">,
  IBigIntegerPositive
>(async ({ search_query }, ctx) => {
  const count = await selectReleaseCount({ search_query }, ctx);

  return count;
});

export const searchReleaseItems = dbQuery<
  Omit<IPaginatedSearchQueryArgs, "is_merged_included">,
  IEntityItem[]
>(async ({ search_query, pagination }, ctx) => {
  const ids = await selectReleaseIDs({ pagination, search_query }, ctx);
  const items = await selectReleaseItems(ids, ctx);

  return items;
});

export const searchReleasePreviews = dbQuery<
  Omit<IPaginatedSearchQueryArgs, "is_merged_included">,
  IReleasePreview[]
>(async ({ search_query, pagination }, ctx) => {
  const ids = await selectReleaseIDs({ pagination, search_query }, ctx);
  const previews = await selectReleasePreviews(ids, ctx);

  return previews;
});
