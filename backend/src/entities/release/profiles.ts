import type { IEntityItem } from "#lib/entities";
import type { IPaginationDB } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  insertReleaseProfiles,
  deleteReleaseProfiles,
  type IReleaseProfileDBInit,
} from "#database/queries/releases";
import {
  selectProfileCount,
  selectProfileIDs,
  selectProfileItems,
  selectProfilePreviews,
} from "#database/queries/profiles";
import type { IProfilePreview, IProfile } from "#entities/profile";
import type { IBigIntegerPositive } from "#types";
import type { IReleaseArgs, IRelease } from "./types.js";

interface ICreateReleaseProfilesArgs extends IReleaseArgs {
  profile_ids: IProfile["id"][];
}

export const createReleaseProfiles = dbQuery<
  ICreateReleaseProfilesArgs,
  IEntityItem[]
>(async ({ release_id, profile_ids }, ctx) => {
  const dbInits = profile_ids.map<IReleaseProfileDBInit>((profile_id) => {
    return {
      release_id,
      profile_id,
    };
  });
  const { profiles: profileIDs } = await insertReleaseProfiles(dbInits, ctx);

  const profiles = await selectProfileItems(profileIDs, ctx);

  return profiles;
});

export const countReleaseProfiles = dbQuery<
  IRelease["id"],
  IBigIntegerPositive
>(async (release_id, ctx) => {
  const count = await selectProfileCount({ release_ids: [release_id] }, ctx);

  return count;
});

interface IGetReleaseProfilesArgs extends IReleaseArgs {
  pagination: IPaginationDB;
}

export const getReleaseProfiles = dbQuery<
  IGetReleaseProfilesArgs,
  IProfilePreview[]
>(async ({ release_id, pagination }, ctx) => {
  const ids = await selectProfileIDs(
    {
      release_ids: [release_id],
      pagination,
    },
    ctx,
  );
  const previews = await selectProfilePreviews(ids, ctx);

  return previews;
});

export const removeReleaseProfiles = dbQuery<
  ICreateReleaseProfilesArgs,
  IEntityItem[]
>(async ({ release_id, profile_ids }, ctx) => {
  const dbInits = profile_ids.map<IReleaseProfileDBInit>((profile_id) => {
    return {
      release_id,
      profile_id,
    };
  });
  const { profiles: profileIDs } = await deleteReleaseProfiles(dbInits, ctx);
  const posts = await selectProfileItems(profileIDs, ctx);

  return posts;
});
