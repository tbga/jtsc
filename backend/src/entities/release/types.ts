import type { IRelease } from "#codegen/schema/lib/entities/release/entity";

export type { IRelease } from "#codegen/schema/lib/entities/release/entity";
export type { IReleaseInit } from "#codegen/schema/lib/entities/release/init";
export type { IReleasePreview } from "#codegen/schema/lib/entities/release/preview";
export type { IReleaseUpdate } from "#codegen/schema/lib/entities/release/update";

export interface IReleaseArgs {
  release_id: IRelease["id"];
}
