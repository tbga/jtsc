import type { IPublicImportSiteCategoryAPI } from "#codegen/schema/lib/entities/public-import/site/category-api";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery } from "#database";
import {
  selectPublicImportEntities,
  selectPublicImportIDs,
  selectPublicImportPreviews,
} from "#database/queries/public-imports";
import {
  selectPublicImportSiteEntities,
  selectPublicImportSiteIDs,
  selectPublicImportSitePreviews,
} from "#database/queries/public-imports/sites";
import type {
  IPublicImportArgs,
  IPublicImport,
  IPublicImportPreview,
  IPublicImportSite,
  IPublicImportSitePreview,
  IPublicImportSiteCategory,
} from "./types.js";
import { ProjectError } from "#lib/errors";

export const getPublicImport = dbQuery<IPublicImportArgs, IPublicImport>(
  async ({ public_import_id }, ctx) => {
    const [entity] = await selectPublicImportEntities([public_import_id], ctx);

    return entity;
  },
);

export const getPublicImports = dbQuery<
  IPaginatedQueryArgs,
  IPublicImportPreview[]
>(async ({ pagination }, ctx) => {
  const ids = await selectPublicImportIDs({ pagination }, ctx);
  const previews = await selectPublicImportPreviews(ids, ctx);

  return previews;
});

export const getPublicImportSite = dbQuery<
  IPublicImportArgs & { public_import_site_id: IPublicImportSite["id"] },
  IPublicImportSite
>(async ({ public_import_id, public_import_site_id }, ctx) => {
  const [site] = await selectPublicImportSiteEntities(
    [public_import_site_id],
    ctx,
  );

  if (site.public_import.id !== public_import_id) {
    throw new ProjectError(
      `Public import site "${site.title}" (${site.id}) is not a part of public import "${public_import_id}".`,
    );
  }

  return site;
});

export const getPublicImportSites = dbQuery<
  IPublicImportArgs &
    IPaginatedQueryArgs & { category: IPublicImportSiteCategoryAPI },
  IPublicImportSitePreview[]
>(async ({ public_import_id, pagination, category }, ctx) => {
  const finalCategory: IPublicImportSiteCategory | undefined =
    category === "all" ? undefined : category;

  const ids = await selectPublicImportSiteIDs(
    {
      public_import_ids: [public_import_id],
      pagination,
      category: finalCategory,
    },
    ctx,
  );
  const sites = await selectPublicImportSitePreviews(ids, ctx);

  return sites;
});
