import { dbQuery } from "#database";
import {
  type IPublicImportSiteUpdate,
  selectPublicImportSiteEntities,
  updatePublicImportSites,
} from "#database/queries/public-imports/sites";
import type { IPublicImportSite } from "./types.js";

interface IArgs {
  public_import_site_id: IPublicImportSite["id"];
}

export const approvePublicImportSite = dbQuery<IArgs, IPublicImportSite>(
  async ({ public_import_site_id }, ctx) => {
    const update: IPublicImportSiteUpdate = {
      id: public_import_site_id,
      approval_status: "approved",
    };
    const result = await updatePublicImportSites([update], ctx);
    const [updatedSite] = await selectPublicImportSiteEntities(result, ctx);

    return updatedSite;
  },
);

export const rejectPublicImportSite = dbQuery<IArgs, IPublicImportSite>(
  async ({ public_import_site_id }, ctx) => {
    const update: IPublicImportSiteUpdate = {
      id: public_import_site_id,
      approval_status: "rejected",
    };
    const result = await updatePublicImportSites([update], ctx);
    const [updatedSite] = await selectPublicImportSiteEntities(result, ctx);

    return updatedSite;
  },
);
