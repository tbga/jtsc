import type { IEntityItem } from "#lib/entities";
import { dbQuery } from "#database";
import {
  insertPublicImports,
  selectPublicImportItems,
} from "#database/queries/public-imports";
import type { IAccountArgs } from "#entities/account";
import { collectPublicExport } from "#public-exports";

interface IArgs extends IAccountArgs {
  importPath: string;
}

export const createPublicImport = dbQuery<IArgs, IEntityItem>(
  async ({ account_id, importPath }, ctx) => {
    const dbInit = await collectPublicExport(account_id, importPath);
    const ids = await insertPublicImports([dbInit], ctx);
    const [item] = await selectPublicImportItems(ids, ctx);

    return item;
  },
);
