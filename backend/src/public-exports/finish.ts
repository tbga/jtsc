import path from "node:path";
import { lstat } from "node:fs/promises";
import { v4 as uuidv4 } from "uuid";
import { PUBLIC_EXPORTS_FOLDER } from "#environment/variables.js";
import { ProjectError } from "#lib/errors";
import { isNonEmptyArray } from "#lib/array";
import { log } from "#lib/logs";
import { ensurePath, writeJSONFile } from "#lib/fs";
import { serializeData } from "#lib/api";
import { dbQuery } from "#database";
import { selectSiteEntities, selectSiteIDs } from "#database/queries/sites";
import { updatePublicExports } from "#database/queries/public-exports";
import { getPublicExport, type IPublicExport } from "#entities/public-export";
import type { ISitePublished } from "#entities/site";
import { EMPTY_OBJECT } from "#types";
import { validatePublicExportData, validatePublicExportMeta } from "./lib.js";
import type {
  IPublicExportData,
  IPublicExportMeta,
  IPublicSite,
} from "./types.js";

export const finishPublicExport = dbQuery<IPublicExport["id"], string>(
  async (public_export_id, ctx) => {
    log(`Starting the export of the public export ${public_export_id}...`);

    const publicExport = await getPublicExport({ public_export_id }, ctx);

    if (publicExport.status !== "finalized") {
      throw new ProjectError(
        `Public export "${publicExport.title}" (${publicExport.id}) is not ready for export.`,
      );
    }

    const siteIDs = await selectSiteIDs(
      { pagination: EMPTY_OBJECT, public_export_ids: [public_export_id] },
      ctx,
    );
    const siteEntities = (await selectSiteEntities(
      siteIDs,
      ctx,
    )) as ISitePublished[];

    const sites = toPublicSites(siteEntities);
    const data = createExportData({ publicExport, sites });
    const meta: IPublicExportMeta = {
      id: uuidv4(),
      title: publicExport.title,
      type: "basic",
      version: 1,
    };

    validatePublicExportMeta(meta);

    const folder = await savePublicExport(data, meta);

    await updatePublicExports(
      [{ id: publicExport.id, status: "finished", public_id: meta.id }],
      ctx,
    );

    log(
      `Exported public export "${publicExport.title}" (${publicExport.id}) to "${folder}".`,
    );

    return folder;
  },
);

interface ICreateExportDataArgs {
  publicExport: IPublicExport;
  sites: IPublicSite[];
}

function createExportData({
  publicExport,
  sites,
}: ICreateExportDataArgs): IPublicExportData {
  const data: IPublicExportData = {
    sites: isNonEmptyArray(sites) ? sites : undefined,
  };

  const serializedData = toStrippedNullish(serializeData(data));

  if (serializedData === null || serializedData === undefined) {
    throw new ProjectError(
      `The data for public export "${publicExport.title}" (${publicExport.id}) serialized to \`null\`.`,
    );
  }

  validatePublicExportData(serializedData);

  return serializedData;
}

function toPublicSites(sites: ISitePublished[]): IPublicSite[] {
  const publicSites = sites.map<IPublicSite>(
    ({
      public_id,
      home_page,
      title,
      description,
      long_title,
      url_templates,
    }) => {
      return {
        id: public_id,
        home_page,
        title,
        description,
        long_title,
        url_templates,
      };
    },
  );

  return publicSites;
}

async function savePublicExport(
  data: IPublicExportData,
  meta: IPublicExportMeta,
): Promise<string> {
  log(`Saving public export "${meta.title}" (${meta.id}) to folder...`);

  const exportPath = path.join(PUBLIC_EXPORTS_FOLDER, meta.id);
  const metaPath = path.join(exportPath, "meta.json");
  const dataPath = path.join(exportPath, "data.json");

  try {
    const stats = await lstat(exportPath);

    if (!stats.isDirectory()) {
      throw new ProjectError(
        `Public export already exists at path "${exportPath}".`,
      );
    }
  } catch (error) {
    if (ProjectError.isError(error)) {
      throw error;
    }

    await ensurePath(exportPath, false);
    await writeJSONFile(metaPath, meta);
    await writeJSONFile(dataPath, data);

    log(
      `Saved public export "${meta.title}" (${meta.id}) to folder "${exportPath}".`,
    );
  }

  return exportPath;
}

function toStrippedNullish<DataType>(data: DataType): DataType | undefined {
  let strippedData: DataType | undefined = undefined;

  switch (typeof data) {
    case "undefined": {
      strippedData = undefined;
      break;
    }

    case "string": {
      if (data === "") {
        strippedData = undefined;
        break;
      }

      strippedData = data;
      break;
    }

    case "object": {
      if (data === null) {
        strippedData = undefined;
        break;
      }

      // @ts-expect-error array is `any[]` due to signature
      strippedData = Array.isArray(data)
        ? toStrippedArray(data)
        : toStrippedObject(data);
      break;
    }

    default: {
      strippedData = data;
      break;
    }
  }

  return strippedData;
}

function toStrippedArray<DataType>(
  data: DataType[],
): [...DataType[]] | undefined {
  const strippedArray = data.reduce<DataType[]>((strippedArray, value) => {
    const strippedValue = toStrippedNullish(value);

    if (strippedValue === undefined) {
      return strippedArray;
    }

    strippedArray.push(strippedValue);

    return strippedArray;
  }, []);

  return !strippedArray.length ? undefined : strippedArray;
}

function toStrippedObject<DataType extends object>(
  data: DataType,
): DataType | undefined {
  const strippedObject = Object.entries(data).reduce<DataType>(
    (strippedObject, [key, value]) => {
      const strippedValue = toStrippedNullish(value);

      if (strippedValue === undefined) {
        return strippedObject;
      }

      // @ts-expect-error object index type issue
      strippedObject[key] = strippedValue;

      return strippedObject;
    },
    {} as DataType,
  );

  return !Object.keys(strippedObject).length ? undefined : strippedObject;
}
