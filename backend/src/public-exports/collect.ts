import path from "node:path";
import { ProjectError } from "#lib/errors";
import { readFolder, readJSONFile } from "#lib/fs";
import type { IPublicImportDBinit } from "#database/queries/public-imports";
import type { IAccountDB } from "#database/queries/accounts";
import { validatePublicExportData, validatePublicExportMeta } from "./lib.js";
import type { IPublicExportMeta, IPublicExportData } from "./types.js";

const metaFileName = "meta.json";
const dataFileName = "data.json";

export async function collectPublicExport(
  account_id: IAccountDB["id"],
  importPath: string,
): Promise<IPublicImportDBinit> {
  const { meta, data } = await validateImportPath(importPath);
  const { id, title, type, version } = meta;
  const dbInit: IPublicImportDBinit = {
    account_id,
    public_id: id,
    title,
    type,
    version,
    sites: data.sites,
  };

  return dbInit;
}

async function validateImportPath(
  importPath: string,
): Promise<{ meta: IPublicExportMeta; data: IPublicExportData }> {
  const entries = await readFolder(importPath);
  const metaPath = path.join(importPath, metaFileName);
  const dataPath = path.join(importPath, dataFileName);

  if (entries.length !== 2) {
    throw new ProjectError(
      entries.length < 2
        ? `The import folder at "${importPath}" does not have enough files.`
        : `The import folder at "${importPath}" has extraneous files.`,
    );
  }

  for (const entry of entries) {
    const entryPath = path.join(importPath, entry.name);

    if (!entry.isFile()) {
      throw new ProjectError(
        `The path at "${entryPath}" is not a valid public import path.`,
      );
    }

    if (entry.name !== metaFileName && entry.name !== dataFileName) {
      throw new ProjectError(
        `The file at "${entryPath}" is not a valid public import file.`,
      );
    }
  }

  const meta = await readJSONFile<IPublicExportMeta>(metaPath);
  validatePublicExportMeta(meta);
  const data = await readJSONFile<IPublicExportData>(dataPath);
  validatePublicExportData(data);

  return { meta, data };
}
