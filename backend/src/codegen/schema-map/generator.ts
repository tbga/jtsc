import { default as stringifyObject } from "stringify-object";
import { SCHEMA_FOLDER } from "#environment/derived-variables.js";
import {
  type ICodegenFunc,
  type ICodegen,
  type ICodegenExport,
  collectJSONSchemas,
} from "#lib/codegen";
import { multilineString } from "#lib/strings";

const generateSchemaMap: ICodegenFunc<ICodegen> = async () => {
  const schemaCollection = await collectJSONSchemas(SCHEMA_FOLDER);
  const concreteExports: string[] = ["schemaMap"];
  const mapKeys = Object.values(schemaCollection).map(({ schema }) => {
    // @ts-ignore @TODO fix definitions
    const schemaObj = stringifyObject(schema);

    return `"${schema.$id}": ${schemaObj},`;
  });
  const mapCode = ["export const schemaMap = {", ...mapKeys, "} as const;"];
  const result = multilineString(...mapCode);
  const exports: ICodegenExport = {
    concrete: concreteExports,
  };
  const codegen: ICodegen = {
    exports,
    result,
  };

  return codegen;
};

export default generateSchemaMap;
