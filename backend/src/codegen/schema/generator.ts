import path from "node:path";
import { default as stringifyObject } from "stringify-object";
import { SCHEMA_FOLDER } from "#environment/derived-variables.js";
import { IS_WINDOWS } from "#environment/constants.js";
import { decapitalizeString, multilineString } from "#lib/strings";
import {
  collectJSONSchemas,
  createInterfaceFromSchema,
  type ICodegenNode,
  type ICodegenFunc,
  type ICodegenGraph,
  schemaFileEnd,
  type IJSONSchemaCollection,
  type IModule,
} from "#lib/codegen";
import { collectRefs, IJSONSchema, IRefSet } from "#lib/json-schema";

const generateJSONSchemas: ICodegenFunc<ICodegenGraph> = async () => {
  const schemaCollection = await collectJSONSchemas(SCHEMA_FOLDER);
  const root = "lib";
  const nodes: ICodegenNode[] = [];

  for await (const { localPath, schema } of Object.values(schemaCollection)) {
    // @TODO a better way to detect and match multi-extensions
    const nodePath = localPath.replace(schemaFileEnd, "");
    const refs = collectRefs(schema);
    const schemaTypes = await createSchemaTypes(schema, {
      refs,
      root,
      schemaCollection,
    });
    const schemaLib = await createSchemaLib(schema);

    const node: ICodegenNode = {
      nodePath,
      lib: schemaLib,
      types: schemaTypes,
    };

    nodes.push(node);
  }

  const codegen: ICodegenGraph = {
    root,
    type: "graph",
    nodes,
  };

  return codegen;
};

interface ICreateSchemaTypesMeta {
  refs: IRefSet;
  schemaCollection: IJSONSchemaCollection;
  root: string;
}

async function createSchemaTypes(
  schema: IJSONSchema,
  { refs, root, schemaCollection }: ICreateSchemaTypesMeta,
): Promise<IModule> {
  const typeExports: string[] = [];
  const imports: string[] = [];

  const interfaceCode = await createSchemaInterface(schema, {
    imports,
    refs,
    root,
    schemaCollection,
    typeExports,
  });

  const typesModule: IModule = {
    exports: {
      types: typeExports,
    },
    result: multilineString(...imports, interfaceCode),
  };
  return typesModule;
}

interface ICreateSchemaInterfaceMeta {
  refs: IRefSet;
  imports: string[];
  typeExports: string[];
  schemaCollection: IJSONSchemaCollection;
  root: string;
}

async function createSchemaInterface(
  schema: IJSONSchema,
  {
    imports,
    refs,
    typeExports,
    schemaCollection,
    root,
  }: ICreateSchemaInterfaceMeta,
): Promise<string> {
  const schemaInterface = await createInterfaceFromSchema(schema, {
    declareExternallyReferenced: false,
  });
  const { code, inputSchema } = schemaInterface;
  const importStatements = Array.from(refs).map((ref) => {
    const { localPath, schema } = schemaCollection[ref];
    const symbolName = `I${schema.title}`;
    const normalizedPath = (
      IS_WINDOWS ? localPath.split(path.sep).join(path.posix.sep) : localPath
    ).replace(schemaFileEnd, "");
    const modulePath = `#codegen/schema/${root}/${normalizedPath}`;
    const importStatement = `import { type ${symbolName} } from "${modulePath}"`;

    return importStatement;
  });

  typeExports.push(inputSchema.title);
  imports.push(...importStatements, "\n");

  return code;
}

async function createSchemaLib(schema: IJSONSchema): Promise<IModule> {
  const concreteExports: string[] = [];
  const imports: string[] = [
    `import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema"`,
    `import { type I${schema.title} } from "./types.js"`,
  ];
  const inlineSchemaCode = await createInlineSchema(schema, {
    exports: concreteExports,
  });
  const schemaValidatorCode = await createSchemaValidator(schema, {
    exports: concreteExports,
  });
  const result = multilineString(
    ...imports,
    "\n",
    inlineSchemaCode,
    "\n",
    schemaValidatorCode,
  );
  const libModule: IModule = {
    exports: {
      concrete: concreteExports,
    },
    result,
  };
  return libModule;
}

interface ICreateInlineSchemaMeta {
  exports: string[];
}

async function createInlineSchema(
  schema: IJSONSchema,
  { exports }: ICreateInlineSchemaMeta,
) {
  const schemaName = `${decapitalizeString(schema.title)}Schema`;
  const schemaObj = multilineString(
    // @ts-ignore @TODO fix definitions
    `export const ${schemaName} = ${stringifyObject(schema)} as const`,
    `addSchema(${schemaName} as unknown as IJSONSchema)`,
  );

  // add schema name to the index exports
  exports.push(schemaName);

  return schemaObj;
}

interface ICreateSchemaValidatorMeta {
  exports: string[];
}

async function createSchemaValidator(
  schema: IJSONSchema,
  { exports }: ICreateSchemaValidatorMeta,
) {
  const funcName = `validate${schema.title}`;
  const interfaceName = `I${schema.title}`;
  const schemaName = `${decapitalizeString(schema.title)}Schema`;
  const validatorFunc = `export const ${funcName} = createValidator<${interfaceName}>(${schemaName} as unknown as IJSONSchema);`;

  exports.push(funcName);

  return validatorFunc;
}

export default generateJSONSchemas;
