import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type ISiteURLTemplates } from "#codegen/schema/lib/entities/site/url-templates";

export interface IPublicSite {
  id: IUUID;
  /**
   * The site's home page.
   */
  home_page: string;
  title: ITitle;
  long_title?: INonEmptyString;
  description?: INonEmptyString;
  url_templates?: ISiteURLTemplates;
}
