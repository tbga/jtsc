import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";

/**
 * The metadata of the public export.
 */
export interface IPublicExportMeta {
  id: IUUID;
  type: "basic";
  version: 1;
  title: ITitle;
}
