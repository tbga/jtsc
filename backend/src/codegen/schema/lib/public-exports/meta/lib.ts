import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportMeta } from "./types.js";

export const publicExportMetaSchema = {
  $id: "http://jtsc-schemas.org/public-exports/meta.schema.json",
  title: "PublicExportMeta",
  description: "The metadata of the public export.",
  type: "object",
  additionalProperties: false,
  required: ["id", "type", "version", "title"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    type: {
      const: "basic",
    },
    version: {
      const: 1,
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
addSchema(publicExportMetaSchema as unknown as IJSONSchema);

export const validatePublicExportMeta = createValidator<IPublicExportMeta>(
  publicExportMetaSchema as unknown as IJSONSchema,
);
