import { type IPublicSite } from "#codegen/schema/lib/public-exports/entities/site";

/**
 * The data of the public export.
 */
export interface IPublicExportData {
  /**
   * Exported sites.
   *
   * @minItems 1
   */
  sites?: [IPublicSite, ...IPublicSite[]];
}
