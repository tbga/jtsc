import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportData } from "./types.js";

export const publicExportDataSchema = {
  $id: "http://jtsc-schemas.org/public-exports/data.schema.json",
  title: "PublicExportData",
  description: "The data of the public export.",
  type: "object",
  additionalProperties: false,
  required: [],
  minProperties: 1,
  properties: {
    sites: {
      description: "Exported sites.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/public-exports/entities/site.schema.json",
      },
    },
  },
} as const;
addSchema(publicExportDataSchema as unknown as IJSONSchema);

export const validatePublicExportData = createValidator<IPublicExportData>(
  publicExportDataSchema as unknown as IJSONSchema,
);
