import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPlatform } from "./types.js";

export const platformSchema = {
  $id: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
  title: "Platform",
  description: "Information about the local file system.",
  type: "string",
  enum: [
    "aix",
    "android",
    "darwin",
    "freebsd",
    "haiku",
    "linux",
    "openbsd",
    "sunos",
    "win32",
    "cygwin",
    "netbsd",
  ],
} as const;
addSchema(platformSchema as unknown as IJSONSchema);

export const validatePlatform = createValidator<IPlatform>(
  platformSchema as unknown as IJSONSchema,
);
