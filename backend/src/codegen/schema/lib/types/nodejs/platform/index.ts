export { platformSchema, validatePlatform } from "./lib.js";
export type { IPlatform } from "./types.js";
