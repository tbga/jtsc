import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPagination } from "./types.js";

export const paginationSchema = {
  $id: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
  title: "Pagination",
  description: "Pagination information for the collection.",
  type: "object",
  required: ["total_count", "limit", "total_pages", "current_page"],
  additionalProperties: false,
  properties: {
    current_page: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    total_count: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    limit: {
      type: "integer",
    },
    total_pages: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
addSchema(paginationSchema as unknown as IJSONSchema);

export const validatePagination = createValidator<IPagination>(
  paginationSchema as unknown as IJSONSchema,
);
