import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type INonEmptyStringArray } from "./types.js";

export const nonEmptyStringArraySchema = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-string-array.schema.json",
  title: "NonEmptyStringArray",
  description: "A non-empty array of non-empty strings.",
  type: "array",
  minItems: 1,
  items: {
    $comment:
      "For some reason [`json-schema-to-typescript`](https://github.com/bcherny/json-schema-to-typescript) redeclares `http://jtsc-schemas.org/types/strings/minimal.schema.json` $ref, so using literal instead.",
    type: "string",
    minLength: 1,
  },
} as const;
addSchema(nonEmptyStringArraySchema as unknown as IJSONSchema);

export const validateNonEmptyStringArray =
  createValidator<INonEmptyStringArray>(
    nonEmptyStringArraySchema as unknown as IJSONSchema,
  );
