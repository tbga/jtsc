/**
 * A non-empty array of non-empty strings.
 *
 * @minItems 1
 */
export type INonEmptyStringArray = [string, ...string[]];
