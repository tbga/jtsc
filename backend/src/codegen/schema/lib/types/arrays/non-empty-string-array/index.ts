export {
  nonEmptyStringArraySchema,
  validateNonEmptyStringArray,
} from "./lib.js";
export type { INonEmptyStringArray } from "./types.js";
