export { arrayNonEmptySchema, validateArrayNonEmpty } from "./lib.js";
export type { IArrayNonEmpty } from "./types.js";
