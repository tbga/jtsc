import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IArrayNonEmpty } from "./types.js";

export const arrayNonEmptySchema = {
  $id: "http://jtsc-schemas.org/types/arrays/non-empty-array.schema.json",
  title: "ArrayNonEmpty",
  description: "An array with at least one element",
  type: "array",
  minItems: 1,
} as const;
addSchema(arrayNonEmptySchema as unknown as IJSONSchema);

export const validateArrayNonEmpty = createValidator<IArrayNonEmpty>(
  arrayNonEmptySchema as unknown as IJSONSchema,
);
