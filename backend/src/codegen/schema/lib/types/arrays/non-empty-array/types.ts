/**
 * An array with at least one element
 *
 * @minItems 1
 */
export type IArrayNonEmpty = [unknown, ...unknown[]];
