import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IJSONAny } from "./types.js";

export const jSONAnySchema = {
  $id: "https://jtsc-schemas.org/types/json/any.schema.json",
  title: "JSONAny",
  description: "Any jsonable value.",
  anyOf: [
    {
      type: "null",
    },
    {
      type: "boolean",
    },
    {
      type: "number",
    },
    {
      type: "string",
    },
    {
      type: "object",
    },
    {
      type: "array",
    },
    {
      type: "array",
      minItems: 1,
    },
  ],
} as const;
addSchema(jSONAnySchema as unknown as IJSONSchema);

export const validateJSONAny = createValidator<IJSONAny>(
  jSONAnySchema as unknown as IJSONSchema,
);
