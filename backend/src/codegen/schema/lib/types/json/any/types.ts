/**
 * Any jsonable value.
 */
export type IJSONAny =
  | null
  | boolean
  | number
  | string
  | {
      [k: string]: unknown;
    }
  | unknown[]
  | [unknown, ...unknown[]];
