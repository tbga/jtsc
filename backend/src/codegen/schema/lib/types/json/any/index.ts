export { jSONAnySchema, validateJSONAny } from "./lib.js";
export type { IJSONAny } from "./types.js";
