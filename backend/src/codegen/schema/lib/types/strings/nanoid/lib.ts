import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type INanoID } from "./types.js";

export const nanoIDSchema = {
  $id: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
  title: "NanoID",
  description:
    "The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.",
  type: "string",
  minLength: 21,
  maxLength: 21,
} as const;
addSchema(nanoIDSchema as unknown as IJSONSchema);

export const validateNanoID = createValidator<INanoID>(
  nanoIDSchema as unknown as IJSONSchema,
);
