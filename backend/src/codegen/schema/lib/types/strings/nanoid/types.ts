/**
 * The identifier created by [`nanoid`](https://github.com/ai/nanoid) with default options.
 */
export type INanoID = string;
