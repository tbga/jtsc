import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IUUID } from "./types.js";

export const uUIDSchema = {
  $id: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
  title: "UUID",
  description:
    "The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).",
  type: "string",
  format: "uuid",
  minLength: 36,
  maxLength: 36,
} as const;
addSchema(uUIDSchema as unknown as IJSONSchema);

export const validateUUID = createValidator<IUUID>(
  uUIDSchema as unknown as IJSONSchema,
);
