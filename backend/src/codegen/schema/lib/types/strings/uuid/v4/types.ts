/**
 * The unique identifier as per RFC4122. Version used by [postgresql](https://www.postgresql.org/docs/13/datatype-uuid.html).
 */
export type IUUID = string;
