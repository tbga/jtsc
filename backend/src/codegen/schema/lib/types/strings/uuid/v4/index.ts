export { uUIDSchema, validateUUID } from "./lib.js";
export type { IUUID } from "./types.js";
