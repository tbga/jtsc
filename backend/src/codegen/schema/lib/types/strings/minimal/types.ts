/**
 * A string with at least 1 character.
 */
export type INonEmptyString = string;
