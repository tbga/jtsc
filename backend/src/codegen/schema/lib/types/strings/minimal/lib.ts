import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type INonEmptyString } from "./types.js";

export const nonEmptyStringSchema = {
  $id: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
  title: "NonEmptyString",
  description: "A string with at least 1 character.",
  type: "string",
  minLength: 1,
} as const;
addSchema(nonEmptyStringSchema as unknown as IJSONSchema);

export const validateNonEmptyString = createValidator<INonEmptyString>(
  nonEmptyStringSchema as unknown as IJSONSchema,
);
