export { nonEmptyStringSchema, validateNonEmptyString } from "./lib.js";
export type { INonEmptyString } from "./types.js";
