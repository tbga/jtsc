export { titleSchema, validateTitle } from "./lib.js";
export type { ITitle } from "./types.js";
