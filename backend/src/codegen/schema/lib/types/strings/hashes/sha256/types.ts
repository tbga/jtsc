/**
 * SHA256 hash string.
 */
export type IHashSHA256 = string;
