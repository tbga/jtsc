import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IHashSHA1 } from "./types.js";

export const hashSHA1Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha1.schema.json",
  title: "HashSHA1",
  description: "SHA1 hash string.",
  type: "string",
  minLength: 40,
  maxLength: 40,
} as const;
addSchema(hashSHA1Schema as unknown as IJSONSchema);

export const validateHashSHA1 = createValidator<IHashSHA1>(
  hashSHA1Schema as unknown as IJSONSchema,
);
