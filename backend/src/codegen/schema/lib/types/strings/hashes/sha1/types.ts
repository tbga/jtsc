/**
 * SHA1 hash string.
 */
export type IHashSHA1 = string;
