import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IHashMD5 } from "./types.js";

export const hashMD5Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/md5.schema.json",
  title: "HashMD5",
  description: "MD5 hash string.",
  type: "string",
  minLength: 32,
  maxLength: 32,
} as const;
addSchema(hashMD5Schema as unknown as IJSONSchema);

export const validateHashMD5 = createValidator<IHashMD5>(
  hashMD5Schema as unknown as IJSONSchema,
);
