/**
 * MD5 hash string.
 */
export type IHashMD5 = string;
