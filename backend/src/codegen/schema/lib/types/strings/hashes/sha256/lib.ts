import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IHashSHA256 } from "./types.js";

export const hashSHA256Schema = {
  $id: "http://jtsc-schemas.org/types/strings/hashes/sha256.schema.json",
  title: "HashSHA256",
  description: "SHA256 hash string.",
  type: "string",
  minLength: 64,
  maxLength: 64,
} as const;
addSchema(hashSHA256Schema as unknown as IJSONSchema);

export const validateHashSHA256 = createValidator<IHashSHA256>(
  hashSHA256Schema as unknown as IJSONSchema,
);
