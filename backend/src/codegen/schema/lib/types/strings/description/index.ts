export { descriptionSchema, validateDescription } from "./lib.js";
export type { IDescription } from "./types.js";
