import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IDescription } from "./types.js";

export const descriptionSchema = {
  $id: "http://jtsc-schemas.org/types/strings/description.schema.json",
  title: "Description",
  type: "string",
  minLength: 1,
  maxLength: 512,
  examples: [
    "Eum omnis vel quod quasi. A aut et eveniet saepe dolor aliquam nulla ea. Et corporis veniam dolorem eos et voluptatem. Voluptatum cupiditate provident provident qui.",
  ],
} as const;
addSchema(descriptionSchema as unknown as IJSONSchema);

export const validateDescription = createValidator<IDescription>(
  descriptionSchema as unknown as IJSONSchema,
);
