/**
 * Plain text date of arbitrary format.
 */
export type IDateTimeGeneric = string;
