export { dateTimeGenericSchema, validateDateTimeGeneric } from "./lib.js";
export type { IDateTimeGeneric } from "./types.js";
