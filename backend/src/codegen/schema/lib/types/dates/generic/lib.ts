import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IDateTimeGeneric } from "./types.js";

export const dateTimeGenericSchema = {
  $id: "http://jtsc-schemas.org/types/dates/generic.schema.json",
  title: "DateTimeGeneric",
  description: "Plain text date of arbitrary format.",
  type: "string",
  minLength: 1,
  maxLength: 256,
} as const;
addSchema(dateTimeGenericSchema as unknown as IJSONSchema);

export const validateDateTimeGeneric = createValidator<IDateTimeGeneric>(
  dateTimeGenericSchema as unknown as IJSONSchema,
);
