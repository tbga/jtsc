import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IDateTime } from "./types.js";

export const dateTimeSchema = {
  $id: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
  title: "DateTime",
  description:
    "[RFC3339](https://www.rfc-editor.org/rfc/rfc3339) datetime string which is compatible with html and ISO 8601 as per [this graph](https://ijmacd.github.io/rfc3339-iso8601/).",
  type: "string",
  format: "date-time",
  minLength: 29,
  maxLength: 29,
  default: "1970-01-01T00:00:00.000+00:00",
  examples: ["0000-01-01T00:00:00.000+00:00"],
} as const;
addSchema(dateTimeSchema as unknown as IJSONSchema);

export const validateDateTime = createValidator<IDateTime>(
  dateTimeSchema as unknown as IJSONSchema,
);
