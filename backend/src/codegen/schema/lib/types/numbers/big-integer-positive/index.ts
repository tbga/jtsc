export { bigIntegerPositiveSchema, validateBigIntegerPositive } from "./lib.js";
export type { IBigIntegerPositive } from "./types.js";
