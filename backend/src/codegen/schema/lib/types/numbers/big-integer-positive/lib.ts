import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IBigIntegerPositive } from "./types.js";

export const bigIntegerPositiveSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
  title: "BigIntegerPositive",
  description: "Big integer but only positive values and zero.",
  type: "string",
  minLength: 1,
  maxLength: 20,
} as const;
addSchema(bigIntegerPositiveSchema as unknown as IJSONSchema);

export const validateBigIntegerPositive = createValidator<IBigIntegerPositive>(
  bigIntegerPositiveSchema as unknown as IJSONSchema,
);
