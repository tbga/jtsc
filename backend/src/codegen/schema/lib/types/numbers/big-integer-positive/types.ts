/**
 * Big integer but only positive values and zero.
 */
export type IBigIntegerPositive = string;
