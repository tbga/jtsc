/**
 * Big integer
 */
export type IBigInteger = string;
