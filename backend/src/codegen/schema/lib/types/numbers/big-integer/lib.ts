import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IBigInteger } from "./types.js";

export const bigIntegerSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/big-integer.schema.json",
  title: "BigInteger",
  description: "Big integer",
  type: "string",
  minLength: 1,
  maxLength: 20,
} as const;
addSchema(bigIntegerSchema as unknown as IJSONSchema);

export const validateBigInteger = createValidator<IBigInteger>(
  bigIntegerSchema as unknown as IJSONSchema,
);
