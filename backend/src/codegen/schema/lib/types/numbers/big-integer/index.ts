export { bigIntegerSchema, validateBigInteger } from "./lib.js";
export type { IBigInteger } from "./types.js";
