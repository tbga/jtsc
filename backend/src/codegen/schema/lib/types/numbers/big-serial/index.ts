export { bigSerialIntegerSchema, validateBigSerialInteger } from "./lib.js";
export type { IBigSerialInteger } from "./types.js";
