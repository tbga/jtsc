import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IBigSerialInteger } from "./types.js";

export const bigSerialIntegerSchema = {
  $id: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
  title: "BigSerialInteger",
  description:
    "Integer equivalent of `BIG SERIAL` type.\nMax length is 19 (without the sign) as per [postgresql docs](https://www.postgresql.org/docs/13/datatype-numeric.html).",
  type: "string",
  minLength: 1,
  maxLength: 19,
} as const;
addSchema(bigSerialIntegerSchema as unknown as IJSONSchema);

export const validateBigSerialInteger = createValidator<IBigSerialInteger>(
  bigSerialIntegerSchema as unknown as IJSONSchema,
);
