export { serialIntegerSchema, validateSerialInteger } from "./lib.js";
export type { ISerialInteger } from "./types.js";
