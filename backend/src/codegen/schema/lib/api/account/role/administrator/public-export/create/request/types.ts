import { type IPublicExportInit } from "#codegen/schema/lib/entities/public-export/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestPublicExportCreate {
  data: IPublicExportInit;
}
