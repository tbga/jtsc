export {
  aPIRequestArtistUpdateSchema,
  validateAPIRequestArtistUpdate,
} from "./lib.js";
export type { IAPIRequestArtistUpdate } from "./types.js";
