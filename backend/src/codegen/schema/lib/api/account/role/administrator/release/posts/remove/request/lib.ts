import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleasePostsRemove } from "./types.js";

export const aPIRequestReleasePostsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/remove/request.schema.json",
  title: "APIRequestReleasePostsRemove",
  description: "Remove posts from a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of post IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestReleasePostsRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestReleasePostsRemove =
  createValidator<IAPIRequestReleasePostsRemove>(
    aPIRequestReleasePostsRemoveSchema as unknown as IJSONSchema,
  );
