import { type IRelease } from "#codegen/schema/lib/entities/release/entity";

/**
 * Added profile releases.
 */
export interface IAPIResponseProfileUpdate {
  is_successful: true;
  data: IRelease;
}
