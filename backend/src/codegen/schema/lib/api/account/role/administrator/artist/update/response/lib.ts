import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseArtistUpdate } from "./types.js";

export const aPIResponseArtistUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/artist/update/response.schema.json",
  title: "APIResponseArtistUpdate",
  description: "Updated artist.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/artist/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseArtistUpdateSchema as unknown as IJSONSchema);

export const validateAPIResponseArtistUpdate =
  createValidator<IAPIResponseArtistUpdate>(
    aPIResponseArtistUpdateSchema as unknown as IJSONSchema,
  );
