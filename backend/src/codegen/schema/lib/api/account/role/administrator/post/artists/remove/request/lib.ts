import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPostArtistsRemove } from "./types.js";

export const aPIRequestPostArtistsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/artists/remove/request.schema.json",
  title: "APIRequestPostArtistsRemove",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestPostArtistsRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestPostArtistsRemove =
  createValidator<IAPIRequestPostArtistsRemove>(
    aPIRequestPostArtistsRemoveSchema as unknown as IJSONSchema,
  );
