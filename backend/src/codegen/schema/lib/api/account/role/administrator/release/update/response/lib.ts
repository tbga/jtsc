import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleaseUpdate } from "./types.js";

export const aPIResponseReleaseUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/update/response.schema.json",
  title: "APIResponseReleaseUpdate",
  description: "Updated release.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseReleaseUpdateSchema as unknown as IJSONSchema);

export const validateAPIResponseReleaseUpdate =
  createValidator<IAPIResponseReleaseUpdate>(
    aPIResponseReleaseUpdateSchema as unknown as IJSONSchema,
  );
