export {
  aPIResponseProfileArtistsRemoveSchema,
  validateAPIResponseProfileArtistsRemove,
} from "./lib.js";
export type { IAPIResponseProfileArtistsRemove } from "./types.js";
