import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleaseProfilesRemove } from "./types.js";

export const aPIResponseReleaseProfilesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/release/profiles/remove/response.schema.json",
  title: "APIResponseReleaseProfilesRemove",
  description: "Removed release profiles.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed profiles.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseReleaseProfilesRemoveSchema as unknown as IJSONSchema);

export const validateAPIResponseReleaseProfilesRemove =
  createValidator<IAPIResponseReleaseProfilesRemove>(
    aPIResponseReleaseProfilesRemoveSchema as unknown as IJSONSchema,
  );
