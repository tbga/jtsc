export {
  aPIResponseReleasePostsAddSchema,
  validateAPIResponseReleasePostsAdd,
} from "./lib.js";
export type { IAPIResponseReleasePostsAdd } from "./types.js";
