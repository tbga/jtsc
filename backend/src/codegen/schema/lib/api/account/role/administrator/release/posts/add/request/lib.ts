import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleasePostsAdd } from "./types.js";

export const aPIRequestReleasePostsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/add/request.schema.json",
  title: "APIRequestReleasePostsAdd",
  description: "Add posts to a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of post IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestReleasePostsAddSchema as unknown as IJSONSchema);

export const validateAPIRequestReleasePostsAdd =
  createValidator<IAPIRequestReleasePostsAdd>(
    aPIRequestReleasePostsAddSchema as unknown as IJSONSchema,
  );
