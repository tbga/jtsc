import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseOperationCancel } from "./types.js";

export const aPIResponseOperationCancelSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/operation/cancel/response.schema.json",
  $comment: "@TODO change data into a proper operation entity after rework.",
  title: "APIResponseOperationCancel",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
addSchema(aPIResponseOperationCancelSchema as unknown as IJSONSchema);

export const validateAPIResponseOperationCancel =
  createValidator<IAPIResponseOperationCancel>(
    aPIResponseOperationCancelSchema as unknown as IJSONSchema,
  );
