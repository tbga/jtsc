import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileCreate } from "./types.js";

export const aPIResponseProfileCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/create/response.schema.json",
  title: "APIResponseProfileCreate",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
addSchema(aPIResponseProfileCreateSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileCreate =
  createValidator<IAPIResponseProfileCreate>(
    aPIResponseProfileCreateSchema as unknown as IJSONSchema,
  );
