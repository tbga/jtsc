import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileUpdate } from "./types.js";

export const aPIResponseProfileUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/update/response.schema.json",
  title: "APIResponseProfileUpdate",
  description: "Added profile releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseProfileUpdateSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileUpdate =
  createValidator<IAPIResponseProfileUpdate>(
    aPIResponseProfileUpdateSchema as unknown as IJSONSchema,
  );
