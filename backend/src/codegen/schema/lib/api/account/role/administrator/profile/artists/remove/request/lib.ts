import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestProfileArtistsRemove } from "./types.js";

export const aPIRequestProfileArtistsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/remove/request.schema.json",
  title: "APIRequestProfileArtistsRemove",
  description: "Remove profile artists request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestProfileArtistsRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestProfileArtistsRemove =
  createValidator<IAPIRequestProfileArtistsRemove>(
    aPIRequestProfileArtistsRemoveSchema as unknown as IJSONSchema,
  );
