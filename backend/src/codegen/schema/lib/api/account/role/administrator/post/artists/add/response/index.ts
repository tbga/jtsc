export {
  aPIResponsePostArtistsAddSchema,
  validateAPIResponsePostArtistsAdd,
} from "./lib.js";
export type { IAPIResponsePostArtistsAdd } from "./types.js";
