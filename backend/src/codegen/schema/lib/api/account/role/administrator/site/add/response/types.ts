import { type ISite } from "#codegen/schema/lib/entities/site/entity";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSiteAdd {
  is_successful: true;
  data: ISite;
}
