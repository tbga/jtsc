export {
  aPIResponseProfileCreateSchema,
  validateAPIResponseProfileCreate,
} from "./lib.js";
export type { IAPIResponseProfileCreate } from "./types.js";
