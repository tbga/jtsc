import { type ISiteUpdate } from "#codegen/schema/lib/entities/site/update";

/**
 * Site update request.
 */
export interface IAPIRequestSiteUpdate {
  data: ISiteUpdate;
}
