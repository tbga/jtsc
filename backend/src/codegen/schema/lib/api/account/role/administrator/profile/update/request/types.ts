import { type IProfileUpdate } from "#codegen/schema/lib/entities/profile/update";

/**
 * Profile's update
 */
export interface IAPIRequestProfileUpdate {
  data: IProfileUpdate;
}
