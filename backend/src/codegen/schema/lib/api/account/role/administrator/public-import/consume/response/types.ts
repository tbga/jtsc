import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * An entity item for consumption operation.
 */
export interface IAPIResponsePublicImportConsume {
  is_successful: true;
  data: IEntityItem;
}
