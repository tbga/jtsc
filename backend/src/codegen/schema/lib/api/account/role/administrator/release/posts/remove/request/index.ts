export {
  aPIRequestReleasePostsRemoveSchema,
  validateAPIRequestReleasePostsRemove,
} from "./lib.js";
export type { IAPIRequestReleasePostsRemove } from "./types.js";
