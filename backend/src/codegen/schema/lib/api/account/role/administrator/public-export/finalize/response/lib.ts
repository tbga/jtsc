import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportFinalize } from "./types.js";

export const aPIResponsePublicExportFinalizeSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/finalize/response.schema.json",
  title: "APIResponsePublicExportFinalize",
  description: "Response of the public export finalization.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicExportFinalizeSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportFinalize =
  createValidator<IAPIResponsePublicExportFinalize>(
    aPIResponsePublicExportFinalizeSchema as unknown as IJSONSchema,
  );
