import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleaseProfilesRemove } from "./types.js";

export const aPIRequestReleaseProfilesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/release/profiles/remove/request.schema.json",
  title: "APIRequestReleaseProfilesRemove",
  description: "Remove profiles from a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of profile IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestReleaseProfilesRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestReleaseProfilesRemove =
  createValidator<IAPIRequestReleaseProfilesRemove>(
    aPIRequestReleaseProfilesRemoveSchema as unknown as IJSONSchema,
  );
