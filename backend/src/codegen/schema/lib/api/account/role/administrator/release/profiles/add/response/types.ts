import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Added release profiles.
 */
export interface IAPIResponseReleaseProfilesAdd {
  is_successful: true;
  /**
   * A list of added profiles.
   *
   * @minItems 1
   */
  data: [IEntityItem, ...IEntityItem[]];
}
