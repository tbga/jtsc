import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseOperationCancel {
  is_successful: true;
  data: IBigSerialInteger;
}
