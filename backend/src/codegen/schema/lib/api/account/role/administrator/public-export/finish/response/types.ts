import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Response of the finished public export. Returns an operation item.
 */
export interface IAPIResponsePublicExportFinish {
  is_successful: true;
  data: IEntityItem;
}
