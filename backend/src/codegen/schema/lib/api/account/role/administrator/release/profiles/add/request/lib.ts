import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleaseProfilesAdd } from "./types.js";

export const aPIRequestReleaseProfilesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/profiles/add/request.schema.json",
  title: "APIRequestReleaseProfilesAdd",
  description: "Add profiles to a release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of profile IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestReleaseProfilesAddSchema as unknown as IJSONSchema);

export const validateAPIRequestReleaseProfilesAdd =
  createValidator<IAPIRequestReleaseProfilesAdd>(
    aPIRequestReleaseProfilesAddSchema as unknown as IJSONSchema,
  );
