export {
  aPIResponseReleaseProfilesRemoveSchema,
  validateAPIResponseReleaseProfilesRemove,
} from "./lib.js";
export type { IAPIResponseReleaseProfilesRemove } from "./types.js";
