import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicImportCreate } from "./types.js";

export const aPIResponsePublicImportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/create/response.schema.json",
  title: "APIResponsePublicImportCreate",
  description: "An entity item for creaton operation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicImportCreateSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicImportCreate =
  createValidator<IAPIResponsePublicImportCreate>(
    aPIResponsePublicImportCreateSchema as unknown as IJSONSchema,
  );
