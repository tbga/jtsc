export {
  aPIRequestPostReleasesRemoveSchema,
  validateAPIRequestPostReleasesRemove,
} from "./lib.js";
export type { IAPIRequestPostReleasesRemove } from "./types.js";
