export {
  aPIResponseSiteUpdateSchema,
  validateAPIResponseSiteUpdate,
} from "./lib.js";
export type { IAPIResponseSiteUpdate } from "./types.js";
