import { type IPublicExportUpdate } from "#codegen/schema/lib/entities/public-export/update";

/**
 * Public export update.
 */
export interface IAPIRequestPublicExportUpdate {
  data: IPublicExportUpdate;
}
