import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseOperationsStats {
  is_successful: true;
  /**
   * Operations overview.
   */
  data: {
    all: IBigIntegerPositive;
    pending: IBigIntegerPositive;
    in_progress: IBigIntegerPositive;
    finished: IBigIntegerPositive;
    failed: IBigIntegerPositive;
  };
}
