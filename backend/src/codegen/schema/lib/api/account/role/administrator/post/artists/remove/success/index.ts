export {
  aPIResponsePostArtistsRemoveSchema,
  validateAPIResponsePostArtistsRemove,
} from "./lib.js";
export type { IAPIResponsePostArtistsRemove } from "./types.js";
