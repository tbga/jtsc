import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleaseUpdate } from "./types.js";

export const aPIRequestReleaseUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/update/request.schema.json",
  title: "APIRequestReleaseUpdate",
  description: "Update release.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/update.schema.json",
    },
  },
} as const;
addSchema(aPIRequestReleaseUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestReleaseUpdate =
  createValidator<IAPIRequestReleaseUpdate>(
    aPIRequestReleaseUpdateSchema as unknown as IJSONSchema,
  );
