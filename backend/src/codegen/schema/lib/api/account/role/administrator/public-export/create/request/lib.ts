import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicExportCreate } from "./types.js";

export const aPIRequestPublicExportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/create/request.schema.json",
  title: "APIRequestPublicExportCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/init.schema.json",
    },
  },
} as const;
addSchema(aPIRequestPublicExportCreateSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicExportCreate =
  createValidator<IAPIRequestPublicExportCreate>(
    aPIRequestPublicExportCreateSchema as unknown as IJSONSchema,
  );
