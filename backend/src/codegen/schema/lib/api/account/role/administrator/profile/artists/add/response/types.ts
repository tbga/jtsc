import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Added profile artists.
 */
export interface IAPIResponseProfileArtistsAdd {
  is_successful: true;
  /**
   * A list of added artists.
   */
  data: IEntityItem[];
}
