import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Body of the API request.
 */
export interface IAPIRequestPostArtistsRemove {
  /**
   * A list of artist IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
