import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPostArtistsAdd } from "./types.js";

export const aPIRequestPostArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/artists/add/request.schema.json",
  title: "APIRequestPostArtistsAdd",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestPostArtistsAddSchema as unknown as IJSONSchema);

export const validateAPIRequestPostArtistsAdd =
  createValidator<IAPIRequestPostArtistsAdd>(
    aPIRequestPostArtistsAddSchema as unknown as IJSONSchema,
  );
