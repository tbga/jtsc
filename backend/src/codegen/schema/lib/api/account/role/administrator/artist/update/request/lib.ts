import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestArtistUpdate } from "./types.js";

export const aPIRequestArtistUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/artist/update/request.schema.json",
  title: "APIRequestArtistUpdate",
  description: "Update artist.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/artist/update.schema.json",
    },
  },
} as const;
addSchema(aPIRequestArtistUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestArtistUpdate =
  createValidator<IAPIRequestArtistUpdate>(
    aPIRequestArtistUpdateSchema as unknown as IJSONSchema,
  );
