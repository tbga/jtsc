import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicImportConsume } from "./types.js";

export const aPIResponsePublicImportConsumeSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/consume/response.schema.json",
  title: "APIResponsePublicImportConsume",
  description: "An entity item for consumption operation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicImportConsumeSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicImportConsume =
  createValidator<IAPIResponsePublicImportConsume>(
    aPIResponsePublicImportConsumeSchema as unknown as IJSONSchema,
  );
