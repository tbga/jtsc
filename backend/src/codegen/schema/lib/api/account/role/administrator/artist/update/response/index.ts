export {
  aPIResponseArtistUpdateSchema,
  validateAPIResponseArtistUpdate,
} from "./lib.js";
export type { IAPIResponseArtistUpdate } from "./types.js";
