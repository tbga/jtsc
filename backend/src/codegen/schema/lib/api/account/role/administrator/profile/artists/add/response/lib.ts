import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileArtistsAdd } from "./types.js";

export const aPIResponseProfileArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/add/response.schema.json",
  title: "APIResponseProfileArtistsAdd",
  description: "Added profile artists.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added artists.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseProfileArtistsAddSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileArtistsAdd =
  createValidator<IAPIResponseProfileArtistsAdd>(
    aPIResponseProfileArtistsAddSchema as unknown as IJSONSchema,
  );
