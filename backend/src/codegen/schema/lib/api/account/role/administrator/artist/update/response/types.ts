import { type IArtist } from "#codegen/schema/lib/entities/artist/entity";

/**
 * Updated artist.
 */
export interface IAPIResponseArtistUpdate {
  is_successful: true;
  data: IArtist;
}
