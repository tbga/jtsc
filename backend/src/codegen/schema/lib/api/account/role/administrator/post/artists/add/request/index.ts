export {
  aPIRequestPostArtistsAddSchema,
  validateAPIRequestPostArtistsAdd,
} from "./lib.js";
export type { IAPIRequestPostArtistsAdd } from "./types.js";
