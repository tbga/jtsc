import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Response of the release creation.
 */
export interface IAPIResponseReleaseCreate {
  is_successful: true;
  data: IEntityItem;
}
