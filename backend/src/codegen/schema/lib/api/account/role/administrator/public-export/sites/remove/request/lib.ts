import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicExportSitesRemove } from "./types.js";

export const aPIRequestPublicExportSitesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/remove/request.schema.json",
  title: "APIRequestPublicExportSitesRemove",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of site IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestPublicExportSitesRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicExportSitesRemove =
  createValidator<IAPIRequestPublicExportSitesRemove>(
    aPIRequestPublicExportSitesRemoveSchema as unknown as IJSONSchema,
  );
