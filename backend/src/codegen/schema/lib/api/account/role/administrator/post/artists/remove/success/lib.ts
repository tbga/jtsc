import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostArtistsRemove } from "./types.js";

export const aPIResponsePostArtistsRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/artists/remove/response.schema.json",
  title: "APIResponsePostArtistsRemove",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed artist IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponsePostArtistsRemoveSchema as unknown as IJSONSchema);

export const validateAPIResponsePostArtistsRemove =
  createValidator<IAPIResponsePostArtistsRemove>(
    aPIResponsePostArtistsRemoveSchema as unknown as IJSONSchema,
  );
