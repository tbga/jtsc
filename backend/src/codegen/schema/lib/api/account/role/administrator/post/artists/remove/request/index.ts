export {
  aPIRequestPostArtistsRemoveSchema,
  validateAPIRequestPostArtistsRemove,
} from "./lib.js";
export type { IAPIRequestPostArtistsRemove } from "./types.js";
