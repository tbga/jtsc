import { type IPublicImportStats } from "#codegen/schema/lib/entities/public-import/stats";

export interface IAPIResponsePublicImportStats {
  is_successful: true;
  data: IPublicImportStats;
}
