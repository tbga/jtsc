import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestReleaseCreate } from "./types.js";

export const aPIRequestReleaseCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/create/request.schema.json",
  title: "APIRequestReleaseCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
    },
  },
} as const;
addSchema(aPIRequestReleaseCreateSchema as unknown as IJSONSchema);

export const validateAPIRequestReleaseCreate =
  createValidator<IAPIRequestReleaseCreate>(
    aPIRequestReleaseCreateSchema as unknown as IJSONSchema,
  );
