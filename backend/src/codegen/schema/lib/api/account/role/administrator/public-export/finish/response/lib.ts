import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportFinish } from "./types.js";

export const aPIResponsePublicExportFinishSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/finish/response.schema.json",
  title: "APIResponsePublicExportFinish",
  description:
    "Response of the finished public export. Returns an operation item.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicExportFinishSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportFinish =
  createValidator<IAPIResponsePublicExportFinish>(
    aPIResponsePublicExportFinishSchema as unknown as IJSONSchema,
  );
