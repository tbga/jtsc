import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePublicExportSitesRemove {
  is_successful: true;
  /**
   * A list of removed site IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
