export {
  aPIRequestProfileUpdateSchema,
  validateAPIRequestProfileUpdate,
} from "./lib.js";
export type { IAPIRequestProfileUpdate } from "./types.js";
