import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleasePostsAdd } from "./types.js";

export const aPIResponseReleasePostsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/add/response.schema.json",
  title: "APIResponseReleasePostsAdd",
  description: "Added release posts.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added posts.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseReleasePostsAddSchema as unknown as IJSONSchema);

export const validateAPIResponseReleasePostsAdd =
  createValidator<IAPIResponseReleasePostsAdd>(
    aPIResponseReleasePostsAddSchema as unknown as IJSONSchema,
  );
