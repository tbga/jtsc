export {
  aPIResponseProfileArtistsAddSchema,
  validateAPIResponseProfileArtistsAdd,
} from "./lib.js";
export type { IAPIResponseProfileArtistsAdd } from "./types.js";
