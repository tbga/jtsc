import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Response of the public export creation.
 */
export interface IAPIResponsePublicExportCreate {
  is_successful: true;
  data: IEntityItem;
}
