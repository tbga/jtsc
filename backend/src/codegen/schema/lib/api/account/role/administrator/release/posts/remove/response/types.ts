import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Removed release posts.
 */
export interface IAPIResponseReleasePosts {
  is_successful: true;
  /**
   * A list of removed posts.
   *
   * @minItems 1
   */
  data: [IEntityItem, ...IEntityItem[]];
}
