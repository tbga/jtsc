import { type IReleaseInit } from "#codegen/schema/lib/entities/release/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestReleaseCreate {
  data: IReleaseInit;
}
