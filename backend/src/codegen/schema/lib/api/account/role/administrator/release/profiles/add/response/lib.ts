import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleaseProfilesAdd } from "./types.js";

export const aPIResponseReleaseProfilesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/profiles/add/response.schema.json",
  title: "APIResponseReleaseProfilesAdd",
  description: "Added release profiles.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added profiles.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseReleaseProfilesAddSchema as unknown as IJSONSchema);

export const validateAPIResponseReleaseProfilesAdd =
  createValidator<IAPIResponseReleaseProfilesAdd>(
    aPIResponseReleaseProfilesAddSchema as unknown as IJSONSchema,
  );
