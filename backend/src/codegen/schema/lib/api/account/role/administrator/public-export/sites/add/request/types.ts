import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Add published sites to the public export.
 */
export interface IAPIRequestPublicExportSitesAdd {
  /**
   * A list of site IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
