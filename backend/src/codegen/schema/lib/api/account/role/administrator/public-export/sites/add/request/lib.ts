import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicExportSitesAdd } from "./types.js";

export const aPIRequestPublicExportSitesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/add/request.schema.json",
  title: "APIRequestPublicExportSitesAdd",
  description: "Add published sites to the public export.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of site IDs.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestPublicExportSitesAddSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicExportSitesAdd =
  createValidator<IAPIRequestPublicExportSitesAdd>(
    aPIRequestPublicExportSitesAddSchema as unknown as IJSONSchema,
  );
