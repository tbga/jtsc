import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportSitesRemove } from "./types.js";

export const aPIResponsePublicExportSitesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/remove/response.schema.json",
  title: "APIResponsePublicExportSitesRemove",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed site IDs.",
      type: "array",
      uniqueItems: true,
      minItems: 1,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponsePublicExportSitesRemoveSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportSitesRemove =
  createValidator<IAPIResponsePublicExportSitesRemove>(
    aPIResponsePublicExportSitesRemoveSchema as unknown as IJSONSchema,
  );
