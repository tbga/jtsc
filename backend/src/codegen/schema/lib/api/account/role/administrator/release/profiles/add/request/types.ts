import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Add profiles to a release.
 */
export interface IAPIRequestReleaseProfilesAdd {
  /**
   * A list of profile IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
