export {
  aPIRequestReleaseProfilesRemoveSchema,
  validateAPIRequestReleaseProfilesRemove,
} from "./lib.js";
export type { IAPIRequestReleaseProfilesRemove } from "./types.js";
