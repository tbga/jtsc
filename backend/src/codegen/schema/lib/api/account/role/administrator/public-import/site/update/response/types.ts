import { type IPublicImportSite } from "#codegen/schema/lib/entities/public-import/site/entity";

export interface IAPIResponsePublicImportSiteUpdate {
  is_successful: true;
  data: IPublicImportSite;
}
