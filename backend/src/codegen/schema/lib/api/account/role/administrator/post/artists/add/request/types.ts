import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Body of the API request.
 */
export interface IAPIRequestPostArtistsAdd {
  /**
   * A list of artist IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
