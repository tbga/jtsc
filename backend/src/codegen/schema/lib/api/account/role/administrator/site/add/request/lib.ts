import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestSiteAdd } from "./types.js";

export const aPIRequestSiteAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/add/request.schema.json",
  title: "APIRequestSiteAdd",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/init.schema.json",
    },
  },
} as const;
addSchema(aPIRequestSiteAddSchema as unknown as IJSONSchema);

export const validateAPIRequestSiteAdd = createValidator<IAPIRequestSiteAdd>(
  aPIRequestSiteAddSchema as unknown as IJSONSchema,
);
