export {
  aPIResponsePostReleasesAddSchema,
  validateAPIResponsePostReleasesAdd,
} from "./lib.js";
export type { IAPIResponsePostReleasesAdd } from "./types.js";
