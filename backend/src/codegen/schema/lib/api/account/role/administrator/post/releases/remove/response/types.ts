import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * Removed psot releases.
 */
export interface IAPIResponsePostReleasesRemove {
  is_successful: true;
  /**
   * A list of removed releases.
   */
  data: IReleasePreview[];
}
