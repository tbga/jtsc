import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPostReleasesRemove } from "./types.js";

export const aPIRequestPostReleasesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/releases/remove/request.schema.json",
  title: "APIRequestPostReleasesRemove",
  description: "Remove post releases request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of release IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestPostReleasesRemoveSchema as unknown as IJSONSchema);

export const validateAPIRequestPostReleasesRemove =
  createValidator<IAPIRequestPostReleasesRemove>(
    aPIRequestPostReleasesRemoveSchema as unknown as IJSONSchema,
  );
