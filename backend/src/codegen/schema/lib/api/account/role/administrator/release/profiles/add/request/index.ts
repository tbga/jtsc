export {
  aPIRequestReleaseProfilesAddSchema,
  validateAPIRequestReleaseProfilesAdd,
} from "./lib.js";
export type { IAPIRequestReleaseProfilesAdd } from "./types.js";
