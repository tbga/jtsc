import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicExportUpdate } from "./types.js";

export const aPIRequestPublicExportUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/update/request.schema.json",
  title: "APIRequestPublicExportUpdate",
  description: "Public export update.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
    },
  },
} as const;
addSchema(aPIRequestPublicExportUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicExportUpdate =
  createValidator<IAPIRequestPublicExportUpdate>(
    aPIRequestPublicExportUpdateSchema as unknown as IJSONSchema,
  );
