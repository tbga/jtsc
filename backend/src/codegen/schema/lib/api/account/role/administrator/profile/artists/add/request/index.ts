export {
  aPIRequestProfileArtistsAddSchema,
  validateAPIRequestProfileArtistsAdd,
} from "./lib.js";
export type { IAPIRequestProfileArtistsAdd } from "./types.js";
