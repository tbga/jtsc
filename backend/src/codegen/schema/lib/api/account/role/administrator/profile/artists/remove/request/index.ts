export {
  aPIRequestProfileArtistsRemoveSchema,
  validateAPIRequestProfileArtistsRemove,
} from "./lib.js";
export type { IAPIRequestProfileArtistsRemove } from "./types.js";
