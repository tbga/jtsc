export interface IAPIRequestPublicImportSiteUpdate {
  data: {
    is_approved?: boolean;
  };
}
