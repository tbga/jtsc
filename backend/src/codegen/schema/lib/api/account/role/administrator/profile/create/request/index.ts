export {
  aPIRequestProfileCreateSchema,
  validateAPIRequestProfileCreate,
} from "./lib.js";
export type { IAPIRequestProfileCreate } from "./types.js";
