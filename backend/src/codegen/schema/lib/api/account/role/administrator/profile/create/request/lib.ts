import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestProfileCreate } from "./types.js";

export const aPIRequestProfileCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/create/request.schema.json",
  title: "APIRequestProfileCreate",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/profile/init.schema.json",
    },
  },
} as const;
addSchema(aPIRequestProfileCreateSchema as unknown as IJSONSchema);

export const validateAPIRequestProfileCreate =
  createValidator<IAPIRequestProfileCreate>(
    aPIRequestProfileCreateSchema as unknown as IJSONSchema,
  );
