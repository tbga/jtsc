import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportSitesAdd } from "./types.js";

export const aPIResponsePublicExportSitesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/sites/add/add/response.schema.json",
  title: "APIResponsePublicExportSitesAdd",
  description: "Added public export sites.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added sites.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/site/preview.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponsePublicExportSitesAddSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportSitesAdd =
  createValidator<IAPIResponsePublicExportSitesAdd>(
    aPIResponsePublicExportSitesAddSchema as unknown as IJSONSchema,
  );
