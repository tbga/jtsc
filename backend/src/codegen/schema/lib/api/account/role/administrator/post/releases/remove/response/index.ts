export {
  aPIResponsePostReleasesRemoveSchema,
  validateAPIResponsePostReleasesRemove,
} from "./lib.js";
export type { IAPIResponsePostReleasesRemove } from "./types.js";
