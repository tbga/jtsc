export {
  aPIRequestReleasePostsAddSchema,
  validateAPIRequestReleasePostsAdd,
} from "./lib.js";
export type { IAPIRequestReleasePostsAdd } from "./types.js";
