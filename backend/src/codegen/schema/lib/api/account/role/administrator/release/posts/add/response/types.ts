import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Added release posts.
 */
export interface IAPIResponseReleasePostsAdd {
  is_successful: true;
  /**
   * A list of added posts.
   *
   * @minItems 1
   */
  data: [IEntityItem, ...IEntityItem[]];
}
