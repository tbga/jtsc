import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestSiteUpdate } from "./types.js";

export const aPIRequestSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/update/request.schema.json",
  title: "APIRequestSiteUpdate",
  description: "Site update request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/update.schema.json",
    },
  },
} as const;
addSchema(aPIRequestSiteUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestSiteUpdate =
  createValidator<IAPIRequestSiteUpdate>(
    aPIRequestSiteUpdateSchema as unknown as IJSONSchema,
  );
