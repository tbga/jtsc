export {
  aPIResponseProfileUpdateSchema,
  validateAPIResponseProfileUpdate,
} from "./lib.js";
export type { IAPIResponseProfileUpdate } from "./types.js";
