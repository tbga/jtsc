import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestProfileArtistsAdd } from "./types.js";

export const aPIRequestProfileArtistsAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/artists/add/request.schema.json",
  title: "APIRequestProfileArtistsAdd",
  description: "Add profile artists request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      description: "A list of artist IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIRequestProfileArtistsAddSchema as unknown as IJSONSchema);

export const validateAPIRequestProfileArtistsAdd =
  createValidator<IAPIRequestProfileArtistsAdd>(
    aPIRequestProfileArtistsAddSchema as unknown as IJSONSchema,
  );
