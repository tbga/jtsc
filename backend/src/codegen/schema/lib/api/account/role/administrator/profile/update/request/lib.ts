import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestProfileUpdate } from "./types.js";

export const aPIRequestProfileUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/profile/update/request.schema.json",
  title: "APIRequestProfileUpdate",
  description: "Profile's update",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/profile/update.schema.json",
    },
  },
} as const;
addSchema(aPIRequestProfileUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestProfileUpdate =
  createValidator<IAPIRequestProfileUpdate>(
    aPIRequestProfileUpdateSchema as unknown as IJSONSchema,
  );
