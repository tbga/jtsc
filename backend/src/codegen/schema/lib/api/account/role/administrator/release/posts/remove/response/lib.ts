import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleasePosts } from "./types.js";

export const aPIResponseReleasePostsSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/release/posts/remove/response.schema.json",
  title: "APIResponseReleasePosts",
  description: "Removed release posts.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed posts.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/item.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseReleasePostsSchema as unknown as IJSONSchema);

export const validateAPIResponseReleasePosts =
  createValidator<IAPIResponseReleasePosts>(
    aPIResponseReleasePostsSchema as unknown as IJSONSchema,
  );
