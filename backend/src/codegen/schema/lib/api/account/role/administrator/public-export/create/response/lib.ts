import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportCreate } from "./types.js";

export const aPIResponsePublicExportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/create/response.schema.json",
  title: "APIResponsePublicExportCreate",
  description: "Response of the public export creation.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicExportCreateSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportCreate =
  createValidator<IAPIResponsePublicExportCreate>(
    aPIResponsePublicExportCreateSchema as unknown as IJSONSchema,
  );
