import { type IRelease } from "#codegen/schema/lib/entities/release/entity";

/**
 * Updated release.
 */
export interface IAPIResponseReleaseUpdate {
  is_successful: true;
  data: IRelease;
}
