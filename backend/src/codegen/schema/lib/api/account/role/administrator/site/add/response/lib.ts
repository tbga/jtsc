import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSiteAdd } from "./types.js";

export const aPIResponseSiteAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/site/add/response.schema.json",
  title: "APIResponseSiteAdd",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseSiteAddSchema as unknown as IJSONSchema);

export const validateAPIResponseSiteAdd = createValidator<IAPIResponseSiteAdd>(
  aPIResponseSiteAddSchema as unknown as IJSONSchema,
);
