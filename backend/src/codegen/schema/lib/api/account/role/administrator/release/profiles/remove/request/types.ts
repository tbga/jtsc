import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Remove profiles from a release.
 */
export interface IAPIRequestReleaseProfilesRemove {
  /**
   * A list of profile IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
