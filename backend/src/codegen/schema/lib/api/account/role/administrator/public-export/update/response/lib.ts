import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePublicExportUpdate } from "./types.js";

export const aPIResponsePublicExportUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-export/update/response.schema.json",
  title: "APIResponsePublicExportUpdate",
  description: "Updated public export.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/public-export/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePublicExportUpdateSchema as unknown as IJSONSchema);

export const validateAPIResponsePublicExportUpdate =
  createValidator<IAPIResponsePublicExportUpdate>(
    aPIResponsePublicExportUpdateSchema as unknown as IJSONSchema,
  );
