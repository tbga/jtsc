import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Add posts to a release.
 */
export interface IAPIRequestReleasePostsAdd {
  /**
   * A list of post IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
