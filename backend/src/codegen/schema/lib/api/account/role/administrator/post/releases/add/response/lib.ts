import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostReleasesAdd } from "./types.js";

export const aPIResponsePostReleasesAddSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/releases/add/response.schema.json",
  title: "APIResponsePostReleasesAdd",
  description: "Added post releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of added releases.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponsePostReleasesAddSchema as unknown as IJSONSchema);

export const validateAPIResponsePostReleasesAdd =
  createValidator<IAPIResponsePostReleasesAdd>(
    aPIResponsePostReleasesAddSchema as unknown as IJSONSchema,
  );
