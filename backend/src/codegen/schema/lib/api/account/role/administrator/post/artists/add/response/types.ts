import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePostArtistsAdd {
  is_successful: true;
  /**
   * A list of added artists.
   *
   * @minItems 1
   */
  data: [IEntityItem, ...IEntityItem[]];
}
