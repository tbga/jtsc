import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostReleasesRemove } from "./types.js";

export const aPIResponsePostReleasesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/post/releases/remove/response.schema.json",
  title: "APIResponsePostReleasesRemove",
  description: "Removed psot releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed releases.",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponsePostReleasesRemoveSchema as unknown as IJSONSchema);

export const validateAPIResponsePostReleasesRemove =
  createValidator<IAPIResponsePostReleasesRemove>(
    aPIResponsePostReleasesRemoveSchema as unknown as IJSONSchema,
  );
