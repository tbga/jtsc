export {
  aPIResponseReleaseProfilesAddSchema,
  validateAPIResponseReleaseProfilesAdd,
} from "./lib.js";
export type { IAPIResponseReleaseProfilesAdd } from "./types.js";
