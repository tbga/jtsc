import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Removed release profiles.
 */
export interface IAPIResponseReleaseProfilesRemove {
  is_successful: true;
  /**
   * A list of removed profiles.
   *
   * @minItems 1
   */
  data: [IEntityItem, ...IEntityItem[]];
}
