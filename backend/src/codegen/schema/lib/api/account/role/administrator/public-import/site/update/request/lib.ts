import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicImportSiteUpdate } from "./types.js";

export const aPIRequestPublicImportSiteUpdateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/site/update/request.schema.json",
  title: "APIRequestPublicImportSiteUpdate",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        is_approved: {
          type: "boolean",
        },
      },
    },
  },
} as const;
addSchema(aPIRequestPublicImportSiteUpdateSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicImportSiteUpdate =
  createValidator<IAPIRequestPublicImportSiteUpdate>(
    aPIRequestPublicImportSiteUpdateSchema as unknown as IJSONSchema,
  );
