import { type IReleaseUpdate } from "#codegen/schema/lib/entities/release/update";

/**
 * Update release.
 */
export interface IAPIRequestReleaseUpdate {
  data: IReleaseUpdate;
}
