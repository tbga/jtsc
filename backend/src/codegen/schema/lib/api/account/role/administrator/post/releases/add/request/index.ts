export {
  aPIRequestPostReleasesAddSchema,
  validateAPIRequestPostReleasesAdd,
} from "./lib.js";
export type { IAPIRequestPostReleasesAdd } from "./types.js";
