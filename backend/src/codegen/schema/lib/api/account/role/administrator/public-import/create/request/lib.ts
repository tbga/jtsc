import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestPublicImportCreate } from "./types.js";

export const aPIRequestPublicImportCreateSchema = {
  $id: "https://jtsc-schemas.org/api/account/role/administrator/public-import/create/request.schema.json",
  title: "APIRequestPublicImportCreate",
  description: "A path to the public export folder.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
    },
  },
} as const;
addSchema(aPIRequestPublicImportCreateSchema as unknown as IJSONSchema);

export const validateAPIRequestPublicImportCreate =
  createValidator<IAPIRequestPublicImportCreate>(
    aPIRequestPublicImportCreateSchema as unknown as IJSONSchema,
  );
