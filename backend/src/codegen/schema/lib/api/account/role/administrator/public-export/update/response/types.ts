import { type IPublicExport } from "#codegen/schema/lib/entities/public-export/entity";

/**
 * Updated public export.
 */
export interface IAPIResponsePublicExportUpdate {
  is_successful: true;
  data: IPublicExport;
}
