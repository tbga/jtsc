import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePostArtistsRemove {
  is_successful: true;
  /**
   * A list of removed artist IDs.
   *
   * @minItems 1
   */
  data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
