import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseUserAgent {
  is_successful: true;
  data: INonEmptyString;
}
