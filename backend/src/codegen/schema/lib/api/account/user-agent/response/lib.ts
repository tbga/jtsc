import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseUserAgent } from "./types.js";

export const aPIResponseUserAgentSchema = {
  $id: "https://jtsc-schemas.org/api/account/user-agent/response.schema.json",
  title: "APIResponseUserAgent",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
  },
} as const;
addSchema(aPIResponseUserAgentSchema as unknown as IJSONSchema);

export const validateAPIResponseUserAgent =
  createValidator<IAPIResponseUserAgent>(
    aPIResponseUserAgentSchema as unknown as IJSONSchema,
  );
