export {
  aPIResponseUserAgentSchema,
  validateAPIResponseUserAgent,
} from "./lib.js";
export type { IAPIResponseUserAgent } from "./types.js";
