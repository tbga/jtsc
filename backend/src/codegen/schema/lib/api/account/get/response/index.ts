export {
  aPIResponseAccountGetSchema,
  validateAPIResponseAccountGet,
} from "./lib.js";
export type { IAPIResponseAccountGet } from "./types.js";
