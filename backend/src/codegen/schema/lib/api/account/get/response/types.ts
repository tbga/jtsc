import { type IAccount } from "#codegen/schema/lib/entities/account/entity";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseAccountGet {
  is_successful: true;
  data: IAccount;
}
