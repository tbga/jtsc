export { aPISearchArtistsSchema, validateAPISearchArtists } from "./lib.js";
export type { IAPISearchArtists } from "./types.js";
