import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSearchPostItems {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * A list of post items.
     */
    posts: IEntityItem[];
  };
}
