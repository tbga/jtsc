import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPISearchSiteItems } from "./types.js";

export const aPISearchSiteItemsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/sites/items/response.schema.json",
  title: "APISearchSiteItems",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "files"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        files: {
          description: "A list of file items.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPISearchSiteItemsSchema as unknown as IJSONSchema);

export const validateAPISearchSiteItems = createValidator<IAPISearchSiteItems>(
  aPISearchSiteItemsSchema as unknown as IJSONSchema,
);
