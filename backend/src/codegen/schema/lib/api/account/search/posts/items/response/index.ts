export {
  aPIResponseSearchPostItemsSchema,
  validateAPIResponseSearchPostItems,
} from "./lib.js";
export type { IAPIResponseSearchPostItems } from "./types.js";
