import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IProfilePreview } from "#codegen/schema/lib/entities/profile/preview";

/**
 * Body of the successful API response.
 */
export interface IAPISearchProfiles {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * A list of profile previews.
     *
     * @minItems 1
     */
    profiles: [IProfilePreview, ...IProfilePreview[]];
  };
}
