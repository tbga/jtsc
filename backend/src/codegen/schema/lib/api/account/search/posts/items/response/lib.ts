import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSearchPostItems } from "./types.js";

export const aPIResponseSearchPostItemsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/posts/items/response.schema.json",
  title: "APIResponseSearchPostItems",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "posts"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        posts: {
          description: "A list of post items.",
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseSearchPostItemsSchema as unknown as IJSONSchema);

export const validateAPIResponseSearchPostItems =
  createValidator<IAPIResponseSearchPostItems>(
    aPIResponseSearchPostItemsSchema as unknown as IJSONSchema,
  );
