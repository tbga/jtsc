import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSearchReleasePreviews {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * A list of release previews.
     *
     * @minItems 1
     */
    releases: [IReleasePreview, ...IReleasePreview[]];
  };
}
