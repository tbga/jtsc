export {
  aPISearchSitesResponseSchema,
  validateAPISearchSitesResponse,
} from "./lib.js";
export type { IAPISearchSitesResponse } from "./types.js";
