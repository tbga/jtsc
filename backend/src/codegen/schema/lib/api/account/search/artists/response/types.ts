import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IArtistPreview } from "#codegen/schema/lib/entities/artist/preview";

/**
 * Body of the successful API response.
 */
export interface IAPISearchArtists {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * A list of artist previews.
     *
     * @minItems 1
     */
    artists: [IArtistPreview, ...IArtistPreview[]];
  };
}
