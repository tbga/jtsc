export {
  aPIResponseSearchPostPreviewsSchema,
  validateAPIResponseSearchPostPreviews,
} from "./lib.js";
export type { IAPIResponseSearchPostPreviews } from "./types.js";
