import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPISearchSitePreviews } from "./types.js";

export const aPISearchSitePreviewsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/sites/previews/response.schema.json",
  title: "APISearchSitePreviews",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "files"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        files: {
          description: "A list of file previews.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/site/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPISearchSitePreviewsSchema as unknown as IJSONSchema);

export const validateAPISearchSitePreviews =
  createValidator<IAPISearchSitePreviews>(
    aPISearchSitePreviewsSchema as unknown as IJSONSchema,
  );
