export { aPISearchProfilesSchema, validateAPISearchProfiles } from "./lib.js";
export type { IAPISearchProfiles } from "./types.js";
