import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";

/**
 * Body of the successful API response.
 */
export interface IAPISearchSitesResponse {
  is_successful: true;
  data: IPagination;
}
