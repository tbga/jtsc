import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSearchReleaseItems } from "./types.js";

export const aPIResponseSearchReleaseItemsSchema = {
  $id: "https://jtsc-schemas.org/api/account/search/releases/items/response.schema.json",
  title: "APIResponseSearchReleaseItems",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "releases"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        releases: {
          description: "A list of release items.",
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseSearchReleaseItemsSchema as unknown as IJSONSchema);

export const validateAPIResponseSearchReleaseItems =
  createValidator<IAPIResponseSearchReleaseItems>(
    aPIResponseSearchReleaseItemsSchema as unknown as IJSONSchema,
  );
