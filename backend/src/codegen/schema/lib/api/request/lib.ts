import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestStats } from "./types.js";

export const aPIRequestStatsSchema = {
  $id: "https://jtsc-schemas.org/api/stats/request.schema.json",
  title: "APIRequestStats",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
    },
  },
} as const;
addSchema(aPIRequestStatsSchema as unknown as IJSONSchema);

export const validateAPIRequestStats = createValidator<IAPIRequestStats>(
  aPIRequestStatsSchema as unknown as IJSONSchema,
);
