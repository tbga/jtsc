import { type IJSONAny } from "#codegen/schema/lib/types/json/any";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSuccess {
  is_successful: true;
  data: IJSONAny;
}
