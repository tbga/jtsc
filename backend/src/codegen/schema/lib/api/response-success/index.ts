export { aPIResponseSuccessSchema, validateAPIResponseSuccess } from "./lib.js";
export type { IAPIResponseSuccess } from "./types.js";
