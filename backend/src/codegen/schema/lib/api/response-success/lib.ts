import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSuccess } from "./types.js";

export const aPIResponseSuccessSchema = {
  $id: "https://jtsc-schemas.org/api/response-success.schema.json",
  title: "APIResponseSuccess",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
    },
  },
} as const;
addSchema(aPIResponseSuccessSchema as unknown as IJSONSchema);

export const validateAPIResponseSuccess = createValidator<IAPIResponseSuccess>(
  aPIResponseSuccessSchema as unknown as IJSONSchema,
);
