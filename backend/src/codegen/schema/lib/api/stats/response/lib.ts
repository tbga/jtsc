import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseStats } from "./types.js";

export const aPIResponseStatsSchema = {
  $id: "https://jtsc-schemas.org/api/stats/response.schema.json",
  title: "APIResponseStats",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/stats/all.schema.json",
    },
  },
} as const;
addSchema(aPIResponseStatsSchema as unknown as IJSONSchema);

export const validateAPIResponseStats = createValidator<IAPIResponseStats>(
  aPIResponseStatsSchema as unknown as IJSONSchema,
);
