import { type IStatsAll } from "#codegen/schema/lib/stats/all";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseStats {
  is_successful: true;
  data: IStatsAll;
}
