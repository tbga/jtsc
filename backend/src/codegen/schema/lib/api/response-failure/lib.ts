import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseFailure } from "./types.js";

export const aPIResponseFailureSchema = {
  $id: "https://jtsc-schemas.org/api/response-failure.schema.json",
  title: "APIResponseFailure",
  description: "Body of the failed API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "errors"],
  properties: {
    is_successful: {
      type: "boolean",
      const: false,
    },
    errors: {
      type: "array",
      items: {
        type: "string",
      },
    },
  },
} as const;
addSchema(aPIResponseFailureSchema as unknown as IJSONSchema);

export const validateAPIResponseFailure = createValidator<IAPIResponseFailure>(
  aPIResponseFailureSchema as unknown as IJSONSchema,
);
