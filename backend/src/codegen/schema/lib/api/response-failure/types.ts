/**
 * Body of the failed API response.
 */
export interface IAPIResponseFailure {
  is_successful: false;
  errors: string[];
}
