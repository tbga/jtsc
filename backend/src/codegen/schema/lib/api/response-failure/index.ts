export { aPIResponseFailureSchema, validateAPIResponseFailure } from "./lib.js";
export type { IAPIResponseFailure } from "./types.js";
