export { aPIResponseSiteGetSchema, validateAPIResponseSiteGet } from "./lib.js";
export type { IAPIResponseSiteGet } from "./types.js";
