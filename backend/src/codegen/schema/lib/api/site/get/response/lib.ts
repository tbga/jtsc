import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSiteGet } from "./types.js";

export const aPIResponseSiteGetSchema = {
  $id: "https://jtsc-schemas.org/api/site/get/response.schema.json",
  title: "APIResponseSiteGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/site/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseSiteGetSchema as unknown as IJSONSchema);

export const validateAPIResponseSiteGet = createValidator<IAPIResponseSiteGet>(
  aPIResponseSiteGetSchema as unknown as IJSONSchema,
);
