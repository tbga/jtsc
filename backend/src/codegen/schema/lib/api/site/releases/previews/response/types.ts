import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSiteReleases {
  is_successful: true;
  data: {
    pagination: IPagination;
    releases: IReleasePreview[];
  };
}
