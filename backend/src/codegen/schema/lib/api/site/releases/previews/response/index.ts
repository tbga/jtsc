export {
  aPIResponseSiteReleasesSchema,
  validateAPIResponseSiteReleases,
} from "./lib.js";
export type { IAPIResponseSiteReleases } from "./types.js";
