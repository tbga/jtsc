export {
  aPIResponseReleaseProfilesSchema,
  validateAPIResponseReleaseProfiles,
} from "./lib.js";
export type { IAPIResponseReleaseProfiles } from "./types.js";
