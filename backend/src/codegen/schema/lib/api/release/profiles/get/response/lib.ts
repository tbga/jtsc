import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleaseProfiles } from "./types.js";

export const aPIResponseReleaseProfilesSchema = {
  $id: "https://jtsc-schemas.org/api/release/profiles/get/response.schema.json",
  title: "APIResponseReleaseProfiles",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "profiles"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        profiles: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/profile/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseReleaseProfilesSchema as unknown as IJSONSchema);

export const validateAPIResponseReleaseProfiles =
  createValidator<IAPIResponseReleaseProfiles>(
    aPIResponseReleaseProfilesSchema as unknown as IJSONSchema,
  );
