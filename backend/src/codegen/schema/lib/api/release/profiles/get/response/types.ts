import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IProfilePreview } from "#codegen/schema/lib/entities/profile/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseReleaseProfiles {
  is_successful: true;
  data: {
    pagination: IPagination;
    profiles: IProfilePreview[];
  };
}
