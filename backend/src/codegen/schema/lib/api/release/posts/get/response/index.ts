export {
  aPIResponseReleasePostsSchema,
  validateAPIResponseReleasePosts,
} from "./lib.js";
export type { IAPIResponseReleasePosts } from "./types.js";
