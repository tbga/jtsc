import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseReleasePosts } from "./types.js";

export const aPIResponseReleasePostsSchema = {
  $id: "https://jtsc-schemas.org/api/release/posts/get/response.schema.json",
  title: "APIResponseReleasePosts",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "posts"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        posts: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseReleasePostsSchema as unknown as IJSONSchema);

export const validateAPIResponseReleasePosts =
  createValidator<IAPIResponseReleasePosts>(
    aPIResponseReleasePostsSchema as unknown as IJSONSchema,
  );
