import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IGetReleaseResponse } from "./types.js";

export const getReleaseResponseSchema = {
  $id: "https://jtsc-schemas.org/api/release/get/response.schema.json",
  title: "GetReleaseResponse",
  description: "Release info.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/release/entity.schema.json",
    },
  },
} as const;
addSchema(getReleaseResponseSchema as unknown as IJSONSchema);

export const validateGetReleaseResponse = createValidator<IGetReleaseResponse>(
  getReleaseResponseSchema as unknown as IJSONSchema,
);
