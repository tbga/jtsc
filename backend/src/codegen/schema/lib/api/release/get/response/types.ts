import { type IRelease } from "#codegen/schema/lib/entities/release/entity";

/**
 * Release info.
 */
export interface IGetReleaseResponse {
  is_successful: true;
  data: IRelease;
}
