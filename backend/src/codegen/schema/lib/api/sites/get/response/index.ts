export {
  aPIResponseSitesGetSchema,
  validateAPIResponseSitesGet,
} from "./lib.js";
export type { IAPIResponseSitesGet } from "./types.js";
