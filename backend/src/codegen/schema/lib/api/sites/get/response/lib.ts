import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseSitesGet } from "./types.js";

export const aPIResponseSitesGetSchema = {
  $id: "https://jtsc-schemas.org/api/sites/get/response.schema.json",
  title: "APIResponseSitesGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "data"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        files: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/site/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseSitesGetSchema as unknown as IJSONSchema);

export const validateAPIResponseSitesGet =
  createValidator<IAPIResponseSitesGet>(
    aPIResponseSitesGetSchema as unknown as IJSONSchema,
  );
