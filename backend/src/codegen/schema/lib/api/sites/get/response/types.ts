import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type ISitePreview } from "#codegen/schema/lib/entities/site/preview";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseSitesGet {
  is_successful: true;
  data: {
    pagination: IPagination;
    /**
     * @minItems 1
     */
    files?: [ISitePreview, ...ISitePreview[]];
  };
}
