export {
  aPIResponsePostsGetSchema,
  validateAPIResponsePostsGet,
} from "./lib.js";
export type { IAPIResponsePostsGet } from "./types.js";
