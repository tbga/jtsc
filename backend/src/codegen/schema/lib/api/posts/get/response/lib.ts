import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostsGet } from "./types.js";

export const aPIResponsePostsGetSchema = {
  $id: "https://jtsc-schemas.org/api/posts/get/response.schema.json",
  title: "APIResponsePostsGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "posts"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        posts: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponsePostsGetSchema as unknown as IJSONSchema);

export const validateAPIResponsePostsGet =
  createValidator<IAPIResponsePostsGet>(
    aPIResponsePostsGetSchema as unknown as IJSONSchema,
  );
