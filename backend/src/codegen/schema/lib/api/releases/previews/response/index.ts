export {
  getReleasesResponseSchema,
  validateGetReleasesResponse,
} from "./lib.js";
export type { IGetReleasesResponse } from "./types.js";
