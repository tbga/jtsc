import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IGetReleasesResponse } from "./types.js";

export const getReleasesResponseSchema = {
  $id: "https://jtsc-schemas.org/api/releases/previews/response.schema.json",
  title: "GetReleasesResponse",
  description: "List of releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "releases"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        releases: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(getReleasesResponseSchema as unknown as IJSONSchema);

export const validateGetReleasesResponse =
  createValidator<IGetReleasesResponse>(
    getReleasesResponseSchema as unknown as IJSONSchema,
  );
