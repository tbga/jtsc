export {
  aPIResponsePaginatedSchema,
  validateAPIResponsePaginated,
} from "./lib.js";
export type { IAPIResponsePaginated } from "./types.js";
