import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePaginated } from "./types.js";

export const aPIResponsePaginatedSchema = {
  $id: "https://jtsc-schemas.org/api/paginated-response.schema.json",
  title: "APIResponsePaginated",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "items"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        items: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/types/json/any.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponsePaginatedSchema as unknown as IJSONSchema);

export const validateAPIResponsePaginated =
  createValidator<IAPIResponsePaginated>(
    aPIResponsePaginatedSchema as unknown as IJSONSchema,
  );
