import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Removed profile names.
 */
export interface IAPIResponseProfileNamesRemove {
  is_successful: true;
  /**
   * A list of removed name IDs.
   */
  data: IBigSerialInteger[];
}
