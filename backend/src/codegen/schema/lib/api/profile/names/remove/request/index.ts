export {
  aPIRequestProfileNamesRemoveSchema,
  validateAPIRequestProfileNamesRemove,
} from "./lib.js";
export type { IAPIRequestProfileNamesRemove } from "./types.js";
