export {
  aPIResponseProfileNamesAddSchema,
  validateAPIResponseProfileNamesAdd,
} from "./lib.js";
export type { IAPIResponseProfileNamesAdd } from "./types.js";
