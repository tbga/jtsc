import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileNamesRemove } from "./types.js";

export const aPIResponseProfileNamesRemoveSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/remove/response.schema.json",
  title: "APIResponseProfileNamesRemove",
  description: "Removed profile names.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      description: "A list of removed name IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(aPIResponseProfileNamesRemoveSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileNamesRemove =
  createValidator<IAPIResponseProfileNamesRemove>(
    aPIResponseProfileNamesRemoveSchema as unknown as IJSONSchema,
  );
