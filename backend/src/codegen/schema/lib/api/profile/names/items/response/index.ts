export {
  aPIResponseProfileNameItemsGetSchema,
  validateAPIResponseProfileNameItemsGet,
} from "./lib.js";
export type { IAPIResponseProfileNameItemsGet } from "./types.js";
