export {
  aPIRequestProfileNamesAddSchema,
  validateAPIRequestProfileNamesAdd,
} from "./lib.js";
export type { IAPIRequestProfileNamesAdd } from "./types.js";
