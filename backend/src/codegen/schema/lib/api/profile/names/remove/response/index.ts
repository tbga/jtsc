export {
  aPIResponseProfileNamesRemoveSchema,
  validateAPIResponseProfileNamesRemove,
} from "./lib.js";
export type { IAPIResponseProfileNamesRemove } from "./types.js";
