import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileNameItemsGet } from "./types.js";

export const aPIResponseProfileNameItemsGetSchema = {
  $id: "https://jtsc-schemas.org/api/profile/names/items/response.schema.json",
  title: "APIResponseProfileNameItemsGet",
  description: "A successful response for profile names.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "names"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        names: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/item.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseProfileNameItemsGetSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileNameItemsGet =
  createValidator<IAPIResponseProfileNameItemsGet>(
    aPIResponseProfileNameItemsGetSchema as unknown as IJSONSchema,
  );
