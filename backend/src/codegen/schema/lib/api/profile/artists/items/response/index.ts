export {
  aPIResponseProfileArtistItemsGetSchema,
  validateAPIResponseProfileArtistItemsGet,
} from "./lib.js";
export type { IAPIResponseProfileArtistItemsGet } from "./types.js";
