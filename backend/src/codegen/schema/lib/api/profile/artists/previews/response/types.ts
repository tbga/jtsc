import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * A successful response for profile artists.
 */
export interface IAPIResponseProfileArtistPreviewsGet {
  is_successful: true;
  data: {
    pagination: IPagination;
    artists: IReleasePreview[];
  };
}
