import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseProfileArtistPreviewsGet } from "./types.js";

export const aPIResponseProfileArtistPreviewsGetSchema = {
  $id: "https://jtsc-schemas.org/api/profile/artists/previews/response.schema.json",
  title: "APIResponseProfileArtistPreviewsGet",
  description: "A successful response for profile artists.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "artists"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        artists: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponseProfileArtistPreviewsGetSchema as unknown as IJSONSchema);

export const validateAPIResponseProfileArtistPreviewsGet =
  createValidator<IAPIResponseProfileArtistPreviewsGet>(
    aPIResponseProfileArtistPreviewsGetSchema as unknown as IJSONSchema,
  );
