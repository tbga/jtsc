export {
  aPIResponseProfileArtistPreviewsGetSchema,
  validateAPIResponseProfileArtistPreviewsGet,
} from "./lib.js";
export type { IAPIResponseProfileArtistPreviewsGet } from "./types.js";
