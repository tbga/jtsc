export { aPIResponseSchema, validateAPIResponse } from "./lib.js";
export type { IAPIResponse } from "./types.js";
