import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponse } from "./types.js";

export const aPIResponseSchema = {
  $id: "https://jtsc-schemas.org/api/response.schema.json",
  title: "APIResponse",
  description: "Body of the API response.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    {
      $ref: "https://jtsc-schemas.org/api/response-failure.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/api/response-success.schema.json",
    },
  ],
} as const;
addSchema(aPIResponseSchema as unknown as IJSONSchema);

export const validateAPIResponse = createValidator<IAPIResponse>(
  aPIResponseSchema as unknown as IJSONSchema,
);
