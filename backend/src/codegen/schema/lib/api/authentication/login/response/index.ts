export {
  aPIResponseAccountLoginSchema,
  validateAPIResponseAccountLogin,
} from "./lib.js";
export type { IAPIResponseAccountLogin } from "./types.js";
