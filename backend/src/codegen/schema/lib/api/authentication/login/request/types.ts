import { type IAccountLogin } from "#codegen/schema/lib/entities/account/login";

/**
 * Body of the API request.
 */
export interface IAPIRequestAccountLogin {
  data: IAccountLogin;
}
