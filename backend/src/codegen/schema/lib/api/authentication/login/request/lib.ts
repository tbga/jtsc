import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestAccountLogin } from "./types.js";

export const aPIRequestAccountLoginSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/login/request.schema.json",
  title: "APIRequestAccountLogin",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/login.schema.json",
    },
  },
} as const;
addSchema(aPIRequestAccountLoginSchema as unknown as IJSONSchema);

export const validateAPIRequestAccountLogin =
  createValidator<IAPIRequestAccountLogin>(
    aPIRequestAccountLoginSchema as unknown as IJSONSchema,
  );
