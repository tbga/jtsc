import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseAccountLogout } from "./types.js";

export const aPIResponseAccountLogoutSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/logout/response.schema.json",
  title: "APIResponseAccountLogout",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      const: null,
    },
  },
} as const;
addSchema(aPIResponseAccountLogoutSchema as unknown as IJSONSchema);

export const validateAPIResponseAccountLogout =
  createValidator<IAPIResponseAccountLogout>(
    aPIResponseAccountLogoutSchema as unknown as IJSONSchema,
  );
