export {
  aPIResponseAccountLogoutSchema,
  validateAPIResponseAccountLogout,
} from "./lib.js";
export type { IAPIResponseAccountLogout } from "./types.js";
