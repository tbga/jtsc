/**
 * Body of the successful API response.
 */
export interface IAPIResponseAccountLogout {
  is_successful: true;
  data: null;
}
