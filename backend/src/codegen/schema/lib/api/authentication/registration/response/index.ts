export {
  aPIResponseAccountRegistrationSchema,
  validateAPIResponseAccountRegistration,
} from "./lib.js";
export type { IAPIResponseAccountRegistration } from "./types.js";
