export {
  aPIRequestAccountRegistrationSchema,
  validateAPIRequestAccountRegistration,
} from "./lib.js";
export type { IAPIRequestAccountRegistration } from "./types.js";
