import { type IAccount } from "#codegen/schema/lib/entities/account/entity";

/**
 * Body of the successful API response.
 */
export interface IAPIResponseAccountRegistration {
  is_successful: true;
  data: IAccount;
}
