import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIRequestAccountRegistration } from "./types.js";

export const aPIRequestAccountRegistrationSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/registration/request.schema.json",
  title: "APIRequestAccountRegistration",
  description: "Body of the API request.",
  type: "object",
  additionalProperties: false,
  required: ["data"],
  properties: {
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/init.schema.json",
    },
  },
} as const;
addSchema(aPIRequestAccountRegistrationSchema as unknown as IJSONSchema);

export const validateAPIRequestAccountRegistration =
  createValidator<IAPIRequestAccountRegistration>(
    aPIRequestAccountRegistrationSchema as unknown as IJSONSchema,
  );
