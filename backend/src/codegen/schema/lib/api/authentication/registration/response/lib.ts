import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponseAccountRegistration } from "./types.js";

export const aPIResponseAccountRegistrationSchema = {
  $id: "https://jtsc-schemas.org/api/authentication/registration/response.schema.json",
  title: "APIResponseAccountRegistration",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/account/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponseAccountRegistrationSchema as unknown as IJSONSchema);

export const validateAPIResponseAccountRegistration =
  createValidator<IAPIResponseAccountRegistration>(
    aPIResponseAccountRegistrationSchema as unknown as IJSONSchema,
  );
