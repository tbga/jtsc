import { type IAccountInit } from "#codegen/schema/lib/entities/account/init";

/**
 * Body of the API request.
 */
export interface IAPIRequestAccountRegistration {
  data: IAccountInit;
}
