import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostArtistsPreviews } from "./types.js";

export const aPIResponsePostArtistsPreviewsSchema = {
  $id: "https://jtsc-schemas.org/api/post/artists/get/response.schema.json",
  title: "APIResponsePostArtistsPreviews",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "artists"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        artists: {
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponsePostArtistsPreviewsSchema as unknown as IJSONSchema);

export const validateAPIResponsePostArtistsPreviews =
  createValidator<IAPIResponsePostArtistsPreviews>(
    aPIResponsePostArtistsPreviewsSchema as unknown as IJSONSchema,
  );
