export {
  aPIResponsePostArtistsPreviewsSchema,
  validateAPIResponsePostArtistsPreviews,
} from "./lib.js";
export type { IAPIResponsePostArtistsPreviews } from "./types.js";
