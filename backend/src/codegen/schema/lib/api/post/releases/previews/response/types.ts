import { type IPagination } from "#codegen/schema/lib/types/pagination/pagination";
import { type IReleasePreview } from "#codegen/schema/lib/entities/release/preview";

/**
 * A successful response for post releases.
 */
export interface IAPIResponsePostReleasePreviews {
  is_successful: true;
  data: {
    pagination: IPagination;
    releases: IReleasePreview[];
  };
}
