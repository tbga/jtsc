import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostReleasePreviews } from "./types.js";

export const aPIResponsePostReleasePreviewsSchema = {
  $id: "https://jtsc-schemas.org/api/post/releases/previews/response.schema.json",
  title: "APIResponsePostReleasePreviews",
  description: "A successful response for post releases.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      type: "object",
      additionalProperties: false,
      required: ["pagination", "releases"],
      properties: {
        pagination: {
          $ref: "http://jtsc-schemas.org/types/pagination/pagination.schema.json",
        },
        releases: {
          type: "array",
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/preview.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(aPIResponsePostReleasePreviewsSchema as unknown as IJSONSchema);

export const validateAPIResponsePostReleasePreviews =
  createValidator<IAPIResponsePostReleasePreviews>(
    aPIResponsePostReleasePreviewsSchema as unknown as IJSONSchema,
  );
