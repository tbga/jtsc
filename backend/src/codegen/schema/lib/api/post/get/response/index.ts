export { aPIResponsePostGetSchema, validateAPIResponsePostGet } from "./lib.js";
export type { IAPIResponsePostGet } from "./types.js";
