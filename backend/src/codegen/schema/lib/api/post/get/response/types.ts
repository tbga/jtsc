import { type IPost } from "#codegen/schema/lib/entities/post/entity";

/**
 * Body of the successful API response.
 */
export interface IAPIResponsePostGet {
  is_successful: true;
  data: IPost;
}
