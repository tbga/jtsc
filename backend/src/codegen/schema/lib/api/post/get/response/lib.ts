import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAPIResponsePostGet } from "./types.js";

export const aPIResponsePostGetSchema = {
  $id: "https://jtsc-schemas.org/api/post/get/response.schema.json",
  title: "APIResponsePostGet",
  description: "Body of the successful API response.",
  type: "object",
  additionalProperties: false,
  required: ["is_successful", "data"],
  properties: {
    is_successful: {
      type: "boolean",
      const: true,
    },
    data: {
      $ref: "https://jtsc-schemas.org/entities/post/entity.schema.json",
    },
  },
} as const;
addSchema(aPIResponsePostGetSchema as unknown as IJSONSchema);

export const validateAPIResponsePostGet = createValidator<IAPIResponsePostGet>(
  aPIResponsePostGetSchema as unknown as IJSONSchema,
);
