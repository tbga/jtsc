export {
  environmentConfigurationSchema,
  validateEnvironmentConfiguration,
} from "./lib.js";
export type { IEnvironmentConfiguration } from "./types.js";
