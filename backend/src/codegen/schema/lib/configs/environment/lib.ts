import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IEnvironmentConfiguration } from "./types.js";

export const environmentConfigurationSchema = {
  $id: "https://jtsc-schemas.org/configs/environment.schema.json",
  title: "EnvironmentConfiguration",
  description: "Environment configuration options.",
  type: "object",
  additionalProperties: false,
  required: ["NODE_ENV"],
  properties: {
    NODE_ENV: {
      description: "Node.js environment variable.",
      type: "string",
      enum: ["development", "production"],
    },
  },
} as const;
addSchema(environmentConfigurationSchema as unknown as IJSONSchema);

export const validateEnvironmentConfiguration =
  createValidator<IEnvironmentConfiguration>(
    environmentConfigurationSchema as unknown as IJSONSchema,
  );
