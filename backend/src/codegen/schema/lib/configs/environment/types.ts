/**
 * Environment configuration options.
 */
export interface IEnvironmentConfiguration {
  /**
   * Node.js environment variable.
   */
  NODE_ENV: "development" | "production";
}
