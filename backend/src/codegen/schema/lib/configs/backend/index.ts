export {
  backendConfigurationSchema,
  validateBackendConfiguration,
} from "./lib.js";
export type { IBackendConfiguration } from "./types.js";
