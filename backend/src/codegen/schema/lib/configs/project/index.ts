export {
  projectConfigurationSchema,
  validateProjectConfiguration,
} from "./lib.js";
export type { IProjectConfiguration } from "./types.js";
