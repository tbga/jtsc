import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IProjectConfiguration } from "./types.js";

export const projectConfigurationSchema = {
  $id: "https://jtsc-schemas.org/configs/project.schema.json",
  title: "ProjectConfiguration",
  description: "Project configuration options.",
  type: "object",
  additionalProperties: false,
  required: ["PROJECT_PATH", "OUTPUT_PATH"],
  properties: {
    PROJECT_PATH: {
      description: "A path to the project folder",
      type: "string",
    },
    OUTPUT_PATH: {
      description: "A path to build output folder",
      type: "string",
    },
  },
} as const;
addSchema(projectConfigurationSchema as unknown as IJSONSchema);

export const validateProjectConfiguration =
  createValidator<IProjectConfiguration>(
    projectConfigurationSchema as unknown as IJSONSchema,
  );
