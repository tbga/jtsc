/**
 * Project configuration options.
 */
export interface IProjectConfiguration {
  /**
   * A path to the project folder
   */
  PROJECT_PATH: string;
  /**
   * A path to build output folder
   */
  OUTPUT_PATH: string;
}
