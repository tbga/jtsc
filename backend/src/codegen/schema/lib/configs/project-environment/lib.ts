import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IProjectEnvironment } from "./types.js";

export const projectEnvironmentSchema = {
  $id: "https://jtsc-schemas.org/configs/project-environment.schema.json",
  title: "ProjectEnvironment",
  description: "Project environment states.",
  type: "string",
  enum: ["development", "production"],
} as const;
addSchema(projectEnvironmentSchema as unknown as IJSONSchema);

export const validateProjectEnvironment = createValidator<IProjectEnvironment>(
  projectEnvironmentSchema as unknown as IJSONSchema,
);
