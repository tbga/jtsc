/**
 * Project environment states.
 */
export type IProjectEnvironment = "development" | "production";
