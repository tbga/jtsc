export { mediaTypeTextSchema, validateMediaTypeText } from "./lib.js";
export type { IMediaTypeText } from "./types.js";
