import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeText } from "./types.js";

export const mediaTypeTextSchema = {
  $id: "https://jtsc-schemas.org/media/text.schema.json",
  title: "MediaTypeText",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "text",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC6015]",
          type: "string",
          const: "1d-interleaved-parityfec",
        },
        {
          description: "[W3C][Robin_Berjon]",
          type: "string",
          const: "cache-manifest",
        },
        {
          description: "[RFC5545]",
          type: "string",
          const: "calendar",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "cql",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "cql-expression",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "cql-identifier",
        },
        {
          description: "[RFC2318]",
          type: "string",
          const: "css",
        },
        {
          description: "[RFC4180][RFC7111]",
          type: "string",
          const: "csv",
        },
        {
          description: "[National_Archives_UK][David_Underdown]",
          type: "string",
          const: "csv-schema",
        },
        {
          description: "[RFC2425][RFC6350]",
          type: "string",
          const: "directory - DEPRECATED by RFC6350",
        },
        {
          description: "[RFC4027]",
          type: "string",
          const: "dns",
        },
        {
          description: "[RFC9239]",
          type: "string",
          const: "ecmascript (OBSOLETED in favor of text/javascript)",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "encaprtp",
        },
        {
          description: "[RFC1896]",
          type: "string",
          const: "enriched",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "fhirpath",
        },
        {
          description: "[RFC8627]",
          type: "string",
          const: "flexfec",
        },
        {
          description: "[RFC6354]",
          type: "string",
          const: "fwdred",
        },
        {
          description: "[Sequence_Ontology]",
          type: "string",
          const: "gff3",
        },
        {
          description: "[RFC6787]",
          type: "string",
          const: "grammar-ref-list",
        },
        {
          description: "[HL7][Marc_Duteau]",
          type: "string",
          const: "hl7v2",
        },
        {
          description: "[W3C][Robin_Berjon]",
          type: "string",
          const: "html",
        },
        {
          description: "[RFC9239]",
          type: "string",
          const: "javascript",
        },
        {
          description: "[Peeter_Piegaze]",
          type: "string",
          const: "jcr-cnd",
        },
        {
          description: "[RFC7763]",
          type: "string",
          const: "markdown",
        },
        {
          description: "[Jesse_Alama]",
          type: "string",
          const: "mizar",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "n3",
        },
        {
          description: "[RFC7826]",
          type: "string",
          const: "parameters",
        },
        {
          description: "[RFC3009]",
          type: "string",
          const: "parityfec",
        },
        {
          description: "[RFC2046][RFC3676][RFC5147]",
          type: "string",
          const: "plain",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "provenance-notation",
        },
        {
          description: "[Benja_Fallenstein]",
          type: "string",
          const: "prs.fallenstein.rst",
        },
        {
          description: "[John_Lines]",
          type: "string",
          const: "prs.lines.tag",
        },
        {
          description: "[Hans-Dieter_A._Hiep]",
          type: "string",
          const: "prs.prop.logic",
        },
        {
          description: "[RFC6682]",
          type: "string",
          const: "raptorfec",
        },
        {
          description: "[RFC4102]",
          type: "string",
          const: "RED",
        },
        {
          description: "[RFC6522]",
          type: "string",
          const: "rfc822-headers",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "richtext",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "rtf",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "rtp-enc-aescm128",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "rtploopback",
        },
        {
          description: "[RFC4588]",
          type: "string",
          const: "rtx",
        },
        {
          description: "[RFC1874]",
          type: "string",
          const: "SGML",
        },
        {
          description: "[W3C_SHACL_Community_Group][Vladimir_Alexiev]",
          type: "string",
          const: "shaclc",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "shex",
        },
        {
          description: "[Linux_Foundation][Rose_Judge]",
          type: "string",
          const: "spdx",
        },
        {
          description: "[IEEE-ISTO-PWG-PPP]",
          type: "string",
          const: "strings",
        },
        {
          description: "[RFC4103]",
          type: "string",
          const: "t140",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "tab-separated-values",
        },
        {
          description: "[RFC4263]",
          type: "string",
          const: "troff",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "turtle",
        },
        {
          description: "[RFC5109]",
          type: "string",
          const: "ulpfec",
        },
        {
          description: "[RFC2483]",
          type: "string",
          const: "uri-list",
        },
        {
          description: "[RFC6350]",
          type: "string",
          const: "vcard",
        },
        {
          description: "[Regis_Dehoux]",
          type: "string",
          const: "vnd.a",
        },
        {
          description: "[Steve_Allen]",
          type: "string",
          const: "vnd.abc",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.ascii-art",
        },
        {
          description: "[Robert_Byrnes]",
          type: "string",
          const: "vnd.curl",
        },
        {
          description: "[Charles_Plessy]",
          type: "string",
          const: "vnd.debian.copyright",
        },
        {
          description: "[Dan_Bradley]",
          type: "string",
          const: "vnd.DMClientScript",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.subtitle",
        },
        {
          description: "[Stefan_Eilemann]",
          type: "string",
          const: "vnd.esmertec.theme-descriptor",
        },
        {
          description: "[Martin_Cizek]",
          type: "string",
          const: "vnd.exchangeable",
        },
        {
          description: "[Gordon_Clarke]",
          type: "string",
          const: "vnd.familysearch.gedcom",
        },
        {
          description: "[Steve_Gilberd]",
          type: "string",
          const: "vnd.ficlab.flt",
        },
        {
          description: "[John-Mark_Gurney]",
          type: "string",
          const: "vnd.fly",
        },
        {
          description: "[Kari_E._Hurtta]",
          type: "string",
          const: "vnd.fmi.flexstor",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.gml",
        },
        {
          description: "[John_Ellson]",
          type: "string",
          const: "vnd.graphviz",
        },
        {
          description: "[Hill_Hanxv]",
          type: "string",
          const: "vnd.hans",
        },
        {
          description: "[Heungsub_Lee]",
          type: "string",
          const: "vnd.hgl",
        },
        {
          description: "[Michael_Powers]",
          type: "string",
          const: "vnd.in3d.3dml",
        },
        {
          description: "[Michael_Powers]",
          type: "string",
          const: "vnd.in3d.spot",
        },
        {
          description: "[IPTC]",
          type: "string",
          const: "vnd.IPTC.NewsML",
        },
        {
          description: "[IPTC]",
          type: "string",
          const: "vnd.IPTC.NITF",
        },
        {
          description: "[Mikusiak_Lubos]",
          type: "string",
          const: "vnd.latex-z",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.reflex",
        },
        {
          description: "[Jan_Nelson]",
          type: "string",
          const: "vnd.ms-mediapackage",
        },
        {
          description: "[Feiyu_Xie]",
          type: "string",
          const: "vnd.net2phone.commcenter.command",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-basic-layout",
        },
        {
          description: "[Pierre_Papin]",
          type: "string",
          const: "vnd.senx.warpscript",
        },
        {
          description: "[Nicholas_Parks_Young]",
          type: "string",
          const: "vnd.si.uricatalogue (OBSOLETED by request)",
        },
        {
          description: "[Gary_Adams]",
          type: "string",
          const: "vnd.sun.j2me.app-descriptor",
        },
        {
          description: "[Petter_Reinholdtsen]",
          type: "string",
          const: "vnd.sosi",
        },
        {
          description: "[David_Lee_Lambert]",
          type: "string",
          const: "vnd.trolltech.linguist",
        },
        {
          description: "[WAP-Forum]",
          type: "string",
          const: "vnd.wap.si",
        },
        {
          description: "[WAP-Forum]",
          type: "string",
          const: "vnd.wap.sl",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wml",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wmlscript",
        },
        {
          description: "[W3C][Silvia_Pfeiffer]",
          type: "string",
          const: "vtt",
        },
        {
          description: "[W3C][David_Neto]",
          type: "string",
          const: "wgsl",
        },
        {
          description: "[RFC7303]",
          type: "string",
          const: "xml",
        },
        {
          description: "[RFC7303]",
          type: "string",
          const: "xml-external-parsed-entity",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeTextSchema as unknown as IJSONSchema);

export const validateMediaTypeText = createValidator<IMediaTypeText>(
  mediaTypeTextSchema as unknown as IJSONSchema,
);
