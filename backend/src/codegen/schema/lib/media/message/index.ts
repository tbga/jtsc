export { mediaTypeMessageSchema, validateMediaTypeMessage } from "./lib.js";
export type { IMediaTypeMessage } from "./types.js";
