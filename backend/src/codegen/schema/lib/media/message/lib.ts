import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeMessage } from "./types.js";

export const mediaTypeMessageSchema = {
  $id: "https://jtsc-schemas.org/media/message.schema.json",
  title: "MediaTypeMessage",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "message",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC9292]",
          type: "string",
          const: "bhttp",
        },
        {
          description: "[RFC3862]",
          type: "string",
          const: "CPIM",
        },
        {
          description: "[RFC1894]",
          type: "string",
          const: "delivery-status",
        },
        {
          description: "[RFC8098]",
          type: "string",
          const: "disposition-notification",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "external-body",
        },
        {
          description: "[RFC5965]",
          type: "string",
          const: "feedback-report",
        },
        {
          description: "[RFC6532]",
          type: "string",
          const: "global",
        },
        {
          description: "[RFC6533]",
          type: "string",
          const: "global-delivery-status",
        },
        {
          description: "[RFC6533]",
          type: "string",
          const: "global-disposition-notification",
        },
        {
          description: "[RFC6533]",
          type: "string",
          const: "global-headers",
        },
        {
          description: "[RFC9112]",
          type: "string",
          const: "http",
        },
        {
          description: "[RFC5438]",
          type: "string",
          const: "imdn+xml",
        },
        {
          description: "[RFC5537][Henry_Spencer]",
          type: "string",
          const: "news (OBSOLETED by [RFC5537])",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "partial",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "rfc822",
        },
        {
          description: "[RFC2660][status-change-http-experiments-to-historic]",
          type: "string",
          const: "s-http (OBSOLETE)",
        },
        {
          description: "[RFC3261]",
          type: "string",
          const: "sip",
        },
        {
          description: "[RFC3420]",
          type: "string",
          const: "sipfrag",
        },
        {
          description: "[RFC3886]",
          type: "string",
          const: "tracking-status",
        },
        {
          description: "[Nicholas_Parks_Young]",
          type: "string",
          const: "vnd.si.simp (OBSOLETED by request)",
        },
        {
          description: "[Mick_Conley]",
          type: "string",
          const: "vnd.wfa.wsc",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeMessageSchema as unknown as IJSONSchema);

export const validateMediaTypeMessage = createValidator<IMediaTypeMessage>(
  mediaTypeMessageSchema as unknown as IJSONSchema,
);
