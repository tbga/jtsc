export interface IMediaTypeMessage {
  type: "message";
  subtype:
    | "bhttp"
    | "CPIM"
    | "delivery-status"
    | "disposition-notification"
    | "example"
    | "external-body"
    | "feedback-report"
    | "global"
    | "global-delivery-status"
    | "global-disposition-notification"
    | "global-headers"
    | "http"
    | "imdn+xml"
    | "news (OBSOLETED by [RFC5537])"
    | "partial"
    | "rfc822"
    | "s-http (OBSOLETE)"
    | "sip"
    | "sipfrag"
    | "tracking-status"
    | "vnd.si.simp (OBSOLETED by request)"
    | "vnd.wfa.wsc";
}
