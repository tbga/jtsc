export { mediaTypeSchema, validateMediaType } from "./lib.js";
export type { IMediaType } from "./types.js";
