import { type IMediaTypeApplication } from "#codegen/schema/lib/media/application";
import { type IMediaTypeAudio } from "#codegen/schema/lib/media/audio";
import { type IMediaTypeFont } from "#codegen/schema/lib/media/font";
import { type IMediaTypeImage } from "#codegen/schema/lib/media/image";
import { type IMediaTypeMessage } from "#codegen/schema/lib/media/message";
import { type IMediaTypeModel } from "#codegen/schema/lib/media/model";
import { type IMediaTypeMultipart } from "#codegen/schema/lib/media/multipart";
import { type IMediaTypeText } from "#codegen/schema/lib/media/text";
import { type IMediaTypeVideo } from "#codegen/schema/lib/media/video";

/**
 * Formerly MIME type.
 */
export type IMediaType =
  | IMediaTypeApplication
  | IMediaTypeAudio
  | IMediaTypeFont
  | IMediaTypeImage
  | IMediaTypeMessage
  | IMediaTypeModel
  | IMediaTypeMultipart
  | IMediaTypeText
  | IMediaTypeVideo;
