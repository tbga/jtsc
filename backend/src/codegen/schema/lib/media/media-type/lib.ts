import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaType } from "./types.js";

export const mediaTypeSchema = {
  $id: "https://jtsc-schemas.org/media/media-type.schema.json",
  title: "MediaType",
  description: "Formerly MIME type.",
  type: "object",
  anyOf: [
    {
      $ref: "https://jtsc-schemas.org/media/application.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/audio.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/font.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/image.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/message.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/model.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/multipart.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/text.schema.json",
    },
    {
      $ref: "https://jtsc-schemas.org/media/video.schema.json",
    },
  ],
} as const;
addSchema(mediaTypeSchema as unknown as IJSONSchema);

export const validateMediaType = createValidator<IMediaType>(
  mediaTypeSchema as unknown as IJSONSchema,
);
