export { mediaTypeImageSchema, validateMediaTypeImage } from "./lib.js";
export type { IMediaTypeImage } from "./types.js";
