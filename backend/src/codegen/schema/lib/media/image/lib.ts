import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeImage } from "./types.js";

export const mediaTypeImageSchema = {
  $id: "https://jtsc-schemas.org/media/image.schema.json",
  title: "MediaTypeImage",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "image",
    },
    subtype: {
      anyOf: [
        {
          description: "[SMPTE][Howard_Lukk]",
          type: "string",
          const: "aces",
        },
        {
          description: "[W3C][W3C_PNG_Working_Group]",
          type: "string",
          const: "apng",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "avci",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "avcs",
        },
        {
          description: "[Alliance_for_Open_Media][Cyril_Concolato]",
          type: "string",
          const: "avif",
        },
        {
          description: "[RFC7903]",
          type: "string",
          const: "bmp",
        },
        {
          description: "[Alan_Francis]",
          type: "string",
          const: "cgm",
        },
        {
          description: "[DICOM_Standard_Committee][David_Clunie]",
          type: "string",
          const: "dicom-rle",
        },
        {
          description: "[SMPTE][SMPTE_Director_of_Standards_Development]",
          type: "string",
          const: "dpx",
        },
        {
          description: "[RFC7903]",
          type: "string",
          const: "emf",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[RFC4047]",
          type: "string",
          const: "fits",
        },
        {
          description: "[RFC1494]",
          type: "string",
          const: "g3fax",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "gif",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "heic",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "heic-sequence",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "heif",
        },
        {
          description: "[ISO-IEC_JTC1][David_Singer]",
          type: "string",
          const: "heif-sequence",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "hej2k",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "hsj2",
        },
        {
          description: "[RFC1314]",
          type: "string",
          const: "ief",
        },
        {
          description: "[DICOM_Standard_Committee][David_Clunie]",
          type: "string",
          const: "jls",
        },
        {
          description: "[RFC3745]",
          type: "string",
          const: "jp2",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "jpeg",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "jph",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "jphc",
        },
        {
          description: "[RFC3745]",
          type: "string",
          const: "jpm",
        },
        {
          description: "[RFC3745]",
          type: "string",
          const: "jpx",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "jxr",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "jxrA",
        },
        {
          description: "[ISO-IEC_JTC1][ITU-T]",
          type: "string",
          const: "jxrS",
        },
        {
          description: "[ISO-IEC_JTC1]",
          type: "string",
          const: "jxs",
        },
        {
          description: "[ISO-IEC_JTC1]",
          type: "string",
          const: "jxsc",
        },
        {
          description: "[ISO-IEC_JTC1]",
          type: "string",
          const: "jxsi",
        },
        {
          description: "[ISO-IEC_JTC1]",
          type: "string",
          const: "jxss",
        },
        {
          description: "[Khronos][Mark_Callow]",
          type: "string",
          const: "ktx",
        },
        {
          description: "[Khronos][Mark_Callow]",
          type: "string",
          const: "ktx2",
        },
        {
          description: "[Ilya_Ferber]",
          type: "string",
          const: "naplps",
        },
        {
          description: "[W3C][PNG_Working_Group]",
          type: "string",
          const: "png",
        },
        {
          description: "[Ben_Simon]",
          type: "string",
          const: "prs.btif",
        },
        {
          description: "[Juern_Laun]",
          type: "string",
          const: "prs.pti",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "pwg-raster",
        },
        {
          description: "[W3C][http://www.w3.org/TR/SVG/mimereg.html]",
          type: "string",
          const: "svg+xml",
        },
        {
          description: "[RFC3362]",
          type: "string",
          const: "t38",
        },
        {
          description: "[RFC3302]",
          type: "string",
          const: "tiff",
        },
        {
          description: "[RFC3950]",
          type: "string",
          const: "tiff-fx",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.adobe.photoshop",
        },
        {
          description: "[Gary_Clueit]",
          type: "string",
          const: "vnd.airzip.accelerator.azv",
        },
        {
          description: "[Ann_McLaughlin]",
          type: "string",
          const: "vnd.cns.inf2",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.graphic",
        },
        {
          description: "[Leon_Bottou]",
          type: "string",
          const: "vnd.djvu",
        },
        {
          description: "[Jodi_Moline]",
          type: "string",
          const: "vnd.dwg",
        },
        {
          description: "[Jodi_Moline]",
          type: "string",
          const: "vnd.dxf",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.subtitle",
        },
        {
          description: "[Scott_Becker]",
          type: "string",
          const: "vnd.fastbidsheet",
        },
        {
          description: "[Marc_Douglas_Spencer]",
          type: "string",
          const: "vnd.fpx",
        },
        {
          description: "[Arild_Fuldseth]",
          type: "string",
          const: "vnd.fst",
        },
        {
          description: "[Masanori_Onda]",
          type: "string",
          const: "vnd.fujixerox.edmics-mmr",
        },
        {
          description: "[Masanori_Onda]",
          type: "string",
          const: "vnd.fujixerox.edmics-rlc",
        },
        {
          description: "[Martin_Bailey]",
          type: "string",
          const: "vnd.globalgraphics.pgb",
        },
        {
          description: "[Simon_Butcher]",
          type: "string",
          const: "vnd.microsoft.icon",
        },
        {
          description: "[Saveen_Reddy]",
          type: "string",
          const: "vnd.mix",
        },
        {
          description: "[Gregory_Vaughan]",
          type: "string",
          const: "vnd.ms-modi",
        },
        {
          description: "[Stuart_Parmenter]",
          type: "string",
          const: "vnd.mozilla.apng",
        },
        {
          description: "[Marc_Douglas_Spencer]",
          type: "string",
          const: "vnd.net-fpx",
        },
        {
          description: "[PCO_AG][Jan_Zeman]",
          type: "string",
          const: "vnd.pco.b16",
        },
        {
          description: "[Randolph_Fritz][Greg_Ward]",
          type: "string",
          const: "vnd.radiance",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.png",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.gif",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.jpg",
        },
        {
          description: "[Jodi_Moline]",
          type: "string",
          const: "vnd.svf",
        },
        {
          description: "[Ni_Hui]",
          type: "string",
          const: "vnd.tencent.tap",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.valve.source.texture",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wbmp",
        },
        {
          description: "[Steven_Martin]",
          type: "string",
          const: "vnd.xiff",
        },
        {
          description: "[Chris_Charabaruk]",
          type: "string",
          const: "vnd.zbrush.pcx",
        },
        {
          description: "[RFC-zern-webp-12]",
          type: "string",
          const: "webp",
        },
        {
          description: "[RFC7903]",
          type: "string",
          const: "wmf",
        },
        {
          description: "[RFC7903]",
          type: "string",
          const: "x-emf - DEPRECATED in favor of image/emf",
        },
        {
          description: "[RFC7903]",
          type: "string",
          const: "x-wmf - DEPRECATED in favor of image/wmf",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeImageSchema as unknown as IJSONSchema);

export const validateMediaTypeImage = createValidator<IMediaTypeImage>(
  mediaTypeImageSchema as unknown as IJSONSchema,
);
