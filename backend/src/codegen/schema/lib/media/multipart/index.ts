export { mediaTypeMultipartSchema, validateMediaTypeMultipart } from "./lib.js";
export type { IMediaTypeMultipart } from "./types.js";
