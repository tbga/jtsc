export interface IMediaTypeMultipart {
  type: "multipart";
  subtype:
    | "alternative"
    | "appledouble"
    | "byteranges"
    | "digest"
    | "encrypted"
    | "example"
    | "form-data"
    | "header-set"
    | "mixed"
    | "multilingual"
    | "parallel"
    | "related"
    | "report"
    | "signed"
    | "vnd.bint.med-plus"
    | "voice-message"
    | "x-mixed-replace";
}
