import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeMultipart } from "./types.js";

export const mediaTypeMultipartSchema = {
  $id: "https://jtsc-schemas.org/media/multipart.schema.json",
  title: "MediaTypeMultipart",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "multipart",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC2046][RFC2045]",
          type: "string",
          const: "alternative",
        },
        {
          description: "[Patrik_Faltstrom]",
          type: "string",
          const: "appledouble",
        },
        {
          description: "[RFC9110]",
          type: "string",
          const: "byteranges",
        },
        {
          description: "[RFC2046][RFC2045]",
          type: "string",
          const: "digest",
        },
        {
          description: "[RFC1847]",
          type: "string",
          const: "encrypted",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[RFC7578]",
          type: "string",
          const: "form-data",
        },
        {
          description: "[Dave_Crocker]",
          type: "string",
          const: "header-set",
        },
        {
          description: "[RFC2046][RFC2045]",
          type: "string",
          const: "mixed",
        },
        {
          description: "[RFC8255]",
          type: "string",
          const: "multilingual",
        },
        {
          description: "[RFC2046][RFC2045]",
          type: "string",
          const: "parallel",
        },
        {
          description: "[RFC2387]",
          type: "string",
          const: "related",
        },
        {
          description: "[RFC6522]",
          type: "string",
          const: "report",
        },
        {
          description: "[RFC1847]",
          type: "string",
          const: "signed",
        },
        {
          description: "[Heinz-Peter_Schütz]",
          type: "string",
          const: "vnd.bint.med-plus",
        },
        {
          description: "[RFC3801]",
          type: "string",
          const: "voice-message",
        },
        {
          description: "[W3C][Robin_Berjon]",
          type: "string",
          const: "x-mixed-replace",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeMultipartSchema as unknown as IJSONSchema);

export const validateMediaTypeMultipart = createValidator<IMediaTypeMultipart>(
  mediaTypeMultipartSchema as unknown as IJSONSchema,
);
