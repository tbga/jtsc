import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeVideo } from "./types.js";

export const mediaTypeVideoSchema = {
  $id: "https://jtsc-schemas.org/media/video.schema.json",
  title: "MediaTypeVideo",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "video",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC6015]",
          type: "string",
          const: "1d-interleaved-parityfec",
        },
        {
          description: "[RFC3839][RFC6381]",
          type: "string",
          const: "3gpp",
        },
        {
          description: "[RFC4393][RFC6381]",
          type: "string",
          const: "3gpp2",
        },
        {
          description: "[RFC4396]",
          type: "string",
          const: "3gpp-tt",
        },
        {
          description: "[Alliance_for_Open_Media]",
          type: "string",
          const: "AV1",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "BMPEG",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "BT656",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "CelB",
        },
        {
          description: "[RFC6469]",
          type: "string",
          const: "DV",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "encaprtp",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[RFC9043]",
          type: "string",
          const: "FFV1",
        },
        {
          description: "[RFC8627]",
          type: "string",
          const: "flexfec",
        },
        {
          description: "[RFC4587]",
          type: "string",
          const: "H261",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "H263",
        },
        {
          description: "[RFC4629]",
          type: "string",
          const: "H263-1998",
        },
        {
          description: "[RFC4629]",
          type: "string",
          const: "H263-2000",
        },
        {
          description: "[RFC6184]",
          type: "string",
          const: "H264",
        },
        {
          description: "[RFC6185]",
          type: "string",
          const: "H264-RCDO",
        },
        {
          description: "[RFC6190]",
          type: "string",
          const: "H264-SVC",
        },
        {
          description: "[RFC7798]",
          type: "string",
          const: "H265",
        },
        {
          description: "[RFC9328]",
          type: "string",
          const: "H266",
        },
        {
          description: "[David_Singer][ISO-IEC_JTC1]",
          type: "string",
          const: "iso.segment",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "JPEG",
        },
        {
          description: "[RFC5371][RFC5372]",
          type: "string",
          const: "jpeg2000",
        },
        {
          description: "[RFC9134]",
          type: "string",
          const: "jxsv",
        },
        {
          description: "[RFC3745]",
          type: "string",
          const: "mj2",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "MP1S",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "MP2P",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "MP2T",
        },
        {
          description: "[RFC4337][RFC6381]",
          type: "string",
          const: "mp4",
        },
        {
          description: "[RFC6416]",
          type: "string",
          const: "MP4V-ES",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "MPV",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "mpeg",
        },
        {
          description: "[RFC3640]",
          type: "string",
          const: "mpeg4-generic",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "nv",
        },
        {
          description: "[RFC5334][RFC7845]",
          type: "string",
          const: "ogg",
        },
        {
          description: "[RFC3009]",
          type: "string",
          const: "parityfec",
        },
        {
          description: "[RFC2862]",
          type: "string",
          const: "pointer",
        },
        {
          description: "[RFC6381][Paul_Lindner]",
          type: "string",
          const: "quicktime",
        },
        {
          description: "[RFC6682]",
          type: "string",
          const: "raptorfec",
        },
        {
          description: "[RFC4175]",
          type: "string",
          const: "raw",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "rtp-enc-aescm128",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "rtploopback",
        },
        {
          description: "[RFC4588]",
          type: "string",
          const: "rtx",
        },
        {
          description: "[SCIP][Michael_Faller][Daniel_Hanson]",
          type: "string",
          const: "scip",
        },
        {
          description: "[RFC8331]",
          type: "string",
          const: "smpte291",
        },
        {
          description: "[RFC3497]",
          type: "string",
          const: "SMPTE292M",
        },
        {
          description: "[RFC5109]",
          type: "string",
          const: "ulpfec",
        },
        {
          description: "[RFC4425]",
          type: "string",
          const: "vc1",
        },
        {
          description: "[RFC8450]",
          type: "string",
          const: "vc2",
        },
        {
          description: "[Frank_Rottmann]",
          type: "string",
          const: "vnd.CCTV",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.hd",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.mobile",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.mp4",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.pd",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.sd",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.video",
        },
        {
          description: "[Nathan_Zerbe]",
          type: "string",
          const: "vnd.directv.mpeg",
        },
        {
          description: "[Nathan_Zerbe]",
          type: "string",
          const: "vnd.directv.mpeg-tts",
        },
        {
          description: "[Edwin_Heredia]",
          type: "string",
          const: "vnd.dlna.mpeg-tts",
        },
        {
          description: "[Peter_Siebert][Kevin_Murray]",
          type: "string",
          const: "vnd.dvb.file",
        },
        {
          description: "[Arild_Fuldseth]",
          type: "string",
          const: "vnd.fvt",
        },
        {
          description: "[Swaminathan]",
          type: "string",
          const: "vnd.hns.video",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.1dparityfec-1010",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.1dparityfec-2005",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.2dparityfec-1010",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.2dparityfec-2005",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.ttsavc",
        },
        {
          description: "[Shuji_Nakamura]",
          type: "string",
          const: "vnd.iptvforum.ttsmpeg2",
        },
        {
          description: "[Tom_McGinty]",
          type: "string",
          const: "vnd.motorola.video",
        },
        {
          description: "[Tom_McGinty]",
          type: "string",
          const: "vnd.motorola.videop",
        },
        {
          description: "[Heiko_Recktenwald]",
          type: "string",
          const: "vnd.mpegurl",
        },
        {
          description: "[Steve_DiAcetis]",
          type: "string",
          const: "vnd.ms-playready.media.pyv",
        },
        {
          description: "[Petteri_Kangaslampi]",
          type: "string",
          const: "vnd.nokia.interleaved-multimedia",
        },
        {
          description: "[Miska_M._Hannuksela]",
          type: "string",
          const: "vnd.nokia.mp4vr",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.videovoip",
        },
        {
          description: "[John_Clark]",
          type: "string",
          const: "vnd.objectvideo",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.radgamettools.bink",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.radgamettools.smacker",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.mpeg1",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.mpeg4",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.swf",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.mov",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.uvvu.mp4",
        },
        {
          description: "[Google]",
          type: "string",
          const: "vnd.youtube.yt",
        },
        {
          description: "[John_Wolfe]",
          type: "string",
          const: "vnd.vivo",
        },
        {
          description: "[RFC7741]",
          type: "string",
          const: "VP8",
        },
        {
          description: "[RFC-ietf-payload-vp9-16]",
          type: "string",
          const: "VP9",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeVideoSchema as unknown as IJSONSchema);

export const validateMediaTypeVideo = createValidator<IMediaTypeVideo>(
  mediaTypeVideoSchema as unknown as IJSONSchema,
);
