export { mediaTypeVideoSchema, validateMediaTypeVideo } from "./lib.js";
export type { IMediaTypeVideo } from "./types.js";
