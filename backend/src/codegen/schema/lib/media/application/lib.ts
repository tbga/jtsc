import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeApplication } from "./types.js";

export const mediaTypeApplicationSchema = {
  $id: "https://jtsc-schemas.org/media/application.schema.json",
  title: "MediaTypeApplication",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "application",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC6015]",
          type: "string",
          const: "1d-interleaved-parityfec",
        },
        {
          description: "[_3GPP][Ozgur_Oyman]",
          type: "string",
          const: "3gpdash-qoe-report+xml",
        },
        {
          description: "[_3GPP][Ulrich_Wiehe]",
          type: "string",
          const: "3gppHal+json",
        },
        {
          description: "[_3GPP][Ulrich_Wiehe]",
          type: "string",
          const: "3gppHalForms+json",
        },
        {
          description: "[_3GPP][John_M_Meredith]",
          type: "string",
          const: "3gpp-ims+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "A2L",
        },
        {
          description: "[RFC9200]",
          type: "string",
          const: "ace+cbor",
        },
        {
          description: "[RFC-ietf-ace-mqtt-tls-profile-17]",
          type: "string",
          const: "ace+json",
        },
        {
          description: "[Ehud_Shapiro]",
          type: "string",
          const: "activemessage",
        },
        {
          description: "[W3C][Benjamin_Goering]",
          type: "string",
          const: "activity+json",
        },
        {
          description: "[RFC9237]",
          type: "string",
          const: "aif+cbor",
        },
        {
          description: "[RFC9237]",
          type: "string",
          const: "aif+json",
        },
        {
          description: "[RFC9241]",
          type: "string",
          const: "alto-cdni+json",
        },
        {
          description: "[RFC9241]",
          type: "string",
          const: "alto-cdnifilter+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-costmap+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-costmapfilter+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-directory+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-endpointprop+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-endpointpropparams+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-endpointcost+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-endpointcostparams+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-error+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-networkmapfilter+json",
        },
        {
          description: "[RFC7285]",
          type: "string",
          const: "alto-networkmap+json",
        },
        {
          description: "[RFC9240]",
          type: "string",
          const: "alto-propmap+json",
        },
        {
          description: "[RFC9240]",
          type: "string",
          const: "alto-propmapparams+json",
        },
        {
          description: "[RFC8895]",
          type: "string",
          const: "alto-updatestreamcontrol+json",
        },
        {
          description: "[RFC8895]",
          type: "string",
          const: "alto-updatestreamparams+json",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "AML",
        },
        {
          description: "[Nathaniel_Borenstein]",
          type: "string",
          const: "andrew-inset",
        },
        {
          description: "[Patrik_Faltstrom]",
          type: "string",
          const: "applefile",
        },
        {
          description: "[RFC9068]",
          type: "string",
          const: "at+jwt",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "ATF",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "ATFX",
        },
        {
          description: "[RFC4287][RFC5023]",
          type: "string",
          const: "atom+xml",
        },
        {
          description: "[RFC5023]",
          type: "string",
          const: "atomcat+xml",
        },
        {
          description: "[RFC6721]",
          type: "string",
          const: "atomdeleted+xml",
        },
        {
          description: "[Nathaniel_Borenstein]",
          type: "string",
          const: "atomicmail",
        },
        {
          description: "[RFC5023]",
          type: "string",
          const: "atomsvc+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "atsc-dwd+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "atsc-dynamic-event-message",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "atsc-held+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "atsc-rdt+json",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "atsc-rsat+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "ATXML",
        },
        {
          description: "[RFC4745]",
          type: "string",
          const: "auth-policy+xml",
        },
        {
          description: "[AutomationML_e.V.]",
          type: "string",
          const: "automationml-aml+xml",
        },
        {
          description: "[AutomationML_e.V.]",
          type: "string",
          const: "automationml-amlx+zip",
        },
        {
          description: "[ASHRAE][Dave_Robin]",
          type: "string",
          const: "bacnet-xdd+zip",
        },
        {
          description: "[RFC2442]",
          type: "string",
          const: "batch-SMTP",
        },
        {
          description: "[RFC3080]",
          type: "string",
          const: "beep+xml",
        },
        {
          description: "[RFC7265]",
          type: "string",
          const: "calendar+json",
        },
        {
          description: "[RFC6321]",
          type: "string",
          const: "calendar+xml",
        },
        {
          description: "[RFC6910]",
          type: "string",
          const: "call-completion",
        },
        {
          description: "[RFC1895]",
          type: "string",
          const: "CALS-1840",
        },
        {
          description: "[RFC8908]",
          type: "string",
          const: "captive+json",
        },
        {
          description: "[RFC8949]",
          type: "string",
          const: "cbor",
        },
        {
          description: "[RFC8742]",
          type: "string",
          const: "cbor-seq",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "cccex",
        },
        {
          description: "[RFC6503]",
          type: "string",
          const: "ccmp+xml",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "ccxml+xml",
        },
        {
          description: "[HL7][Marc_Duteau]",
          type: "string",
          const: "cda+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "CDFX+XML",
        },
        {
          description: "[RFC6208]",
          type: "string",
          const: "cdmi-capability",
        },
        {
          description: "[RFC6208]",
          type: "string",
          const: "cdmi-container",
        },
        {
          description: "[RFC6208]",
          type: "string",
          const: "cdmi-domain",
        },
        {
          description: "[RFC6208]",
          type: "string",
          const: "cdmi-object",
        },
        {
          description: "[RFC6208]",
          type: "string",
          const: "cdmi-queue",
        },
        {
          description: "[RFC7736]",
          type: "string",
          const: "cdni",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "CEA",
        },
        {
          description: "[Gottfried_Zimmermann]",
          type: "string",
          const: "cea-2018+xml",
        },
        {
          description: "[RFC4708]",
          type: "string",
          const: "cellml+xml",
        },
        {
          description: "[RFC6230]",
          type: "string",
          const: "cfw",
        },
        {
          description: "[OGC][Scott_Simmons]",
          type: "string",
          const: "city+json",
        },
        {
          description: "[IMS_Global][Andy_Miller]",
          type: "string",
          const: "clr",
        },
        {
          description: "[RFC8846]",
          type: "string",
          const: "clue_info+xml",
        },
        {
          description: "[RFC8847]",
          type: "string",
          const: "clue+xml",
        },
        {
          description: "[RFC7193]",
          type: "string",
          const: "cms",
        },
        {
          description: "[RFC3367]",
          type: "string",
          const: "cnrp+xml",
        },
        {
          description: "[RFC7390]",
          type: "string",
          const: "coap-group+json",
        },
        {
          description: "[RFC8075]",
          type: "string",
          const: "coap-payload",
        },
        {
          description: "[David_Glazer]",
          type: "string",
          const: "commonground",
        },
        {
          description: "[RFC9290, Section 6.3]",
          type: "string",
          const: "concise-problem-details+cbor",
        },
        {
          description: "[RFC4575]",
          type: "string",
          const: "conference-info+xml",
        },
        {
          description: "[RFC3880]",
          type: "string",
          const: "cpl+xml",
        },
        {
          description: "[RFC9052]",
          type: "string",
          const: "cose",
        },
        {
          description: "[RFC9052]",
          type: "string",
          const: "cose-key",
        },
        {
          description: "[RFC9052]",
          type: "string",
          const: "cose-key-set",
        },
        {
          description: "[RFC-ietf-cose-x509-09]",
          type: "string",
          const: "cose-x509",
        },
        {
          description: "[RFC7030]",
          type: "string",
          const: "csrattrs",
        },
        {
          description: "[Ecma_International_Helpdesk]",
          type: "string",
          const: "csta+xml",
        },
        {
          description: "[Ecma_International_Helpdesk]",
          type: "string",
          const: "CSTAdata+xml",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "csvm+json",
        },
        {
          description: "[CWL_Project][Michael_R._Crusoe]",
          type: "string",
          const: "cwl",
        },
        {
          description: "[CWL_Project][Michael_R._Crusoe]",
          type: "string",
          const: "cwl+json",
        },
        {
          description: "[RFC8392]",
          type: "string",
          const: "cwt",
        },
        {
          description: "[Donald_E._Eastlake_3rd]",
          type: "string",
          const: "cybercash",
        },
        {
          description: "[ISO-IEC_JTC1][Thomas_Stockhammer]",
          type: "string",
          const: "dash+xml",
        },
        {
          description: "[ISO-IEC_JTC1]",
          type: "string",
          const: "dash-patch+xml",
        },
        {
          description: "[David_Furbeck]",
          type: "string",
          const: "dashdelta",
        },
        {
          description: "[RFC4709]",
          type: "string",
          const: "davmount+xml",
        },
        {
          description: "[Larry_Campbell]",
          type: "string",
          const: "dca-rft",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "DCD",
        },
        {
          description: "[Larry_Campbell]",
          type: "string",
          const: "dec-dx",
        },
        {
          description: "[RFC4235]",
          type: "string",
          const: "dialog-info+xml",
        },
        {
          description: "[RFC3240]",
          type: "string",
          const: "dicom",
        },
        {
          description: "[DICOM_Standard_Committee][David_Clunie]",
          type: "string",
          const: "dicom+json",
        },
        {
          description: "[DICOM_Standard_Committee][David_Clunie]",
          type: "string",
          const: "dicom+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "DII",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "DIT",
        },
        {
          description: "[RFC4027]",
          type: "string",
          const: "dns",
        },
        {
          description: "[RFC8427]",
          type: "string",
          const: "dns+json",
        },
        {
          description: "[RFC8484]",
          type: "string",
          const: "dns-message",
        },
        {
          description: "[RFC9132]",
          type: "string",
          const: "dots+cbor",
        },
        {
          description: "[RFC6063]",
          type: "string",
          const: "dskpp+xml",
        },
        {
          description: "[RFC5698]",
          type: "string",
          const: "dssc+der",
        },
        {
          description: "[RFC5698]",
          type: "string",
          const: "dssc+xml",
        },
        {
          description: "[RFC3029]",
          type: "string",
          const: "dvcs",
        },
        {
          description: "[RFC4329][RFC9239]",
          type: "string",
          const: "ecmascript (OBSOLETED in favor of text/javascript)",
        },
        {
          description: "[RFC1767]",
          type: "string",
          const: "EDI-consent",
        },
        {
          description: "[RFC1767]",
          type: "string",
          const: "EDIFACT",
        },
        {
          description: "[RFC1767]",
          type: "string",
          const: "EDI-X12",
        },
        {
          description: "[UEFI_Forum][Samer_El-Haj-Mahmoud]",
          type: "string",
          const: "efi",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "elm+json",
        },
        {
          description: "[HL7][Bryn_Rhodes]",
          type: "string",
          const: "elm+xml",
        },
        {
          description: "[RFC8876]",
          type: "string",
          const: "EmergencyCallData.cap+xml",
        },
        {
          description: "[RFC7852]",
          type: "string",
          const: "EmergencyCallData.Comment+xml",
        },
        {
          description: "[RFC8147]",
          type: "string",
          const: "EmergencyCallData.Control+xml",
        },
        {
          description: "[RFC7852]",
          type: "string",
          const: "EmergencyCallData.DeviceInfo+xml",
        },
        {
          description: "[RFC8147]",
          type: "string",
          const: "EmergencyCallData.eCall.MSD",
        },
        {
          description: "[NENA][Randall_Gellens]",
          type: "string",
          const: "EmergencyCallData.LegacyESN+json",
        },
        {
          description: "[RFC7852]",
          type: "string",
          const: "EmergencyCallData.ProviderInfo+xml",
        },
        {
          description: "[RFC7852]",
          type: "string",
          const: "EmergencyCallData.ServiceInfo+xml",
        },
        {
          description: "[RFC7852]",
          type: "string",
          const: "EmergencyCallData.SubscriberInfo+xml",
        },
        {
          description: "[RFC8148][RFC Errata 6500]",
          type: "string",
          const: "EmergencyCallData.VEDS+xml",
        },
        {
          description:
            "[W3C][http://www.w3.org/TR/2007/CR-emma-20071211/#media-type-registration][ISO-IEC_JTC1]",
          type: "string",
          const: "emma+xml",
        },
        {
          description: "[W3C][Kazuyuki_Ashimura]",
          type: "string",
          const: "emotionml+xml",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "encaprtp",
        },
        {
          description: "[RFC5730]",
          type: "string",
          const: "epp+xml",
        },
        {
          description: "[W3C][EPUB_3_WG]",
          type: "string",
          const: "epub+zip",
        },
        {
          description: "[Steve_Katz]",
          type: "string",
          const: "eshop",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description:
            "[W3C][http://www.w3.org/TR/2009/CR-exi-20091208/#mediaTypeRegistration]",
          type: "string",
          const: "exi",
        },
        {
          description: "[RFC9163]",
          type: "string",
          const: "expect-ct-report+json",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "express",
        },
        {
          description:
            "[ITU-T_ASN.1_Rapporteur][ISO-IEC_JTC1_SC6_ASN.1_Rapporteur]",
          type: "string",
          const: "fastinfoset",
        },
        {
          description:
            "[ITU-T_ASN.1_Rapporteur][ISO-IEC_JTC1_SC6_ASN.1_Rapporteur]",
          type: "string",
          const: "fastsoap",
        },
        {
          description: "[ISO-TC_171-SC_2][Betsy_Fanning]",
          type: "string",
          const: "fdf",
        },
        {
          description: "[RFC6726]",
          type: "string",
          const: "fdt+xml",
        },
        {
          description: "[HL7][Grahame_Grieve]",
          type: "string",
          const: "fhir+json",
        },
        {
          description: "[HL7][Grahame_Grieve]",
          type: "string",
          const: "fhir+xml",
        },
        {
          description: "[RFC4047]",
          type: "string",
          const: "fits",
        },
        {
          description: "[RFC8627]",
          type: "string",
          const: "flexfec",
        },
        {
          description: "[Levantovsky][ISO-IEC_JTC1][RFC8081]",
          type: "string",
          const: "font-sfnt - DEPRECATED in favor of font/sfnt",
        },
        {
          description: "[RFC3073]",
          type: "string",
          const: "font-tdpfr",
        },
        {
          description: "[W3C][RFC8081]",
          type: "string",
          const: "font-woff - DEPRECATED in favor of font/woff",
        },
        {
          description: "[RFC6230]",
          type: "string",
          const: "framework-attributes+xml",
        },
        {
          description: "[RFC7946]",
          type: "string",
          const: "geo+json",
        },
        {
          description: "[RFC8142]",
          type: "string",
          const: "geo+json-seq",
        },
        {
          description: "[OGC][Scott_Simmons]",
          type: "string",
          const: "geopackage+sqlite3",
        },
        {
          description: "[OGC][Scott_Simmons]",
          type: "string",
          const: "geoxacml+xml",
        },
        {
          description: "[Khronos][Saurabh_Bhatia]",
          type: "string",
          const: "gltf-buffer",
        },
        {
          description: "[OGC][Clemens_Portele]",
          type: "string",
          const: "gml+xml",
        },
        {
          description: "[RFC6713]",
          type: "string",
          const: "gzip",
        },
        {
          description: "[RFC4573]",
          type: "string",
          const: "H224",
        },
        {
          description: "[RFC5985]",
          type: "string",
          const: "held+xml",
        },
        {
          description: "[HL7][Marc_Duteau]",
          type: "string",
          const: "hl7v2+xml",
        },
        {
          description: "[RFC9112]",
          type: "string",
          const: "http",
        },
        {
          description: "[Michael_Domino]",
          type: "string",
          const: "hyperstudio",
        },
        {
          description: "[RFC5408]",
          type: "string",
          const: "ibe-key-request+xml",
        },
        {
          description: "[RFC5408]",
          type: "string",
          const: "ibe-pkg-reply+xml",
        },
        {
          description: "[RFC5408]",
          type: "string",
          const: "ibe-pp-data",
        },
        {
          description: "[Curtis_Parks]",
          type: "string",
          const: "iges",
        },
        {
          description: "[RFC3994]",
          type: "string",
          const: "im-iscomposing+xml",
        },
        {
          description: "[RFC2652]",
          type: "string",
          const: "index",
        },
        {
          description: "[RFC2652]",
          type: "string",
          const: "index.cmd",
        },
        {
          description: "[RFC2652]",
          type: "string",
          const: "index.obj",
        },
        {
          description: "[RFC2652]",
          type: "string",
          const: "index.response",
        },
        {
          description: "[RFC2652]",
          type: "string",
          const: "index.vnd",
        },
        {
          description: "[Kazuyuki_Ashimura]",
          type: "string",
          const: "inkml+xml",
        },
        {
          description: "[RFC2935]",
          type: "string",
          const: "IOTP",
        },
        {
          description: "[RFC5655]",
          type: "string",
          const: "ipfix",
        },
        {
          description: "[RFC8010]",
          type: "string",
          const: "ipp",
        },
        {
          description: "[RFC3204]",
          type: "string",
          const: "ISUP",
        },
        {
          description: "[W3C][ITS-IG-W3C]",
          type: "string",
          const: "its+xml",
        },
        {
          description: "[RFC4329][RFC9239]",
          type: "string",
          const: "javascript (OBSOLETED in favor of text/javascript)",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "jf2feed+json",
        },
        {
          description: "[RFC7515]",
          type: "string",
          const: "jose",
        },
        {
          description: "[RFC7515]",
          type: "string",
          const: "jose+json",
        },
        {
          description: "[RFC7033]",
          type: "string",
          const: "jrd+json",
        },
        {
          description: "[RFC8984]",
          type: "string",
          const: "jscalendar+json",
        },
        {
          description: "[RFC8259]",
          type: "string",
          const: "json",
        },
        {
          description: "[RFC6902]",
          type: "string",
          const: "json-patch+json",
        },
        {
          description: "[RFC7464]",
          type: "string",
          const: "json-seq",
        },
        {
          description: "[RFC7517]",
          type: "string",
          const: "jwk+json",
        },
        {
          description: "[RFC7517]",
          type: "string",
          const: "jwk-set+json",
        },
        {
          description: "[RFC7519]",
          type: "string",
          const: "jwt",
        },
        {
          description: "[RFC4730]",
          type: "string",
          const: "kpml-request+xml",
        },
        {
          description: "[RFC4730]",
          type: "string",
          const: "kpml-response+xml",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "ld+json",
        },
        {
          description: "[RFC7940]",
          type: "string",
          const: "lgr+xml",
        },
        {
          description: "[RFC6690]",
          type: "string",
          const: "link-format",
        },
        {
          description: "[RFC9264]",
          type: "string",
          const: "linkset",
        },
        {
          description: "[RFC9264]",
          type: "string",
          const: "linkset+json",
        },
        {
          description: "[RFC7200]",
          type: "string",
          const: "load-control+xml",
        },
        {
          description: "[OpenID_Foundation_Artifact_Binding_WG]",
          type: "string",
          const: "logout+jwt",
        },
        {
          description: "[RFC5222]",
          type: "string",
          const: "lost+xml",
        },
        {
          description: "[RFC6739]",
          type: "string",
          const: "lostsync+xml",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "lpf+zip",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "LXF",
        },
        {
          description: "[Patrik_Faltstrom]",
          type: "string",
          const: "mac-binhex40",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "macwriteii",
        },
        {
          description: "[RFC6207]",
          type: "string",
          const: "mads+xml",
        },
        {
          description: "[W3C][Marcos_Caceres]",
          type: "string",
          const: "manifest+json",
        },
        {
          description: "[RFC2220]",
          type: "string",
          const: "marc",
        },
        {
          description: "[RFC6207]",
          type: "string",
          const: "marcxml+xml",
        },
        {
          description: "[Wolfram]",
          type: "string",
          const: "mathematica",
        },
        {
          description: "[W3C][http://www.w3.org/TR/MathML3/appendixb.html]",
          type: "string",
          const: "mathml+xml",
        },
        {
          description: "[W3C][http://www.w3.org/TR/MathML3/appendixb.html]",
          type: "string",
          const: "mathml-content+xml",
        },
        {
          description: "[W3C][http://www.w3.org/TR/MathML3/appendixb.html]",
          type: "string",
          const: "mathml-presentation+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-associated-procedure-description+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-deregister+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-envelope+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-msk-response+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-msk+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-protection-description+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-reception-report+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-register-response+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-register+xml",
        },
        {
          description: "[_3GPP][Eric_Turcotte]",
          type: "string",
          const: "mbms-schedule+xml",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "mbms-user-service-description+xml",
        },
        {
          description: "[RFC4155]",
          type: "string",
          const: "mbox",
        },
        {
          description: "[RFC5168]",
          type: "string",
          const: "media_control+xml",
        },
        {
          description: "[RFC6796]",
          type: "string",
          const: "media-policy-dataset+xml",
        },
        {
          description: "[RFC5022]",
          type: "string",
          const: "mediaservercontrol+xml",
        },
        {
          description: "[RFC7396]",
          type: "string",
          const: "merge-patch+json",
        },
        {
          description: "[RFC5854]",
          type: "string",
          const: "metalink4+xml",
        },
        {
          description: "[RFC6207]",
          type: "string",
          const: "mets+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "MF4",
        },
        {
          description: "[RFC3830]",
          type: "string",
          const: "mikey",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "mipc",
        },
        {
          description: "[RFC9177]",
          type: "string",
          const: "missing-blocks+cbor-seq",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "mmt-aei+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "mmt-usd+xml",
        },
        {
          description: "[RFC6207]",
          type: "string",
          const: "mods+xml",
        },
        {
          description: "[RFC1848]",
          type: "string",
          const: "moss-keys",
        },
        {
          description: "[RFC1848]",
          type: "string",
          const: "moss-signature",
        },
        {
          description: "[RFC1848]",
          type: "string",
          const: "mosskey-data",
        },
        {
          description: "[RFC1848]",
          type: "string",
          const: "mosskey-request",
        },
        {
          description: "[RFC6381][David_Singer]",
          type: "string",
          const: "mp21",
        },
        {
          description: "[RFC4337][RFC6381]",
          type: "string",
          const: "mp4",
        },
        {
          description: "[RFC3640]",
          type: "string",
          const: "mpeg4-generic",
        },
        {
          description: "[RFC4337]",
          type: "string",
          const: "mpeg4-iod",
        },
        {
          description: "[RFC4337]",
          type: "string",
          const: "mpeg4-iod-xmt",
        },
        {
          description: "[RFC6917]",
          type: "string",
          const: "mrb-consumer+xml",
        },
        {
          description: "[RFC6917]",
          type: "string",
          const: "mrb-publish+xml",
        },
        {
          description: "[RFC6231]",
          type: "string",
          const: "msc-ivr+xml",
        },
        {
          description: "[RFC6505]",
          type: "string",
          const: "msc-mixer+xml",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "msword",
        },
        {
          description: "[RFC8520]",
          type: "string",
          const: "mud+json",
        },
        {
          description: "[RFC8710]",
          type: "string",
          const: "multipart-core",
        },
        {
          description: "[RFC4539]",
          type: "string",
          const: "mxf",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "n-quads",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "n-triples",
        },
        {
          description: "[RFC4707]",
          type: "string",
          const: "nasdata",
        },
        {
          description: "[RFC5537]",
          type: "string",
          const: "news-checkgroups",
        },
        {
          description: "[RFC5537]",
          type: "string",
          const: "news-groupinfo",
        },
        {
          description: "[RFC5537]",
          type: "string",
          const: "news-transmission",
        },
        {
          description: "[RFC6787]",
          type: "string",
          const: "nlsml+xml",
        },
        {
          description: "[Node.js_TSC]",
          type: "string",
          const: "node",
        },
        {
          description: "[Michael_Hammer]",
          type: "string",
          const: "nss",
        },
        {
          description: "[RFC9101]",
          type: "string",
          const: "oauth-authz-req+jwt",
        },
        {
          description: "[RFC9230]",
          type: "string",
          const: "oblivious-dns-message",
        },
        {
          description: "[RFC6960]",
          type: "string",
          const: "ocsp-request",
        },
        {
          description: "[RFC6960]",
          type: "string",
          const: "ocsp-response",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "octet-stream",
        },
        {
          description: "[RFC1494]",
          type: "string",
          const: "ODA",
        },
        {
          description: "[CDISC][Sam_Hume]",
          type: "string",
          const: "odm+xml",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "ODX",
        },
        {
          description: "[W3C][EPUB_3_WG]",
          type: "string",
          const: "oebps-package+xml",
        },
        {
          description: "[RFC5334][RFC7845]",
          type: "string",
          const: "ogg",
        },
        {
          description: "[OPC_Foundation]",
          type: "string",
          const: "opc-nodeset+xml",
        },
        {
          description: "[RFC8613]",
          type: "string",
          const: "oscore",
        },
        {
          description: "[Ecma_International_Helpdesk]",
          type: "string",
          const: "oxps",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "p21",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "p21+zip",
        },
        {
          description: "[RFC6940]",
          type: "string",
          const: "p2p-overlay+xml",
        },
        {
          description: "[RFC3009]",
          type: "string",
          const: "parityfec",
        },
        {
          description: "[RFC8225]",
          type: "string",
          const: "passport",
        },
        {
          description: "[RFC5261]",
          type: "string",
          const: "patch-ops-error+xml",
        },
        {
          description: "[RFC8118]",
          type: "string",
          const: "pdf",
        },
        {
          description: "[ASAM][Thomas_Thomsen]",
          type: "string",
          const: "PDX",
        },
        {
          description: "[RFC8555]",
          type: "string",
          const: "pem-certificate-chain",
        },
        {
          description: "[RFC3156]",
          type: "string",
          const: "pgp-encrypted",
        },
        {
          description: "[RFC3156]",
          type: "string",
          const: "pgp-keys",
        },
        {
          description: "[RFC3156]",
          type: "string",
          const: "pgp-signature",
        },
        {
          description: "[RFC5262]",
          type: "string",
          const: "pidf-diff+xml",
        },
        {
          description: "[RFC3863]",
          type: "string",
          const: "pidf+xml",
        },
        {
          description: "[RFC5967]",
          type: "string",
          const: "pkcs10",
        },
        {
          description: "[RFC8551][RFC7114]",
          type: "string",
          const: "pkcs7-mime",
        },
        {
          description: "[RFC8551]",
          type: "string",
          const: "pkcs7-signature",
        },
        {
          description: "[RFC5958]",
          type: "string",
          const: "pkcs8",
        },
        {
          description: "[RFC8351]",
          type: "string",
          const: "pkcs8-encrypted",
        },
        {
          description: "[IETF]",
          type: "string",
          const: "pkcs12",
        },
        {
          description: "[RFC5877]",
          type: "string",
          const: "pkix-attr-cert",
        },
        {
          description: "[RFC2585]",
          type: "string",
          const: "pkix-cert",
        },
        {
          description: "[RFC2585]",
          type: "string",
          const: "pkix-crl",
        },
        {
          description: "[RFC6066]",
          type: "string",
          const: "pkix-pkipath",
        },
        {
          description: "[RFC2510]",
          type: "string",
          const: "pkixcmp",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "pls+xml",
        },
        {
          description: "[RFC4354]",
          type: "string",
          const: "poc-settings+xml",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "postscript",
        },
        {
          description: "[RFC7846]",
          type: "string",
          const: "ppsp-tracker+json",
        },
        {
          description: "[RFC7807]",
          type: "string",
          const: "problem+json",
        },
        {
          description: "[RFC7807]",
          type: "string",
          const: "problem+xml",
        },
        {
          description: "[W3C][Ivan_Herman]",
          type: "string",
          const: "provenance+xml",
        },
        {
          description: "[Harald_T._Alvestrand]",
          type: "string",
          const: "prs.alvestrand.titrax-sheet",
        },
        {
          description: "[Khemchart_Rungchavalnont]",
          type: "string",
          const: "prs.cww",
        },
        {
          description: "[Cynthia_Revström]",
          type: "string",
          const: "prs.cyn",
        },
        {
          description: "[Giulio_Zambon]",
          type: "string",
          const: "prs.hpub+zip",
        },
        {
          description: "[Jay_Doggett]",
          type: "string",
          const: "prs.nprend",
        },
        {
          description: "[Bill_Janssen]",
          type: "string",
          const: "prs.plucker",
        },
        {
          description: "[Toby_Inkster]",
          type: "string",
          const: "prs.rdf-xml-crypt",
        },
        {
          description: "[Maik_Stührenberg]",
          type: "string",
          const: "prs.xsf+xml",
        },
        {
          description: "[RFC6030]",
          type: "string",
          const: "pskc+xml",
        },
        {
          description: "[RFC8801]",
          type: "string",
          const: "pvd+json",
        },
        {
          description: "[RFC3870]",
          type: "string",
          const: "rdf+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "route-apd+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "route-s-tsid+xml",
        },
        {
          description: "[ATSC]",
          type: "string",
          const: "route-usd+xml",
        },
        {
          description: "[RFC3204]",
          type: "string",
          const: "QSIG",
        },
        {
          description: "[RFC6682]",
          type: "string",
          const: "raptorfec",
        },
        {
          description: "[RFC9083]",
          type: "string",
          const: "rdap+json",
        },
        {
          description: "[RFC3680]",
          type: "string",
          const: "reginfo+xml",
        },
        {
          description: "[http://www.jtc1sc34.org/repository/0661.pdf]",
          type: "string",
          const: "relax-ng-compact-syntax",
        },
        {
          description:
            "[RFC1486][Marshall_Rose][status-change-int-tlds-to-historic]",
          type: "string",
          const: "remote-printing (OBSOLETE)",
        },
        {
          description: "[RFC7071]",
          type: "string",
          const: "reputon+json",
        },
        {
          description: "[RFC5362]",
          type: "string",
          const: "resource-lists-diff+xml",
        },
        {
          description: "[RFC4826]",
          type: "string",
          const: "resource-lists+xml",
        },
        {
          description: "[RFC7991]",
          type: "string",
          const: "rfc+xml",
        },
        {
          description: "[Nick_Smith]",
          type: "string",
          const: "riscos",
        },
        {
          description: "[RFC4662]",
          type: "string",
          const: "rlmi+xml",
        },
        {
          description: "[RFC4826]",
          type: "string",
          const: "rls-services+xml",
        },
        {
          description: "[RFC9323]",
          type: "string",
          const: "rpki-checklist",
        },
        {
          description: "[RFC6493]",
          type: "string",
          const: "rpki-ghostbusters",
        },
        {
          description: "[RFC6481]",
          type: "string",
          const: "rpki-manifest",
        },
        {
          description: "[RFC8181]",
          type: "string",
          const: "rpki-publication",
        },
        {
          description: "[RFC6481]",
          type: "string",
          const: "rpki-roa",
        },
        {
          description: "[RFC6492]",
          type: "string",
          const: "rpki-updown",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "rtf",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "rtploopback",
        },
        {
          description: "[RFC4588]",
          type: "string",
          const: "rtx",
        },
        {
          description: "[OASIS_Security_Services_Technical_Committee_SSTC]",
          type: "string",
          const: "samlassertion+xml",
        },
        {
          description: "[OASIS_Security_Services_Technical_Committee_SSTC]",
          type: "string",
          const: "samlmetadata+xml",
        },
        {
          description: "[OASIS][David_Keaton][Michael_C._Fanning]",
          type: "string",
          const: "sarif-external-properties+json",
        },
        {
          description: "[OASIS][Michael_C._Fanning][Laurence_J._Golding]",
          type: "string",
          const: "sarif+json",
        },
        {
          description: "[FIX_Trading_Community][Donald_L._Mendelson]",
          type: "string",
          const: "sbe",
        },
        {
          description: "[RFC3823]",
          type: "string",
          const: "sbml+xml",
        },
        {
          description: "[SIS][Oskar_Jonsson]",
          type: "string",
          const: "scaip+xml",
        },
        {
          description: "[RFC7644]",
          type: "string",
          const: "scim+json",
        },
        {
          description: "[RFC5055]",
          type: "string",
          const: "scvp-cv-request",
        },
        {
          description: "[RFC5055]",
          type: "string",
          const: "scvp-cv-response",
        },
        {
          description: "[RFC5055]",
          type: "string",
          const: "scvp-vp-request",
        },
        {
          description: "[RFC5055]",
          type: "string",
          const: "scvp-vp-response",
        },
        {
          description: "[RFC8866]",
          type: "string",
          const: "sdp",
        },
        {
          description: "[RFC8417]",
          type: "string",
          const: "secevent+jwt",
        },
        {
          description: "[RFC8790]",
          type: "string",
          const: "senml-etch+cbor",
        },
        {
          description: "[RFC8790]",
          type: "string",
          const: "senml-etch+json",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "senml-exi",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "senml+cbor",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "senml+json",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "senml+xml",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "sensml-exi",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "sensml+cbor",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "sensml+json",
        },
        {
          description: "[RFC8428]",
          type: "string",
          const: "sensml+xml",
        },
        {
          description: "[Robby_Simpson][ZigBee]",
          type: "string",
          const: "sep-exi",
        },
        {
          description: "[Robby_Simpson][ZigBee]",
          type: "string",
          const: "sep+xml",
        },
        {
          description: "[_3GPP][Frederic_Firmin]",
          type: "string",
          const: "session-info",
        },
        {
          description: "[Brian_Korver]",
          type: "string",
          const: "set-payment",
        },
        {
          description: "[Brian_Korver]",
          type: "string",
          const: "set-payment-initiation",
        },
        {
          description: "[Brian_Korver]",
          type: "string",
          const: "set-registration",
        },
        {
          description: "[Brian_Korver]",
          type: "string",
          const: "set-registration-initiation",
        },
        {
          description: "[RFC1874]",
          type: "string",
          const: "SGML",
        },
        {
          description: "[Paul_Grosso]",
          type: "string",
          const: "sgml-open-catalog",
        },
        {
          description: "[RFC4194]",
          type: "string",
          const: "shf+xml",
        },
        {
          description: "[RFC5228]",
          type: "string",
          const: "sieve",
        },
        {
          description: "[RFC4661]",
          type: "string",
          const: "simple-filter+xml",
        },
        {
          description: "[RFC3842]",
          type: "string",
          const: "simple-message-summary",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "simpleSymbolContainer",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "sipc",
        },
        {
          description: "[Terry_Crowley]",
          type: "string",
          const: "slate",
        },
        {
          description: "[RFC4536]",
          type: "string",
          const: "smil (OBSOLETED in favor of application/smil+xml)",
        },
        {
          description: "[RFC4536]",
          type: "string",
          const: "smil+xml",
        },
        {
          description: "[RFC6597]",
          type: "string",
          const: "smpte336m",
        },
        {
          description:
            "[ITU-T_ASN.1_Rapporteur][ISO-IEC_JTC1_SC6_ASN.1_Rapporteur]",
          type: "string",
          const: "soap+fastinfoset",
        },
        {
          description: "[RFC3902]",
          type: "string",
          const: "soap+xml",
        },
        {
          description:
            "[W3C][http://www.w3.org/TR/2007/CR-rdf-sparql-query-20070614/#mediaType]",
          type: "string",
          const: "sparql-query",
        },
        {
          description: "[Linux_Foundation][Rose_Judge]",
          type: "string",
          const: "spdx+json",
        },
        {
          description:
            "[W3C][http://www.w3.org/TR/2007/CR-rdf-sparql-XMLres-20070925/#mime]",
          type: "string",
          const: "sparql-results+xml",
        },
        {
          description: "[RFC3910]",
          type: "string",
          const: "spirits-event+xml",
        },
        {
          description: "[RFC6922]",
          type: "string",
          const: "sql",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "srgs",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "srgs+xml",
        },
        {
          description: "[RFC6207]",
          type: "string",
          const: "sru+xml",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "ssml+xml",
        },
        {
          description: "[OASIS][Chet_Ensign]",
          type: "string",
          const: "stix+json",
        },
        {
          description: "[RFC-ietf-sacm-coswid-22]",
          type: "string",
          const: "swid+cbor",
        },
        {
          description: "[ISO-IEC_JTC1][David_Waltermire][Ron_Brill]",
          type: "string",
          const: "swid+xml",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-apex-update",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-apex-update-confirm",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-community-update",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-community-update-confirm",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-error",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-sequence-adjust",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-sequence-adjust-confirm",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-status-query",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-status-response",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-update",
        },
        {
          description: "[RFC5934]",
          type: "string",
          const: "tamp-update-confirm",
        },
        {
          description: "[OASIS][Chet_Ensign]",
          type: "string",
          const: "taxii+json",
        },
        {
          description: "[W3C][Matthias_Kovatsch]",
          type: "string",
          const: "td+json",
        },
        {
          description: "[RFC6129]",
          type: "string",
          const: "tei+xml",
        },
        {
          description: "[ETSI][Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "TETRA_ISI",
        },
        {
          description: "[RFC5941]",
          type: "string",
          const: "thraud+xml",
        },
        {
          description: "[RFC3161]",
          type: "string",
          const: "timestamp-query",
        },
        {
          description: "[RFC3161]",
          type: "string",
          const: "timestamp-reply",
        },
        {
          description: "[RFC5955]",
          type: "string",
          const: "timestamped-data",
        },
        {
          description: "[RFC8460]",
          type: "string",
          const: "tlsrpt+gzip",
        },
        {
          description: "[RFC8460]",
          type: "string",
          const: "tlsrpt+json",
        },
        {
          description: "[W3C][Sebastian_Kaebisch]",
          type: "string",
          const: "tm+json",
        },
        {
          description: "[RFC8226]",
          type: "string",
          const: "tnauthlist",
        },
        {
          description: "[RFC-oauth-jwt-introspection-response-12]",
          type: "string",
          const: "token-introspection+jwt",
        },
        {
          description: "[RFC8840]",
          type: "string",
          const: "trickle-ice-sdpfrag",
        },
        {
          description: "[W3C][W3C_RDF_Working_Group]",
          type: "string",
          const: "trig",
        },
        {
          description: "[W3C][W3C_Timed_Text_Working_Group]",
          type: "string",
          const: "ttml+xml",
        },
        {
          description: "[Linda_Welsh]",
          type: "string",
          const: "tve-trigger",
        },
        {
          description: "[RFC8536]",
          type: "string",
          const: "tzif",
        },
        {
          description: "[RFC8536]",
          type: "string",
          const: "tzif-leap",
        },
        {
          description: "[RFC5109]",
          type: "string",
          const: "ulpfec",
        },
        {
          description: "[Gottfried_Zimmermann][ISO-IEC_JTC1]",
          type: "string",
          const: "urc-grpsheet+xml",
        },
        {
          description: "[Gottfried_Zimmermann][ISO-IEC_JTC1]",
          type: "string",
          const: "urc-ressheet+xml",
        },
        {
          description: "[Gottfried_Zimmermann][ISO-IEC_JTC1]",
          type: "string",
          const: "urc-targetdesc+xml",
        },
        {
          description: "[Gottfried_Zimmermann]",
          type: "string",
          const: "urc-uisocketdesc+xml",
        },
        {
          description: "[RFC7095]",
          type: "string",
          const: "vcard+json",
        },
        {
          description: "[RFC6351]",
          type: "string",
          const: "vcard+xml",
        },
        {
          description: "[RFC2122]",
          type: "string",
          const: "vemmi",
        },
        {
          description: "[Franz_Ombler]",
          type: "string",
          const: "vnd.1000minds.decision-model+xml",
        },
        {
          description: "[_3GPP][Jones_Lu_Yunjie]",
          type: "string",
          const: "vnd.3gpp.5gnas",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.access-transfer-events+xml",
        },
        {
          description: "[John_M_Meredith]",
          type: "string",
          const: "vnd.3gpp.bsf+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.GMOP+xml",
        },
        {
          description: "[_3GPP][Yang_Yong]",
          type: "string",
          const: "vnd.3gpp.gtpc",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.interworking-data",
        },
        {
          description: "[_3GPP][Jones_Lu_Yunjie]",
          type: "string",
          const: "vnd.3gpp.lpp",
        },
        {
          description: "[Tim_Woodward]",
          type: "string",
          const: "vnd.3gpp.mc-signalling-ear",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-affiliation-command+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-info+xml",
        },
        {
          description: "[Kiran_Kapale]",
          type: "string",
          const: "vnd.3gpp.mcdata-msgstore-ctrl-request+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-payload",
        },
        {
          description: "[Kiran_Kapale]",
          type: "string",
          const: "vnd.3gpp.mcdata-regroup+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-service-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-signalling",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-ue-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcdata-user-profile+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-affiliation-command+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-floor-request+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-location-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-mbms-usage-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-service-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-signed+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-ue-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-ue-init-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcptt-user-profile+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-affiliation-command+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const:
            "vnd.3gpp.mcvideo-affiliation-info+xml (OBSOLETED in favor of application/vnd.3gpp.mcvideo-info+xml)",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-location-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-mbms-usage-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-service-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-transmission-request+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-ue-config+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mcvideo-user-profile+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.mid-call+xml",
        },
        {
          description: "[_3GPP][Yang_Yong]",
          type: "string",
          const: "vnd.3gpp.ngap",
        },
        {
          description: "[_3GPP][Bruno_Landais]",
          type: "string",
          const: "vnd.3gpp.pfcp",
        },
        {
          description: "[John_M_Meredith]",
          type: "string",
          const: "vnd.3gpp.pic-bw-large",
        },
        {
          description: "[John_M_Meredith]",
          type: "string",
          const: "vnd.3gpp.pic-bw-small",
        },
        {
          description: "[John_M_Meredith]",
          type: "string",
          const: "vnd.3gpp.pic-bw-var",
        },
        {
          description: "[Haorui_Yang]",
          type: "string",
          const: "vnd.3gpp-prose-pc3a+xml",
        },
        {
          description: "[Haorui_Yang]",
          type: "string",
          const: "vnd.3gpp-prose-pc3ach+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp-prose-pc3ch+xml",
        },
        {
          description: "[Haorui_Yang]",
          type: "string",
          const: "vnd.3gpp-prose-pc8+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp-prose+xml",
        },
        {
          description: "[_3GPP][Yang_Yong]",
          type: "string",
          const: "vnd.3gpp.s1ap",
        },
        {
          description: "[John_M_Meredith]",
          type: "string",
          const: "vnd.3gpp.sms",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.sms+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.srvcc-ext+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.SRVCC-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.state-and-event-info+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp.ussd+xml",
        },
        {
          description: "[Frederic_Firmin]",
          type: "string",
          const: "vnd.3gpp-v2x-local-service-information",
        },
        {
          description: "[AC_Mahendran]",
          type: "string",
          const: "vnd.3gpp2.bcmcsinfo+xml",
        },
        {
          description: "[AC_Mahendran]",
          type: "string",
          const: "vnd.3gpp2.sms",
        },
        {
          description: "[AC_Mahendran]",
          type: "string",
          const: "vnd.3gpp2.tcap",
        },
        {
          description: "[Gus_Asadi]",
          type: "string",
          const: "vnd.3lightssoftware.imagescal",
        },
        {
          description: "[Michael_OBrien]",
          type: "string",
          const: "vnd.3M.Post-it-Notes",
        },
        {
          description: "[Steve_Leow]",
          type: "string",
          const: "vnd.accpac.simply.aso",
        },
        {
          description: "[Steve_Leow]",
          type: "string",
          const: "vnd.accpac.simply.imp",
        },
        {
          description: "[Dovid_Lubin]",
          type: "string",
          const: "vnd.acucobol",
        },
        {
          description: "[Dovid_Lubin]",
          type: "string",
          const: "vnd.acucorp",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.adobe.flash.movie",
        },
        {
          description: "[Chris_Solc]",
          type: "string",
          const: "vnd.adobe.formscentral.fcdt",
        },
        {
          description: "[Steven_Heintz]",
          type: "string",
          const: "vnd.adobe.fxp",
        },
        {
          description: "[Tapani_Otala]",
          type: "string",
          const: "vnd.adobe.partial-upload",
        },
        {
          description: "[John_Brinkman]",
          type: "string",
          const: "vnd.adobe.xdp+xml",
        },
        {
          description: "[Jay_Moskowitz]",
          type: "string",
          const: "vnd.aether.imp",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.afplinedata",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.afplinedata-pagedef",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.cmoca-cmresource",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.foca-charset",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.foca-codedfont",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.foca-codepage",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-cmtable",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-formdef",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-mediummap",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-objectcontainer",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-overlay",
        },
        {
          description: "[Jörg_Palmer]",
          type: "string",
          const: "vnd.afpc.modca-pagesegment",
        },
        {
          description: "[Filippo_Valsorda]",
          type: "string",
          const: "vnd.age",
        },
        {
          description: "[Katsuhiko_Ichinose]",
          type: "string",
          const: "vnd.ah-barcode",
        },
        {
          description: "[Tor_Kristensen]",
          type: "string",
          const: "vnd.ahead.space",
        },
        {
          description: "[Daniel_Mould][Gary_Clueit]",
          type: "string",
          const: "vnd.airzip.filesecure.azf",
        },
        {
          description: "[Daniel_Mould][Gary_Clueit]",
          type: "string",
          const: "vnd.airzip.filesecure.azs",
        },
        {
          description: "[Patrick_Brosse]",
          type: "string",
          const: "vnd.amadeus+json",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.amazon.mobi8-ebook",
        },
        {
          description: "[Gary_Sands]",
          type: "string",
          const: "vnd.americandynamics.acc",
        },
        {
          description: "[Kevin_Blumberg]",
          type: "string",
          const: "vnd.amiga.ami",
        },
        {
          description: "[Mike_Amundsen]",
          type: "string",
          const: "vnd.amundsen.maze+xml",
        },
        {
          description: "[Greg_Kaiser]",
          type: "string",
          const: "vnd.android.ota",
        },
        {
          description: "[Kerrick_Staley]",
          type: "string",
          const: "vnd.anki",
        },
        {
          description: "[Hiroyoshi_Mori]",
          type: "string",
          const: "vnd.anser-web-certificate-issue-initiation",
        },
        {
          description: "[Daniel_Shelton]",
          type: "string",
          const: "vnd.antix.game-component",
        },
        {
          description: "[Apache_Arrow_Project]",
          type: "string",
          const: "vnd.apache.arrow.file",
        },
        {
          description: "[Apache_Arrow_Project]",
          type: "string",
          const: "vnd.apache.arrow.stream",
        },
        {
          description: "[Roger_Meier]",
          type: "string",
          const: "vnd.apache.thrift.binary",
        },
        {
          description: "[Roger_Meier]",
          type: "string",
          const: "vnd.apache.thrift.compact",
        },
        {
          description: "[Roger_Meier]",
          type: "string",
          const: "vnd.apache.thrift.json",
        },
        {
          description: "[Fawad_Shaikh]",
          type: "string",
          const: "vnd.apexlang",
        },
        {
          description: "[Steve_Klabnik]",
          type: "string",
          const: "vnd.api+json",
        },
        {
          description: "[Oleg_Uryutin]",
          type: "string",
          const: "vnd.aplextor.warrp+json",
        },
        {
          description: "[Adrian_Föder]",
          type: "string",
          const: "vnd.apothekende.reservation+json",
        },
        {
          description: "[Peter_Bierman]",
          type: "string",
          const: "vnd.apple.installer+xml",
        },
        {
          description: "[Manichandra_Sajjanapu]",
          type: "string",
          const: "vnd.apple.keynote",
        },
        {
          description: "[RFC8216]",
          type: "string",
          const: "vnd.apple.mpegurl",
        },
        {
          description: "[Manichandra_Sajjanapu]",
          type: "string",
          const: "vnd.apple.numbers",
        },
        {
          description: "[Manichandra_Sajjanapu]",
          type: "string",
          const: "vnd.apple.pages",
        },
        {
          description: "[Bill_Fenner]",
          type: "string",
          const:
            "vnd.arastra.swi (OBSOLETED in favor of application/vnd.aristanetworks.swi)",
        },
        {
          description: "[Bill_Fenner]",
          type: "string",
          const: "vnd.aristanetworks.swi",
        },
        {
          description: "[Brad_Turner]",
          type: "string",
          const: "vnd.artisan+json",
        },
        {
          description: "[Christopher_Smith]",
          type: "string",
          const: "vnd.artsquare",
        },
        {
          description: "[Christopher_Snazell]",
          type: "string",
          const: "vnd.astraea-software.iota",
        },
        {
          description: "[Horia_Cristian_Slusanschi]",
          type: "string",
          const: "vnd.audiograph",
        },
        {
          description: "[Mike_Hearn]",
          type: "string",
          const: "vnd.autopackage",
        },
        {
          description: "[Ben_Hinman]",
          type: "string",
          const: "vnd.avalon+json",
        },
        {
          description: "[Vladimir_Vysotsky]",
          type: "string",
          const: "vnd.avistar+xml",
        },
        {
          description: "[Giacomo_Guilizzoni]",
          type: "string",
          const: "vnd.balsamiq.bmml+xml",
        },
        {
          description: "[José_Del_Romano]",
          type: "string",
          const: "vnd.banana-accounting",
        },
        {
          description: "[Broadband_Forum]",
          type: "string",
          const: "vnd.bbf.usp.error",
        },
        {
          description: "[Broadband_Forum]",
          type: "string",
          const: "vnd.bbf.usp.msg",
        },
        {
          description: "[Broadband_Forum]",
          type: "string",
          const: "vnd.bbf.usp.msg+json",
        },
        {
          description: "[Giacomo_Guilizzoni]",
          type: "string",
          const: "vnd.balsamiq.bmpr",
        },
        {
          description: "[Jegulsky]",
          type: "string",
          const: "vnd.bekitzur-stech+json",
        },
        {
          description: "[Dmytro_Yunchyk]",
          type: "string",
          const: "vnd.belightsoft.lhzd+zip",
        },
        {
          description: "[Dmytro_Yunchyk]",
          type: "string",
          const: "vnd.belightsoft.lhzl+zip",
        },
        {
          description: "[Heinz-Peter_Schütz]",
          type: "string",
          const: "vnd.bint.med-content",
        },
        {
          description: "[Pathway_Commons]",
          type: "string",
          const: "vnd.biopax.rdf+xml",
        },
        {
          description: "[Victor_Costan]",
          type: "string",
          const: "vnd.blink-idb-value-wrapper",
        },
        {
          description: "[Thomas_Holmstrom]",
          type: "string",
          const: "vnd.blueice.multipass",
        },
        {
          description: "[Mike_Foley]",
          type: "string",
          const: "vnd.bluetooth.ep.oob",
        },
        {
          description: "[Mark_Powell]",
          type: "string",
          const: "vnd.bluetooth.le.oob",
        },
        {
          description: "[Tadashi_Gotoh]",
          type: "string",
          const: "vnd.bmi",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "vnd.bpf",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "vnd.bpf3",
        },
        {
          description: "[Philippe_Imoucha]",
          type: "string",
          const: "vnd.businessobjects",
        },
        {
          description: "[Brent_Moore]",
          type: "string",
          const: "vnd.byu.uapi+json",
        },
        {
          description: "[Joerg_Falkenberg]",
          type: "string",
          const: "vnd.cab-jscript",
        },
        {
          description: "[Shin_Muto]",
          type: "string",
          const: "vnd.canon-cpdl",
        },
        {
          description: "[Shin_Muto]",
          type: "string",
          const: "vnd.canon-lips",
        },
        {
          description: "[Yüksel_Aydemir]",
          type: "string",
          const: "vnd.capasystems-pg+json",
        },
        {
          description: "[Peter_Astrand]",
          type: "string",
          const: "vnd.cendio.thinlinc.clientconf",
        },
        {
          description: "[Shuji_Fujii]",
          type: "string",
          const: "vnd.century-systems.tcp_stream",
        },
        {
          description: "[Glenn_Howes]",
          type: "string",
          const: "vnd.chemdraw+xml",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.chess-pgn",
        },
        {
          description: "[Chunyun_Xiong]",
          type: "string",
          const: "vnd.chipnuts.karaoke-mmd",
        },
        {
          description: "[Hidekazu_Enjo]",
          type: "string",
          const: "vnd.ciedi",
        },
        {
          description: "[Ulrich_Kortenkamp]",
          type: "string",
          const: "vnd.cinderella",
        },
        {
          description: "[Pascal_Mayeux]",
          type: "string",
          const: "vnd.cirpack.isdn-ext",
        },
        {
          description: "[Rintze_M._Zelle]",
          type: "string",
          const: "vnd.citationstyles.style+xml",
        },
        {
          description: "[Ray_Simpson]",
          type: "string",
          const: "vnd.claymore",
        },
        {
          description: "[Mike_Labatt]",
          type: "string",
          const: "vnd.cloanto.rp9",
        },
        {
          description: "[Guenther_Brammer]",
          type: "string",
          const: "vnd.clonk.c4group",
        },
        {
          description: "[Gaige_Paulsen]",
          type: "string",
          const: "vnd.cluetrust.cartomobile-config",
        },
        {
          description: "[Gaige_Paulsen]",
          type: "string",
          const: "vnd.cluetrust.cartomobile-config-pkg",
        },
        {
          description: "[Andrew_Block]",
          type: "string",
          const: "vnd.cncf.helm.chart.content.v1.tar+gzip",
        },
        {
          description: "[Andrew_Block]",
          type: "string",
          const: "vnd.cncf.helm.chart.provenance.v1.prov",
        },
        {
          description: "[Devyn_Collier_Johnson]",
          type: "string",
          const: "vnd.coffeescript",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.document",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.document-template",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.presentation",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.presentation-template",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.spreadsheet",
        },
        {
          description: "[Alexey_Meandrov]",
          type: "string",
          const: "vnd.collabio.xodocuments.spreadsheet-template",
        },
        {
          description: "[Irakli_Nadareishvili]",
          type: "string",
          const: "vnd.collection.doc+json",
        },
        {
          description: "[Mike_Amundsen]",
          type: "string",
          const: "vnd.collection+json",
        },
        {
          description: "[Ioseb_Dzmanashvili]",
          type: "string",
          const: "vnd.collection.next+json",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.comicbook-rar",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.comicbook+zip",
        },
        {
          description: "[David_Applebaum]",
          type: "string",
          const: "vnd.commerce-battelle",
        },
        {
          description: "[Ravinder_Chandhok]",
          type: "string",
          const: "vnd.commonspace",
        },
        {
          description: "[Alex_Crawford]",
          type: "string",
          const: "vnd.coreos.ignition+json",
        },
        {
          description: "[Steve_Dellutri]",
          type: "string",
          const: "vnd.cosmocaller",
        },
        {
          description: "[Frank_Patz]",
          type: "string",
          const: "vnd.contact.cmsg",
        },
        {
          description: "[Andrew_Burt]",
          type: "string",
          const: "vnd.crick.clicker",
        },
        {
          description: "[Andrew_Burt]",
          type: "string",
          const: "vnd.crick.clicker.keyboard",
        },
        {
          description: "[Andrew_Burt]",
          type: "string",
          const: "vnd.crick.clicker.palette",
        },
        {
          description: "[Andrew_Burt]",
          type: "string",
          const: "vnd.crick.clicker.template",
        },
        {
          description: "[Andrew_Burt]",
          type: "string",
          const: "vnd.crick.clicker.wordbank",
        },
        {
          description: "[Jim_Spiller]",
          type: "string",
          const: "vnd.criticaltools.wbs+xml",
        },
        {
          description: "[Fränz_Friederes]",
          type: "string",
          const: "vnd.cryptii.pipe+json",
        },
        {
          description: "[Connor_Horman]",
          type: "string",
          const: "vnd.crypto-shade-file",
        },
        {
          description: "[Sebastian_Stenzel]",
          type: "string",
          const: "vnd.cryptomator.encrypted",
        },
        {
          description: "[Sebastian_Stenzel]",
          type: "string",
          const: "vnd.cryptomator.vault",
        },
        {
          description: "[Bayard_Kohlhepp]",
          type: "string",
          const: "vnd.ctc-posml",
        },
        {
          description: "[Jim_Ancona]",
          type: "string",
          const: "vnd.ctct.ws+xml",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "vnd.cups-pdf",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "vnd.cups-postscript",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "vnd.cups-ppd",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "vnd.cups-raster",
        },
        {
          description: "[Michael_Sweet]",
          type: "string",
          const: "vnd.cups-raw",
        },
        {
          description: "[Robert_Byrnes]",
          type: "string",
          const: "vnd.curl",
        },
        {
          description: "[Matt_Kern]",
          type: "string",
          const: "vnd.cyan.dean.root+xml",
        },
        {
          description: "[Nor_Helmee]",
          type: "string",
          const: "vnd.cybank",
        },
        {
          description: "[Patrick_Dwyer]",
          type: "string",
          const: "vnd.cyclonedx+json",
        },
        {
          description: "[Patrick_Dwyer]",
          type: "string",
          const: "vnd.cyclonedx+xml",
        },
        {
          description: "[Viktor_Haag]",
          type: "string",
          const: "vnd.d2l.coursepackage1p0+zip",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.d3m-dataset",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.d3m-problem",
        },
        {
          description: "[Anders_Sandholm]",
          type: "string",
          const: "vnd.dart",
        },
        {
          description: "[James_Fields]",
          type: "string",
          const: "vnd.data-vision.rdz",
        },
        {
          description: "[Simon_Johnston]",
          type: "string",
          const: "vnd.datalog",
        },
        {
          description: "[Paul_Walsh]",
          type: "string",
          const: "vnd.datapackage+json",
        },
        {
          description: "[Paul_Walsh]",
          type: "string",
          const: "vnd.dataresource+json",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.dbf",
        },
        {
          description: "[Debian_Policy_mailing_list]",
          type: "string",
          const: "vnd.debian.binary-package",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.data",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.ttml+xml",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.unspecified",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.zip",
        },
        {
          description: "[Michael_Dixon]",
          type: "string",
          const: "vnd.denovo.fcselayout-link",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.desmume.movie",
        },
        {
          description: "[Yamanaka]",
          type: "string",
          const: "vnd.dir-bi.plate-dl-nosuffix",
        },
        {
          description: "[Axel_Ferrazzini]",
          type: "string",
          const: "vnd.dm.delegation+xml",
        },
        {
          description: "[Meredith_Searcy]",
          type: "string",
          const: "vnd.dna",
        },
        {
          description: "[Tom_Christie]",
          type: "string",
          const: "vnd.document+json",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.mobile.1",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.mobile.2",
        },
        {
          description: "[Erik_Ronström]",
          type: "string",
          const: "vnd.doremir.scorecloud-binary-document",
        },
        {
          description: "[David_Parker]",
          type: "string",
          const: "vnd.dpgraph",
        },
        {
          description: "[William_C._Appleton]",
          type: "string",
          const: "vnd.dreamfactory",
        },
        {
          description: "[Keith_Kester]",
          type: "string",
          const: "vnd.drive+json",
        },
        {
          description: "[Ali_Teffahi]",
          type: "string",
          const: "vnd.dtg.local",
        },
        {
          description: "[Ali_Teffahi]",
          type: "string",
          const: "vnd.dtg.local.flash",
        },
        {
          description: "[Ali_Teffahi]",
          type: "string",
          const: "vnd.dtg.local.html",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.ait",
        },
        {
          description: "[Emily_DUBS]",
          type: "string",
          const: "vnd.dvb.dvbisl+xml",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.dvbj",
        },
        {
          description: "[Joerg_Heuer]",
          type: "string",
          const: "vnd.dvb.esgcontainer",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.ipdcdftnotifaccess",
        },
        {
          description: "[Joerg_Heuer]",
          type: "string",
          const: "vnd.dvb.ipdcesgaccess",
        },
        {
          description: "[Jerome_Marcon]",
          type: "string",
          const: "vnd.dvb.ipdcesgaccess2",
        },
        {
          description: "[Jerome_Marcon]",
          type: "string",
          const: "vnd.dvb.ipdcesgpdd",
        },
        {
          description: "[Yiling_Xu]",
          type: "string",
          const: "vnd.dvb.ipdcroaming",
        },
        {
          description: "[Jean-Baptiste_Henry]",
          type: "string",
          const: "vnd.dvb.iptv.alfec-base",
        },
        {
          description: "[Jean-Baptiste_Henry]",
          type: "string",
          const: "vnd.dvb.iptv.alfec-enhancement",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-aggregate-root+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-container+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-generic+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-ia-msglist+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-ia-registration-request+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-ia-registration-response+xml",
        },
        {
          description: "[Roy_Yue]",
          type: "string",
          const: "vnd.dvb.notif-init+xml",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.pfr",
        },
        {
          description: "[Peter_Siebert][Michael_Lagally]",
          type: "string",
          const: "vnd.dvb.service",
        },
        {
          description: "[Michael_Duffy]",
          type: "string",
          const: "vnd.dxr",
        },
        {
          description: "[Roland_Mechling]",
          type: "string",
          const: "vnd.dynageo",
        },
        {
          description: "[Carl_Anderson]",
          type: "string",
          const: "vnd.dzr",
        },
        {
          description: "[Iain_Downs]",
          type: "string",
          const: "vnd.easykaraoke.cdgdownload",
        },
        {
          description: "[Wei_Tang]",
          type: "string",
          const: "vnd.ecip.rlp",
        },
        {
          description: "[Gert_Buettgenbach]",
          type: "string",
          const: "vnd.ecdis-update",
        },
        {
          description: "[Eclipse_Ditto_developers]",
          type: "string",
          const: "vnd.eclipse.ditto+json",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.chart",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.filerequest",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.fileupdate",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.series",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.seriesrequest",
        },
        {
          description: "[Thomas_Olsson]",
          type: "string",
          const: "vnd.ecowin.seriesupdate",
        },
        {
          description: "[UEFI_Forum][Fu_Siyuan]",
          type: "string",
          const: "vnd.efi.img",
        },
        {
          description: "[UEFI_Forum][Fu_Siyuan]",
          type: "string",
          const: "vnd.efi.iso",
        },
        {
          description: "[Filip_Navara]",
          type: "string",
          const: "vnd.emclient.accessrequest+xml",
        },
        {
          description: "[Paul_Santinelli_Jr.]",
          type: "string",
          const: "vnd.enliven",
        },
        {
          description: "[Chris_Eich]",
          type: "string",
          const: "vnd.enphase.envoy",
        },
        {
          description: "[Tim_Brody]",
          type: "string",
          const: "vnd.eprints.data+xml",
        },
        {
          description: "[Shoji_Hoshina]",
          type: "string",
          const: "vnd.epson.esf",
        },
        {
          description: "[Shoji_Hoshina]",
          type: "string",
          const: "vnd.epson.msf",
        },
        {
          description: "[Yu_Gu]",
          type: "string",
          const: "vnd.epson.quickanime",
        },
        {
          description: "[Yasuhito_Nagatomo]",
          type: "string",
          const: "vnd.epson.salt",
        },
        {
          description: "[Shoji_Hoshina]",
          type: "string",
          const: "vnd.epson.ssf",
        },
        {
          description: "[Paul_Tidwell]",
          type: "string",
          const: "vnd.ericsson.quickcall",
        },
        {
          description: "[Marcus_Ligi_Büschleb]",
          type: "string",
          const: "vnd.espass-espass+zip",
        },
        {
          description: "[Szilveszter_Tóth]",
          type: "string",
          const: "vnd.eszigno3+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.aoc+xml",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.asic-s+zip",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.asic-e+zip",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.cug+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvcommand+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvdiscovery+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvprofile+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvsad-bc+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvsad-cod+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvsad-npvr+xml",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.iptvservice+xml",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.iptvsync+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.iptvueprofile+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.mcid+xml",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega][Ian_Medland]",
          type: "string",
          const: "vnd.etsi.mheg5",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.overload-control-policy-dataset+xml",
        },
        {
          description: "[Jiwan_Han][Thomas_Belling]",
          type: "string",
          const: "vnd.etsi.pstn+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.sci+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.simservs+xml",
        },
        {
          description: "[Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "vnd.etsi.timestamp-token",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.tsl+xml",
        },
        {
          description: "[Shicheng_Hu]",
          type: "string",
          const: "vnd.etsi.tsl.der",
        },
        {
          description: "[Hervé_Kasparian]",
          type: "string",
          const: "vnd.eu.kasparian.car+json",
        },
        {
          description: "[Pete_Resnick]",
          type: "string",
          const: "vnd.eudora.data",
        },
        {
          description: "[James_Bellinger]",
          type: "string",
          const: "vnd.evolv.ecig.profile",
        },
        {
          description: "[James_Bellinger]",
          type: "string",
          const: "vnd.evolv.ecig.settings",
        },
        {
          description: "[James_Bellinger]",
          type: "string",
          const: "vnd.evolv.ecig.theme",
        },
        {
          description: "[Bill_Kidwell]",
          type: "string",
          const: "vnd.exstream-empower+zip",
        },
        {
          description: "[Bill_Kidwell]",
          type: "string",
          const: "vnd.exstream-package",
        },
        {
          description: "[ElectronicZombieCorp]",
          type: "string",
          const: "vnd.ezpix-album",
        },
        {
          description: "[ElectronicZombieCorp]",
          type: "string",
          const: "vnd.ezpix-package",
        },
        {
          description: "[Samu_Sarivaara]",
          type: "string",
          const: "vnd.f-secure.mobile",
        },
        {
          description: "[Thomas_Huth]",
          type: "string",
          const: "vnd.fastcopy-disk-image",
        },
        {
          description: "[Gordon_Clarke]",
          type: "string",
          const: "vnd.familysearch.gedcom+zip",
        },
        {
          description: "[Chad_Trabant]",
          type: "string",
          const: "vnd.fdsn.mseed",
        },
        {
          description: "[Chad_Trabant]",
          type: "string",
          const: "vnd.fdsn.seed",
        },
        {
          description: "[Holstage]",
          type: "string",
          const: "vnd.ffsns",
        },
        {
          description: "[Steve_Gilberd]",
          type: "string",
          const: "vnd.ficlab.flb+zip",
        },
        {
          description: "[Harms_Moeller]",
          type: "string",
          const: "vnd.filmit.zfc",
        },
        {
          description: "[Ingo_Hammann]",
          type: "string",
          const: "vnd.fints",
        },
        {
          description: "[Alex_Dubov]",
          type: "string",
          const: "vnd.firemonkeys.cloudcell",
        },
        {
          description: "[Dick_Floersch]",
          type: "string",
          const: "vnd.FloGraphIt",
        },
        {
          description: "[Marc_Winter]",
          type: "string",
          const: "vnd.fluxtime.clip",
        },
        {
          description: "[George_Williams]",
          type: "string",
          const: "vnd.font-fontforge-sfd",
        },
        {
          description: "[Mike_Wexler]",
          type: "string",
          const: "vnd.framemaker",
        },
        {
          description: "[OP3FT][Alexis_Tamas]",
          type: "string",
          const: "vnd.frogans.fnc (OBSOLETE)",
        },
        {
          description: "[OP3FT][Alexis_Tamas]",
          type: "string",
          const: "vnd.frogans.ltf (OBSOLETE)",
        },
        {
          description: "[Derek_Smith]",
          type: "string",
          const: "vnd.fsc.weblaunch",
        },
        {
          description: "[Kazuya_Iimura]",
          type: "string",
          const: "vnd.fujifilm.fb.docuworks",
        },
        {
          description: "[Kazuya_Iimura]",
          type: "string",
          const: "vnd.fujifilm.fb.docuworks.binder",
        },
        {
          description: "[Kazuya_Iimura]",
          type: "string",
          const: "vnd.fujifilm.fb.docuworks.container",
        },
        {
          description: "[Keitaro_Ishida]",
          type: "string",
          const: "vnd.fujifilm.fb.jfi+xml",
        },
        {
          description: "[Nobukazu_Togashi]",
          type: "string",
          const: "vnd.fujitsu.oasys",
        },
        {
          description: "[Nobukazu_Togashi]",
          type: "string",
          const: "vnd.fujitsu.oasys2",
        },
        {
          description: "[Seiji_Okudaira]",
          type: "string",
          const: "vnd.fujitsu.oasys3",
        },
        {
          description: "[Masahiko_Sugimoto]",
          type: "string",
          const: "vnd.fujitsu.oasysgp",
        },
        {
          description: "[Masumi_Ogita]",
          type: "string",
          const: "vnd.fujitsu.oasysprs",
        },
        {
          description: "[Fumio_Tanabe]",
          type: "string",
          const: "vnd.fujixerox.ART4",
        },
        {
          description: "[Fumio_Tanabe]",
          type: "string",
          const: "vnd.fujixerox.ART-EX",
        },
        {
          description: "[Masanori_Onda]",
          type: "string",
          const: "vnd.fujixerox.ddd",
        },
        {
          description: "[Takatomo_Wakibayashi]",
          type: "string",
          const: "vnd.fujixerox.docuworks",
        },
        {
          description: "[Takashi_Matsumoto]",
          type: "string",
          const: "vnd.fujixerox.docuworks.binder",
        },
        {
          description: "[Kiyoshi_Tashiro]",
          type: "string",
          const: "vnd.fujixerox.docuworks.container",
        },
        {
          description: "[Fumio_Tanabe]",
          type: "string",
          const: "vnd.fujixerox.HBPL",
        },
        {
          description: "[Jann_Pruulman]",
          type: "string",
          const: "vnd.fut-misnet",
        },
        {
          description: "[Andrey_Galkin]",
          type: "string",
          const: "vnd.futoin+cbor",
        },
        {
          description: "[Andrey_Galkin]",
          type: "string",
          const: "vnd.futoin+json",
        },
        {
          description: "[Simon_Birtwistle]",
          type: "string",
          const: "vnd.fuzzysheet",
        },
        {
          description: "[Torben_Frey]",
          type: "string",
          const: "vnd.genomatix.tuxedo",
        },
        {
          description: "[Divon_Lan]",
          type: "string",
          const: "vnd.genozip",
        },
        {
          description: "[Philipp_Gortan]",
          type: "string",
          const: "vnd.gentics.grd+json",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.catmetadata+xml",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.ebuild",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.eclass",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.gpkg",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.manifest",
        },
        {
          description: "[Gentoo_Portage_Project]",
          type: "string",
          const: "vnd.gentoo.xpak",
        },
        {
          description: "[Michał_Górny]",
          type: "string",
          const: "vnd.gentoo.pkgmetadata+xml",
        },
        {
          description: "[Sean_Gillies]",
          type: "string",
          const:
            "vnd.geo+json (OBSOLETED by [RFC7946] in favor of application/geo+json)",
        },
        {
          description: "[Francois_Pirsch]",
          type: "string",
          const: "vnd.geocube+xml (OBSOLETED by request)",
        },
        {
          description: "[GeoGebra][Yves_Kreis]",
          type: "string",
          const: "vnd.geogebra.file",
        },
        {
          description: "[GeoGebra][Michael_Borcherds][Markus_Hohenwarter]",
          type: "string",
          const: "vnd.geogebra.slides",
        },
        {
          description: "[GeoGebra][Yves_Kreis]",
          type: "string",
          const: "vnd.geogebra.tool",
        },
        {
          description: "[Michael_Hvidsten]",
          type: "string",
          const: "vnd.geometry-explorer",
        },
        {
          description: "[Matthias_Ehmann]",
          type: "string",
          const: "vnd.geonext",
        },
        {
          description: "[Christian_Mercat]",
          type: "string",
          const: "vnd.geoplan",
        },
        {
          description: "[Christian_Mercat]",
          type: "string",
          const: "vnd.geospace",
        },
        {
          description: "[Thomas_Weyn]",
          type: "string",
          const: "vnd.gerber",
        },
        {
          description: "[Gil_Bernabeu]",
          type: "string",
          const: "vnd.globalplatform.card-content-mgt",
        },
        {
          description: "[Gil_Bernabeu]",
          type: "string",
          const: "vnd.globalplatform.card-content-mgt-response",
        },
        {
          description: "[Christian_V._Sciberras]",
          type: "string",
          const: "vnd.gmx - DEPRECATED",
        },
        {
          description: "[Christian_Grothoff]",
          type: "string",
          const: "vnd.gnu.taler.exchange+json",
        },
        {
          description: "[Christian_Grothoff]",
          type: "string",
          const: "vnd.gnu.taler.merchant+json",
        },
        {
          description: "[Michael_Ashbridge]",
          type: "string",
          const: "vnd.google-earth.kml+xml",
        },
        {
          description: "[Michael_Ashbridge]",
          type: "string",
          const: "vnd.google-earth.kmz",
        },
        {
          description: "[Peter_Biro][Stefan_Szilva]",
          type: "string",
          const: "vnd.gov.sk.e-form+xml",
        },
        {
          description: "[Peter_Biro][Stefan_Szilva]",
          type: "string",
          const: "vnd.gov.sk.e-form+zip",
        },
        {
          description: "[Peter_Biro][Stefan_Szilva]",
          type: "string",
          const: "vnd.gov.sk.xmldatacontainer+xml",
        },
        {
          description: "[Jeff_Tupper]",
          type: "string",
          const: "vnd.grafeq",
        },
        {
          description: "[Jeff_Lawson]",
          type: "string",
          const: "vnd.gridmp",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-account",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-help",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-identity-message",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-injector",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-tool-message",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-tool-template",
        },
        {
          description: "[Todd_Joseph]",
          type: "string",
          const: "vnd.groove-vcard",
        },
        {
          description: "[Mike_Kelly]",
          type: "string",
          const: "vnd.hal+json",
        },
        {
          description: "[Mike_Kelly]",
          type: "string",
          const: "vnd.hal+xml",
        },
        {
          description: "[Eric_Hamilton]",
          type: "string",
          const: "vnd.HandHeld-Entertainment+xml",
        },
        {
          description: "[Ingo_Hammann]",
          type: "string",
          const: "vnd.hbci",
        },
        {
          description: "[Jan_Schütze]",
          type: "string",
          const: "vnd.hc+json",
        },
        {
          description: "[Doug_R._Serres]",
          type: "string",
          const: "vnd.hcl-bireports",
        },
        {
          description: "[Javier_D._Fernández]",
          type: "string",
          const: "vnd.hdt",
        },
        {
          description: "[Wesley_Beary]",
          type: "string",
          const: "vnd.heroku+json",
        },
        {
          description: "[Randy_Jones]",
          type: "string",
          const: "vnd.hhe.lesson-player",
        },
        {
          description: "[Bob_Pentecost]",
          type: "string",
          const: "vnd.hp-HPGL",
        },
        {
          description: "[Aloke_Gupta]",
          type: "string",
          const: "vnd.hp-hpid",
        },
        {
          description: "[Steve_Aubrey]",
          type: "string",
          const: "vnd.hp-hps",
        },
        {
          description: "[Amir_Gaash]",
          type: "string",
          const: "vnd.hp-jlyt",
        },
        {
          description: "[Bob_Pentecost]",
          type: "string",
          const: "vnd.hp-PCL",
        },
        {
          description: "[Bob_Pentecost]",
          type: "string",
          const: "vnd.hp-PCLXL",
        },
        {
          description: "[Franck_Lefevre]",
          type: "string",
          const: "vnd.httphone",
        },
        {
          description: "[Allen_Gillam]",
          type: "string",
          const: "vnd.hydrostatix.sof-data",
        },
        {
          description: "[Mario_Demuth]",
          type: "string",
          const: "vnd.hyper-item+json",
        },
        {
          description: "[Irakli_Nadareishvili]",
          type: "string",
          const: "vnd.hyper+json",
        },
        {
          description: "[Daniel_Sims]",
          type: "string",
          const: "vnd.hyperdrive+json",
        },
        {
          description: "[James_Minnis]",
          type: "string",
          const: "vnd.hzn-3d-crossword",
        },
        {
          description: "[Roger_Buis]",
          type: "string",
          const:
            "vnd.ibm.afplinedata (OBSOLETED in favor of vnd.afpc.afplinedata)",
        },
        {
          description: "[Bruce_Tantlinger]",
          type: "string",
          const: "vnd.ibm.electronic-media",
        },
        {
          description: "[Amir_Herzberg]",
          type: "string",
          const: "vnd.ibm.MiniPay",
        },
        {
          description: "[Reinhard_Hohensee]",
          type: "string",
          const:
            "vnd.ibm.modcap (OBSOLETED in favor of application/vnd.afpc.modca)",
        },
        {
          description: "[Bruce_Tantlinger]",
          type: "string",
          const: "vnd.ibm.rights-management",
        },
        {
          description: "[Bruce_Tantlinger]",
          type: "string",
          const: "vnd.ibm.secure-container",
        },
        {
          description: "[Phil_Green]",
          type: "string",
          const: "vnd.iccprofile",
        },
        {
          description: "[Purva_R_Rajkotia]",
          type: "string",
          const: "vnd.ieee.1905",
        },
        {
          description: "[Tim_Fisher]",
          type: "string",
          const: "vnd.igloader",
        },
        {
          description: "[Dirk_Farin]",
          type: "string",
          const: "vnd.imagemeter.folder+zip",
        },
        {
          description: "[Dirk_Farin]",
          type: "string",
          const: "vnd.imagemeter.image+zip",
        },
        {
          description: "[Mathieu_Villegas]",
          type: "string",
          const: "vnd.immervision-ivp",
        },
        {
          description: "[Mathieu_Villegas]",
          type: "string",
          const: "vnd.immervision-ivu",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.imsccv1p1",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.imsccv1p2",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.imsccv1p3",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lis.v2.result+json",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lti.v2.toolconsumerprofile+json",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lti.v2.toolproxy.id+json",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lti.v2.toolproxy+json",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lti.v2.toolsettings+json",
        },
        {
          description: "[Lisa_Mattson]",
          type: "string",
          const: "vnd.ims.lti.v2.toolsettings.simple+json",
        },
        {
          description: "[Mark_Wahl]",
          type: "string",
          const: "vnd.informedcontrol.rms+xml",
        },
        {
          description: "[Charles_Engelke]",
          type: "string",
          const: "vnd.infotech.project",
        },
        {
          description: "[Charles_Engelke]",
          type: "string",
          const: "vnd.infotech.project+xml",
        },
        {
          description: "[Christopher_Gales]",
          type: "string",
          const:
            "vnd.informix-visionary (OBSOLETED in favor of application/vnd.visionary)",
        },
        {
          description: "[Takanori_Sudo]",
          type: "string",
          const: "vnd.innopath.wamp.notification",
        },
        {
          description: "[Jon_Swanson]",
          type: "string",
          const: "vnd.insors.igm",
        },
        {
          description: "[Tom_Gurak]",
          type: "string",
          const: "vnd.intercon.formnet",
        },
        {
          description: "[Yves_Kreis_2]",
          type: "string",
          const: "vnd.intergeo",
        },
        {
          description: "[Luke_Tomasello]",
          type: "string",
          const: "vnd.intertrust.digibox",
        },
        {
          description: "[Luke_Tomasello]",
          type: "string",
          const: "vnd.intertrust.nncp",
        },
        {
          description: "[Greg_Scratchley]",
          type: "string",
          const: "vnd.intu.qbo",
        },
        {
          description: "[Greg_Scratchley]",
          type: "string",
          const: "vnd.intu.qfx",
        },
        {
          description: "[Marcin_Rataj]",
          type: "string",
          const: "vnd.ipld.car",
        },
        {
          description: "[Marcin_Rataj]",
          type: "string",
          const: "vnd.ipld.dag-cbor",
        },
        {
          description: "[Marcin_Rataj]",
          type: "string",
          const: "vnd.ipld.dag-json",
        },
        {
          description: "[Marcin_Rataj]",
          type: "string",
          const: "vnd.ipld.raw",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.catalogitem+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.conceptitem+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.knowledgeitem+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.newsitem+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.newsmessage+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.packageitem+xml",
        },
        {
          description: "[Michael_Steidl]",
          type: "string",
          const: "vnd.iptc.g2.planningitem+xml",
        },
        {
          description: "[Per_Ersson]",
          type: "string",
          const: "vnd.ipunplugged.rcprofile",
        },
        {
          description: "[Martin_Knowles]",
          type: "string",
          const: "vnd.irepository.package+xml",
        },
        {
          description: "[Satish_Navarajan]",
          type: "string",
          const: "vnd.is-xpr",
        },
        {
          description: "[Ryan_Brinkman]",
          type: "string",
          const: "vnd.isac.fcs",
        },
        {
          description: "[Brijesh_Kumar]",
          type: "string",
          const: "vnd.jam",
        },
        {
          description: "[Frank_Wiebeler]",
          type: "string",
          const: "vnd.iso11783-10+zip",
        },
        {
          description: "[Kiyofusa_Fujii]",
          type: "string",
          const: "vnd.japannet-directory-service",
        },
        {
          description: "[Jun_Yoshitake]",
          type: "string",
          const: "vnd.japannet-jpnstore-wakeup",
        },
        {
          description: "[Kiyofusa_Fujii]",
          type: "string",
          const: "vnd.japannet-payment-wakeup",
        },
        {
          description: "[Jun_Yoshitake]",
          type: "string",
          const: "vnd.japannet-registration",
        },
        {
          description: "[Kiyofusa_Fujii]",
          type: "string",
          const: "vnd.japannet-registration-wakeup",
        },
        {
          description: "[Jun_Yoshitake]",
          type: "string",
          const: "vnd.japannet-setstore-wakeup",
        },
        {
          description: "[Jun_Yoshitake]",
          type: "string",
          const: "vnd.japannet-verification",
        },
        {
          description: "[Kiyofusa_Fujii]",
          type: "string",
          const: "vnd.japannet-verification-wakeup",
        },
        {
          description: "[Mikhail_Gorshenev]",
          type: "string",
          const: "vnd.jcp.javame.midlet-rms",
        },
        {
          description: "[Sebastiaan_Deckers]",
          type: "string",
          const: "vnd.jisp",
        },
        {
          description: "[Joost]",
          type: "string",
          const: "vnd.joost.joda-archive",
        },
        {
          description: "[Yokoyama_Kiyonobu]",
          type: "string",
          const: "vnd.jsk.isdn-ngn",
        },
        {
          description: "[Tim_Macdonald]",
          type: "string",
          const: "vnd.kahootz",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.karbon",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kchart",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kformula",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kivio",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kontour",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kpresenter",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kspread",
        },
        {
          description: "[David_Faure]",
          type: "string",
          const: "vnd.kde.kword",
        },
        {
          description: "[Dirk_DiGiorgio-Haag]",
          type: "string",
          const: "vnd.kenameaapp",
        },
        {
          description: "[Jack_Bennett]",
          type: "string",
          const: "vnd.kidspiration",
        },
        {
          description: "[Hemant_Thakkar]",
          type: "string",
          const: "vnd.Kinar",
        },
        {
          description: "[Pete_Cole]",
          type: "string",
          const: "vnd.koan",
        },
        {
          description: "[Michael_J._Donahue]",
          type: "string",
          const: "vnd.kodak-descriptor",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "vnd.las",
        },
        {
          description: "[Rob_Bailey]",
          type: "string",
          const: "vnd.las.las+json",
        },
        {
          description: "[Rob_Bailey]",
          type: "string",
          const: "vnd.las.las+xml",
        },
        {
          description: "[NCGIS][Bryan_Blank]",
          type: "string",
          const: "vnd.laszip",
        },
        {
          description: "[Mark_C_Fralick]",
          type: "string",
          const: "vnd.leap+json",
        },
        {
          description: "[Brett_McDowell]",
          type: "string",
          const: "vnd.liberty-request+xml",
        },
        {
          description: "[Catherine_E._White]",
          type: "string",
          const: "vnd.llamagraphics.life-balance.desktop",
        },
        {
          description: "[Catherine_E._White]",
          type: "string",
          const: "vnd.llamagraphics.life-balance.exchange+xml",
        },
        {
          description: "[Victor_Kuchynsky]",
          type: "string",
          const: "vnd.logipipe.circuit+zip",
        },
        {
          description: "[Sten_Linnarsson]",
          type: "string",
          const: "vnd.loom",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-1-2-3",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-approach",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-freelance",
        },
        {
          description: "[Michael_Laramie]",
          type: "string",
          const: "vnd.lotus-notes",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-organizer",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-screencam",
        },
        {
          description: "[Paul_Wattenberger]",
          type: "string",
          const: "vnd.lotus-wordpro",
        },
        {
          description: "[James_Berry]",
          type: "string",
          const: "vnd.macports.portpkg",
        },
        {
          description: "[Blake_Thompson]",
          type: "string",
          const: "vnd.mapbox-vector-tile",
        },
        {
          description: "[Gary_Ellison]",
          type: "string",
          const: "vnd.marlin.drm.actiontoken+xml",
        },
        {
          description: "[Gary_Ellison]",
          type: "string",
          const: "vnd.marlin.drm.conftoken+xml",
        },
        {
          description: "[Gary_Ellison]",
          type: "string",
          const: "vnd.marlin.drm.license+xml",
        },
        {
          description: "[Gary_Ellison]",
          type: "string",
          const: "vnd.marlin.drm.mdcf",
        },
        {
          description: "[Jorn_Wildt]",
          type: "string",
          const: "vnd.mason+json",
        },
        {
          description: "[Erik_Dahlström]",
          type: "string",
          const: "vnd.maxar.archive.3tz+zip",
        },
        {
          description: "[William_Stevenson]",
          type: "string",
          const: "vnd.maxmind.maxmind-db",
        },
        {
          description: "[Tadashi_Gotoh]",
          type: "string",
          const: "vnd.mcd",
        },
        {
          description: "[Frank_Schoonjans]",
          type: "string",
          const: "vnd.medcalcdata",
        },
        {
          description: "[Henry_Flurry]",
          type: "string",
          const: "vnd.mediastation.cdkey",
        },
        {
          description: "[Dominique_Sandoz]",
          type: "string",
          const: "vnd.medicalholodeck.recordxr",
        },
        {
          description: "[Eric_Wedel]",
          type: "string",
          const: "vnd.meridian-slingshot",
        },
        {
          description: "[Masaaki_Hirai]",
          type: "string",
          const: "vnd.MFER",
        },
        {
          description: "[Yukari_Ikeda]",
          type: "string",
          const: "vnd.mfmp",
        },
        {
          description: "[Dali_Zheng]",
          type: "string",
          const: "vnd.micro+json",
        },
        {
          description: "[Joe_Prevo]",
          type: "string",
          const: "vnd.micrografx.flo",
        },
        {
          description: "[Joe_Prevo]",
          type: "string",
          const: "vnd.micrografx.igx",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.microsoft.portable-executable",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.microsoft.windows.thumbnail-cache",
        },
        {
          description: "[Nils_Langhammer]",
          type: "string",
          const: "vnd.miele+json",
        },
        {
          description: "[Mike_Wexler]",
          type: "string",
          const: "vnd.mif",
        },
        {
          description: "[Chris_Bartram]",
          type: "string",
          const: "vnd.minisoft-hp3000-save",
        },
        {
          description: "[Tanaka]",
          type: "string",
          const: "vnd.mitsubishi.misty-guard.trustweb",
        },
        {
          description: "[Allen_K._Kabayama]",
          type: "string",
          const: "vnd.Mobius.DAF",
        },
        {
          description: "[Allen_K._Kabayama]",
          type: "string",
          const: "vnd.Mobius.DIS",
        },
        {
          description: "[Alex_Devasia]",
          type: "string",
          const: "vnd.Mobius.MBK",
        },
        {
          description: "[Alex_Devasia]",
          type: "string",
          const: "vnd.Mobius.MQY",
        },
        {
          description: "[Allen_K._Kabayama]",
          type: "string",
          const: "vnd.Mobius.MSL",
        },
        {
          description: "[Allen_K._Kabayama]",
          type: "string",
          const: "vnd.Mobius.PLC",
        },
        {
          description: "[Allen_K._Kabayama]",
          type: "string",
          const: "vnd.Mobius.TXF",
        },
        {
          description: "[Bjorn_Wennerstrom]",
          type: "string",
          const: "vnd.mophun.application",
        },
        {
          description: "[Bjorn_Wennerstrom]",
          type: "string",
          const: "vnd.mophun.certificate",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.adsi",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.fis",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.gotap",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.kmr",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.ttc",
        },
        {
          description: "[Mark_Patton]",
          type: "string",
          const: "vnd.motorola.flexsuite.wem",
        },
        {
          description: "[Rafie_Shamsaasef]",
          type: "string",
          const: "vnd.motorola.iprm",
        },
        {
          description: "[Braden_N_McDaniel]",
          type: "string",
          const: "vnd.mozilla.xul+xml",
        },
        {
          description: "[Dean_Slawson]",
          type: "string",
          const: "vnd.ms-artgalry",
        },
        {
          description: "[Eric_Fleischman]",
          type: "string",
          const: "vnd.ms-asf",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.ms-cab-compressed",
        },
        {
          description: "[Shawn_Maloney]",
          type: "string",
          const: "vnd.ms-3mfdocument",
        },
        {
          description: "[Sukvinder_S._Gill]",
          type: "string",
          const: "vnd.ms-excel",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-excel.addin.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-excel.sheet.binary.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-excel.sheet.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-excel.template.macroEnabled.12",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.ms-fontobject",
        },
        {
          description: "[Anatoly_Techtonik]",
          type: "string",
          const: "vnd.ms-htmlhelp",
        },
        {
          description: "[Eric_Ledoux]",
          type: "string",
          const: "vnd.ms-ims",
        },
        {
          description: "[Eric_Ledoux]",
          type: "string",
          const: "vnd.ms-lrm",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-office.activeX+xml",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-officetheme",
        },
        {
          description: "[Daniel_Schneider]",
          type: "string",
          const: "vnd.ms-playready.initiator+xml",
        },
        {
          description: "[Sukvinder_S._Gill]",
          type: "string",
          const: "vnd.ms-powerpoint",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-powerpoint.addin.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-powerpoint.presentation.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-powerpoint.slide.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-powerpoint.slideshow.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-powerpoint.template.macroEnabled.12",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-PrintDeviceCapabilities+xml",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-PrintSchemaTicket+xml",
        },
        {
          description: "[Sukvinder_S._Gill]",
          type: "string",
          const: "vnd.ms-project",
        },
        {
          description: "[Sukvinder_S._Gill]",
          type: "string",
          const: "vnd.ms-tnef",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-windows.devicepairing",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-windows.nwprinting.oob",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-windows.printerpairing",
        },
        {
          description: "[Justin_Hutchings]",
          type: "string",
          const: "vnd.ms-windows.wsd.oob",
        },
        {
          description: "[Kevin_Lau]",
          type: "string",
          const: "vnd.ms-wmdrm.lic-chlg-req",
        },
        {
          description: "[Kevin_Lau]",
          type: "string",
          const: "vnd.ms-wmdrm.lic-resp",
        },
        {
          description: "[Kevin_Lau]",
          type: "string",
          const: "vnd.ms-wmdrm.meter-chlg-req",
        },
        {
          description: "[Kevin_Lau]",
          type: "string",
          const: "vnd.ms-wmdrm.meter-resp",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-word.document.macroEnabled.12",
        },
        {
          description: "[Chris_Rae]",
          type: "string",
          const: "vnd.ms-word.template.macroEnabled.12",
        },
        {
          description: "[Sukvinder_S._Gill]",
          type: "string",
          const: "vnd.ms-works",
        },
        {
          description: "[Dan_Plastina]",
          type: "string",
          const: "vnd.ms-wpl",
        },
        {
          description: "[Jesse_McGatha]",
          type: "string",
          const: "vnd.ms-xpsdocument",
        },
        {
          description: "[Thomas_Huth]",
          type: "string",
          const: "vnd.msa-disk-image",
        },
        {
          description: "[Gwenael_Le_Bodic]",
          type: "string",
          const: "vnd.mseq",
        },
        {
          description: "[Malte_Borcherding]",
          type: "string",
          const: "vnd.msign",
        },
        {
          description: "[Steve_Mills]",
          type: "string",
          const: "vnd.multiad.creator",
        },
        {
          description: "[Steve_Mills]",
          type: "string",
          const: "vnd.multiad.creator.cif",
        },
        {
          description: "[Greg_Adams]",
          type: "string",
          const: "vnd.musician",
        },
        {
          description: "[Tim_Butler]",
          type: "string",
          const: "vnd.music-niff",
        },
        {
          description: "[Chandrashekhara_Anantharamu]",
          type: "string",
          const: "vnd.muvee.style",
        },
        {
          description: "[Franck_Lefevre]",
          type: "string",
          const: "vnd.mynfc",
        },
        {
          description: "[Sebastian_A._Weiss]",
          type: "string",
          const: "vnd.nacamar.ybrid+json",
        },
        {
          description: "[Lauri_Tarkkala]",
          type: "string",
          const: "vnd.ncd.control",
        },
        {
          description: "[Lauri_Tarkkala]",
          type: "string",
          const: "vnd.ncd.reference",
        },
        {
          description: "[Thomas_Schoffelen]",
          type: "string",
          const: "vnd.nearst.inv+json",
        },
        {
          description: "[Andreas_Molzer]",
          type: "string",
          const: "vnd.nebumind.line",
        },
        {
          description: "[Steve_Judkins]",
          type: "string",
          const: "vnd.nervana",
        },
        {
          description: "[Andy_Mutz]",
          type: "string",
          const: "vnd.netfpx",
        },
        {
          description: "[Dan_DuFeu]",
          type: "string",
          const: "vnd.neurolanguage.nlu",
        },
        {
          description: "[Amit_Kumar_Gupta]",
          type: "string",
          const: "vnd.nimn",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.nintendo.snes.rom",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.nintendo.nitro.rom",
        },
        {
          description: "[Steve_Rogan]",
          type: "string",
          const: "vnd.nitf",
        },
        {
          description: "[Monty_Solomon]",
          type: "string",
          const: "vnd.noblenet-directory",
        },
        {
          description: "[Monty_Solomon]",
          type: "string",
          const: "vnd.noblenet-sealer",
        },
        {
          description: "[Monty_Solomon]",
          type: "string",
          const: "vnd.noblenet-web",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.catalogs",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.conml+wbxml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.conml+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.iptv.config+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.iSDS-radio-presets",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.landmark+wbxml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.landmark+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.landmarkcollection+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.ncd",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.n-gage.ac+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.n-gage.data",
        },
        {
          description: "[Nokia]",
          type: "string",
          const:
            "vnd.nokia.n-gage.symbian.install (OBSOLETE; no replacement given)",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.pcd+wbxml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.pcd+xml",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.radio-preset",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.radio-presets",
        },
        {
          description: "[Janine_Swenson]",
          type: "string",
          const: "vnd.novadigm.EDM",
        },
        {
          description: "[Janine_Swenson]",
          type: "string",
          const: "vnd.novadigm.EDX",
        },
        {
          description: "[Janine_Swenson]",
          type: "string",
          const: "vnd.novadigm.EXT",
        },
        {
          description: "[Akinori_Taya]",
          type: "string",
          const: "vnd.ntt-local.content-share",
        },
        {
          description: "[NTT-local]",
          type: "string",
          const: "vnd.ntt-local.file-transfer",
        },
        {
          description: "[NTT-local]",
          type: "string",
          const: "vnd.ntt-local.ogw_remote-access",
        },
        {
          description: "[NTT-local]",
          type: "string",
          const: "vnd.ntt-local.sip-ta_remote",
        },
        {
          description: "[NTT-local]",
          type: "string",
          const: "vnd.ntt-local.sip-ta_tcp_stream",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.base",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.chart",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.chart-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const:
            "vnd.oasis.opendocument.database (OBSOLETED in favor of application/vnd.oasis.opendocument.base)",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.formula",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.formula-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.graphics",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.graphics-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.image",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.image-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.presentation",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.presentation-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.spreadsheet",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.spreadsheet-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.text",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.text-master",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.text-template",
        },
        {
          description: "[OASIS_TC_Admin][OASIS]",
          type: "string",
          const: "vnd.oasis.opendocument.text-web",
        },
        {
          description: "[Matthias_Hessling]",
          type: "string",
          const: "vnd.obn",
        },
        {
          description: "[Michael_Koster]",
          type: "string",
          const: "vnd.ocf+cbor",
        },
        {
          description: "[Steven_Lasker]",
          type: "string",
          const: "vnd.oci.image.manifest.v1+json",
        },
        {
          description: "[Eli_Grey]",
          type: "string",
          const: "vnd.oftn.l10n+json",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.contentaccessdownload+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.contentaccessstreaming+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.cspg-hexbinary",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.dae.svg+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.dae.xhtml+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.mippvcontrolmessage+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.pae.gem",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.spdiscovery+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.spdlist+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.ueprofile+xml",
        },
        {
          description: "[Claire_DEsclercs]",
          type: "string",
          const: "vnd.oipf.userprofile+xml",
        },
        {
          description: "[John_Palmieri]",
          type: "string",
          const: "vnd.olpc-sugar",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.associated-procedure-parameter+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.drm-trigger+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.imd+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.ltkm",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.notification+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.provisioningtrigger",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.sgboot",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.sgdd+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.sgdu",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.simple-symbol-container",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.smartcard-trigger+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.sprov+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.bcast.stkm",
        },
        {
          description: "[Hao_Wang][OMA]",
          type: "string",
          const: "vnd.oma.cab-address-book+xml",
        },
        {
          description: "[Hao_Wang][OMA]",
          type: "string",
          const: "vnd.oma.cab-feature-handler+xml",
        },
        {
          description: "[Hao_Wang][OMA]",
          type: "string",
          const: "vnd.oma.cab-pcc+xml",
        },
        {
          description: "[Hao_Wang][OMA]",
          type: "string",
          const: "vnd.oma.cab-subs-invite+xml",
        },
        {
          description: "[Hao_Wang][OMA]",
          type: "string",
          const: "vnd.oma.cab-user-prefs+xml",
        },
        {
          description: "[Avi_Primo][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.dcd",
        },
        {
          description: "[Avi_Primo][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.dcdc",
        },
        {
          description:
            "[Jun_Sato][Open_Mobile_Alliance_BAC_DLDRM_Working_Group]",
          type: "string",
          const: "vnd.oma.dd2+xml",
        },
        {
          description: "[Uwe_Rauschenbach][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.drm.risd+xml",
        },
        {
          description:
            "[Sean_Kelley][OMA_Presence_and_Availability_PAG_Working_Group]",
          type: "string",
          const: "vnd.oma.group-usage-list+xml",
        },
        {
          description: "[Open_Mobile_Naming_Authority][John_Mudge]",
          type: "string",
          const: "vnd.oma.lwm2m+cbor",
        },
        {
          description: "[Open_Mobile_Naming_Authority][John_Mudge]",
          type: "string",
          const: "vnd.oma.lwm2m+json",
        },
        {
          description: "[Open_Mobile_Naming_Authority][John_Mudge]",
          type: "string",
          const: "vnd.oma.lwm2m+tlv",
        },
        {
          description: "[Brian_McColgan][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.pal+xml",
        },
        {
          description: "[OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.oma.poc.detailed-progress-report+xml",
        },
        {
          description: "[OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.oma.poc.final-report+xml",
        },
        {
          description:
            "[Sean_Kelley][OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.oma.poc.groups+xml",
        },
        {
          description: "[OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.oma.poc.invocation-descriptor+xml",
        },
        {
          description: "[OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.oma.poc.optimized-progress-report+xml",
        },
        {
          description: "[Bryan_Sullivan][OMA]",
          type: "string",
          const: "vnd.oma.push",
        },
        {
          description: "[Wenjun_Zeng][Open_Mobile_Naming_Authority]",
          type: "string",
          const: "vnd.oma.scidm.messages+xml",
        },
        {
          description:
            "[Sean_Kelley][OMA_Presence_and_Availability_PAG_Working_Group]",
          type: "string",
          const: "vnd.oma.xcap-directory+xml",
        },
        {
          description: "[OMA_Data_Synchronization_Working_Group]",
          type: "string",
          const: "vnd.omads-email+xml",
        },
        {
          description: "[OMA_Data_Synchronization_Working_Group]",
          type: "string",
          const: "vnd.omads-file+xml",
        },
        {
          description: "[OMA_Data_Synchronization_Working_Group]",
          type: "string",
          const: "vnd.omads-folder+xml",
        },
        {
          description: "[Julien_Grange]",
          type: "string",
          const: "vnd.omaloc-supl-init",
        },
        {
          description: "[Ilan_Mahalal]",
          type: "string",
          const: "vnd.oma-scws-config",
        },
        {
          description: "[Ilan_Mahalal]",
          type: "string",
          const: "vnd.oma-scws-http-request",
        },
        {
          description: "[Ilan_Mahalal]",
          type: "string",
          const: "vnd.oma-scws-http-response",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepager",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepagertamp",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepagertamx",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepagertat",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepagertatp",
        },
        {
          description: "[Nathan_Black]",
          type: "string",
          const: "vnd.onepagertatx",
        },
        {
          description: "[Hans_Busch]",
          type: "string",
          const: "vnd.onvif.metadata",
        },
        {
          description: "[Mark_Otaris]",
          type: "string",
          const: "vnd.openblox.game-binary",
        },
        {
          description: "[Mark_Otaris]",
          type: "string",
          const: "vnd.openblox.game+xml",
        },
        {
          description: "[Craig_Bruce]",
          type: "string",
          const: "vnd.openeye.oeb",
        },
        {
          description: "[Paul_Norman]",
          type: "string",
          const: "vnd.openstreetmap.data+xml",
        },
        {
          description: "[Peter_Todd]",
          type: "string",
          const: "vnd.opentimestamps.ots",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.custom-properties+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.customXmlProperties+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.drawing+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.drawingml.chart+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.drawingml.chartshapes+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.drawingml.diagramColors+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.drawingml.diagramData+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.drawingml.diagramLayout+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.drawingml.diagramStyle+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.extended-properties+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.commentAuthors+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.comments+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.handoutMaster+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.notesMaster+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.notesSlide+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.presentation",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.presentation.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.presProps+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.presentationml.slide",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.presentationml.slide+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.slideMaster+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.presentationml.slideshow",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.slideUpdateInfo+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.tableStyles+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.presentationml.tags+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.presentationml.template",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.template.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.presentationml.viewProps+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.spreadsheetml.comments+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.connections+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.dialogsheet+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.externalLink+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheDefinition+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheRecords+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.pivotTable+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.queryTable+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.revisionHeaders+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.revisionLog+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.sheetMetadata+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.spreadsheetml.styles+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.spreadsheetml.table+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.tableSingleCells+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.spreadsheetml.template",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.userNames+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.volatileDependencies+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.theme+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.themeOverride+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.vmlDrawing",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.comments+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.wordprocessingml.document",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.footer+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.settings+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.styles+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-officedocument.wordprocessingml.template",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-package.core-properties+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const:
            "vnd.openxmlformats-package.digital-signature-xmlsignature+xml",
        },
        {
          description: "[Makoto_Murata]",
          type: "string",
          const: "vnd.openxmlformats-package.relationships+xml",
        },
        {
          description: "[Ning_Dong]",
          type: "string",
          const: "vnd.oracle.resource+json",
        },
        {
          description: "[CHATRAS_Bruno]",
          type: "string",
          const: "vnd.orange.indata",
        },
        {
          description: "[Steven_Klos]",
          type: "string",
          const: "vnd.osa.netdeploy",
        },
        {
          description: "[Jason_Birch]",
          type: "string",
          const: "vnd.osgeo.mapguide.package",
        },
        {
          description: "[Peter_Kriens]",
          type: "string",
          const: "vnd.osgi.bundle",
        },
        {
          description: "[Peter_Kriens]",
          type: "string",
          const: "vnd.osgi.dp",
        },
        {
          description: "[Peter_Kriens]",
          type: "string",
          const: "vnd.osgi.subsystem",
        },
        {
          description: "[Magnus_Nystrom]",
          type: "string",
          const: "vnd.otps.ct-kip+xml",
        },
        {
          description: "[C._Titus_Brown]",
          type: "string",
          const: "vnd.oxli.countgraph",
        },
        {
          description: "[Steve_Rice]",
          type: "string",
          const: "vnd.pagerduty+json",
        },
        {
          description: "[Gavin_Peacock]",
          type: "string",
          const: "vnd.palm",
        },
        {
          description: "[Natarajan_Balasundara]",
          type: "string",
          const: "vnd.panoply",
        },
        {
          description: "[John_Kemp]",
          type: "string",
          const: "vnd.paos.xml",
        },
        {
          description: "[Christian_Trosclair]",
          type: "string",
          const: "vnd.patentdive",
        },
        {
          description: "[Andrew_David_Kendall]",
          type: "string",
          const: "vnd.patientecommsdoc",
        },
        {
          description: "[Prakash_Baskaran]",
          type: "string",
          const: "vnd.pawaafile",
        },
        {
          description: "[Slawomir_Lisznianski]",
          type: "string",
          const: "vnd.pcos",
        },
        {
          description: "[April_Gandert]",
          type: "string",
          const: "vnd.pg.format",
        },
        {
          description: "[April_Gandert]",
          type: "string",
          const: "vnd.pg.osasli",
        },
        {
          description: "[Lucas_Maneos]",
          type: "string",
          const: "vnd.piaccess.application-licence",
        },
        {
          description: "[Giuseppe_Naccarato]",
          type: "string",
          const: "vnd.picsel",
        },
        {
          description: "[Rhys_Lewis]",
          type: "string",
          const: "vnd.pmi.widget",
        },
        {
          description:
            "[Sean_Kelley][OMA_Push_to_Talk_over_Cellular_POC_Working_Group]",
          type: "string",
          const: "vnd.poc.group-advertisement+xml",
        },
        {
          description: "[Jorge_Pando]",
          type: "string",
          const: "vnd.pocketlearn",
        },
        {
          description: "[David_Guy]",
          type: "string",
          const: "vnd.powerbuilder6",
        },
        {
          description: "[David_Guy]",
          type: "string",
          const: "vnd.powerbuilder6-s",
        },
        {
          description: "[Reed_Shilts]",
          type: "string",
          const: "vnd.powerbuilder7",
        },
        {
          description: "[Reed_Shilts]",
          type: "string",
          const: "vnd.powerbuilder75",
        },
        {
          description: "[Reed_Shilts]",
          type: "string",
          const: "vnd.powerbuilder75-s",
        },
        {
          description: "[Reed_Shilts]",
          type: "string",
          const: "vnd.powerbuilder7-s",
        },
        {
          description: "[Juoko_Tenhunen]",
          type: "string",
          const: "vnd.preminet",
        },
        {
          description: "[Roman_Smolgovsky]",
          type: "string",
          const: "vnd.previewsystems.box",
        },
        {
          description: "[Pete_Hoch]",
          type: "string",
          const: "vnd.proteus.magazine",
        },
        {
          description: "[Kristopher_Durski]",
          type: "string",
          const: "vnd.psfs",
        },
        {
          description: "[Oren_Ben-Kiki]",
          type: "string",
          const: "vnd.publishare-delta-tree",
        },
        {
          description: "[Charles_P._Lamb]",
          type: "string",
          const: "vnd.pvi.ptid1",
        },
        {
          description: "[RFC3391]",
          type: "string",
          const: "vnd.pwg-multiplexed",
        },
        {
          description: "[Don_Wright]",
          type: "string",
          const: "vnd.pwg-xhtml-print+xml",
        },
        {
          description: "[Glenn_Forrester]",
          type: "string",
          const: "vnd.qualcomm.brew-app-res",
        },
        {
          description: "[Casper_Joost_Eyckelhof]",
          type: "string",
          const: "vnd.quarantainenet",
        },
        {
          description: "[Hannes_Scheidler]",
          type: "string",
          const: "vnd.Quark.QuarkXPress",
        },
        {
          description: "[Matthias_Ludwig]",
          type: "string",
          const: "vnd.quobject-quoxdocument",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.moml+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-audit-conf+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-audit-conn+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-audit-dialog+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-audit-stream+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-audit+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-conf+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-base+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-fax-detect+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-fax-sendrecv+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-group+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-speech+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog-transform+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml-dialog+xml",
        },
        {
          description: "[RFC5707]",
          type: "string",
          const: "vnd.radisys.msml+xml",
        },
        {
          description: "[Kevin_Crook]",
          type: "string",
          const: "vnd.rainstor.data",
        },
        {
          description: "[Etay_Szekely]",
          type: "string",
          const: "vnd.rapid",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.rar",
        },
        {
          description: "[Nick_Reeves]",
          type: "string",
          const: "vnd.realvnc.bed",
        },
        {
          description: "[W3C_Music_Notation_Community_Group]",
          type: "string",
          const: "vnd.recordare.musicxml",
        },
        {
          description: "[W3C_Music_Notation_Community_Group]",
          type: "string",
          const: "vnd.recordare.musicxml+xml",
        },
        {
          description: "[James_Wick]",
          type: "string",
          const: "vnd.RenLearn.rlprint",
        },
        {
          description: "[Benedikt_Muessig]",
          type: "string",
          const: "vnd.resilient.logic",
        },
        {
          description: "[Stephen_Mizell]",
          type: "string",
          const: "vnd.restful+json",
        },
        {
          description: "[Ken_Jibiki]",
          type: "string",
          const: "vnd.rig.cryptonote",
        },
        {
          description: "[Sybren_Kikstra]",
          type: "string",
          const: "vnd.route66.link66+xml",
        },
        {
          description: "[Lee_Harding]",
          type: "string",
          const: "vnd.rs-274x",
        },
        {
          description: "[Jerry_Harris]",
          type: "string",
          const: "vnd.ruckus.download",
        },
        {
          description: "[Lauri_Tarkkala]",
          type: "string",
          const: "vnd.s3sms",
        },
        {
          description: "[Heikki_Vesalainen]",
          type: "string",
          const: "vnd.sailingtracker.track",
        },
        {
          description: "[Markus_Strehle]",
          type: "string",
          const: "vnd.sar",
        },
        {
          description: "[Shinji_Kusakari]",
          type: "string",
          const: "vnd.sbm.cid",
        },
        {
          description: "[Masanori_Murai]",
          type: "string",
          const: "vnd.sbm.mid2",
        },
        {
          description: "[Craig_Bradney]",
          type: "string",
          const: "vnd.scribus",
        },
        {
          description: "[John_Kwan]",
          type: "string",
          const: "vnd.sealed.3df",
        },
        {
          description: "[John_Kwan]",
          type: "string",
          const: "vnd.sealed.csf",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.doc",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.eml",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.mht",
        },
        {
          description: "[Martin_Lambert]",
          type: "string",
          const: "vnd.sealed.net",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.ppt",
        },
        {
          description: "[John_Kwan][Martin_Lambert]",
          type: "string",
          const: "vnd.sealed.tiff",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealed.xls",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.html",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.pdf",
        },
        {
          description: "[Steve_Webb]",
          type: "string",
          const: "vnd.seemail",
        },
        {
          description: "[ICT_Manager]",
          type: "string",
          const: "vnd.seis+json",
        },
        {
          description: "[Anders_Hansson]",
          type: "string",
          const: "vnd.sema",
        },
        {
          description: "[Anders_Hansson]",
          type: "string",
          const: "vnd.semd",
        },
        {
          description: "[Anders_Hansson]",
          type: "string",
          const: "vnd.semf",
        },
        {
          description: "[Connor_Horman]",
          type: "string",
          const: "vnd.shade-save-file",
        },
        {
          description: "[Guy_Selzler]",
          type: "string",
          const: "vnd.shana.informed.formdata",
        },
        {
          description: "[Guy_Selzler]",
          type: "string",
          const: "vnd.shana.informed.formtemplate",
        },
        {
          description: "[Guy_Selzler]",
          type: "string",
          const: "vnd.shana.informed.interchange",
        },
        {
          description: "[Guy_Selzler]",
          type: "string",
          const: "vnd.shana.informed.package",
        },
        {
          description: "[Ben_Ramsey]",
          type: "string",
          const: "vnd.shootproof+json",
        },
        {
          description: "[Ronald_Jacobs]",
          type: "string",
          const: "vnd.shopkick+json",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.shp",
        },
        {
          description: "[Mi_Tar]",
          type: "string",
          const: "vnd.shx",
        },
        {
          description: "[Uwe_Hermann]",
          type: "string",
          const: "vnd.sigrok.session",
        },
        {
          description: "[Patrick_Koh]",
          type: "string",
          const: "vnd.SimTech-MindMapper",
        },
        {
          description: "[Kevin_Swiber]",
          type: "string",
          const: "vnd.siren+json",
        },
        {
          description: "[Hiroaki_Takahashi]",
          type: "string",
          const: "vnd.smaf",
        },
        {
          description: "[Jonathan_Neitz]",
          type: "string",
          const: "vnd.smart.notebook",
        },
        {
          description: "[Michael_Boyle]",
          type: "string",
          const: "vnd.smart.teacher",
        },
        {
          description: "[Connor_Horman]",
          type: "string",
          const: "vnd.snesdev-page-table",
        },
        {
          description: "[Jakub_Hytka][Martin_Vondrous]",
          type: "string",
          const: "vnd.software602.filler.form+xml",
        },
        {
          description: "[Jakub_Hytka][Martin_Vondrous]",
          type: "string",
          const: "vnd.software602.filler.form-xml-zip",
        },
        {
          description: "[Cliff_Gauntlett]",
          type: "string",
          const: "vnd.solent.sdkm+xml",
        },
        {
          description: "[Stefan_Jernberg]",
          type: "string",
          const: "vnd.spotfire.dxp",
        },
        {
          description: "[Stefan_Jernberg]",
          type: "string",
          const: "vnd.spotfire.sfs",
        },
        {
          description: "[Clemens_Ladisch]",
          type: "string",
          const: "vnd.sqlite3",
        },
        {
          description: "[Asang_Dani]",
          type: "string",
          const: "vnd.sss-cod",
        },
        {
          description: "[Eric_Bruno]",
          type: "string",
          const: "vnd.sss-dtf",
        },
        {
          description: "[Eric_Bruno]",
          type: "string",
          const: "vnd.sss-ntf",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.stepmania.package",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.stepmania.stepchart",
        },
        {
          description: "[Glenn_Levitt]",
          type: "string",
          const: "vnd.street-stream",
        },
        {
          description: "[Marc_Hadley]",
          type: "string",
          const: "vnd.sun.wadl+xml",
        },
        {
          description: "[Jonathan_Niedfeldt]",
          type: "string",
          const: "vnd.sus-calendar",
        },
        {
          description: "[Scott_Becker]",
          type: "string",
          const: "vnd.svd",
        },
        {
          description: "[Glenn_Widener]",
          type: "string",
          const: "vnd.swiftview-ics",
        },
        {
          description: "[Finn_Rayk_Gärtner]",
          type: "string",
          const: "vnd.sybyl.mol2",
        },
        {
          description: "[Johann_Terblanche]",
          type: "string",
          const: "vnd.sycle+xml",
        },
        {
          description: "[Dan_Luhring]",
          type: "string",
          const: "vnd.syft+json",
        },
        {
          description: "[Peter_Thompson][OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dm.notification",
        },
        {
          description: "[OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dmddf+xml",
        },
        {
          description: "[OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dmtnds+wbxml",
        },
        {
          description: "[OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dmtnds+xml",
        },
        {
          description: "[OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dmddf+wbxml",
        },
        {
          description: "[OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dm+wbxml",
        },
        {
          description: "[Bindu_Rama_Rao][OMA-DM_Work_Group]",
          type: "string",
          const: "vnd.syncml.dm+xml",
        },
        {
          description: "[OMA_Data_Synchronization_Working_Group]",
          type: "string",
          const: "vnd.syncml.ds.notification",
        },
        {
          description: "[OMA_Data_Synchronization_Working_Group]",
          type: "string",
          const: "vnd.syncml+xml",
        },
        {
          description: "[Paul_Walsh]",
          type: "string",
          const: "vnd.tableschema+json",
        },
        {
          description: "[Daniel_Shelton]",
          type: "string",
          const: "vnd.tao.intent-module-archive",
        },
        {
          description: "[Guy_Harris][Glen_Turner]",
          type: "string",
          const: "vnd.tcpdump.pcap",
        },
        {
          description: "[Arno_Schoedl]",
          type: "string",
          const: "vnd.think-cell.ppttc+json",
        },
        {
          description: "[Joey_Smith]",
          type: "string",
          const: "vnd.tml",
        },
        {
          description: "[Alex_Sibilev]",
          type: "string",
          const: "vnd.tmd.mediaflex.api+xml",
        },
        {
          description: "[Nicolas_Helin]",
          type: "string",
          const: "vnd.tmobile-livetv",
        },
        {
          description: "[Rick_Rupp]",
          type: "string",
          const: "vnd.tri.onesource",
        },
        {
          description: "[Frank_Cusack]",
          type: "string",
          const: "vnd.trid.tpt",
        },
        {
          description: "[Steven_Simonoff]",
          type: "string",
          const: "vnd.triscape.mxs",
        },
        {
          description: "[J._Scott_Hepler]",
          type: "string",
          const: "vnd.trueapp",
        },
        {
          description: "[Brad_Chase]",
          type: "string",
          const: "vnd.truedoc",
        },
        {
          description: "[Martin_Talbot]",
          type: "string",
          const: "vnd.ubisoft.webplayer",
        },
        {
          description: "[Dave_Manning]",
          type: "string",
          const: "vnd.ufdl",
        },
        {
          description: "[Tim_Ocock]",
          type: "string",
          const: "vnd.uiq.theme",
        },
        {
          description: "[Jamie_Riden]",
          type: "string",
          const: "vnd.umajin",
        },
        {
          description: "[Unity3d]",
          type: "string",
          const: "vnd.unity",
        },
        {
          description: "[Arne_Gerdes]",
          type: "string",
          const: "vnd.uoml+xml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.alert",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.alert-wbxml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.bearer-choice",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.bearer-choice-wbxml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.cacheop",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.cacheop-wbxml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.channel",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.channel-wbxml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.list",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.listcmd",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.listcmd-wbxml",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.list-wbxml",
        },
        {
          description: "[Sebastian_Baer]",
          type: "string",
          const: "vnd.uri-map",
        },
        {
          description: "[Bruce_Martin]",
          type: "string",
          const: "vnd.uplanet.signal",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.valve.source.material",
        },
        {
          description: "[Taisuke_Sugimoto]",
          type: "string",
          const: "vnd.vcx",
        },
        {
          description: "[Luc_Rogge]",
          type: "string",
          const: "vnd.vd-study",
        },
        {
          description: "[Lyndsey_Ferguson][Biplab_Sarkar]",
          type: "string",
          const: "vnd.vectorworks",
        },
        {
          description: "[James_Wigger]",
          type: "string",
          const: "vnd.vel+json",
        },
        {
          description: "[Petr_Peterka]",
          type: "string",
          const: "vnd.verimatrix.vcas",
        },
        {
          description: "[Al_Brown]",
          type: "string",
          const: "vnd.veritone.aion+json",
        },
        {
          description: "[Massimo_Bertoli]",
          type: "string",
          const: "vnd.veryant.thin",
        },
        {
          description: "[Jim_Zubov]",
          type: "string",
          const: "vnd.ves.encrypted",
        },
        {
          description: "[Robert_Hess]",
          type: "string",
          const: "vnd.vidsoft.vidconference",
        },
        {
          description: "[Troy_Sandal]",
          type: "string",
          const: "vnd.visio",
        },
        {
          description: "[Gayatri_Aravindakumar]",
          type: "string",
          const: "vnd.visionary",
        },
        {
          description: "[Mark_Risher]",
          type: "string",
          const: "vnd.vividence.scriptfile",
        },
        {
          description: "[Delton_Rowe]",
          type: "string",
          const: "vnd.vsf",
        },
        {
          description: "[WAP-Forum]",
          type: "string",
          const: "vnd.wap.sic",
        },
        {
          description: "[WAP-Forum]",
          type: "string",
          const: "vnd.wap.slc",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wbxml",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wmlc",
        },
        {
          description: "[Peter_Stark]",
          type: "string",
          const: "vnd.wap.wmlscriptc",
        },
        {
          description: "[Fawad_Shaikh]",
          type: "string",
          const: "vnd.wasmflow.wafl",
        },
        {
          description: "[Yaser_Rehem]",
          type: "string",
          const: "vnd.webturbo",
        },
        {
          description: "[Wi-Fi_Alliance][Dr._Jun_Tian]",
          type: "string",
          const: "vnd.wfa.dpp",
        },
        {
          description: "[Mick_Conley]",
          type: "string",
          const: "vnd.wfa.p2p",
        },
        {
          description: "[Wi-Fi_Alliance]",
          type: "string",
          const: "vnd.wfa.wsc",
        },
        {
          description: "[Priya_Dandawate]",
          type: "string",
          const: "vnd.windows.devicepairing",
        },
        {
          description: "[Thomas_Kjornes]",
          type: "string",
          const: "vnd.wmc",
        },
        {
          description: "[Thinh_Nguyenphu][Prakash_Iyer]",
          type: "string",
          const: "vnd.wmf.bootstrap",
        },
        {
          description: "[Wolfram]",
          type: "string",
          const: "vnd.wolfram.mathematica",
        },
        {
          description: "[Wolfram]",
          type: "string",
          const: "vnd.wolfram.mathematica.package",
        },
        {
          description: "[Wolfram]",
          type: "string",
          const: "vnd.wolfram.player",
        },
        {
          description: "[David_Riccitelli]",
          type: "string",
          const: "vnd.wordlift",
        },
        {
          description: "[Kim_Scarborough]",
          type: "string",
          const: "vnd.wordperfect",
        },
        {
          description: "[Jan_Bostrom]",
          type: "string",
          const: "vnd.wqd",
        },
        {
          description: "[Chris_Bartram]",
          type: "string",
          const: "vnd.wrq-hp3000-labelled",
        },
        {
          description: "[Bill_Wohler]",
          type: "string",
          const: "vnd.wt.stf",
        },
        {
          description: "[John_Ingi_Ingimundarson]",
          type: "string",
          const: "vnd.wv.csp+xml",
        },
        {
          description: "[Matti_Salmi]",
          type: "string",
          const: "vnd.wv.csp+wbxml",
        },
        {
          description: "[John_Ingi_Ingimundarson]",
          type: "string",
          const: "vnd.wv.ssp+xml",
        },
        {
          description: "[David_Brossard]",
          type: "string",
          const: "vnd.xacml+json",
        },
        {
          description: "[David_Matthewman]",
          type: "string",
          const: "vnd.xara",
        },
        {
          description: "[Dave_Manning]",
          type: "string",
          const: "vnd.xfdl",
        },
        {
          description: "[Michael_Mansell]",
          type: "string",
          const: "vnd.xfdl.webform",
        },
        {
          description: "[Fred_Waskiewicz]",
          type: "string",
          const: "vnd.xmi+xml",
        },
        {
          description: "[Reuven_Sherwin]",
          type: "string",
          const: "vnd.xmpie.cpkg",
        },
        {
          description: "[Reuven_Sherwin]",
          type: "string",
          const: "vnd.xmpie.dpkg",
        },
        {
          description: "[Reuven_Sherwin]",
          type: "string",
          const: "vnd.xmpie.plan",
        },
        {
          description: "[Reuven_Sherwin]",
          type: "string",
          const: "vnd.xmpie.ppkg",
        },
        {
          description: "[Reuven_Sherwin]",
          type: "string",
          const: "vnd.xmpie.xlim",
        },
        {
          description: "[Tomohiro_Yamamoto]",
          type: "string",
          const: "vnd.yamaha.hv-dic",
        },
        {
          description: "[Tomohiro_Yamamoto]",
          type: "string",
          const: "vnd.yamaha.hv-script",
        },
        {
          description: "[Tomohiro_Yamamoto]",
          type: "string",
          const: "vnd.yamaha.hv-voice",
        },
        {
          description: "[Mark_Olleson]",
          type: "string",
          const: "vnd.yamaha.openscoreformat.osfpvg+xml",
        },
        {
          description: "[Mark_Olleson]",
          type: "string",
          const: "vnd.yamaha.openscoreformat",
        },
        {
          description: "[Takehiro_Sukizaki]",
          type: "string",
          const: "vnd.yamaha.remote-setup",
        },
        {
          description: "[Keiichi_Shinoda]",
          type: "string",
          const: "vnd.yamaha.smaf-audio",
        },
        {
          description: "[Keiichi_Shinoda]",
          type: "string",
          const: "vnd.yamaha.smaf-phrase",
        },
        {
          description: "[Takehiro_Sukizaki]",
          type: "string",
          const: "vnd.yamaha.through-ngn",
        },
        {
          description: "[Takehiro_Sukizaki]",
          type: "string",
          const: "vnd.yamaha.tunnel-udpencap",
        },
        {
          description: "[Jens_Jorgensen]",
          type: "string",
          const: "vnd.yaoweme",
        },
        {
          description: "[Mr._Yellow]",
          type: "string",
          const: "vnd.yellowriver-custom-menu",
        },
        {
          description: "[Laura_Wood]",
          type: "string",
          const: "vnd.youtube.yt (OBSOLETED in favor of video/vnd.youtube.yt)",
        },
        {
          description: "[Rene_Grothmann]",
          type: "string",
          const: "vnd.zul",
        },
        {
          description: "[Micheal_Hewett]",
          type: "string",
          const: "vnd.zzazz.deck+xml",
        },
        {
          description: "[RFC4267]",
          type: "string",
          const: "voicexml+xml",
        },
        {
          description: "[RFC8366]",
          type: "string",
          const: "voucher-cms+json",
        },
        {
          description: "[RFC6035]",
          type: "string",
          const: "vq-rtcpxr",
        },
        {
          description: "[W3C][Eric_Prudhommeaux]",
          type: "string",
          const: "wasm",
        },
        {
          description: "[RFC3858]",
          type: "string",
          const: "watcherinfo+xml",
        },
        {
          description: "[RFC8292]",
          type: "string",
          const: "webpush-options+json",
        },
        {
          description: "[RFC2957]",
          type: "string",
          const: "whoispp-query",
        },
        {
          description: "[RFC2958]",
          type: "string",
          const: "whoispp-response",
        },
        {
          description: "[W3C][Steven_Pemberton][W3C-Widgets-2012]",
          type: "string",
          const: "widget",
        },
        {
          description: "[Larry_Campbell]",
          type: "string",
          const: "wita",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "wordperfect5.1",
        },
        {
          description: "[W3C]",
          type: "string",
          const: "wsdl+xml",
        },
        {
          description: "[W3C]",
          type: "string",
          const: "wspolicy+xml",
        },
        {
          description: "[RFC8894]",
          type: "string",
          const: "x-pki-message",
        },
        {
          description: "[WHATWG][Anne_van_Kesteren]",
          type: "string",
          const: "x-www-form-urlencoded",
        },
        {
          description: "[RFC8894]",
          type: "string",
          const: "x-x509-ca-cert",
        },
        {
          description: "[RFC8894]",
          type: "string",
          const: "x-x509-ca-ra-cert",
        },
        {
          description: "[RFC8894]",
          type: "string",
          const: "x-x509-next-ca-cert",
        },
        {
          description: "[RFC1494]",
          type: "string",
          const: "x400-bp",
        },
        {
          description: "[RFC7061]",
          type: "string",
          const: "xacml+xml",
        },
        {
          description: "[RFC4825]",
          type: "string",
          const: "xcap-att+xml",
        },
        {
          description: "[RFC4825]",
          type: "string",
          const: "xcap-caps+xml",
        },
        {
          description: "[RFC5874]",
          type: "string",
          const: "xcap-diff+xml",
        },
        {
          description: "[RFC4825]",
          type: "string",
          const: "xcap-el+xml",
        },
        {
          description: "[RFC4825]",
          type: "string",
          const: "xcap-error+xml",
        },
        {
          description: "[RFC4825]",
          type: "string",
          const: "xcap-ns+xml",
        },
        {
          description: "[RFC6502]",
          type: "string",
          const: "xcon-conference-info-diff+xml",
        },
        {
          description: "[RFC6502]",
          type: "string",
          const: "xcon-conference-info+xml",
        },
        {
          description: "[Joseph_Reagle][XENC_Working_Group]",
          type: "string",
          const: "xenc+xml",
        },
        {
          description: "[ISO-TC_171-SC_2][Betsy_Fanning]",
          type: "string",
          const: "xfdf",
        },
        {
          description: "[W3C][Robin_Berjon]",
          type: "string",
          const: "xhtml+xml",
        },
        {
          description: "[OASIS][Chet_Ensign]",
          type: "string",
          const: "xliff+xml",
        },
        {
          description: "[RFC7303]",
          type: "string",
          const: "xml",
        },
        {
          description: "[RFC7303]",
          type: "string",
          const: "xml-dtd",
        },
        {
          description: "[RFC7303]",
          type: "string",
          const: "xml-external-parsed-entity",
        },
        {
          description: "[RFC7351]",
          type: "string",
          const: "xml-patch+xml",
        },
        {
          description: "[RFC3923]",
          type: "string",
          const: "xmpp+xml",
        },
        {
          description: "[Mark_Nottingham]",
          type: "string",
          const: "xop+xml",
        },
        {
          description:
            "[W3C][http://www.w3.org/TR/2007/REC-xslt20-20070123/#media-type-registration]",
          type: "string",
          const: "xslt+xml",
        },
        {
          description: "[RFC4374]",
          type: "string",
          const: "xv+xml",
        },
        {
          description: "[RFC6020]",
          type: "string",
          const: "yang",
        },
        {
          description: "[RFC9254]",
          type: "string",
          const: "yang-data+cbor",
        },
        {
          description: "[RFC8040]",
          type: "string",
          const: "yang-data+json",
        },
        {
          description: "[RFC8040]",
          type: "string",
          const: "yang-data+xml",
        },
        {
          description: "[RFC8072]",
          type: "string",
          const: "yang-patch+json",
        },
        {
          description: "[RFC8072]",
          type: "string",
          const: "yang-patch+xml",
        },
        {
          description: "[RFC6020]",
          type: "string",
          const: "yin+xml",
        },
        {
          description: "[Paul_Lindner]",
          type: "string",
          const: "zip",
        },
        {
          description: "[RFC6713]",
          type: "string",
          const: "zlib",
        },
        {
          description: "[RFC8878]",
          type: "string",
          const: "zstd",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeApplicationSchema as unknown as IJSONSchema);

export const validateMediaTypeApplication =
  createValidator<IMediaTypeApplication>(
    mediaTypeApplicationSchema as unknown as IJSONSchema,
  );
