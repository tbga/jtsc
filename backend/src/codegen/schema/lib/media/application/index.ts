export {
  mediaTypeApplicationSchema,
  validateMediaTypeApplication,
} from "./lib.js";
export type { IMediaTypeApplication } from "./types.js";
