export { mediaTypeModelSchema, validateMediaTypeModel } from "./lib.js";
export type { IMediaTypeModel } from "./types.js";
