import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeModel } from "./types.js";

export const mediaTypeModelSchema = {
  $id: "https://jtsc-schemas.org/media/model.schema.json",
  title: "MediaTypeModel",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "model",
    },
    subtype: {
      anyOf: [
        {
          description: "[http://www.3mf.io/specification][_3MF][Michael_Sweet]",
          type: "string",
          const: "3mf",
        },
        {
          description: "[ASTM]",
          type: "string",
          const: "e57",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[Khronos][Saurabh_Bhatia]",
          type: "string",
          const: "gltf-binary",
        },
        {
          description: "[Khronos][Uli_Klumpp]",
          type: "string",
          const: "gltf+json",
        },
        {
          description: "[Curtis_Parks]",
          type: "string",
          const: "iges",
        },
        {
          description: "[RFC2077]",
          type: "string",
          const: "mesh",
        },
        {
          description: "[DICOM_Standard_Committee][DICOM_WG_17][Carolyn_Hull]",
          type: "string",
          const: "mtl",
        },
        {
          description: "[DICOM_Standard_Committee][DICOM_WG_17][Carolyn_Hull]",
          type: "string",
          const: "obj",
        },
        {
          description: "[ISO-TC_171-SC_2][Betsy_Fanning]",
          type: "string",
          const: "prc",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "step",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "step+xml",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "step+zip",
        },
        {
          description: "[ISO-TC_184-SC_4][Dana_Tripp]",
          type: "string",
          const: "step-xml+zip",
        },
        {
          description: "[DICOM_Standard_Committee][DICOM_WG_17][Carolyn_Hull]",
          type: "string",
          const: "stl",
        },
        {
          description: "[PDF_Association][Peter_Wyatt]",
          type: "string",
          const: "u3d",
        },
        {
          description: "[Robert_Monaghan]",
          type: "string",
          const: "vnd.cld",
        },
        {
          description: "[James_Riordon]",
          type: "string",
          const: "vnd.collada+xml",
        },
        {
          description: "[Jason_Pratt]",
          type: "string",
          const: "vnd.dwf",
        },
        {
          description: "[Michael_Powers]",
          type: "string",
          const: "vnd.flatland.3dml",
        },
        {
          description: "[Attila_Babits]",
          type: "string",
          const: "vnd.gdl",
        },
        {
          description: "[Attila_Babits]",
          type: "string",
          const: "vnd.gs-gdl",
        },
        {
          description: "[Yutaka_Ozaki]",
          type: "string",
          const: "vnd.gtw",
        },
        {
          description: "[Christopher_Brooks]",
          type: "string",
          const: "vnd.moml+xml",
        },
        {
          description: "[Boris_Rabinovitch]",
          type: "string",
          const: "vnd.mts",
        },
        {
          description: "[Eric_Lengyel]",
          type: "string",
          const: "vnd.opengex",
        },
        {
          description: "[Parasolid]",
          type: "string",
          const: "vnd.parasolid.transmit.binary",
        },
        {
          description: "[Parasolid]",
          type: "string",
          const: "vnd.parasolid.transmit.text",
        },
        {
          description: "[Daniel_Flassig]",
          type: "string",
          const: "vnd.pytha.pyox",
        },
        {
          description: "[Benson_Margulies]",
          type: "string",
          const: "vnd.rosette.annotated-data-model",
        },
        {
          description: "[SAP_SE][Igor_Afanasyev]",
          type: "string",
          const: "vnd.sap.vds",
        },
        {
          description: "[Sebastian_Grassia]",
          type: "string",
          const: "vnd.usda",
        },
        {
          description: "[Sebastian_Grassia]",
          type: "string",
          const: "vnd.usdz+zip",
        },
        {
          description: "[Henrik_Andersson]",
          type: "string",
          const: "vnd.valve.source.compiled-map",
        },
        {
          description: "[Boris_Rabinovitch]",
          type: "string",
          const: "vnd.vtu",
        },
        {
          description: "[RFC2077]",
          type: "string",
          const: "vrml",
        },
        {
          description: "[Web3D][Web3D_X3D]",
          type: "string",
          const: "x3d-vrml",
        },
        {
          description: "[Web3D_X3D]",
          type: "string",
          const: "x3d+fastinfoset",
        },
        {
          description: "[Web3D][Web3D_X3D]",
          type: "string",
          const: "x3d+xml",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeModelSchema as unknown as IJSONSchema);

export const validateMediaTypeModel = createValidator<IMediaTypeModel>(
  mediaTypeModelSchema as unknown as IJSONSchema,
);
