export { mediaTypeAudioSchema, validateMediaTypeAudio } from "./lib.js";
export type { IMediaTypeAudio } from "./types.js";
