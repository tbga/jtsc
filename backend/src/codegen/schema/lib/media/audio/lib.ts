import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeAudio } from "./types.js";

export const mediaTypeAudioSchema = {
  $id: "https://jtsc-schemas.org/media/audio.schema.json",
  title: "MediaTypeAudio",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "audio",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC6015]",
          type: "string",
          const: "1d-interleaved-parityfec",
        },
        {
          description: "[RFC3802][RFC2421]",
          type: "string",
          const: "32kadpcm",
        },
        {
          description: "[RFC3839][RFC6381]",
          type: "string",
          const: "3gpp",
        },
        {
          description: "[RFC4393][RFC6381]",
          type: "string",
          const: "3gpp2",
        },
        {
          description: "[ISO-IEC_JTC1][Max_Neuendorf]",
          type: "string",
          const: "aac",
        },
        {
          description: "[RFC4184]",
          type: "string",
          const: "ac3",
        },
        {
          description: "[RFC4867]",
          type: "string",
          const: "AMR",
        },
        {
          description: "[RFC4867]",
          type: "string",
          const: "AMR-WB",
        },
        {
          description: "[RFC4352]",
          type: "string",
          const: "amr-wb+",
        },
        {
          description: "[RFC7310]",
          type: "string",
          const: "aptx",
        },
        {
          description: "[RFC6295]",
          type: "string",
          const: "asc",
        },
        {
          description: "[RFC5584]",
          type: "string",
          const: "ATRAC-ADVANCED-LOSSLESS",
        },
        {
          description: "[RFC5584]",
          type: "string",
          const: "ATRAC-X",
        },
        {
          description: "[RFC5584]",
          type: "string",
          const: "ATRAC3",
        },
        {
          description: "[RFC2045][RFC2046]",
          type: "string",
          const: "basic",
        },
        {
          description: "[RFC4298]",
          type: "string",
          const: "BV16",
        },
        {
          description: "[RFC4298]",
          type: "string",
          const: "BV32",
        },
        {
          description: "[RFC4040]",
          type: "string",
          const: "clearmode",
        },
        {
          description: "[RFC3389]",
          type: "string",
          const: "CN",
        },
        {
          description: "[RFC3190]",
          type: "string",
          const: "DAT12",
        },
        {
          description: "[RFC4613]",
          type: "string",
          const: "dls",
        },
        {
          description: "[RFC3557]",
          type: "string",
          const: "dsr-es201108",
        },
        {
          description: "[RFC4060]",
          type: "string",
          const: "dsr-es202050",
        },
        {
          description: "[RFC4060]",
          type: "string",
          const: "dsr-es202211",
        },
        {
          description: "[RFC4060]",
          type: "string",
          const: "dsr-es202212",
        },
        {
          description: "[RFC6469]",
          type: "string",
          const: "DV",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "DVI4",
        },
        {
          description: "[RFC4598]",
          type: "string",
          const: "eac3",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "encaprtp",
        },
        {
          description: "[RFC4788]",
          type: "string",
          const: "EVRC",
        },
        {
          description: "[RFC3625]",
          type: "string",
          const: "EVRC-QCP",
        },
        {
          description: "[RFC4788]",
          type: "string",
          const: "EVRC0",
        },
        {
          description: "[RFC4788]",
          type: "string",
          const: "EVRC1",
        },
        {
          description: "[RFC5188]",
          type: "string",
          const: "EVRCB",
        },
        {
          description: "[RFC5188]",
          type: "string",
          const: "EVRCB0",
        },
        {
          description: "[RFC4788]",
          type: "string",
          const: "EVRCB1",
        },
        {
          description: "[RFC6884]",
          type: "string",
          const: "EVRCNW",
        },
        {
          description: "[RFC6884]",
          type: "string",
          const: "EVRCNW0",
        },
        {
          description: "[RFC6884]",
          type: "string",
          const: "EVRCNW1",
        },
        {
          description: "[RFC5188]",
          type: "string",
          const: "EVRCWB",
        },
        {
          description: "[RFC5188]",
          type: "string",
          const: "EVRCWB0",
        },
        {
          description: "[RFC5188]",
          type: "string",
          const: "EVRCWB1",
        },
        {
          description: "[_3GPP][Kyunghun_Jung]",
          type: "string",
          const: "EVS",
        },
        {
          description: "[RFC4735]",
          type: "string",
          const: "example",
        },
        {
          description: "[RFC8627]",
          type: "string",
          const: "flexfec",
        },
        {
          description: "[RFC6354]",
          type: "string",
          const: "fwdred",
        },
        {
          description: "[RFC7655]",
          type: "string",
          const: "G711-0",
        },
        {
          description: "[RFC5404][RFC Errata 3245]",
          type: "string",
          const: "G719",
        },
        {
          description: "[RFC5577]",
          type: "string",
          const: "G7221",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G722",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G723",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G726-16",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G726-24",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G726-32",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G726-40",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G728",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G729",
        },
        {
          description: "[RFC4749][RFC5459]",
          type: "string",
          const: "G7291",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G729D",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "G729E",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "GSM",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "GSM-EFR",
        },
        {
          description: "[RFC5993]",
          type: "string",
          const: "GSM-HR-08",
        },
        {
          description: "[RFC3952]",
          type: "string",
          const: "iLBC",
        },
        {
          description: "[RFC6262]",
          type: "string",
          const: "ip-mr_v2.5",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "L8",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "L16",
        },
        {
          description: "[RFC3190]",
          type: "string",
          const: "L20",
        },
        {
          description: "[RFC3190]",
          type: "string",
          const: "L24",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "LPC",
        },
        {
          description: "[RFC8130]",
          type: "string",
          const: "MELP",
        },
        {
          description: "[RFC8130]",
          type: "string",
          const: "MELP600",
        },
        {
          description: "[RFC8130]",
          type: "string",
          const: "MELP1200",
        },
        {
          description: "[RFC8130]",
          type: "string",
          const: "MELP2400",
        },
        {
          description: "[ISO-IEC_JTC1][Nils_Peters][Ingo_Hofmann]",
          type: "string",
          const: "mhas",
        },
        {
          description: "[RFC4723]",
          type: "string",
          const: "mobile-xmf",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "MPA",
        },
        {
          description: "[RFC4337][RFC6381]",
          type: "string",
          const: "mp4",
        },
        {
          description: "[RFC6416]",
          type: "string",
          const: "MP4A-LATM",
        },
        {
          description: "[RFC5219]",
          type: "string",
          const: "mpa-robust",
        },
        {
          description: "[RFC3003]",
          type: "string",
          const: "mpeg",
        },
        {
          description: "[RFC3640][RFC5691][RFC6295]",
          type: "string",
          const: "mpeg4-generic",
        },
        {
          description: "[RFC5334][RFC7845]",
          type: "string",
          const: "ogg",
        },
        {
          description: "[RFC7587]",
          type: "string",
          const: "opus",
        },
        {
          description: "[RFC3009]",
          type: "string",
          const: "parityfec",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "PCMA",
        },
        {
          description: "[RFC5391]",
          type: "string",
          const: "PCMA-WB",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "PCMU",
        },
        {
          description: "[RFC5391]",
          type: "string",
          const: "PCMU-WB",
        },
        {
          description: "[Linus_Walleij]",
          type: "string",
          const: "prs.sid",
        },
        {
          description: "[RFC3555][RFC3625]",
          type: "string",
          const: "QCELP",
        },
        {
          description: "[RFC6682]",
          type: "string",
          const: "raptorfec",
        },
        {
          description: "[RFC3555]",
          type: "string",
          const: "RED",
        },
        {
          description: "[_3GPP]",
          type: "string",
          const: "rtp-enc-aescm128",
        },
        {
          description: "[RFC6849]",
          type: "string",
          const: "rtploopback",
        },
        {
          description: "[RFC6295]",
          type: "string",
          const: "rtp-midi",
        },
        {
          description: "[RFC4588]",
          type: "string",
          const: "rtx",
        },
        {
          description: "[SCIP][Michael_Faller][Daniel_Hanson]",
          type: "string",
          const: "scip",
        },
        {
          description: "[RFC3558]",
          type: "string",
          const: "SMV",
        },
        {
          description: "[RFC3558]",
          type: "string",
          const: "SMV0",
        },
        {
          description: "[RFC3625]",
          type: "string",
          const: "SMV-QCP",
        },
        {
          description: "[AES][Piotr_Majdak]",
          type: "string",
          const: "sofa",
        },
        {
          description: "[Athan_Billias][MIDI_Association]",
          type: "string",
          const: "sp-midi",
        },
        {
          description: "[RFC5574]",
          type: "string",
          const: "speex",
        },
        {
          description: "[RFC4351]",
          type: "string",
          const: "t140c",
        },
        {
          description: "[RFC4612]",
          type: "string",
          const: "t38",
        },
        {
          description: "[RFC4733]",
          type: "string",
          const: "telephone-event",
        },
        {
          description: "[ETSI][Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "TETRA_ACELP",
        },
        {
          description: "[ETSI][Miguel_Angel_Reina_Ortega]",
          type: "string",
          const: "TETRA_ACELP_BB",
        },
        {
          description: "[RFC4733]",
          type: "string",
          const: "tone",
        },
        {
          description: "[RFC8817]",
          type: "string",
          const: "TSVCIS",
        },
        {
          description: "[RFC5686]",
          type: "string",
          const: "UEMCLIP",
        },
        {
          description: "[RFC5109]",
          type: "string",
          const: "ulpfec",
        },
        {
          description: "[ISO-IEC_JTC1][Max_Neuendorf]",
          type: "string",
          const: "usac",
        },
        {
          description: "[RFC4856]",
          type: "string",
          const: "VDVI",
        },
        {
          description: "[RFC4348][RFC4424]",
          type: "string",
          const: "VMR-WB",
        },
        {
          description: "[Thomas_Belling]",
          type: "string",
          const: "vnd.3gpp.iufp",
        },
        {
          description: "[Serge_De_Jaham]",
          type: "string",
          const: "vnd.4SB",
        },
        {
          description: "[Vicki_DeBarros]",
          type: "string",
          const: "vnd.audiokoz",
        },
        {
          description: "[Serge_De_Jaham]",
          type: "string",
          const: "vnd.CELP",
        },
        {
          description: "[Rajesh_Kumar]",
          type: "string",
          const: "vnd.cisco.nse",
        },
        {
          description: "[Jean-Philippe_Goulet]",
          type: "string",
          const: "vnd.cmles.radio-events",
        },
        {
          description: "[Ann_McLaughlin]",
          type: "string",
          const: "vnd.cns.anp1",
        },
        {
          description: "[Ann_McLaughlin]",
          type: "string",
          const: "vnd.cns.inf1",
        },
        {
          description: "[Michael_A_Dolan]",
          type: "string",
          const: "vnd.dece.audio",
        },
        {
          description: "[Armands_Strazds]",
          type: "string",
          const: "vnd.digital-winds",
        },
        {
          description: "[Edwin_Heredia]",
          type: "string",
          const: "vnd.dlna.adts",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.heaac.1",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.heaac.2",
        },
        {
          description: "[Mike_Ward]",
          type: "string",
          const: "vnd.dolby.mlp",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.mps",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.pl2",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.pl2x",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.pl2z",
        },
        {
          description: "[Steve_Hattersley]",
          type: "string",
          const: "vnd.dolby.pulse.1",
        },
        {
          description: "[Jiang_Tian]",
          type: "string",
          const: "vnd.dra",
        },
        {
          description: "[William_Zou]",
          type: "string",
          const: "vnd.dts",
        },
        {
          description: "[William_Zou]",
          type: "string",
          const: "vnd.dts.hd",
        },
        {
          description: "[Phillip_Maness]",
          type: "string",
          const: "vnd.dts.uhd",
        },
        {
          description: "[Peter_Siebert]",
          type: "string",
          const: "vnd.dvb.file",
        },
        {
          description: "[Shay_Cicelsky]",
          type: "string",
          const: "vnd.everad.plj",
        },
        {
          description: "[Swaminathan]",
          type: "string",
          const: "vnd.hns.audio",
        },
        {
          description: "[Greg_Vaudreuil]",
          type: "string",
          const: "vnd.lucent.voice",
        },
        {
          description: "[Steve_DiAcetis]",
          type: "string",
          const: "vnd.ms-playready.media.pya",
        },
        {
          description: "[Nokia]",
          type: "string",
          const: "vnd.nokia.mobile-xmf",
        },
        {
          description: "[Glenn_Parsons]",
          type: "string",
          const: "vnd.nortel.vbk",
        },
        {
          description: "[Michael_Fox]",
          type: "string",
          const: "vnd.nuera.ecelp4800",
        },
        {
          description: "[Michael_Fox]",
          type: "string",
          const: "vnd.nuera.ecelp7470",
        },
        {
          description: "[Michael_Fox]",
          type: "string",
          const: "vnd.nuera.ecelp9600",
        },
        {
          description: "[Greg_Vaudreuil]",
          type: "string",
          const: "vnd.octel.sbc",
        },
        {
          description: "[Matthias_Juwan]",
          type: "string",
          const: "vnd.presonus.multitrack",
        },
        {
          description: "[RFC3625]",
          type: "string",
          const: "vnd.qcelp - DEPRECATED in favor of audio/qcelp",
        },
        {
          description: "[Greg_Vaudreuil]",
          type: "string",
          const: "vnd.rhetorex.32kadpcm",
        },
        {
          description: "[Martin_Dawe]",
          type: "string",
          const: "vnd.rip",
        },
        {
          description: "[David_Petersen]",
          type: "string",
          const: "vnd.sealedmedia.softseal.mpeg",
        },
        {
          description: "[Greg_Vaudreuil]",
          type: "string",
          const: "vnd.vmx.cvsd",
        },
        {
          description: "[RFC5215]",
          type: "string",
          const: "vorbis",
        },
        {
          description: "[RFC5215]",
          type: "string",
          const: "vorbis-config",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeAudioSchema as unknown as IJSONSchema);

export const validateMediaTypeAudio = createValidator<IMediaTypeAudio>(
  mediaTypeAudioSchema as unknown as IJSONSchema,
);
