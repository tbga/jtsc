export { mediaTypeFontSchema, validateMediaTypeFont } from "./lib.js";
export type { IMediaTypeFont } from "./types.js";
