export interface IMediaTypeFont {
  type: "font";
  subtype: "collection" | "otf" | "sfnt" | "ttf" | "woff" | "woff2";
}
