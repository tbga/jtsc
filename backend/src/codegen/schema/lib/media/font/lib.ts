import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IMediaTypeFont } from "./types.js";

export const mediaTypeFontSchema = {
  $id: "https://jtsc-schemas.org/media/font.schema.json",
  title: "MediaTypeFont",
  type: "object",
  additionalProperties: false,
  required: ["type", "subtype"],
  properties: {
    type: {
      type: "string",
      const: "font",
    },
    subtype: {
      anyOf: [
        {
          description: "[RFC8081]",
          type: "string",
          const: "collection",
        },
        {
          description: "[RFC8081]",
          type: "string",
          const: "otf",
        },
        {
          description: "[RFC8081]",
          type: "string",
          const: "sfnt",
        },
        {
          description: "[RFC8081]",
          type: "string",
          const: "ttf",
        },
        {
          description: "[RFC8081]",
          type: "string",
          const: "woff",
        },
        {
          description: "[RFC8081]",
          type: "string",
          const: "woff2",
        },
      ],
    },
  },
} as const;
addSchema(mediaTypeFontSchema as unknown as IJSONSchema);

export const validateMediaTypeFont = createValidator<IMediaTypeFont>(
  mediaTypeFontSchema as unknown as IJSONSchema,
);
