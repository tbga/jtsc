import { type INameInit } from "#codegen/schema/lib/entities/name/init";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IArtistInit } from "#codegen/schema/lib/entities/artist/init";

/**
 * Profile initializer.
 */
export interface IProfileInit {
  /**
   * @minItems 1
   */
  names?: [INameInit, ...INameInit[]];
  site_id?: IBigSerialInteger;
  /**
   * The profile ID on the site.
   */
  original_id?: string;
  original_bio?: string;
  /**
   * Profile releases data.
   */
  releases?: {
    /**
     * A list of existing release IDs.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
  };
  /**
   * Profile artists data.
   */
  artists?: {
    /**
     * A list of existing artist IDs.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
    /**
     * A list of artist initializers.
     *
     * @minItems 1
     */
    inits?: [IArtistInit, ...IArtistInit[]];
  };
}
