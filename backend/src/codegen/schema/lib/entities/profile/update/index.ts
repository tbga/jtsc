export { profileUpdateSchema, validateProfileUpdate } from "./lib.js";
export type { IProfileUpdate } from "./types.js";
