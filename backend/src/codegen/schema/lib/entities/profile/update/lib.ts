import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IProfileUpdate } from "./types.js";

export const profileUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/update.schema.json",
  title: "ProfileUpdate",
  description: "An update of artist's profile.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    original_id: {
      type: "string",
      minLength: 1,
      description:
        "The ID of the profile on the site it was registered. It is a string because the sites have a different opinion on what is an ID.\nIs optional because it's not always possible prove the existence of the profile (it is a private profile or the site is defunct etc...).",
    },
    original_bio: {
      type: "string",
      minLength: 1,
      description: "The profile's bio.",
    },
  },
} as const;
addSchema(profileUpdateSchema as unknown as IJSONSchema);

export const validateProfileUpdate = createValidator<IProfileUpdate>(
  profileUpdateSchema as unknown as IJSONSchema,
);
