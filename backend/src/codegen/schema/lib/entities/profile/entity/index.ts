export { profileSchema, validateProfile } from "./lib.js";
export type { IProfile } from "./types.js";
