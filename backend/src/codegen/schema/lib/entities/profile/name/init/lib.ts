import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IProfileNameInit } from "./types.js";

export const profileNameInitSchema = {
  $id: "https://jtsc-schemas.org/entities/profile/name/init.schema.json",
  $comment: "@TODO remove it",
  title: "ProfileNameInit",
  description: "Initializer for a profile name.",
  type: "object",
  additionalProperties: false,
  required: ["profile_id", "name_id"],
  properties: {
    profile_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
  },
} as const;
addSchema(profileNameInitSchema as unknown as IJSONSchema);

export const validateProfileNameInit = createValidator<IProfileNameInit>(
  profileNameInitSchema as unknown as IJSONSchema,
);
