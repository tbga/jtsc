import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Initializer for a profile name.
 */
export interface IProfileNameInit {
  profile_id: IBigSerialInteger;
  name_id: IBigSerialInteger;
}
