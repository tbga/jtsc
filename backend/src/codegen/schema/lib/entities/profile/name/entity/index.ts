export { profileNameSchema, validateProfileName } from "./lib.js";
export type { IProfileName } from "./types.js";
