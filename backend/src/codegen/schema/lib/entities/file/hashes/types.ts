import { type IHashSHA256 } from "#codegen/schema/lib/types/strings/hashes/sha256";
import { type IHashSHA1 } from "#codegen/schema/lib/types/strings/hashes/sha1";
import { type IHashMD5 } from "#codegen/schema/lib/types/strings/hashes/md5";

/**
 * The hashes of a file.
 */
export interface IFileHashes {
  sha256?: IHashSHA256;
  sha1?: IHashSHA1;
  md5?: IHashMD5;
}
