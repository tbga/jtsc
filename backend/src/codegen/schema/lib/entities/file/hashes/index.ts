export { fileHashesSchema, validateFileHashes } from "./lib.js";
export type { IFileHashes } from "./types.js";
