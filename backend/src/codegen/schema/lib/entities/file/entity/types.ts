import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IFileHashes } from "#codegen/schema/lib/entities/file/hashes";

/**
 * The file on the resource.
 */
export interface IFile {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  /**
   * The name of the file.
   */
  name: string;
  /**
   * A url of the file.
   */
  url: string;
  /**
   * Media type.
   */
  type:
    | "application"
    | "audio"
    | "font"
    | "image"
    | "message"
    | "model"
    | "multipart"
    | "text"
    | "video";
  /**
   * Media subtype.
   */
  subtype: string;
  /**
   * Size of the file in bytes.
   */
  size: string;
  hashes?: IFileHashes;
}
