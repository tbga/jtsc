export { fileUpdateSchema, validateFileUpdate } from "./lib.js";
export type { IFileUpdate } from "./types.js";
