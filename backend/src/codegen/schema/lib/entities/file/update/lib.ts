import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IFileUpdate } from "./types.js";

export const fileUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/file/update.schema.json",
  title: "FileUpdate",
  description: "The update of the file.",
  type: "object",
  additionalProperties: false,
  required: ["name"],
  properties: {
    name: {
      description: "The name of the file.",
      type: "string",
    },
  },
} as const;
addSchema(fileUpdateSchema as unknown as IJSONSchema);

export const validateFileUpdate = createValidator<IFileUpdate>(
  fileUpdateSchema as unknown as IJSONSchema,
);
