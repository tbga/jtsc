/**
 * The update of the file.
 */
export interface IFileUpdate {
  /**
   * The name of the file.
   */
  name: string;
}
