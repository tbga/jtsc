import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Preview of the file,
 */
export interface IFilePreview {
  id: IBigSerialInteger;
  /**
   * The name of the file.
   */
  name: string;
  /**
   * A url of the file.
   */
  url: string;
  /**
   * Media type.
   */
  type:
    | "application"
    | "audio"
    | "font"
    | "image"
    | "message"
    | "model"
    | "multipart"
    | "text"
    | "video";
  /**
   * Media subtype.
   */
  subtype: string;
  /**
   * Size of the file in bytes.
   */
  size: string;
}
