export { filePreviewSchema, validateFilePreview } from "./lib.js";
export type { IFilePreview } from "./types.js";
