import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IFilePreview } from "./types.js";

export const filePreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/file/preview.schema.json",
  title: "FilePreview",
  description: "Preview of the file,",
  type: "object",
  additionalProperties: false,
  required: ["id", "url", "name", "type", "subtype", "size"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "The name of the file.",
      type: "string",
      minLength: 1,
    },
    url: {
      description: "A url of the file.",
      type: "string",
      minLength: 1,
    },
    type: {
      description: "Media type.",
      type: "string",
      enum: [
        "application",
        "audio",
        "font",
        "image",
        "message",
        "model",
        "multipart",
        "text",
        "video",
      ],
    },
    subtype: {
      description: "Media subtype.",
      $comment: '@TODO make it dependant off the `"type"` key.',
      type: "string",
      minLength: 1,
    },
    size: {
      type: "string",
      description: "Size of the file in bytes.",
      minLength: 1,
    },
  },
} as const;
addSchema(filePreviewSchema as unknown as IJSONSchema);

export const validateFilePreview = createValidator<IFilePreview>(
  filePreviewSchema as unknown as IJSONSchema,
);
