export { siteURLTemplatesSchema, validateSiteURLTemplates } from "./lib.js";
export type { ISiteURLTemplates } from "./types.js";
