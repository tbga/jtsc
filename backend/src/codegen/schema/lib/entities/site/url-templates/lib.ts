import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type ISiteURLTemplates } from "./types.js";

export const siteURLTemplatesSchema = {
  $id: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
  title: "SiteURLTemplates",
  description:
    "[URL templates](tools.ietf.org/html/rfc6570) for a given site. They must include the same data as the site uses for its frontend, even if it's not needed for a successful URL resolution.\nFor example some sites append human-readable text to an ID and the url resolves without the append just fine too. But the text must still be included in the original value of the ID or the template, depending on use case.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    profile_list: {
      description: "A template for the url to the list of profiles.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profiles",
        "{origin}/profiles/{profile_page}",
        "{origin}/profiles{?profile_page}",
      ],
    },
    profile_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    profile: {
      description: "A template for the url to the profile.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/profile/{profile_id}",
        "{origin}/profile{?profile_id}",
      ],
    },
    profile_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release_list: {
      description: "A template for the url to the list of releases.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/releases",
        "{origin}/releases/{release_page}",
        "{origin}/releases{?release_page}",
        "{origin}/profile/{profile_id}/releases/{release_page}",
        "{origin}/profile/{profile_id}/releases{?release_page}",
      ],
    },
    release_list_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
    release: {
      description: "A template for the url to the release.",
      type: "string",
      format: "uri-template",
      examples: [
        "{origin}/release/{release_id}",
        "{origin}/release{?release_id}",
        "{origin}/{profile_id}/{release_id}",
        "{origin}{?profile_id,release_id}",
        "{origin}/profile/{profile_id}/release/{release_id}",
        "{origin}/profile/{profile_id}/release{?release_id}",
      ],
    },
    release_notes: {
      $ref: "http://jtsc-schemas.org/types/strings/description.schema.json",
    },
  },
} as const;
addSchema(siteURLTemplatesSchema as unknown as IJSONSchema);

export const validateSiteURLTemplates = createValidator<ISiteURLTemplates>(
  siteURLTemplatesSchema as unknown as IJSONSchema,
);
