import { type IDescription } from "#codegen/schema/lib/types/strings/description";

/**
 * [URL templates](tools.ietf.org/html/rfc6570) for a given site. They must include the same data as the site uses for its frontend, even if it's not needed for a successful URL resolution.
 * For example some sites append human-readable text to an ID and the url resolves without the append just fine too. But the text must still be included in the original value of the ID or the template, depending on use case.
 */
export interface ISiteURLTemplates {
  /**
   * A template for the url to the list of profiles.
   */
  profile_list?: string;
  profile_list_notes?: IDescription;
  /**
   * A template for the url to the profile.
   */
  profile?: string;
  profile_notes?: IDescription;
  /**
   * A template for the url to the list of releases.
   */
  release_list?: string;
  release_list_notes?: IDescription;
  /**
   * A template for the url to the release.
   */
  release?: string;
  release_notes?: IDescription;
}
