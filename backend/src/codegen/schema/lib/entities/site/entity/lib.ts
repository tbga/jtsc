import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type ISite } from "./types.js";

export const siteSchema = {
  $id: "https://jtsc-schemas.org/entities/site/entity.schema.json",
  title: "Site",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at", "home_page", "title", "name"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    long_title: {
      type: "string",
      description:
        "Long title, most likely a follow up to the main title on the home page.",
    },
    description: {
      type: "string",
      description:
        "A description of the site, most likely to be a follow up to the long title on the home page.",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    published_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    published_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
} as const;
addSchema(siteSchema as unknown as IJSONSchema);

export const validateSite = createValidator<ISite>(
  siteSchema as unknown as IJSONSchema,
);
