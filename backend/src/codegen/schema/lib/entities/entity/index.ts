export { entitySchema, validateEntity } from "./lib.js";
export type { IEntity } from "./types.js";
