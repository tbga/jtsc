import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";

/**
 * A baseline entity.
 */
export interface IEntity {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  /**
   * A human-readable name for the entity
   */
  name?: string;
}
