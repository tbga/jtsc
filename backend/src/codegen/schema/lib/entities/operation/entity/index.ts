export { operationSchema, validateOperation } from "./lib.js";
export type { IOperation } from "./types.js";
