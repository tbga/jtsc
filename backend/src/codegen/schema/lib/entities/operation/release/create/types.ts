import { type IReleaseInit } from "#codegen/schema/lib/entities/release/init";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for the releases creation
 */
export interface IOperationReleasesCreate {
  type: "releases_create";
  input_data: IReleaseInit[];
  /**
   * A list of added releases IDs.
   */
  result_data: IBigSerialInteger[];
}
