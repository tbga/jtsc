import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationReleasesCreate } from "./types.js";

export const operationReleasesCreateSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/release/create.schema.json",
  title: "OperationReleasesCreate",
  description: "Operation for the releases creation",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "releases_create",
    },
    input_data: {
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
      },
    },
    result_data: {
      description: "A list of added releases IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(operationReleasesCreateSchema as unknown as IJSONSchema);

export const validateOperationReleasesCreate =
  createValidator<IOperationReleasesCreate>(
    operationReleasesCreateSchema as unknown as IJSONSchema,
  );
