export { operationTypeSchema, validateOperationType } from "./lib.js";
export type { IOperationType } from "./types.js";
