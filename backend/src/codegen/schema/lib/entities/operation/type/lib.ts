import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationType } from "./types.js";

export const operationTypeSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/type.schema.json",
  title: "OperationType",
  description: "The type of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    {
      const: "local_upload",
      description: "Upload of the files.",
    },
    {
      const: "releases_create",
      description: "Creation of releases.",
    },
    {
      const: "profiles_create",
      description: "Creation of profiles.",
    },
    {
      const: "public_exports_finish",
      description: "Finish public exports.",
    },
    {
      const: "public_imports_create",
      description: "Create public imports.",
    },
    {
      const: "public_imports_consume",
      description: "Consume public imports.",
    },
  ],
} as const;
addSchema(operationTypeSchema as unknown as IJSONSchema);

export const validateOperationType = createValidator<IOperationType>(
  operationTypeSchema as unknown as IJSONSchema,
);
