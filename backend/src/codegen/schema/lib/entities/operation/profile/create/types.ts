import { type IProfileInit } from "#codegen/schema/lib/entities/profile/init";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for the profiles creation.
 */
export interface IOperationProfilesCreate {
  type: "profiles_create";
  /**
   * A list of profile initializers.
   *
   * @minItems 1
   */
  input_data: [IProfileInit, ...IProfileInit[]];
  /**
   * A list of added profile IDs.
   *
   * @minItems 1
   */
  result_data: [IBigSerialInteger, ...IBigSerialInteger[]];
}
