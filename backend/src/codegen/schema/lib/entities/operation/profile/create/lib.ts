import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationProfilesCreate } from "./types.js";

export const operationProfilesCreateSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/profile/create.schema.json",
  title: "OperationProfilesCreate",
  description: "Operation for the profiles creation.",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "profiles_create",
    },
    input_data: {
      description: "A list of profile initializers.",
      type: "array",
      minItems: 1,
      items: {
        $ref: "https://jtsc-schemas.org/entities/profile/init.schema.json",
      },
    },
    result_data: {
      description: "A list of added profile IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(operationProfilesCreateSchema as unknown as IJSONSchema);

export const validateOperationProfilesCreate =
  createValidator<IOperationProfilesCreate>(
    operationProfilesCreateSchema as unknown as IJSONSchema,
  );
