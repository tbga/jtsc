import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationPreview } from "./types.js";

export const operationPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/preview.schema.json",
  title: "OperationPreview",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at", "created_by", "type", "status"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/operation/status.schema.json",
    },
    type: {
      $ref: "https://jtsc-schemas.org/entities/operation/type.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
  },
} as const;
addSchema(operationPreviewSchema as unknown as IJSONSchema);

export const validateOperationPreview = createValidator<IOperationPreview>(
  operationPreviewSchema as unknown as IJSONSchema,
);
