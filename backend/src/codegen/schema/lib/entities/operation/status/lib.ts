import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationStatus } from "./types.js";

export const operationStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/status.schema.json",
  title: "OperationStatus",
  description: "The status of operation.",
  type: "object",
  additionalProperties: false,
  anyOf: [
    {
      const: "pending",
      description: "The operation is awaiting in queue.",
    },
    {
      const: "in-progress",
      description: "The operation is currently being ran.",
    },
    {
      const: "finished",
      description: "The operation has been successfully finished.",
    },
    {
      const: "failed",
      description: "The operation failed with errors.",
    },
  ],
} as const;
addSchema(operationStatusSchema as unknown as IJSONSchema);

export const validateOperationStatus = createValidator<IOperationStatus>(
  operationStatusSchema as unknown as IJSONSchema,
);
