/**
 * The status of operation.
 */
export type IOperationStatus =
  | "pending"
  | "in-progress"
  | "finished"
  | "failed";
