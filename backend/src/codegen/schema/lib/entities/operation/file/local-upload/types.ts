import { type IFileInit } from "#codegen/schema/lib/entities/file/init";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for the local upload
 */
export interface IOperationLocalUpload {
  type: "local_upload";
  /**
   * A list of file initializers.
   */
  input_data: IFileInit[];
  /**
   * A list of added files IDs.
   */
  result_data: IBigSerialInteger[];
}
