import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for the consumption of public imports.
 */
export interface IOperationPublicImportsConsume {
  type: "public_imports_consume";
  /**
   * A list of public import IDs to consume.
   */
  input_data: IBigSerialInteger[];
  /**
   * A list of consumed public import IDs.
   */
  result_data: IBigSerialInteger[];
}
