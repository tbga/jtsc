export {
  operationPublicImportsConsumeSchema,
  validateOperationPublicImportsConsume,
} from "./lib.js";
export type { IOperationPublicImportsConsume } from "./types.js";
