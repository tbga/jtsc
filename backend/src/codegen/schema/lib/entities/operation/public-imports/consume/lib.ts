import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IOperationPublicImportsConsume } from "./types.js";

export const operationPublicImportsConsumeSchema = {
  $id: "https://jtsc-schemas.org/entities/operation/public-imports/consume.schema.json",
  title: "OperationPublicImportsConsume",
  description: "Operation for the consumption of public imports.",
  type: "object",
  additionalProperties: false,
  required: ["type", "input_data", "result_data"],
  properties: {
    type: {
      const: "public_imports_consume",
    },
    input_data: {
      description: "A list of public import IDs to consume.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
    result_data: {
      description: "A list of consumed public import IDs.",
      type: "array",
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(operationPublicImportsConsumeSchema as unknown as IJSONSchema);

export const validateOperationPublicImportsConsume =
  createValidator<IOperationPublicImportsConsume>(
    operationPublicImportsConsumeSchema as unknown as IJSONSchema,
  );
