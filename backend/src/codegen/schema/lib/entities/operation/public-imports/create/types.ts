import { type IPath } from "#codegen/schema/lib/local-file-system/path";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Operation for the creation of public imports.
 */
export interface IOperationPublicImportsCreate {
  type: "public_imports_create";
  /**
   * A list of file urls to the import folders.
   */
  input_data: IPath[];
  /**
   * A list of added public import IDs.
   */
  result_data: IBigSerialInteger[];
}
