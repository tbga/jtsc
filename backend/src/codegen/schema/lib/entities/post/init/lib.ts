import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPostInit } from "./types.js";

export const postInitSchema = {
  $id: "https://jtsc-schemas.org/entities/post/init.schema.json",
  title: "PostInit",
  description: "Initializer for the post.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    artists: {
      description: "Artists of the post.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "IDs of existing artists.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "Initializers for new artists.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/artist/init.schema.json",
          },
        },
      },
    },
    releases: {
      description: "Releases of the post.",
      type: "object",
      additionalProperties: false,
      minProperties: 1,
      properties: {
        ids: {
          description: "IDs of existing releases.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
        inits: {
          description: "Initializers for new releases.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/release/init.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(postInitSchema as unknown as IJSONSchema);

export const validatePostInit = createValidator<IPostInit>(
  postInitSchema as unknown as IJSONSchema,
);
