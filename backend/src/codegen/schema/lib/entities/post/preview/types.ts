import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";

/**
 * Preview of the post.
 */
export interface IPostPreview {
  id: IBigSerialInteger;
  releases: IBigIntegerPositive;
  artists: IBigIntegerPositive;
  name?: ITitle;
  title?: ITitle;
}
