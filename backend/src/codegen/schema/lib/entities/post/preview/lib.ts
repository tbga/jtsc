import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPostPreview } from "./types.js";

export const postPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/post/preview.schema.json",
  title: "PostPreview",
  description: "Preview of the post.",
  type: "object",
  additionalProperties: false,
  required: ["id", "releases", "artists"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    artists: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
addSchema(postPreviewSchema as unknown as IJSONSchema);

export const validatePostPreview = createValidator<IPostPreview>(
  postPreviewSchema as unknown as IJSONSchema,
);
