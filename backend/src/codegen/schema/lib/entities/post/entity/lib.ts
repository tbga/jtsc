import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPost } from "./types.js";

export const postSchema = {
  $id: "https://jtsc-schemas.org/entities/post/entity.schema.json",
  title: "Post",
  description: "The post details.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
addSchema(postSchema as unknown as IJSONSchema);

export const validatePost = createValidator<IPost>(
  postSchema as unknown as IJSONSchema,
);
