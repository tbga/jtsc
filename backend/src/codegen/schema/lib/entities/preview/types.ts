import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";

/**
 * A preview of the entity.
 */
export interface IEntityPreview {
  id: IBigSerialInteger;
  /**
   * A human-readable name for the entity
   */
  name?: string;
  public_id?: IUUID;
}
