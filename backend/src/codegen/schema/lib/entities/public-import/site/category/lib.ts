import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImportSiteCategory } from "./types.js";

export const publicImportSiteCategorySchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
  title: "PublicImportSiteCategory",
  anyOf: [
    {
      description: "Sites of the import which are absent from the database.",
      const: "new",
    },
    {
      description:
        "Sites of the import which are present in the database and have exact same fields.",
      const: "same",
    },
    {
      description:
        "Sites of the import which are present in the database and have at least one different field.",
      const: "different",
    },
  ],
} as const;
addSchema(publicImportSiteCategorySchema as unknown as IJSONSchema);

export const validatePublicImportSiteCategory =
  createValidator<IPublicImportSiteCategory>(
    publicImportSiteCategorySchema as unknown as IJSONSchema,
  );
