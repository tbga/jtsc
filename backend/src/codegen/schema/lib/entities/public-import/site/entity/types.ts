import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";
import { type IPublicImportSiteCategory } from "#codegen/schema/lib/entities/public-import/site/category";
import { type IPublicImportSiteApprovalStatus } from "#codegen/schema/lib/entities/public-import/site/approval-status";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type ISiteURLTemplates } from "#codegen/schema/lib/entities/site/url-templates";

/**
 * A public import site entity.
 */
export interface IPublicImportSite {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  public_import: IEntityItem;
  category: IPublicImportSiteCategory;
  approval_status: IPublicImportSiteApprovalStatus;
  public_id: IUUID;
  local_site?: IEntityItem;
  /**
   * The site's home page.
   */
  home_page: string;
  title: ITitle;
  name?: ITitle;
  long_title?: INonEmptyString;
  description?: INonEmptyString;
  url_templates?: ISiteURLTemplates;
}
