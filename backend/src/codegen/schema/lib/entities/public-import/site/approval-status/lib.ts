import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImportSiteApprovalStatus } from "./types.js";

export const publicImportSiteApprovalStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
  title: "PublicImportSiteApprovalStatus",
  description: "Approval status of public import's site.",
  anyOf: [
    {
      const: "undecided",
      description: "There was no approval decision yet.",
    },
    {
      const: "approved",
      description: "The site is approved for public import.",
    },
    {
      const: "rejected",
      description: "The site is rejected from public import.",
    },
  ],
} as const;
addSchema(publicImportSiteApprovalStatusSchema as unknown as IJSONSchema);

export const validatePublicImportSiteApprovalStatus =
  createValidator<IPublicImportSiteApprovalStatus>(
    publicImportSiteApprovalStatusSchema as unknown as IJSONSchema,
  );
