import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IPublicImportSiteCategory } from "#codegen/schema/lib/entities/public-import/site/category";
import { type IPublicImportSiteApprovalStatus } from "#codegen/schema/lib/entities/public-import/site/approval-status";

/**
 * A preview of the public import site.
 */
export interface IPublicImportSitePreview {
  id: IBigSerialInteger;
  public_import: IEntityItem;
  public_id: IUUID;
  local_site?: IEntityItem;
  title: ITitle;
  category: IPublicImportSiteCategory;
  approval_status: IPublicImportSiteApprovalStatus;
  name?: ITitle;
}
