import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImportSite } from "./types.js";

export const publicImportSiteSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/site/entity.schema.json",
  title: "PublicImportSite",
  description: "A public import site entity.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "category",
    "approval_status",
    "public_import",
    "public_id",
    "home_page",
    "title",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    public_import: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    category: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/category.schema.json",
    },
    approval_status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/site/approval-status.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    local_site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    home_page: {
      type: "string",
      format: "uri",
      description: "The site's home page.",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    long_title: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    url_templates: {
      $ref: "https://jtsc-schemas.org/entities/site/url-templates.schema.json",
    },
  },
} as const;
addSchema(publicImportSiteSchema as unknown as IJSONSchema);

export const validatePublicImportSite = createValidator<IPublicImportSite>(
  publicImportSiteSchema as unknown as IJSONSchema,
);
