/**
 * Approval status of public import's site.
 */
export type IPublicImportSiteApprovalStatus =
  | "undecided"
  | "approved"
  | "rejected";
