/**
 * Status of the public import.
 */
export type IPublicImportStatus = "pending" | "in-progress" | "consumed";
