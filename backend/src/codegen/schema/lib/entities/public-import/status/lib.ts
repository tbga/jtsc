import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImportStatus } from "./types.js";

export const publicImportStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/status.schema.json",
  title: "PublicImportStatus",
  description: "Status of the public import.",
  anyOf: [
    {
      const: "pending",
      description: "Public import is pending.",
    },
    {
      const: "in-progress",
      description: "Public import is being consumed.",
    },
    {
      const: "consumed",
      description: "Public import is consumed.",
    },
  ],
} as const;
addSchema(publicImportStatusSchema as unknown as IJSONSchema);

export const validatePublicImportStatus = createValidator<IPublicImportStatus>(
  publicImportStatusSchema as unknown as IJSONSchema,
);
