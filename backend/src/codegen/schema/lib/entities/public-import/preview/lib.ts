import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImportPreview } from "./types.js";

export const publicImportPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/preview.schema.json",
  title: "PublicImportPreview",
  description: "A preview of the public import.",
  type: "object",
  additionalProperties: false,
  required: ["id", "public_id", "created_by", "status", "title", "sites"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/status.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
addSchema(publicImportPreviewSchema as unknown as IJSONSchema);

export const validatePublicImportPreview =
  createValidator<IPublicImportPreview>(
    publicImportPreviewSchema as unknown as IJSONSchema,
  );
