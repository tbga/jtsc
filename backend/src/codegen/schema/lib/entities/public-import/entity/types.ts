import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IUUID } from "#codegen/schema/lib/types/strings/uuid/v4";
import { type IPublicImportStatus } from "#codegen/schema/lib/entities/public-import/status";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

/**
 * A public import entity.
 */
export interface IPublicImport {
  id: IBigSerialInteger;
  created_at: IDateTime;
  updated_at: IDateTime;
  created_by: IBigSerialInteger;
  public_id: IUUID;
  status: IPublicImportStatus;
  type: "basic";
  version: 1;
  title: ITitle;
  name?: ITitle;
  sites: IBigIntegerPositive;
  is_consumable: boolean;
}
