import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicImport } from "./types.js";

export const publicImportSchema = {
  $id: "https://jtsc-schemas.org/entities/public-import/entity.schema.json",
  title: "PublicImport",
  description: "A public import entity.",
  type: "object",
  additionalProperties: false,
  required: [
    "id",
    "created_at",
    "updated_at",
    "created_by",
    "public_id",
    "status",
    "type",
    "version",
    "title",
    "sites",
    "is_consumable",
  ],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/public-import/status.schema.json",
    },
    type: {
      const: "basic",
    },
    version: {
      const: 1,
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    is_consumable: {
      type: "boolean",
    },
  },
} as const;
addSchema(publicImportSchema as unknown as IJSONSchema);

export const validatePublicImport = createValidator<IPublicImport>(
  publicImportSchema as unknown as IJSONSchema,
);
