import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IAccountRole } from "#codegen/schema/lib/entities/account/role";

/**
 * A preview of the account.
 */
export interface IAccountPreview {
  id?: IBigSerialInteger;
  role: IAccountRole;
}
