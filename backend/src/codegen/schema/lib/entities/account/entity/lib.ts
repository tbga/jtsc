import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAccount } from "./types.js";

export const accountSchema = {
  $id: "https://jtsc-schemas.org/entities/account/entity.schema.json",
  title: "Account",
  type: "object",
  additionalProperties: false,
  required: ["role"],
  properties: {
    role: {
      $ref: "https://jtsc-schemas.org/entities/account/role.schema.json",
    },
  },
} as const;
addSchema(accountSchema as unknown as IJSONSchema);

export const validateAccount = createValidator<IAccount>(
  accountSchema as unknown as IJSONSchema,
);
