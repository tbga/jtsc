export { accountSchema, validateAccount } from "./lib.js";
export type { IAccount } from "./types.js";
