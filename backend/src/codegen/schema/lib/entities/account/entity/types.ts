import { type IAccountRole } from "#codegen/schema/lib/entities/account/role";

export interface IAccount {
  role: IAccountRole;
}
