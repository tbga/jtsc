import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAccountRole } from "./types.js";

export const accountRoleSchema = {
  $id: "https://jtsc-schemas.org/entities/account/role.schema.json",
  title: "AccountRole",
  anyOf: [
    {
      const: "user",
    },
    {
      const: "administrator",
    },
  ],
} as const;
addSchema(accountRoleSchema as unknown as IJSONSchema);

export const validateAccountRole = createValidator<IAccountRole>(
  accountRoleSchema as unknown as IJSONSchema,
);
