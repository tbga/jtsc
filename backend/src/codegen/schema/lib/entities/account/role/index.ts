export { accountRoleSchema, validateAccountRole } from "./lib.js";
export type { IAccountRole } from "./types.js";
