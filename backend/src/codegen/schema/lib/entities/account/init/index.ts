export { accountInitSchema, validateAccountInit } from "./lib.js";
export type { IAccountInit } from "./types.js";
