import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAccountInit } from "./types.js";

export const accountInitSchema = {
  $id: "https://jtsc-schemas.org/entities/account/init.schema.json",
  title: "AccountInit",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: {
      type: "string",
      minLength: 5,
      maxLength: 25,
    },
    password: {
      $comment:
        "https://github.com/kelektiv/node.bcrypt.js#security-issues-and-concerns",
      type: "string",
      minLength: 10,
      maxLength: 72,
    },
    invitation_code: {
      $ref: "http://jtsc-schemas.org/types/strings/nanoid.schema.json",
    },
  },
} as const;
addSchema(accountInitSchema as unknown as IJSONSchema);

export const validateAccountInit = createValidator<IAccountInit>(
  accountInitSchema as unknown as IJSONSchema,
);
