import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IAccountLogin } from "./types.js";

export const accountLoginSchema = {
  $id: "https://jtsc-schemas.org/entities/account/login.schema.json",
  title: "AccountLogin",
  description: "Account initializer.",
  type: "object",
  additionalProperties: false,
  required: ["login", "password"],
  properties: {
    login: {
      type: "string",
      minLength: 5,
      maxLength: 25,
    },
    password: {
      type: "string",
      minLength: 10,
      maxLength: 72,
    },
  },
} as const;
addSchema(accountLoginSchema as unknown as IJSONSchema);

export const validateAccountLogin = createValidator<IAccountLogin>(
  accountLoginSchema as unknown as IJSONSchema,
);
