/**
 * Account initializer.
 */
export interface IAccountLogin {
  login: string;
  password: string;
}
