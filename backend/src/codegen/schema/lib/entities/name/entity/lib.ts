import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IName } from "./types.js";

export const nameSchema = {
  $id: "https://jtsc-schemas.org/entities/name/entity.schema.json",
  title: "Name",
  description: "A container to hold as much name information as possible.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at", "full_name"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    full_name: {
      type: "string",
      minLength: 1,
      description: "The complete name of the name entry.",
    },
  },
} as const;
addSchema(nameSchema as unknown as IJSONSchema);

export const validateName = createValidator<IName>(
  nameSchema as unknown as IJSONSchema,
);
