export { artistSchema, validateArtist } from "./lib.js";
export type { IArtist } from "./types.js";
