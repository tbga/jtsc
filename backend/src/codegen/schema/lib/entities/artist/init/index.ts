export { artistInitSchema, validateArtistInit } from "./lib.js";
export type { IArtistInit } from "./types.js";
