import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IArtistInit } from "./types.js";

export const artistInitSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/init.schema.json",
  title: "ArtistInit",
  description: "Initializer for the artist.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
      description:
        "Is optional because the artist can hide it by using pseudonyms.",
    },
  },
} as const;
addSchema(artistInitSchema as unknown as IJSONSchema);

export const validateArtistInit = createValidator<IArtistInit>(
  artistInitSchema as unknown as IJSONSchema,
);
