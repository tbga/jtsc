export { artistUpdateSchema, validateArtistUpdate } from "./lib.js";
export type { IArtistUpdate } from "./types.js";
