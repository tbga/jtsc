import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IArtistUpdate } from "./types.js";

export const artistUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/update.schema.json",
  title: "ArtistUpdate",
  description: "An update on the artist entry.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    sex: {
      type: "boolean",
    },
  },
} as const;
addSchema(artistUpdateSchema as unknown as IJSONSchema);

export const validateArtistUpdate = createValidator<IArtistUpdate>(
  artistUpdateSchema as unknown as IJSONSchema,
);
