/**
 * An update on the artist entry.
 */
export interface IArtistUpdate {
  sex?: boolean;
}
