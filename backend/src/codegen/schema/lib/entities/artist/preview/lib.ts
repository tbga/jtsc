import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IArtistPreview } from "./types.js";

export const artistPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/artist/preview.schema.json",
  title: "ArtistPreview",
  type: "object",
  additionalProperties: false,
  required: ["id", "sites", "profiles", "posts", "releases"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    sex: {
      type: "boolean",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
addSchema(artistPreviewSchema as unknown as IJSONSchema);

export const validateArtistPreview = createValidator<IArtistPreview>(
  artistPreviewSchema as unknown as IJSONSchema,
);
