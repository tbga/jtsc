export { artistPreviewSchema, validateArtistPreview } from "./lib.js";
export type { IArtistPreview } from "./types.js";
