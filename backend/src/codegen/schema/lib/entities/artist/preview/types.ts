import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";

export interface IArtistPreview {
  id: IBigSerialInteger;
  name?: INonEmptyString;
  sex?: boolean;
  sites: IBigIntegerPositive;
  profiles: IBigIntegerPositive;
  posts: IBigIntegerPositive;
  releases: IBigIntegerPositive;
}
