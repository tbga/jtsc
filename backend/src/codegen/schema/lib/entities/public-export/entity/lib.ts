import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExport } from "./types.js";

export const publicExportSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/entity.schema.json",
  title: "PublicExport",
  description: "A public export entity.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_at", "updated_at", "created_by", "title", "status"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    updated_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
} as const;
addSchema(publicExportSchema as unknown as IJSONSchema);

export const validatePublicExport = createValidator<IPublicExport>(
  publicExportSchema as unknown as IJSONSchema,
);
