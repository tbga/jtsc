import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportUpdate } from "./types.js";

export const publicExportUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/update.schema.json",
  title: "PublicExportUpdate",
  description: "An update schema for an entity.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
  },
} as const;
addSchema(publicExportUpdateSchema as unknown as IJSONSchema);

export const validatePublicExportUpdate = createValidator<IPublicExportUpdate>(
  publicExportUpdateSchema as unknown as IJSONSchema,
);
