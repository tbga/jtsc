import { type ITitle } from "#codegen/schema/lib/types/strings/title";

/**
 * An update schema for an entity.
 */
export interface IPublicExportUpdate {
  title?: ITitle;
}
