import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportPreview } from "./types.js";

export const publicExportPreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/preview.schema.json",
  title: "PublicExportPreview",
  description: "A preview of the public export.",
  type: "object",
  additionalProperties: false,
  required: ["id", "created_by", "title", "sites", "status"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    created_by: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    status: {
      $ref: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
    },
    sites: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
} as const;
addSchema(publicExportPreviewSchema as unknown as IJSONSchema);

export const validatePublicExportPreview =
  createValidator<IPublicExportPreview>(
    publicExportPreviewSchema as unknown as IJSONSchema,
  );
