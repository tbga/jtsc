import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";

/**
 * Public export initializer.
 */
export interface IPublicExportInit {
  title: ITitle;
  /**
   * A list of published site IDs.
   *
   * @minItems 1
   */
  sites?: [IBigSerialInteger, ...IBigSerialInteger[]];
}
