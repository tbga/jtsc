import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportInit } from "./types.js";

export const publicExportInitSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/init.schema.json",
  title: "PublicExportInit",
  description: "Public export initializer.",
  type: "object",
  additionalProperties: false,
  required: ["title"],
  properties: {
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    sites: {
      description: "A list of published site IDs.",
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: {
        $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
      },
    },
  },
} as const;
addSchema(publicExportInitSchema as unknown as IJSONSchema);

export const validatePublicExportInit = createValidator<IPublicExportInit>(
  publicExportInitSchema as unknown as IJSONSchema,
);
