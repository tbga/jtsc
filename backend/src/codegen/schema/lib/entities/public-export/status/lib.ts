import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPublicExportStatus } from "./types.js";

export const publicExportStatusSchema = {
  $id: "https://jtsc-schemas.org/entities/public-export/status.schema.json",
  title: "PublicExportStatus",
  description: "A public export's status.",
  anyOf: [
    {
      const: "in-progress",
      description: "Public export is being worked on.",
    },
    {
      const: "finalized",
      description: "Public export is ready to be exported.",
    },
    {
      const: "finished",
      description: "Public export has been exported.",
    },
  ],
} as const;
addSchema(publicExportStatusSchema as unknown as IJSONSchema);

export const validatePublicExportStatus = createValidator<IPublicExportStatus>(
  publicExportStatusSchema as unknown as IJSONSchema,
);
