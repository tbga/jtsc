/**
 * A public export's status.
 */
export type IPublicExportStatus = "in-progress" | "finalized" | "finished";
