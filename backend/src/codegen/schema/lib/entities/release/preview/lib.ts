import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IReleasePreview } from "./types.js";

export const releasePreviewSchema = {
  $id: "https://jtsc-schemas.org/entities/release/preview.schema.json",
  title: "ReleasePreview",
  description: "Preview of the release.",
  type: "object",
  additionalProperties: false,
  required: ["id", "profiles", "posts"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    site: {
      $ref: "https://jtsc-schemas.org/entities/item.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    original_release_id: {
      type: "string",
    },
  },
} as const;
addSchema(releasePreviewSchema as unknown as IJSONSchema);

export const validateReleasePreview = createValidator<IReleasePreview>(
  releasePreviewSchema as unknown as IJSONSchema,
);
