import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IEntityItem } from "#codegen/schema/lib/entities/item";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";

/**
 * Preview of the release.
 */
export interface IReleasePreview {
  id: IBigSerialInteger;
  profiles: IBigIntegerPositive;
  posts: IBigIntegerPositive;
  name?: INonEmptyString;
  site?: IEntityItem;
  title?: ITitle;
  released_at?: IDateTime;
  original_release_id?: string;
}
