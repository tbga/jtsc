import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IReleaseUpdate } from "./types.js";

export const releaseUpdateSchema = {
  $id: "https://jtsc-schemas.org/entities/release/update.schema.json",
  title: "ReleaseUpdate",
  description: "An update of a release.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    original_release_id: {
      type: "string",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    description: {
      type: "string",
      minLength: 1,
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    released_at_original: {
      $ref: "http://jtsc-schemas.org/types/dates/generic.schema.json",
    },
  },
} as const;
addSchema(releaseUpdateSchema as unknown as IJSONSchema);

export const validateReleaseUpdate = createValidator<IReleaseUpdate>(
  releaseUpdateSchema as unknown as IJSONSchema,
);
