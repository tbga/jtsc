import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IDateTimeGeneric } from "#codegen/schema/lib/types/dates/generic";

/**
 * An update of a release.
 */
export interface IReleaseUpdate {
  site_id?: IBigSerialInteger;
  original_release_id?: string;
  title?: ITitle;
  description?: string;
  released_at?: IDateTime;
  released_at_original?: IDateTimeGeneric;
}
