import { type IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
import { type ITitle } from "#codegen/schema/lib/types/strings/title";
import { type INonEmptyString } from "#codegen/schema/lib/types/strings/minimal";
import { type IDateTime } from "#codegen/schema/lib/types/dates/datetime";
import { type IDateTimeGeneric } from "#codegen/schema/lib/types/dates/generic";
import { type IPostInit } from "#codegen/schema/lib/entities/post/init";

/**
 * Release initializer.
 */
export interface IReleaseInit {
  profile_id?: IBigSerialInteger;
  site_id?: IBigSerialInteger;
  title?: ITitle;
  description?: INonEmptyString;
  original_release_id?: INonEmptyString;
  released_at?: IDateTime;
  released_at_original?: IDateTimeGeneric;
  /**
   * Related profiles.
   */
  profiles?: {
    /**
     * Existing profile IDs.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
  };
  /**
   * Related posts.
   */
  posts?: {
    /**
     * Post initializers.
     *
     * @minItems 1
     */
    inits?: [IPostInit, ...IPostInit[]];
    /**
     * Existing post IDs.
     *
     * @minItems 1
     */
    ids?: [IBigSerialInteger, ...IBigSerialInteger[]];
  };
}
