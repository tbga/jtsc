import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IReleaseInit } from "./types.js";

export const releaseInitSchema = {
  $id: "https://jtsc-schemas.org/entities/release/init.schema.json",
  title: "ReleaseInit",
  description: "Release initializer.",
  type: "object",
  additionalProperties: false,
  required: [],
  properties: {
    profile_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    site_id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    title: {
      $ref: "http://jtsc-schemas.org/types/strings/title.schema.json",
    },
    description: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    original_release_id: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    released_at: {
      $ref: "http://jtsc-schemas.org/types/dates/datetime.schema.json",
    },
    released_at_original: {
      $ref: "http://jtsc-schemas.org/types/dates/generic.schema.json",
    },
    profiles: {
      description: "Related profiles.",
      type: "object",
      minProperties: 1,
      additionalProperties: false,
      properties: {
        ids: {
          description: "Existing profile IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
    posts: {
      description: "Related posts.",
      type: "object",
      minProperties: 1,
      additionalProperties: false,
      properties: {
        inits: {
          description: "Post initializers.",
          type: "array",
          minItems: 1,
          items: {
            $ref: "https://jtsc-schemas.org/entities/post/init.schema.json",
          },
        },
        ids: {
          description: "Existing post IDs.",
          type: "array",
          minItems: 1,
          uniqueItems: true,
          items: {
            $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
          },
        },
      },
    },
  },
} as const;
addSchema(releaseInitSchema as unknown as IJSONSchema);

export const validateReleaseInit = createValidator<IReleaseInit>(
  releaseInitSchema as unknown as IJSONSchema,
);
