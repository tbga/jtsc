export { entityItemSchema, validateEntityItem } from "./lib.js";
export type { IEntityItem } from "./types.js";
