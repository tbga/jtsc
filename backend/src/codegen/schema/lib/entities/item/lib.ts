import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IEntityItem } from "./types.js";

export const entityItemSchema = {
  $id: "https://jtsc-schemas.org/entities/item.schema.json",
  title: "EntityItem",
  description: "An entity's item.",
  type: "object",
  additionalProperties: false,
  required: ["id"],
  properties: {
    id: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-serial.schema.json",
    },
    name: {
      description: "A human-readable name for the entity",
      type: "string",
      minLength: 1,
    },
    public_id: {
      $ref: "http://jtsc-schemas.org/types/strings/uuid/v4.schema.json",
    },
  },
} as const;
addSchema(entityItemSchema as unknown as IJSONSchema);

export const validateEntityItem = createValidator<IEntityItem>(
  entityItemSchema as unknown as IJSONSchema,
);
