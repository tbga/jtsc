import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IFileSystem } from "./types.js";

export const fileSystemSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/system.schema.json",
  title: "FileSystem",
  description: "Information about the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["platform", "home_dir"],
  properties: {
    platform: {
      $ref: "https://jtsc-schemas.org/types/nodejs/platform.schema.json",
    },
    home_dir: {
      $ref: "https://jtsc-schemas.org/local-file-system/path.schema.json",
    },
  },
} as const;
addSchema(fileSystemSchema as unknown as IJSONSchema);

export const validateFileSystem = createValidator<IFileSystem>(
  fileSystemSchema as unknown as IJSONSchema,
);
