export { fileSystemSchema, validateFileSystem } from "./lib.js";
export type { IFileSystem } from "./types.js";
