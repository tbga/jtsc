export { entrySchema, validateEntry } from "./lib.js";
export type { IEntry } from "./types.js";
