import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IFile } from "./types.js";

export const fileSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/file.schema.json",
  title: "File",
  description: "A file on the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["name", "size"],
  properties: {
    name: {
      description: "Name of the file.",
      type: "string",
      minLength: 1,
    },
    size: {
      description: "Size of the file.",
      type: "string",
      minLength: 1,
    },
  },
} as const;
addSchema(fileSchema as unknown as IJSONSchema);

export const validateFile = createValidator<IFile>(
  fileSchema as unknown as IJSONSchema,
);
