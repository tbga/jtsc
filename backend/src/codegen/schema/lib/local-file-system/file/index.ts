export { fileSchema, validateFile } from "./lib.js";
export type { IFile } from "./types.js";
