/**
 * A `URL` file path.
 */
export type IPath = string;
