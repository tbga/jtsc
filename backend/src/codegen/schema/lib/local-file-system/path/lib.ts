import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IPath } from "./types.js";

export const pathSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/path.schema.json",
  title: "Path",
  description: "A `URL` file path.",
  type: "string",
  format: "uri",
  minLength: 1,
} as const;
addSchema(pathSchema as unknown as IJSONSchema);

export const validatePath = createValidator<IPath>(
  pathSchema as unknown as IJSONSchema,
);
