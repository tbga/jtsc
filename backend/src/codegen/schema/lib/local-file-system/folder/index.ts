export { folderSchema, validateFolder } from "./lib.js";
export type { IFolder } from "./types.js";
