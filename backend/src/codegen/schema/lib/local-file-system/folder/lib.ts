import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IFolder } from "./types.js";

export const folderSchema = {
  $id: "https://jtsc-schemas.org/local-file-system/folder.schema.json",
  title: "Folder",
  description: "A folder on the local file system.",
  type: "object",
  additionalProperties: false,
  required: ["name", "entries"],
  properties: {
    name: {
      $ref: "http://jtsc-schemas.org/types/strings/minimal.schema.json",
    },
    entries: {
      description: "Entries within the folder",
      type: "array",
      items: {
        $ref: "https://jtsc-schemas.org/local-file-system/entry.schema.json",
      },
    },
  },
} as const;
addSchema(folderSchema as unknown as IJSONSchema);

export const validateFolder = createValidator<IFolder>(
  folderSchema as unknown as IJSONSchema,
);
