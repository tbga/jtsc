import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IStatsSites } from "./types.js";

export const statsSitesSchema = {
  $id: "https://jtsc-schemas.org/stats/sites.schema.json",
  title: "StatsSites",
  description: "Stats of the sites.",
  type: "object",
  additionalProperties: false,
  required: ["all", "published", "local"],
  properties: {
    all: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    published: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    local: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
addSchema(statsSitesSchema as unknown as IJSONSchema);

export const validateStatsSites = createValidator<IStatsSites>(
  statsSitesSchema as unknown as IJSONSchema,
);
