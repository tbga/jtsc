import { createValidator, addSchema, type IJSONSchema } from "#lib/json-schema";
import { type IStatsAll } from "./types.js";

export const statsAllSchema = {
  $id: "https://jtsc-schemas.org/stats/all.schema.json",
  title: "StatsAll",
  description: "All stats of the application.",
  type: "object",
  additionalProperties: false,
  required: ["artists", "posts", "sites", "profiles", "releases"],
  properties: {
    artists: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    posts: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    sites: {
      $ref: "https://jtsc-schemas.org/stats/sites.schema.json",
    },
    profiles: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
    releases: {
      $ref: "http://jtsc-schemas.org/types/numbers/big-integer-positive.schema.json",
    },
  },
} as const;
addSchema(statsAllSchema as unknown as IJSONSchema);

export const validateStatsAll = createValidator<IStatsAll>(
  statsAllSchema as unknown as IJSONSchema,
);
