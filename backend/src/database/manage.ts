import { cwd } from "node:process";
import path from "node:path";
import type { RunnerOption } from "node-pg-migrate";
import pgMigrate from "node-pg-migrate/dist/runner.js";
import { DATABASE_URL } from "#environment/variables.js";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";
import { log } from "#lib/logs";
import { dbQuery, sqlQuery } from "./lib.js";

const migrationsFolderPath = path.join(cwd(), "src", "database", "migrations");
const migrationsOptions: RunnerOption = {
  databaseUrl: DATABASE_URL,
  dir: migrationsFolderPath,
  migrationsTable: "pgmigrations",
  direction: "up",
};
const wipeQuery = sqlQuery("manage", "wipe-database.sql");
const resetQuery = sqlQuery("manage", "reset-database.sql");

export async function initDatabase() {
  if (!IS_DEVELOPMENT) {
    await runDatabaseMigrations();
  }
}

export const wipeDatabase = dbQuery<undefined, null>(async (_, ctx) => {
  return ctx.none(wipeQuery);
});

export const resetDatabase = dbQuery<undefined, null>(async (_, ctx) => {
  return ctx.none(resetQuery);
});

export async function runDatabaseMigrations() {
  log("Starting database migrations...");

  const results = await pgMigrate.default(migrationsOptions);

  if (results.length) {
    log(`Performed ${results.length} migrations.`);
  }
}
