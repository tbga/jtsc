import {
  type IEntityResult,
  toEntityIDs,
  type IPaginatedSearchQueryArgs,
} from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IProfile } from "#entities/profile";
import type { IPost } from "#entities/post";

const query = sqlQuery("artists", "ids.sql");

interface IQueryArgs extends IPaginatedSearchQueryArgs {
  profile_ids?: IProfile["id"][];
  post_ids?: IPost["id"][];
}

export const selectArtistIDs = dbQuery<IQueryArgs, IArtist["id"][]>(
  async ({ pagination, profile_ids, post_ids, search_query }, ctx) => {
    const { limit, offset } = pagination;
    const queryArgs = {
      limit,
      offset,
      profile_ids,
      post_ids,
      search_query,
    };
    const entities = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(entities);

    return ids;
  },
);
