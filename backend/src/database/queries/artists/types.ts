import { TableName } from "#database";
import type { IArtist } from "#entities/artist";

export interface IArtistDBUpdate extends Pick<IArtist, "id" | "sex"> {}

export const artistsTable = new TableName({
  schema: "public",
  table: "artists",
});
