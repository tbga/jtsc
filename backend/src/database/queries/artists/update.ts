import { toEntityIDs, type IEntityResult } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IArtistDBUpdate } from "./types.js";

const updateArtistQuery = sqlQuery("artists", "update.sql");

export const updateArtists = dbQuery<IArtistDBUpdate[], IArtist["id"][]>(
  async (updates, ctx) => {
    const queryArgs = {
      updates,
    };
    const result = await ctx.many<IEntityResult>(updateArtistQuery, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
