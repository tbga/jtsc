SELECT
  count(*)
FROM
  artists
WHERE
  (
    ${profile_ids} IS NULL
    OR
    id IN (
      SELECT DISTINCT
        artist_id AS id
      FROM
        profile_artists
      WHERE
        profile_id = ANY (
          CAST (${profile_ids} AS bigint[])
        )
    )
  )
  AND
  (
    ${post_ids} IS NULL
    OR
    id IN (
      SELECT DISTINCT
        artist_id AS id
      FROM
        post_artists
      WHERE
        post_id = ANY (
          CAST (${post_ids} AS bigint[])
        )
    )
  )
  AND
  (
    ${search_query} IS NULL
    OR
    id IN (
      (
        SELECT DISTINCT
          profile_artists.artist_id AS id
        FROM
          profile_artists
          INNER JOIN
          profiles
          ON
            profile_artists.profile_id = profiles.id
        WHERE
          profiles.original_id &@~ ${search_query}
      )
      UNION
      (
        SELECT DISTINCT
          profile_artists.artist_id AS id
        FROM
          profile_artists
          INNER JOIN
          profile_names
          ON
            profile_artists.profile_id = profile_names.profile_id
          INNER JOIN
          names
          ON
            profile_names.name_id = names.id
        WHERE
          names.full_name &@~ ${search_query}
      )
    )
  )
