import { dbQuery, sqlQuery } from "#database";
import type { IArtist, IArtistPreview } from "#entities/artist";

const query = sqlQuery("artists", "previews.sql");

export const selectArtistPreviews = dbQuery<IArtist["id"][], IArtistPreview[]>(
  async (artist_ids, ctx) => {
    const queryArgs = {
      artist_ids,
    };

    const previews = await ctx.many<IArtistPreview>(query, queryArgs);

    return previews;
  },
);
