WITH input_updates AS (
  SELECT
    artist_update.*
  FROM
    json_to_recordset(${updates:json}) AS artist_update(
      id bigint,
      sex boolean
    )
),
updated_artists AS (
  UPDATE artists
  SET
    sex = update_if_changed(input_updates.sex, artists.sex)
  FROM
    input_updates
  WHERE
    input_updates.id = artists.id
  RETURNING
    artists.id
)
SELECT
  id
FROM
  updated_artists
;
