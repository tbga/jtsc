-- collect input artists
WITH input_artists AS (
  SELECT
    id,
    created_at,
    updated_at,
    sex
  FROM
    artists
  WHERE
    id = ANY (
      CAST (${artist_ids} AS bigint[])
    )
),
-- collect input artist profiles
input_artist_profiles AS (
  SELECT
    profile_artists.artist_id,
    profile_artists.profile_id
  FROM
    input_artists
    INNER JOIN
    profile_artists
    ON
      input_artists.id = profile_artists.artist_id
),
-- collect artist names
artist_items AS (
  -- collect input profile names
  WITH input_profile_names AS (
    SELECT DISTINCT ON ( profile_names.profile_id )
      profile_names.profile_id,
      names.full_name AS name
    FROM
      input_artist_profiles
      INNER JOIN
      profile_names
      ON
        input_artist_profiles.profile_id = profile_names.profile_id
      INNER JOIN
      names
      ON
        profile_names.name_id = names.id
    -- order in a way so the lowest profile `name_id` will be picked
    ORDER BY
      profile_names.profile_id ASC,
      profile_names.id ASC
  )
  SELECT
    input_artist_profiles.artist_id AS id,
    input_profile_names.name
  FROM
    input_artist_profiles
    INNER JOIN
    input_profile_names
    ON
      input_artist_profiles.profile_id = input_profile_names.profile_id
),
output_artists AS (
  SELECT
    input_artists.id,
    input_artists.created_at,
    input_artists.updated_at,
    input_artists.sex,
    artist_items.name
  FROM
    input_artists
    LEFT JOIN
    artist_items
    ON
      input_artists.id = artist_items.id
)
SELECT
  id,
  created_at,
  updated_at,
  name,
  sex
FROM
  output_artists
ORDER BY
  id ASC
