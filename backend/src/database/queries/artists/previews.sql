WITH input_artists AS (
  SELECT
    id,
    sex
  FROM
    artists
  WHERE
    id = ANY (
      CAST (${artist_ids} AS bigint[])
    )
),
input_artist_profiles AS (
  SELECT
    profile_artists.artist_id,
    profile_artists.profile_id
  FROM
    input_artists
    INNER JOIN
    profile_artists
    ON
      input_artists.id = profile_artists.artist_id
),
-- collect artist names
artist_items AS (
  -- collect input profile names
  WITH input_profile_names AS (
    SELECT DISTINCT ON ( profile_names.profile_id )
      profile_names.profile_id,
      names.full_name AS name
    FROM
      input_artist_profiles
      INNER JOIN
      profile_names
      ON
        input_artist_profiles.profile_id = profile_names.profile_id
      INNER JOIN
      names
      ON
        profile_names.name_id = names.id
    -- order in a way so the lowest profile `name_id` will be picked
    ORDER BY
      profile_names.profile_id ASC,
      profile_names.id ASC
  )
  SELECT
    input_artist_profiles.artist_id AS id,
    input_profile_names.name
  FROM
    input_artist_profiles
    INNER JOIN
    input_profile_names
    ON
      input_artist_profiles.profile_id = input_profile_names.profile_id
),
artist_profile_counts AS (
  SELECT
    input_artist_profiles.artist_id,
    count(input_artist_profiles.profile_id) AS profiles
  FROM
    input_artist_profiles
  GROUP BY
    input_artist_profiles.artist_id
),
-- select artist site counts
artist_site_counts AS (
  SELECT
    input_artist_profiles.artist_id,
    count(DISTINCT profiles.site_id) AS sites
  FROM
    input_artist_profiles
    INNER JOIN
    profiles
    ON
      input_artist_profiles.profile_id = profiles.id
  GROUP BY
    input_artist_profiles.artist_id
),
artist_release_counts AS (
  SELECT
    input_artist_profiles.artist_id,
    count(release_profiles.id) AS releases
  FROM
    input_artist_profiles
    INNER JOIN
    release_profiles
    ON
      input_artist_profiles.profile_id = release_profiles.profile_id
  GROUP BY
    input_artist_profiles.artist_id
),
artist_posts_counts AS (
  SELECT
    input_artist_profiles.artist_id,
    count(DISTINCT release_posts.post_id) AS posts
  FROM
    input_artist_profiles
    INNER JOIN
    release_profiles
    ON
      input_artist_profiles.profile_id = release_profiles.profile_id
    INNER JOIN
    release_posts
    ON
      release_profiles.release_id = release_posts.release_id
  GROUP BY
    input_artist_profiles.artist_id
),
output_artists AS (
  SELECT DISTINCT ON (input_artists.id)
    input_artists.id,
    input_artists.sex,
    artist_items.name,
    COALESCE (artist_site_counts.sites, 0) AS sites,
    COALESCE (artist_profile_counts.profiles, 0) AS profiles,
    COALESCE (artist_release_counts.releases, 0) AS releases,
    COALESCE (artist_posts_counts.posts, 0) AS posts
  FROM
    input_artists
    LEFT JOIN
    artist_items
    ON
      input_artists.id = artist_items.id
    LEFT JOIN
    artist_profile_counts
    ON
      input_artists.id = artist_profile_counts.artist_id
    LEFT JOIN
    artist_site_counts
    ON
      input_artists.id = artist_site_counts.artist_id
    LEFT JOIN
    artist_release_counts
    ON
      input_artists.id = artist_release_counts.artist_id
    LEFT JOIN
    artist_posts_counts
    ON
      input_artists.id = artist_posts_counts.artist_id
)
SELECT
  id,
  name,
  sex,
  sites,
  profiles,
  posts,
  releases
FROM
  output_artists
ORDER BY
  id ASC
