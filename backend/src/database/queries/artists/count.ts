import type { ISearchQueryArgs } from "#lib/entities";
import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import type { IBigIntegerPositive } from "#types";
import type { IPost } from "#entities/post";

interface IArgs extends ISearchQueryArgs {
  profile_ids?: IProfile["id"][];
  post_ids?: IPost["id"][];
}

const query = sqlQuery("artists", "count.sql");

export const selectArtistCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ profile_ids, post_ids, search_query }, ctx) => {
    const queryArgs = {
      profile_ids,
      post_ids,
      search_query,
    };

    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
