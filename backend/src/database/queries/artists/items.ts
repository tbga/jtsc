import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IEntityItem } from "#lib/entities";

const query = sqlQuery("artists", "items.sql");

export const selectArtistItems = dbQuery<IArtist["id"][], IEntityItem[]>(
  async (artist_ids, ctx) => {
    const queryArgs = {
      artist_ids,
    };
    const entities = await ctx.many<IEntityItem>(query, queryArgs);

    return entities;
  },
);
