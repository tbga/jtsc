import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";

const query = sqlQuery("artists", "entities.sql");

export const selectArtistEntities = dbQuery<IArtist["id"][], IArtist[]>(
  async (artist_ids, ctx) => {
    const queryArgs = {
      artist_ids,
    };
    const entities = await ctx.many<IArtist>(query, queryArgs);

    return entities;
  },
);
