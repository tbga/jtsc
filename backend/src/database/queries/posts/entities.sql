WITH input_posts AS (
  SELECT
    id,
    created_at,
    updated_at
  FROM
    posts
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
post_titles AS (
  SELECT DISTINCT ON (input_posts.id)
    input_posts.id AS post_id,
    releases.title
  FROM
    input_posts
    INNER JOIN
    release_posts
    ON
      input_posts.id = release_posts.post_id
    INNER JOIN
    releases
    ON
      release_posts.release_id = releases.id
  ORDER BY
    input_posts.id ASC,
    release_posts.id ASC
),
output_posts AS (
  SELECT
    input_posts.id,
    input_posts.created_at,
    input_posts.updated_at,
    post_titles.title AS name,
    post_titles.title
  FROM
    input_posts
    LEFT JOIN
    post_titles
    ON
      input_posts.id = post_titles.post_id
)
SELECT
  id,
  created_at,
  updated_at,
  name,
  title
FROM
  output_posts
ORDER BY
  id ASC
;
