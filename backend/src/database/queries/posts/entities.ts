import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";

const query = sqlQuery("posts", "entities.sql");

export const selectPostEntities = dbQuery<IPost["id"][], IPost[]>(
  async (ids, ctx) => {
    const queryArgs = { ids };

    const entities = await ctx.many<IPost>(query, queryArgs);

    return entities;
  },
);
