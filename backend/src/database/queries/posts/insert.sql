WITH input_inits AS (
  SELECT
    nextval(pg_get_serial_sequence('public.posts', 'id')) AS post_id,
    post_init.*
  FROM
    json_to_recordset(${inits:json}) AS post_init(
      artist_inits json,
      artist_ids bigint[]
    )
),
post_inits AS (
  SELECT
    post_id
  FROM
    input_inits
),
artist_inits AS (
  SELECT
    input_inits.post_id,
    nextval(pg_get_serial_sequence('public.artists', 'id')) AS artist_id,
    artist_init.*
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.artist_inits) AS artist_init(
      sex boolean,
      born_at timestamptz,
      died_at timestamptz
    )
),
post_artists_inits AS (
  (
    SELECT
      input_inits.post_id,
      artist_data.artist_id
    FROM
      input_inits
      CROSS JOIN
      UNNEST(input_inits.artist_ids) AS artist_data(
        artist_id
      )
  )
  UNION ALL
  (
    SELECT
      artist_inits.post_id,
      artist_inits.artist_id
    FROM
      artist_inits
  )
),
new_posts AS (
  INSERT INTO posts
    (
      id
    ) OVERRIDING SYSTEM VALUE
  SELECT
    post_id AS id
  FROM
    post_inits
  RETURNING
    posts.id
),
new_artists AS (
  INSERT INTO artists
    (
      id,
      sex
    ) OVERRIDING SYSTEM VALUE
  SELECT
    artist_id AS id,
    sex
  FROM
    artist_inits
  RETURNING
    artists.id
),
new_post_artists AS (
  INSERT INTO post_artists
    (
      post_id,
      artist_id
    )
  SELECT
    post_id,
    artist_id
  FROM
    post_artists_inits
  RETURNING
    post_artists.id,
    post_artists.post_id,
    post_artists.artist_id
)
SELECT
  id
FROM
  new_posts
ORDER BY
  id ASC
;
