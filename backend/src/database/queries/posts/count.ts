import { dbQuery, sqlQuery } from "#database";
import type { ITableCount } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";
import type { IPostQueryArgs } from "./types.js";

interface IArgs extends Omit<IPostQueryArgs, "pagination"> {}

const query = sqlQuery("posts", "count.sql");

export const selectPostCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ release_ids, search_query }, ctx) => {
    const queryArgs = { release_ids, search_query };

    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
