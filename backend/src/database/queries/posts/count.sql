SELECT
  count(*)
FROM
  posts
WHERE
  (
    ${release_ids} IS NULL
    OR
    id IN (
      SELECT DISTINCT
        post_id AS id
      FROM
        release_posts
      WHERE
        release_id = ANY (
          CAST (${release_ids} AS bigint[])
        )
    )
  )
  AND
  (
    ${search_query} IS NULL
    OR
    id IN (
      SELECT DISTINCT
        release_posts.post_id AS id
      FROM
        release_posts
        INNER JOIN
        releases
        ON
          release_posts.release_id = releases.id
      WHERE
        (
          releases.title &@~ ${search_query}
          OR
          releases.original_release_id &@~ ${search_query}
          OR
          releases.description &@~ ${search_query}
        )
    )
  )
;
