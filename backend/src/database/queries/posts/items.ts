import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";

const query = sqlQuery("posts", "items.sql");

export const selectPostItems = dbQuery<IPost["id"][], IEntityItem[]>(
  async (ids, ctx) => {
    const queryArgs = {
      ids,
    };

    const items = await ctx.many<IEntityItem>(query, queryArgs);

    return items;
  },
);
