import type { IPostArgs } from "#entities/post";
import type { IReleaseArgs, IRelease, IReleaseInit } from "#entities/release";

export interface IPostReleaseDBInit extends IPostArgs {
  release_ids?: IRelease["id"][];
  release_inits?: IReleaseInit[];
}

export interface IPostReleaseDBDelete extends IPostArgs, IReleaseArgs {}
