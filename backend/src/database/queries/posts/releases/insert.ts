import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IRelease } from "#entities/release";
import type { IPostReleaseDBInit } from "./types.js";

interface IResult {
  posts: IPost["id"][];
  releases: IRelease["id"][];
  post_releases: {
    id: IEntity["id"];
    post_id: IPost["id"];
    release_id: IRelease["id"];
  }[];
}

const query = sqlQuery("posts", "releases", "insert.sql");

export const insertPostReleases = dbQuery<IPostReleaseDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
