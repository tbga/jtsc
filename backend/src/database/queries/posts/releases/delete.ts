import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IRelease } from "#entities/release";
import type { IEntity } from "#lib/entities";
import type { IPostReleaseDBDelete } from "./types.js";

interface IResult {
  posts: IPost["id"][];
  releases: IRelease["id"][];
  post_releases: ({
    id: IEntity["id"];
  } & IPostReleaseDBDelete)[];
}

const query = sqlQuery("posts", "releases", "delete.sql");

export const deletePostReleases = dbQuery<IPostReleaseDBDelete[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
