WITH input_deletes AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS release_delete(
      post_id bigint,
      release_id bigint
    )
),
deleted_post_releases AS (
  DELETE
  FROM
    release_posts
  WHERE
    id IN (
      SELECT
        id
      FROM
        input_deletes
        INNER JOIN
        release_posts
        ON
          input_deletes.post_id = release_posts.post_id
          AND
          input_deletes.release_id = release_posts.release_id
    )
  RETURNING
    release_posts.id,
    release_posts.post_id,
    release_posts.release_id
),
result AS (
  SELECT
    ARRAY(
      SELECT DISTINCT
        post_id
      FROM
        deleted_post_releases
    ) AS posts,
    ARRAY(
      SELECT DISTINCT
        release_id
      FROM
        deleted_post_releases
    ) AS releases,
    (
      SELECT json_agg(items)
      FROM
        (
          SELECT
            CAST (id AS text) AS id,
            CAST (post_id AS text) AS post_id,
            CAST (release_id AS text) AS release_id
          FROM
            deleted_post_releases
        ) AS items
    ) AS post_releases
)
SELECT
  posts,
  releases,
  post_releases
FROM
  result
