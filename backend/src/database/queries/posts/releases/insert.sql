WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      post_id bigint,
      release_ids bigint[],
      release_inits json
    )
),
release_inits AS (
  SELECT
    input_inits.post_id,
    nextval(pg_get_serial_sequence('public.releases', 'id')) AS release_id,
    release_init.*
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.release_inits) AS release_init(
      site_id bigint,
      profile_id bigint,
      title text,
      description text,
      original_release_id text,
      released_at timestamptz,
      last_change_at timestamptz,
      taken_down_at timestamptz
    )
),
post_release_inits AS (
  (
    SELECT
      input_inits.post_id,
      release_data.release_id
    FROM
      input_inits
      CROSS JOIN
      UNNEST(input_inits.release_ids) AS release_data(
        release_id
      )
  )
  UNION ALL
  (
    SELECT
      post_id,
      release_id
    FROM
      release_inits
  )
),
new_releases AS (
  INSERT INTO releases
    (
      id,
      site_id,
      profile_id,
      title,
      description,
      original_release_id,
      released_at,
      last_change_at,
      taken_down_at
    ) OVERRIDING SYSTEM VALUE
  SELECT
    release_id AS id,
    site_id,
    profile_id,
    title,
    description,
    original_release_id,
    released_at,
    last_change_at,
    taken_down_at
  FROM
    release_inits
),
new_post_releases AS (
  INSERT INTO release_posts
    (
      post_id,
      release_id
    )
  SELECT
    post_id,
    release_id
  FROM
    post_release_inits
  RETURNING
    release_posts.id,
    release_posts.post_id,
    release_posts.release_id
),
result AS (
  SELECT
    ARRAY(
      SELECT DISTINCT
        post_id
      FROM
        post_release_inits
    ) AS posts,
    ARRAY(
      SELECT DISTINCT
        release_id
      FROM
        post_release_inits
    ) AS releases,
    (
      SELECT json_agg(items)
      FROM
        (
          SELECT
            CAST (id AS text) AS id,
            CAST (post_id AS text) AS post_id,
            CAST (release_id AS text) AS release_id
          FROM
            new_post_releases
        ) AS items
    ) AS post_releases
)
SELECT
  posts,
  releases,
  post_releases
FROM
  result
