import { TableName } from "#database";
import type { IPaginatedSearchQueryArgs } from "#lib/entities";
import type { IArtistInit, IArtist } from "#entities/artist";
import type { IPost, IPostInit } from "#entities/post";
import type { IRelease } from "#entities/release";

export interface IPostDBInit extends Omit<IPostInit, "artists"> {
  artist_ids?: IArtist["id"][];
  artist_inits?: IArtistInit[];
}

export interface IPostQueryArgs extends IPaginatedSearchQueryArgs {
  release_ids?: IRelease["id"][];
}

export const postsTable = new TableName({
  schema: "public",
  table: "posts",
});
