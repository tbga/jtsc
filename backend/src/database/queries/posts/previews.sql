WITH input_posts AS (
  SELECT
    id
  FROM
    posts
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
post_titles AS (
  SELECT DISTINCT ON (input_posts.id)
    input_posts.id,
    releases.title
  FROM
    input_posts
    INNER JOIN
    release_posts
    ON
      input_posts.id = release_posts.post_id
    INNER JOIN
    releases
    ON
      release_posts.release_id = releases.id
  ORDER BY
    input_posts.id ASC,
    release_posts.id ASC
),
release_counts AS (
  SELECT
    input_posts.id,
    count(release_posts.release_id) AS release_count
  FROM
    input_posts
    INNER JOIN
    release_posts
    ON
      input_posts.id = release_posts.post_id
  GROUP BY
    input_posts.id
),
artist_counts AS (
  SELECT
    input_posts.id,
    count(post_artists.artist_id) AS artist_count
  FROM
    input_posts
    INNER JOIN
    post_artists
    ON
      input_posts.id = post_artists.post_id
  GROUP BY
    input_posts.id
),
output_posts AS (
  SELECT
    input_posts.id,
    COALESCE (release_counts.release_count, 0) AS releases,
    COALESCE (artist_counts.artist_count, 0) AS artists,
    post_titles.title,
    post_titles.title AS name
  FROM
    input_posts
    LEFT JOIN
    release_counts
    ON
      input_posts.id = release_counts.id
    LEFT JOIN
    artist_counts
    ON
      input_posts.id = artist_counts.id
    LEFT JOIN
    post_titles
    ON
      input_posts.id = post_titles.id
)
SELECT
  id,
  releases,
  artists,
  name,
  title
FROM
  output_posts
ORDER BY
  id ASC
;
