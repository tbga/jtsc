import type { IArtist } from "#entities/artist";
import type { IPost } from "#entities/post";

export interface IPostArtistDBInit {
  post_id: IPost["id"];
  artist_id: IArtist["id"];
}
