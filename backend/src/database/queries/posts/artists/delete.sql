WITH input_deletes AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS post_delete(
      post_id bigint,
      artist_id bigint
    )
),
deleted_post_artists AS (
  DELETE
  FROM
    post_artists
  WHERE
    id IN (
      SELECT
        id
      FROM
        input_deletes
        INNER JOIN
        post_artists
        ON
          input_deletes.post_id = post_artists.post_id
          AND
          input_deletes.artist_id = post_artists.artist_id
    )
  RETURNING
    post_artists.id,
    post_artists.post_id,
    post_artists.artist_id
)
SELECT
  ARRAY(
    SELECT
      post_id
    FROM
      input_deletes
  ) AS posts,
  ARRAY(
    SELECT
      artist_id
    FROM
      input_deletes
  ) AS artists,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (post_id AS text) AS post_id,
          CAST (artist_id AS text) AS artist_id
        FROM
          deleted_post_artists
      ) AS items
  ) AS post_artists
