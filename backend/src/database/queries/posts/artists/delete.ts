import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IRelease } from "#entities/release";
import type { IEntity } from "#lib/entities";
import type { IPostArtistDBInit } from "./types.js";

interface IResult {
  posts: IPost["id"][];
  artists: IRelease["id"][];
  post_artists: {
    id: IEntity["id"];
    post_id: IPost["id"];
    artist_id: IRelease["id"];
  }[];
}

const query = sqlQuery("posts", "artists", "delete.sql");

export const deletePostArtists = dbQuery<IPostArtistDBInit[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
