import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IPost } from "#entities/post";
import type { IEntity } from "#lib/entities";
import type { IPostArtistDBInit } from "./types.js";

interface IResult {
  posts: IPost["id"][];
  artists: IArtist["id"][];
  post_artists: {
    id: IEntity["id"];
    post_id: IPost["id"];
    artist_id: IArtist["id"];
  }[];
}

const query = sqlQuery("posts", "artists", "insert.sql");

export const insertPostArtists = dbQuery<IPostArtistDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
