WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS post_init(
      post_id bigint,
      artist_id bigint
    )
),
new_post_artists AS (
  INSERT INTO post_artists
    (
      post_id,
      artist_id
    )
  SELECT
    post_id,
    artist_id
  FROM
    input_inits
  RETURNING
    post_artists.id,
    post_artists.post_id,
    post_artists.artist_id
)
SELECT
  ARRAY(
    SELECT
      post_id
    FROM
      input_inits
  ) AS posts,
  ARRAY(
    SELECT
      artist_id
    FROM
      input_inits
  ) AS artists,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (post_id AS text) AS post_id,
          CAST (artist_id AS text) AS artist_id
        FROM
          new_post_artists
      ) AS items
  ) AS post_artists
