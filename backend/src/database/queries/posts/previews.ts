import { dbQuery, sqlQuery } from "#database";
import type { IPost, IPostPreview } from "#entities/post";

const query = sqlQuery("posts", "previews.sql");

export const selectPostPreviews = dbQuery<IPost["id"][], IPostPreview[]>(
  async (ids, ctx) => {
    const queryArgs = {
      ids,
    };

    const previews = await ctx.many<IPostPreview>(query, queryArgs);

    return previews;
  },
);
