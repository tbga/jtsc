import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IPostQueryArgs } from "./types.js";

const query = sqlQuery("posts", "ids.sql");

export const selectPostIDs = dbQuery<IPostQueryArgs, IPost["id"][]>(
  async ({ pagination, release_ids, search_query }, ctx) => {
    const { limit, offset } = pagination;
    const queryArgs = { limit, offset, release_ids, search_query };

    const result = await ctx.manyOrNone<IPost>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
