import { type IEntityResult, toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IPostDBInit } from "./types.js";

const query = sqlQuery("posts", "insert.sql");

export const insertPosts = dbQuery<IPostDBInit[], IPost["id"][]>(
  async (inits, ctx) => {
    const queryArgs = { inits };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
