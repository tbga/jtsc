import { dbQuery, sqlQuery } from "#database";
import type { IAccountAuth, IAccountLogin } from "#entities/account";

const query = sqlQuery("accounts", "auth.sql");

export const selectAccountAuth = dbQuery<IAccountLogin["login"], IAccountAuth>(
  async (login, ctx) => {
    const queryArgs = {
      login,
    };

    const auth = await ctx.one<IAccountAuth>(query, queryArgs);

    return auth;
  },
);
