import { dbQuery, sqlQuery } from "#database";
import type { IAccountDB } from "#database/queries/accounts";
import type { IAccountAuth } from "#entities/account";

const query = sqlQuery("accounts", "entities.sql");

export const selectAccountEntities = dbQuery<
  IAccountAuth["id"][],
  IAccountDB[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const entities = await ctx.many<IAccountDB>(query, queryArgs);

  return entities;
});
