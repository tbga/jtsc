import type { ISearchQueryArgs } from "#lib/entities";
import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";

interface IArgs extends ISearchQueryArgs {}

const query = sqlQuery("accounts", "count.sql");

export const selectAccountCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ search_query }, ctx) => {
    const queryArgs = {
      search_query,
    };

    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
