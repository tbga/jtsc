import { dbQuery, sqlQuery } from "#database";
import type { IAccountDB, IAccountDBInit } from "./types.js";

const query = sqlQuery("accounts", "insert.sql");

export const insertAccounts = dbQuery<IAccountDBInit[], IAccountDB["id"][]>(
  async (inits, ctx) => {
    const queryArgs = {
      inits,
    };
    const results = await ctx.many<{ id: IAccountDB["id"] }>(query, queryArgs);
    const ids = results.map(({ id }) => id);

    return ids;
  },
);
