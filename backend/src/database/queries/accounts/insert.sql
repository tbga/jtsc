WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS input_init(
      login text,
      password text,
      role text
    )
)
INSERT INTO accounts
  (
    login,
    password,
    role
  )
SELECT
  login,
  password,
  role
FROM
  input_inits
RETURNING
  id
;
