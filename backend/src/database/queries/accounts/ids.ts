import {
  toEntityIDs,
  type IEntityResult,
  type IPaginatedSearchQueryArgs,
} from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IAccountDB } from "./types.js";

const query = sqlQuery("accounts", "ids.sql");

interface IQueryArgs extends IPaginatedSearchQueryArgs {}

export const selectAccountIDs = dbQuery<IQueryArgs, IAccountDB["id"][]>(
  async ({ pagination, search_query }, ctx) => {
    const { limit, offset } = pagination;
    const queryArgs = {
      limit,
      offset,
      search_query,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
