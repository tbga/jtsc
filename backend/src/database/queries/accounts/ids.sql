WITH input_accounts AS (
  SELECT
    id
  FROM
    accounts
  WHERE
    (
      ${search_query} IS NULL
      OR
      login &@~ ${search_query}
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_accounts
ORDER BY
  id ASC
;
