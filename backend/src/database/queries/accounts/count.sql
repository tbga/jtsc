SELECT
  count(*)
FROM
  accounts
WHERE
  ${search_query} IS NULL
  OR
  login &@~ ${search_query}
