import type {
  IAccount,
  IAccountAuth,
  IAccountInit,
  IAccountRole,
} from "#entities/account";

export interface IAccountDBInit extends IAccountInit {
  role: IAccountRole;
}

export interface IAccountDB extends Pick<IAccountAuth, "id">, IAccount {}
