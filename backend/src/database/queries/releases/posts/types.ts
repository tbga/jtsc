import type { IPost } from "#entities/post";
import type { IRelease } from "#entities/release";

export interface IReleasePostDBInit {
  release_id: IRelease["id"];
  post_id: IPost["id"];
}
