WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      release_id bigint,
      post_id bigint
    )
),
new_release_posts AS (
  INSERT INTO release_posts
    (
      release_id,
      post_id
    )
  SELECT
    release_id,
    post_id
  FROM
    input_inits
  RETURNING
    release_posts.id,
    release_posts.release_id,
    release_posts.post_id
)
SELECT
  ARRAY(
    SELECT DISTINCT
      release_id
    FROM
      input_inits
  ) AS releases,
  ARRAY(
    SELECT DISTINCT
      post_id
    FROM
      input_inits
  ) AS posts,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (release_id AS text) AS release_id,
          CAST (post_id AS text) AS post_id
        FROM
          new_release_posts
      ) AS items
  ) AS release_posts
