import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPost } from "#entities/post";
import type { IRelease } from "#entities/release";
import type { IReleasePostDBInit } from "./types.js";

interface IResult {
  releases: IRelease["id"][];
  posts: IPost["id"][];
  release_posts: {
    id: IEntity["id"];
    release_id: IRelease["id"];
    post_id: IPost["id"];
  }[];
}

const query = sqlQuery("releases", "posts", "insert.sql");

export const insertReleasePosts = dbQuery<IReleasePostDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
