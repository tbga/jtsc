import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IPost } from "#entities/post";
import type { IReleasePostDBInit } from "./types.js";

interface IResult {
  releases: IRelease["id"][];
  posts: IPost["id"][];
  release_posts: ({ id: IEntity["id"] } & IReleasePostDBInit)[];
}

const query = sqlQuery("releases", "posts", "delete.sql");

export const deleteReleasePosts = dbQuery<IReleasePostDBInit[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
