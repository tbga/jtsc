WITH delete_inputs AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS delete_input(
      release_id bigint,
      post_id bigint
    )
),
deleted_release_posts AS (
  DELETE
  FROM
    release_posts
  WHERE
    id IN (
      SELECT
        id
      FROM
        delete_inputs
        INNER JOIN
        release_posts
        ON
          delete_inputs.release_id = release_posts.release_id
          AND
          delete_inputs.post_id = release_posts.post_id
    )
  RETURNING
    *
)
SELECT
  ARRAY(
    SELECT DISTINCT
      release_id
    FROM
      delete_inputs
  ) AS releases,
  ARRAY(
    SELECT DISTINCT
      post_id
    FROM
      delete_inputs
  ) AS posts,
  (
    SELECT json_agg(items)
    FROM (
      SELECT
        CAST (deleted_release_posts.id AS text) AS id,
        CAST (deleted_release_posts.release_id AS text) AS release_id,
        CAST (deleted_release_posts.post_id AS text) AS post_id
      FROM
        deleted_release_posts
    ) AS items
  ) as release_posts
