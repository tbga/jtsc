import { dbQuery, sqlQuery } from "#database";
import type { IRelease, IReleasePreview } from "#entities/release";

const query = sqlQuery("releases", "previews.sql");

export const selectReleasePreviews = dbQuery<
  IRelease["id"][],
  IReleasePreview[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const previews = await ctx.many<IReleasePreview>(query, queryArgs);

  return previews;
});
