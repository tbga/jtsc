import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";

const query = sqlQuery("releases", "items.sql");

export const selectReleaseItems = dbQuery<IRelease["id"][], IEntityItem[]>(
  async (ids, ctx) => {
    const queryArgs = {
      ids,
    };

    const items = await ctx.many<IEntityItem>(query, queryArgs);

    return items;
  },
);
