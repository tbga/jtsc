WITH input_updates AS (
  SELECT
    *
  FROM
    json_to_recordset(${updates:json}) AS release_update(
      id bigint,
      site_id bigint,
      original_release_id text,
      title text,
      description text,
      released_at timestamptz,
      released_at_original text
    )
),
updated_releases AS (
  UPDATE
    releases
  SET
    site_id = COALESCE(NULLIF(input_updates.site_id, NULL), releases.site_id),
    original_release_id = COALESCE(NULLIF(input_updates.original_release_id, NULL), releases.original_release_id),
    title = COALESCE(NULLIF(input_updates.title, NULL), releases.title),
    description = COALESCE(NULLIF(input_updates.description, NULL), releases.description),
    released_at = COALESCE(NULLIF(input_updates.released_at, NULL), releases.released_at),
    released_at_original = COALESCE(NULLIF(input_updates.released_at_original, NULL), releases.released_at_original)
  FROM
    input_updates
  WHERE
    input_updates.id = releases.id
  RETURNING
    releases.id
)
SELECT
    id
  FROM
    updated_releases
  ORDER BY
    id ASC
