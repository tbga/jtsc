import { TableName } from "#database";
import type { IPaginatedSearchQueryArgs } from "#lib/entities";
import type { IFileDBInit } from "#database/queries/files";
import type { IFile } from "#entities/file";
import type { IReleaseUpdate, IRelease, IReleaseInit } from "#entities/release";
import type { IPost, IPostInit } from "#entities/post";
import type { IProfile } from "#entities/profile";
import type { ISite } from "#entities/site";

export interface IReleaseDBInit
  extends Omit<IReleaseInit, "posts" | "previews" | "profiles"> {
  profile_ids?: IProfile["id"][];
  post_inits?: IPostInit[];
  post_ids?: IPost["id"][];
  preview_inits?: IFileDBInit[];
  preview_ids?: IFile["id"][];
}

export interface IReleaseDBUpdate extends IReleaseUpdate {
  id: IProfile["id"];
}

export interface IReleaseFileInitDB {
  release_id: IRelease["id"];
  file_id: IFile["id"];
}

export interface IReleaseQueryArgs
  extends Omit<IPaginatedSearchQueryArgs, "is_merged_included"> {
  release_ids?: IRelease["id"][];
  profile_ids?: IProfile["id"][];
  post_ids?: IPost["id"][];
  site_ids?: ISite["id"][];
}

export const releasesTable = new TableName({
  schema: "public",
  table: "releases",
});
