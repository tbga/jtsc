import { type IEntityResult, toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IReleaseDBInit } from "./types.js";

const query = sqlQuery("releases", "insert.sql");

export const insertReleases = dbQuery<IReleaseDBInit[], IRelease["id"][]>(
  async (release_inits, ctx) => {
    const queryArgs = { release_inits };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
