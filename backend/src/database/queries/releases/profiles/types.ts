import type { IProfile } from "#entities/profile";
import type { IRelease } from "#entities/release";

export interface IReleaseProfileDBInit {
  release_id: IRelease["id"];
  profile_id: IProfile["id"];
}
