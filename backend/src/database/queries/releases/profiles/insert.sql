WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      release_id bigint,
      profile_id bigint
    )
),
new_release_profiles AS (
  INSERT INTO release_profiles
    (
      release_id,
      profile_id
    )
  SELECT
    release_id,
    profile_id
  FROM
    input_inits
  RETURNING
    release_profiles.id,
    release_profiles.release_id,
    release_profiles.profile_id
)
SELECT
  ARRAY(
    SELECT DISTINCT
      release_id
    FROM
      input_inits
  ) AS releases,
  ARRAY(
    SELECT DISTINCT
      profile_id
    FROM
      input_inits
  ) AS profiles,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (release_id AS text) AS release_id,
          CAST (profile_id AS text) AS profile_id
        FROM
          new_release_profiles
      ) AS items
  ) AS release_profiles
