WITH delete_inputs AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS delete_input(
      release_id bigint,
      profile_id bigint
    )
),
deleted_release_profiles AS (
  DELETE
  FROM
    release_profiles
  WHERE
    id IN (
      SELECT
        id
      FROM
        delete_inputs
        INNER JOIN
        release_profiles
        ON
          delete_inputs.release_id = release_profiles.release_id
          AND
          delete_inputs.profile_id = release_profiles.profile_id
    )
  RETURNING
    *
)
SELECT
  ARRAY(
    SELECT DISTINCT
      release_id
    FROM
      delete_inputs
  ) AS releases,
  ARRAY(
    SELECT DISTINCT
      profile_id
    FROM
      delete_inputs
  ) AS profiles,
  (
    SELECT json_agg(items)
    FROM (
      SELECT
        CAST (deleted_release_profiles.id AS text) AS id,
        CAST (deleted_release_profiles.release_id AS text) AS release_id,
        CAST (deleted_release_profiles.profile_id AS text) AS profile_id
      FROM
        deleted_release_profiles
    ) AS items
  ) as release_profiles
