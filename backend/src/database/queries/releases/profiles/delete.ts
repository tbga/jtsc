import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IProfile } from "#entities/profile";
import type { IReleaseProfileDBInit } from "./types.js";

interface IResult {
  releases: IRelease["id"][];
  profiles: IProfile["id"][];
  release_profiles: ({ id: IEntity["id"] } & IReleaseProfileDBInit)[];
}

const query = sqlQuery("releases", "profiles", "delete.sql");

export const deleteReleaseProfiles = dbQuery<IReleaseProfileDBInit[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
