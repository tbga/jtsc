import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IProfile } from "#entities/profile";
import type { IReleaseProfileDBInit } from "./types.js";

interface IResult {
  releases: IRelease["id"][];
  profiles: IProfile["id"][];
  release_posts: {
    id: IEntity["id"];
    release_id: IRelease["id"];
    profile_id: IProfile["id"];
  }[];
}

const query = sqlQuery("releases", "profiles", "insert.sql");

export const insertReleaseProfiles = dbQuery<IReleaseProfileDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };
    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
