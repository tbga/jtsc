import { toEntityIDs, type IEntityResult } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IReleaseDBUpdate } from "./types.js";

const query = sqlQuery("releases", "update.sql");

export const updateReleases = dbQuery<IReleaseDBUpdate[], IRelease["id"][]>(
  async (updates, ctx) => {
    const queryArgs = { updates };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
