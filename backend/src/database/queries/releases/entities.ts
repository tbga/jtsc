import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";

const query = sqlQuery("releases", "entities.sql");

export const selectReleaseEntities = dbQuery<IRelease["id"][], IRelease[]>(
  async (ids, ctx) => {
    const queryArgs = {
      ids,
    };

    const entities = await ctx.many<IRelease>(query, queryArgs);

    return entities;
  },
);
