WITH input_inits AS (
  SELECT
    nextval(pg_get_serial_sequence('public.releases', 'id')) AS release_id,
    release_init.*
  FROM
    json_to_recordset(${release_inits:json}) AS release_init(
      site_id bigint,
      title text,
      description text,
      original_release_id text,
      released_at timestamptz,
      released_at_original text,
      profile_ids bigint[],
      post_inits json,
      post_ids bigint[]
    )
),
release_inits AS (
  SELECT
    release_id,
    site_id,
    title,
    description,
    original_release_id,
    released_at,
    released_at_original
  FROM
    input_inits
),
post_inits AS (
  SELECT
    release_id,
    nextval(pg_get_serial_sequence('public.posts', 'id')) AS post_id
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.post_inits) AS post_init(
      filler text
    )
),
release_profile_inits AS (
  SELECT
    input_inits.release_id,
    profile_data.profile_id
  FROM
    input_inits
    CROSS JOIN
    UNNEST(input_inits.profile_ids) as profile_data(
      profile_id
    )
),
release_posts_inits AS (
  (
    SELECT
      input_inits.release_id,
      post_data.post_id
    FROM
      input_inits
      CROSS JOIN
      UNNEST(input_inits.post_ids) AS post_data(
        post_id
      )
  )
  UNION ALL
  (
    SELECT
      release_id,
      post_id
    FROM
      post_inits
  )
),
new_releases AS (
  INSERT INTO releases
    (
      id,
      site_id,
      title,
      description,
      original_release_id,
      released_at,
      released_at_original
    ) OVERRIDING SYSTEM VALUE
  SELECT
    release_id AS id,
    site_id,
    title,
    description,
    original_release_id,
    released_at,
    released_at_original
  FROM
    release_inits
  RETURNING
    releases.id
),
new_posts AS (
  INSERT INTO posts
    (
      id
    ) OVERRIDING SYSTEM VALUE
  SELECT
    post_id AS id
  FROM
    post_inits
),
new_release_profiles AS (
  INSERT INTO release_profiles
    (
      release_id,
      profile_id
    )
  SELECT
    release_id,
    profile_id
  FROM
    release_profile_inits
),
new_release_posts AS (
  INSERT INTO release_posts
    (
      release_id,
      post_id
    )
  SELECT
    release_id,
    post_id
  FROM
    release_posts_inits
)
SELECT
  id
FROM
  new_releases
;
