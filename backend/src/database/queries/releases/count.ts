import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";
import type { IReleaseQueryArgs } from "./types.js";

const query = sqlQuery("releases", "count.sql");

export const selectReleaseCount = dbQuery<
  Omit<IReleaseQueryArgs, "release_ids" | "pagination">,
  IBigIntegerPositive
>(async ({ profile_ids, post_ids, site_ids, search_query }, ctx) => {
  const queryArgs = {
    profile_ids,
    post_ids,
    site_ids,
    search_query,
  };

  const { count } = await ctx.one<ITableCount>(query, queryArgs);

  return count;
});
