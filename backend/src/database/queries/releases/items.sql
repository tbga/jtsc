WITH input_releases AS (
  SELECT
    id,
    title
  FROM
    releases
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
)
SELECT
  id,
  title AS name
FROM
  input_releases
ORDER BY
  id ASC
;
