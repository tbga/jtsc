WITH input_releases AS (
  SELECT
    id,
    created_at,
    updated_at,
    site_id,
    title,
    description,
    original_release_id,
    released_at,
    released_at_original
  FROM
    releases
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
input_sites AS (
  SELECT DISTINCT ON (sites.id)
    sites.id,
    (
      SELECT json_build_object(
        'id', sites.id,
        'name', sites.title
      )
    ) AS site
  FROM
    sites
    INNER JOIN
    input_releases
    ON
      input_releases.site_id = sites.id
  ORDER BY
    sites.id
),
output_releases AS (
  SELECT
    input_releases.id,
    input_releases.created_at,
    input_releases.updated_at,
    input_releases.title,
    input_releases.description,
    input_releases.original_release_id,
    input_releases.released_at,
    input_releases.released_at_original,
    input_releases.title AS name,
    input_sites.site
  FROM
    input_releases
    LEFT JOIN
    input_sites
    ON
      input_releases.site_id = input_sites.id
)
SELECT
  id,
  created_at,
  updated_at,
  name,
  site,
  title,
  description,
  original_release_id,
  released_at,
  released_at_original
FROM
  output_releases
ORDER BY
  id ASC
;
