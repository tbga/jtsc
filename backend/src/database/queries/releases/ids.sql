WITH input_releases AS (
  SELECT
    id
  FROM
    releases
  WHERE
    (
      ${release_ids} IS NULL
      OR
      id = ANY (
        CAST (${release_ids} AS bigint[])
      )
    )
    AND
    (
      ${site_ids} IS NULL
      OR
      site_id = ANY (
        CAST (${site_ids} AS bigint[])
      )
    )
    AND
    (
      ${profile_ids} IS NULL
      OR
      id IN (
        SELECT DISTINCT
          release_id AS id
        FROM
          release_profiles
        WHERE
          profile_id = ANY (
            CAST (${profile_ids} AS bigint[])
          )
      )
    )
    AND
    (
      ${post_ids} IS NULL
      OR
      id IN (
        SELECT DISTINCT
          release_id AS id
        FROM
          release_posts
        WHERE
          post_id = ANY (
            CAST (${post_ids} AS bigint[])
          )
      )
    )
    AND
    (
      ${search_query} IS NULL
      OR
      (
        title &@~ ${search_query}
        OR
        original_release_id &@~ ${search_query}
        OR
        description &@~ ${search_query}
      )
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_releases
ORDER BY
  id ASC
;
