WITH input_releases AS (
  SELECT
    id,
    site_id,
    title,
    original_release_id,
    released_at
  FROM
    releases
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
input_sites AS (
  SELECT DISTINCT ON (sites.id)
    sites.id,
    (
      SELECT json_build_object(
        'id', sites.id,
        'name', sites.title
      )
    ) AS site
  FROM
    sites
    INNER JOIN
    input_releases
    ON
      input_releases.site_id = sites.id
  ORDER BY
    sites.id
),
profile_counts AS (
  SELECT
    input_releases.id AS release_id,
    count(release_profiles.profile_id) AS profiles
  FROM
    input_releases
    INNER JOIN
    release_profiles
    ON
      input_releases.id = release_profiles.release_id
  GROUP BY
    input_releases.id
),
post_counts AS (
  SELECT
    input_releases.id AS release_id,
    count(release_posts.post_id) AS posts
  FROM
    input_releases
    INNER JOIN
    release_posts
    ON
      input_releases.id = release_posts.release_id
  GROUP BY
    input_releases.id
),
output_releases AS (
  SELECT
    input_releases.id,
    COALESCE (post_counts.posts, 0) AS posts,
    COALESCE (profile_counts.profiles, 0) AS profiles,
    input_releases.title AS name,
    input_sites.site,
    input_releases.original_release_id,
    input_releases.title,
    input_releases.released_at
  FROM
    input_releases
    LEFT JOIN
    input_sites
    ON
      input_releases.site_id = input_sites.id
    LEFT JOIN
    profile_counts
    ON
      input_releases.id = profile_counts.release_id
    LEFT JOIN
    post_counts
    ON
      input_releases.id = post_counts.release_id
)
SELECT
  id,
  profiles,
  posts,
  name,
  site,
  original_release_id,
  title,
  released_at
FROM
  output_releases
ORDER BY
  id ASC
;
