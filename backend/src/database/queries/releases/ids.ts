import { toEntityIDs, type IEntityResult } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IRelease } from "#entities/release";
import type { IReleaseQueryArgs } from "./types.js";

const query = sqlQuery("releases", "ids.sql");

export const selectReleaseIDs = dbQuery<IReleaseQueryArgs, IRelease["id"][]>(
  async (
    { pagination, release_ids, profile_ids, post_ids, site_ids, search_query },
    ctx,
  ) => {
    const { limit, offset } = pagination;
    const queryArgs = {
      limit,
      offset,
      release_ids,
      profile_ids,
      post_ids,
      site_ids,
      search_query,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
