import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import { toEntityIDs, type IEntityResult } from "#lib/entities";
import type { IProfileDBInit } from "./types.js";

const query = sqlQuery("profiles", "insert.sql");

export const insertProfiles = dbQuery<IProfileDBInit[], IProfile["id"][]>(
  async (profile_inits, ctx) => {
    const queryArgs = {
      profile_inits,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
