import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IProfile } from "#entities/profile";

interface IProfileArtistDBInit {
  profile_id: IProfile["id"];
  artist_id: IArtist["id"];
}

interface IResult {
  profiles: { id: IProfile["id"] }[];
  artists: { id: IArtist["id"] }[];
  profile_artists: ({ id: IEntity["id"] } & IProfileArtistDBInit)[];
}

const query = sqlQuery("profiles", "artists", "insert.sql");

export const insertProfileArtists = dbQuery<IProfileArtistDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
