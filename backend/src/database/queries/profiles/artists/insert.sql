WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      profile_id bigint,
      artist_id bigint
    )
),
-- @TODO name select
input_profile_items AS (
  SELECT DISTINCT
    profile_id
  FROM
    input_inits
),
-- @TODO name select
input_artist_items AS (
  SELECT DISTINCT
    artist_id
  FROM
    input_inits
),
new_profile_artists AS (
  INSERT INTO profile_artists
    (
      profile_id,
      artist_id
    )
  SELECT
    profile_id,
    artist_id
  FROM
    input_inits
  RETURNING
    profile_artists.id,
    profile_artists.profile_id,
    profile_artists.artist_id
)
SELECT
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (profile_id AS text) AS id
        FROM
          input_profile_items
      ) AS items
  ) AS profiles,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (artist_id AS text) AS id
        FROM
          input_artist_items
      ) AS items
  ) AS artists,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (profile_id AS text) AS profile_id,
          CAST (artist_id AS text) AS artist_id
        FROM
          new_profile_artists
      ) AS items
  ) AS profile_artists
