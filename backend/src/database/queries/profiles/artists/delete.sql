WITH delete_inputs AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS delete_input(
      profile_id bigint,
      artist_id bigint
    )
),
input_profiles AS (
  SELECT DISTINCT
    profile_id AS id
  FROM
    delete_inputs
),
input_artists AS (
  SELECT DISTINCT
    artist_id AS id
  FROM
    delete_inputs
),
deleted_profile_artists AS (
  DELETE
  FROM
    profile_artists
  WHERE
    id IN (
      SELECT
        id
      FROM
        delete_inputs
        INNER JOIN
        profile_artists
        ON
          delete_inputs.profile_id = profile_artists.profile_id
          AND
          delete_inputs.artist_id = profile_artists.artist_id
    )
  RETURNING
    *
)
SELECT
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (input_profiles.id AS text) AS id
        FROM
          input_profiles
      ) AS items
  ) as profiles,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (input_artists.id AS text) AS id
        FROM
          input_artists
      ) AS items
  ) as artists,
  (
    SELECT json_agg(items)
    FROM (
      SELECT
        CAST (deleted_profile_artists.id AS text) AS id,
        CAST (deleted_profile_artists.profile_id AS text) AS profile_id,
        CAST (deleted_profile_artists.artist_id AS text) AS artist_id
      FROM
        deleted_profile_artists
    ) AS items
  ) as profile_artists
