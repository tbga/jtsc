import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IArtist } from "#entities/artist";
import type { IProfile } from "#entities/profile";

interface IProfileArtistDBDelete {
  profile_id: IProfile["id"];
  artist_id: IArtist["id"];
}

interface IResult {
  profiles: { id: IProfile["id"] }[];
  artists: { id: IArtist["id"] }[];
  profile_artists: ({ id: IEntity["id"] } & IProfileArtistDBDelete)[];
}

const query = sqlQuery("profiles", "artists", "delete.sql");

export const deleteProfileArtists = dbQuery<IProfileArtistDBDelete[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
