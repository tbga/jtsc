import { type IEntityResult, toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import type { IProfileDBUpdate } from "./types.js";

const query = sqlQuery("profiles", "update.sql");

export const updateProfiles = dbQuery<IProfileDBUpdate[], IProfile["id"][]>(
  async (updates, ctx) => {
    const queryArgs = {
      updates,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
