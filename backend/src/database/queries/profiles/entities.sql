-- select base profiles
WITH input_profiles AS (
  SELECT
    id,
    created_at,
    updated_at,
    site_id,
    original_id,
    original_bio
  FROM
    profiles
  WHERE
    id = ANY (
      CAST (${profile_ids} AS bigint[])
    )
),
-- select input sites present in the input profiles
input_site_items AS (
  SELECT DISTINCT
    sites.id,
    sites.title
  FROM
    input_profiles
    INNER JOIN
    sites
    ON
      input_profiles.site_id = sites.id
),
-- select names for input profiles
input_profile_names AS (
  SELECT DISTINCT ON (profile_names.profile_id)
    profile_names.profile_id,
    names.full_name
  FROM
    input_profiles
    INNER JOIN
    profile_names
    ON
      input_profiles.id = profile_names.profile_id
    INNER JOIN
    names
    ON
      profile_names.name_id = names.id
  ORDER BY
    -- sort it so the earliest profile name get selected
    profile_names.profile_id ASC,
    profile_names.id ASC
),
output_profiles AS (
  SELECT
    input_profiles.id,
    input_profiles.created_at,
    input_profiles.updated_at,
    input_profiles.original_id,
    input_profiles.original_bio,
    input_profile_names.full_name AS name,
    (
      SELECT json_build_object(
        'id', input_site_items.id,
        'name', input_site_items.title
      )
    ) AS site
  FROM
    input_profiles
    LEFT JOIN
    input_profile_names
    ON
      input_profiles.id = input_profile_names.profile_id
    LEFT JOIN
    input_site_items
    ON
      input_profiles.site_id = input_site_items.id
)
SELECT
  id,
  created_at,
  updated_at,
  original_id,
  original_bio,
  name,
  site
FROM
  output_profiles
ORDER BY
  id
;
