WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      profile_id bigint,
      name_init json
    )
),
name_inits AS (
  SELECT
    input_inits.profile_id,
    nextval(pg_get_serial_sequence('public.names', 'id')) AS name_id,
    init.full_name
  FROM
    input_inits
    CROSS JOIN
    json_to_record(name_init) AS init(
      full_name text
    )
),
new_names AS (
  INSERT INTO names
    (
      id,
      full_name
    ) OVERRIDING SYSTEM VALUE
  SELECT
    name_id AS id,
    full_name
  FROM
    name_inits
),
new_profile_names AS (
  INSERT INTO profile_names
    (
      profile_id,
      name_id
    )
  SELECT
    profile_id,
    name_id
  FROM
    name_inits
  RETURNING
    profile_names.id,
    profile_names.profile_id,
    profile_names.name_id
),
result AS (
  SELECT
    ARRAY (
      SELECT DISTINCT
        profile_id
      FROM
        input_inits
    ) AS profiles,
    ARRAY (
      SELECT DISTINCT
        name_id
      FROM
        name_inits
    ) AS names,
    (
      SELECT json_agg(items)
      FROM
        (
          SELECT
            CAST (id AS text) AS id,
            CAST (profile_id AS text) AS profile_id,
            CAST (name_id AS text) AS name_id
          FROM
            new_profile_names
        ) AS items
    ) AS profile_names
)
SELECT
  profiles,
  names,
  profile_names
FROM
  result
;
