import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import type { IName } from "#entities/name";
import type { IProfileNameDBDelete } from "./types.js";

interface IResult {
  profiles: IProfile["id"][];
  names: IName["id"][];
  profile_names: ({ id: IEntity["id"] } & IProfileNameDBDelete)[];
}

const query = sqlQuery("profiles", "names", "delete.sql");

export const deleteProfileNames = dbQuery<IProfileNameDBDelete[], IResult>(
  async (deletes, ctx) => {
    const queryArgs = { deletes };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
