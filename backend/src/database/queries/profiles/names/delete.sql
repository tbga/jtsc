WITH input_deletes AS (
  SELECT
    *
  FROM
    json_to_recordset(${deletes:json}) AS delete_input(
      profile_id bigint,
      name_id bigint
    )
),
deleted_profile_names AS (
  DELETE
  FROM
    profile_names
  WHERE
    id IN (
      SELECT
        id
      FROM
        input_deletes
        INNER JOIN
        profile_names
        ON
          input_deletes.profile_id = profile_names.profile_id
          AND
          input_deletes.name_id = profile_names.name_id
    )
  RETURNING
    profile_names.id,
    profile_names.profile_id,
    profile_names.name_id
),
-- names also get deleted because they aren't used by anything else
deleted_names AS (
  DELETE
  FROM
    names
  WHERE
    id IN (
      SELECT
        name_id
      FROM
        deleted_profile_names
    )
),
result AS (
  SELECT
    ARRAY (
      SELECT DISTINCT
        profile_id
      FROM
        input_deletes
    ) AS profiles,
    ARRAY (
      SELECT DISTINCT
        name_id
      FROM
        input_deletes
    ) AS names,
    (
      SELECT json_agg(items)
      FROM
        (
          SELECT
            CAST (id AS text) AS id,
            CAST (profile_id AS text) AS profile_id,
            CAST (name_id AS text) AS name_id
          FROM
            deleted_profile_names
        ) AS items
    ) AS profile_names
)
SELECT
  profiles,
  names,
  profile_names
FROM
  result
;
