import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import type { IProfileNameDBInit } from "./types.js";
import type { IName } from "#entities/name";

interface IResult {
  profiles: IProfile["id"][];
  names: IName["id"][];
  profile_names: {
    id: IEntity["id"];
    profile_id: IProfile["id"];
    name_id: IName["id"];
  }[];
}

const query = sqlQuery("profiles", "names", "insert.sql");

export const insertProfileNames = dbQuery<IProfileNameDBInit[], IResult>(
  async (inits, ctx) => {
    const queryArgs = { inits };

    const result = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
