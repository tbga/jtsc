import type { IProfile } from "#entities/profile";
import type { IName, INameInit } from "#entities/name";

export interface IProfileNameDBInit {
  profile_id: IProfile["id"];
  name_init: INameInit;
}

export interface IProfileNameDBDelete {
  profile_id: IProfile["id"];
  name_id: IName["id"];
}
