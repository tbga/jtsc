import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import { type IEntityResult, toEntityIDs } from "#lib/entities";
import type { IProfileQueryArgs } from "./types.js";

interface IArgs extends IProfileQueryArgs {}

const query = sqlQuery("profiles", "ids.sql");

export const selectProfileIDs = dbQuery<IArgs, IProfile["id"][]>(
  async (
    {
      pagination,
      artist_ids,
      profile_ids,
      site_ids,
      release_ids,
      search_query,
    },
    ctx,
  ) => {
    const { limit, offset } = pagination;
    const queryArgs = {
      limit,
      offset,
      artist_ids,
      profile_ids,
      release_ids,
      site_ids,
      search_query,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
