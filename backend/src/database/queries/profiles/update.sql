WITH input_updates AS (
  SELECT
    profile_update.*
  FROM
    json_to_recordset(${updates:json}) AS profile_update(
      id bigint,
      original_id text,
      original_bio text,
      site_id bigint
    )
),
updated_profiles AS (
  UPDATE
    profiles
  SET
    original_id = update_if_changed(input_updates.original_id, profiles.original_id),
    original_bio = update_if_changed(input_updates.original_bio, profiles.original_bio),
    site_id = update_if_changed(input_updates.site_id, profiles.site_id)
  FROM
    input_updates
  WHERE
    input_updates.id = profiles.id
  RETURNING
    profiles.id
)
SELECT
  id
FROM
  updated_profiles
;
