WITH input_profiles AS (
  SELECT
    id
  FROM
    profiles
  WHERE
    -- select only a subset of profiles
    (
      ${profile_ids} IS NULL
      OR
      id = ANY (
        CAST (${profile_ids} AS bigint[])
      )
    )
    AND
    -- select by a subset of sites
    (
      ${site_ids} IS NULL
      OR
      site_id = ANY (
        CAST (${site_ids} AS bigint[])
      )
    )
    AND
    -- select by a subset of releases
    (
      ${release_ids} IS NULL
      OR
      id IN (
        SELECT
          profile_id AS id
        FROM
          release_profiles
        WHERE
          release_id = ANY (
            CAST (${release_ids} AS bigint[])
          )
      )
    )
    AND
    -- select by a subset of artists
    (
      ${artist_ids} IS NULL
      OR
      id IN (
        SELECT
          profile_id AS id
        FROM
          profile_artists
        WHERE
          artist_id = ANY (
            CAST (${artist_ids} AS bigint[])
          )
      )
    )
    AND
    -- select by name search query
    (
      ${search_query} IS NULL
      OR
      id IN (
        (
          SELECT DISTINCT
            id
          FROM
            profiles
          WHERE
            original_id &@~ ${search_query}
        )
        UNION
        (
          SELECT DISTINCT
            profile_id AS id
          FROM
            profile_names
          WHERE
            name_id IN (
              SELECT
                id AS name_id
              FROM
                names
              WHERE
                full_name &@~ ${search_query}
            )
        )
      )
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_profiles
ORDER BY
  id
;
