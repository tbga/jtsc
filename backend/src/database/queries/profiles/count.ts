import { dbQuery, sqlQuery } from "#database";
import type { ITableCount } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";
import type { IProfileQueryArgs } from "./types.js";

interface IArgs extends Omit<IProfileQueryArgs, "profile_ids" | "pagination"> {}

const query = sqlQuery("profiles", "count.sql");

export const selectProfileCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ artist_ids, site_ids, release_ids, search_query }, ctx) => {
    const queryArgs = {
      artist_ids,
      site_ids,
      release_ids,
      search_query,
    };
    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
