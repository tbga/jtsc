-- select base profiles
WITH input_profiles AS (
  SELECT
    id
  FROM
    profiles
  WHERE
    id = ANY (
      CAST (${profile_ids} AS bigint[])
    )
),
-- select profile names
input_profile_names AS (
  SELECT DISTINCT ON (profile_names.profile_id)
    profile_names.profile_id,
    names.full_name
  FROM
    input_profiles
    INNER JOIN
    profile_names
    ON
      input_profiles.id = profile_names.profile_id
    INNER JOIN
    names
    ON
      profile_names.name_id = names.id
  ORDER BY
    -- sort it so the earliest profile name get selected
    profile_names.profile_id ASC,
    profile_names.id ASC
),
-- build final profiles
output_profiles AS (
  SELECT
    input_profiles.id,
    input_profile_names.full_name AS name
  FROM
    input_profiles
    LEFT JOIN
    input_profile_names
    ON
      input_profiles.id = input_profile_names.profile_id
)
SELECT
  id,
  name
FROM
  output_profiles
ORDER By
  id ASC
;
