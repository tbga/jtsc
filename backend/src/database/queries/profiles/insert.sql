WITH input_inits AS (
  SELECT
    nextval(pg_get_serial_sequence('public.profiles', 'id')) AS profile_id,
    input_init.*
  FROM
    json_to_recordset(${profile_inits:json}) AS input_init(
      names json,
      site_id bigint,
      original_id text,
      original_bio text,
      release_ids bigint[],
      artist_ids bigint[],
      artist_inits json
    )
),
profile_inits AS (
  SELECT
    profile_id,
    site_id,
    original_id,
    original_bio
  FROM
    input_inits
),
name_inits AS (
  SELECT
    profile_id,
    nextval(pg_get_serial_sequence('public.names', 'id')) AS name_id,
    name_init.full_name
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.names) AS name_init(
      full_name text
    )
),
artist_inits AS (
  SELECT
    profile_id,
    nextval(pg_get_serial_sequence('public.artists', 'id')) AS artist_id,
    artist_init.sex
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.artist_inits) AS artist_init(
      sex boolean
    )
),
profile_name_inits AS (
  SELECT
    profile_id,
    name_id
  FROM
    name_inits
),
profile_release_inits AS (
  SELECT
    input_inits.profile_id,
    release_data.release_id
  FROM
    input_inits
    CROSS JOIN
    UNNEST(input_inits.release_ids) AS release_data(
      release_id
    )
),
profile_artist_inits AS (
  (
    SELECT
      input_inits.profile_id,
      artist_data.artist_id
    FROM
      input_inits
      CROSS JOIN
      UNNEST(input_inits.artist_ids) AS artist_data(
        artist_id
      )
  )
  UNION ALL
  (
    SELECT
      profile_id,
      artist_id
    FROM
      artist_inits
  )
),
new_profiles AS (
  INSERT INTO profiles
    (
      id,
      site_id,
      original_id,
      original_bio
    ) OVERRIDING SYSTEM VALUE
  SELECT
    profile_id AS id,
    site_id,
    original_id,
    original_bio
  FROM
    profile_inits
  RETURNING
    profiles.id
),
new_names AS (
  INSERT INTO names
    (
      id,
      full_name
    ) OVERRIDING SYSTEM VALUE
  SELECT
    name_id AS id,
    full_name
  FROM
    name_inits
),
new_artists AS (
  INSERT INTO artists
    (
      id,
      sex
    ) OVERRIDING SYSTEM VALUE
  SELECT
    artist_id AS id,
    sex
  FROM
    artist_inits
),
new_profile_names AS (
  INSERT INTO profile_names
    (
      profile_id,
      name_id
    )
  SELECT
    profile_id,
    name_id
  FROM
    profile_name_inits
),
new_profile_releases AS (
  INSERT INTO release_profiles
    (
      profile_id,
      release_id
    )
  SELECT
    profile_id,
    release_id
  FROM
    profile_release_inits
),
new_profile_artists AS (
  INSERT INTO profile_artists
    (
      profile_id,
      artist_id
    )
  SELECT
    profile_id,
    artist_id
  FROM
    profile_artist_inits
)
SELECT
  id
FROM
  new_profiles
ORDER BY
  id ASC
;
