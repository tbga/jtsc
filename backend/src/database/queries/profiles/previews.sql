-- select base profiles
WITH input_profiles AS (
  SELECT
    id,
    site_id,
    original_id
  FROM
    profiles
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
-- select profile names
input_profile_names AS (
  SELECT DISTINCT ON (profile_names.profile_id)
    profile_names.profile_id,
    names.full_name
  FROM
    input_profiles
    INNER JOIN
    profile_names
    ON
      input_profiles.id = profile_names.profile_id
    INNER JOIN
    names
    ON
      profile_names.name_id = names.id
  ORDER BY
    -- sort it so the earliest profile name get selected
    profile_names.profile_id ASC,
    profile_names.id ASC
),
-- select profile sites
input_site_items AS (
  SELECT DISTINCT ON (sites.id)
    sites.id,
    (
      SELECT json_build_object(
        'id', sites.id,
        'name', sites.title
      )
    ) AS site
  FROM
    input_profiles
    INNER JOIN
    sites
    ON
      input_profiles.site_id = sites.id
),
artist_counts AS (
  SELECT
    input_profiles.id AS profile_id,
    count(profile_artists.artist_id) AS artist_count
  FROM
    input_profiles
    INNER JOIN
    profile_artists
    ON
      input_profiles.id = profile_artists.profile_id
  GROUP BY
    input_profiles.id
),
release_counts AS (
  SELECT
    input_profiles.id AS profile_id,
    count(release_profiles.id) AS release_count
  FROM
    input_profiles
    INNER JOIN
    release_profiles
    ON
      input_profiles.id = release_profiles.profile_id
  GROUP BY
    input_profiles.id
),
-- build final profiles
output_profiles AS (
  SELECT
    input_profiles.id,
    input_profile_names.full_name AS name,
    input_site_items.site,
    input_profiles.original_id,
    COALESCE (artist_counts.artist_count, 0) AS artists,
    COALESCE (release_counts.release_count, 0) AS releases
  FROM
    input_profiles
    LEFT JOIN
    input_profile_names
    ON
      input_profiles.id = input_profile_names.profile_id
    LEFT JOIN
    input_site_items
    ON
      input_profiles.site_id = input_site_items.id
    LEFT JOIN
    artist_counts
    ON
      input_profiles.id = artist_counts.profile_id
    LEFT JOIN
    release_counts
    ON
      input_profiles.id = release_counts.profile_id
)
SELECT
  id,
  name,
  site,
  original_id,
  artists,
  releases
FROM
  output_profiles
ORDER By
  id ASC
;
