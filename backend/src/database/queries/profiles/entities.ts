import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";

const query = sqlQuery("profiles", "entities.sql");

export const selectProfileEntities = dbQuery<IProfile["id"][], IProfile[]>(
  async (profile_ids, ctx) => {
    const queryArgs = { profile_ids };

    const entities = await ctx.many<IProfile>(query, queryArgs);

    return entities;
  },
);
