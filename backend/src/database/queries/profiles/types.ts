import { TableName } from "#database";
import type { IPaginatedSearchQueryArgs } from "#lib/entities";
import type { IFileDBInit } from "#database/queries/files";
import type { IArtist, IArtistInit } from "#entities/artist";
import type { IFile } from "#entities/file";
import type { IProfile, IProfileUpdate, IProfileInit } from "#entities/profile";
import type { ISite } from "#entities/site";
import type { IRelease } from "#entities/release";

export interface IProfileDBInit
  extends Omit<IProfileInit, "artists" | "releases"> {
  release_ids?: IRelease["id"][];
  artist_ids?: IArtist["id"][];
  artist_inits?: IArtistInit[];
}

export interface IProfileDBUpdate extends IProfileUpdate {
  id: IProfile["id"];
}

export interface IProfileQueryArgs extends IPaginatedSearchQueryArgs {
  profile_ids?: IProfile["id"][];
  site_ids?: ISite["id"][];
  release_ids?: IRelease["id"][];
  artist_ids?: IArtist["id"][];
}

export const profilesTable = new TableName({
  schema: "public",
  table: "profiles",
});

export const profileArtistsTable = new TableName({
  schema: "public",
  table: "profile_artists",
});

export const profileNamesTable = new TableName({
  schema: "public",
  table: "profile_names",
});

export const profilePortraitsTable = new TableName({
  schema: "public",
  table: "profile_portraits",
});
