import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";

const query = sqlQuery("profiles", "items.sql");

export const selectProfileItems = dbQuery<IProfile["id"][], IEntityItem[]>(
  async (profile_ids, ctx) => {
    const queryArgs = {
      profile_ids,
    };

    const items = await ctx.many<IEntityItem>(query, queryArgs);

    return items;
  },
);
