import { dbQuery, sqlQuery } from "#database";
import type { IProfile, IProfilePreview } from "#entities/profile";

const query = sqlQuery("profiles", "previews.sql");

export const selectProfilePreviews = dbQuery<
  IProfile["id"][],
  IProfilePreview[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const profilePreviews = await ctx.many<IProfilePreview>(query, queryArgs);

  return profilePreviews;
});
