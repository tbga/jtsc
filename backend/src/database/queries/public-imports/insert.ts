import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImportSiteApprovalStatus,
  IPublicImport,
  IPublicImportStatus,
} from "#entities/public-import";
import type { IPublicImportDBinit } from "./types.js";

const query = sqlQuery("public-imports", "insert.sql");
const default_public_import_status = "pending" satisfies IPublicImportStatus;
const default_approval_status =
  "undecided" satisfies IPublicImportSiteApprovalStatus;

export const insertPublicImports = dbQuery<
  IPublicImportDBinit[],
  IPublicImport["id"][]
>(async (inits, ctx) => {
  const args = {
    inits,
    default_public_import_status,
    default_approval_status,
  };

  const result = await ctx.many<{ id: IPublicImport["id"] }>(query, args);
  const ids = result.map<IPublicImport["id"]>(({ id }) => id);

  return ids;
});
