import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImport,
  IPublicImportPreview,
} from "#entities/public-import";

const query = sqlQuery("public-imports", "previews.sql");

export const selectPublicImportPreviews = dbQuery<
  IPublicImport["id"][],
  IPublicImportPreview[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const previews = await ctx.many<IPublicImportPreview>(query, queryArgs);

  return previews;
});
