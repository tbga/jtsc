WITH input_finishers AS (
  SELECT
    input_finisher.*
  FROM
    json_to_recordset(${finishers:json}) AS input_finisher(
      id bigint,
      account_id bigint
    )
),
input_imports AS (
  SELECT
    id,
    created_by,
    status
  FROM
    public_imports
  WHERE
    id IN (
      SELECT DISTINCT
        id
      FROM
        input_finishers
    )
    AND
    public_imports.status = ${public_import_status_in_progress}
),
input_sites AS (
  SELECT
    public_import_sites.id,
    sites.id AS site_id,
    public_import_sites.public_import_id,
    public_import_sites.public_id,
    public_import_sites.home_page,
    public_import_sites.title,
    public_import_sites.long_title,
    public_import_sites.description,
    public_import_sites.url_template_profile_list,
    public_import_sites.url_template_profile_list_notes,
    public_import_sites.url_template_profile,
    public_import_sites.url_template_profile_notes,
    public_import_sites.url_template_release_list,
    public_import_sites.url_template_release_list_notes,
    public_import_sites.url_template_release,
    public_import_sites.url_template_release_notes,
    (
      CASE
        WHEN
          sites.public_id IS NULL
        THEN
          ${public_import_site_category_new}
        WHEN
          (
            public_import_sites.home_page IS NOT DISTINCT FROM sites.home_page
            AND
            public_import_sites.title IS NOT DISTINCT FROM sites.title
            AND
            public_import_sites.long_title IS NOT DISTINCT FROM sites.long_title
            AND
            public_import_sites.description IS NOT DISTINCT FROM sites.description
            AND
            public_import_sites.url_template_profile_list IS NOT DISTINCT FROM sites.url_template_profile_list
            AND
            public_import_sites.url_template_profile_list_notes IS NOT DISTINCT FROM sites.url_template_profile_list_notes
            AND
            public_import_sites.url_template_profile IS NOT DISTINCT FROM sites.url_template_profile
            AND
            public_import_sites.url_template_profile_notes IS NOT DISTINCT FROM sites.url_template_profile_notes
            AND
            public_import_sites.url_template_release_list IS NOT DISTINCT FROM sites.url_template_release_list
            AND
            public_import_sites.url_template_release_list_notes IS NOT DISTINCT FROM sites.url_template_release_list_notes
            AND
            public_import_sites.url_template_release IS NOT DISTINCT FROM sites.url_template_release
            AND
            public_import_sites.url_template_release_notes IS NOT DISTINCT FROM sites.url_template_release_notes
          )
        THEN
          ${public_import_site_category_same}
        ELSE
          ${public_import_site_category_different}
      END
    ) AS category
  FROM
    public_import_sites
    INNER JOIN
    input_imports
    ON
      public_import_sites.public_import_id = input_imports.id
    LEFT JOIN
    sites
    ON
      public_import_sites.public_id = sites.public_id
  WHERE
    -- exclude non-aprroved and same sites
    public_import_sites.approval_status = ${public_import_site_approval_status_approved}
),
site_inits AS (
  SELECT
    input_sites.public_id,
    input_sites.home_page,
    input_sites.title,
    input_sites.long_title,
    input_sites.description,
    CURRENT_TIMESTAMP AS published_at,
    input_imports.created_by AS published_by,
    input_sites.url_template_profile_list,
    input_sites.url_template_profile_list_notes,
    input_sites.url_template_profile,
    input_sites.url_template_profile_notes,
    input_sites.url_template_release_list,
    input_sites.url_template_release_list_notes,
    input_sites.url_template_release,
    input_sites.url_template_release_notes
  FROM
    input_sites
    INNER JOIN
    input_imports
    ON
      input_sites.public_import_id = input_imports.id
  WHERE
    -- only include new sites
    input_sites.category = ${public_import_site_category_new}
),
site_updates AS (
  SELECT
    input_sites.id,
    input_sites.site_id,
    input_sites.home_page,
    input_sites.title,
    input_sites.long_title,
    input_sites.description,
    input_sites.url_template_profile_list,
    input_sites.url_template_profile_list_notes,
    input_sites.url_template_profile,
    input_sites.url_template_profile_notes,
    input_sites.url_template_release_list,
    input_sites.url_template_release_list_notes,
    input_sites.url_template_release,
    input_sites.url_template_release_notes
  FROM
    input_sites
  WHERE
    -- only include different sites
    input_sites.category = ${public_import_site_category_different}
),
new_sites AS (
  INSERT INTO sites
    (
      public_id,
      home_page,
      title,
      long_title,
      description,
      published_at,
      published_by,
      url_template_profile_list,
      url_template_profile_list_notes,
      url_template_profile,
      url_template_profile_notes,
      url_template_release_list,
      url_template_release_list_notes,
      url_template_release,
      url_template_release_notes
    )
  SELECT
    public_id,
    home_page,
    title,
    long_title,
    description,
    published_at,
    published_by,
    url_template_profile_list,
    url_template_profile_list_notes,
    url_template_profile,
    url_template_profile_notes,
    url_template_release_list,
    url_template_release_list_notes,
    url_template_release,
    url_template_release_notes
  FROM
    site_inits
),
updated_sites AS (
  UPDATE
    sites
  SET
    home_page = update_if_changed(
      site_updates.home_page,
      sites.home_page
    ),
    title = update_if_changed(
      site_updates.title,
      sites.title
    ),
    long_title = update_if_changed(
      site_updates.long_title,
      sites.long_title
    ),
    description = update_if_changed(
      site_updates.description,
      sites.description
    ),
    url_template_profile_list = update_if_changed(
      site_updates.url_template_profile_list,
      sites.url_template_profile_list
    ),
    url_template_profile_list_notes = update_if_changed(
      site_updates.url_template_profile_list_notes,
      sites.url_template_profile_list_notes
    ),
    url_template_profile = update_if_changed(
      site_updates.url_template_profile,
      sites.url_template_profile
    ),
    url_template_profile_notes = update_if_changed(
      site_updates.url_template_profile_notes,
      sites.url_template_profile_notes
    ),
    url_template_release_list = update_if_changed(
      site_updates.url_template_release_list,
      sites.url_template_release_list
    ),
    url_template_release_list_notes = update_if_changed(
      site_updates.url_template_release_list_notes,
      sites.url_template_release_list_notes
    ),
    url_template_release = update_if_changed(
      site_updates.url_template_release,
      sites.url_template_release
    ),
    url_template_release_notes = update_if_changed(
      site_updates.url_template_release_notes,
      sites.url_template_release_notes
    )
  FROM
    site_updates
  WHERE
    sites.id = site_updates.site_id
),
-- update imports into finished state
updated_imports AS (
  UPDATE
    public_imports
  SET
    status = ${public_import_status_consumed}
  FROM
    input_imports
  WHERE
    input_imports.id = public_imports.id
  RETURNING
    public_imports.id
),
-- remove public import sites
deleted_sites AS (
  DELETE
  FROM
    public_import_sites
  WHERE
    id IN (
      SELECT
        input_sites.id
      FROM
        input_sites
    )
)
SELECT
  id
FROM
  updated_imports
;
