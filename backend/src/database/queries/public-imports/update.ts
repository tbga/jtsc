import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImport,
  IPublicImportStatus,
} from "#entities/public-import";
import type { IPublicImportDBUpdate } from "./types.js";

const query = sqlQuery("public-imports", "update.sql");
const invalid_import_update_status = "consumed" satisfies IPublicImportStatus;

export const updatePublicImports = dbQuery<
  IPublicImportDBUpdate[],
  IPublicImport["id"][]
>(async (updates, ctx) => {
  const queryArgs = { updates, invalid_import_update_status };
  const result = await ctx.many<{ id: IPublicImport["id"] }>(query, queryArgs);
  const ids = toEntityIDs(result);

  return ids;
});
