import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPublicImport } from "#entities/public-import";

const query = sqlQuery("public-imports", "items.sql");

export const selectPublicImportItems = dbQuery<
  IPublicImport["id"][],
  IEntityItem[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const items = await ctx.many<IEntityItem>(query, queryArgs);

  return items;
});
