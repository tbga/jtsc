import { dbQuery, sqlQuery } from "#database";
import type { IPublicImport } from "#entities/public-import";
import type { IPublicImportQueryArgs } from "./types.js";

const query = sqlQuery("public-imports", "ids.sql");

export const selectPublicImportIDs = dbQuery<
  IPublicImportQueryArgs,
  IPublicImport["id"][]
>(async ({ pagination }, ctx) => {
  const { limit, offset } = pagination;
  const args = {
    limit,
    offset,
  };

  const result = await ctx.many<{ id: IPublicImport["id"] }>(query, args);
  const ids = result.map<IPublicImport["id"]>(({ id }) => id);

  return ids;
});
