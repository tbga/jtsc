WITH input_public_imports AS (
  SELECT
    id,
    sites_count
  FROM
    public_imports
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
input_site_stats AS (
  SELECT
    id,
    sites_count AS all,
    (
      SELECT
        count(*)
      FROM
        public_import_sites
        INNER JOIN
        input_public_imports
        ON
          public_import_sites.public_import_id = input_public_imports.id
        LEFT JOIN
        sites
        ON
          public_import_sites.public_id = sites.public_id
      WHERE
        sites.public_id IS NULL
    ) AS new,
    (
      SELECT
        count(*)
      FROM
        public_import_sites
        INNER JOIN
        input_public_imports
        ON
          public_import_sites.public_import_id = input_public_imports.id
        INNER JOIN
        sites
        ON
          public_import_sites.public_id = sites.public_id
      WHERE
        public_import_sites.home_page IS NOT DISTINCT FROM sites.home_page
        AND
        public_import_sites.title IS NOT DISTINCT FROM sites.title
        AND
        public_import_sites.long_title IS NOT DISTINCT FROM sites.long_title
        AND
        public_import_sites.description IS NOT DISTINCT FROM sites.description
        AND
        public_import_sites.url_template_profile_list IS NOT DISTINCT FROM sites.url_template_profile_list
        AND
        public_import_sites.url_template_profile_list_notes IS NOT DISTINCT FROM sites.url_template_profile_list_notes
        AND
        public_import_sites.url_template_profile IS NOT DISTINCT FROM sites.url_template_profile
        AND
        public_import_sites.url_template_profile_notes IS NOT DISTINCT FROM sites.url_template_profile_notes
        AND
        public_import_sites.url_template_release_list IS NOT DISTINCT FROM sites.url_template_release_list
        AND
        public_import_sites.url_template_release_list_notes IS NOT DISTINCT FROM sites.url_template_release_list_notes
        AND
        public_import_sites.url_template_release IS NOT DISTINCT FROM sites.url_template_release
        AND
        public_import_sites.url_template_release_notes IS NOT DISTINCT FROM sites.url_template_release_notes
  ) AS same,
  (
    SELECT
      count(*)
    FROM
      public_import_sites
      INNER JOIN
      input_public_imports
      ON
        public_import_sites.public_import_id = input_public_imports.id
      INNER JOIN
      sites
      ON
        public_import_sites.public_id = sites.public_id
    WHERE
      public_import_sites.home_page IS DISTINCT FROM sites.home_page
      OR
      public_import_sites.title IS DISTINCT FROM sites.title
      OR
      public_import_sites.long_title IS DISTINCT FROM sites.long_title
      OR
      public_import_sites.description IS DISTINCT FROM sites.description
      OR
      public_import_sites.url_template_profile_list IS DISTINCT FROM sites.url_template_profile_list
      OR
      public_import_sites.url_template_profile_list_notes IS DISTINCT FROM sites.url_template_profile_list_notes
      OR
      public_import_sites.url_template_profile IS DISTINCT FROM sites.url_template_profile
      OR
      public_import_sites.url_template_profile_notes IS DISTINCT FROM sites.url_template_profile_notes
      OR
      public_import_sites.url_template_release_list IS DISTINCT FROM sites.url_template_release_list
      OR
      public_import_sites.url_template_release_list_notes IS DISTINCT FROM sites.url_template_release_list_notes
      OR
      public_import_sites.url_template_release IS DISTINCT FROM sites.url_template_release
      OR
      public_import_sites.url_template_release_notes IS DISTINCT FROM sites.url_template_release_notes
  ) AS different
  FROM
    input_public_imports
),
output_stats AS (
  SELECT
    id,
    (
      SELECT json_build_object(
        'all', input_site_stats.all,
        'new', input_site_stats.new,
        'same', input_site_stats.same,
        'different', input_site_stats.different
      )
    ) AS sites
  FROM
    input_site_stats
)
SELECT
  id,
  sites
FROM
  output_stats
;
