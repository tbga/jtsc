import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImport,
  IPublicImportStats,
} from "#entities/public-import";

const query = sqlQuery("public-imports", "stats.sql");

export const selectPublicImportStats = dbQuery<
  IPublicImport["id"][],
  IPublicImportStats[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };

  const stats = await ctx.many<IPublicImportStats>(query, queryArgs);

  return stats;
});
