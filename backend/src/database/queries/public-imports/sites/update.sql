WITH input_updates AS (
  SELECT
    incoming_update.id,
    incoming_update.approval_status
  FROM
    json_to_recordset(${updates:json}) AS incoming_update(
      id bigint,
      approval_status text
    )
),
input_sites AS (
  SELECT DISTINCT
    id
  FROM
    input_updates
),
site_categories AS (
  WITH input_site_values AS (
    SELECT
      public_import_sites.id,
      public_import_sites.public_id,
      public_import_sites.home_page,
      public_import_sites.title,
      public_import_sites.long_title,
      public_import_sites.description,
      public_import_sites.url_template_profile_list,
      public_import_sites.url_template_profile_list_notes,
      public_import_sites.url_template_profile,
      public_import_sites.url_template_profile_notes,
      public_import_sites.url_template_release_list,
      public_import_sites.url_template_release_list_notes,
      public_import_sites.url_template_release,
      public_import_sites.url_template_release_notes
    FROM
      input_sites
      INNER JOIN
      public_import_sites
      ON
        input_sites.id = public_import_sites.id
  )
  SELECT
    input_site_values.id,
    (
      CASE
        WHEN
          sites.public_id IS NULL
        THEN
          ${category_new}
        WHEN
          (
            input_site_values.home_page = sites.home_page
            AND
            input_site_values.title = sites.title
            AND
            input_site_values.long_title = sites.long_title
            AND
            input_site_values.description = sites.description
            AND
            input_site_values.url_template_profile_list = sites.url_template_profile_list
            AND
            input_site_values.url_template_profile_list_notes = sites.url_template_profile_list_notes
            AND
            input_site_values.url_template_profile = sites.url_template_profile
            AND
            input_site_values.url_template_profile_notes = sites.url_template_profile_notes
            AND
            input_site_values.url_template_release_list = sites.url_template_release_list
            AND
            input_site_values.url_template_release_list_notes = sites.url_template_release_list_notes
            AND
            input_site_values.url_template_release = sites.url_template_release
            AND
            input_site_values.url_template_release_notes = sites.url_template_release_notes
          )
        THEN
          ${category_same}
        ELSE
          ${category_different}
      END
    ) AS category
  FROM
    input_site_values
    LEFT JOIN
    sites
    ON
      input_site_values.public_id = sites.public_id
),
updated_public_import_sites AS (
  UPDATE
    public_import_sites
  SET
    approval_status = update_if_changed(
      input_updates.approval_status,
      public_import_sites.approval_status
    )
  FROM
    input_updates
    INNER JOIN
    site_categories
    ON
      input_updates.id = site_categories.id
  WHERE
    input_updates.id = public_import_sites.id
    AND
    -- prevent approvals on `same` category
    site_categories.category != ${category_same}
  RETURNING
    public_import_sites.id
)
SELECT
  id
FROM
  updated_public_import_sites
;
