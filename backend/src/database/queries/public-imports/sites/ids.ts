import { publicImportSiteCategorySchema } from "#codegen/schema/lib/entities/public-import/site/category";
import type { IPaginatedQueryArgs } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImportSite,
  IPublicImport,
  IPublicImportSiteCategory,
} from "#entities/public-import";

interface IArgs extends IPaginatedQueryArgs {
  public_import_ids: IPublicImport["id"][];
  category?: IPublicImportSiteCategory;
}

const category_new = publicImportSiteCategorySchema.anyOf[0].const;
const category_same = publicImportSiteCategorySchema.anyOf[1].const;
const category_different = publicImportSiteCategorySchema.anyOf[2].const;
const query = sqlQuery("public-imports", "sites", "ids.sql");

export const selectPublicImportSiteIDs = dbQuery<
  IArgs,
  IPublicImportSite["id"][]
>(async ({ pagination, public_import_ids, category }, ctx) => {
  const { limit, offset } = pagination;
  const args = {
    limit,
    offset,
    public_import_ids,
    category,
    category_new,
    category_same,
    category_different,
  };

  const result = await ctx.many<{ id: IPublicImport["id"] }>(query, args);
  const ids = result.map<IPublicImport["id"]>(({ id }) => id);

  return ids;
});
