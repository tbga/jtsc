import { publicImportSiteCategorySchema } from "#codegen/schema/lib/entities/public-import/site/category";
import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPublicImportSite } from "#entities/public-import";
import type { IPublicImportSiteUpdate } from "./types.js";

const category_new = publicImportSiteCategorySchema.anyOf[0].const;
const category_same = publicImportSiteCategorySchema.anyOf[1].const;
const category_different = publicImportSiteCategorySchema.anyOf[2].const;
const query = sqlQuery("public-imports", "sites", "update.sql");

export const updatePublicImportSites = dbQuery<
  IPublicImportSiteUpdate[],
  IPublicImportSite["id"][]
>(async (updates, ctx) => {
  const queryArgs = {
    updates,
    category_new,
    category_same,
    category_different,
  };
  const result = await ctx.many<{ id: IPublicImportSite["id"] }>(
    query,
    queryArgs,
  );
  const ids = toEntityIDs(result);

  return ids;
});
