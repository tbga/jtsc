import type { IPublicImportSite } from "#entities/public-import";

export interface IPublicImportSiteUpdate
  extends Pick<IPublicImportSite, "id" | "approval_status"> {}
