import { publicImportSiteCategorySchema } from "#codegen/schema/lib/entities/public-import/site/category";
import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicImportSite,
  IPublicImportSitePreview,
} from "#entities/public-import";

const category_new = publicImportSiteCategorySchema.anyOf[0].const;
const category_same = publicImportSiteCategorySchema.anyOf[1].const;
const category_different = publicImportSiteCategorySchema.anyOf[2].const;
const query = sqlQuery("public-imports", "sites", "previews.sql");

export const selectPublicImportSitePreviews = dbQuery<
  IPublicImportSite["id"][],
  IPublicImportSitePreview[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
    category_new,
    category_same,
    category_different,
  };

  const previews = await ctx.many<IPublicImportSitePreview>(query, queryArgs);

  return previews;
});
