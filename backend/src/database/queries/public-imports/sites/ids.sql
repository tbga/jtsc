SELECT
  id
FROM
  public_import_sites
WHERE
  (
    ${public_import_ids} IS NULL
    OR
    public_import_id = ANY (
      CAST (${public_import_ids} AS bigint[])
    )
  )
  AND
  (
    ${category} IS NULL
    OR
    (
      ${category} = ${category_new}
      AND
      id IN (
        SELECT
          public_import_sites.id
        FROM
          public_import_sites
          LEFT JOIN
          sites
          ON
            public_import_sites.public_id = sites.public_id
        WHERE
          sites.public_id IS NULL
      )
    )
    OR
    (
      ${category} = ${category_same}
      AND
      id IN (
        SELECT
          public_import_sites.id
        FROM
          public_import_sites
          INNER JOIN
          sites
          ON
            public_import_sites.public_id = sites.public_id
        WHERE
          public_import_sites.home_page IS NOT DISTINCT FROM sites.home_page
          AND
          public_import_sites.title IS NOT DISTINCT FROM sites.title
          AND
          public_import_sites.long_title IS NOT DISTINCT FROM sites.long_title
          AND
          public_import_sites.description IS NOT DISTINCT FROM sites.description
          AND
          public_import_sites.url_template_profile_list IS NOT DISTINCT FROM sites.url_template_profile_list
          AND
          public_import_sites.url_template_profile_list_notes IS NOT DISTINCT FROM sites.url_template_profile_list_notes
          AND
          public_import_sites.url_template_profile IS NOT DISTINCT FROM sites.url_template_profile
          AND
          public_import_sites.url_template_profile_notes IS NOT DISTINCT FROM sites.url_template_profile_notes
          AND
          public_import_sites.url_template_release_list IS NOT DISTINCT FROM sites.url_template_release_list
          AND
          public_import_sites.url_template_release_list_notes IS NOT DISTINCT FROM sites.url_template_release_list_notes
          AND
          public_import_sites.url_template_release IS NOT DISTINCT FROM sites.url_template_release
          AND
          public_import_sites.url_template_release_notes IS NOT DISTINCT FROM sites.url_template_release_notes
      )
    )
    OR
    (
      ${category} = ${category_different}
      AND
      id IN (
        SELECT
          public_import_sites.id
        FROM
          public_import_sites
          INNER JOIN
          sites
          ON
            public_import_sites.public_id = sites.public_id
        WHERE
          public_import_sites.home_page IS DISTINCT FROM sites.home_page
          OR
          public_import_sites.title IS DISTINCT FROM sites.title
          OR
          public_import_sites.long_title IS DISTINCT FROM sites.long_title
          OR
          public_import_sites.description IS DISTINCT FROM sites.description
          OR
          public_import_sites.url_template_profile_list IS DISTINCT FROM sites.url_template_profile_list
          OR
          public_import_sites.url_template_profile_list_notes IS DISTINCT FROM sites.url_template_profile_list_notes
          OR
          public_import_sites.url_template_profile IS DISTINCT FROM sites.url_template_profile
          OR
          public_import_sites.url_template_profile_notes IS DISTINCT FROM sites.url_template_profile_notes
          OR
          public_import_sites.url_template_release_list IS DISTINCT FROM sites.url_template_release_list
          OR
          public_import_sites.url_template_release_list_notes IS DISTINCT FROM sites.url_template_release_list_notes
          OR
          public_import_sites.url_template_release IS DISTINCT FROM sites.url_template_release
          OR
          public_import_sites.url_template_release_notes IS DISTINCT FROM sites.url_template_release_notes
      )
    )
  )
ORDER BY
  id ASC
LIMIT ${limit}
OFFSET ${offset}
;
