WITH input_sites AS (
  SELECT
    id,
    created_at,
    updated_at,
    public_import_id,
    approval_status,
    public_id,
    home_page,
    title,
    long_title,
    description,
    (
      SELECT json_build_object (
        'profile_list', public_import_sites.url_template_profile_list,
        'profile_list_notes', public_import_sites.url_template_profile_list_notes,
        'profile', public_import_sites.url_template_profile,
        'profile_notes', public_import_sites.url_template_profile_notes,
        'release_list', public_import_sites.url_template_release_list,
        'release_list_notes', public_import_sites.url_template_release_list_notes,
        'release', public_import_sites.url_template_release,
        'release_notes', public_import_sites.url_template_release_notes
      )
    ) AS url_templates
  FROM
    public_import_sites
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
input_public_imports AS (
  SELECT
    id,
    (
      SELECT json_build_object (
        'id', CAST (public_imports.id AS text),
        'public_id', public_imports.public_id,
        'name', public_imports.title
      )
    ) AS public_import
  FROM
    public_imports
  WHERE
    id IN (
      SELECT DISTINCT
        public_import_id AS id
      FROM
        input_sites
    )
),
input_local_sites AS (
  SELECT
    public_id,
    (
      SELECT json_build_object (
        'id', CAST (sites.id AS text),
        'public_id', sites.public_id,
        'name', sites.title
      )
    ) AS local_site
  FROM
    sites
  WHERE
    public_id IN (
      SELECT DISTINCT
        public_id
      FROM
        input_sites
    )
),
site_categories AS (
  WITH input_site_values AS (
    SELECT
      public_import_sites.id,
      public_import_sites.public_id,
      public_import_sites.home_page,
      public_import_sites.title,
      public_import_sites.long_title,
      public_import_sites.description,
      public_import_sites.url_template_profile_list,
      public_import_sites.url_template_profile_list_notes,
      public_import_sites.url_template_profile,
      public_import_sites.url_template_profile_notes,
      public_import_sites.url_template_release_list,
      public_import_sites.url_template_release_list_notes,
      public_import_sites.url_template_release,
      public_import_sites.url_template_release_notes
    FROM
      input_sites
      INNER JOIN
      public_import_sites
      ON
        input_sites.id = public_import_sites.id
  )
  SELECT
    input_site_values.id,
    (
      CASE
        WHEN
          sites.public_id IS NULL
        THEN
          ${category_new}
        WHEN
          (
            input_site_values.home_page IS NOT DISTINCT FROM sites.home_page
            AND
            input_site_values.title IS NOT DISTINCT FROM sites.title
            AND
            input_site_values.long_title IS NOT DISTINCT FROM sites.long_title
            AND
            input_site_values.description IS NOT DISTINCT FROM sites.description
            AND
            input_site_values.url_template_profile_list IS NOT DISTINCT FROM sites.url_template_profile_list
            AND
            input_site_values.url_template_profile_list_notes IS NOT DISTINCT FROM sites.url_template_profile_list_notes
            AND
            input_site_values.url_template_profile IS NOT DISTINCT FROM sites.url_template_profile
            AND
            input_site_values.url_template_profile_notes IS NOT DISTINCT FROM sites.url_template_profile_notes
            AND
            input_site_values.url_template_release_list IS NOT DISTINCT FROM sites.url_template_release_list
            AND
            input_site_values.url_template_release_list_notes IS NOT DISTINCT FROM sites.url_template_release_list_notes
            AND
            input_site_values.url_template_release IS NOT DISTINCT FROM sites.url_template_release
            AND
            input_site_values.url_template_release_notes IS NOT DISTINCT FROM sites.url_template_release_notes
          )
        THEN
          ${category_same}
        ELSE
          ${category_different}
      END
    ) AS category
  FROM
    input_site_values
    LEFT JOIN
    sites
    ON
      input_site_values.public_id = sites.public_id
),
output_sites AS (
  SELECT
    input_sites.id,
    input_sites.created_at,
    input_sites.updated_at,
    input_public_imports.public_import,
    input_sites.public_id,
    input_local_sites.local_site,
    site_categories.category,
    input_sites.approval_status,
    input_sites.home_page,
    input_sites.title,
    input_sites.title AS name,
    input_sites.long_title,
    input_sites.description,
    input_sites.url_templates
  FROM
    input_sites
    INNER JOIN
    input_public_imports
    ON
      input_sites.public_import_id = input_public_imports.id
    INNER JOIN
    site_categories
    ON
      input_sites.id = site_categories.id
    LEFT JOIN
    input_local_sites
    ON
      input_sites.public_id = input_local_sites.public_id

)
SELECT
  id,
  created_at,
  updated_at,
  public_import,
  public_id,
  local_site,
  category,
  approval_status,
  home_page,
  title,
  name,
  long_title,
  description,
  url_templates
FROM
  output_sites
ORDER BY
  id
;
