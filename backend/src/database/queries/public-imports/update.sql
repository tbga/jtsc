WITH input_updates AS (
  SELECT
    public_import_update.*
  FROM
    json_to_recordset(${updates:json}) AS public_import_update(
      id bigint,
      status text
    )
),
updated_public_imports AS (
  UPDATE
    public_imports
  SET
    status = update_if_changed(
      input_updates.status,
      public_imports.status
    )
  FROM
    input_updates
  WHERE
    -- exclude updates for finished imports
    public_imports.status != ${invalid_import_update_status}
    AND
    input_updates.id = public_imports.id
  RETURNING
    public_imports.id
)
SELECT
  id
FROM
  updated_public_imports
;
