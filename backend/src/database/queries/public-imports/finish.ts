import { publicImportSiteCategorySchema } from "#codegen/schema/lib/entities/public-import/site/category";
import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import {
  type IPublicImport,
  publicImportStatusSchema,
  publicImportSiteApprovalStatusSchema,
} from "#entities/public-import";
import type { IPublicImportDBFinisher } from "./types.js";

const public_import_status_in_progress =
  publicImportStatusSchema.anyOf[1].const;
const public_import_status_consumed = publicImportStatusSchema.anyOf[2].const;
const public_import_site_approval_status_approved =
  publicImportSiteApprovalStatusSchema.anyOf[1].const;
const public_import_site_category_new =
  publicImportSiteCategorySchema.anyOf[0].const;
const public_import_site_category_same =
  publicImportSiteCategorySchema.anyOf[1].const;
const public_import_site_category_different =
  publicImportSiteCategorySchema.anyOf[2].const;
const query = sqlQuery("public-imports", "finish.sql");

export const finishPublicImports = dbQuery<
  IPublicImportDBFinisher[],
  IPublicImport["id"][]
>(async (finishers, ctx) => {
  const queryArgs = {
    finishers,
    public_import_status_in_progress,
    public_import_status_consumed,
    public_import_site_approval_status_approved,
    public_import_site_category_new,
    public_import_site_category_same,
    public_import_site_category_different,
  };
  const result = await ctx.many<{ id: IPublicImport["id"] }>(query, queryArgs);
  const ids = toEntityIDs(result);

  return ids;
});
