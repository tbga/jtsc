WITH input_public_imports AS (
  SELECT
    id,
    created_at,
    updated_at,
    created_by,
    public_id,
    status,
    type,
    version,
    title,
    sites_count
  FROM
    public_imports
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
consumable_public_imports AS (
  SELECT
    input_public_imports.id,
    (
      count(
        public_import_sites.id
      ) FILTER (
        WHERE
          public_import_sites.approval_status = ${public_import_site_approval_status_approved}
      ) > 0
    ) AS is_consumable
  FROM
    input_public_imports
    INNER JOIN
    public_import_sites
    ON
      input_public_imports.id = public_import_sites.public_import_id
  GROUP BY
    input_public_imports.id
  ORDER BY
    input_public_imports.id

),
output_public_imports AS (
  SELECT
    input_public_imports.id,
    input_public_imports.created_at,
    input_public_imports.updated_at,
    input_public_imports.created_by,
    input_public_imports.public_id,
    input_public_imports.status,
    input_public_imports.type,
    input_public_imports.version,
    input_public_imports.title,
    input_public_imports.title AS name,
    input_public_imports.sites_count AS sites,
    consumable_public_imports.is_consumable
  FROM
    input_public_imports
    INNER JOIN
    consumable_public_imports
    ON
      input_public_imports.id = consumable_public_imports.id

)
SELECT
  id,
  created_at,
  updated_at,
  created_by,
  public_id,
  status,
  type,
  version,
  title,
  name,
  sites,
  is_consumable
FROM
  output_public_imports
ORDER BY
  id ASC
;
