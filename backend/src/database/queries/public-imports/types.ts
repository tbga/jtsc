import type { IPaginatedQueryArgs } from "#lib/pagination";
import type { IPublicSite, IPublicExportMeta } from "#public-exports";
import type { IPublicImport } from "#entities/public-import";
import type { IAccountDB } from "#database/queries/accounts";

export interface IPublicImportDBinit extends Omit<IPublicExportMeta, "id"> {
  account_id: IAccountDB["id"];
  public_id: IPublicExportMeta["id"];
  sites?: IPublicSite[];
}

export interface IPublicImportDBUpdate
  extends Pick<IPublicImport, "id" | "status"> {}

export interface IPublicImportQueryArgs extends IPaginatedQueryArgs {}

export interface IPublicImportDBFinisher extends Pick<IPublicImport, "id"> {
  account_id: IAccountDB["id"];
}
