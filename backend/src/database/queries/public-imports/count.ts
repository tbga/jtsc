import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";

const query = sqlQuery("public-imports", "count.sql");

export const selectPublicImportCount = dbQuery<undefined, IBigIntegerPositive>(
  async (_, ctx) => {
    const { count } = await ctx.one<ITableCount>(query);

    return count;
  },
);
