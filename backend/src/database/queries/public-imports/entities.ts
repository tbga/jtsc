import { dbQuery, sqlQuery } from "#database";
import {
  publicImportSiteApprovalStatusSchema,
  type IPublicImport,
} from "#entities/public-import";

const public_import_site_approval_status_approved =
  publicImportSiteApprovalStatusSchema.anyOf[1].const;
const query = sqlQuery("public-imports", "entities.sql");

export const selectPublicImportEntities = dbQuery<
  IPublicImport["id"][],
  IPublicImport[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
    public_import_site_approval_status_approved,
  };

  const entities = await ctx.many<IPublicImport>(query, queryArgs);

  return entities;
});
