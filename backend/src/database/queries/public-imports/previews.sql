WITH input_public_imports AS (
  SELECT
    id,
    created_by,
    public_id,
    status,
    title,
    sites_count
  FROM
    public_imports
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
output_public_imports AS (
  SELECT
    id,
    created_by,
    public_id,
    status,
    title,
    title AS name,
    sites_count AS sites
  FROM
    input_public_imports
)
SELECT
  id,
  created_by,
  public_id,
  status,
  title,
  name,
  sites
FROM
  output_public_imports
ORDER By
  id ASC
;
