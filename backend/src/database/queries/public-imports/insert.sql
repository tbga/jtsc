WITH input_inits AS (
  SELECT
    nextval(
      pg_get_serial_sequence('public.public_imports', 'id')
    ) AS public_import_id,
    input_init.*
  FROM
    json_to_recordset(${inits:json}) AS input_init(
      public_id uuid,
      account_id bigint,
      type text,
      version smallint,
      title text,
      -- an array of public sites
      sites json
    )
),
import_inits AS (
  SELECT
    public_import_id AS id,
    account_id AS created_by,
    public_id,
    ${default_public_import_status} AS status,
    type,
    version,
    title,
    json_array_length(
      sites
    ) AS sites_count
  FROM
    input_inits
),
site_inits AS (
  SELECT
    input_inits.public_import_id,
    ${default_approval_status} AS approval_status,
    site_init.id AS public_id,
    site_init.home_page,
    site_init.title,
    site_init.long_title,
    site_init.description,
    url_templates_record.profile_list AS url_template_profile_list,
    url_templates_record.profile_list_notes AS url_template_profile_list_notes,
    url_templates_record.profile AS url_template_profile,
    url_templates_record.profile_notes AS url_template_profile_notes,
    url_templates_record.release_list AS url_template_release_list,
    url_templates_record.release_list_notes AS url_template_release_list_notes,
    url_templates_record.release AS url_template_release,
    url_templates_record.release_notes AS url_template_release_notes
  FROM
    input_inits
    CROSS JOIN
    json_to_recordset(input_inits.sites) AS site_init(
      id uuid,
      home_page text,
      title text,
      long_title text,
      description text,
      url_templates json
    )
    CROSS JOIN
    json_to_record(site_init.url_templates) AS url_templates_record(
      profile_list text,
      profile_list_notes text,
      profile text,
      profile_notes text,
      release_list text,
      release_list_notes text,
      release text,
      release_notes text
    )
),
new_imports AS (
  INSERT INTO public_imports
    (
      id,
      created_by,
      public_id,
      status,
      type,
      version,
      title,
      sites_count
    ) OVERRIDING SYSTEM VALUE
  SELECT
    id,
    created_by,
    public_id,
    status,
    type,
    version,
    title,
    sites_count
  FROM
    import_inits
  RETURNING
    public_imports.id
),
new_sites AS (
  INSERT INTO public_import_sites
    (
      public_import_id,
      approval_status,
      public_id,
      home_page,
      title,
      long_title,
      description,
      url_template_profile_list,
      url_template_profile_list_notes,
      url_template_profile,
      url_template_profile_notes,
      url_template_release_list,
      url_template_release_list_notes,
      url_template_release,
      url_template_release_notes
    )
  SELECT
    public_import_id,
    approval_status,
    public_id,
    home_page,
    title,
    long_title,
    description,
    url_template_profile_list,
    url_template_profile_list_notes,
    url_template_profile,
    url_template_profile_notes,
    url_template_release_list,
    url_template_release_list_notes,
    url_template_release,
    url_template_release_notes
  FROM
    site_inits
)
SELECT
  id
FROM
  new_imports
;
