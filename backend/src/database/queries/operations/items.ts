import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";
import type { IEntityItem } from "#lib/entities";

const query = sqlQuery("operations", "items.sql");

export const selectOperationItems = dbQuery<IOperation["id"][], IEntityItem[]>(
  async (ids, ctx) => {
    const queryArgs = {
      ids,
    };
    const operations = await ctx.many<IEntityItem>(query, queryArgs);

    return operations;
  },
);
