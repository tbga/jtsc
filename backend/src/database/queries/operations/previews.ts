import { dbQuery, sqlQuery } from "#database";
import type { IOperationPreview, IOperation } from "#entities/operation";

const query = sqlQuery("operations", "previews.sql");

export const selectOperationPreviews = dbQuery<
  IOperation["id"][],
  IOperationPreview[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };
  const operations = await ctx.many<IOperationPreview>(query, queryArgs);

  return operations;
});
