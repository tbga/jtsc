import { TableName } from "#database";
import type { IOperation } from "#entities/operation";
import type { IPaginatedQueryArgs } from "#lib/pagination";

export interface IOperationDBInit
  extends Pick<IOperation, "type" | "input_data" | "created_by"> {
  status: "pending";
}

export interface IOperationQueryArgs
  extends IPaginatedQueryArgs,
    Pick<Partial<IOperation>, "type" | "status"> {}

export interface IOperationDBUpdate
  extends Pick<IOperation, "id">,
    Pick<Partial<IOperation>, "status" | "result_data" | "errors"> {}

export const operationsTable = new TableName({
  schema: "public",
  table: "operations",
});
