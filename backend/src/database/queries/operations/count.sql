SELECT
  count(*)
FROM
  operations
WHERE
  (
    ${type} IS NULL
    OR
    operations.type = ${type}
  )
  AND
  (
    ${status} IS NULL
    OR
    operations.status = ${status}
  )
