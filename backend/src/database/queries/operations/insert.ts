import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";
import type { IOperationDBInit } from "./types.js";

const query = sqlQuery("operations", "insert.sql");

export const insertOperations = dbQuery<IOperationDBInit[], IOperation["id"][]>(
  async (inits, ctx) => {
    const queryArgs = {
      inits,
    };
    const result = await ctx.many<{ id: IOperation["id"] }>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
