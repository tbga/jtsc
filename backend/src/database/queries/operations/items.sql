WITH input_operations AS (
  SELECT
    id,
    type AS name
  FROM
    operations
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
  ORDER BY
    id ASC
)
SELECT
  id,
  name
FROM
  input_operations
ORDER BY
  id ASC
;
