SELECT
  id,
  created_at,
  updated_at,
  created_by,
  type,
  status,
  type AS name
FROM
  operations
WHERE
  id = ANY (
    CAST (${ids} AS bigint[])
  )
ORDER BY
  id ASC
;
