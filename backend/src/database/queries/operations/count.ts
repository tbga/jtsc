import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";
import type { ITableCount } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";
import type { IOperationQueryArgs } from "./types.js";

interface IArgs {
  type?: IOperation["type"];
  status?: IOperation["status"];
}

const query = sqlQuery("operations", "count.sql");

export const selectOperationCount = dbQuery<
  Omit<IOperationQueryArgs, "pagination">,
  IBigIntegerPositive
>(async ({ type, status }: IArgs, ctx) => {
  const queryArgs = { type, status };
  const { count } = await ctx.one<ITableCount>(query, queryArgs);

  return count;
});
