WITH updates AS (
  SELECT
    operation_update.*
  FROM
    json_to_recordset(${updates:json}) AS operation_update(
      id bigint,
      status text,
      result_data json,
      errors json
    )
)
UPDATE
  operations
SET
  status = update_if_changed(updates.status, operations.status),
  result_data = update_if_changed(updates.result_data, operations.result_data),
  errors = update_if_changed(updates.errors, operations.errors)
FROM
  updates
WHERE
  operations.id = updates.id
RETURNING
  operations.id
;
