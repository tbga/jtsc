WITH input_operations AS (
  SELECT
    id,
    created_at,
    updated_at,
    created_by,
    type,
    status,
    input_data,
    result_data,
    errors,
    type AS name
  FROM
    operations
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
  ORDER BY
    id ASC
)
SELECT
  id,
  created_at,
  updated_at,
  created_by,
  type,
  status,
  input_data,
  result_data,
  errors,
  name
FROM
  input_operations
ORDER BY
  id ASC
;
