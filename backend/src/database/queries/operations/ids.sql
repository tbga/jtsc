WITH input_ids AS (
  SELECT
    id
  FROM
    operations
  WHERE
    (
      ${type} IS NULL
      OR
      operations.type = ${type}
    )
    AND
    (
      ${status} IS NULL
      OR
      operations.status = ${status}
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_ids
ORDER BY
  id ASC
