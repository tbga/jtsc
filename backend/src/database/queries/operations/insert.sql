WITH input_inits AS (
  SELECT
    *
  FROM
    json_to_recordset(${inits:json}) AS init(
      created_by bigint,
      type text,
      status text,
      input_data json
    )
),
new_operations AS (
  INSERT INTO operations
    (
      created_by,
      type,
      status,
      input_data
    )
  SELECT
    created_by,
    type,
    status,
    input_data
  FROM
    input_inits
  RETURNING
    id
)
SELECT
  id
FROM
  new_operations
;
