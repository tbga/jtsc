import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";
import type { IOperationQueryArgs } from "./types.js";

const query = sqlQuery("operations", "ids.sql");

export const selectOperationIDs = dbQuery<
  IOperationQueryArgs,
  IOperation["id"][]
>(async ({ pagination, type, status }, ctx) => {
  const { limit, offset } = pagination;
  const queryArgs = { limit, offset, type, status };
  const result = await ctx.many<{ id: IOperation["id"] }>(query, queryArgs);
  const ids = toEntityIDs(result);

  return ids;
});
