import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";

const query = sqlQuery("operations", "entities.sql");

export const selectOperationEntities = dbQuery<
  IOperation["id"][],
  IOperation[]
>(async (ids, ctx) => {
  const queryArgs = {
    ids,
  };
  const operations = await ctx.many<IOperation>(query, queryArgs);

  return operations;
});
