import { toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IOperation } from "#entities/operation";
import type { IOperationDBUpdate } from "./types.js";

const query = sqlQuery("operations", "update.sql");

export const updateOperations = dbQuery<
  IOperationDBUpdate[],
  IOperation["id"][]
>(async (updates, ctx) => {
  const queryArgs = {
    updates,
  };
  const result = await ctx.many<{ id: IOperation["id"] }>(query, queryArgs);
  const ids = toEntityIDs(result);

  return ids;
});
