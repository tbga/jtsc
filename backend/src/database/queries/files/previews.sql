SELECT
  id,
  url,
  name,
  type,
  subtype,
  size
FROM
  files
WHERE
  id = ANY (
    CAST (${file_ids} AS bigint[])
  )
ORDER BY
  id ASC
;
