WITH input_inits AS (
  SELECT
    init.*
  FROM
    json_to_recordset(${inits:json}) AS init(
      name text,
      url text,
      type text,
      subtype text,
      size text,
      sha_256 text,
      sha_1 text,
      md5 text
    )
),
new_files AS (
  INSERT INTO files
    (
      name,
      url,
      type,
      subtype,
      size,
      sha_256,
      sha_1,
      md5
    )
  SELECT
    name,
    url,
    type,
    subtype,
    size,
    sha_256,
    sha_1,
    md5
  FROM
    input_inits
  RETURNING
    files.id
)
SELECT
  id
FROM
  new_files
;
