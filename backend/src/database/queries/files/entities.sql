WITH input_files AS (
  SELECT
    id,
    created_at,
    updated_at,
    url,
    name,
    type,
    subtype,
    size,
    sha_256,
    sha_1,
    md5
  FROM
    files
  WHERE
    id = ANY (
      CAST (${file_ids} AS bigint[])
    )
),
output_files AS (
  SELECT
    id,
    created_at,
    updated_at,
    url,
    name,
    type,
    subtype,
    size,
    (
      SELECT json_build_object(
        'sha256', sha_256,
        'sha1', sha_1,
        'md5', md5
      )
    ) AS hashes
  FROM
    input_files
)
SELECT
  id,
  created_at,
  updated_at,
  url,
  name,
  type,
  subtype,
  size,
  hashes
FROM
  output_files
ORDER BY
  id ASC
;
