import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";

const query = sqlQuery("files", "items.sql");

export const selectFileItems = dbQuery<IFile["id"][], IEntityItem[]>(
  async (file_ids, ctx) => {
    const queryArgs = { file_ids };
    const entities = await ctx.manyOrNone<IEntityItem>(query, queryArgs);

    return entities;
  },
);
