import type { ISearchQueryArgs } from "#lib/entities";
import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";

const query = sqlQuery("files", "count.sql");

/**
 * @TODO `type`/`subtype` filters.
 */
export const selectFileCount = dbQuery<ISearchQueryArgs, IBigIntegerPositive>(
  async ({ search_query }, ctx) => {
    const queryArgs = { search_query };
    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
