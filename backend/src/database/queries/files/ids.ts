import {
  type IEntityResult,
  toEntityIDs,
  type IPaginatedSearchQueryArgs,
} from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";

const query = sqlQuery("files", "ids.sql");

/**
 * @TODO tybe/subtype filters
 */
export const selectFileIDs = dbQuery<IPaginatedSearchQueryArgs, IFile["id"][]>(
  async ({ pagination, search_query }, ctx) => {
    const { limit, offset } = pagination;
    const queryArgs = { limit, offset, search_query };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
