import { dbQuery, sqlQuery } from "#database";
import type { IFilePreview, IFile } from "#entities/file";

const query = sqlQuery("files", "previews.sql");

export const selectFilePreviews = dbQuery<IFile["id"][], IFilePreview[]>(
  async (file_ids, ctx) => {
    const queryArgs = { file_ids };
    const entities = await ctx.manyOrNone<IFilePreview>(query, queryArgs);

    return entities;
  },
);
