import { toEntityIDs, type IEntityResult } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";
import type { IFileDBInit } from "./types.js";

const query = sqlQuery("files", "insert.sql");

export const insertFiles = dbQuery<IFileDBInit[], IFile["id"][]>(
  async (inits, ctx) => {
    const queryArgs = {
      inits,
    };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
