WITH deleted_files AS (
  DELETE
  FROM
    files
  WHERE
    id = ANY (
      CAST (${deletes} AS bigint[])
    )
  RETURNING
    files.id
)
SELECT
  (
    SELECT
      id
    FROM
      deleted_files
    ORDER BY
      id ASC
  ) AS result
;
