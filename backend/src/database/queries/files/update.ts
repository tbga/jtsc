import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";
import { type IEntity, toEntityIDs } from "#lib/entities";
import type { IFileDBUpdate } from "./types.js";

const query = sqlQuery("files", "update.sql");

export const updateFiles = dbQuery<IFileDBUpdate[], IFile["id"][]>(
  async (updates, ctx) => {
    const queryArgs = {
      updates,
    };

    const result = await ctx.many<{ id: IEntity["id"] }>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
