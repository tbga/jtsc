import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";

const query = sqlQuery("files", "entities.sql");

export const selectFileEntities = dbQuery<IFile["id"][], IFile[]>(
  async (file_ids, ctx) => {
    const queryArgs = { file_ids };
    const entities = await ctx.manyOrNone<IFile>(query, queryArgs);

    return entities;
  },
);
