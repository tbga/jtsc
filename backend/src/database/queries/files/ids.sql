WITH input_ids AS (
  SELECT
    id
  FROM
    files
  WHERE
    (
      ${search_query} IS NULL
      OR
      (
        name &@~ ${search_query}
        OR
        url &@~ ${search_query}
      )
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_ids
;
