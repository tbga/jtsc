WITH file_updates AS (
  SELECT
    *
  FROM
    json_to_recordset(${updates:json}) AS file_update(
      id bigint,
      url text,
      name text,
      size text,
      type text,
      subtype text,
      sha_256 text,
      sha_1 text,
      md5 text
    )
),
updated_files AS (
  UPDATE files
  SET
    url = COALESCE(NULLIF(file_updates.url, NULL), files.url),
    name = COALESCE(NULLIF(file_updates.name, NULL), files.name),
    size = COALESCE(NULLIF(file_updates.size, NULL), files.size),
    type = COALESCE(NULLIF(file_updates.type, NULL), files.type),
    subtype = COALESCE(NULLIF(file_updates.subtype, NULL), files.subtype),
    sha_256 = COALESCE(NULLIF(file_updates.sha_256, NULL), files.sha_256),
    sha_1 = COALESCE(NULLIF(file_updates.sha_1, NULL), files.sha_1),
    md5 = COALESCE(NULLIF(file_updates.md5, NULL), files.md5)
  FROM
    file_updates
  WHERE
    file_updates.id = files.id
  RETURNING
    files.id
)
SELECT
  id
FROM
  updated_files
