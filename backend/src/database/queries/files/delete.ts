import { dbQuery, sqlQuery } from "#database";
import type { IFile } from "#entities/file";

interface IResult {
  result: IFile["id"][];
}

const query = sqlQuery("files", "delete.sql");

export const deleteFiles = dbQuery<IFile["id"][], IFile["id"][]>(
  async (deletes, ctx) => {
    const queryArgs = {
      deletes,
    };
    const { result } = await ctx.one<IResult>(query, queryArgs);

    return result;
  },
);
