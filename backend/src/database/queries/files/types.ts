import { TableName } from "#database";
import type { IHashMD5, IHashSHA1, IHashSHA256 } from "#lib/strings/hashes";
import type { IFileUpdate, IFile } from "#entities/file";

export interface IFileDBInit
  extends Pick<IFile, "name" | "url" | "type" | "subtype" | "size"> {
  sha_256?: IHashSHA256;
  sha_1?: IHashSHA1;
  md5?: IHashMD5;
}

export interface IFileDBUpdate
  extends Pick<Partial<IFileUpdate>, "name">,
    Pick<Partial<IFile>, "id" | "url" | "size" | "type" | "subtype">,
    Pick<Partial<IFileDBInit>, "sha_256" | "sha_1" | "md5"> {}

export const filesTable = new TableName({
  schema: "public",
  table: "files",
});
