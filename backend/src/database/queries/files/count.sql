SELECT
  count(*)
FROM
  files
WHERE
  (
    ${search_query} IS NULL
    OR
    (
      name &@~ ${search_query}
      OR
      url &@~ ${search_query}
    )
  )
;
