import type { IPaginatedSearchQueryArgs } from "#lib/entities";
import type {
  IPublicExportUpdate,
  IPublicExport,
  IPublicExportInit,
} from "#entities/public-export";
import type { IAccountDB } from "#database/queries/accounts";

export interface IPublicExportDBInit
  extends IPublicExportInit,
    Pick<IPublicExport, "created_by"> {}

export interface IPublicExportQueryArgs
  extends Omit<IPaginatedSearchQueryArgs, "is_merged_included"> {
  public_export_ids?: IPublicExport["id"];
  account_ids?: IAccountDB["id"];
}

export interface IPublicExportDBUpdate
  extends IPublicExportUpdate,
    Pick<IPublicExport, "id">,
    Pick<Partial<IPublicExport>, "status" | "public_id"> {}
