WITH input_inits AS (
  SELECT
    nextval(pg_get_serial_sequence('public.public_exports', 'id')) AS public_export_id,
    input_init.*
  FROM
    json_to_recordset(${inits:json}) AS input_init(
      created_by bigint,
      title text,
      sites bigint[]
    )
),
-- allow only published sites
input_sites AS (
  SELECT DISTINCT
    sites.id
  FROM
    sites
  WHERE
    id IN (
      SELECT DISTINCT
        site_data.site_id
      FROM
        input_inits
        CROSS JOIN
        UNNEST(input_inits.sites) AS site_data(
          site_id
        )
    )
    AND
    sites.public_id IS NOT NULL
),
public_export_site_inits AS (
  SELECT
    input_inits.public_export_id,
    site_data.site_id
  FROM
    input_inits
    CROSS JOIN
    UNNEST(input_inits.sites) AS site_data(
      site_id
    )
  WHERE
    site_data.site_id IN (
      SELECT
        id AS site_id
      FROM
        input_sites
    )
),
new_public_exports AS (
  INSERT INTO public_exports
    (
      id,
      created_by,
      title,
      status
    ) OVERRIDING SYSTEM VALUE
  SELECT
    public_export_id AS id,
    created_by,
    title,
    ${initial_status} AS status
  FROM
    input_inits
  RETURNING
    public_exports.id
),
new_public_export_sites AS (
  INSERT INTO public_export_sites
    (
      public_export_id,
      site_id
    )
  SELECT
    public_export_id,
    site_id
  FROM
    public_export_site_inits
),
result AS (
  SELECT
    ARRAY (
      SELECT
        id
      FROM
        new_public_exports
      ORDER BY
        id
    ) AS ids
)
SELECT
  ids
FROM
  result
;
