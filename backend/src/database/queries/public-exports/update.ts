import { dbQuery, sqlQuery } from "#database";
import type { IPublicExport } from "#entities/public-export";
import type { IPublicExportDBUpdate } from "./types.js";

interface IResult {
  ids: IPublicExport["id"][];
}

const query = sqlQuery("public-exports", "update.sql");

export const updatePublicExports = dbQuery<
  IPublicExportDBUpdate[],
  IPublicExport["id"][]
>(async (updates, ctx) => {
  const queryArgs = { updates };
  const { ids } = await ctx.one<IResult>(query, queryArgs);

  return ids;
});
