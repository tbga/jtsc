WITH input_public_exports AS (
  SELECT
    id,
    title
  FROM
    public_exports
  WHERE
    id = ANY (
      CAST (${public_export_ids} AS bigint[])
    )
)
SELECT
  id,
  title AS name
FROM
  input_public_exports
ORDER BY
  id ASC
;
