import type { IPublicExport } from "#entities/public-export";
import type { ISite } from "#entities/site";

export interface IPublicExportSiteRelation {
  public_export_id: IPublicExport["id"];
  site_id: ISite["id"];
}
