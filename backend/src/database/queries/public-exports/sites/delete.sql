WITH delete_inputs AS (
  SELECT
    delete_input.*
  FROM
    json_to_recordset(${deletes:json}) AS delete_input(
      public_export_id bigint,
      site_id bigint
    )
),
input_public_exports AS (
  SELECT
    id,
    public_id
  FROM
    public_exports
  WHERE
    id IN (
      SELECT DISTINCT
        public_export_id AS id
      FROM
        delete_inputs
    )
),
deleted_public_export_sites AS (
  DELETE
  FROM
    public_export_sites
  WHERE
    id IN (
      SELECT
        id
      FROM
        delete_inputs
        INNER JOIN
        input_public_exports
        ON
          delete_inputs.public_export_id = input_public_exports.id
        INNER JOIN
        public_export_sites
        ON
          delete_inputs.public_export_id = public_export_sites.public_export_id
          AND
          delete_inputs.site_id = public_export_sites.site_id
      WHERE
        -- only allow to remove from unfinished public exports
        input_public_exports.public_id IS NULL
    )
  RETURNING
    public_export_sites.id,
    public_export_sites.public_export_id,
    public_export_sites.site_id
)
SELECT
  ARRAY(
    SELECT DISTINCT
      public_export_id
    FROM
      deleted_public_export_sites
  ) AS public_exports,
  ARRAY(
    SELECT DISTINCT
      site_id
    FROM
      deleted_public_export_sites
  ) AS sites,
  (
    SELECT json_agg(items)
    FROM (
      SELECT
        CAST (id AS text) AS id,
        CAST (public_export_id AS text) AS public_export_id,
        CAST (site_id AS text) AS site_id
      FROM
        deleted_public_export_sites
    ) AS items
  ) as public_export_sites
