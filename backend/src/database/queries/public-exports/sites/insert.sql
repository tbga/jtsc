WITH input_inits AS (
  SELECT
    init.*
  FROM
    json_to_recordset(${inits:json}) AS init(
      public_export_id bigint,
      site_id bigint
    )
),
input_public_exports AS (
  SELECT
    id,
    public_id
  FROM
    public_exports
  WHERE
    id IN (
      SELECT DISTINCT
        public_export_id AS id
      FROM
        input_inits
    )
),
input_sites AS (
  SELECT
    id,
    public_id
  FROM
    sites
  WHERE
    id IN (
      SELECT DISTINCT
        site_id AS id
      FROM
        input_inits
    )
),
new_public_export_sites AS (
  INSERT INTO public_export_sites
    (
      public_export_id,
      site_id
    )
  SELECT
    input_inits.public_export_id,
    input_inits.site_id
  FROM
    input_inits
    INNER JOIN
    input_sites
    ON
      input_inits.site_id = input_sites.id
    INNER JOIN
    input_public_exports
    ON
      input_inits.public_export_id = input_public_exports.id
  WHERE
    -- only allow published sites to be added
    input_sites.public_id IS NOT NULL
    AND
    -- only allow adding to unfinished public exports
    input_public_exports.public_id IS NULL
  RETURNING
    public_export_sites.id,
    public_export_sites.public_export_id,
    public_export_sites.site_id
)
SELECT
  ARRAY(
    SELECT DISTINCT
      public_export_id
    FROM
      new_public_export_sites
  ) AS public_exports,
  ARRAY(
    SELECT DISTINCT
      site_id
    FROM
      new_public_export_sites
  ) AS sites,
  (
    SELECT json_agg(items)
    FROM
      (
        SELECT
          CAST (id AS text) AS id,
          CAST (public_export_id AS text) AS public_export_id,
          CAST (site_id AS text) AS site_id
        FROM
          new_public_export_sites
      ) AS items
  ) AS public_export_sites
