import type { IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPublicExport } from "#entities/public-export";
import type { ISite } from "#entities/site";
import type { IPublicExportSiteRelation } from "./types.js";

interface IResult {
  public_exports: IPublicExport["id"][];
  sites: ISite["id"][];
  release_profiles: ({ id: IEntity["id"] } & IPublicExportSiteRelation)[];
}

const query = sqlQuery("public-exports", "sites", "delete.sql");

export const deletePublicExportSites = dbQuery<
  IPublicExportSiteRelation[],
  IResult
>(async (deletes, ctx) => {
  const queryArgs = { deletes };

  const result = await ctx.one<IResult>(query, queryArgs);

  return result;
});
