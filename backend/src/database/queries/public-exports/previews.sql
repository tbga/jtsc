WITH input_public_exports AS (
  SELECT
    id,
    created_by,
    title,
    status
  FROM
    public_exports
  WHERE
    id = ANY (
      CAST (${public_export_ids} AS bigint[])
    )
),
site_counts AS (
  SELECT
    input_public_exports.id AS public_export_id,
    count(public_export_sites.site_id) AS site_count
  FROM
    input_public_exports
    INNER JOIN
    public_export_sites
    ON
      input_public_exports.id = public_export_sites.public_export_id
  GROUP BY
    input_public_exports.id
),
output_public_exports AS (
  SELECT
    input_public_exports.id,
    input_public_exports.created_by,
    input_public_exports.title,
    input_public_exports.title AS name,
    input_public_exports.status,
    COALESCE (site_counts.site_count, 0) AS sites
  FROM
    input_public_exports
    LEFT JOIN
    site_counts
    ON
      input_public_exports.id = site_counts.public_export_id
)
SELECT
  id,
  created_by,
  title,
  name,
  status,
  sites
FROM
  output_public_exports
ORDER By
  id ASC
;
