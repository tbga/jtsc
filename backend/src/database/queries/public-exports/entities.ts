import { dbQuery, sqlQuery } from "#database";
import type { IPublicExport } from "#entities/public-export";

const query = sqlQuery("public-exports", "entities.sql");

export const selectPublicExportEntities = dbQuery<
  IPublicExport["id"][],
  IPublicExport[]
>(async (public_export_ids, ctx) => {
  const queryArgs = {
    public_export_ids,
  };

  const entities = await ctx.manyOrNone<IPublicExport>(query, queryArgs);

  return entities;
});
