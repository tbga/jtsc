import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";
import type { IPublicExportQueryArgs } from "./types.js";

interface IArgs
  extends Omit<IPublicExportQueryArgs, "pagination" | "public_export_ids"> {}

const query = sqlQuery("public-exports", "count.sql");

export const selectPublicExportCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ account_ids, search_query }, ctx) => {
    const queryArgs = { account_ids, search_query };

    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
