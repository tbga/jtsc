SELECT
  count(*)
FROM
  public_exports
WHERE
  (
    ${account_ids} IS NULL
    OR
    created_by = ANY (
      CAST (${account_ids} AS bigint[])
    )
  )
  AND
  (
    ${search_query} IS NULL
    OR
    (
      title &@~ ${search_query}
    )
  )
;
