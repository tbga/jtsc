import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPublicExport } from "#entities/public-export";

const query = sqlQuery("public-exports", "items.sql");

export const selectPublicExportItems = dbQuery<
  IPublicExport["id"][],
  IEntityItem[]
>(async (public_export_ids, ctx) => {
  const queryArgs = {
    public_export_ids,
  };

  const items = await ctx.manyOrNone<IEntityItem>(query, queryArgs);

  return items;
});
