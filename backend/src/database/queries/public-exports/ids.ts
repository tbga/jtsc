import { dbQuery, sqlQuery } from "#database";
import type { IPublicExport } from "#entities/public-export";
import type { IPublicExportQueryArgs } from "./types.js";

const query = sqlQuery("public-exports", "ids.sql");

export const selectPublicExportIDs = dbQuery<
  IPublicExportQueryArgs,
  IPublicExport["id"][]
>(async ({ pagination, public_export_ids, account_ids, search_query }, ctx) => {
  const { limit, offset } = pagination;
  const queryArgs = {
    limit,
    offset,
    public_export_ids,
    account_ids,
    search_query,
  };

  const { ids } = await ctx.one<{ ids: IPublicExport["id"][] }>(
    query,
    queryArgs,
  );

  return ids;
});
