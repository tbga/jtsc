import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicExport,
  IPublicExportStatus,
} from "#entities/public-export";
import type { IPublicExportDBInit } from "./types.js";

const initialStatus: IPublicExportStatus = "in-progress";
const query = sqlQuery("public-exports", "insert.sql");

export const insertPublicExports = dbQuery<
  IPublicExportDBInit[],
  IPublicExport["id"][]
>(async (inits, ctx) => {
  const queryArgs = {
    inits,
    initial_status: initialStatus,
  };

  const { ids } = await ctx.one<{ ids: IPublicExport["id"][] }>(
    query,
    queryArgs,
  );

  return ids;
});
