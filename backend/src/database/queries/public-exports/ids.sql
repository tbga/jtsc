WITH input_ids AS (
  SELECT
    id
  FROM
    public_exports
  WHERE
    (
      ${public_export_ids} IS NULL
      OR
      id = ANY (
        CAST (${public_export_ids} AS bigint[])
      )
    )
    AND
    (
      ${account_ids} IS NULL
      OR
      created_by = ANY (
        CAST (${account_ids} AS bigint[])
      )
    )
    AND
    (
      ${search_query} IS NULL
      OR
      (
        title &@~ ${search_query}
      )
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  ARRAY (
    SELECT
      id
    FROM
      input_ids
    ORDER BY
      id ASC
  ) AS ids
;
