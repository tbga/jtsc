WITH input_public_exports AS (
  SELECT
    id,
    created_at,
    updated_at,
    created_by,
    title,
    status
  FROM
    public_exports
  WHERE
    id = ANY (
      CAST (${public_export_ids} AS bigint[])
    )
),
output_public_exports AS (
  SELECT
    input_public_exports.id,
    input_public_exports.created_at,
    input_public_exports.updated_at,
    input_public_exports.created_by,
    input_public_exports.title,
    input_public_exports.title AS name,
    input_public_exports.status
  FROM
    input_public_exports
)
SELECT
  id,
  created_at,
  updated_at,
  created_by,
  title,
  name,
  status
FROM
  output_public_exports
ORDER BY
  id ASC
;
