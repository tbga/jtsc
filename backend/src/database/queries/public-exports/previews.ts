import { dbQuery, sqlQuery } from "#database";
import type {
  IPublicExportPreview,
  IPublicExport,
} from "#entities/public-export";

const query = sqlQuery("public-exports", "previews.sql");

export const selectPublicExportPreviews = dbQuery<
  IPublicExport["id"][],
  IPublicExportPreview[]
>(async (public_export_ids, ctx) => {
  const queryArgs = {
    public_export_ids,
  };

  const previews = await ctx.manyOrNone<IPublicExportPreview>(query, queryArgs);

  return previews;
});
