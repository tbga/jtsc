WITH input_names AS (
  SELECT
    id,
    full_name
  FROM
    names
  WHERE
    id = ANY(
      CAST (${ids} AS bigint[])
    )
)
SELECT
  id,
  full_name AS name
FROM
  input_names
;
