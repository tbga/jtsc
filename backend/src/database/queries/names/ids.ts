import { dbQuery, sqlQuery } from "#database";
import {
  type IPaginatedSearchQueryArgs,
  type IEntityResult,
  toEntityIDs,
} from "#lib/entities";
import type { IName } from "#entities/name";
import type { IProfile } from "#entities/profile";

const query = sqlQuery("names", "ids.sql");

interface IQueryArgs extends IPaginatedSearchQueryArgs {
  profile_ids?: IProfile["id"][];
}

export const selectNameIDs = dbQuery<IQueryArgs, IName["id"][]>(
  async ({ pagination, profile_ids, search_query }, ctx) => {
    const { limit, offset } = pagination;
    const queryArgs = { profile_ids, limit, offset, search_query };
    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
