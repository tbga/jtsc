import { TableName } from "#database";

export const namesTable = new TableName({
  schema: "public",
  table: "names",
});
