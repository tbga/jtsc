import { dbQuery, sqlQuery } from "#database";
import type { IProfile } from "#entities/profile";
import type { ISearchQueryArgs } from "#lib/entities";
import type { ITableCount } from "#lib/pagination";
import type { IBigIntegerPositive } from "#types";

interface IArgs extends ISearchQueryArgs {
  profile_ids?: IProfile["id"][];
}

const query = sqlQuery("names", "count.sql");

export const selectNameCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ search_query, profile_ids }, ctx) => {
    const queryArgs = {
      profile_ids,
      search_query,
    };
    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
