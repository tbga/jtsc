import { dbQuery, sqlQuery } from "#database";
import type { IEntityItem } from "#lib/entities";
import type { IName } from "#entities/name";

const query = sqlQuery("names", "items.sql");

export const selectNameItems = dbQuery<IName["id"][], IEntityItem[]>(
  async (ids, ctx) => {
    const queryArgs = { ids };
    const entities = await ctx.many<IEntityItem>(query, queryArgs);

    return entities;
  },
);
