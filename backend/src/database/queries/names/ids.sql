WITH input_names AS (
  SELECT
    id
  FROM
    names
  WHERE
    (
      ${profile_ids} IS NULL
      OR
      id IN (
        SELECT
          name_id AS id
        FROM
          profile_names
        WHERE
          profile_id = ANY (
            CAST (${profile_ids} AS bigint[])
          )
      )
    )
    AND
    (
      ${search_query} IS NULL
      OR
      full_name &@~ ${search_query}
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_names
;
