import type { IStatsAll } from "#codegen/schema/lib/stats/all";
import { dbQuery, sqlQuery } from "#database";

const query = sqlQuery("stats", "basic.sql");

export const selectBasicStats = dbQuery<undefined, IStatsAll>(async (_, ctx) =>
  ctx.one<IStatsAll>(query),
);
