WITH site_stats AS (
  SELECT
    (
      SELECT
        count(*)
      FROM
        sites
    ) AS all,
    (
      SELECT
        count(*)
      FROM
        sites
      WHERE
        sites.public_id IS NOT NULL
    ) AS published,
    (
      SELECT
        count(*)
      FROM
        sites
      WHERE
        sites.public_id IS NULL
    ) AS local
),
stats AS (
  SELECT
    (
      SELECT
        count(*)
      FROM
        artists
    ) AS artists,
    (
      SELECT
        count(*)
      FROM
        posts
    ) AS posts,
    (
      SELECT json_build_object(
        'all', site_stats.all,
        'published', site_stats.published,
        'local', site_stats.local
      )
    ) AS sites,
    (
      SELECT
        count(*)
      FROM
        profiles
    ) AS profiles,
    (
      SELECT
        count(*)
      FROM
        releases
    ) AS releases,
    (
      SELECT
        count(*)
      FROM
        files
    ) AS files
  FROM
    site_stats
)
SELECT
  artists,
  posts,
  sites,
  profiles,
  releases,
  files
FROM
  stats
;
