WITH stats AS (
  SELECT
    (
      SELECT
        count(*)
      FROM
        operations
    ) AS all,
    (
      SELECT
        count(*)
      FROM
        operations
      WHERE
        status = ${pending}
    ) AS pending,
    (
      SELECT
        count(*)
      FROM
        operations
      WHERE
        status = ${in_progress}
    ) AS in_progress,
    (
      SELECT
        count(*)
      FROM
        operations
      WHERE
        status = ${finished}
    ) AS finished,
    (
      SELECT
        count(*)
      FROM
        operations
      WHERE
        status = ${failed}
    ) AS failed
)
SELECT
  -- table qualifier there becase otherwise
  -- the query is seen as `SELECT ALL`
  stats.all,
  pending,
  in_progress,
  finished,
  failed
FROM
  stats
;
