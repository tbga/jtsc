import type { IAPIResponseOperationsStats } from "#codegen/schema/lib/api/account/role/administrator/operations/stats/response";
import { dbQuery, sqlQuery } from "#database";
import type { IOperationStatus } from "#entities/operation";

type IStats = IAPIResponseOperationsStats["data"];

const query = sqlQuery("stats", "operations", "basic.sql");
const queryArgs: Record<keyof Omit<IStats, "all">, IOperationStatus> = {
  pending: "pending",
  in_progress: "in-progress",
  finished: "finished",
  failed: "failed",
};

export const selectBasicOperationStats = dbQuery<undefined, IStats>(
  async (_, ctx) => {
    const stats = await ctx.one<IStats>(query, queryArgs);

    return stats;
  },
);
