import type { TableName } from "pg-promise";
import type { IEntity } from "#lib/entities";
import { randomPercentage } from "#lib/random";
import { dbQuery, sqlQuery } from "#database";

interface IArgs {
  table: TableName;
  sample?: number;
}

const query = sqlQuery("random", "ids.sql");

/**
 * @TODO add upper limit
 */
export const selectRandomIDs = dbQuery<IArgs, IEntity["id"][]>(
  async ({ table, sample }, ctx) => {
    const queryArgs = { table, sample: sample ?? randomPercentage() };
    const result = await ctx.manyOrNone<{ id: IEntity["id"] }>(
      query,
      queryArgs,
    );
    const ids = result.map(({ id }) => id);

    return ids;
  },
);
