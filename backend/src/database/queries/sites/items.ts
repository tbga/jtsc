import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";

const query = sqlQuery("sites", "items.sql");

export const selectSiteItems = dbQuery<ISite["id"][], IEntityItem[]>(
  async (ids, ctx) => {
    const queryArgs = { ids };

    const previews = await ctx.manyOrNone<IEntityItem>(query, queryArgs);

    return previews;
  },
);
