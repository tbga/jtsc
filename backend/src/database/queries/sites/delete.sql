DELETE
FROM
  sites
WHERE
  id = ANY (
    CAST (${deletes} AS bigint[])
  )
RETURNING
  id,
  title AS name
;
