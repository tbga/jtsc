WITH input_sites AS (
  SELECT
    id,
    title AS name,
    public_id
  FROM
    sites
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
)
SELECT
  id,
  name,
  public_id
FROM
  input_sites
ORDER BY
  id ASC
;
