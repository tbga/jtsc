import type { ITableCount } from "#lib/pagination";
import { dbQuery, sqlQuery } from "#database";
import type { IBigIntegerPositive } from "#types";
import type { ISiteQueryArgs } from "#database/queries/sites";

interface IArgs
  extends Omit<ISiteQueryArgs, "pagination" | "site_ids" | "public_ids"> {}

const query = sqlQuery("sites", "count.sql");

export const selectSiteCount = dbQuery<IArgs, IBigIntegerPositive>(
  async ({ public_export_ids, search_query }, ctx) => {
    const queryArgs = {
      public_export_ids,
      search_query,
    };

    const { count } = await ctx.one<ITableCount>(query, queryArgs);

    return count;
  },
);
