import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";

const query = sqlQuery("sites", "entities.sql");

export const selectSiteEntities = dbQuery<ISite["id"][], ISite[]>(
  async (ids, ctx) => {
    const queryArgs = { ids };

    const entities = await ctx.manyOrNone<ISite>(query, queryArgs);

    return entities;
  },
);
