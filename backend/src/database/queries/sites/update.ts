import { type IEntityResult, toEntityIDs } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";
import type { ISiteDBUpdate } from "./types.js";

const query = sqlQuery("sites", "update.sql");

export const updateSites = dbQuery<ISiteDBUpdate[], ISite["id"][]>(
  async (updates, ctx) => {
    const queryArgs = {
      updates,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
