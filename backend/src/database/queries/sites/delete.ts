import type { IEntityItem } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";

const query = sqlQuery("sites", "delete.sql");

export const deleteSites = dbQuery<ISite["id"][], IEntityItem[]>(
  async (deletes, ctx) => {
    const queryArgs = {
      deletes,
    };
    const sites = await ctx.many<IEntityItem>(query, queryArgs);

    return sites;
  },
);
