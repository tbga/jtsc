WITH input_inits AS (
  SELECT
    input_init.*
  FROM
    json_to_recordset(${inits:json}) AS input_init(
      home_page text,
      title text,
      long_title text,
      description text,
      url_template_profile_list text,
      url_template_profile_list_notes text,
      url_template_profile text,
      url_template_profile_notes text,
      url_template_release_list text,
      url_template_release_list_notes text,
      url_template_release text,
      url_template_release_notes text
    )
),
new_sites AS (
  INSERT INTO sites
    (
      home_page,
      title,
      long_title,
      description,
      url_template_profile_list,
      url_template_profile_list_notes,
      url_template_profile,
      url_template_profile_notes,
      url_template_release_list,
      url_template_release_list_notes,
      url_template_release,
      url_template_release_notes
    )
  SELECT
    home_page,
    title,
    long_title,
    description,
    url_template_profile_list,
    url_template_profile_list_notes,
    url_template_profile,
    url_template_profile_notes,
    url_template_release_list,
    url_template_release_list_notes,
    url_template_release,
    url_template_release_notes
  FROM
    input_inits
  RETURNING
    id
)
SELECT
  id
FROM
  new_sites
ORDER BY
  id
;
