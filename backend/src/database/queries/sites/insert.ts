import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";
import { toEntityIDs, type IEntityResult } from "#lib/entities";
import type { ISiteDBInit } from "./types.js";

const query = sqlQuery("sites", "insert.sql");

export const insertSites = dbQuery<ISiteDBInit[], ISite["id"][]>(
  async (inits, ctx) => {
    const queryArgs = {
      inits,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
