WITH input_sites AS (
  SELECT
    id,
    home_page,
    title,
    public_id,
    published_at
  FROM
    sites
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
profile_counts AS (
  SELECT
    input_sites.id AS site_id,
    count(profiles.site_id) AS profiles
  FROM
    input_sites
    INNER JOIN
    profiles
    ON
      input_sites.id = profiles.site_id
  GROUP BY
    input_sites.id
),
release_counts AS (
  SELECT
    input_sites.id AS site_id,
    count(releases.site_id) AS releases
  FROM
    input_sites
    INNER JOIN
    releases
    ON
      input_sites.id = releases.site_id
  GROUP BY
    input_sites.id
),
output_sites AS (
  SELECT
    id,
    home_page,
    title,
    public_id,
    published_at,
    title AS name,
    COALESCE(profile_counts.profiles, 0) AS profiles,
    COALESCE(release_counts.releases, 0) AS releases
  FROM
    input_sites
    LEFT JOIN
    profile_counts
    ON
      input_sites.id = profile_counts.site_id
    LEFT JOIN
    release_counts
    ON
      input_sites.id = release_counts.site_id
)
SELECT
  id,
  home_page,
  title,
  name,
  profiles,
  releases,
  public_id,
  published_at
FROM
  output_sites
ORDER BY
  id ASC
;
