import { TableName } from "#database";
import type { IPaginatedSearchQueryArgs } from "#lib/entities";
import type { ISiteInit, ISite, ISiteUpdate } from "#entities/site";
import type { IPublicExport } from "#entities/public-export";

export interface ISiteDBInit extends Omit<ISiteInit, "url_templates"> {
  url_template_profile_list?: string;
  url_template_profile_list_notes?: string;
  url_template_profile?: string;
  url_template_profile_notes?: string;
  url_template_release_list?: string;
  url_template_release_list_notes?: string;
  url_template_release?: string;
  url_template_release_notes?: string;
}

export interface ISiteDBUpdate
  extends Pick<ISite, "id">,
    Omit<ISiteUpdate, "url_templates"> {
  url_template_profile_list?: string;
  url_template_profile_list_notes?: string;
  url_template_profile?: string;
  url_template_profile_notes?: string;
  url_template_release_list?: string;
  url_template_release_list_notes?: string;
  url_template_release?: string;
  url_template_release_notes?: string;
}

export interface ISiteQueryArgs extends IPaginatedSearchQueryArgs {
  site_ids?: ISite["id"][];
  public_ids?: Required<ISite>["public_id"][];
  public_export_ids?: IPublicExport["id"][];
}

export const sitesTable = new TableName({
  schema: "public",
  table: "sites",
});
