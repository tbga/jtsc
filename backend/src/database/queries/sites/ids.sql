WITH input_sites AS (
  SELECT
    id
  FROM
    sites
  WHERE
    (
      ${site_ids} IS NULL
      OR
      id = ANY (
        CAST (${site_ids} AS bigint[])
      )
    )
    AND
    (
      ${public_ids} IS NULL
      OR
      public_id = ANY (
        CAST (${public_ids} AS uuid[])
      )
    )
    AND
    (
      ${public_export_ids} IS NULL
      OR
      id IN (
        SELECT DISTINCT
          site_id AS id
        FROM
          public_export_sites
        WHERE
          public_export_id = ANY (
            CAST (${public_export_ids} AS bigint[])
          )
      )
    )
    AND
    (
      ${search_query} IS NULL
      OR
      (
        home_page &@~ ${search_query}
        OR
        title &@~ ${search_query}
        OR
        long_title &@~ ${search_query}
      )
    )
  ORDER BY
    id ASC
  LIMIT ${limit}
  OFFSET ${offset}
)
SELECT
  id
FROM
  input_sites
ORDER BY
  id ASC
;
