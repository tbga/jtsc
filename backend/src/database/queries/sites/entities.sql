WITH input_sites AS (
  SELECT
    id,
    created_at,
    updated_at,
    home_page,
    title,
    long_title,
    description,
    public_id,
    published_at,
    published_by,
    url_template_profile_list,
    url_template_profile_list_notes,
    url_template_profile,
    url_template_profile_notes,
    url_template_release_list,
    url_template_release_list_notes,
    url_template_release,
    url_template_release_notes
  FROM
    sites
  WHERE
    id = ANY (
      CAST (${ids} AS bigint[])
    )
),
result AS (
  SELECT
    id,
    created_at,
    updated_at,
    home_page,
    title,
    title AS name,
    long_title,
    description,
    public_id,
    published_at,
    published_by,
    (
      SELECT json_build_object(
        'profile_list', url_template_profile_list,
        'profile_list_notes', url_template_profile_list_notes,
        'profile', url_template_profile,
        'profile_notes', url_template_profile_notes,
        'release_list', url_template_release_list,
        'release_list_notes', url_template_release_list_notes,
        'release', url_template_release,
        'release_notes', url_template_release_notes
      )
    ) AS url_templates
  FROM
    input_sites
)
SELECT
  id,
  created_at,
  updated_at,
  home_page,
  title,
  name,
  long_title,
  description,
  public_id,
  published_at,
  published_by,
  url_templates
FROM
  result
ORDER BY
  id ASC
;
