import { dbQuery, sqlQuery } from "#database";
import type { ISite } from "#entities/site";
import { toEntityIDs, type IEntityResult } from "#lib/entities";
import type { ISiteQueryArgs } from "./types.js";

const query = sqlQuery("sites", "ids.sql");

export const selectSiteIDs = dbQuery<ISiteQueryArgs, ISite["id"][]>(
  async (
    { pagination, site_ids, public_ids, public_export_ids, search_query },
    ctx,
  ) => {
    const { limit, offset } = pagination;
    const queryArgs = {
      limit,
      offset,
      site_ids,
      public_ids,
      public_export_ids,
      search_query,
    };

    const result = await ctx.many<IEntityResult>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
