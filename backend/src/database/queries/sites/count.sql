SELECT
  count(*)
FROM
  sites
WHERE
  (
      ${public_export_ids} IS NULL
      OR
      id IN (
        SELECT DISTINCT
          site_id AS id
        FROM
          public_export_sites
        WHERE
          public_export_id = ANY (
            CAST (${public_export_ids} AS bigint[])
          )
      )
    )
  AND
  (
    ${search_query} IS NULL
    OR
    (
      home_page &@~ ${search_query}
      OR
      title &@~ ${search_query}
      OR
      long_title &@~ ${search_query}
    )
  )
