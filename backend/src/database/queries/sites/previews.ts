import { dbQuery, sqlQuery } from "#database";
import type { ISitePreview, ISite } from "#entities/site";

const query = sqlQuery("sites", "previews.sql");

export const selectSitePreviews = dbQuery<ISite["id"][], ISitePreview[]>(
  async (ids, ctx) => {
    const queryArgs = { ids };

    const previews = await ctx.manyOrNone<ISitePreview>(query, queryArgs);

    return previews;
  },
);
