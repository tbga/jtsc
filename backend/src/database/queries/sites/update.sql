WITH input_updates AS (
  SELECT
    site_update.*
  FROM
    json_to_recordset(${updates:json}) AS site_update(
      id bigint,
      home_page text,
      title text,
      long_title text,
      description text,
      url_template_profile_list text,
      url_template_profile_list_notes text,
      url_template_profile text,
      url_template_profile_notes text,
      url_template_release_list text,
      url_template_release_list_notes text,
      url_template_release text,
      url_template_release_notes text
    )
),
updated_sites AS (
  UPDATE sites
  SET
    home_page = update_if_changed(
      input_updates.home_page,
      sites.home_page
    ),
    title = update_if_changed(
      input_updates.title,
      sites.title
    ),
    long_title = update_if_changed(
      input_updates.long_title,
      sites.long_title
    ),
    description = update_if_changed(
      input_updates.description,
      sites.description
    ),
    url_template_profile_list = update_if_changed(
      input_updates.url_template_profile_list,
      sites.url_template_profile_list
    ),
    url_template_profile_list_notes = update_if_changed(
      input_updates.url_template_profile_list_notes,
      sites.url_template_profile_list_notes
    ),
    url_template_profile = update_if_changed(
      input_updates.url_template_profile,
      sites.url_template_profile
    ),
    url_template_profile_notes = update_if_changed(
      input_updates.url_template_profile_notes,
      sites.url_template_profile_notes
    ),
    url_template_release_list = update_if_changed(
      input_updates.url_template_release_list,
      sites.url_template_release_list
    ),
    url_template_release_list_notes = update_if_changed(
      input_updates.url_template_release_list_notes,
      sites.url_template_release_list_notes
    ),
    url_template_release = update_if_changed(
      input_updates.url_template_release,
      sites.url_template_release
    ),
    url_template_release_notes = update_if_changed(
      input_updates.url_template_release_notes,
      sites.url_template_release_notes
    )
  FROM
    input_updates
  WHERE
    input_updates.id = sites.id
  RETURNING
    sites.id
)
SELECT
  id
FROM
  updated_sites
ORDER BY
  id
;
