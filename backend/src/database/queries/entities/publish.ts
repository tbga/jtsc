import type { TableName } from "pg-promise";
import { toEntityIDs, type IEntity } from "#lib/entities";
import { dbQuery, sqlQuery } from "#database";
import type { IPublishInit } from "./types.js";

interface IArgs {
  table: TableName;
  inits: IPublishInit[];
}

const query = sqlQuery("entities", "publish.sql");

export const publishEntities = dbQuery<IArgs, IEntity["id"][]>(
  async ({ table, inits }, ctx) => {
    const queryArgs = {
      table,
      inits,
    };

    const result = await ctx.many<{ id: IEntity["id"] }>(query, queryArgs);
    const ids = toEntityIDs(result);

    return ids;
  },
);
