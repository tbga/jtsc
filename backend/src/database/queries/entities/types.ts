import type { IEntity } from "#lib/entities";
import type { IAccountDB } from "#database/queries/accounts";

export interface IPublishInit {
  account_id: IAccountDB["id"];
  entity_ids: IEntity["id"][];
}
