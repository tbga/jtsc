WITH publication_inits AS (
  SELECT
    publication_init.*
  FROM
    json_to_recordset(${inits:json}) AS publication_init(
      account_id bigint,
      entity_ids bigint[]
    )
),
publications AS (
  SELECT
    publication_inits.account_id,
    entity.id
  FROM
    publication_inits
    CROSS JOIN
    unnest(publication_inits.entity_ids) AS entity(
      id
    )
),
published_entities AS (
  UPDATE ${table}
  SET
    public_id = gen_random_uuid(),
    published_at = CURRENT_TIMESTAMP,
    published_by = publications.account_id
  FROM
    publications
  WHERE
    -- do not allow republishing already published entities
    ${table}.public_id IS NULL
    AND
    publications.id = ${table}.id
  RETURNING
    ${table}.id
)
SELECT
  id
FROM
  published_entities
;
