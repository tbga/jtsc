-- Up Migration
CREATE INDEX profiles_pgroonga_original_id_index ON profiles USING pgroonga (original_id) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');
-- Down Migration
