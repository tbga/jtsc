-- Up Migration
/**
 * There is no way to conditionaly create an index on the column
 * without referencing the exact index name, so all index creations refer by name.
 * For subsequent migrations use nameless `CREATE INDEX on {table_name} ({column_names});` instead.
 */
/**
 * Extensions.
 */
CREATE EXTENSION IF NOT EXISTS pgroonga;


/*
 * Trigger functions.
 */
CREATE OR REPLACE FUNCTION trigger_set_timestamp ()
  RETURNS TRIGGER
  AS $$
BEGIN
  NEW.updated_at = CURRENT_TIMESTAMP;
  RETURN NEW;
END;
$$
LANGUAGE plpgsql;


/*
 * Primary tables.
 */
CREATE TABLE IF NOT EXISTS accounts (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  login text UNIQUE NOT NULL,
  password text NOT NULL,
  role text NOT NULL
);

DROP TRIGGER IF EXISTS set_timestamp ON accounts;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON accounts
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS accounts_role_idx ON accounts (ROLE);

CREATE TABLE IF NOT EXISTS names (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  full_name text NOT NULL
);

DROP TRIGGER IF EXISTS set_timestamp ON names;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON names
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS names_pgroonga_full_name_index ON names USING pgroonga (full_name) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE TABLE IF NOT EXISTS files (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  url text NOT NULL,
  name text NOT NULL,
  type text NOT NULL,
  subtype text NOT NULL,
  size text NOT NULL,
  sha_256 text,
  sha_1 text,
  md5 text
);

DROP TRIGGER IF EXISTS set_timestamp ON files;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON files
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE TABLE IF NOT EXISTS sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  home_page text,
  title text NOT NULL,
  long_title text,
  description text,
  public_id uuid,
  published_at timestamptz,
  published_by bigint REFERENCES accounts,
  url_template_profile_list text,
  url_template_profile text,
  url_template_release_list text,
  url_template_release text
);

DROP TRIGGER IF EXISTS set_timestamp ON sites;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS sites_pgroonga_home_page_index ON sites USING pgroonga (home_page) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX IF NOT EXISTS sites_pgroonga_title_index ON sites USING pgroonga (title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX IF NOT EXISTS sites_pgroonga_long_title_index ON sites USING pgroonga (long_title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX IF NOT EXISTS sites_public_id_idx ON sites (public_id);

CREATE INDEX IF NOT EXISTS sites_published_by_idx ON sites (published_by);

CREATE TABLE IF NOT EXISTS artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sex boolean
);

DROP TRIGGER IF EXISTS set_timestamp ON artists;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE TABLE IF NOT EXISTS profiles (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  site_id bigint REFERENCES sites,
  original_id text,
  original_bio text,
  registered_at timestamptz
);

DROP TRIGGER IF EXISTS set_timestamp ON profiles;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profiles
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();


CREATE INDEX IF NOT EXISTS profiles_site_id_idx ON profiles (site_id);

CREATE TABLE IF NOT EXISTS posts (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TRIGGER IF EXISTS set_timestamp ON posts;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON posts
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE TABLE IF NOT EXISTS releases (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  site_id bigint REFERENCES sites,
  title text,
  description text,
  original_release_id text,
  released_at_original text,
  released_at timestamptz
);

DROP TRIGGER IF EXISTS set_timestamp ON releases;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON releases
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS releases_site_id_idx ON releases (site_id);

CREATE INDEX IF NOT EXISTS releases_released_at_idx ON releases (released_at);

CREATE INDEX IF NOT EXISTS releases_pgroonga_title_index ON releases USING pgroonga (title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX IF NOT EXISTS releases_pgroonga_original_release_id_index ON releases USING pgroonga (original_release_id) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX IF NOT EXISTS releases_pgroonga_description_id_index ON releases USING pgroonga (description) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE TABLE IF NOT EXISTS operations (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  type text NOT NULL,
  status text NOT NULL,
  input_data json NOT NULL,
  result_data json,
  errors json
);

DROP TRIGGER IF EXISTS set_timestamp ON operations;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON operations
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS operations_type_idx ON operations (type);

CREATE INDEX IF NOT EXISTS operations_status_idx ON operations (status);

CREATE TABLE IF NOT EXISTS public_exports (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  title text NOT NULL,
  status text NOT NULL,
  public_id uuid
);

DROP TRIGGER IF EXISTS set_timestamp ON public_exports;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_exports
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS public_exports_created_by_idx ON public_exports (created_by);

CREATE INDEX IF NOT EXISTS public_exports_status_idx ON public_exports (status);

CREATE INDEX IF NOT EXISTS public_exports_public_id_idx ON public_exports (public_id);

CREATE TABLE IF NOT EXISTS public_imports (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  public_id uuid NOT NULL,
  type text NOT NULL,
  version smallint NOT NULL,
  title text NOT NULL,
  status text NOT NULL,
  sites_count bigint DEFAULT 0
);

DROP TRIGGER IF EXISTS set_timestamp ON public_imports;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_imports
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();


/*
 * Secondary tables.
 */
CREATE TABLE IF NOT EXISTS profile_artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  artist_id bigint NOT NULL REFERENCES artists
);

DROP TRIGGER IF EXISTS set_timestamp ON profile_artists;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS profile_artists_profile_id_idx ON profile_artists (profile_id);

CREATE INDEX IF NOT EXISTS profile_artists_artist_id_idx ON profile_artists (artist_id);

-- names of profiles
CREATE TABLE IF NOT EXISTS profile_names (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  name_id bigint NOT NULL REFERENCES names
);

DROP TRIGGER IF EXISTS set_timestamp ON profile_names;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_names
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

-- idx1 indices are due to copypasta error of statements for another table
CREATE INDEX IF NOT EXISTS profile_names_profile_id_idx ON profile_names (profile_id);

CREATE INDEX IF NOT EXISTS profile_names_name_id_idx ON profile_names (name_id);

-- portraits of profiles
CREATE TABLE IF NOT EXISTS profile_portraits (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  file_id bigint NOT NULL REFERENCES files
);

DROP TRIGGER IF EXISTS set_timestamp ON profile_portraits;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_portraits
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS profile_portraits_created_at ON profile_portraits (created_at);

CREATE INDEX IF NOT EXISTS profile_portraits_updated_at_idx ON profile_portraits (updated_at);

CREATE INDEX IF NOT EXISTS profile_portraits_profile_id_idx ON profile_portraits (profile_id);

CREATE INDEX IF NOT EXISTS profile_portraits_file_id_idx ON profile_portraits (file_id);

-- posts related to releases
CREATE TABLE IF NOT EXISTS release_posts (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  post_id bigint NOT NULL REFERENCES posts,
  UNIQUE (release_id, post_id)
);

DROP TRIGGER IF EXISTS set_timestamp ON release_posts;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_posts
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS release_posts_release_id_idx ON release_posts (release_id);

CREATE INDEX IF NOT EXISTS release_posts_post_id_idx ON release_posts (post_id);

-- preview files of releases
CREATE TABLE IF NOT EXISTS release_previews (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  file_id bigint NOT NULL REFERENCES files
);

DROP TRIGGER IF EXISTS set_timestamp ON release_previews;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_previews
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS release_previews_release_id_idx ON release_previews (release_id);

CREATE INDEX IF NOT EXISTS release_previews_file_id_idx ON release_previews (file_id);

CREATE TABLE IF NOT EXISTS release_files (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  file_id bigint NOT NULL REFERENCES files
);

DROP TRIGGER IF EXISTS set_timestamp ON release_previews;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_previews
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS release_files_release_id_idx ON release_files (release_id);

CREATE INDEX IF NOT EXISTS release_files_file_id_idx ON release_files (file_id);

CREATE TABLE IF NOT EXISTS post_artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  post_id bigint NOT NULL REFERENCES posts,
  artist_id bigint NOT NULL REFERENCES artists,
  UNIQUE (post_id, artist_id)
);

DROP TRIGGER IF EXISTS set_timestamp ON post_artists;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON post_artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS post_artists_post_id_idx ON post_artists (post_id);

CREATE INDEX IF NOT EXISTS post_artists_artist_id_idx ON post_artists (artist_id);

CREATE TABLE IF NOT EXISTS release_profiles (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  profile_id bigint NOT NULL REFERENCES profiles,
  UNIQUE (release_id, profile_id)
);

DROP TRIGGER IF EXISTS set_timestamp ON release_profiles;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_profiles
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS release_profiles_release_id_idx ON release_profiles (release_id);

CREATE INDEX IF NOT EXISTS release_profiles_profile_id_idx ON release_profiles (profile_id);

CREATE TABLE IF NOT EXISTS public_export_sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  public_export_id bigint NOT NULL REFERENCES public_exports,
  site_id bigint NOT NULL REFERENCES sites,
  UNIQUE (public_export_id, site_id)
);

DROP TRIGGER IF EXISTS set_timestamp ON public_export_sites;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_export_sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX IF NOT EXISTS public_export_sites_public_export_id_idx ON public_export_sites (public_export_id);

CREATE INDEX IF NOT EXISTS public_export_sites_site_id_idx ON public_export_sites (site_id);

CREATE TABLE IF NOT EXISTS public_import_sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  public_id uuid NOT NULL,
  public_import_id bigint NOT NULL REFERENCES public_imports,
  approval_status text NOT NULL,
  home_page text NOT NULL,
  title text NOT NULL,
  long_title text,
  description text,
  url_template_profile_list text,
  url_template_profile text,
  url_template_release_list text,
  url_template_release text,
  UNIQUE (public_id, public_import_id)
);

DROP TRIGGER IF EXISTS set_timestamp ON public_import_sites;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_import_sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();


/**
 * Functions.
 */
CREATE OR REPLACE FUNCTION update_if_changed (new_value ANYELEMENT, old_value ANYELEMENT, is_null_allowed boolean DEFAULT FALSE)
  RETURNS ANYELEMENT
  LANGUAGE plpgsql
  AS $$
BEGIN
  IF is_null_allowed = FALSE AND new_value IS NULL THEN
    RETURN old_value;
  ELSE
    RETURN new_value;
  END IF;
END;
$$;

-- Down Migration
