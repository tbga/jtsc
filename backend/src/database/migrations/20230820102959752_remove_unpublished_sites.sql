-- Up Migration
/**
 * Prune unpublished sites from
 * unfinished public exports.
 */
WITH input_public_exports AS (
  SELECT
    id
  FROM
    public_exports
  WHERE
    -- finished exports get assigned with public ID
    public_id IS NULL
),
input_public_export_sites AS (
  SELECT
    public_export_sites.id
  FROM
    public_export_sites
    LEFT JOIN
    sites
    ON
      public_export_sites.site_id = sites.id
  WHERE
    public_export_id IN (
      SELECT
        id AS public_export_id
      FROM
        input_public_exports
    )
    AND
    sites.public_id IS NULL
)
DELETE
FROM
  public_export_sites
WHERE
  id IN (
    SELECT
      id
    FROM
      input_public_export_sites
  )
;
-- Down Migration
