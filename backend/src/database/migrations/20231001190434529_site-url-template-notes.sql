-- Up Migration
ALTER TABLE sites
  ADD COLUMN url_template_profile_list_notes text;

ALTER TABLE sites
  ADD COLUMN url_template_profile_notes text;

ALTER TABLE sites
  ADD COLUMN url_template_release_list_notes text;

ALTER TABLE sites
  ADD COLUMN url_template_release_notes text;

-- Down Migration
