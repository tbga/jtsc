import path from "node:path";
import { isNativeError } from "node:util/types";
import pgPromise, { type IInitOptions, type ITask } from "pg-promise";
import pgMonitor from "pg-monitor";
import {
  DATABASE_URL,
  IS_DATABASE_LOGGING_ENABLED,
} from "#environment/variables.js";
import { DATABASE_QUERIES_FOLDER } from "#environment/derived-variables.js";
import { DatabaseError } from "./errors.js";

const { QueryFile, errors } = pgPromise;
const { QueryFileError } = errors;

const initOptions: IInitOptions = {
  capSQL: true,
};
const pgp = pgPromise(initOptions);
// exporting it this way because typescript cannot into direct re-exports from `"pg-promise"`
export const { ColumnSet, Column, TableName } = pgp.helpers;

if (IS_DATABASE_LOGGING_ENABLED) {
  pgMonitor.attach(initOptions);
}

// Type id-s supported by PostgreSQL, copied from:
// http://github.com/brianc/node-pg-types/blob/master/lib/builtins.js
// return date strings as strict subset of RFC3339 strings
pgp.pg.types.setTypeParser(1184, (dateString) => {
  const rfc3339Date = dateString.replace(" ", "T");

  return rfc3339Date;
});
// return bigints as `BigInt` instead of string
// pgp.pg.types.setTypeParser(20, BigInt);

const database = pgp(DATABASE_URL);

// doing that juggling so the signature wouldn't look humongous
type IContext = Omit<
  typeof database,
  "connect" | "$config" | "$cn" | "$dc" | "$pool"
>;
export interface IDBContext extends IContext {}
export type IQueryFunc<ArgsType, ReturnShape> = (
  args: ArgsType,
  dbContext: IDBContext,
) => Promise<ReturnShape>;

/**
 * For terminating CLI commands
 * @link https://github.com/vitaly-t/pg-promise#library-de-initialization
 */
export async function terminateDatabase() {
  return pgp.end();
}
export function dbQuery<ArgsType = unknown, ReturnShape = unknown>(
  queryFunc: IQueryFunc<ArgsType, ReturnShape>,
) {
  async function queryDB(
    args: ArgsType,
    databaseContext?: IDBContext,
  ): Promise<ReturnShape> {
    try {
      const result = await queryFunc(
        args,
        databaseContext ? databaseContext : database,
      );

      return result;
    } catch (error) {
      if (!isNativeError(error) || error instanceof DatabaseError) {
        throw error;
      }

      if (error instanceof QueryFileError) {
        throw new DatabaseError(
          `Failed to run the query file "${error.file}".`,
          {
            cause: error,
          },
        );
      }

      throw new DatabaseError("Failed to run the query.", { cause: error });
    }
  }

  return queryDB;
}

export async function startTransaction<ReturnShape = unknown>(
  // biome-ignore lint/complexity/noBannedTypes: fuck generics I guess
  dbFunc: (task: ITask<{}>) => Promise<ReturnShape>,
) {
  return database.tx(async (task) => {
    return dbFunc(task);
  });
}

/**
 * @param pathSegments Path segments to the `.sql` file
 * relative to the query folder.
 */
export function sqlQuery(...pathSegments: string[]) {
  const isSQLFile = pathSegments[pathSegments.length - 1].endsWith(".sql");

  if (!isSQLFile) {
    throw new DatabaseError(
      `Path "${path.join(...pathSegments)}" is not a query file.`,
    );
  }

  const fullPath = path.join(DATABASE_QUERIES_FOLDER, ...pathSegments);

  return new QueryFile(fullPath, { minify: true });
}

/**
 * A function which generates the multi-insert query.
 */
export function sqlInsert(...args: Parameters<typeof pgp.helpers.insert>) {
  const query = pgp.helpers.insert(...args);

  return query;
}
