import { ProjectError } from "#lib/errors";

export class DatabaseError extends ProjectError {}
