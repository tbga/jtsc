CREATE TABLE profiles (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  site_id bigint REFERENCES sites,
  original_id text,
  original_bio text,
  registered_at timestamptz
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profiles
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON profiles (site_id);

CREATE INDEX profiles_pgroonga_original_id_index ON profiles USING pgroonga (original_id) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE TABLE profile_artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  artist_id bigint NOT NULL REFERENCES artists
);

CREATE INDEX ON profile_artists (profile_id);

CREATE INDEX ON profile_artists (artist_id);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

-- Names of the profile
CREATE TABLE profile_names (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  name_id bigint NOT NULL REFERENCES names
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_names
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON profile_names (profile_id);

CREATE INDEX ON profile_names (name_id);

-- Portraits of the profile
CREATE TABLE profile_portraits (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  profile_id bigint NOT NULL REFERENCES profiles,
  file_id bigint NOT NULL REFERENCES files
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON profile_portraits
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON profile_portraits (created_at);

CREATE INDEX ON profile_portraits (updated_at);

CREATE INDEX ON profile_portraits (profile_id);

CREATE INDEX ON profile_portraits (file_id);
