CREATE TABLE files (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  url text NOT NULL,
  name text NOT NULL,
  type text NOT NULL,
  subtype text NOT NULL,
  size text NOT NULL,
  sha_256 text,
  sha_1 text,
  md5 text
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON files
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();
