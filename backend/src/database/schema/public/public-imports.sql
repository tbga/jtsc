CREATE TABLE public_imports (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  status text NOT NULL,
  public_id uuid NOT NULL,
  type text NOT NULL,
  version smallint NOT NULL,
  title text NOT NULL,
  -- the total amount of sites is known at insert time
  -- and doesn't change
  sites_count bigint DEFAULT 0
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_imports
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE TABLE public_import_sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  public_import_id bigint NOT NULL REFERENCES public_imports,
  approval_status text NOT NULL,
  public_id uuid NOT NULL,
  home_page text NOT NULL,
  title text NOT NULL,
  long_title text,
  description text,
  url_template_profile_list text,
  url_template_profile_list_notes text,
  url_template_profile text,
  url_template_profile_notes text,
  url_template_release_list text,
  url_template_release_list_notes text,
  url_template_release text,
  url_template_release_notes text,
  UNIQUE (public_id, public_import_id)
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_import_sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();
