-- The releases of posts by profiles.
CREATE TABLE releases (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  site_id bigint REFERENCES sites,
  title text,
  description text,
  original_release_id text,
  released_at timestamptz,
  released_at_original text
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON releases
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON releases (site_id);

CREATE INDEX ON releases (released_at);

CREATE INDEX releases_pgroonga_title_index ON releases USING pgroonga (title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX releases_pgroonga_original_release_id_index ON releases USING pgroonga (original_release_id) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX releases_pgroonga_description_id_index ON releases USING pgroonga (description) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

-- The posts related to the release.
CREATE TABLE release_posts (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  post_id bigint NOT NULL REFERENCES posts,
  UNIQUE (release_id, post_id)
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_posts
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON release_posts (release_id);

CREATE INDEX ON release_posts (post_id);

-- Preview files of the release.
CREATE TABLE release_previews (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  file_id bigint NOT NULL REFERENCES files
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_previews
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON release_previews (release_id);

CREATE INDEX ON release_previews (file_id);

CREATE TABLE release_files (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  file_id bigint NOT NULL REFERENCES files
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_files
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON release_files (release_id);

CREATE INDEX ON release_files (file_id);

CREATE TABLE release_profiles (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  release_id bigint NOT NULL REFERENCES releases,
  profile_id bigint NOT NULL REFERENCES profiles,
  UNIQUE (release_id, profile_id)
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON release_profiles
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON release_profiles (release_id);

CREATE INDEX ON release_profiles (profile_id);
