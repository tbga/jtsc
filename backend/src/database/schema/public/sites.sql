CREATE TABLE sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  home_page text,
  title text NOT NULL,
  long_title text,
  description text,
  public_id uuid,
  published_at timestamptz,
  published_by bigint REFERENCES accounts,
  url_template_profile_list text,
  url_template_profile_list_notes text,
  url_template_profile text,
  url_template_profile_notes text,
  url_template_release_list text,
  url_template_release_list_notes text,
  url_template_release text,
  url_template_release_notes text
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX sites_pgroonga_home_page_index ON sites USING pgroonga (home_page) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX sites_pgroonga_title_index ON sites USING pgroonga (title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX sites_pgroonga_long_title_index ON sites USING pgroonga (long_title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE INDEX ON sites (public_id);

CREATE INDEX ON sites (published_by);
