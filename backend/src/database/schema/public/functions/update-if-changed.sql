/**
 * Using a stored procedure because `COALESCE(NULLIF(new_value, NULL), old_value)`
 * chokes on `json` values.
 *
 * https://medium.com/developer-rants/conditional-update-in-postgresql-a27ddb5dd35
 */
CREATE FUNCTION update_if_changed (
  new_value ANYELEMENT,
  old_value ANYELEMENT,
  is_null_allowed boolean DEFAULT FALSE
)
RETURNS ANYELEMENT
LANGUAGE plpgsql
AS
$$
BEGIN
  IF
    is_null_allowed = FALSE
    AND
    new_value IS NULL
  THEN
    RETURN old_value;
  ELSE
    RETURN new_value;
  END IF;
END;
$$;
