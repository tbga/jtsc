CREATE TABLE public_exports (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  title text NOT NULL,
  status text NOT NULL,
  public_id uuid
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_exports
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON public_exports (created_by);

CREATE INDEX ON public_exports (status);

CREATE INDEX ON public_exports (public_id);

CREATE INDEX public_exports_pgroonga_title_index ON public_exports USING pgroonga (title) WITH (tokenizer = 'TokenNgram("unify_alphabet", false, "unify_symbol", false, "unify_digit", false)');

CREATE TABLE public_export_sites (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  public_export_id bigint NOT NULL REFERENCES public_exports,
  site_id bigint NOT NULL REFERENCES sites,
  UNIQUE (public_export_id, site_id)
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON public_export_sites
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON public_export_sites (public_export_id);

CREATE INDEX ON public_export_sites (site_id);
