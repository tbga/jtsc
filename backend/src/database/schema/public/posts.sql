CREATE TABLE posts (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON posts
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE TABLE post_artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  post_id bigint NOT NULL REFERENCES posts,
  artist_id bigint NOT NULL REFERENCES artists,
  UNIQUE (post_id, artist_id)
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON post_artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON post_artists (post_id);

CREATE INDEX ON post_artists (artist_id);
