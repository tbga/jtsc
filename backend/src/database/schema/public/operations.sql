CREATE TABLE operations (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by bigint NOT NULL REFERENCES accounts,
  type text NOT NULL,
  status text NOT NULL,
  input_data json NOT NULL,
  result_data json,
  -- array
  errors json
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON operations
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();

CREATE INDEX ON operations (type);

CREATE INDEX ON operations (status);
