CREATE TABLE artists (
  id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sex boolean
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON artists
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp ();
