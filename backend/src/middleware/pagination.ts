import type { NextFunction, Request, Response } from "express";
import { InputValidationError } from "#lib/api";
import { PaginationError, validatePageNumber } from "#lib/pagination";

const defaultParamNames = ["page"] as const;
/**
 * Create a page number validator middleware.
 * @param paramNames
 * A list of param names which will be validated as page numbers.
 * @returns A middleware which validates page params.
 */
export function createPageNumberValidator(paramNames = defaultParamNames) {
  async function validatePageValue(
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    try {
      for (const paramName of paramNames) {
        const value = req.params[paramName];

        validateClientPageNumber(value);
      }

      next();
    } catch (error) {
      next(error);
    }
  }

  return validatePageValue;
}

/**
 * A request-specific validator.
 */
export function validateClientPageNumber(
  ...args: Parameters<typeof validatePageNumber>
) {
  try {
    validatePageNumber(...args);
  } catch (error) {
    if (!PaginationError.isError(error)) {
      throw error;
    }
    const [current, total] = args;
    throw new InputValidationError(
      `Failed to validate page number ${current}${
        !total ? "" : ` out of ${total}`
      }.`,
      { cause: error },
    );
  }
}
