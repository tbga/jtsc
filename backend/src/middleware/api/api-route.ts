import type { NextFunction, Request, Response } from "express";
import { APISuccess, type IAPIResponse } from "#lib/api";
import { HTTP_STATUS } from "#lib/http";

export function apiRoute<
  Params,
  ResBody,
  ReqBody,
  Query = URLSearchParams,
  Locals extends Record<string, unknown> = Record<string, unknown>,
>(
  func: (
    req: Request<Params, ResBody, ReqBody, Query, Locals>,
    res: Response<IAPIResponse<ResBody>, Locals>,
  ) => Promise<ResBody>,
) {
  async function route(
    req: Request<Params, ResBody, ReqBody, Query, Locals>,
    res: Response<IAPIResponse<ResBody>, Locals>,
    next: NextFunction,
  ) {
    try {
      const responseBody = await func(req, res);

      return res.status(HTTP_STATUS.OK).json(new APISuccess(responseBody));
    } catch (error) {
      next(error);
    }
  }

  return route;
}
