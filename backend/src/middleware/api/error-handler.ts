import { isNativeError } from "node:util/types";
import type { ErrorRequestHandler } from "express";
import { APIFailure } from "#lib/api";
import { ClientError } from "#lib/errors";
import { HTTP_STATUS } from "#lib/http";

export const errorHandler: ErrorRequestHandler = async (
  error,
  req,
  res,
  next,
) => {
  // log the full error but return only client error message
  if (ClientError.isError(error)) {
    console.log(error);

    return res.status(error.status).json(new APIFailure(error.message));
  }

  if (isNativeError(error)) {
    console.log(error);

    return res
      .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
      .json(new APIFailure("Unknown Error"));
  }

  next(error);
};
