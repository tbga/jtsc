import type { RequestHandler } from "express";
import { IS_PUBLIC } from "#environment/variables.js";
import { InputValidationError } from "#lib/api";
import { HTTP_STATUS } from "#lib/http";
import { ClientError } from "#lib/errors";
import { getSession, SESSION_COOKIE_NAME } from "#lib/session";
import { UnauthorizedError } from "#lib/server";
import { startTransaction } from "#database";
import { getAccount } from "#entities/account";

export const apiHandler: RequestHandler = async (req, res, next) => {
  try {
    if (req.method === "OPTIONS") {
      return res.sendStatus(HTTP_STATUS.OK);
    }

    const url = new URL(req.originalUrl, `${req.protocol}://${req.hostname}`);
    const isAuthRoute = url.pathname.startsWith("/api/v1/authentication");

    if (!isAuthRoute && !IS_PUBLIC) {
      const sessionCookie: unknown = req.cookies[SESSION_COOKIE_NAME];

      if (!sessionCookie) {
        throw new UnauthorizedError();
      }

      const session = await getSession(req, res);
      const { account_id } = session;

      if (!account_id) {
        throw new UnauthorizedError();
      }
      // biome-ignore lint/suspicious/noImplicitAnyLet: no nesting please
      let account;

      try {
        account = await startTransaction(async (ctx) => {
          const account = await getAccount(account_id, ctx);
          return account;
        });
      } catch (error) {
        throw new UnauthorizedError("Unauthorized", { cause: error });
      }

      if (!account) {
        throw new UnauthorizedError();
      }

      res.locals.account = account;
    }

    const isEmptyRequest = req.method === "GET" || req.method === "HEAD";

    if (isEmptyRequest) {
      return next();
    }

    const isJSONRequest =
      req.accepts("application/json") && req.is("application/json");

    if (!isJSONRequest) {
      throw new ClientError(
        "'Content-Type' and 'Accept' headers must be 'application/json'",
        { status: HTTP_STATUS.NOT_ACCEPTABLE },
      );
    }

    const isAPIrequest =
      "data" in req.body &&
      (typeof req.body.data === "object" || typeof req.body.data === "string");

    if (!isAPIrequest) {
      throw new InputValidationError("Invalid request body shape.");
    }

    next();
  } catch (error) {
    next(error);
  }
};
