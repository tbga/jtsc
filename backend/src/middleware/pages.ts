import path from "node:path";
import { isNativeError } from "node:util/types";
import type { Router, ErrorRequestHandler, RequestHandler } from "express";
import { IS_PUBLIC } from "#environment/variables.js";
import { PROJECT_ROOT, PUBLIC_FOLDER } from "#environment/derived-variables.js";
import { readJSONFile } from "#lib/fs";
import { log } from "#lib/logs";
import { getSession, SESSION_COOKIE_NAME } from "#lib/session";
import { UnauthorizedError } from "#lib/server";
import { ClientError } from "#lib/errors";
import { HTTP_STATUS } from "#lib/http";
import { startTransaction } from "#database";
import { getAccount, type IAccount } from "#entities/account";
import type { IPageRouteMap } from "./types.js";

export async function setupPages(router: Router) {
  const pathMapPath = path.join(PROJECT_ROOT, "dist", "path-info.json");
  const pathMap = await readJSONFile<IPageRouteMap>(pathMapPath);
  const pathMapEntries = Object.entries(pathMap);

  log(`Setting up ${pathMapEntries.length} static pages.`);

  // router.use(authHandler);

  for await (const [nextjsRoute, publicPath] of pathMapEntries) {
    const expressRoute = nextjsPathToExpressPath(nextjsRoute);

    router.get(expressRoute, async (req, res, next) => {
      try {
        /**
         * Types do not provide the options:
         * https://expressjs.com/en/api.html#res.sendFile
         */
        res.sendFile(publicPath, { root: PUBLIC_FOLDER }, (error) => {
          next(error);
        });
      } catch (error) {
        next(error);
      }
    });
  }

  router.use(errorHandler);

  return router;
}

const nextjsPathParamRegex = /\[(\w+)\]/gi;

/**
 * Creates an express path out of a nextjs path for use in routes.
 */
function nextjsPathToExpressPath(nextjsPath: string) {
  return nextjsPath.replaceAll(
    nextjsPathParamRegex,
    // the value is captured along with angled brackets
    // and they are removed on transform
    (value) => `:${value.slice(1, -1)}`,
  );
}

const authHandler: RequestHandler<
  unknown,
  unknown,
  unknown,
  URLSearchParams,
  { account: IAccount }
> = async (req, res, next) => {
  try {
    const isAuthRoute = req.path.startsWith("/authentication");

    if (!isAuthRoute && !IS_PUBLIC) {
      const sessionCookie: unknown = req.cookies[SESSION_COOKIE_NAME];

      if (!sessionCookie) {
        throw new UnauthorizedError();
      }

      const session = await getSession(req, res);
      const { account_id } = session;

      if (!account_id) {
        throw new UnauthorizedError();
      }

      const account = await startTransaction(async (ctx) => {
        return getAccount(account_id, ctx);
      });

      if (!account) {
        throw new UnauthorizedError();
      }

      res.locals.account = account;
    }

    next();
  } catch (error) {
    next(error);
  }
};

const errorHandler: ErrorRequestHandler = async (error, req, res, next) => {
  // log the full error but return only client error message
  if (ClientError.isError(error)) {
    console.log(error);

    let status: number;

    switch (error.status) {
      case HTTP_STATUS.UNAUTHORIZED:
      case HTTP_STATUS.NOT_FOUND: {
        status = error.status;
        break;
      }

      default:
        status = HTTP_STATUS.INTERNAL_SERVER_ERROR;
    }

    return res.redirect(`/${status}`);
  }

  if (isNativeError(error)) {
    console.log(error);

    return res.redirect("/500");
  }

  next(error);
};
