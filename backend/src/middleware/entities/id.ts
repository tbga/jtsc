import type { NextFunction, Request, Response } from "express";
import { InputValidationError } from "#lib/api";
import { EntityError, validateEntityID } from "#lib/entities";

/**
 * Create an entity ID validator middleware.
 * @param paramNames
 * A list of param names which will be validated as entity IDs.
 * @returns A middleware which validates entity ID params.
 */
export function createEntityIDValidator(paramNames = ["id"]) {
  async function validate(req: Request, res: Response, next: NextFunction) {
    try {
      for (const paramName of paramNames) {
        const value = req.params[paramName];

        validateInputEntityID(value);
      }

      next();
    } catch (error) {
      next(error);
    }
  }

  return validate;
}

/**
 * A request-specific validator.
 */
export function validateInputEntityID(
  ...args: Parameters<typeof validateEntityID>
) {
  try {
    validateEntityID(...args);
  } catch (error) {
    if (!EntityError.isError(error)) {
      throw error;
    }

    throw new InputValidationError(error.message, { cause: error });
  }
}
