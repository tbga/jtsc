export type IEntityValidator<EntityShape = unknown> = (
  inputData: unknown,
) => inputData is EntityShape;
