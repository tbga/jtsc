import { InputValidationError } from "#lib/api";
import { ValidationError } from "#lib/json-schema";
import type { NextFunction, Request, Response } from "express";
import type { IEntityValidator } from "./types.js";

export function createBodyValidator<BodyShape = unknown>(
  validator: IEntityValidator<BodyShape>,
) {
  async function validate(
    req: Request<unknown, unknown, BodyShape, URLSearchParams>,
    res: Response,
    next: NextFunction,
  ) {
    try {
      const entity = req.body;

      validateClientEntity(entity, validator);

      next();
    } catch (error) {
      next(error);
    }
  }

  return validate;
}

export function validateClientEntity<EntityShape = unknown>(
  entity: unknown,
  validator: IEntityValidator<EntityShape>,
) {
  try {
    validator(entity);
  } catch (error) {
    if (!ValidationError.isError(error)) {
      throw error;
    }

    throw new InputValidationError(error.message, { cause: error });
  }
}
