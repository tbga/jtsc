import type { IAccountDB } from "#database/queries/accounts";

/**
 * A map of nextjs routes and their public files.
 */
export interface IPageRouteMap extends Record<string, string> {}

/**
 * A map of express routes and their nextjs routes.
 */
export interface IRoutePageMap extends Record<string, string> {}

/**
 * @TODO write it without extending `Record`.
 */
export interface IAccountLocals extends Record<string, unknown> {
  account: IAccountDB;
}
