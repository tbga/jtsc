import type { RequestHandler } from "express";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";
import { IS_HTTPS_ENABLED } from "#environment/variables.js";

export const corsHandler: RequestHandler = async (req, res, next) => {
  // add nextjs dev server to allowed origins
  if (IS_DEVELOPMENT) {
    const protocol = IS_HTTPS_ENABLED ? "https" : "http";

    if (req.method === "OPTIONS") {
      res.set("Access-Control-Allow-Headers", ["content-type", "cookie"]);
      res.set("Access-Control-Allow-Methods", [
        "GET",
        "POST",
        "PUT",
        "PATCH",
        "DELETE",
      ]);
    }

    res.set("Access-Control-Allow-Credentials", "true");
    res.set("Access-Control-Allow-Origin", `${protocol}://localhost:3000`);
  }

  next();
};
