import path from "node:path";
import { cwd } from "node:process";
import pgConnectionString from "pg-connection-string";
import {
  PROJECT_ENV,
  PUBLIC_PATH,
  DATABASE_URL,
  STORAGE_ORIGIN,
  PROJECT_FOLDER,
  SCHEMA_PATH,
  DATABASE_FOLDER,
} from "./variables.js";

export const IS_DEVELOPMENT = PROJECT_ENV === "development";
export const DATABASE_CONNECTION = pgConnectionString.parse(DATABASE_URL);
export const STORAGE_URL = new URL(STORAGE_ORIGIN);
export const PROJECT_ROOT = PROJECT_FOLDER ?? cwd();
export const PUBLIC_FOLDER =
  PUBLIC_PATH ?? path.join(PROJECT_ROOT, "dist", "public");
export const SCHEMA_FOLDER =
  SCHEMA_PATH ?? path.join(PROJECT_ROOT, "..", "schema");

export const DATABASE_QUERIES_FOLDER = path.join(
  PROJECT_ROOT,
  "src",
  "database",
  "queries",
);
export const ASSETS_FOLDER = path.join(PROJECT_ROOT, "src", "assets");
export const DATABASE_BACKUPS_FOLDER = path.join(DATABASE_FOLDER, "backups");
// @TODO better detection of system folder
export const SYSTEM_FOLDER = path.join(cwd(), "..", ".system", PROJECT_ENV);
