import { resolveConfiguration, resolveEnvironment } from "#lib/configs";

const env = await resolveEnvironment();
const config = await resolveConfiguration();

export const PROJECT_ENV = env.PROJECT_ENV;
/**
 * Use it only for 3rd party applications
 * which make assumptions
 * about specific `NODE_ENV` values.
 */
export const NODE_ENV = env.NODE_ENV;
export const SECRET_KEY = config.SECRET_KEY;
export const ADMIN_INVITE = !config.ADMIN_INVITE
  ? undefined
  : config.ADMIN_INVITE;
export const IS_PUBLIC = config.IS_PUBLIC;
export const IS_INVITE_ONLY = config.IS_INVITE_ONLY;
export const PROJECT_FOLDER = config.BACKEND_PATH;
export const DATABASE_FOLDER = config.DATABASE_FOLDER;
export const PUBLIC_PATH = config.PUBLIC_PATH;
export const DATABASE_URL = config.DATABASE_URL;
export const IS_DATABASE_LOGGING_ENABLED = config.IS_DATABASE_LOGGING_ENABLED;
export const LOCAL_STORAGE_FOLDER = config.LOCAL_STORAGE_FOLDER;
export const PUBLIC_STORAGE_FOLDER = config.PUBLIC_STORAGE_FOLDER;
export const TEMPORARY_FOLDER = config.TEMPORARY_FOLDER;
export const PUBLIC_EXPORTS_FOLDER = config.PUBLIC_EXPORTS_FOLDER;
export const SCHEMA_PATH = config.SCHEMA_PATH;
export const STORAGE_ORIGIN = config.STORAGE_ORIGIN;
export const IS_HTTPS_ENABLED = config.IS_HTTPS_ENABLED;
export const BACKEND_PORT = config.BACKEND_PORT;
export const SECURE_KEY_PATH = config.SECURE_KEY_PATH;
export const SECURE_CERTIFICATE_PATH = config.SECURE_CERTIFICATE_PATH;
