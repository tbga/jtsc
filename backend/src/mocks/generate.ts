import { dbQuery } from "#database";
import { loggedFunction } from "#lib/logs";
import { generatePrimaryData } from "./primary.js";
import { generateSecondaryData } from "./secondary.js";
import { generateUpdates } from "./updates.js";
// import { generateOperations } from "./operations.js";
import { generateDeletions } from "./deletions.js";

export const generateData = loggedFunction(
  { onStart: "Generating data...", onFinish: "Generated data." },
  dbQuery<undefined, void>(async (_, ctx) => {
    await generatePrimaryData(undefined, ctx);
    await generateSecondaryData(undefined, ctx);
    await generateUpdates(undefined, ctx);
    // await generateOperations(undefined, ctx);
    await generateDeletions(undefined, ctx);
  }),
);
