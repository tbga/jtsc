import { loggedFunction } from "#lib/logs";
import { dbQuery } from "#database";
import { selectRandomIDs } from "#database/queries/random";
import { deleteFiles, filesTable } from "#database/queries/files";

export const generateDeletions = loggedFunction(
  {
    onStart: "Generating deletions...",
    onFinish: "Generated deletions.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {}),
);

export const generateFiles = loggedFunction(
  {
    onStart: "Deleting files...",
    onFinish: "Deleted files.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    const file_ids = await selectRandomIDs({ table: filesTable }, ctx);

    await deleteFiles(file_ids);
  }),
);
