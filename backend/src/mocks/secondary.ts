import { loggedFunction } from "#lib/logs";
import {
  randomPostInits,
  randomProfileInits,
  randomReleaseInits,
} from "#lib/random";
import { dbQuery } from "#database";
import { selectRandomIDs } from "#database/queries/random";
import { insertProfiles, profilesTable } from "#database/queries/profiles";
import { artistsTable } from "#database/queries/artists";
import { sitesTable } from "#database/queries/sites";
import { insertPosts } from "#database/queries/posts";
import { toProfileDBInits } from "#entities/profile";
import { createReleases } from "#entities/release";
import type { IArtist } from "#entities/artist";
import { toPostDBInits } from "#entities/post";
import { DEFAULT_AMOUNT } from "./types.js";

export const generateSecondaryData = loggedFunction(
  {
    onStart: "Generating secondary data...",
    onFinish: "Generated secondary data.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    const artist_ids = await selectRandomIDs(
      {
        table: artistsTable,
      },
      ctx,
    );

    await generateProfiles({ artist_ids }, ctx);
    await generateProfileReleases(undefined, ctx);
    await generatePosts({ artist_ids }, ctx);
  }),
);

interface IGenerateProfilesArgs {
  artist_ids: IArtist["id"][];
}

const generateProfiles = loggedFunction(
  { onStart: "Generating profiles...", onFinish: "Generated profiles." },
  dbQuery<IGenerateProfilesArgs, void>(async ({ artist_ids }, ctx) => {
    const site_ids = await selectRandomIDs(
      {
        table: sitesTable,
      },
      ctx,
    );
    const profileInits = randomProfileInits(DEFAULT_AMOUNT, {
      artist_ids,
      site_ids,
    });
    const profileDBInits = await toProfileDBInits(profileInits);

    await insertProfiles(profileDBInits, ctx);
  }),
);

const generateProfileReleases = loggedFunction(
  {
    onStart: "Generating profile releases...",
    onFinish: "Generated profile releases.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    const profile_ids: string[] = await selectRandomIDs(
      { table: profilesTable },
      ctx,
    );
    const releaseInits = randomReleaseInits(DEFAULT_AMOUNT, { profile_ids });
    await createReleases(releaseInits, ctx);
  }),
);

const generatePosts = loggedFunction(
  { onStart: "Generating posts...", onFinish: "Generated posts." },
  dbQuery<{ artist_ids: IArtist["id"][] }, void>(
    async ({ artist_ids }, ctx) => {
      const postInits = randomPostInits(DEFAULT_AMOUNT, { artist_ids });
      const dbInits = toPostDBInits(postInits);
      await insertPosts(dbInits, ctx);
    },
  ),
);
