import { loggedFunction } from "#lib/logs";
import { randomOperationInit } from "#lib/random";
import type { IOperationTypeOptions } from "#lib/random";
import { dbQuery } from "#database";
import { selectRandomIDs } from "#database/queries/random";
import { profilesTable } from "#database/queries/profiles";
import {
  insertOperations,
  type IOperationDBInit,
} from "#database/queries/operations";
import { filesTable } from "#database/queries/files";
import { releasesTable } from "#database/queries/releases";
import type { IProfile } from "#entities/profile";
import { type IOperationType, OPERATION_TYPES } from "#entities/operation";
import type { IFile } from "#entities/file";
import type { IRelease } from "#entities/release";
import type { IAccount } from "#entities/account";

// export interface IGenerateOperationsArgs {
//   amount: number;
//   allowedTypes?: Readonly<IOperationType[]>;
//   profile_ids: IProfile["id"][];
//   account_ids: IAccount["id"][];
// }

// export class OperationTypeOptions implements IOperationTypeOptions {
//   profile_ids;
//   portrait_ids;
//   release_ids;

//   constructor(
//     profile_ids: IProfile["id"][],
//     portrait_ids: IFile["id"][],
//     release_ids: IRelease["id"][],
//   ) {
//     this.profile_ids = profile_ids;
//     this.portrait_ids = portrait_ids;
//     this.release_ids = release_ids;
//   }

//   local_upload() {
//     return {
//       type: "local_upload" as "local_upload",
//       minAmount: 1,
//       maxAmount: 150,
//     };
//   }

//   releases_create() {
//     return {
//       type: "releases_create" as "releases_create",
//       profile_ids: this.profile_ids,
//       minAmount: 1,
//       maxAmount: 150,
//     };
//   }

//   profiles_create() {
//     return {
//       type: "profiles_create" as "profiles_create",
//       portrait_ids: this.portrait_ids,
//       minAmount: 1,
//       maxAmount: 150,
//     };
//   }

//   releases_previews_create() {
//     return {
//       type: "releases_previews_create" as "releases_previews_create",
//       release_ids: this.release_ids,
//       minAmount: 1,
//       maxAmount: 150,
//     };
//   }
// }

// /**
//  * @TODO account selector
//  */
// export const generateOperations = loggedFunction(
//   {
//     onStart: "Generating operations...",
//     onFinish: "Generated operations.",
//   },
//   dbQuery<undefined, void>(async (_, ctx) => {
//     const profile_ids = await getRandomIDs({ table: profilesTable }, ctx);
//     const file_ids = await getRandomIDs({ table: filesTable }, ctx);
//     const release_ids = await getRandomIDs({ table: releasesTable }, ctx);

//     const operationTypeOptions = new OperationTypeOptions(
//       profile_ids,
//       file_ids,
//       release_ids,
//     );
//     const randomInits = OPERATION_TYPES.reduce<
//       ReturnType<typeof randomOperationInit>[]
//     >((inits, type) => {
//       switch (type) {
//         // skipping public export operations for now
//         case "public_exports_finish": {
//           break;
//         }

//         default: {
//           const options = operationTypeOptions[type]();
//           const init = randomOperationInit(options);

//           inits.push(init);
//         }
//       }

//       return inits;
//     }, []);

//     // const dbInits = randomInits.map<IOperationDBInit>(
//     //   ({ input_data, type }) => {
//     //     return {
//     //       status: "pending",
//     //       type,
//     //       input_data,
//     //     };
//     //   },
//     // );
//     // await insertOperations(dbInits, ctx);
//   }),
// );
