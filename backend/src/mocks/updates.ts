import { loggedFunction } from "#lib/logs";
import { randomArtistsUpdates, randomSiteUpdates } from "#lib/random";
import { dbQuery } from "#database";
import {
  artistsTable,
  updateArtists as updateArtistsQuery,
} from "#database/queries/artists";
import { selectRandomIDs } from "#database/queries/random";
import {
  sitesTable,
  updateSites as queryUpdateSites,
} from "#database/queries/sites";

export const generateUpdates = loggedFunction(
  {
    onStart: "Generating updates...",
    onFinish: "Generated updates.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    await updateArtists(undefined, ctx);
    await updateSites(undefined, ctx);
  }),
);

/**
 * @TODO Rework artist update query.
 */
const updateArtists = loggedFunction(
  {
    onStart: "Updating artists...",
    onFinish: "Updated artists.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    const artist_ids = await selectRandomIDs({ table: artistsTable }, ctx);
    const artistsUpdates = randomArtistsUpdates(artist_ids);
    await updateArtistsQuery(artistsUpdates, ctx);
  }),
);

const updateSites = loggedFunction(
  {
    onStart: "Updating sites...",
    onFinish: "Updated sites.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    const site_ids = await selectRandomIDs({ table: sitesTable }, ctx);
    const siteDBUpdates = randomSiteUpdates(site_ids);
    await queryUpdateSites(siteDBUpdates, ctx);
  }),
);
