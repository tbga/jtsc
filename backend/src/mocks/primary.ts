import bcrypt from "bcrypt";
import { loggedFunction } from "#lib/logs";
import {
  randomPostInits,
  randomProfileInits,
  randomSiteInits,
  randomAccountInits,
  randomReleaseInits,
} from "#lib/random";
import { dbQuery } from "#database";
import { insertPosts } from "#database/queries/posts";
import { insertProfiles } from "#database/queries/profiles";
import { insertSites } from "#database/queries/sites";
import {
  insertAccounts,
  type IAccountDBInit,
} from "#database/queries/accounts";
import { insertReleases } from "#database/queries/releases";
import { DEFAULT_AMOUNT } from "#mocks";
import { toProfileDBInits } from "#entities/profile";
import { toReleaseDBInits } from "#entities/release";
import { toPostDBInits } from "#entities/post";
import { EMPTY_OBJECT } from "#types";

export const generatePrimaryData = loggedFunction(
  {
    onStart: "Generating primary data...",
    onFinish: "Generated primary data.",
  },
  dbQuery<undefined, void>(async (_, ctx) => {
    await generateAccounts(undefined, ctx);
    await generateProfiles(undefined, ctx);
    await generateSites(undefined, ctx);
    await generateReleases(undefined, ctx);
    await generatePosts(undefined, ctx);
  }),
);

const saltRounds = 10;

const generateAccounts = loggedFunction(
  { onStart: "Generating accounts...", onFinish: "Generated accounts." },
  dbQuery<undefined, void>(async (_, ctx) => {
    const accountInits = randomAccountInits(17, EMPTY_OBJECT);
    const dbInits: Parameters<typeof insertAccounts>["0"] = [];

    for await (const { login, password } of accountInits) {
      const hashedPassword = await bcrypt.hash(password, saltRounds);
      const dbInit: IAccountDBInit = {
        login,
        password: hashedPassword,
        role: "user",
      };

      dbInits.push(dbInit);
    }

    await insertAccounts(dbInits, ctx);
  }),
);

const generateProfiles = loggedFunction(
  { onStart: "Generating profiles...", onFinish: "Generated profiles." },
  dbQuery<undefined, void>(async (_, ctx) => {
    const profileInits = randomProfileInits(DEFAULT_AMOUNT, EMPTY_OBJECT);
    const profileDBInits = await toProfileDBInits(profileInits);

    await insertProfiles(profileDBInits, ctx);
  }),
);

const generateSites = loggedFunction(
  { onStart: "Generating sites...", onFinish: "Generated sites." },
  dbQuery<undefined, void>(async (_, ctx) => {
    const siteInits = randomSiteInits(DEFAULT_AMOUNT, undefined);

    await insertSites(siteInits, ctx);
  }),
);

const generateReleases = loggedFunction(
  { onStart: "Generating releases...", onFinish: "Generated releases." },
  dbQuery<undefined, void>(async (_, ctx) => {
    const releaseInits = randomReleaseInits(DEFAULT_AMOUNT, EMPTY_OBJECT);
    const dbInits = await toReleaseDBInits(releaseInits);
    await insertReleases(dbInits, ctx);
  }),
);

const generatePosts = loggedFunction(
  { onStart: "Generating posts...", onFinish: "Generated posts." },
  dbQuery<undefined, void>(async (_, ctx) => {
    const postInits = randomPostInits(DEFAULT_AMOUNT, EMPTY_OBJECT);
    const dbInits = toPostDBInits(postInits);
    await insertPosts(dbInits, ctx);
  }),
);
