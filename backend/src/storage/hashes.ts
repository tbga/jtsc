import path from "node:path";
import { createReadStream } from "node:fs";
import fs from "node:fs/promises";
import { createHash } from "node:crypto";
import { reduceFolder } from "#lib/fs";
import { log } from "#lib/logs";
import { splitString } from "#lib/strings";
import { HASHING_ALGORITHM } from "./types.js";

const defaultAlgorithms = [
  HASHING_ALGORITHM.SHA256,
  HASHING_ALGORITHM.SHA1,
  HASHING_ALGORITHM.MD5,
] as const;

type IAlgorithm = (typeof HASHING_ALGORITHM)[keyof typeof HASHING_ALGORITHM];

interface IFileInfo {
  path: string;
  resolvedPaths?: (string | null)[];

  hashes?: Record<IAlgorithm, string>;
}

export async function collectFolderHashes(
  folderPath: string,
  algorithms: readonly IAlgorithm[] = defaultAlgorithms,
): Promise<IFileInfo[]> {
  const infos = await reduceFolder<IFileInfo[]>(
    folderPath,
    [],
    async (infos, parsedpath, entry) => {
      if (entry.isSymbolicLink()) {
        const linkPath = path.format(parsedpath);

        try {
          const filePath = await fs.realpath(linkPath);
          const fileHashes = await collectFileHashes(filePath, algorithms);

          const fileInfo: IFileInfo = {
            path: linkPath,
            resolvedPaths: [filePath],
            hashes: fileHashes,
          };

          infos.push(fileInfo);
        } catch (error) {
          log(String(error));

          const fileInfo: IFileInfo = {
            path: linkPath,
            resolvedPaths: [null],
          };

          infos.push(fileInfo);
        }
      }

      if (entry.isFile()) {
        const filePath = path.format(parsedpath);
        const fileHashes = await collectFileHashes(filePath, algorithms);

        const fileInfo: IFileInfo = {
          path: filePath,
          hashes: fileHashes,
        };

        infos.push(fileInfo);
      }

      return infos;
    },
  );

  return infos;
}

/**
 * @TODO stream argument
 */
export function collectFileHashes(
  filePath: string,
  algorithms: readonly IAlgorithm[] = defaultAlgorithms,
): Promise<Record<(typeof algorithms)[number], string>> {
  const hashCalcs = algorithms.map((name) => createHash(name));

  const result = new Promise<Record<(typeof algorithms)[number], string>>(
    (resolve, reject) => {
      const fileStream = createReadStream(filePath);

      fileStream.on("data", (chunk) => {
        for (const hash of hashCalcs) {
          hash.update(chunk);
        }
      });

      fileStream.on("error", (error) => {
        reject(error);
      });

      fileStream.on("end", () => {
        const result = algorithms.reduce<
          Record<(typeof algorithms)[number], string>
        >(
          (hashes, name, index) => {
            const hashCalc = hashCalcs[index];
            const hashValue = hashCalc.digest("hex");

            hashes[name] = hashValue;

            return hashes;
          },
          {} as Record<(typeof algorithms)[number], string>,
        );

        resolve(result);
      });
    },
  );

  return result;
}

/**
 * Splits the hash into equal sized strings.
 */
export function splitHash(value: string, size = 2): string[] {
  return splitString(value, size);
}
