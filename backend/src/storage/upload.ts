import { fileURLToPath } from "node:url";
import type { IFileDBInit } from "#database/queries/files";
import type { IFileInit } from "#entities/file";
import { DownloadError } from "./errors.js";
import { uploadLocalFile } from "./upload/local.js";

/**
 * Flat only. No collection.
 */
export async function uploadFiles(
  fileInits: IFileInit[],
): Promise<IFileDBInit[]> {
  const fileDBInits: IFileDBInit[] = [];

  for await (const fileInit of fileInits) {
    const dbInits = await uploadFile(fileInit);

    fileDBInits.push(...dbInits);
  }

  return fileDBInits;
}

async function uploadFile(fileInit: IFileInit): Promise<IFileDBInit[]> {
  const isExternalFile = !fileInit.local_path.startsWith("file:");

  if (isExternalFile) {
    throw new DownloadError(
      `Path "${fileInit.local_path}" is not a file system path.`,
    );
  }
  const parsedPath = fileURLToPath(fileInit.local_path);
  const localInit: IFileInit = { ...fileInit, local_path: parsedPath };

  const dbInit = await uploadLocalFile(localInit);

  return Array.isArray(dbInit) ? dbInit : [dbInit];
}
