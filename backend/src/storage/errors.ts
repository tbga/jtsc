import { ProjectError } from "#lib/errors";

export class StorageError extends ProjectError {}

export class DownloadError extends StorageError {}
