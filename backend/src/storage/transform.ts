import path from "node:path";
import fs from "node:fs/promises";
import { nanoid } from "nanoid";
import { foldersLastThenNames, readFolder } from "#lib/fs";
import type { IFileInit } from "#entities/file";
import { StorageError } from "./errors.js";
import type { ILocalFolder } from "./types.js";

/**
 * Transform a list of file initializers
 * into a list of collection initializers suitable for database.
 * Creates a collection initializer for each folder path.
 * Invalid paths in the top level list
 * abort the entire process.
 * But due to unpredictable nature of traversing folders
 * nested errorneus paths are skipped.
 */
export async function transformInits(
  fileInits: IFileInit[],
  collectionName?: string,
): Promise<ILocalFolder[]> {
  const isSingleInit = fileInits.length === 1;
  const getRef = createRefMap();
  const localFolders: ILocalFolder[] = [];
  const erroredInits = new Map<IFileInit, StorageError>();
  const topLevelFolder: ILocalFolder = {
    name: collectionName ?? nanoid(),
    reference_id: 1,
    files: [],
  };

  // do not add the top level folder
  // with a single folder init
  if (!isSingleInit) {
    localFolders.push(topLevelFolder);
  }

  for await (const init of fileInits) {
    const stats = await fs.lstat(init.local_path);

    if (stats.isFile()) {
      // push root folder to the top
      // in case single init is a file
      if (isSingleInit) {
        localFolders.push(topLevelFolder);
      }

      topLevelFolder.files.push(init);

      continue;
    }

    if (stats.isDirectory()) {
      const localFolder: ILocalFolder = isSingleInit
        ? {
            name: collectionName ?? path.basename(init.local_path),
            reference_id: 1,
            files: [],
          }
        : {
            name: path.basename(init.local_path),
            reference_id: getRef(init.local_path),
            parent_reference: 1,
            files: [],
          };

      localFolders.push(localFolder);

      await transformFolderInit({
        parentFolder: localFolder,
        folderInit: init,
        erroredInits: erroredInits,
        transformedInits: localFolders,
        getRef,
      });

      continue;
    }

    throw new StorageError(
      `The path "${init.local_path}" is neither a file or a folder.`,
    );
  }

  return localFolders;
}

interface IArgs {
  parentFolder: ILocalFolder;
  folderInit: IFileInit;
  transformedInits: ILocalFolder[];
  erroredInits: Map<IFileInit, StorageError>;
  getRef: (pathName: string) => number;
}

async function transformFolderInit({
  parentFolder,
  folderInit,
  transformedInits,
  erroredInits,
  getRef,
}: IArgs) {
  const { local_path } = folderInit;

  const entries = await readFolder(local_path, foldersLastThenNames);

  for await (const entry of entries) {
    const localInit: IFileInit = {
      local_path: path.join(local_path, entry.name),
    };

    if (entry.isFile()) {
      parentFolder.files.push(localInit);

      continue;
    }

    if (entry.isDirectory()) {
      const localFolder: ILocalFolder = {
        name: entry.name,
        reference_id: getRef(localInit.local_path),
        parent_reference: parentFolder.reference_id,
        files: [],
      };

      transformedInits.push(localFolder);

      await transformFolderInit({
        getRef,
        parentFolder: localFolder,
        erroredInits,
        transformedInits,
        folderInit: localInit,
      });

      continue;
    }

    erroredInits.set(
      localInit,
      new StorageError(
        `The path "${localInit.local_path}" is neither a file or a folder.`,
      ),
    );
  }
}

function createRefMap() {
  const refMap = new Map<string, number>();
  let currentIndex = 1;

  function getRefIndex(pathName: string) {
    if (!refMap.has(pathName)) {
      currentIndex++;

      refMap.set(pathName, currentIndex);

      return currentIndex;
    }

    // biome-ignore lint/style/noNonNullAssertion: typescript and `Map` methods types
    return refMap.get(pathName)!;
  }

  return getRefIndex;
}
