import { LOCAL_STORAGE_FOLDER } from "#environment/variables.js";
import { folderStat } from "#lib/fs";
import { log, loggedFunction } from "#lib/logs";
import { collectFolderHashes } from "#storage";

export const collectStorageHashes = loggedFunction(
  {
    onStart: () => "Collecting storage hashes...",
    onFinish: () => "Collected storage hashes.",
  },
  async () => {
    log(`Analyzing folder "${LOCAL_STORAGE_FOLDER}" ...`);
    const { files, symlinks } = await folderStat(LOCAL_STORAGE_FOLDER);
    log(`Collecting hashes for ${files + symlinks} files...`);

    const infos = await collectFolderHashes(LOCAL_STORAGE_FOLDER);
    log(`Collected hashes for ${infos.length} files.`);

    return infos;
  },
);
