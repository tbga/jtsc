import type { IFileInit } from "#entities/file";

export interface ILocalFolder {
  name: string;
  reference_id: number;
  parent_reference?: number;
  files: IFileInit[];
}

export interface IStorageStats {
  items: number;
  folders: number;
  files: number;
  symlinks: number;
  broken_symlinks: number;
  total_size: bigint;
  total_virtual_size: bigint;
}

export const BASE_FILE_TYPE = {
  APPLICATION: "application",
  AUDIO: "audio",
  FONT: "font",
  EXAMPLE: "example",
  IMAGE: "image",
  MESSAGE: "message",
  MODEL: "model",
  MULTIPART: "multipart",
  TEXT: "text",
  VIDEO: "video",
} as const;

export const BASE_FILE_TYPES = Object.values(BASE_FILE_TYPE);

export type IBaseFileType = (typeof BASE_FILE_TYPES)[number];

export const DEFAULT_STORAGE_FOLDER = "/generic";

export const HASHING_ALGORITHM = {
  SHA1: "sha1",
  SHA256: "sha256",
  MD5: "md5",
} as const;

export const DEFAULT_FILENAME = "unnamed";
