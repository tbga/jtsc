import path from "node:path";
import fs from "node:fs/promises";
import { nanoid } from "nanoid";
import { IS_WINDOWS } from "#environment/constants.js";
import { ensurePath } from "#lib/fs";
import { convertWindowsPathToLinux } from "#lib/path";
import { splitHash } from "#storage";
import { PUBLIC_STORAGE_FOLDER } from "#environment/variables.js";

export async function saveToPublicStorage(filePath: string): Promise<string> {
  // generate random base name
  const baseName = nanoid();
  const extension = path.extname(filePath);
  // create the filename with the same extension as local file
  const linkName = `${baseName}${extension}`;
  // create sub-directories
  const folders = splitHash(baseName.toLowerCase());
  // create public URL
  const publicURL = path.join("/", ...folders, linkName);
  // create the public storage path
  const linkPath = path.join(PUBLIC_STORAGE_FOLDER, publicURL);

  // create the symlinks
  await ensurePath(linkPath);
  await fs.symlink(filePath, linkPath, "file");

  return IS_WINDOWS ? convertWindowsPathToLinux(publicURL) : publicURL;
}
