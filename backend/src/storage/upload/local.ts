import path from "node:path";
import fs from "node:fs/promises";
import { LOCAL_STORAGE_FOLDER } from "#environment/variables.js";
import { type getMediaType, getPathMediaType } from "#lib/media";
import { ensurePath } from "#lib/fs";
import { log } from "#lib/logs";
import type { IFileDBInit } from "#database/queries/files";
import { collectFileHashes, splitHash } from "#storage";
import type { IFileHashes, IFileInit } from "#entities/file";
import { saveToPublicStorage } from "./to-public-storage.js";

/**
 * Soft upload only. Ignores symlinks.
 */
export async function uploadLocalFile({
  local_path,
  name,
}: IFileInit): Promise<IFileDBInit[]> {
  const dbInits: IFileDBInit[] = [];
  const stats = await fs.lstat(local_path);

  if (stats.isDirectory()) {
    const folderDBInis = await uploadFolder(local_path, name);
    dbInits.push(...folderDBInis);
  }

  if (stats.isFile()) {
    const dbInit = await uploadFile(local_path, name);
    dbInits.push(dbInit);
  }

  return dbInits;
}

/**
 * @param folderPath
 * @param name Prepended to the filenames of the folder.
 */
export async function uploadFolder(
  folderPath: string,
  name?: string,
): Promise<IFileDBInit[]> {
  const dbInits: IFileDBInit[] = [];
  const entries = await fs.readdir(folderPath, {
    withFileTypes: true,
    encoding: "utf-8",
  });

  for await (const entry of entries) {
    if (entry.isDirectory()) {
      const nestedPath = path.join(folderPath, entry.name);
      const folderDBInits = await uploadFolder(nestedPath);
      dbInits.push(...folderDBInits);
    }

    if (entry.isFile()) {
      const folderName = path.basename(folderPath);
      const filePath = path.join(folderPath, entry.name);
      const fileName = `[${folderName}]-${entry.name}`;
      const fileDBInit = await uploadFile(filePath, fileName);

      dbInits.push(fileDBInit);
    }
  }

  return dbInits;
}

export async function uploadFile(
  filePath: string,
  name?: string,
): Promise<IFileDBInit> {
  const stats = await fs.stat(filePath);
  const fileName =
    name ??
    `${path.basename(path.dirname(filePath))}-${path.basename(filePath)}`;

  const mediaType = getPathMediaType(filePath);
  const { localPath, sha256, md5, sha1 } = await saveToLocalStorage(
    filePath,
    mediaType,
  );
  const publicURL = await saveToPublicStorage(localPath);
  const dbInit: IFileDBInit = {
    url: publicURL,
    name: fileName,
    size: String(stats.size),
    type: mediaType.type,
    subtype: mediaType.subtype,
    sha_256: sha256,
    sha_1: sha1,
    md5: md5,
  };

  return dbInit;
}

interface ILocalFile extends IFileHashes {
  localPath: string;
}

export async function saveToLocalStorage(
  filePath: string,
  mediaType: ReturnType<typeof getMediaType>,
): Promise<ILocalFile> {
  // constructing the local file path
  const hashes = await collectFileHashes(filePath);
  const baseName = hashes.sha256;
  const linkName = `${baseName}.${mediaType.extension}`;
  const folders = splitHash(baseName.toLowerCase());
  const linkPath = path.join(LOCAL_STORAGE_FOLDER, ...folders, linkName);

  try {
    // skip if there is already an entry in the path
    // @TODO check if the entry in the path
    // is a working symbolic link
    await fs.lstat(linkPath);
    log(
      `File "${linkPath}" already exists, the file "${filePath}" will not be linked.`,
    );
  } catch (error) {
    log(String(error));
    await ensurePath(linkPath);
    await fs.symlink(filePath, linkPath, "file");
  }

  const localFile: ILocalFile = {
    localPath: linkPath,
    ...hashes,
  };

  return localFile;
}
