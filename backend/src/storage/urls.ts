import { STORAGE_URL } from "#environment/derived-variables.js";

export class StorageURL extends URL {
  constructor(pathname: string) {
    super(`${STORAGE_URL.pathname}${pathname}`, STORAGE_URL.origin);
    this.host = STORAGE_URL.host;
  }
}
