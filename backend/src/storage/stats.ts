import path from "node:path";
import fs from "node:fs/promises";
import { reduceFolder } from "#lib/fs";
import { BIGINT_ZERO } from "#lib/numbers/bigint";
import type { IStorageStats } from "./types.js";

export async function collectStorageStats(folderPath: string) {
  const initStats: IStorageStats = {
    items: 0,
    folders: 0,
    files: 0,
    symlinks: 0,
    broken_symlinks: 0,
    total_size: BIGINT_ZERO,
    total_virtual_size: BIGINT_ZERO,
  };

  const storageStats = await reduceFolder(
    folderPath,
    initStats,
    async (stats, parsedPath, entry) => {
      stats.items++;

      if (entry.isDirectory()) {
        stats.folders++;
      }

      if (entry.isFile()) {
        stats.files++;

        const fileStats = await fs.stat(path.format(parsedPath), {
          bigint: true,
        });
        stats.total_size += fileStats.size;
        stats.total_virtual_size += fileStats.size;
      }

      if (entry.isSymbolicLink()) {
        stats.symlinks++;

        const symStats = await fs.lstat(path.format(parsedPath), {
          bigint: true,
        });

        stats.total_size += symStats.size;

        try {
          const filePath = await fs.realpath(path.format(parsedPath), {
            encoding: "utf-8",
          });
          const fileStats = await fs.stat(filePath, {
            bigint: true,
          });

          stats.total_virtual_size += fileStats.size;
        } catch (error) {
          stats.broken_symlinks++;
        }
      }

      return stats;
    },
  );

  return storageStats;
}
