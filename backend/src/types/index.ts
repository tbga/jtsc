export type { IBigInteger } from "#codegen/schema/lib/types/numbers/big-integer";
export type { IBigIntegerPositive } from "#codegen/schema/lib/types/numbers/big-integer-positive";
export type { IBigSerialInteger } from "#codegen/schema/lib/types/numbers/big-serial";
export type { ISerialInteger } from "#codegen/schema/lib/types/numbers/serial";

export type IActionResult = IActionSuccess | IActionFailure;

export interface IActionSuccess {
  is_successful: true;
}

export interface IActionFailure {
  is_successful: false;
}

/**
 * A type for mutable and readonly arrays.
 */
export type AnyArray<ItemType = unknown> = ItemType[] | readonly ItemType[];

export type IMessage = string | AnyArray<string>;

type Enumerate<
  N extends number,
  Acc extends number[] = [],
> = Acc["length"] extends N
  ? Acc[number]
  : Enumerate<N, [...Acc, Acc["length"]]>;

export type IntRange<F extends number, T extends number> = Exclude<
  Enumerate<T>,
  Enumerate<F>
>;

export type INumeric = number | bigint;

export type IArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

/**
 * Make selected keys optional.
 */
export type IPartialSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Partial<Type>, Key>;

/**
 * Make selected key required.
 */
export type IRequiredSome<Type, Key extends keyof Type> = Omit<Type, Key> &
  Pick<Required<Type>, Key>;

/**
 * An object ref to use in place of empty objects.
 */
export const EMPTY_OBJECT = {} as const;

Object.freeze(EMPTY_OBJECT);

export const EMPTY_ARRAY = [] as const;

Object.freeze(EMPTY_ARRAY);
