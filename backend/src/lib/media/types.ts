export type { IMediaType } from "#codegen/schema/lib/media/media-type";
export type { IMediaTypeApplication } from "#codegen/schema/lib/media/application";
export type { IMediaTypeAudio } from "#codegen/schema/lib/media/audio";
export type { IMediaTypeFont } from "#codegen/schema/lib/media/font";
export type { IMediaTypeImage } from "#codegen/schema/lib/media/image";
export type { IMediaTypeMessage } from "#codegen/schema/lib/media/message";
export type { IMediaTypeModel } from "#codegen/schema/lib/media/model";
export type { IMediaTypeMultipart } from "#codegen/schema/lib/media/multipart";
export type { IMediaTypeText } from "#codegen/schema/lib/media/text";
export type { IMediaTypeVideo } from "#codegen/schema/lib/media/video";

export const DEFAULT_MIME_TYPE = "application/octet-stream";
