import mime from "mime";
import { DEFAULT_MIME_TYPE, type IMediaType } from "./types.js";

export function getPathMediaType(filePath: string) {
  const mimetype = mime.getType(filePath) ?? DEFAULT_MIME_TYPE;

  return getMediaType(mimetype);
}

export function getMediaType(mimetype: string) {
  const [type, subtype] = mimetype.split("/") as [
    IMediaType["type"],
    IMediaType["subtype"],
  ];
  const extension = mime.getExtension(mimetype) ?? "bin";
  // @ts-expect-error @TODO discriminated union somehow
  const mediaType: IMediaType & { extension: string } = {
    type,
    subtype,
    extension,
  };

  return mediaType;
}
