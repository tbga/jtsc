export { getMediaType, getPathMediaType } from "./lib.js";
export type {
  IMediaType,
  IMediaTypeApplication,
  IMediaTypeAudio,
  IMediaTypeFont,
  IMediaTypeImage,
  IMediaTypeMessage,
  IMediaTypeModel,
  IMediaTypeMultipart,
  IMediaTypeText,
  IMediaTypeVideo,
} from "./types.js";
