import type { Request, Response } from "express";
import { type SessionOptions, getIronSession } from "iron-session";
import { SECRET_KEY } from "#environment/variables.js";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";

interface ISessionObject {
  account_id?: string;
}

export const SESSION_COOKIE_NAME = "cookie-clicker";

const cookieOptions: SessionOptions["cookieOptions"] = {
  secure: !IS_DEVELOPMENT,
};

const options: SessionOptions = {
  cookieName: SESSION_COOKIE_NAME,
  password: SECRET_KEY,
  cookieOptions,
};

export async function getSession<
  Params,
  ResBody,
  ReqBody,
  Query = URLSearchParams,
  Locals extends Record<string, unknown> = Record<string, unknown>,
>(
  req: Request<Params, ResBody, ReqBody, Query, Locals>,
  res: Response<ResBody, Locals>,
) {
  const session = await getIronSession<ISessionObject>(req, res, options);

  return session;
}
