export interface IPropertyDescriptor<ObjectType = unknown>
  extends PropertyDescriptor {
  value?: ObjectType[keyof ObjectType];
}

/**
 * Typed version of `Object.defineProperty()`.
 */
export function defineProperty<ObjectType = unknown>(
  obj: ObjectType,
  key: keyof ObjectType,
  attributes: IPropertyDescriptor<ObjectType>,
) {
  Object.defineProperty(obj, key, attributes);
}
