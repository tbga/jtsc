import { ProjectError } from "#lib/errors";

export class EntityError extends ProjectError {}
