import type { IBigSerialInteger } from "#types";
import type { IEntityItem } from "./types.js";
import { EntityError } from "./errors.js";

export function validateEntityID(entityID: IBigSerialInteger) {
  const parsedID = BigInt(entityID);

  if (parsedID < 1) {
    throw new EntityError(
      `Entity identifier "${parsedID}" must be a positive integer.`,
    );
  }
}

export function toEntityIDs<EntityType extends { id: IBigSerialInteger }>(
  entries: { id: EntityType["id"] }[],
): EntityType["id"][] {
  return entries.map(({ id }) => id);
}

export function entityTitle(id: IBigSerialInteger, name: IEntityItem["name"]) {
  const finalName = name ? `"${name}"` : "Unknown";
  const title = `${finalName} (${id})`;

  return title;
}
