import type { IPaginatedQueryArgs } from "#lib/pagination";
import type { IBigSerialInteger } from "#types";

export type { IEntity } from "#codegen/schema/lib/entities/entity";
export type { IEntityItem } from "#codegen/schema/lib/entities/item";

export interface ISearchQueryArgs {
  search_query?: string;
}

export interface IPaginatedSearchQueryArgs
  extends ISearchQueryArgs,
    IPaginatedQueryArgs {}

export interface IEntityResult {
  id: IBigSerialInteger;
}

export interface IEntityIDs<EntityType extends IEntityResult>
  extends Array<EntityType["id"]> {}
