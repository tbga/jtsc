import {
  PROJECT_ENV,
  BACKEND_PORT,
  IS_HTTPS_ENABLED,
} from "#environment/variables.js";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";
import { log } from "#lib/logs";
import { startRunner } from "#entities/operation";
import { createApplication } from "./application.js";
import { createHTTPServer } from "./http.js";
import { createHTTPSServer } from "./https.js";

export async function createServer() {
  const application = await createApplication();

  if (IS_HTTPS_ENABLED) {
    await createHTTPSServer(application);
  } else {
    await createHTTPServer(application);
  }

  startRunner();

  if (!IS_DEVELOPMENT) {
    log(
      `Running server in "${PROJECT_ENV}" mode at port "${BACKEND_PORT}" in ${
        IS_HTTPS_ENABLED ? "secure" : "insecure"
      } context.`,
    );
  }
}
