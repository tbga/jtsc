export interface IError extends Error {
  syscall: string;
  code: string;
}

/**
 * https://expressjs.com/en/4x/api.html#app.settings.table
 */
export const EXPRESSJS_OPTIONS = {
  /**
   * Enable case sensitivity. When enabled, "/Foo" and "/foo" are different routes.
   * When disabled, "/Foo" and "/foo" are treated the same.
   *
   * **NOTE**: Sub-apps will inherit the value of this setting.
   */
  CASE_SENSITIVE_ROUTING: "case sensitive routing",
  /**
   * Environment mode. Be sure to set to "production" in a production environment;
   * see Production best practices: performance and reliability.
   */
  ENV: "env",
  /**
   * Set the ETag response header.
   * For possible values, see the etag options table.
   * More about the HTTP ETag header.
   */
  ETAG: "etag",
  /**
   * Specifies the default JSONP callback name.
   */
  JSONP_CALLBACK_NAME: "jsonp callback name",
  /**
   * Enable escaping JSON responses from the res.json, res.jsonp, and res.send APIs.
   * This will escape the characters <, >, and & as Unicode escape sequences in JSON.
   * The purpose of this it to assist with mitigating certain types of persistent XSS attacks
   * when clients sniff responses for HTML.
   *
   * **NOTE**: Sub-apps will inherit the value of this setting.
   */
  JSON_ESCAPE: "json escape",
  /**
   * The 'replacer' argument used by `JSON.stringify`.
   * **NOTE**: Sub-apps will inherit the value of this setting.
   */
  JSON_REPLACER: "json replacer",
  /**
   * Varied 	The 'space' argument used by `JSON.stringify`.
   * This is typically set to the number of spaces to use to indent prettified JSON.
   * **NOTE**: Sub-apps will inherit the value of this setting.
   */
  JSON_SPACES: "json spaces",
  /**
   * Disable query parsing by setting the value to false,
   * or set the query parser to use either “simple” or “extended” or a custom query string parsing function.
   * The simple query parser is based on Node’s native query parser, querystring.
   * The extended query parser is based on qs.
   *
   * A custom query string parsing function will receive the complete query string, and must return an object of query keys and their values.
   */
  QUERY_PARSER: "query parser",
  /**
   * Enable strict routing. When enabled, the router treats "/foo" and "/foo/" as different. Otherwise, the router treats "/foo" and "/foo/" as the same.
   * NOTE: Sub-apps will inherit the value of this setting.
   */
  STRICT_ROUTING: "strict routing",
  /**
   * The number of dot-separated parts of the host to remove to access subdomain.
   */
  SUBDOMAIN_OFFSET: "subdomain offset",
  /**
   * Indicates the app is behind a front-facing proxy, and to use the X-Forwarded-* headers to determine the connection and the IP address of the client. NOTE: X-Forwarded-* headers are easily spoofed and the detected IP addresses are unreliable.
   * When enabled, Express attempts to determine the IP address of the client connected through the front-facing proxy, or series of proxies. The `req.ips` property, then contains an array of IP addresses the client is connected through. To enable it, use the values described in the trust proxy options table.
   * The `trust proxy` setting is implemented using the proxy-addr package. For more information, see its documentation.
   *
   * NOTE: Sub-apps will inherit the value of this setting, even though it has a default value.
   */
  TRUST_PROXY: "trust proxy",
  /**
   * A directory or an array of directories for the application's views. If an array, the views are looked up in the order they occur in the array.
   */
  VIEWS: "views",
  /**
   * Enables view template compilation caching.
   * NOTE: Sub-apps will not inherit the value of this setting in production (when `NODE_ENV` is "production").
   */
  VIEW_CACHE: "view cache",
  /**
   * The default engine extension to use when omitted.
   * NOTE: Sub-apps will inherit the value of this setting.
   */
  VIEW_ENGINE: "view engine",
  /**
   * Enables the "X-Powered-By: Express" HTTP header.
   */
  X_POWERED_BY: "x-powered-by",
} as const;
