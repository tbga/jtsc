import path from "node:path";
import { URLSearchParams } from "node:url";
import express from "express";
import cookieParser from "cookie-parser";
import {
  NODE_ENV,
  BACKEND_PORT,
  PUBLIC_STORAGE_FOLDER,
} from "#environment/variables.js";
import {
  IS_DEVELOPMENT,
  PUBLIC_FOLDER,
} from "#environment/derived-variables.js";
import { getHTTPLogger } from "#lib/logs";
import { corsHandler } from "#middleware";
import { routes } from "#routes";
import { normalizePort } from "./normalize-port.js";
import { EXPRESSJS_OPTIONS } from "./types.js";

export async function createApplication() {
  const app = express();
  const normalizedPort = normalizePort(BACKEND_PORT);

  app.set("port", normalizedPort);

  // App settings.
  app.set(EXPRESSJS_OPTIONS.CASE_SENSITIVE_ROUTING, false);
  app.set(EXPRESSJS_OPTIONS.ENV, NODE_ENV);
  app.set(EXPRESSJS_OPTIONS.JSON_ESCAPE, true);
  app.set(EXPRESSJS_OPTIONS.STRICT_ROUTING, false);
  app.set(EXPRESSJS_OPTIONS.X_POWERED_BY, false);
  app.set(
    EXPRESSJS_OPTIONS.QUERY_PARSER,
    (str: string) => new URLSearchParams(str),
  );

  app.use(getHTTPLogger(IS_DEVELOPMENT));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());

  if (!IS_DEVELOPMENT) {
    app.use(
      express.static(PUBLIC_FOLDER, {
        setHeaders(res, fsPath, stat) {
          const relPath = path.relative(PUBLIC_FOLDER, fsPath);
          const isStaticModule = relPath.startsWith("_next");

          if (isStaticModule) {
            res.setHeader("Cache-Control", ["immutable", "max-age=31536000"]);
          }
        },
      }),
    );
  }

  app.use("/storage", express.static(PUBLIC_STORAGE_FOLDER));

  app.use(corsHandler);

  app.use("/", routes);

  return app;
}
