import { ClientError, type IErrorMessage } from "#lib/errors";
import { HTTP_STATUS } from "#lib/http";

export class NotFoundError extends ClientError {
  constructor(message?: IErrorMessage, options?: ErrorOptions) {
    super(message ?? "Not Found", {
      status: HTTP_STATUS.NOT_FOUND,
      ...options,
    });
  }
}

export class UnauthorizedError extends ClientError {
  constructor(message?: IErrorMessage, options?: ErrorOptions) {
    super(message ?? "Unauthorized", {
      status: HTTP_STATUS.UNAUTHORIZED,
      ...options,
    });
  }
}
