/**
 * Normalize a port into a number, string, or false.
 */
export function normalizePort(portValue: string | number) {
  const port =
    typeof portValue === "string" ? Number.parseInt(portValue, 10) : portValue;

  if (Number.isNaN(port)) {
    // named pipe
    return portValue;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
