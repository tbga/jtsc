import http from "node:http";
import type { Express } from "express";
import debugLib from "debug";
import type { IError } from "./types.js";

/**
 * Create HTTP server.
 */
export async function createHTTPServer(application: Express) {
  const normalizedPort = application.get("port") as string | number;
  const debug = debugLib("backend:server");
  const httpServer = http.createServer(application);

  // Listen on provided port, on all network interfaces.
  httpServer.listen(normalizedPort);
  httpServer.on("error", onError);
  httpServer.on("listening", onListening);

  /**
   * Event listener for HTTP server "listening" event.
   */
  function onListening() {
    const addr = httpServer.address();

    if (addr === null) {
      throw new Error("No server address provided.");
    }

    const bind =
      typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
  }

  /**
   * Event listener for HTTP server "error" event.
   */
  function onError(error: IError) {
    if (error.syscall !== "listen") {
      throw error;
    }

    const bind =
      typeof normalizedPort === "string"
        ? `Pipe ${normalizedPort}`
        : `Port ${normalizedPort}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case "EACCES":
        console.error(`${bind} requires elevated privileges`);
        process.exit(1);
        break;
      case "EADDRINUSE":
        console.error(`${bind} is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
}
