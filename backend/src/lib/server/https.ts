import https from "node:https";
import { readFile } from "node:fs/promises";
import type { Express } from "express";
import debugLib from "debug";
import {
  SECURE_KEY_PATH,
  SECURE_CERTIFICATE_PATH,
} from "#environment/variables.js";
import type { IError } from "./types.js";

export async function createHTTPSServer(app: Express) {
  const normalizedPort = app.get("port") as string | number;
  const debug = debugLib("backend:server");
  const key = await readFile(SECURE_KEY_PATH);
  const cert = await readFile(SECURE_CERTIFICATE_PATH);
  const credentials = { key, cert };

  const httpsServer = https.createServer(credentials, app);

  // Listen on provided port, on all network interfaces.
  httpsServer.listen(normalizedPort);
  httpsServer.on("error", onError);
  httpsServer.on("listening", onListening);

  /**
   * Event listener for HTTP server "error" event.
   */
  function onError(error: IError) {
    if (error.syscall !== "listen") {
      throw error;
    }

    const bind =
      typeof normalizedPort === "string"
        ? `Pipe ${normalizedPort}`
        : `Port ${normalizedPort}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case "EACCES":
        console.error(`${bind} requires elevated privileges`);
        process.exit(1);
        break;
      case "EADDRINUSE":
        console.error(`${bind}  is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */
  function onListening() {
    const addr = httpsServer.address();

    if (addr === null) {
      throw new Error("No server address provided.");
    }

    const bind =
      typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
  }
}
