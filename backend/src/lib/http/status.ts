/**
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
 */
export const HTTP_STATUS = {
  OK: 200,
  CREATED: 201,
  RESET_CONTENT: 205,
  PARTIAL_CONTENT: 206,
  /**
   * The URL of the requested resource has been changed permanently.
   * The new URL is given in the response.
   */
  MOVED_PERMANENTLY: 301,
  /**
   * This response code means that the URI of requested resource has been changed _temporarily_.
   * Further changes in the URI might be made in the future.
   * Therefore, this same URI should be used by the client in future requests.
   */
  FOUND: 302,
  /**
   * The server sent this response to direct the client to get the requested resource at another URI with a GET request.
   */
  SEE_OTHER: 303,
  /**
   * This is used for caching purposes.
   * It tells the client that the response has not been modified,
   * so the client can continue to use the same cached version of the response.
   */
  NOT_MODIFIED: 304,
  /**
   * The server sends this response to direct the client
   * to get the requested resource at another URI with same method
   * that was used in the prior request.
   * This has the same semantics as the `302 Found` HTTP response code,
   * with the exception that the user agent must not change the HTTP method used:
   * if a `POST` was used in the first request,
   * a `POST` must be used in the second request.
   */
  TEMPORARY_REDIRECT: 307,
  /**
   * This means that the resource is now permanently located at another URI,
   * specified by the `Location:` HTTP Response header.
   * This has the same semantics as the `301 Moved Permanently` HTTP response code,
   * with the exception that the user agent must not change the HTTP method used:
   * if a `POST` was used in the first request, a `POST` must be used in the second request.
   */
  PERMANENT_REDIRECT: 308,
  /**
   * The server cannot or will not process the request
   * due to something that is perceived to be a client error
   * (e.g., malformed request syntax, invalid request message framing,
   * or deceptive request routing).
   */
  BAD_REQUEST: 400,
  /**
   * Although the HTTP standard specifies "unauthorized",
   * semantically this response means "unauthenticated".
   * That is, the client must authenticate itself to get the requested response.
   */
  UNAUTHORIZED: 401,
  /**
   * This response code is reserved for future use.
   * The initial aim for creating this code was using it for digital payment systems,
   * however this status code is used very rarely and no standard convention exists.
   */
  PAYMENT_REQUIRED: 402,
  /**
   * The client does not have access rights to the content;
   * that is, it is unauthorized, so the server is refusing to give the requested resource.
   * Unlike `401 Unauthorized`, the client's identity is known to the server.
   */
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  METHOD_NOT_ALLOWED: 405,
  /**
   * This response is sent when the web server, after performing server-driven content negotiation,
   * doesn't find any content that conforms to the criteria given by the user agent.
   */
  NOT_ACCEPTABLE: 406,
  PROXY_AUTHENTICATION_REQUIRED: 407,
  /**
   * This response is sent on an idle connection by some servers, even without any previous request by the client.
   * It means that the server would like to shut down this unused connection.
   * This response is used much more since some browsers, like Chrome, Firefox 27+, or IE9,
   * use HTTP pre-connection mechanisms to speed up surfing.
   * Also note that some servers merely shut down the connection without sending this message.
   */
  REQUEST_TIMEOUT: 408,
  /**
   * This response is sent when a request conflicts with the current state of the server.
   */
  CONFLICT: 409,
  GONE: 410,
  LENGTH_REQUIRED: 411,
  PAYLOAD_TOO_LARGE: 413,
  URI_TOO_LONG: 414,
  UNSUPPORTED_MEDIA_TYPE: 415,
  RANGE_NOT_SATISFIABLE: 416,
  /**
   * The request was well-formed but was unable to be followed due to semantic errors.
   */
  UNPROCESSABLE_ENTITY: 422,
  UPGRADE_REQUIRED: 426,
  TOO_MANY_REQUESTS: 429,
  REQUEST_HEADER_FIELDS_TOO_LARGE: 431,
  UNAVAILABLE_FOR_LEGAL_REASONS: 451,
  INTERNAL_SERVER_ERROR: 500,
  NOT_IMPLEMENTED: 501,
  BAD_GATEWAY: 502,
  SERVICE_UNAVAILABLE: 503,
  GATEWAY_TIMEOUT: 504,
  HTTP_VERSION_NOT_SUPPORTED: 505,
  INSUFFICIENT_STORAGE: 507,
  LOOP_DETECTED: 508,
  NOT_EXTENDED: 510,
  NETWORK_AUTHENTICATION_REQUIRED: 511,
} as const;
