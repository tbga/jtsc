import path from "node:path";
import fs from "node:fs/promises";
import { nanoid } from "nanoid";
import { TEMPORARY_FOLDER } from "#environment/variables.js";
import type { INanoID } from "#lib/strings";
import { ensurePath } from "#lib/fs";

export async function getTemporaryFolder(id?: INanoID) {
  const folderName = id ?? nanoid();
  const tempFolder = path.join(TEMPORARY_FOLDER, folderName);

  await ensurePath(tempFolder, false);

  return { tempFolder, id: folderName };
}

export async function removeTemporaryFolder(id: INanoID) {
  const tempPath = path.join(TEMPORARY_FOLDER, id);

  await fs.rm(tempPath, { recursive: true, force: true });
}
