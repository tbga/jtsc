import type { IBigSerialInteger } from "#types";
import type { IPagination } from "./types.js";

const PAGINATION_LIMIT = 25;

/**
 * @TODO TBIQ (The Big Int Question)
 */
export class Pagination implements IPagination {
  total_count: IBigSerialInteger;
  limit: number;
  current_page: IBigSerialInteger;
  total_pages: IBigSerialInteger;

  constructor(totalCount: IBigSerialInteger, currentPage?: IBigSerialInteger) {
    this.total_count = totalCount;
    this.limit = PAGINATION_LIMIT;
    this.total_pages = String(
      Math.ceil(Number.parseInt(totalCount) / this.limit),
    );
    this.current_page = currentPage ? currentPage : this.total_pages;
  }

  get offset() {
    return (Number.parseInt(this.current_page) - 1) * this.limit;
  }
}
