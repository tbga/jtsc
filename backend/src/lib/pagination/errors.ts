import { ProjectError } from "#lib/errors";

export class PaginationError extends ProjectError {}
