import type { IBigIntegerPositive } from "#types";
import { PaginationError } from "./errors.js";

/**
 * Validates the page number.
 * @param pageNumber
 * @param totalPages
 */
export function validatePageNumber(
  pageNumber: IBigIntegerPositive,
  totalPages?: IBigIntegerPositive,
) {
  let currentPage: bigint;

  try {
    currentPage = BigInt(pageNumber);
  } catch (error) {
    throw new PaginationError(`"${pageNumber}" is not a valid page number.`);
  }

  if (currentPage < 1) {
    throw new PaginationError(
      `Current page "${currentPage}" must be a positive integer.`,
    );
  }

  if (totalPages === undefined) {
    return;
  }

  // also validate its value against total
  const total = BigInt(totalPages);

  if (currentPage > total) {
    throw new PaginationError(
      `Current page "${currentPage}" is greater than total of "${totalPages}".`,
    );
  }
}
