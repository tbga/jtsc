export { Pagination } from "./lib.js";
export { PaginationError } from "./errors.js";
export { validatePageNumber } from "./validate.js";
export type {
  IPagination,
  ITableCount,
  IPaginationDB,
  IPaginatedQueryArgs,
} from "./types.js";
