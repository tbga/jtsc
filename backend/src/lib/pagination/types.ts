import type { IBigIntegerPositive } from "#types";

export type { IPagination } from "#codegen/schema/lib/types/pagination/pagination";

export interface IPaginationDB {
  limit?: number | IBigIntegerPositive;
  offset?: number | IBigIntegerPositive;
}

export interface IPaginatedQueryArgs {
  pagination: IPaginationDB;
}

export interface ITableCount {
  count: IBigIntegerPositive;
}
