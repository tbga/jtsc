import { readFile } from "node:fs/promises";
import path from "node:path";

import {
  compile as compileSchemaInterface,
  type Options as JS2TSOptions,
} from "json-schema-to-typescript";

import type { IJSONSchema } from "#lib/json-schema";
import { reduceFolder } from "#lib/fs";
import { fromJSON, toJSON } from "#lib/json";
import { ProjectError } from "#lib/errors";

export interface IJSONSchemaCollection
  extends Record<IJSONSchema["$id"], IJSONSchemaInfo> {}

export interface IJSONSchemaInfo {
  localPath: string;
  schema: IJSONSchema;
}

export const schemaFileEnd = ".schema.json";
const metaSchemaFolder = "meta";
let collection: IJSONSchemaCollection;

export async function collectJSONSchemas(schemaFolder: string) {
  if (collection) {
    return collection;
  }

  collection = await reduceFolder<IJSONSchemaCollection>(
    schemaFolder,
    {},
    async (schemaCollection, filePath, entry) => {
      // for now exclude meta schemas
      const isSchema =
        entry.isFile() &&
        filePath.dir !== path.join(schemaFolder, metaSchemaFolder) &&
        entry.name.endsWith(schemaFileEnd);

      if (!isSchema) {
        return schemaCollection;
      }

      const schemaPath = path.format(filePath);
      const schemaJSON = await readFile(schemaPath, { encoding: "utf-8" });
      const jsonSchema = fromJSON<IJSONSchema>(schemaJSON);

      if (!jsonSchema.$id || !jsonSchema.title) {
        throw new ProjectError(
          `JSON schema file should have "$id" and "title" properties at the top level and the schema at "${filePath}" misses either of them`,
        );
      }

      if (schemaCollection[jsonSchema.$id]) {
        throw new ProjectError(
          `JSON schema file with ID "${jsonSchema.$id}" already exists, the schema at "${schemaPath}" should have a different "$id" value.`,
        );
      }

      schemaCollection[jsonSchema.$id] = {
        localPath: path.relative(schemaFolder, schemaPath),
        schema: jsonSchema,
      };

      return schemaCollection;
    },
  );

  return collection;
}

interface ISchemaInterface {
  inputSchema: IJSONSchema;
  code: string;
}

const parserOptions: JS2TSOptions["$refOptions"] = {
  resolve: {
    http: {
      // @ts-expect-error @TODO fix it later
      async read(file) {
        const schemaCopy = transformSchema(collection[file.url].schema);
        return schemaCopy;
      },
    },
  },
};

export async function createInterfaceFromSchema(
  inputSchema: IJSONSchema,
  options?: Partial<Omit<JS2TSOptions, "$refOptions" | "bannerComment">>,
): Promise<ISchemaInterface> {
  const schemaCopy = transformSchema(inputSchema);
  const interfaceCode = await compileSchemaInterface(
    // @ts-expect-error draft4 type
    schemaCopy,
    schemaCopy.$id,
    { ...options, bannerComment: "", $refOptions: parserOptions },
  );

  const schemaInterface: ISchemaInterface = {
    inputSchema: schemaCopy,
    code: interfaceCode,
  };

  return schemaInterface;
}

function transformSchema(inputSchema: IJSONSchema): IJSONSchema {
  const modifedSchema = fromJSON<IJSONSchema>(toJSON(inputSchema));
  modifedSchema.title = `I${modifedSchema.title}`;

  return modifedSchema;
}
