import path from "node:path";
import fs from "node:fs/promises";
import { multilineString } from "#lib/strings";
import { writeToFile } from "#lib/fs";
import { CodegenError } from "./errors.js";
import {
  type IModule,
  indexFilename,
  libFilename,
  typesFilename,
  type ICodegenGraph,
  type ICodegenNode,
  type IExport,
} from "./types.js";

export async function runCodegenGraph(
  moduleFolder: string,
  { root, nodes }: ICodegenGraph,
) {
  const rootFolder = path.join(moduleFolder, root);

  // @TODO more intelligent cleanup
  await fs.rm(rootFolder, { recursive: true, force: true });

  for await (const node of nodes) {
    await createNode(rootFolder, node);
  }
}

export async function createNode(
  rootFolder: string,
  { nodePath, lib, types }: ICodegenNode,
) {
  const nodeFolder = path.join(rootFolder, nodePath);
  const indexExports: IExport[] = [];

  if (!lib && !types) {
    throw new CodegenError(
      `The codegen node at "${nodeFolder}" doesn't produce neither lib nor types.`,
    );
  }

  if (lib) {
    const exports = await createLib(nodeFolder, lib);
    indexExports.push(...exports);
  }

  if (types) {
    const exports = await createTypes(nodeFolder, types);
    indexExports.push(...exports);
  }

  await createIndex(nodeFolder, indexExports);
}

export async function createLib(nodeFolder: string, lib: IModule) {
  const libPath = path.join(nodeFolder, `${libFilename}.ts`);
  const indexExports = [];
  const { exports, result } = lib;
  const { concrete, types } = exports;

  if (concrete) {
    const exports = concrete.map<IExport>((name) => {
      return {
        name,
        path: `./${libFilename}.js`,
      };
    });
    indexExports.push(...exports);
  }

  if (types) {
    const exports = types.map<IExport>((name) => {
      return {
        name,
        path: `./${libFilename}.js`,
        isAbstract: true,
      };
    });
    indexExports.push(...exports);
  }

  await writeToFile(libPath, result);

  return indexExports;
}

export async function createTypes(nodeFolder: string, types: IModule) {
  const typesPath = path.join(nodeFolder, `${typesFilename}.ts`);
  const indexExports = [];
  const { exports, result } = types;
  const { concrete, types: abstract } = exports;

  if (concrete) {
    const exports = concrete.map<IExport>((name) => {
      return {
        name,
        path: `./${typesFilename}.js`,
      };
    });
    indexExports.push(...exports);
  }

  if (abstract) {
    const exports = abstract.map<IExport>((name) => {
      return {
        name,
        path: `./${typesFilename}.js`,
        isAbstract: true,
      };
    });
    indexExports.push(...exports);
  }

  await writeToFile(typesPath, result);

  return indexExports;
}

export async function createIndex(nodeFolder: string, exports: IExport[]) {
  const indexPath = path.join(nodeFolder, `${indexFilename}.ts`);
  const moduleMap = exports.reduce<Map<string, IExport[]>>(
    (map, exportInfo) => {
      const { path } = exportInfo;

      if (map.has(path)) {
        // biome-ignore lint/style/noNonNullAssertion: typescript `Map` methods
        map.get(path)!.push(exportInfo);
      } else {
        map.set(path, [exportInfo]);
      }

      return map;
    },
    new Map(),
  );

  const lines: string[] = [];

  for (const [path, exports] of moduleMap) {
    const types: string[] = [];
    const concrete: string[] = [];

    for (const { name, isAbstract } of exports) {
      isAbstract ? types.push(name) : concrete.push(name);
    }

    if (concrete.length) {
      lines.push(`export {${concrete.join(", ")}} from "${path}";`);
    }

    if (types.length) {
      lines.push(`export type {${types.join(", ")}} from "${path}";`);
    }
  }

  const result = multilineString(...lines);

  await writeToFile(indexPath, result);
}
