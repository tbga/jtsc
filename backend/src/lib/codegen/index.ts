export { runCodegen } from "./lib.js";
export {
  collectJSONSchemas,
  createInterfaceFromSchema,
  schemaFileEnd,
} from "./json-schema.js";
export type { IJSONSchemaCollection } from "./json-schema.js";
export { CodegenError } from "./errors.js";
export type {
  ICodegen,
  ICodegenExport,
  ICodegenModule,
  ICodegenFunc,
  ICodegenGraph,
  ICodegenNode,
  IExport,
  IModule,
} from "./types.js";
