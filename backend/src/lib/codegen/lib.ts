import path from "node:path";
import fs from "node:fs/promises";
import { cwd } from "node:process";
import { isNativeError } from "node:util/types";
import { dynamicImport, reduceFolder } from "#lib/fs";
import { multilineString } from "#lib/strings";
import { runCodegenGraph } from "./graph.js";
import {
  type ICodegen,
  type ICodegenModule,
  codegenFolder,
  generatorModule,
  generatorFilename,
  resultModule,
  indexModule,
  resultFilename,
  type ICodegenFunc,
  type ICodegenGraph,
} from "./types.js";

export async function runCodegen() {
  const codegenPath = path.join(cwd(), ...codegenFolder);
  console.log("Codegen started.");

  const codegenDirs = await reduceFolder<string[]>(
    codegenPath,
    [],
    async (codegenDirs, entryPath, dirEntry) => {
      try {
        if (!dirEntry.isFile() || dirEntry.name !== generatorModule) {
          return codegenDirs;
        }

        const modulePath = path.format(entryPath);
        // @TODO remove lib export logic
        const { default: codegen, ...libExports }: ICodegenModule =
          await dynamicImport(modulePath);
        const codegenInfo = await codegen();

        if (isCodegenGraph(codegenInfo)) {
          await runCodegenGraph(entryPath.dir, codegenInfo);
        } else {
          const { exports, result } = codegenInfo;
          const importKeys = Object.keys(libExports);
          const imports = importKeys.length
            ? multilineString(
                "import {",
                Object.keys(libExports).join(", "),
                `} from "./${generatorFilename}.js"`,
              )
            : undefined;
          const resultPath = path.join(entryPath.dir, resultModule);
          const finalResult = multilineString(
            "/*",
            "  This module was created by the codegen, do not edit it manually.",
            "*/",
            imports,
            result,
            "\n",
          );
          const indexPath = path.join(entryPath.dir, indexModule);
          const indexContent = createIndex(exports);
          const codegenModule = {
            index: indexContent,
            result: finalResult,
          };

          await fs.writeFile(resultPath, codegenModule.result);
          await fs.writeFile(indexPath, codegenModule.index);
        }

        codegenDirs.push(path.relative(codegenPath, entryPath.dir));

        return codegenDirs;
      } catch (error) {
        if (!isNativeError(error)) {
          throw error;
        }

        const message = multilineString(
          "Failed to run codegen",
          `Reason: ${error.message}`,
        );
        throw new Error(message, { cause: error });
      }
    },
  );
  console.log("Codegen finished.");

  return codegenDirs;
}

function createIndex(exports: ICodegen["exports"]): string {
  const concreteExports = [
    "export {",
    (exports.concrete || []).join(", "),
    `} from "./${resultFilename}.js"`,
  ].join(" ");
  const typeExports = [
    "export type {",
    (exports.types || []).join(", "),
    `} from "./${resultFilename}.js"`,
  ].join(" ");
  const indexContent = multilineString(concreteExports, typeExports);
  return indexContent;
}

function isCodegenGraph(
  info: Awaited<ReturnType<ICodegenFunc>>,
  // @ts-expect-error `"type"` is a dicriminator key
): info is ICodegenGraph {
  return "type" in info;
}
