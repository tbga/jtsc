export interface ICodegenModule extends Record<string, unknown> {
  default: ICodegenFunc;
}

export type ICodegenFunc<
  ReturnShape extends ICodegen | ICodegenGraph = ICodegen,
> = () => Promise<ReturnShape>;

export interface ICodegen {
  /**
   * Export info used for creating index file.
   */
  exports: ICodegenExport;
  result: string;
}

export interface ICodegenGraph {
  type: "graph";
  root: string;
  nodes: ICodegenNode[];
}

export interface ICodegenNode {
  /**
   * A path to the module relative to the graph root.
   */
  nodePath: string;
  lib?: IModule;
  types?: IModule;
}

export interface IModule {
  exports: ICodegenExport;
  result: string;
}

export interface IExport {
  name: string;
  path: string;
  isAbstract?: boolean;
}

/**
 * Exports of the module.
 */
export interface ICodegenExport {
  types?: string[];
  concrete?: string[];
}

export const codegenFolder = ["src", "codegen"];
export const generatorFilename = "generator";
export const generatorModule = `${generatorFilename}.ts`;
export const resultFilename = "result";
export const resultModule = `${resultFilename}.ts`;
export const indexFilename = "index";
export const indexModule = `${indexFilename}.ts`;
export const libFilename = "lib";
export const libModule = `${libFilename}.ts`;
export const typesFilename = "types";
