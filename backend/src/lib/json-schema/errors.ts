import type { DefinedError } from "ajv";
import { ProjectError } from "#lib/errors";
import { multilineString } from "#lib/strings";

export class ValidationError extends ProjectError {
  errors: DefinedError[];
  schemaID: string;

  constructor(errors: DefinedError[], schemaID: string) {
    const errorMessages = errors.map((error) => {
      if (!error.message) {
        throw new ProjectError(`No message was provided for error ${error}.`);
      }

      return error.message;
    });
    const message = multilineString(
      `Failed to validate according to schema "${schemaID}"`,
      ...errorMessages,
    );

    super(message);
    this.name = this.constructor.name;
    this.errors = errors;
    this.schemaID = schemaID;
  }
}
