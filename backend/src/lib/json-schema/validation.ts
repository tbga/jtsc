import Ajv, {
  type ValidateFunction,
  type DefinedError,
} from "ajv/dist/2020.js";
import addFormats from "ajv-formats";
import { schemaMap } from "#codegen/schema-map";
import { ValidationError } from "./errors.js";
import type { IJSONSchema, ISchemaMap } from "./types.js";

let ajv: Ajv.default | undefined = undefined;

export function createAJV(schemaMap: ISchemaMap) {
  const newAjv = new Ajv.default({
    schemas: Object.values(schemaMap),
  });

  addFormats.default(newAjv);

  ajv = newAjv;

  return newAjv;
}

/**
 * @TODO less side-effecty behaviour
 * @TODO make it actually work
 */
export function addSchema(schema: IJSONSchema) {
  return;
  // if (schema.$id in schemaMap) {
  //   throw new ProjectError(
  //     `Schema with "$id" "${schema.$id}" already exists in the map.`
  //   );
  // }

  // Object.defineProperty(schemaMap, schema.$id, {
  //   value: schema,
  //   enumerable: true,
  // });
}

/**
 * @TODO defered compilation
 */
export function createValidator<SchemaInterface>(schema: IJSONSchema) {
  let validate: ValidateFunction<SchemaInterface> | undefined = undefined;

  return (inputJSON: unknown): inputJSON is SchemaInterface => {
    if (!ajv) {
      ajv = createAJV(schemaMap as unknown as ISchemaMap);
    }

    if (!validate) {
      validate = ajv.getSchema<SchemaInterface>(schema.$id);

      if (!validate) {
        throw new Error(
          `JSON Schema with "$id" "${schema.$id}" doesn't exist.`,
        );
      }
    }

    const result = validate(inputJSON);

    if (!result) {
      // `errors` key is always an array when validation is failed
      const errors = [...(validate.errors as DefinedError[])];
      throw new ValidationError(errors, schema.$id);
    }

    return true;
  };
}
