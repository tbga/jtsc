import type { IJSONSubSchema } from "./types.js";

export interface IRefSet extends Set<IRefKey> {}
type IRefKey = Required<IJSONSubSchema>["$ref"];

export function collectRefs(
  schema: IJSONSubSchema,
  refs: IRefSet = new Set<IRefKey>(),
): IRefSet {
  if ("type" in schema && schema.type === "array") {
    // @ts-expect-error narrowed type
    collectArraySchemaRefs(schema, refs);
  }

  if ("$ref" in schema && typeof schema.$ref === "string") {
    refs.add(schema.$ref);
  }

  if ("allOf" in schema && schema.allOf) {
    for (const allOfSchema of schema.allOf) {
      if (typeof allOfSchema === "boolean") {
        continue;
      }

      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(allOfSchema, refs);
    }
  }

  if ("anyOf" in schema && schema.anyOf) {
    for (const anyOfSchema of schema.anyOf) {
      if (typeof anyOfSchema === "boolean") {
        continue;
      }
      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(anyOfSchema, refs);
    }
  }

  if ("oneOf" in schema && schema.oneOf) {
    for (const oneOfSchema of schema.oneOf) {
      if (typeof oneOfSchema === "boolean") {
        continue;
      }
      // @ts-expect-error underlying interface being autstic over booleans
      collectRefs(oneOfSchema, refs);
    }
  }

  if ("propertyNames" in schema && schema.propertyNames) {
    collectRefs(schema.propertyNames, refs);
  }

  if ("properties" in schema && schema.properties) {
    // collect refs from propery schemas
    for (const propertySchema of Object.values(schema.properties)) {
      if (typeof propertySchema === "boolean") {
        continue;
      }
      collectRefs(propertySchema, refs);
    }
  }

  if (
    "additionalProperties" in schema &&
    typeof schema.additionalProperties !== "boolean" &&
    schema.additionalProperties
  ) {
    collectRefs(schema.additionalProperties, refs);
  }

  // remove self-references
  refs.delete("#");

  return refs;
}

interface IArraySchema extends IJSONSubSchema {
  type: "array";
}

function collectArraySchemaRefs(schema: IArraySchema, refs: IRefSet) {
  if (!("items" in schema) || schema.items === undefined) {
    return refs;
  }

  const items = schema.items;

  if (!Array.isArray(items)) {
    collectRefs(items, refs);
  } else {
    for (const itemSchema of items) {
      collectRefs(itemSchema, refs);
    }
  }

  return refs;
}
