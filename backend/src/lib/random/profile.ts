import type { IProfileInit } from "#entities/profile";
import type { IArtist } from "#entities/artist";
import type { ISite } from "#entities/site";
import { EMPTY_OBJECT } from "#types";
import {
  maybe,
  randomString,
  randomLoremIpsum,
  randomItems,
  randomNumber,
  randomItem,
  randomBoolean,
  createRandomSequencer,
} from "./lib.js";
import { randomArtistInits } from "./artist.js";
import { randomNameInits } from "./name.js";

export const randomProfileInits = createRandomSequencer(
  EMPTY_OBJECT,
  randomProfileInit,
);

interface IRandomProfileInit {
  artist_ids?: IArtist["id"][];
  site_ids?: ISite["id"][];
}

export function randomProfileInit({
  artist_ids: artist_id_pool,
  site_ids,
}: IRandomProfileInit): IProfileInit {
  const site_id = site_ids && randomItem(site_ids);
  const original_id = maybe(randomString);
  const original_bio = maybe(randomLoremIpsum);
  const names = maybe(
    () => {
      return randomNameInits(randomNumber(0, 15), maybe(randomBoolean));
    },
    { probability: 0.8 },
  );
  const artist_ids = artist_id_pool && randomItems(artist_id_pool);
  const artist_inits = maybe(() => randomArtistInits(randomNumber(0, 10)));
  const artists = { ids: artist_ids, inits: artist_inits };

  const profileInit: ReturnType<typeof randomProfileInit> = {
    site_id,
    original_id,
    original_bio,
    // @ts-expect-error tuple types
    names,
    //@ts-expect-error tuple types
    artists,
  };

  return profileInit;
}
