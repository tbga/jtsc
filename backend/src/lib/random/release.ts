import type { IReleaseFileInitDB } from "#database/queries/releases";
import type { IFile } from "#entities/file";
import type { IPost } from "#entities/post";
import type { IProfile } from "#entities/profile";
import type { IRelease, IReleaseInit } from "#entities/release";
import type { ISite } from "#entities/site";
import { EMPTY_ARRAY, EMPTY_OBJECT } from "#types";
import {
  createRandomSequencer,
  randomString,
  maybe,
  randomLoremIpsum,
  randomItem,
  randomDate,
  randomItems,
  randomNumber,
} from "./lib.js";
import { randomPostInits } from "./post.js";

export const randomReleaseInits = createRandomSequencer(
  EMPTY_OBJECT,
  randomReleaseInit,
);

interface IRandomReleaseInitArgs {
  site_ids?: readonly ISite["id"][];
  profile_ids?: readonly IProfile["id"][];
  post_ids?: readonly IPost["id"][];
}

/**
 * @TODO preview inits
 */
export function randomReleaseInit({
  site_ids,
  profile_ids,
  post_ids,
}: IRandomReleaseInitArgs): IReleaseInit {
  const site_id = site_ids && randomItem(site_ids);
  // @TODO many profiles
  const profile_id = profile_ids && randomItem(profile_ids);
  const title = maybe(() => {
    return randomString(1, 100);
  });
  const description = maybe(() => {
    return randomLoremIpsum(2, 7);
  });
  const original_release_id = maybe(() => {
    return randomString(1, 100);
  });
  const released_at = maybe(() => {
    return randomDate();
  });
  const taken_down_at = maybe(() => {
    return randomDate(released_at);
  });
  const last_change_at = maybe(() => {
    return randomDate(taken_down_at ?? released_at, taken_down_at);
  });
  const postIDs = post_ids && randomItems(post_ids);
  const postInits = randomPostInits(randomNumber(1, 12), EMPTY_OBJECT);

  const init: IReleaseInit = {
    site_id,
    profile_id,
    title,
    description,
    original_release_id,
    released_at,
    last_change_at,
    taken_down_at,
    posts: {
      // @ts-expect-error tuple type
      ids: postIDs,
      // @ts-expect-error tuple type
      inits: postInits,
    },
  };

  return init;
}

export const randomReleaseFiles = createRandomSequencer(
  EMPTY_OBJECT,
  randomReleaseFile,
);

interface IRandomReleaseFileArgs {
  release_ids: IRelease["id"][];
  file_ids: IFile["id"][];
}

export function randomReleaseFile({
  release_ids,
  file_ids,
}: IRandomReleaseFileArgs): IReleaseFileInitDB {
  const release_id = randomItem(release_ids);
  const file_id = randomItem(file_ids);
  const init: IReleaseFileInitDB = {
    release_id,
    file_id,
  };

  return init;
}
