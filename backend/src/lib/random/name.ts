import type { INameInit } from "#entities/name";
import { randomName } from "./lib.js";

export function randomNameInits(
  amount = 500,
  sex?: boolean,
): ReturnType<typeof randomNameInit>[] {
  const nameInits: ReturnType<typeof randomNameInits> = [];

  for (let count = 1; count <= amount; count++) {
    const nameInit = randomNameInit(sex);
    nameInits.push(nameInit);
  }

  return nameInits;
}

export function randomNameInit(sex?: boolean): INameInit {
  const nameInit: ReturnType<typeof randomNameInit> = {
    full_name: randomName(sex),
  };

  return nameInit;
}
