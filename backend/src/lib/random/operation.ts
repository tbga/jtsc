import { ProjectError } from "#lib/errors";
import type { IOperationType, IOperation } from "#entities/operation";
import type { IProfile } from "#entities/profile";
import type { IFile } from "#entities/file";
import type { IRelease } from "#entities/release";
import type { IAccountDB } from "#database/queries/accounts";
import type { AnyArray } from "#types";
import { randomItem, randomNumber } from "./lib.js";
import { randomProfileInits } from "./profile.js";
import { randomReleaseInits } from "./release.js";

interface IBaseRandomInputDataOptions {
  type: IOperation["type"];
  account_ids: IAccountDB["id"][];
  minAmount?: number;
  maxAmount?: number;
}

export interface IOperationTypeOptions {
  local_upload: () => IBaseRandomInputDataOptions;
  releases_create: () => IBaseRandomInputDataOptions & {
    type: "releases_create";
    profile_ids: IProfile["id"][];
  };
  profiles_create: () => IBaseRandomInputDataOptions & {
    type: "profiles_create";
  };
  releases_previews_create: () => IBaseRandomInputDataOptions & {
    type: "releases_previews_create";
    release_ids: IRelease["id"][];
  };
}

interface IOperationInit extends Pick<IOperation, "type" | "input_data"> {}

/**
 * @param operationTypes `"public_exports_finish"` is excluded
 * because it requires admin mock and selection
 */
export function randomOperationInits(
  amount: number,
  operationTypes: AnyArray<
    Exclude<
      IOperationType,
      | "public_exports_finish"
      | "public_imports_create"
      | "public_imports_consume"
    >
  >,
  typeOptions: IOperationTypeOptions,
): IOperationInit[] {
  const inits: IOperationInit[] = [];

  for (let count = 1; count <= amount; count++) {
    const type = randomItem(operationTypes);
    const options = typeOptions[type]();
    const operationInit = randomOperationInit(
      // @ts-expect-error @TODO rewrite random gen
      options,
    );
    inits.push(operationInit);
  }

  return inits;
}

type IRandomInputDataOptions = IBaseRandomInputDataOptions &
  (
    | { type: "releases_create"; profile_ids: IProfile["id"][] }
    | {
        type: "profiles_create";
        portrait_ids: IFile["id"][];
      }
    | { type: "releases_previews_create"; release_ids: IRelease["id"][] }
  );

export function randomOperationInit(
  options: IRandomInputDataOptions,
): IOperationInit {
  const input_data = randomInputData(options);
  const init: IOperationInit = {
    type: options.type,
    input_data,
  };

  return init;
}

function randomInputData(options: IRandomInputDataOptions) {
  const { type } = options;

  switch (type) {
    case "releases_create": {
      const { profile_ids, minAmount, maxAmount } = options;
      const releasesAmount = randomNumber(minAmount ?? 1, maxAmount ?? 150);
      const inputData = randomReleaseInits(releasesAmount, { profile_ids });

      return inputData;
    }

    case "profiles_create": {
      const { minAmount, maxAmount } = options;
      const profileAmount = randomNumber(minAmount ?? 1, maxAmount ?? 150);
      const inputData = randomProfileInits(profileAmount, {});

      return inputData;
    }

    default: {
      throw new ProjectError(
        `Unknown operation type "${type satisfies never}".`,
      );
    }
  }
}
