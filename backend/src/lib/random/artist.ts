import type { IArtistDBUpdate } from "#database/queries/artists";
import type { IArtistUpdate, IArtistInit, IArtist } from "#entities/artist";
import { randomItem, maybe } from "./lib.js";

export function randomArtistInits(amount = 500): IArtistInit[] {
  const inits: ReturnType<typeof randomArtistInits> = [];

  for (let count = 1; count <= amount; count++) {
    const artistInit = randomArtistInit();
    inits.push(artistInit);
  }

  return inits;
}

export function randomArtistInit(): IArtistInit {
  const randomArtistInit: IArtistInit = {
    sex: randomItem([undefined, true, false]),
  };

  return randomArtistInit;
}

export function randomArtistsUpdates(artists: IArtist["id"][]) {
  const updates = artists.map<IArtistDBUpdate>((id) => {
    const update = randomArtistUpdate();
    return {
      id,
      sex: update.sex,
    };
  });

  return updates;
}

export function randomArtistUpdate(): IArtistUpdate {
  const artistUpdate: ReturnType<typeof randomArtistUpdate> = {
    sex: maybe(() => randomItem([undefined, true, false])),
  };

  return artistUpdate;
}
