import { type IAccountInit, accountInitSchema } from "#entities/account";
import { EMPTY_OBJECT } from "#types";
import { createRandomSequencer, randomString } from "./lib.js";

export const randomAccountInits = createRandomSequencer(
  EMPTY_OBJECT,
  randomAccountInit,
);

const { minLength: minLog, maxLength: maxLog } =
  accountInitSchema.properties.login;
const { minLength: minPass, maxLength: maxPass } =
  accountInitSchema.properties.password;

function randomAccountInit(): IAccountInit {
  const login = randomString(minLog, maxLog);
  const password = randomString(minPass, maxPass);

  const init: IAccountInit = {
    login,
    password,
  };

  return init;
}
