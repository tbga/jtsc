import { faker } from "@faker-js/faker";
import {
  type IDateTime,
  nowISO,
  toISODateTime,
  unixEraStart,
} from "#lib/dates";
import { BIGINT_ONE, BIGINT_ZERO } from "#lib/numbers/bigint";
import { type AnyArray, EMPTY_OBJECT } from "#types";

const { helpers, datatype, date, random, lorem, name, number, string, word } =
  faker;

export const { maybe } = helpers;

export const randomBoolean = datatype.boolean;

export function randomNumber(min = 0, max: number = min + 1) {
  return number.int({ min, max });
}

export function randomBigint(
  min: bigint = BIGINT_ZERO,
  max: bigint = min + BIGINT_ONE,
) {
  return number.bigInt({ min, max });
}

export function randomPercentage(min = 1, max = 99) {
  return randomNumber(min, max);
}

export function randomString(minLength = 1, maxLength = 19) {
  const length = { min: minLength, max: maxLength };
  const resultString = string.alphanumeric({ length, casing: "mixed" });

  return resultString;
}

export function randomWord() {
  const resultWord = randomBoolean() ? lorem.word() : word.sample();

  return resultWord;
}

export function randomWords(min = 1, max = 3) {
  const resultWords = randomBoolean()
    ? lorem.words({ min, max })
    : word.words({ count: { min, max } });

  return resultWords;
}

/**
 *
 * @param min Minimum amount of paragraphs.
 * @param max Maximum amount of paragraphs.
 * @returns Multi-paragraph lorem ipsum.
 */
export function randomLoremIpsum(min = 3, max = 5): string {
  const loremParagraphs = lorem.paragraphs({ min, max });

  return loremParagraphs;
}

export function randomName(sex?: boolean) {
  const finalSex =
    sex !== undefined
      ? sex
        ? "male"
        : "female"
      : randomBoolean()
        ? "male"
        : "female";
  const names = [
    `${name.prefix(finalSex)} ${name.firstName(finalSex)} ${name.middleName(
      finalSex,
    )} ${name.lastName(finalSex)}`,
    `${name.fullName({ sex: finalSex })}`,
  ];

  return randomItem(names);
}

export function randomItem<ItemType = unknown>(
  inputArray: AnyArray<ItemType>,
): ItemType {
  return helpers.arrayElement(inputArray);
}

export function randomItems<ItemType = unknown>(
  inputArray: AnyArray<ItemType>,
  count?: number,
): ItemType[] {
  return helpers.arrayElements(inputArray, count);
}

export function randomDate(
  minDate: IDateTime = unixEraStart,
  maxDate: IDateTime = nowISO(),
) {
  return toISODateTime(date.between({ from: minDate, to: maxDate }));
}

interface ICreateRandomSequencerOptions {}

/**
 * @TODO type it properly
 */
export function createRandomSequencer<ItemType, ItemGenOptions>(
  seqOptions: ICreateRandomSequencerOptions,
  itemGenerator: (options: ItemGenOptions) => ItemType,
) {
  function generateItems(amount: number, options: ItemGenOptions) {
    const items: ItemType[] = [];

    for (let count = 1; count <= amount; count++) {
      const item = itemGenerator(options);
      items.push(item);
    }

    return items;
  }

  return generateItems;
}
