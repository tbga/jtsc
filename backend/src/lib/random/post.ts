import type { IArtist } from "#entities/artist";
import type { IPostInit } from "#entities/post";
import { randomArtistInits } from "#lib/random";
import { EMPTY_OBJECT } from "#types";
import {
  createRandomSequencer,
  maybe,
  randomItems,
  randomNumber,
} from "./lib.js";

export const randomPostInits = createRandomSequencer(
  EMPTY_OBJECT,
  randomPostInit,
);

interface IArgs {
  artist_ids?: IArtist["id"][];
}

export function randomPostInit({ artist_ids }: IArgs): IPostInit {
  const ids = !artist_ids ? undefined : randomItems(artist_ids);
  const inits = maybe(() => randomArtistInits(randomNumber(1, 27)));
  const artists: IPostInit["artists"] = {
    // @ts-expect-error tuple type
    ids,
    // @ts-expect-error tuple type
    inits,
  };
  const postInit: IPostInit = {
    artists,
  };

  return postInit;
}
