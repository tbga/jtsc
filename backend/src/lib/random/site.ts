import { IS_HTTPS_ENABLED } from "#environment/variables.js";
import type { ISiteDBUpdate } from "#database/queries/sites";
import type { ISite, ISiteUpdate, ISiteInit } from "#entities/site";
import { EMPTY_OBJECT } from "#types";
import {
  createRandomSequencer,
  randomWord,
  randomWords,
  randomLoremIpsum,
  maybe,
} from "./lib.js";

export const randomSiteInits = createRandomSequencer<ISiteInit, undefined>(
  EMPTY_OBJECT,
  randomSiteInit,
);

function randomSiteInit(): ISiteInit {
  const home_page = `${IS_HTTPS_ENABLED ? "https" : "http"}://localhost:3000`;
  const title = randomWord();
  const long_title = maybe(() => randomWords(2, 5));
  const description = maybe(() => randomLoremIpsum(1, 3));

  const init: ISiteInit = {
    home_page,
    title,
    long_title,
    description,
  };

  return init;
}

export function randomSiteUpdates(
  input_site_ids: ISite["id"][],
): ISiteDBUpdate[] {
  const updates = input_site_ids.map((id) => {
    const update = randomSiteUpdate();
    return {
      id,
      ...update,
    };
  });

  return updates;
}

export function randomSiteUpdate(): ISiteUpdate {
  const home_page = maybe(
    () => `${IS_HTTPS_ENABLED ? "https" : "http"}://localhost:3000`,
  );
  const title = maybe(() => randomWord());
  const long_title = maybe(() => randomWords(2, 5));
  const description = maybe(() => randomLoremIpsum(1, 3));

  const update = {
    home_page,
    title,
    long_title,
    description,
  };

  return update;
}
