export function isNonEmptyArray<ArrayItem = unknown>(
  inputArray: ArrayItem[],
): inputArray is [ArrayItem, ...ArrayItem[]] {
  return Boolean(inputArray.length);
}
