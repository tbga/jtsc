import path from "node:path";
import type { PathLike } from "node:fs";
import fs from "node:fs/promises";

/**
 * Creates a path entry with all subfolders up to it recursively.
 * @param entryPath
 * @param isFile Assumes the path is a file and therefore will create paths up to its folder.
 */
export async function ensurePath(entryPath: PathLike, isFile = true) {
  const finalPath = isFile ? path.dirname(String(entryPath)) : entryPath;
  await fs.mkdir(finalPath, { recursive: true });
}
