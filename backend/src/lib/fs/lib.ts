import fs from "node:fs/promises";
import path from "node:path";
import { getSystemErrorName } from "node:util";
import { isNativeError } from "node:util/types";
import { FileSystemError, isSystemError } from "./errors.js";

interface IWriteTOFileOptions {
  /**
   * Create the file at the path if doesn't exist.
   * @default true
   */
  createNew?: boolean;
}

const defaultOptions: Required<IWriteTOFileOptions> = {
  createNew: true,
};

export async function writeToFile(
  filePath: Parameters<typeof fs.writeFile>["0"],
  content: Parameters<typeof fs.writeFile>["1"],
  options?: Partial<IWriteTOFileOptions>,
) {
  const finalOptions =
    options && !Object.is(options, defaultOptions)
      ? { ...defaultOptions, options }
      : defaultOptions;

  try {
    await fs.writeFile(filePath, content);
  } catch (error) {
    if (!isNativeError(error)) {
      throw error;
    }

    const isMissingFileError =
      finalOptions.createNew &&
      isSystemError(error) &&
      getSystemErrorName(error.errno) === "ENOENT";

    if (isMissingFileError) {
      await fs.mkdir(path.dirname(String(filePath)), { recursive: true });
      await fs.appendFile(filePath, "");
      await writeToFile(filePath, content, finalOptions);
      return;
    }

    throw new FileSystemError(`Failed to write a file to "${filePath}".`, {
      cause: error,
      path: String(filePath),
    });
  }
}
