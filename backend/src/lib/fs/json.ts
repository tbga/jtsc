import fs from "node:fs/promises";
import type { PathLike } from "node:fs";
import { fromJSON, toJSON } from "#lib/json";
import { writeToFile } from "./lib.js";

export async function readJSONFile<OutputType = unknown>(filePath: PathLike) {
  const fileContent = await fs.readFile(filePath, { encoding: "utf-8" });
  const value = fromJSON<OutputType>(fileContent);

  return value;
}

export async function writeJSONFile<InputType = unknown>(
  filePath: PathLike,
  value: InputType,
  options?: Parameters<typeof writeToFile>["2"],
) {
  const jsonString = toJSON(value);
  await writeToFile(filePath, jsonString, options);
}
