import { reduceFolder } from "./reduce-folder.js";
import type { IFolderStats } from "./types.js";

/**
 * Recursively walks provided folder
 * and collects item counts.
 * Does not resolve symlinks or collect file info.
 */
export async function folderStat(folderPath: string) {
  const initStats: IFolderStats = {
    items: 0,
    folders: 0,
    files: 0,
    symlinks: 0,
  };

  const stats = await reduceFolder(
    folderPath,
    initStats,
    async (stats, parsedPath, entry) => {
      stats.items++;

      if (entry.isDirectory()) {
        stats.folders++;
      }

      if (entry.isFile()) {
        stats.files++;
      }

      if (entry.isSymbolicLink()) {
        stats.symlinks++;
      }

      return stats;
    },
  );

  return stats;
}
