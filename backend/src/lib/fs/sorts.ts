import type { Dirent } from "node:fs";

/**
 * Sort folders first then by name, putting hidden entries at the bottom.
 * @param prev
 * @param next
 * @returns
 */
export function foldersFirstThenNames(prev: Dirent, next: Dirent): number {
  const prevDirectory = prev.isDirectory();
  const nextDirectory = next.isDirectory();

  // prev is not a folder while next is
  if (!prevDirectory && nextDirectory) {
    return 1;
  }

  // prev is a folder while next is not
  if (prevDirectory && !nextDirectory) {
    return -1;
  }

  const isPrevHidden = prev.name.startsWith(".");
  const isNextHidden = next.name.startsWith(".");

  // prev is not hidden while next is
  if (!isPrevHidden && isNextHidden) {
    return -1;
  }

  // prev is hidden while next isn't
  if (isPrevHidden && !isNextHidden) {
    return 1;
  }

  // both are of the same type
  return prev.name.localeCompare(next.name);
}

/**
 * Sort folders last then by name, putting hidden entries at the bottom.
 * @param prev
 * @param next
 * @returns
 */
export function foldersLastThenNames(prev: Dirent, next: Dirent): number {
  const prevDirectory = prev.isDirectory();
  const nextDirectory = next.isDirectory();

  // prev is a folder while next is not
  if (prevDirectory && !nextDirectory) {
    return 1;
  }

  // prev is not a folder while next is
  if (!prevDirectory && nextDirectory) {
    return -1;
  }

  const isPrevHidden = prev.name.startsWith(".");
  const isNextHidden = next.name.startsWith(".");

  // prev is not hidden while next is
  if (!isPrevHidden && isNextHidden) {
    return -1;
  }

  // prev is hidden while next isn't
  if (isPrevHidden && !isNextHidden) {
    return 1;
  }

  // both are of the same type
  return prev.name.localeCompare(next.name);
}
