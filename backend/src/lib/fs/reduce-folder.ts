import type { Dirent } from "node:fs";
import path, { type ParsedPath } from "node:path";
import { isNativeError } from "node:util/types";
import { FolderReductionError } from "./errors.js";
import { readFolder } from "./folder.js";
import { foldersLastThenNames } from "./sorts.js";

export type IWalkFunc<InitType> = (
  accumulator: InitType,
  parsedPath: ParsedPath,
  dirEntry: Dirent,
) => Promise<typeof accumulator>;

/**
 * Walk from the root folder and call the callback
 * on each file and folder within it,
 * visiting all nested folders.
 * @TODOs
 * - non-recursive reduction
 * - more precise errors
 * (right now they are pointing to the folder when
 * the file is the cause).
 * @param rootFolder A folder to start walking from.
 */
export async function reduceFolder<InitType = unknown>(
  rootFolder: string,
  initValue: InitType,
  walkFunc: IWalkFunc<InitType>,
): Promise<typeof initValue> {
  // top level try...catch also included because `readFolder()` can fail.
  try {
    const rootEntries = await readFolder(rootFolder, foldersLastThenNames);
    let accumulator: InitType = initValue;

    for await (const dirEntry of rootEntries) {
      try {
        const entryPath = path.join(rootFolder, dirEntry.name);
        const parsedPath = path.parse(entryPath);
        // run callback on a file
        if (dirEntry.isFile()) {
          accumulator = await walkFunc(accumulator, parsedPath, dirEntry);
          continue;
        }

        if (dirEntry.isSymbolicLink()) {
          accumulator = await walkFunc(accumulator, parsedPath, dirEntry);
          continue;
        }

        // run the function itself on a folder
        if (dirEntry.isDirectory()) {
          accumulator = await walkFunc(accumulator, parsedPath, dirEntry);
          accumulator = await reduceFolder(entryPath, accumulator, walkFunc);
        }
      } catch (error) {
        if (!isNativeError(error) || FolderReductionError.isError(error)) {
          throw error;
        }

        const entryPath = path.join(rootFolder, dirEntry.name);
        throw new FolderReductionError(entryPath, error);
      }
    }

    return accumulator;
  } catch (error) {
    if (!isNativeError(error) || FolderReductionError.isError(error)) {
      throw error;
    }

    throw new FolderReductionError(rootFolder, error);
  }
}
