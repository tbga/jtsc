export interface IFolderStats {
  items: number;
  folders: number;
  files: number;
  symlinks: number;
}
