import { pathToFileURL } from "node:url";

/**
 * Need to wrap it into a separate function because
 * dynamic import on Windows requires `file:` `URL`.
 */
export async function dynamicImport(modulePath: string) {
  const fileURL = pathToFileURL(modulePath);
  const importedModule = await import(fileURL.toString());

  return importedModule;
}
