import type { Stats } from "node:fs";
import path from "node:path";
import fs from "node:fs/promises";
import { isNativeError } from "node:util/types";

interface ISymlinkStats {
  stats: Stats;
  paths: ISymlinkPath[];
}

interface ISymlinkPath {
  path: string;
  type: "file" | "folder" | "symlink" | "unknown";
  error?: Error;
}

/**
 * Collects extra info on a symlink.
 * @TODO finish it
 */
async function symlinkStat(symlinkPath: string): Promise<ISymlinkStats> {
  const stats = await fs.lstat(symlinkPath);
  const paths = await collectSymlinkPaths(symlinkPath);
  const symlinkStats = {
    stats,
    paths,
  };

  return symlinkStats;
}

async function collectSymlinkPaths(symlinkPath: string) {
  const paths: ISymlinkPath[] = [];
  let currentPath: string | undefined = symlinkPath;

  do {
    const linkPath = await fs.readlink(currentPath);
    const resolvedPath = path.isAbsolute(linkPath)
      ? linkPath
      : path.resolve(path.dirname(currentPath), linkPath);

    try {
      const targetStats = await fs.lstat(resolvedPath);

      if (targetStats.isSymbolicLink()) {
        const symlinkPath: ISymlinkPath = {
          type: "symlink",
          path: resolvedPath,
        };

        paths.push(symlinkPath);
        currentPath = resolvedPath;
      }

      if (targetStats.isFile()) {
      }
    } catch (error) {
      const symlinkPath: ISymlinkPath = {
        type: "unknown",
        path: resolvedPath,
        error: isNativeError(error)
          ? error
          : new Error("Unknown error.", { cause: error }),
      };

      paths.push(symlinkPath);
      currentPath = resolvedPath;
    }

    currentPath = undefined;
  } while (currentPath !== undefined);

  return paths;
}
