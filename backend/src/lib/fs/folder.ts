import type { Dirent, PathLike } from "node:fs";
import fs from "node:fs/promises";
import { foldersFirstThenNames } from "./sorts.js";

type IPredicate = Parameters<Dirent[]["filter"]>["0"];

/**
 * @TODO default predicate
 */
export async function countFolderItems(
  folderPath: PathLike,
  predicate?: IPredicate,
) {
  const dirEntries = await fs.readdir(folderPath, {
    encoding: "utf-8",
    withFileTypes: true,
  });

  const count = !predicate
    ? dirEntries.length
    : dirEntries.filter(predicate).length;

  return count;
}

export async function readFolder(
  folderPath: PathLike,
  sortingFunction: Parameters<Dirent[]["sort"]>["0"] = foldersFirstThenNames,
) {
  const dirEntries = await fs.readdir(folderPath, {
    encoding: "utf-8",
    withFileTypes: true,
  });

  dirEntries.sort(sortingFunction);

  return dirEntries;
}
