export type { IFileSystem } from "#codegen/schema/lib/local-file-system/system";
export type { IPath } from "#codegen/schema/lib/local-file-system/path";
export type { IFile } from "#codegen/schema/lib/local-file-system/file";
export type { IFolder } from "#codegen/schema/lib/local-file-system/folder";
export type { IEntry } from "#codegen/schema/lib/local-file-system/entry";
