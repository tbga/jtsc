import { homedir } from "node:os";
import { platform } from "node:process";
import path from "node:path";
import fs from "node:fs/promises";
import type { Dirent, Stats } from "node:fs";
import { pathToFileURL, fileURLToPath } from "node:url";
import { countFolderItems, readFolder as fsReadFolder } from "#lib/fs";
import { LocalFileSystemError } from "./errors.js";
import type { IPath, IEntry, IFolder, IFile, IFileSystem } from "./types.js";

export {
  fileSystemSchema,
  validateFileSystem,
} from "#codegen/schema/lib/local-file-system/system";
export {
  pathSchema,
  validatePath,
} from "#codegen/schema/lib/local-file-system/path";
export {
  fileSchema,
  validateFile,
} from "#codegen/schema/lib/local-file-system/file";
export {
  folderSchema,
  validateFolder,
} from "#codegen/schema/lib/local-file-system/folder";
export {
  entrySchema,
  validateEntry,
} from "#codegen/schema/lib/local-file-system/entry";

const homeDir = pathToFileURL(homedir()).toString();

export async function getSystem(): Promise<IFileSystem> {
  const systemInfo: IFileSystem = {
    platform: platform,
    home_dir: homeDir,
  };

  return systemInfo;
}

export async function getEntry(fileURL: IPath): Promise<IEntry> {
  const localPath = fileURLToPath(fileURL);
  const stats = await fs.lstat(localPath);

  const type = guessEntryType(localPath, stats);

  switch (type) {
    case "folder": {
      const count = await countFolderItems(localPath, (entry) =>
        isValidEntry(entry),
      );
      const entry: IEntry = {
        type,
        name: path.basename(localPath),
        entries: count,
      };

      return entry;
    }

    case "file": {
      const entry: IEntry = {
        type,
        name: path.basename(localPath),
        size: stats.size,
      };

      return entry;
    }

    default: {
      throw new LocalFileSystemError(`Unsupported type "${type}".`);
    }
  }
}

export async function getFolder(fileURL: IPath): Promise<IFolder> {
  const localPath = fileURLToPath(fileURL);
  const dirEntries = await fsReadFolder(localPath);
  const validEntries = dirEntries.reduce<IEntry[]>((entries, dirEntry) => {
    if (!isValidEntry(dirEntry)) {
      return entries;
    }

    const entry = dirEntryToEntry(dirEntry);
    entries.push(entry);

    return entries;
  }, []);
  const folder: IFolder = {
    name: path.basename(localPath),
    entries: validEntries,
  };

  return folder;
}

export async function getFile(fileURL: IPath): Promise<IFile> {
  const localPath = fileURLToPath(fileURL);
  const stats = await fs.lstat(localPath);
  const file: IFile = {
    name: path.basename(localPath),
    size: String(stats.size),
  };

  return file;
}

function isValidEntry(dirEntry: Dirent) {
  return dirEntry.isDirectory() || dirEntry.isFile();
}

function guessEntryType(fileURL: IPath, entryStats: Stats): IEntry["type"] {
  const localPath = fileURLToPath(fileURL);
  if (entryStats.isDirectory()) {
    return "folder";
  }

  if (entryStats.isFile()) {
    return "file";
  }

  throw new LocalFileSystemError(`Path "${localPath}" is not a valid path.`);
}

function dirEntryToEntry(dirEntry: Dirent): IEntry {
  if (dirEntry.isDirectory()) {
    const folderEntry: IEntry = {
      type: "folder",
      name: dirEntry.name,
    };

    return folderEntry;
  }

  const fileEntry: IEntry = {
    type: "file",
    name: dirEntry.name,
  };

  return fileEntry;
}
