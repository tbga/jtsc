/**
 * @TODO completely refactor
 */
export { getSystem, getEntry, getFolder, getFile } from "./lib.js";
export { LocalFileSystemError } from "./errors.js";
export type { IFileSystem, IPath, IEntry, IFolder, IFile } from "./types.js";
