import { ProjectError } from "#lib/errors";

export class LocalFileSystemError extends ProjectError {}
