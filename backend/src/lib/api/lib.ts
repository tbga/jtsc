import { isNativeError } from "node:util/types";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";
import { ProjectError } from "#lib/errors";
import { toJSON } from "#lib/json";
import { serializeData } from "./serialize.js";
import type {
  IAPIResponseSuccess,
  IAPIResponseFailure,
  IErrorInput,
} from "./types.js";

export class APISuccess<DataShape = unknown>
  implements IAPIResponseSuccess<DataShape>
{
  is_successful: true;
  data: DataShape;

  constructor(data: DataShape) {
    this.is_successful = true;
    try {
      // @ts-expect-error proper null typing
      this.data = serializeData(data);
    } catch (error) {
      if (!isNativeError(error)) {
        throw error;
      }

      throw new ProjectError(
        !IS_DEVELOPMENT
          ? "Failed to serialize the input."
          : `Failed to serialize object ${toJSON(data)}`,
        { cause: error },
      );
    }
  }
}

export class APIFailure implements IAPIResponseFailure {
  is_successful: false;
  errors: string[];

  constructor(errorInput: IErrorInput) {
    this.is_successful = false;
    this.errors = !Array.isArray(errorInput)
      ? [typeof errorInput === "string" ? errorInput : errorInput.message]
      : errorInput.map((error) => error.message);
  }
}
