export function serializeData<DataType = unknown>(
  data: DataType,
): DataType | null {
  let serializedData: DataType | null;

  switch (typeof data) {
    case "boolean":
    case "string": {
      serializedData = data;
      break;
    }

    case "undefined": {
      serializedData = null;
      break;
    }

    case "object": {
      // `null` is an object ACKTHUALLY
      if (data === null) {
        serializedData = null;
        break;
      }

      if (Array.isArray(data)) {
        // @ts-expect-error `isArray()` returning `any[]`
        serializedData = serializeArray(data);
      } else {
        // @ts-expect-error `typeof data === "object"` doesn't mean shit
        serializedData = serializeObject(data);
      }
      break;
    }

    default: {
      serializedData = data;
      break;
    }
  }

  return serializedData;
}

export function serializeArray<ItemType>(data: ItemType[]): ItemType[] {
  const serializedArray = data.reduce<ItemType[]>((array, item) => {
    const serializedItem = serializeData(item);

    // skip null items
    if (serializedItem !== null) {
      array.push(serializedItem);
    }

    return array;
  }, []);

  return serializedArray;
}

export function serializeObject<ObjType extends Record<string, unknown>>(
  data: ObjType,
): ObjType | null {
  const serializedObject = Object.entries(data).reduce<ObjType>(
    (obj, [key, value]) => {
      const serializedValue = serializeData(value);

      if (serializedValue !== null) {
        Object.defineProperty(obj, key, {
          value: serializedValue,
          enumerable: true,
        });
      }

      return obj;
    },
    {} as ObjType,
  );

  if (!Object.keys(serializedObject).length) {
    return null;
  }

  return serializedObject;
}
