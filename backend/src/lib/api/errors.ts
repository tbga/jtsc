import { ClientError, type IErrorMessage } from "#lib/errors";

/**
 * The error for http 422 statuses.
 */
export class InputValidationError extends ClientError {
  /**
   * The error for http 422 statuses.
   */
  constructor(message: IErrorMessage, options?: ErrorOptions) {
    super(message, { status: 422, ...options });
  }
}
