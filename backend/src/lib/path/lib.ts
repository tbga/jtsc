import path from "node:path";

export function convertWindowsPathToLinux(winPath: string) {
  return winPath.split(path.win32.sep).join(path.posix.sep);
}
