import type { AnyArray } from "#types";

export function multilineString(...lines: AnyArray<string | undefined>) {
  const newLines = lines.filter((line) => line);
  const finalString = `${newLines.join("\n")}\n`;

  return finalString;
}

/**
 * Changes the first letter of a string to lower case.
 * @returns Lower-cased string.
 */
export function decapitalizeString(inputString: string) {
  const firstLetter = inputString.charAt(0).toLowerCase();
  const rest = inputString.slice(1);
  const result = `${firstLetter}${rest}`;

  return result;
}

/**
 * Splits the string into equal sized strings,
 * except for the last string.
 */
export function splitString(value: string, size = 8): string[] {
  const chunks: string[] = [];
  let currentIndex = 0;

  do {
    const finalIndex = currentIndex + size;
    const chunk = value.slice(currentIndex, finalIndex);

    chunks.push(chunk);

    currentIndex += size;
  } while (currentIndex < value.length);

  return chunks;
}
