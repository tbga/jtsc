export const LOG_TYPE = {
  DEBUG: "debug",
  INFO: "info",
  WARNING: "warning",
  ERROR: "error",
} as const;

export type ILogType = (typeof LOG_TYPE)[keyof typeof LOG_TYPE];
