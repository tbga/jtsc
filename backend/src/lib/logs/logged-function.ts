import { log } from "#lib/logs";

interface IOptions<ArgsType extends unknown[], OutputType> {
  onStart: string | ((...args: ArgsType) => string);
  onFinish?: string | ((value: OutputType) => string);
}

/**
 * @TODO
 * - return type inferrence from the function
 */
export function loggedFunction<ArgType extends unknown[], OutputType>(
  options: IOptions<ArgType, OutputType>,
  func: (...args: ArgType) => Promise<OutputType> | OutputType,
): typeof func {
  const { onStart, onFinish } = options;

  function logged(...args: Parameters<typeof func>): ReturnType<typeof func> {
    const startLog = typeof onStart === "string" ? onStart : onStart(...args);
    log(startLog);

    const result = func(...args);

    if (onFinish) {
      if (result instanceof Promise) {
        return result.then((value) => {
          const finishLog =
            typeof onFinish === "string" ? onFinish : onFinish(value);
          log(finishLog);

          return value;
        });
      }

      const finishLog =
        typeof onFinish === "string" ? onFinish : onFinish(result);
      log(finishLog);
    }

    return result;
  }

  return logged;
}
