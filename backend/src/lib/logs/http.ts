import logger from "morgan";

export function getHTTPLogger(isDev: boolean) {
  return isDev
    ? logger("dev")
    : logger("combined", {
        skip(req, res) {
          return res.statusCode < 400;
        },
      });
}
