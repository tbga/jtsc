import { ProjectError } from "#lib/errors";
import { toJSONPretty } from "#lib/json";
import { multilineString } from "#lib/strings";
import type { IMessage } from "#types";
import { LOG_TYPE, type ILogType } from "./types.js";

const logger = console;

export function log(message: IMessage, type: ILogType = LOG_TYPE.DEBUG) {
  const formattedMessage = Array.isArray(message)
    ? multilineString(...message)
    : message;

  switch (type) {
    case LOG_TYPE.DEBUG: {
      return logger.debug(formattedMessage);
    }

    case LOG_TYPE.INFO: {
      return logger.info(formattedMessage);
    }

    case LOG_TYPE.WARNING: {
      return logger.warn(formattedMessage);
    }

    case LOG_TYPE.ERROR: {
      return logger.error(formattedMessage);
    }

    default: {
      throw new ProjectError(`Unknown log type "${type}".`);
    }
  }
}

export function logJSON(data: unknown, type: ILogType = LOG_TYPE.DEBUG) {
  log(toJSONPretty(data), type);
}

export function debug(message: IMessage) {
  log(message, LOG_TYPE.DEBUG);
}
