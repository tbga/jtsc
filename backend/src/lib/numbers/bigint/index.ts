export const BIGINT_ZERO = BigInt(0);
export const BIGINT_ONE = BigInt(1);

export function bigIntMax(...args: bigint[]) {
  return args.reduce<bigint>(
    (max, current) => (current > max ? current : max),
    args[0],
  );
}
export function bigIntMin(...args: bigint[]) {
  return args.reduce<bigint>(
    (min, current) => (current < min ? current : min),
    args[0],
  );
}

export function bigIntMinAndMax(...args: bigint[]) {
  return args.reduce<[bigint, bigint]>(
    (minMax, current) => {
      const [min, max] = minMax;
      const nextMin = current < min ? current : min;
      const nextMax = current > max ? current : max;

      if (min !== nextMin) {
        minMax[0] = nextMin;
      }

      if (max !== nextMax) {
        minMax[1] = nextMax;
      }

      return minMax;
    },
    [args[0], args[0]],
  );
}
