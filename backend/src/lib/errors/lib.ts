import { ProjectError } from "./project.js";

export class NotImplementedError extends ProjectError {
  featureName: string;

  constructor(featureName: string) {
    super(`Feature "${featureName}" is not implemented.`);

    this.featureName = featureName;
  }
}
