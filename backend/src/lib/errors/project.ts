import { isNativeError } from "node:util/types";
import { multilineString } from "#lib/strings";
import type { IErrorMessage } from "./types.js";

export class ProjectError extends Error {
  static isError<ErrorType extends ProjectError>(
    this: new (
      // biome-ignore lint/suspicious/noExplicitAny: fuck OOP
      ...args: any[]
    ) => ErrorType,
    error: unknown,
  ): error is ErrorType {
    // biome-ignore lint/complexity/noThisInStatic: fuck OOP
    return error instanceof this;
  }

  constructor(
    message: IErrorMessage,
    options?: ErrorOptions,
    ...args: unknown[]
  ) {
    super(
      Array.isArray(message) ? multilineString(...message) : message,
      options,
      // @ts-expect-error base error args type
      ...args,
    );

    this.name = this.constructor.name;

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if ("captureStackTrace" in Error) {
      Error.captureStackTrace(this, this.constructor);
    }
  }

  getMessageStack() {
    const messages = [this.message];
    let currentCause = this.cause;

    while (true) {
      if (currentCause === undefined) {
        break;
      }

      if (!isNativeError(currentCause)) {
        messages.push(String(currentCause));
        break;
      }

      messages.push(currentCause.message);
      currentCause = currentCause.cause;
    }

    messages.reverse();

    return multilineString(...messages);
  }
}
