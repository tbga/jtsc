import type { IntRange } from "#types";
import { ProjectError } from "#lib/errors";
import { HTTP_STATUS } from "#lib/http";
import type { IErrorMessage } from "./types.js";

export interface IClientErrorOptions extends ErrorOptions {
  /**
   * `4xx` HTTP status code of the error.
   */
  status?: IntRange<400, 500>;
}

/**
 * The error which is safe to send to the client.
 */
export class ClientError extends ProjectError {
  status: number;

  /**
   * The error which is safe to send to the client.
   */
  constructor(message: IErrorMessage, options: IClientErrorOptions) {
    super(message, options);
    const { status } = options;

    if (status && (status < HTTP_STATUS.BAD_REQUEST || status > 499)) {
      throw new ProjectError(
        `Invalid status code "${status}" of the client error.`,
      );
    }

    this.status = status ?? HTTP_STATUS.BAD_REQUEST;
  }
}
