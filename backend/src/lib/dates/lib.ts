import {
  formatISO,
  parseISO,
  getUnixTime,
  fromUnixTime as fromUnixTimefn,
} from "date-fns";
import type { IDateTime } from "./types.js";

const defaultOptions: Parameters<typeof formatISO>["1"] = {
  format: "extended",
  representation: "complete",
};

export function toISODateTime(date: Date): IDateTime {
  return formatISO(date, defaultOptions);
}
export function fromISODateTime(date: IDateTime): Date {
  return parseISO(date);
}

export function nowISO() {
  return toISODateTime(new Date());
}

export function fromUnixTime(date: number) {
  return toISODateTime(fromUnixTimefn(date));
}

export function toUnixTime(date: IDateTime) {
  return getUnixTime(fromISODateTime(date));
}
