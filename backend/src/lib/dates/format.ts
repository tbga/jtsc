import { format } from "date-fns";
import { fromISODateTime } from "./lib.js";
import type { IDateTime } from "./types.js";

export const defaultFormat = "do MMMM yyyy GGGG";

export function formatDate(date: IDateTime) {
  return format(fromISODateTime(date), defaultFormat);
}
