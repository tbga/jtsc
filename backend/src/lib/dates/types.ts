export type { IDateTime } from "#codegen/schema/lib/types/dates/datetime";
export type { IDateTimeGeneric } from "#codegen/schema/lib/types/dates/generic";

export const unixEraStart = "1970-01-01T00:00:00.000Z";
