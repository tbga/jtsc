import { addYears as addYearsFunc } from "date-fns";
import { fromISODateTime, toISODateTime } from "./lib.js";
import type { IDateTime } from "./types.js";

export function addYears(date: IDateTime, amount: number) {
  return toISODateTime(addYearsFunc(fromISODateTime(date), amount));
}
