import path from "node:path";
import { cwd } from "node:process";
import fs from "node:fs/promises";
import { isNativeError } from "node:util/types";
import { defineProperty } from "#lib/std";
import { ensurePath, readJSONFile } from "#lib/fs";
import { log } from "#lib/logs";
import { validateBackendConfiguration } from "#codegen/schema/lib/configs/backend";
import { ConfigurationError } from "./errors.js";
import { getEnvironment } from "./environment.js";
import {
  configFileName,
  configFileNameDefault,
  type IConfiguration,
  type IConfigKey,
  configKeys,
  type IFinalConfiguration,
} from "./types.js";

let config: IFinalConfiguration | undefined = undefined;

export async function getConfiguration(): Promise<IFinalConfiguration> {
  return config ?? resolveConfiguration();
}

export async function resolveConfiguration(): Promise<IFinalConfiguration> {
  if (config) {
    return config;
  }

  const { PROJECT_ENV } = await getEnvironment();
  const configFolder = path.join(cwd(), "configs", PROJECT_ENV);

  try {
    const defaultConfigPath = path.join(configFolder, configFileNameDefault);
    const userConfigPath = path.join(configFolder, configFileName);

    const defaultConfig =
      await readJSONFile<Partial<IConfiguration>>(defaultConfigPath);
    const userConfig =
      await readJSONFile<Partial<IConfiguration>>(userConfigPath);
    const mergedConfig = mergeConfigs(defaultConfig, userConfig);

    validateBackendConfiguration(mergedConfig);

    const finalConfig = await parseConfig(mergedConfig);

    config = finalConfig;

    return finalConfig;
  } catch (error) {
    if (!isNativeError(error)) {
      throw error;
    }

    throw new ConfigurationError(
      `Failed to resolve configuration file at "${configFolder}".`,
      { cause: error },
    );
  }
}

function mergeConfigs(
  defaultConfig: Partial<IConfiguration>,
  userConfig: Partial<IConfiguration>,
): IConfiguration {
  const mergedConfig = { ...defaultConfig, ...userConfig };
  const resultConfig = Object.entries(mergedConfig).reduce<IConfiguration>(
    (finalConfig, [key, value]) => {
      if (!isConfigKey(key)) {
        // @ts-expect-error pass the key/value anyway
        // so the final config would be rejected by validation
        defineProperty(finalConfig, key, {
          value,
          enumerable: true,
          writable: false,
        });

        return finalConfig;
      }

      defineProperty(finalConfig, key, {
        value,
        enumerable: true,
      });

      return finalConfig;
    },
    {} as IConfiguration,
  );

  return resultConfig;
}

function isConfigKey(key: string): key is IConfigKey {
  // @ts-expect-error const array crying
  return configKeys.includes(key);
}

async function parseConfig(
  inputConfig: IConfiguration,
): Promise<IFinalConfiguration> {
  const finalConfig = {} as IFinalConfiguration;

  for await (const entry of Object.entries(inputConfig)) {
    // casting it this way because the object was already validated
    const [key, value] = entry as [
      keyof IConfiguration,
      IConfiguration[keyof IConfiguration],
    ];

    switch (key) {
      case "LOCAL_STORAGE_FOLDER":
      case "PUBLIC_STORAGE_FOLDER":
      case "TEMPORARY_FOLDER":
      case "DATABASE_FOLDER":
      case "PUBLIC_EXPORTS_FOLDER": {
        const resolvedPath = path.resolve(value as string);

        defineProperty(finalConfig, key, {
          value: resolvedPath,
          enumerable: true,
        });

        try {
          // @TODO check for path being a directory
          await fs.lstat(resolvedPath);
        } catch (error) {
          log(String(error));
          await ensurePath(resolvedPath, false);
        }

        break;
      }

      default: {
        defineProperty(finalConfig, key, {
          value,
          enumerable: true,
        });
        break;
      }
    }
  }

  return finalConfig;
}
