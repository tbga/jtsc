import {
  type IBackendConfiguration,
  backendConfigurationSchema,
} from "#codegen/schema/lib/configs/backend";
import {
  type IEnvironmentConfiguration,
  environmentConfigurationSchema,
} from "#codegen/schema/lib/configs/environment";

/**
 * Project configuration.
 */
export interface IConfiguration extends IBackendConfiguration {}
export interface IFinalConfiguration
  extends Readonly<Required<IConfiguration>> {}
export interface IEnvironment extends IEnvironmentConfiguration {}
export interface IFinalEnvironment extends Readonly<Required<IEnvironment>> {
  PROJECT_ENV: "development" | "production";
}
export type IConfigKey = keyof typeof backendConfigurationSchema.properties;
export type IEnvironmentKey =
  keyof typeof environmentConfigurationSchema.properties;

export const configKeys = Object.keys(
  backendConfigurationSchema.properties,
) as readonly IConfigKey[];
export const environmentKeys = Object.keys(
  environmentConfigurationSchema.properties,
) as readonly IEnvironmentKey[];
export const configName = "configuration";
export const configFileName = `${configName}.json`;
export const configFileNameDefault = `${configName}.default.json`;
export const environmentName = "environment";
export const environmentFileName = `${environmentName}.json`;
export const environmentFileNameDefault = `${environmentName}.default.json`;
