import path from "node:path";
import { cwd, env } from "node:process";
import { isNativeError } from "node:util/types";
import { validateEnvironmentConfiguration } from "#codegen/schema/lib/configs/environment";
import { defineProperty } from "#lib/std";
import { readJSONFile } from "#lib/fs";
import { ConfigurationError } from "./errors.js";
import {
  type IEnvironment,
  environmentFileName,
  environmentFileNameDefault,
  type IFinalEnvironment,
} from "./types.js";

const allowedValues = ["development", "production"] as const;
type IProjectEnv = (typeof allowedValues)[number];

let environment: IFinalEnvironment | undefined = undefined;

export async function getEnvironment() {
  return environment ?? resolveEnvironment();
}

export async function resolveEnvironment(): Promise<IFinalEnvironment> {
  if (environment) {
    return environment;
  }

  const projectVar = env.PROJECT_ENV;

  if (!projectVar) {
    throw new ConfigurationError(
      `Environment variable "PROJECT_ENV" is not set.`,
    );
  }

  if (!allowedValues.includes(projectVar)) {
    throw new ConfigurationError(
      `The value "${projectVar}" of "PROJECT_ENV" environment variable is illegal.`,
    );
  }

  const configsFolderPath = path.join(cwd(), "configs");

  try {
    const defaultEnvPath = path.join(
      configsFolderPath,
      projectVar,
      environmentFileNameDefault,
    );
    const userEnvPath = path.join(
      configsFolderPath,
      projectVar,
      environmentFileName,
    );

    const defaultEnv =
      await readJSONFile<Partial<IEnvironment>>(defaultEnvPath);
    const userEnv = await readJSONFile<Partial<IEnvironment>>(userEnvPath);
    const mergedEnv = mergeEnvs(defaultEnv, userEnv);

    validateEnvironmentConfiguration(mergedEnv);

    const finalEnv = finalizeEnv(mergedEnv, projectVar);

    environment = finalEnv;

    return finalEnv;
  } catch (error) {
    if (!isNativeError(error)) {
      throw error;
    }

    throw new ConfigurationError(
      `Failed to resolve configuration file at ${configsFolderPath}`,
      { cause: error },
    );
  }
}

function mergeEnvs(
  defaultEnv: Partial<IEnvironment>,
  userEnv: Partial<IEnvironment>,
): IEnvironment {
  const mergedConfig = { ...defaultEnv, ...userEnv } as IEnvironment;

  return mergedConfig;
}

function finalizeEnv(
  mergedEnv: IEnvironment,
  projectEnv: IProjectEnv,
): IFinalEnvironment {
  const finalEnv = Object.entries(mergedEnv).reduce<IFinalEnvironment>(
    (finalEnv, [key, value]) => {
      // overwrite the value of env var
      defineProperty(env, key, {
        value,
        enumerable: true,
        writable: true,
        configurable: true,
      });

      // @ts-expect-error keyname
      defineProperty(finalEnv, key, {
        value,
        enumerable: true,
        writable: false,
      });

      return finalEnv;
    },
    {} as IFinalEnvironment,
  );

  defineProperty(finalEnv, "PROJECT_ENV", {
    value: projectEnv,
    enumerable: true,
    writable: false,
  });

  return finalEnv;
}
