import { ProjectError } from "#lib/errors";

export class ConfigurationError extends ProjectError {}
