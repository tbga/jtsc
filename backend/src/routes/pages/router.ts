import { Router } from "express";
import { setupPages } from "#middleware";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";

export const pages = Router();

if (!IS_DEVELOPMENT) {
  await setupPages(pages);
}
