import { Router } from "express";
import { startTransaction } from "#database";
import {
  countProfileArtists,
  getProfileArtistPreviews,
  getProfileArtistItems,
} from "#entities/profile";
import { Pagination } from "#lib/pagination";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";

export const artists = Router({ mergeParams: true });

artists.get<{ profile_id: string }>(
  "/artists",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const count = await countProfileArtists(profile_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

artists.get<{ profile_id: string }>(
  "/artists/previews/:page",
  createPageNumberValidator(),
);

artists.get<{ profile_id: string; page: string }>(
  "/artists/previews/:page",
  apiRoute(async (req, res) => {
    const { profile_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countProfileArtists(profile_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const artists = await getProfileArtistPreviews(
        { profile_id, pagination },
        ctx,
      );

      return {
        pagination,
        artists,
      };
    });

    return result;
  }),
);

artists.get<{ profile_id: string }>(
  "/artists/items/:page",
  createPageNumberValidator(),
);

artists.get<{ profile_id: string; page: string }>(
  "/artists/items/:page",
  apiRoute(async (req, res) => {
    const { profile_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countProfileArtists(profile_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const artists = await getProfileArtistItems(
        { profile_id, pagination },
        ctx,
      );

      return {
        pagination,
        artists,
      };
    });

    return result;
  }),
);
