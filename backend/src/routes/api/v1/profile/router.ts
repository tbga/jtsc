import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  countProfileNames,
  countProfileReleases,
  getProfile,
  getProfileNames,
  getProfileReleases,
} from "#entities/profile";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { createEntityIDValidator } from "#middleware/entities";
import { apiRoute } from "#middleware/api";

import { artists } from "./artists.js";

export const profile = Router();

profile.get("/profile/:profile_id", createEntityIDValidator(["profile_id"]));

// get profile data
profile.get<{ profile_id: string }>(
  "/profile/:profile_id",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const profile = await getProfile(profile_id);

      return profile;
    });

    return result;
  }),
);

// get profile release pagination
profile.get<{ profile_id: string }>(
  "/profile/:profile_id/releases",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countProfileReleases(profile_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

// validate page number
profile.use("/profile/:profile_id/releases/:page", createPageNumberValidator());

// get profile releases
profile.get<{ profile_id: string; page: string }>(
  "/profile/:profile_id/releases/:page",
  apiRoute(async (req, res) => {
    const { profile_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const profileData = await getProfileReleases({ profile_id, page }, ctx);

      return profileData;
    });

    return result;
  }),
);

// get profile names pagination
profile.get<{ profile_id: string }>(
  "/profile/:profile_id/names",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countProfileNames(profile_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

// get profile names
profile.get("/profile/:profile_id/names/:page", createPageNumberValidator());
profile.get<{ profile_id: string; page: string }>(
  "/profile/:profile_id/names/:page",
  apiRoute(async (req, res) => {
    const { profile_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countProfileNames(profile_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const names = await getProfileNames({ profile_id, pagination }, ctx);

      return {
        pagination,
        names,
      };
    });

    return result;
  }),
);

profile.use("/profile/:profile_id", artists);
