import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  selectProfileCount,
  selectProfileIDs,
  selectProfilePreviews,
} from "#database/queries/profiles";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { createEntityIDValidator } from "#middleware/entities";
import { apiRoute } from "#middleware/api";
import {
  countReleasePosts,
  getRelease,
  getReleasePosts,
} from "#entities/release";

export const release = Router();

// validate release ID
release.use("/release/:release_id", createEntityIDValidator(["release_id"]));

// get a release
release.get<{ release_id: string }>(
  "/release/:release_id",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const release = await getRelease(release_id);

    return release;
  }),
);

// release profiles pagination
release.get<{ release_id: string }>(
  "/release/:release_id/profiles",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await selectProfileCount(
        { release_ids: [release_id] },
        ctx,
      );
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);
// validate page number
release.get("/release/:release_id/profiles/:page", createPageNumberValidator());
// release profiles
release.get<{ release_id: string; page: string }>(
  "/release/:release_id/profiles/:page",
  apiRoute(async (req, res) => {
    const { release_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await selectProfileCount(
        { release_ids: [release_id] },
        ctx,
      );
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const profileIDs = await selectProfileIDs(
        { release_ids: [release_id], pagination },
        ctx,
      );
      const profiles = await selectProfilePreviews(profileIDs, ctx);

      return {
        pagination,
        profiles,
      };
    });

    return result;
  }),
);

release.get<{ release_id: string }>(
  "/release/:release_id/posts",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countReleasePosts(release_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

release.get("/release/:release_id/posts/:page", createPageNumberValidator());

release.get<{ release_id: string; page: string }>(
  "/release/:release_id/posts/:page",
  apiRoute(async (req, res) => {
    const { release_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countReleasePosts(release_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const posts = await getReleasePosts({ pagination, release_id }, ctx);

      return {
        pagination,
        posts,
      };
    });

    return result;
  }),
);
