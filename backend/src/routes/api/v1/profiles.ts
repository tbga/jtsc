import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  selectProfileCount,
  selectProfilePreviews,
} from "#database/queries/profiles";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import { EMPTY_OBJECT } from "#types";
import { getProfiles } from "#entities/profile";

export const profiles = Router({ mergeParams: true });

// get profiles pagination
profiles.get(
  "/profiles",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const profileCount = await selectProfileCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(profileCount);

      return pagination;
    });

    return result;
  }),
);

profiles.get("/profiles/:page", createPageNumberValidator());

// get profiles
profiles.get<{ page: string }>(
  "/profiles/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const profileCount = await selectProfileCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(profileCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const profiles = await getProfiles(pagination, ctx);

      return { pagination, profiles };
    });

    return result;
  }),
);
