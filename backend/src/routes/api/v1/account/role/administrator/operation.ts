import { Router } from "express";
import { startTransaction } from "#database";
import {
  cancelOperation,
  getOperation,
  retryOperation,
} from "#entities/operation";
import type { IAccountLocals } from "#middleware";
import { createEntityIDValidator } from "#middleware/entities";
import { apiRoute } from "#middleware/api";

export const operation = Router({ mergeParams: true });

operation.use("/:operation_id", createEntityIDValidator(["operation_id"]));

// get operation details
operation.get<
  { operation_id: string },
  unknown,
  unknown,
  URLSearchParams,
  IAccountLocals
>(
  "/:operation_id",
  apiRoute(async (req, res) => {
    const { operation_id } = req.params;
    const operation = await startTransaction(async (ctx) =>
      getOperation(operation_id, ctx),
    );

    return operation;
  }),
);

operation.post<
  { operation_id: string },
  unknown,
  unknown,
  URLSearchParams,
  IAccountLocals
>(
  "/:operation_id/cancel",
  apiRoute(async (req, res) => {
    const { operation_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const { id } = await cancelOperation(operation_id, ctx);

      return id;
    });

    return result;
  }),
);

operation.post<
  { operation_id: string },
  unknown,
  unknown,
  URLSearchParams,
  IAccountLocals
>(
  "/:operation_id/retry",
  apiRoute(async (req, res) => {
    const { account } = res.locals;
    const { operation_id } = req.params;
    const newOperation = await startTransaction(async (ctx) => {
      return retryOperation({ account_id: account.id, operation_id }, ctx);
    });

    return newOperation;
  }),
);
