import { Router } from "express";
import {
  type IAPIRequestPublicImportCreate,
  validateAPIRequestPublicImportCreate,
} from "#codegen/schema/lib/api/account/role/administrator/public-import/create/request";
import {
  type IAPIRequestPublicImportSiteUpdate,
  validateAPIRequestPublicImportSiteUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/public-import/site/update/request";
import {
  type IPublicImportSiteCategoryAPI,
  publicImportSiteCategoryAPISchema,
} from "#codegen/schema/lib/entities/public-import/site/category-api";
import { Pagination } from "#lib/pagination";
import { HTTP_STATUS } from "#lib/http";
import { InputValidationError } from "#lib/api";
import { startTransaction } from "#database";
import {
  selectPublicImportEntities,
  selectPublicImportStats,
  updatePublicImports,
} from "#database/queries/public-imports";
import { createOperation } from "#entities/operation";
import {
  type IPublicImportSiteCategory,
  getPublicImport,
  getPublicImportSite,
  getPublicImportSites,
  approvePublicImportSite,
  rejectPublicImportSite,
} from "#entities/public-import";
import {
  createPageNumberValidator,
  validateClientPageNumber,
  type IAccountLocals,
} from "#middleware";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import { apiRoute } from "#middleware/api";
import { ClientError } from "#lib/errors";
import { entityTitle } from "#lib/entities";

export const publicImportSiteCategories =
  publicImportSiteCategoryAPISchema.anyOf.map((schema) => schema.const);

export const publicImport = Router({ mergeParams: true });

publicImport.put(
  "/",
  createBodyValidator(validateAPIRequestPublicImportCreate),
);

publicImport.put<
  unknown,
  unknown,
  IAPIRequestPublicImportCreate,
  URLSearchParams,
  IAccountLocals
>(
  "/",
  apiRoute(async (req, res) => {
    const { account } = res.locals;
    const fileURL = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const operation = await createOperation(
        {
          account_id: account.id,
          type: "public_imports_create",
          input_data: [fileURL],
        },
        ctx,
      );

      return operation;
    });

    return result;
  }),
);

publicImport.use(
  "/:public_import_id",
  createEntityIDValidator(["public_import_id"]),
);

publicImport.get<{ public_import_id: string }>(
  "/:public_import_id",
  apiRoute(async (req, res) => {
    const { public_import_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const publicImport = await getPublicImport({ public_import_id }, ctx);

      return publicImport;
    });

    return result;
  }),
);

publicImport.get<{ public_import_id: string }>(
  "/:public_import_id/stats",
  apiRoute(async (req, res) => {
    const { public_import_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const [stats] = await selectPublicImportStats([public_import_id], ctx);

      return stats;
    });

    return result;
  }),
);

publicImport.get(
  "/:public_import_id/site/:public_import_site_id",
  createEntityIDValidator(["public_import_site_id"]),
);

publicImport.get<{ public_import_id: string; public_import_site_id: string }>(
  "/:public_import_id/site/:public_import_site_id",
  apiRoute(async (req, res) => {
    const { public_import_id, public_import_site_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const site = await getPublicImportSite(
        { public_import_id, public_import_site_id },
        ctx,
      );

      return site;
    });

    return result;
  }),
);

publicImport.patch(
  "/:public_import_id/site/:public_import_site_id",
  createBodyValidator(validateAPIRequestPublicImportSiteUpdate),
);
publicImport.patch<
  { public_import_id: string; public_import_site_id: string },
  unknown,
  IAPIRequestPublicImportSiteUpdate
>(
  "/:public_import_id/site/:public_import_site_id",
  apiRoute(async (req, res) => {
    const { public_import_site_id } = req.params;
    const { is_approved } = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const updatedSite = is_approved
        ? await approvePublicImportSite({ public_import_site_id }, ctx)
        : await rejectPublicImportSite({ public_import_site_id }, ctx);

      return updatedSite;
    });

    return result;
  }),
);

publicImport.get<{ public_import_id: string }>(
  "/:public_import_id/sites",
  async (req, res, next) => {
    try {
      return res.redirect(HTTP_STATUS.MOVED_PERMANENTLY, `${req.baseUrl}/all`);
    } catch (error) {
      next(error);
    }
  },
);

publicImport.use<{ public_import_id: string; category: string }>(
  "/:public_import_id/sites/:category",
  async (req, res, next) => {
    const { category } = req.params;

    try {
      if (
        !publicImportSiteCategories.includes(
          // @ts-expect-error tuple array
          category,
        )
      ) {
        throw new InputValidationError(
          `Unknown public import sites category "${category}".`,
        );
      }
      next();
    } catch (error) {
      next(error);
    }
  },
);

publicImport.get<{
  public_import_id: string;
  category: IPublicImportSiteCategoryAPI;
}>(
  "/:public_import_id/sites/:category",
  apiRoute(async (req, res) => {
    const { public_import_id, category } = req.params;

    const result = await startTransaction(async (ctx) => {
      const [{ sites }] = await selectPublicImportStats(
        [public_import_id],
        ctx,
      );
      const count = sites[category];
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

publicImport.get(
  "/:public_import_id/sites/:category/:page",
  createPageNumberValidator(),
);

publicImport.get<{
  public_import_id: string;
  category: IPublicImportSiteCategory;
  page: string;
}>(
  "/:public_import_id/sites/:category/:page",
  apiRoute(async (req, res) => {
    const { public_import_id, category, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const [{ sites: siteStats }] = await selectPublicImportStats(
        [public_import_id],
        ctx,
      );
      const count = siteStats[category];
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, count);

      const sites = await getPublicImportSites(
        {
          public_import_id,
          pagination,
          category,
        },
        ctx,
      );

      return { pagination, sites };
    });

    return result;
  }),
);

publicImport.post<
  {
    public_import_id: string;
  },
  unknown,
  unknown,
  URLSearchParams,
  IAccountLocals
>(
  "/:public_import_id/consume",
  apiRoute(async (req, res) => {
    const { public_import_id } = req.params;
    const { account } = res.locals;

    const result = await startTransaction(async (ctx) => {
      const [publicImport] = await selectPublicImportEntities(
        [public_import_id],
        ctx,
      );

      if (!publicImport.is_consumable) {
        throw new ClientError(
          `Public import ${entityTitle(
            publicImport.id,
            publicImport.title,
          )} is not consumable.`,
          { status: HTTP_STATUS.CONFLICT },
        );
      }

      await updatePublicImports(
        [{ id: public_import_id, status: "in-progress" }],
        ctx,
      );

      const operation = await createOperation(
        {
          account_id: account.id,
          type: "public_imports_consume",
          input_data: [public_import_id],
        },
        ctx,
      );

      return operation;
    });

    return result;
  }),
);
