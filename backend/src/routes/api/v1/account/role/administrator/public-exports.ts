import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import { countPublicExports, getPublicExports } from "#entities/public-export";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import { EMPTY_OBJECT } from "#types";

export const publicExports = Router({ mergeParams: true });

publicExports.get(
  "/",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const count = await countPublicExports(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

publicExports.get("/:page", createPageNumberValidator());
publicExports.get<{ page: string }>(
  "/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const count = await countPublicExports(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, count);

      const public_exports = await getPublicExports({ pagination }, ctx);

      return {
        pagination,
        public_exports,
      };
    });

    return result;
  }),
);
