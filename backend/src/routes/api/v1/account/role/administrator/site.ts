import { Router } from "express";
import {
  type IAPIRequestSiteAdd,
  validateAPIRequestSiteAdd,
} from "#codegen/schema/lib/api/account/role/administrator/site/add/request";
import {
  type IAPIRequestSiteUpdate,
  validateAPIRequestSiteUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/site/update/request";
import { startTransaction } from "#database";
import { createSites, editSite } from "#entities/site";
import { apiRoute } from "#middleware/api";

import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";

export const site = Router({ mergeParams: true });

// create a site
site.put("/", createBodyValidator(validateAPIRequestSiteAdd));
site.put<unknown, unknown, IAPIRequestSiteAdd>(
  "/",
  apiRoute(async (req, res) => {
    const siteInit = req.body.data;

    const [newSite] = await startTransaction(async (ctx) => {
      return createSites([siteInit], ctx);
    });

    return newSite;
  }),
);

site.use("/:site_id", createEntityIDValidator(["site_id"]));

// edit site
site.patch("/:site_id", createBodyValidator(validateAPIRequestSiteUpdate));
site.patch<{ site_id: string }, unknown, IAPIRequestSiteUpdate>(
  "/:site_id",
  apiRoute(async (req, res) => {
    const { site_id } = req.params;
    const update = req.body.data;

    const updatedSite = await startTransaction(async (ctx) => {
      return editSite({ id: site_id, update }, ctx);
    });

    return updatedSite;
  }),
);
