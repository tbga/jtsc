import {
  type IAPIRequestPostArtistsAdd,
  validateAPIRequestPostArtistsAdd,
} from "#codegen/schema/lib/api/account/role/administrator/post/artists/add/request";
import {
  type IAPIRequestPostArtistsRemove,
  validateAPIRequestPostArtistsRemove,
} from "#codegen/schema/lib/api/account/role/administrator/post/artists/remove/request";
import {
  type IAPIRequestPostReleasesAdd,
  validateAPIRequestPostReleasesAdd,
} from "#codegen/schema/lib/api/account/role/administrator/post/releases/add/request";
import {
  type IAPIRequestReleasePostsRemove,
  validateAPIRequestReleasePostsRemove,
} from "#codegen/schema/lib/api/account/role/administrator/release/posts/remove/request";
import { Router } from "express";
import { startTransaction } from "#database";
import { apiRoute } from "#middleware/api";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import {
  createPostArtists,
  createPostReleases,
  removePostArtists,
  removePostReleases,
} from "#entities/post";

export const post = Router({ mergeParams: true });

post.use("/:post_id", createEntityIDValidator(["post_id"]));

post.put(
  "/:post_id/artists",
  createBodyValidator(validateAPIRequestPostArtistsAdd),
);

post.put<{ post_id: string }, unknown, IAPIRequestPostArtistsAdd>(
  "/:post_id/artists",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;
    const artist_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      return createPostArtists({ post_id, artist_ids }, ctx);
    });

    return result;
  }),
);

post.delete(
  "/:post_id/artists",
  createBodyValidator(validateAPIRequestPostArtistsRemove),
);

post.delete<{ post_id: string }, unknown, IAPIRequestPostArtistsRemove>(
  "/:post_id/artists",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;
    const artist_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      return removePostArtists({ post_id, artist_ids }, ctx);
    });

    return result;
  }),
);

post.put(
  "/:post_id/releases",
  createBodyValidator(validateAPIRequestPostReleasesAdd),
);
post.put<{ post_id: string }, unknown, IAPIRequestPostReleasesAdd>(
  "/:post_id/releases",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;
    const { ids, inits } = req.body.data;

    const result = await startTransaction(async (ctx) => {
      return createPostReleases({ post_id, ids, inits }, ctx);
    });

    return result;
  }),
);

post.delete(
  "/:post_id/releases",
  createBodyValidator(validateAPIRequestReleasePostsRemove),
);
post.delete<{ post_id: string }, unknown, IAPIRequestReleasePostsRemove>(
  "/:post_id/releases",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;
    const release_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      return removePostReleases({ post_id, release_ids }, ctx);
    });

    return result;
  }),
);
