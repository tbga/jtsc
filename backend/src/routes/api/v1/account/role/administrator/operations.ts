import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  selectOperationCount,
  selectOperationIDs,
  selectOperationPreviews,
} from "#database/queries/operations";
import { selectBasicOperationStats } from "#database/queries/stats/operations";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";

export const operations = Router({ mergeParams: true });

// get operations stats
operations.get(
  "/stats",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      return selectBasicOperationStats(undefined, ctx);
    });

    return result;
  }),
);

// get operations pagination
operations.get(
  "/all",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const operationsCount = await selectOperationCount({}, ctx);
      const pagination = new Pagination(operationsCount);

      return pagination;
    });

    return result;
  }),
);

operations.get("/all/:page", createPageNumberValidator());

// get a list of all operations
operations.get<{ page: string }>(
  "/all/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const operationsCount = await selectOperationCount({}, ctx);
      const pagination = new Pagination(operationsCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const ids = await selectOperationIDs({ pagination }, ctx);
      const operations = await selectOperationPreviews(ids, ctx);

      return {
        pagination,
        operations,
      };
    });

    return result;
  }),
);
