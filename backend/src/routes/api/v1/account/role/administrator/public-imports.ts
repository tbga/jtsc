import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import { selectPublicImportCount } from "#database/queries/public-imports";
import { apiRoute } from "#middleware/api";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { getPublicImports } from "#entities/public-import";

export const publicImports = Router({ mergeParams: true });

publicImports.get(
  "/",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const count = await selectPublicImportCount(undefined, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

publicImports.get("/:page", createPageNumberValidator());
publicImports.get<{ page: string }>(
  "/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const count = await selectPublicImportCount(undefined, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, count);

      const public_imports = await getPublicImports({ pagination }, ctx);

      return {
        pagination,
        public_imports,
      };
    });

    return result;
  }),
);
