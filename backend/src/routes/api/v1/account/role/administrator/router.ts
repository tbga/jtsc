import { isNativeError } from "node:util/types";
import { Router } from "express";
import { ClientError } from "#lib/errors";
import {
  getEntry,
  getFile,
  getFolder,
  getSystem,
} from "#lib/local-file-system";
import { NotFoundError } from "#lib/server";
import type { IAccount } from "#entities/account";
import { apiRoute } from "#middleware/api";

import { artist } from "./artist.js";
import { site } from "./site.js";
import { profile } from "./profile.js";
import { post } from "./post.js";
import { release } from "./release.js";
import { operations } from "./operations.js";
import { operation } from "./operation.js";
import { publish } from "./publish.js";
import { publicExports } from "./public-exports.js";
import { publicExport } from "./public-export.js";
import { publicImport } from "./public-import.js";
import { publicImports } from "./public-imports.js";

export const administrator = Router({ mergeParams: true });

// filter non-admin accs
administrator.use<
  unknown,
  unknown,
  unknown,
  URLSearchParams,
  { account: IAccount }
>(async (req, res, next) => {
  try {
    const account = res.locals.account;

    if (account.role !== "administrator") {
      throw new NotFoundError();
    }

    next();
  } catch (error) {
    next(error);
  }
});

// get local file system rundown
administrator.get<
  unknown,
  unknown,
  unknown,
  URLSearchParams,
  { account: IAccount }
>(
  "/file-system",
  apiRoute(async (req, res) => {
    return getSystem();
  }),
);

// validate `path` query
administrator.get<
  unknown,
  unknown,
  unknown,
  URLSearchParams,
  { account: IAccount }
>("/file-system", async (req, res, next) => {
  try {
    const path = req.query.get("path");

    if (!path) {
      throw new ClientError(`Parameter "path" must be provided`, {
        status: 422,
      });
    }

    next();
  } catch (error) {
    next(error);
  }
});

// get path entry
administrator.get<
  unknown,
  unknown,
  unknown,
  URLSearchParams,
  { account: IAccount }
>(
  "/file-system/entry",
  apiRoute(async (req, res) => {
    const path = req.query.get("path") as string;
    const entry = await getEntry(path);

    return entry;
  }),
);

// get folder info
administrator.get<unknown, unknown, unknown, URLSearchParams>(
  "/file-system/folder",
  apiRoute(async (req, res) => {
    const path = req.query.get("path") as string;

    try {
      const folder = await getFolder(path);

      return folder;
    } catch (error) {
      if (!isNativeError(error)) {
        throw error;
      }

      throw new ClientError("Failed to read the folder", { cause: error });
    }
  }),
);

// get file info
administrator.get<unknown, unknown, unknown, URLSearchParams>(
  "/file-system/file",
  apiRoute(async (req, res) => {
    const path = req.query.get("path") as string;

    const file = await getFile(path);

    return file;
  }),
);

administrator.use("/artist", artist);
administrator.use("/site", site);
administrator.use("/profile", profile);
administrator.use("/post", post);
administrator.use("/release", release);
administrator.use("/operations", operations);
administrator.use("/operation", operation);
administrator.use("/publish", publish);
administrator.use("/public-exports", publicExports);
administrator.use("/public-export", publicExport);
administrator.use("/public-imports", publicImports);
administrator.use("/public-import", publicImport);
