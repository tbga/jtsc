import { Router } from "express";
import { startTransaction } from "#database";
import { apiRoute } from "#middleware/api";
import { createEntityIDValidator } from "#middleware/entities";
import type { IAccountLocals } from "#middleware";
import { publishSite } from "#entities/site";

export const publish = Router({ mergeParams: true });

publish.post("/site/:site_id", createEntityIDValidator(["site_id"]));
publish.post(
  "/site/:site_id",
  apiRoute<
    { site_id: string },
    unknown,
    unknown,
    URLSearchParams,
    IAccountLocals
  >(async (req, res) => {
    const { site_id } = req.params;
    const { account } = res.locals;

    const result = await startTransaction(async (ctx) => {
      const site = await publishSite({ account_id: account.id, site_id }, ctx);

      return site;
    });

    return result;
  }),
);
