import { Router } from "express";
import {
  validateAPIRequestArtistUpdate,
  type IAPIRequestArtistUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/artist/update/request";
import { startTransaction } from "#database";

import { updateArtist } from "#entities/artist";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import { apiRoute } from "#middleware/api";

export const artist = Router({ mergeParams: true });

artist.use("/:artist_id", createEntityIDValidator(["artist_id"]));

artist.patch(
  "/:artist_id",
  createBodyValidator(validateAPIRequestArtistUpdate),
);

artist.patch<{ artist_id: string }, unknown, IAPIRequestArtistUpdate>(
  "/:artist_id",
  apiRoute(async (req, res) => {
    const { artist_id } = req.params;
    const update = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const dbUpdate = {
        id: artist_id,
        sex: update.sex,
      };
      const updatedArtist = await updateArtist(dbUpdate, ctx);

      return updatedArtist;
    });

    return result;
  }),
);
