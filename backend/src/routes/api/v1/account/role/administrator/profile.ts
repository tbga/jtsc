import { Router } from "express";
import {
  type IAPIRequestProfileCreate,
  validateAPIRequestProfileCreate,
} from "#codegen/schema/lib/api/account/role/administrator/profile/create/request";
import {
  type IAPIRequestProfileUpdate,
  validateAPIRequestProfileUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/profile/update/request";
import {
  type IAPIRequestProfileNamesAdd,
  validateAPIRequestProfileNamesAdd,
} from "#codegen/schema/lib/api/profile/names/add/request";
import {
  type IAPIRequestProfileNamesRemove,
  validateAPIRequestProfileNamesRemove,
} from "#codegen/schema/lib/api/profile/names/remove/request";
import {
  type IAPIRequestProfileArtistsAdd,
  validateAPIRequestProfileArtistsAdd,
} from "#codegen/schema/lib/api/account/role/administrator/profile/artists/add/request";
import {
  type IAPIRequestProfileArtistsRemove,
  validateAPIRequestProfileArtistsRemove,
} from "#codegen/schema/lib/api/account/role/administrator/profile/artists/remove/request";
import { startTransaction } from "#database";
import { createOperation } from "#entities/operation";
import {
  addProfileArtists,
  createProfileNames,
  removeProfileArtists,
  removeProfileNames,
  updateProfile,
} from "#entities/profile";
import type { IAccountLocals } from "#middleware";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import { apiRoute } from "#middleware/api";

export const profile = Router({ mergeParams: true });

profile.put("/", createBodyValidator(validateAPIRequestProfileCreate));

profile.put<
  { profile_id: string },
  unknown,
  IAPIRequestProfileCreate,
  URLSearchParams,
  IAccountLocals
>(
  "/",
  apiRoute(async (req, res) => {
    const init = req.body.data;
    const { account } = res.locals;

    const result = await startTransaction(async (ctx) => {
      const operationItem = await createOperation(
        { account_id: account.id, type: "profiles_create", input_data: [init] },
        ctx,
      );

      return operationItem;
    });

    return result;
  }),
);

profile.use("/:profile_id", createEntityIDValidator(["profile_id"]));

// edit profile
profile.patch(
  "/:profile_id",
  createBodyValidator(validateAPIRequestProfileUpdate),
);
profile.patch<{ profile_id: string }, unknown, IAPIRequestProfileUpdate>(
  "/:profile_id",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const update = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const updatedProfile = await updateProfile(
        {
          profile_id,
          update,
        },
        ctx,
      );

      return updatedProfile;
    });

    return result;
  }),
);

// create profile names
profile.put(
  "/:profile_id/names",
  createBodyValidator(validateAPIRequestProfileNamesAdd),
);
profile.put<{ profile_id: string }, unknown, IAPIRequestProfileNamesAdd>(
  "/:profile_id/names",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const inits = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const names = await createProfileNames({ profile_id, inits }, ctx);

      return names;
    });

    return result;
  }),
);

// remove profile names
profile.delete(
  "/:profile_id/names",
  createBodyValidator(validateAPIRequestProfileNamesRemove),
);
profile.delete<{ profile_id: string }, unknown, IAPIRequestProfileNamesRemove>(
  "/:profile_id/names",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const name_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      return removeProfileNames({ profile_id, name_ids }, ctx);
    });

    return result;
  }),
);

// create profile artists
profile.put(
  "/:profile_id/artists",
  createBodyValidator(validateAPIRequestProfileArtistsAdd),
);
profile.put<{ profile_id: string }, unknown, IAPIRequestProfileArtistsAdd>(
  "/:profile_id/artists",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const artist_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const addedArtists = await addProfileArtists(
        { profile_id, artist_ids },
        ctx,
      );

      return addedArtists;
    });

    return result;
  }),
);

// remove profile artists
profile.delete(
  "/:profile_id/artists",
  createBodyValidator(validateAPIRequestProfileArtistsRemove),
);
profile.delete<
  { profile_id: string },
  unknown,
  IAPIRequestProfileArtistsRemove
>(
  "/:profile_id/artists",
  apiRoute(async (req, res) => {
    const { profile_id } = req.params;
    const artist_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const removedArtists = await removeProfileArtists(
        { profile_id, artist_ids },
        ctx,
      );
      return removedArtists;
    });

    return result;
  }),
);
