import { Router } from "express";
import {
  type IAPIRequestReleaseUpdate,
  validateAPIRequestReleaseUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/release/update/request";
import {
  type IAPIRequestReleasePostsAdd,
  validateAPIRequestReleasePostsAdd,
} from "#codegen/schema/lib/api/account/role/administrator/release/posts/add/request";
import {
  type IAPIRequestReleasePostsRemove,
  validateAPIRequestReleasePostsRemove,
} from "#codegen/schema/lib/api/account/role/administrator/release/posts/remove/request";
import {
  type IAPIRequestReleaseCreate,
  validateAPIRequestReleaseCreate,
} from "#codegen/schema/lib/api/account/role/administrator/release/create/request";
import {
  type IAPIRequestReleaseProfilesAdd,
  validateAPIRequestReleaseProfilesAdd,
} from "#codegen/schema/lib/api/account/role/administrator/release/profiles/add/request";
import {
  type IAPIRequestReleaseProfilesRemove,
  validateAPIRequestReleaseProfilesRemove,
} from "#codegen/schema/lib/api/account/role/administrator/release/profiles/remove/request";
import { startTransaction } from "#database";
import type { IAccountLocals } from "#middleware";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import { apiRoute } from "#middleware/api";
import {
  createReleasePosts,
  editRelease,
  removeReleasePosts,
} from "#entities/release";
import { createOperation } from "#entities/operation";

import {
  type IReleaseProfileDBInit,
  deleteReleaseProfiles,
  insertReleaseProfiles,
} from "#database/queries/releases";
import { selectProfileItems } from "#database/queries/profiles";

export const release = Router({ mergeParams: true });

// create new release
release.put("/", createBodyValidator(validateAPIRequestReleaseCreate));
release.put<
  unknown,
  unknown,
  IAPIRequestReleaseCreate,
  URLSearchParams,
  IAccountLocals
>(
  "/",
  apiRoute(async (req, res) => {
    const { account } = res.locals;
    const init = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const operation = await createOperation(
        { account_id: account.id, type: "releases_create", input_data: [init] },
        ctx,
      );

      return operation;
    });

    return result;
  }),
);

// validate release ID
release.use("/:release_id", createEntityIDValidator(["release_id"]));

// edit release
release.patch(
  "/:release_id",
  createBodyValidator(validateAPIRequestReleaseUpdate),
);
release.patch<{ release_id: string }, unknown, IAPIRequestReleaseUpdate>(
  "/:release_id",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const update = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const release = await editRelease({ release_id, update }, ctx);

      return release;
    });

    return result;
  }),
);

release.put(
  "/:release_id/posts",
  createBodyValidator(validateAPIRequestReleasePostsAdd),
);
release.put<{ release_id: string }, unknown, IAPIRequestReleasePostsAdd>(
  "/:release_id/posts",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const post_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const createdPosts = await createReleasePosts(
        { release_id, post_ids },
        ctx,
      );

      return createdPosts;
    });

    return result;
  }),
);

release.delete(
  "/:release_id/posts",
  createBodyValidator(validateAPIRequestReleasePostsRemove),
);
release.delete<{ release_id: string }, unknown, IAPIRequestReleasePostsRemove>(
  "/:release_id/posts",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const post_ids = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const removed_posts = await removeReleasePosts(
        { release_id, post_ids },
        ctx,
      );
      return removed_posts;
    });

    return result;
  }),
);

// add profiles
release.put(
  "/:release_id/profiles",
  createBodyValidator(validateAPIRequestReleaseProfilesAdd),
);
release.put<{ release_id: string }, unknown, IAPIRequestReleaseProfilesAdd>(
  "/:release_id/profiles",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const inits = req.body.data;
    const dbInits = inits.map<IReleaseProfileDBInit>((profile_id) => {
      return {
        profile_id,
        release_id,
      };
    });

    const result = await startTransaction(async (ctx) => {
      const { profiles: profileIDs } = await insertReleaseProfiles(
        dbInits,
        ctx,
      );
      const profiles = await selectProfileItems(profileIDs, ctx);

      return profiles;
    });

    return result;
  }),
);

// delete profiles
release.delete(
  "/:release_id/profiles",
  createBodyValidator(validateAPIRequestReleaseProfilesRemove),
);
release.delete<
  { release_id: string },
  unknown,
  IAPIRequestReleaseProfilesRemove
>(
  "/:release_id/profiles",
  apiRoute(async (req, res) => {
    const { release_id } = req.params;
    const inits = req.body.data;
    const dbInits = inits.map<IReleaseProfileDBInit>((profile_id) => {
      return {
        profile_id,
        release_id,
      };
    });

    const result = await startTransaction(async (ctx) => {
      const { profiles: profileIDs } = await deleteReleaseProfiles(
        dbInits,
        ctx,
      );
      const profiles = await selectProfileItems(profileIDs, ctx);

      return profiles;
    });

    return result;
  }),
);
