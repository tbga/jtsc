import { Router } from "express";
import { ClientError } from "#lib/errors";
import { Pagination } from "#lib/pagination";
import { HTTP_STATUS } from "#lib/http";
import {
  type IAPIRequestPublicExportCreate,
  validateAPIRequestPublicExportCreate,
} from "#codegen/schema/lib/api/account/role/administrator/public-export/create/request";
import {
  type IAPIRequestPublicExportUpdate,
  validateAPIRequestPublicExportUpdate,
} from "#codegen/schema/lib/api/account/role/administrator/public-export/update/request";
import {
  type IAPIRequestPublicExportSitesAdd,
  validateAPIRequestPublicExportSitesAdd,
} from "#codegen/schema/lib/api/account/role/administrator/public-export/sites/add/request";
import {
  type IAPIRequestPublicExportSitesRemove,
  validateAPIRequestPublicExportSitesRemove,
} from "#codegen/schema/lib/api/account/role/administrator/public-export/sites/remove/request";
import { startTransaction } from "#database";
import {
  addPublicExportSites,
  countPublicExportSites,
  createPublicExport,
  editPublicExport,
  getPublicExport,
  getPublicExportSites,
  removePublicExportSites,
} from "#entities/public-export";
import {
  createPageNumberValidator,
  validateClientPageNumber,
  type IAccountLocals,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import {
  createBodyValidator,
  createEntityIDValidator,
} from "#middleware/entities";
import { updatePublicExports } from "#database/queries/public-exports";
import { createOperation } from "#entities/operation";

export const publicExport = Router({ mergeParams: true });

publicExport.put(
  "/",
  createBodyValidator(validateAPIRequestPublicExportCreate),
);
publicExport.put<
  unknown,
  unknown,
  IAPIRequestPublicExportCreate,
  URLSearchParams,
  IAccountLocals
>(
  "/",
  apiRoute(async (req, res) => {
    const { account } = res.locals;
    const { title, sites } = req.body.data;
    const result = await startTransaction(async (ctx) => {
      const newPublicExport = await createPublicExport(
        { account_id: account.id, title, sites },
        ctx,
      );

      return newPublicExport;
    });

    return result;
  }),
);

publicExport.use(
  "/:public_export_id",
  createEntityIDValidator(["public_export_id"]),
);

publicExport.patch(
  "/:public_export_id",
  createBodyValidator(validateAPIRequestPublicExportUpdate),
);

publicExport.patch<
  { public_export_id: string },
  unknown,
  IAPIRequestPublicExportUpdate,
  URLSearchParams,
  IAccountLocals
>(
  "/:public_export_id",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;
    const update = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const publicExport = await getPublicExport({ public_export_id }, ctx);

      if (publicExport.status !== "in-progress") {
        throw new ClientError(
          `Public export "${publicExport.title}" (${publicExport.id}) cannot be edited anymore.`,
          { status: HTTP_STATUS.BAD_REQUEST },
        );
      }

      const updatedPublicExport = await editPublicExport(
        { public_export_id, update },
        ctx,
      );

      return updatedPublicExport;
    });

    return result;
  }),
);

publicExport.get<{ public_export_id: string }>(
  "/:public_export_id",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const publicExport = await getPublicExport({ public_export_id }, ctx);

      return publicExport;
    });

    return result;
  }),
);

publicExport.post<{ public_export_id: string }>(
  "/:public_export_id/finalize",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const publicExport = await getPublicExport({ public_export_id }, ctx);

      if (publicExport.status !== "in-progress") {
        throw new ClientError(
          `Public export "${publicExport.title}" (${publicExport.id}) cannot be finalized.`,
          { status: HTTP_STATUS.BAD_REQUEST },
        );
      }

      const editedPublicExport = await editPublicExport({
        public_export_id,
        update: { status: "finalized" },
      });

      return editedPublicExport;
    });

    return result;
  }),
);

publicExport.post<
  { public_export_id: string },
  unknown,
  unknown,
  URLSearchParams,
  IAccountLocals
>(
  "/:public_export_id/finish",
  apiRoute(async (req, res) => {
    const { account } = res.locals;
    const { public_export_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const publicExport = await getPublicExport({ public_export_id }, ctx);

      if (publicExport.status !== "finalized") {
        throw new ClientError(
          `Public export "${publicExport.title}" (${publicExport.id}) cannot be finished.`,
          { status: HTTP_STATUS.BAD_REQUEST },
        );
      }

      const operation = await createOperation(
        {
          account_id: account.id,
          type: "public_exports_finish",
          input_data: [publicExport.id],
        },
        ctx,
      );

      return operation;
    });

    return result;
  }),
);

publicExport.put(
  "/:public_export_id/sites",
  createBodyValidator(validateAPIRequestPublicExportSitesAdd),
);
publicExport.put<
  { public_export_id: string },
  unknown,
  IAPIRequestPublicExportSitesAdd
>(
  "/:public_export_id/sites",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;
    const site_ids = req.body.data;
    const result = await startTransaction(async (ctx) => {
      const sites = await addPublicExportSites(
        { public_export_id, site_ids },
        ctx,
      );

      return sites;
    });

    return result;
  }),
);

publicExport.delete(
  "/:public_export_id/sites",
  createBodyValidator(validateAPIRequestPublicExportSitesRemove),
);
publicExport.delete<
  { public_export_id: string },
  unknown,
  IAPIRequestPublicExportSitesRemove
>(
  "/:public_export_id/sites",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;
    const site_ids = req.body.data;
    const result = await startTransaction(async (ctx) => {
      const sites = await removePublicExportSites(
        { public_export_id, site_ids },
        ctx,
      );

      return sites;
    });

    return result;
  }),
);

publicExport.get<{ public_export_id: string }>(
  "/:public_export_id/sites",
  apiRoute(async (req, res) => {
    const { public_export_id } = req.params;
    const result = await startTransaction(async (ctx) => {
      const count = await countPublicExportSites(public_export_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

publicExport.get<{ public_export_id: string }>(
  "/:public_export_id/sites/:page",
  createPageNumberValidator(),
);
publicExport.get<{ public_export_id: string; page: string }>(
  "/:public_export_id/sites/:page",
  apiRoute(async (req, res) => {
    const { public_export_id, page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const count = await countPublicExportSites(public_export_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const sites = await getPublicExportSites(
        { pagination, public_export_id },
        ctx,
      );

      return { pagination, sites };
    });

    return result;
  }),
);
