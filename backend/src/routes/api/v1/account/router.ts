import { Router } from "express";
import { getSession } from "#lib/session";
import { ClientError } from "#lib/errors";
import { HTTP_HEADER, HTTP_STATUS } from "#lib/http";
import { selectAccountEntities } from "#database/queries/accounts";
import { toPublicAccount } from "#entities/account";
import { apiRoute } from "#middleware/api";
import type { IAccountLocals } from "#middleware";

import { search } from "#routes/api/v1/account/search";
import { administrator } from "#routes/api/v1/account/role/administrator";

export const account = Router();

account.use<unknown, unknown, URLSearchParams, IAccountLocals>(
  async (req, res, next) => {
    try {
      const session = await getSession(req, res);
      const { account_id } = session;

      if (!account_id) {
        throw new ClientError("Not Authorized", {
          status: HTTP_STATUS.UNAUTHORIZED,
        });
      }

      try {
        const [account] = await selectAccountEntities([account_id]);

        res.locals.account = account;
      } catch (error) {
        // @TODO more thorough logging
        throw new ClientError("Not Authorized", {
          status: HTTP_STATUS.UNAUTHORIZED,
          cause: error,
        });
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);

account.get<unknown, unknown, unknown, URLSearchParams, IAccountLocals>(
  "/",
  apiRoute(async (req, res) => {
    const account = toPublicAccount(res.locals.account);

    return account;
  }),
);

account.get<unknown, unknown, unknown, URLSearchParams, IAccountLocals>(
  "/user-agent",
  apiRoute(async (req, res) => {
    const userAgent = req.header(HTTP_HEADER.USER_AGENT);

    return userAgent;
  }),
);

account.use("/search", search);
account.use("/role/administrator", administrator);
