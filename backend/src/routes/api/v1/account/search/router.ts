import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { InputValidationError } from "#lib/api";
import { startTransaction } from "#database";
import { selectNameCount } from "#database/queries/names";
import { selectFileCount } from "#database/queries/files";
import { searchNames } from "#entities/name";
import {
  countProfiles,
  searchProfiles,
  searchProfilesCount,
} from "#entities/profile";
import { countArtistSearch, searchArtists } from "#entities/artist";
import { searchFileItems, searchFiles } from "#entities/file";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import { validateInputEntityID } from "#middleware/entities";
import { countSiteSearch, searchSiteItems, searchSites } from "#entities/site";
import {
  searchReleaseCount,
  searchReleaseItems,
  searchReleasePreviews,
} from "#entities/release";
import {
  searchPostCount,
  searchPostItems,
  searchPostPreviews,
} from "#entities/post";

export const search = Router();

// validate query param
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/",
  async (req, res, next) => {
    try {
      const query = req.query.get("query");

      if (!query) {
        throw new InputValidationError(
          "The search query must have at least 1 character.",
        );
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);

// get pagination for the query
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/names/items",
  apiRoute(async (req, res) => {
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await selectNameCount({ search_query: query }, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

// validate page number
search.get("/names/items/:page", createPageNumberValidator());

// get name items
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/names/items/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const total_count = await selectNameCount({ search_query: query }, ctx);
      const pagination = new Pagination(total_count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const names = await searchNames({ pagination, query }, ctx);

      return {
        pagination,
        names,
      };
    });

    return result;
  }),
);

// validate other queries
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/profiles",
  async (req, res, next) => {
    try {
      const site_id = req.query.get("site_id");

      if (site_id) {
        validateInputEntityID(site_id);
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/profiles",
  apiRoute(async (req, res) => {
    const query = req.query.get("query") as string;
    const site_id = req.query.get("site_id") ?? undefined;

    const total_count = await searchProfilesCount({
      search_query: query,
      site_id,
    });
    const pagination = new Pagination(total_count);

    return pagination;
  }),
);

search.get("/search/profiles/:page", createPageNumberValidator());
// validate other queries
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/profiles/:page",
  async (req, res, next) => {
    try {
      const site_id = req.query.get("site_id");

      if (site_id) {
        validateInputEntityID(site_id);
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/profiles/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;
    const site_id = req.query.get("site_id") ?? undefined;

    const result = await startTransaction(async (ctx) => {
      const total_count = await searchProfilesCount(
        { search_query: query, site_id },
        ctx,
      );
      const pagination = new Pagination(total_count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const profiles = await searchProfiles(
        { pagination, search_query: query, site_id },
        ctx,
      );

      return {
        pagination,
        profiles,
      };
    });

    return result;
  }),
);

// get artist search pagination
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/artists",
  apiRoute(async (req, res) => {
    const query = req.query.get("query") as string;

    const total_count = await countArtistSearch({ search_query: query });
    const pagination = new Pagination(total_count);

    return pagination;
  }),
);

// validate page number
search.get("/artists/:page", createPageNumberValidator());

// get artist search
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/artists/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const total_count = await countArtistSearch({ search_query: query }, ctx);
      const pagination = new Pagination(total_count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const artists = await searchArtists(
        { pagination, search_query: query },
        ctx,
      );

      return {
        pagination,
        artists,
      };
    });

    return result;
  }),
);

// get file search pagination
search.get<unknown, unknown, unknown, URLSearchParams>(
  "/files",
  apiRoute(async (req, res) => {
    const query = req.query.get("query") as string;
    const count = await selectFileCount({ search_query: query });
    const pagination = new Pagination(count);

    return pagination;
  }),
);

// validate page number
search.get("/files/previews/:page", createPageNumberValidator());

search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/files/previews/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;
    const result = await startTransaction(async (ctx) => {
      const count = await selectFileCount({ search_query: query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page);

      const files = await searchFiles({ pagination, search_query: query }, ctx);

      return {
        pagination,
        files,
      };
    });

    return result;
  }),
);

search.get("/files/items/:page", createPageNumberValidator());

search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/files/items/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await selectFileCount({ search_query: query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page);

      const files = await searchFileItems(
        { pagination, search_query: query },
        ctx,
      );

      return {
        pagination,
        files,
      };
    });

    return result;
  }),
);

search.get<unknown, unknown, unknown, URLSearchParams>(
  "/sites",
  apiRoute(async (req, res) => {
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await countSiteSearch({ search_query: query }, ctx);
      const pagination = new Pagination(count);
      return pagination;
    });

    return result;
  }),
);

search.get("/sites/previews/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/sites/previews/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await countSiteSearch({ search_query: query }, ctx);
      const pagination = new Pagination(count, page);
      validateClientPageNumber(page, pagination.total_pages);

      const sites = await searchSites({ pagination, search_query: query }, ctx);

      return {
        pagination,
        sites,
      };
    });

    return result;
  }),
);

search.get("/sites/items/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/sites/items/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await countSiteSearch({ search_query: query }, ctx);
      const pagination = new Pagination(count, page);
      validateClientPageNumber(page, pagination.total_pages);

      const sites = await searchSiteItems(
        { pagination, search_query: query },
        ctx,
      );

      return {
        pagination,
        sites,
      };
    });

    return result;
  }),
);

search.get<unknown, unknown, unknown, URLSearchParams>(
  "/releases",
  apiRoute(async (req, res) => {
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchReleaseCount({ search_query }, ctx);
      const pagination = new Pagination(count);
      return pagination;
    });

    return result;
  }),
);

search.get("/releases/previews/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/releases/previews/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchReleaseCount({ search_query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const releases = await searchReleasePreviews(
        { pagination, search_query },
        ctx,
      );

      return {
        pagination,
        releases,
      };
    });

    return result;
  }),
);

search.get("/releases/items/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/releases/items/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchReleaseCount({ search_query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const releases = await searchReleaseItems(
        { pagination, search_query },
        ctx,
      );

      return {
        pagination,
        releases,
      };
    });

    return result;
  }),
);

search.get<unknown, unknown, unknown, URLSearchParams>(
  "/posts",
  apiRoute(async (req, res) => {
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchPostCount({ search_query }, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

search.get("/posts/previews/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/posts/previews/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchPostCount({ search_query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const posts = await searchPostPreviews({ pagination, search_query }, ctx);

      return {
        pagination,
        posts,
      };
    });

    return result;
  }),
);

search.get("/posts/items/:page", createPageNumberValidator());
search.get<{ page: string }, unknown, unknown, URLSearchParams>(
  "/posts/items/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const search_query = req.query.get("query") as string;

    const result = await startTransaction(async (ctx) => {
      const count = await searchPostCount({ search_query }, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const posts = await searchPostItems({ pagination, search_query }, ctx);

      return {
        pagination,
        posts,
      };
    });

    return result;
  }),
);
