import { Router } from "express";
import { APIFailure, APISuccess } from "#lib/api";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  selectArtistCount,
  selectArtistPreviews,
} from "#database/queries/artists";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { getArtists } from "#entities/artist";
import { EMPTY_OBJECT } from "#types";

export const artists = Router();

// get artist count
artists.get("/artists", async (req, res, next) => {
  try {
    const artistCount = await selectArtistCount(EMPTY_OBJECT);
    const pagination = new Pagination(artistCount);

    return res.status(200).json(new APISuccess(pagination));
  } catch (error) {
    next(error);
  }
});

artists.get("/artists/:page", createPageNumberValidator());

// get artist list
artists.get("/artists/:page", async (req, res, next) => {
  try {
    const { page } = req.params;

    const { pagination, artists } = await startTransaction(async (ctx) => {
      const artistCount = await selectArtistCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(artistCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const artists = await getArtists(pagination, ctx);

      return {
        pagination,
        artists,
      };
    });

    return res.status(200).json(new APISuccess({ pagination, artists }));
  } catch (error) {
    next(error);
  }
});
