import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import { selectPostCount } from "#database/queries/posts";
import { getPosts } from "#entities/post";
import { apiRoute } from "#middleware/api";
import { validateClientPageNumber } from "#middleware";
import { EMPTY_OBJECT } from "#types";

export const posts = Router();

// get posts pagination
posts.get(
  "/posts",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const postCount = await selectPostCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(postCount);

      return pagination;
    });

    return result;
  }),
);

// get all posts
posts.get<{ page: string }>(
  "/posts/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const postCount = await selectPostCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(postCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const posts = await getPosts(pagination, ctx);

      return { pagination, posts };
    });

    return result;
  }),
);
