import { Router } from "express";
import { startTransaction } from "#database";
import { selectBasicStats } from "#database/queries/stats";
import { apiRoute } from "#middleware/api";

export const stats = Router({ mergeParams: true });

stats.get(
  "/",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      return selectBasicStats(undefined, ctx);
    });

    return result;
  }),
);
