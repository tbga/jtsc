import { Router } from "express";
import { ADMIN_INVITE, IS_INVITE_ONLY } from "#environment/variables.js";
import {
  type IAPIRequestAccountLogin,
  validateAPIRequestAccountLogin,
} from "#codegen/schema/lib/api/authentication/login/request";
import {
  type IAPIRequestAccountRegistration,
  validateAPIRequestAccountRegistration,
} from "#codegen/schema/lib/api/authentication/registration/request";
import { validateNanoID } from "#codegen/schema/lib/types/strings/nanoid";
import { HTTP_STATUS } from "#lib/http";
import { ClientError } from "#lib/errors";
import { getSession } from "#lib/session";
import { startTransaction } from "#database";
import {
  loginAccount,
  registerAccount,
  registerAdministrator,
  toPublicAccount,
} from "#entities/account";
import { apiRoute } from "#middleware/api";
import { createBodyValidator } from "#middleware/entities";

export const authentication = Router({ mergeParams: true });

interface ISessionLocals extends Record<string, unknown> {
  session: Awaited<ReturnType<typeof getSession>>;
}

// get session
authentication.use<unknown, unknown, unknown, unknown, ISessionLocals>(
  async (req, res, next) => {
    try {
      const session = await getSession(req, res);
      res.locals.session = session;

      next();
    } catch (error) {
      next(error);
    }
  },
);

// registration
authentication.post<unknown, unknown, unknown, unknown, ISessionLocals>(
  "/register",
  async (req, res, next) => {
    try {
      const { account_id } = res.locals.session;

      if (account_id) {
        throw new ClientError("Must be logged out to register.", {
          status: HTTP_STATUS.BAD_REQUEST,
        });
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);
authentication.post(
  "/register",
  createBodyValidator(validateAPIRequestAccountRegistration),
);
authentication.post<
  unknown,
  unknown,
  IAPIRequestAccountRegistration,
  unknown,
  ISessionLocals
>(
  "/register",
  apiRoute(async (req, res) => {
    const accountInit = req.body.data;

    if (IS_INVITE_ONLY) {
      validateNanoID(accountInit.invitation_code);
    }

    const result = await startTransaction(async (ctx) => {
      const account =
        ADMIN_INVITE && accountInit.invitation_code === ADMIN_INVITE
          ? await registerAdministrator(accountInit, ctx)
          : await registerAccount({ init: accountInit }, ctx);
      const session = res.locals.session;
      session.account_id = account.id;

      await session.save();

      return toPublicAccount(account);
    });

    return result;
  }),
);

// login
authentication.post<unknown, unknown, unknown, unknown, ISessionLocals>(
  "/login",
  async (req, res, next) => {
    try {
      const { account_id } = res.locals.session;

      if (account_id) {
        throw new ClientError("Must be logged out to login.", {
          status: HTTP_STATUS.BAD_REQUEST,
        });
      }

      next();
    } catch (error) {
      next(error);
    }
  },
);
authentication.post(
  "/login",
  createBodyValidator(validateAPIRequestAccountLogin),
);
authentication.post<
  unknown,
  unknown,
  IAPIRequestAccountLogin,
  unknown,
  ISessionLocals
>(
  "/login",
  apiRoute(async (req, res) => {
    const accountLogin = req.body.data;

    const result = await startTransaction(async (ctx) => {
      const account = await loginAccount(accountLogin, ctx);
      const session = res.locals.session;
      session.account_id = account.id;

      await session.save();

      return toPublicAccount(account);
    });

    return result;
  }),
);

// logout
authentication.post<unknown, unknown, unknown, unknown, ISessionLocals>(
  "/logout",
  apiRoute(async (req, res) => {
    res.locals.session.destroy();

    return null;
  }),
);
