import { Router } from "express";
import { Pagination } from "#lib/pagination";
import {
  getSiteProfilesPagination,
  getSiteProfiles,
  getSites,
  getSite,
  countSiteReleases,
  countSites,
  getSiteReleases,
} from "#entities/site";
import { startTransaction } from "#database";
import { apiRoute } from "#middleware/api";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { createEntityIDValidator } from "#middleware/entities";

export const sites = Router();

// get sites pagination
sites.get(
  "/sites",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const count = await countSites(undefined, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

sites.get("/sites/:page", createPageNumberValidator());
// get all sites
sites.get<{ page: string }>(
  "/sites/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const siteCount = await countSites(undefined, ctx);
      const pagination = new Pagination(siteCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const sites = await getSites(pagination, ctx);

      return { pagination, sites };
    });

    return result;
  }),
);

sites.use("/site/:site_id", createEntityIDValidator(["site_id"]));

// get a site
sites.get<{ site_id: string }>(
  "/site/:site_id",
  apiRoute(async (req, res) => {
    const { site_id } = req.params;
    const site = await startTransaction(async (ctx) => {
      return getSite(site_id, ctx);
    });

    return site;
  }),
);

// get site profiles pagination
sites.get<{ site_id: string }>(
  "/site/:site_id/profiles",
  apiRoute(async (req, res) => {
    const { site_id } = req.params;
    const siteProfilesCount = await getSiteProfilesPagination(site_id);
    const pagination = new Pagination(siteProfilesCount);

    return pagination;
  }),
);

sites.get("/site/:site_id/profiles/:page", createPageNumberValidator());

// get site profiles
sites.get<{ site_id: string; page: string }>(
  "/site/:site_id/profiles/:page",
  apiRoute(async (req, res) => {
    const { site_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const siteProfilesCount = await getSiteProfilesPagination(site_id, ctx);
      const pagination = new Pagination(siteProfilesCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const profiles = await getSiteProfiles({ site_id, pagination }, ctx);

      return {
        pagination,
        profiles,
      };
    });

    return result;
  }),
);

// get site profiles pagination
sites.get<{ site_id: string }>(
  "/site/:site_id/releases",
  apiRoute(async (req, res) => {
    const { site_id } = req.params;
    const count = await startTransaction(async (ctx) =>
      countSiteReleases(site_id, ctx),
    );
    const pagination = new Pagination(count);

    return pagination;
  }),
);

sites.get("/site/:site_id/releases/:page", createPageNumberValidator());

// get site profiles
sites.get<{ site_id: string; page: string }>(
  "/site/:site_id/releases/:page",
  apiRoute(async (req, res) => {
    const { site_id, page } = req.params;
    const result = await startTransaction(async (ctx) => {
      const siteProfilesCount = await countSiteReleases(site_id, ctx);
      const pagination = new Pagination(siteProfilesCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const releases = await getSiteReleases({ site_id, pagination }, ctx);

      return {
        pagination,
        releases,
      };
    });

    return result;
  }),
);
