import { Router } from "express";
import { selectReleaseCount } from "#database/queries/releases";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import { getReleases } from "#entities/release";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import { EMPTY_OBJECT } from "#types";

export const releases = Router();

// get releases pagination
releases.get(
  "/releases",
  apiRoute(async (req, res) => {
    const result = await startTransaction(async (ctx) => {
      const releaseCount = await selectReleaseCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(releaseCount);

      return pagination;
    });

    return result;
  }),
);

releases.get("/releases/:page", createPageNumberValidator());

// get releases
releases.get<{ page: string }>(
  "/releases/:page",
  apiRoute(async (req, res) => {
    const { page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const releaseCount = await selectReleaseCount(EMPTY_OBJECT, ctx);
      const pagination = new Pagination(releaseCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const releases = await getReleases(pagination, ctx);

      return {
        pagination,
        releases,
      };
    });

    return result;
  }),
);
