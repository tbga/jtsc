import { Router } from "express";
import { APISuccess } from "#lib/api";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  selectProfileIDs,
  selectProfilePreviews,
} from "#database/queries/profiles";
import { countArtistProfiles } from "#entities/profile";
import { getArtist } from "#entities/artist";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { createEntityIDValidator } from "#middleware/entities";
import { apiRoute } from "#middleware/api";

export const artist = Router();

artist.use("/artist/:artist_id", createEntityIDValidator(["artist_id"]));

// get artist data
artist.get("/artist/:artist_id", async (req, res, next) => {
  try {
    const { artist_id } = req.params;
    const artist = await getArtist(artist_id);

    return res.status(200).json(new APISuccess(artist));
  } catch (error) {
    next(error);
  }
});

// get artist profiles pagination
artist.get("/artist/:artist_id/profiles", async (req, res, next) => {
  try {
    const { artist_id } = req.params;
    const artistProfileCount = await countArtistProfiles({ artist_id });
    const pagination = new Pagination(artistProfileCount);

    return res.status(200).json(new APISuccess(pagination));
  } catch (error) {
    next(error);
  }
});

artist.get("/artist/:artist_id/profiles/:page", createPageNumberValidator());

// get artist profiles
artist.get<{ artist_id: string; page: string }>(
  "/artist/:artist_id/profiles/:page",
  apiRoute(async (req, res) => {
    const { artist_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const artistProfileCount = await countArtistProfiles({ artist_id }, ctx);
      const pagination = new Pagination(artistProfileCount, page);

      validateClientPageNumber(page, pagination.total_pages);

      const ids = await selectProfileIDs(
        { artist_ids: [artist_id], pagination },
        ctx,
      );
      const profiles = await selectProfilePreviews(ids, ctx);

      return {
        pagination,
        profiles,
      };
    });

    return result;
  }),
);
