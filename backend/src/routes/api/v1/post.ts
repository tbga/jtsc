import { Router } from "express";
import { Pagination } from "#lib/pagination";
import { startTransaction } from "#database";
import {
  createPageNumberValidator,
  validateClientPageNumber,
} from "#middleware";
import { apiRoute } from "#middleware/api";
import { createEntityIDValidator } from "#middleware/entities";
import {
  countPostArtists,
  countPostReleases,
  getPost,
  getPostArtists,
  getPostReleases,
} from "#entities/post";

export const post = Router({ mergeParams: true });

post.use("/post/:post_id", createEntityIDValidator(["post_id"]));

// get a post
post.get<{ post_id: string }>(
  "/post/:post_id",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const post = await getPost(post_id, ctx);

      return post;
    });

    return result;
  }),
);

post.get<{ post_id: string }>(
  "/post/:post_id/artists",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countPostArtists(post_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

post.get("/post/:post_id/artists/:page", createPageNumberValidator());

post.get<{ post_id: string; page: string }>(
  "/post/:post_id/artists/:page",
  apiRoute(async (req, res) => {
    const { post_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countPostArtists(post_id, ctx);
      const pagination = new Pagination(count, page);

      validateClientPageNumber(page, pagination.total_pages);

      const artists = await getPostArtists({ pagination, post_id }, ctx);

      return {
        pagination,
        artists,
      };
    });

    return result;
  }),
);

post.get<{ post_id: string }>(
  "/post/:post_id/releases",
  apiRoute(async (req, res) => {
    const { post_id } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countPostReleases(post_id, ctx);
      const pagination = new Pagination(count);

      return pagination;
    });

    return result;
  }),
);

post.get("/post/:post_id/releases/:page", createPageNumberValidator());
post.get<{ post_id: string; page: string }>(
  "/post/:post_id/releases/:page",
  apiRoute(async (req, res) => {
    const { post_id, page } = req.params;

    const result = await startTransaction(async (ctx) => {
      const count = await countPostReleases(post_id, ctx);
      const pagination = new Pagination(count);

      validateClientPageNumber(page, pagination.total_pages);

      const releases = await getPostReleases({ post_id, pagination }, ctx);

      return {
        pagination,
        releases,
      };
    });

    return result;
  }),
);
