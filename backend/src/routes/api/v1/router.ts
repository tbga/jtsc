import { Router } from "express";
import { account } from "#routes/api/v1/account";
import { profile } from "#routes/api/v1/profile";
import { authentication } from "#routes/api/v1/authentication";
import { apiRoute } from "#middleware/api";
import { stats } from "./stats.js";
import { sites } from "./sites.js";
import { artists } from "./artists.js";
import { artist } from "./artist.js";
import { post } from "./post.js";
import { posts } from "./posts.js";
import { profiles } from "./profiles.js";
import { releases } from "./releases.js";
import { release } from "./release.js";

export const v1 = Router({ mergeParams: true });

const resBody = {
  message: "JTSC API v1",
};

v1.get(
  "/",
  apiRoute(async (req, res) => {
    return resBody;
  }),
);

v1.use("/stats", stats);
v1.use(artists);
v1.use(artist);
v1.use(sites);
v1.use(profiles);
v1.use(profile);
v1.use(post);
v1.use(posts);
v1.use(releases);
v1.use(release);
v1.use("/authentication", authentication);
v1.use("/account", account);
