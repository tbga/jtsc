import { Router } from "express";
import { APISuccess } from "#lib/api";
import { HTTP_STATUS } from "#lib/http";
import { apiHandler, errorHandler } from "#middleware/api";
import { v1 } from "#routes/api/v1";

export const api = Router();

api.use(apiHandler);

api.get("/", async (req, res, next) => {
  try {
    return res.status(HTTP_STATUS.OK).json(new APISuccess("JTSC API"));
  } catch (error) {
    next(error);
  }
});

api.use("/v1", v1);

api.use(errorHandler);
