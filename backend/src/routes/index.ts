import { Router } from "express";
import { api } from "#routes/api";
import { pages } from "#routes/pages";
import { IS_DEVELOPMENT } from "#environment/derived-variables.js";

export const routes = Router();

if (!IS_DEVELOPMENT) {
  routes.use(pages);
}
routes.use("/api", api);
