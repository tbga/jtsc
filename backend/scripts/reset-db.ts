import fs from "node:fs/promises";
import {
  LOCAL_STORAGE_FOLDER,
  PUBLIC_STORAGE_FOLDER,
  TEMPORARY_FOLDER,
} from "#environment/variables.js";
import { resetDatabase } from "#database";

await run();

async function run() {
  console.log("Starting the wipe...");

  console.log("Starting temporary folder wipe...");

  await fs.rm(TEMPORARY_FOLDER, { recursive: true, force: true });

  console.log("Finished temporary folder wipe.");

  console.log("Starting database reset (this will keep tables intact).");

  await resetDatabase(undefined);

  console.log("Finished database reset.");

  console.log("Starting storage wipe...");

  await fs.rm(LOCAL_STORAGE_FOLDER, { recursive: true, force: true });
  await fs.rm(PUBLIC_STORAGE_FOLDER, { recursive: true, force: true });

  console.log("Finished storage wipe.");

  console.log("Finished the wipe.");
}
