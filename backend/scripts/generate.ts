import { startTransaction, terminateDatabase } from "#database";
import { generateData } from "#mocks";

await run();
async function run() {
  try {
    await startTransaction(async (ctx) => {
      await generateData(undefined, ctx);
    });
  } finally {
    terminateDatabase();
  }
}
