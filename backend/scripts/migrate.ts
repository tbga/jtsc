import { runDatabaseMigrations } from "#database";

await runDatabaseMigrations();
