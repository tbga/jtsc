import fs from "node:fs/promises";
import {
  LOCAL_STORAGE_FOLDER,
  PUBLIC_STORAGE_FOLDER,
  TEMPORARY_FOLDER,
} from "#environment/variables.js";
import { log } from "#lib/logs";
import { startTransaction, wipeDatabase } from "#database";

await run();

async function run() {
  log("Starting the wipe...");

  log("Starting temporary folder wipe...");

  await fs.rm(TEMPORARY_FOLDER, { recursive: true, force: true });

  log("Finished temporary folder wipe.");

  log(
    "Starting database wipe (this will remove all project tables, including migration ones)...",
  );
  await startTransaction(async (ctx) => {
    await wipeDatabase(undefined, ctx);
  });

  log("Finished database wipe.");

  log("Starting storage wipe...");

  await fs.rm(LOCAL_STORAGE_FOLDER, { recursive: true, force: true });
  await fs.rm(PUBLIC_STORAGE_FOLDER, { recursive: true, force: true });

  log("Finished storage wipe.");

  log("Finished the wipe.");
}
