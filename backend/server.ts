#!/usr/bin/env node

import { initDatabase } from "#database";
import { createServer } from "#lib/server";

await initDatabase();
createServer();
