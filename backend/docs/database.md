# Database

## Migrations

### Squashing

Migration squashing requirements:
- must work both on empty databases and databases with all prior migrations applied
- except for migrations table, must leave the database in exactly the same state as all prior migrations applied but without the squashed migration
- must be ran in a single transaction

## Application Driver
There are issues with concrete symbol imports from `pg-promise`.
This doesn't work:

```typescript
export { ColumnSet, Column, TableName } from "pg-promise";
```

Neither does this:

```typescript
import pgPropmise from "pg-promise";

export const { ColumnSet, Column, TableName } = pgPropmise;
```

Only this works:

```typescript
import pgPropmise from "pg-promise";

const pgp = pgPromise();

export const { ColumnSet, Column, TableName } = pgp.helpers;
```

This is why these symbols have to be imported from `"#database"` instead of `"pg-promise"`.
