// this file is for extra type declarations

import type { Abortable } from "node:events";

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      /**
       * Project env.
       */
      PROJECT_ENV: "development" | "production" | undefined;
    }
  }
}
