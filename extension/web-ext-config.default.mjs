const config = {
  sourceDir: "dist",
  artifactsDir: "dist",
  ignoreFiles: ["node_modules", "web-ext-config.js"],
  run: {
    startUrl: [
      "about:debugging#/runtime/this-firefox",
      "https://translate.google.com",
      // "https://twitter.com/commentiquette",
      // "https://www.youtube.com/channel/UCyWDmyZRjrGHeKF-ofFsT5Q",
      // "https://www.youtube.com/@djcribdef4920",
      // "https://www.patreon.com/commentiquette",
      // "https://www.patreon.com/user?u=2404739",
      // "https://www.reddit.com/user/NintendoUSA",
      // "https://www.tiktok.com/@genznewz",
      // "https://vimeo.com/user50655577",
      // "https://vimeo.com/patricknelson",
      // "https://space.bilibili.com/116683",
      // "https://fukahire.fanbox.cc/",
      // "https://www.facebook.com/people/Nonsummerjack-Non/pfbid032kvcj9rHQav9tNwTdVjX8GWifR5mpBvUdLKnarf7Avxxi7ws3J26TrB97kiDHQqMl",
      // "https://www.instagram.com/non_nonsummerjack"
      // "https://nonsummerjack.gumroad.com",
      // "https://gumroad.com/2191578588195"
      // "https://fantia.jp/fanclubs/24362",
      // "https://onlyfans.com/nonsummerjack",
      // "https://weibo.com/u/5681945173",
      // "https://www.douyin.com/user/MS4wLjABAAAApP4cjq-eImt-LSBdZeDQMa6qMNJ6eXJgY18t4un7V2g",
    ],
    pref: [
      "browser.startup.windowsLaunchOnLogin.disableLaunchOnLoginPrompt=true",
    ],
  },
  build: {
    overwriteDest: true,
  },
};

export default config;
