// @ts-check
// const path = require("node:path");
const { merge } = require("webpack-merge");
// const CopyPlugin = require("copy-webpack-plugin");
const commonConfig = require("./webpack.common.js");

// @TODO remember how to do named exports in CJS
// const outputPath = path.resolve(__dirname, "dist");
// const sourceCodePaths = [
//   "src",
//   ".gitignore",
//   "LICENSE",
//   "manifest.json",
//   "manifest.schema.json",
//   "package-lock.json",
//   "package.json",
//   "README.md",
//   "tsconfig.json",
//   "web-ext-config.default.js",
//   "webpack.common.js",
//   "webpack.dev.js",
//   "webpack.prod.js",
//   "assets",
//   ".vscode",
// ].map((fromPath) => {
//   const toPath = path.join(outputPath, "source-code", fromPath);
//   return { from: fromPath, to: toPath };
// });

/**
 * @type {import("webpack").Configuration}
 */
const productionConfig = {
  mode: "production",
  devtool: "source-map",
  // plugins: [new CopyPlugin({ patterns: sourceCodePaths })],
  optimization: {
    // runtimeChunk: "single",
    // chunkIds: 'deterministic',
    moduleIds: "deterministic",
    minimize: false,
    runtimeChunk: false,
    // splitChunks: {
    //   cacheGroups: {
    //     vendor: {
    //       test: /[\\/]node_modules[\\/]/,
    //       name: "vendors",
    //       chunks: "all",
    //     },
    //   },
    // },
  },
};

module.exports = merge(commonConfig, productionConfig);
