# Profile Assistant

The purpose of this extension is to collect serializable profile/release fields which tend to be buried deep within markup/scripts.
Such as date stamps which are stored as `ISO8601` strings but then get formatted locally through page scripts.
For that to work it must have access to the URL and the underlying page.
However it must not run side effects such as additional API calls (even within the same origin) or modify the page's DOM in any way.
The only markup it is allowed to modify is one in the popup.

## Build
1. Install all dependencies:
   ```sh
   npm install
   ```
2. Copy the contents of `web-ext-config.default.mjs` into `web-ext-config.mjs`:
   ```sh
   cp --interactive extension/web-ext-config.default.mjs extension/web-ext-config.mjs
   ```
3. Build the extension:
   ```sh
   npm run build --workspace=extension
   ```
4. `extension/dist` will have a `.zip` file with extension.
5. Optionally run an extension on temporary profile:
   ```sh
   npm start --workspace=extension
   ```

## Develop

```sh
git clone https://gitlab.com/the-coommectors/jtsc.git
npm install
cp --interactive extension/web-ext-config.default.mjs extension/web-ext-config.mjs
npm run develop --workspace=extension
```
