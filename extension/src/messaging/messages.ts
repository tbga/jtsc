import type {
  IMessage,
  IMessageTypeBody,
  IProfileData,
  IReleaseData,
} from "./types";

export function createMessage<
  MessageType extends keyof IMessageTypeBody = keyof IMessageTypeBody,
>(
  messageType: MessageType,
  data: IMessageTypeBody[MessageType],
  aggregateError?: AggregateError
): IMessage<MessageType> {
  const message: IMessage<MessageType> = {
    id: crypto.randomUUID(),
    type: messageType,
    data: data,
  };

  if (aggregateError) {
    message.other_messages = [aggregateError];
  }

  return message;
}

export const createErrorMessage = (
  error: Error,
  otherMessages?: AggregateError
) => createMessage("error", error, otherMessages);

export const createProfileMessage = (
  profileData: IProfileData,
  otherMessages?: AggregateError
) => createMessage("profile", profileData, otherMessages);

export const createReleaseMessage = (
  releaseData: IReleaseData,
  otherMessages?: AggregateError
) => createMessage("release", releaseData, otherMessages);
