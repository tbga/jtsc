export interface IMessageTypeBody {
  error: Error;
  profile: IProfileData;
  release: IReleaseData;
}

const messageTypes = [
  "error",
  "profile",
  "release",
] as const satisfies (keyof IMessageTypeBody)[];
export type IMessageType = (typeof messageTypes)[number];

export interface IProfileData {
  id?: string;
  title?: string;
  description?: string;
  other_names?: string[];
  /**
   * A raw join date string as is.
   */
  created_at?: string;
  /**
   * Not all sites provide a way to construct
   * a canonical URL.
   */
  canonical_url?: string;
}

export interface IReleaseData {
  id?: string;
  title?: string;
  description?: string;
  /**
   * A raw join date string as is.
   */
  created_at?: string;
  /**
   * Not all sites provide a way to construct
   * a canonical URL.
   */
  canonical_url?: string;
}

export type IMessage<
  MessageType extends keyof IMessageTypeBody = keyof IMessageTypeBody,
> = IMessageBase &
  {
    [CurrentType in MessageType]: {
      type: CurrentType;
      data: IMessageTypeBody[CurrentType];
    };
  }[MessageType];

interface IMessageBase {
  id: string;
  other_messages?: Error[];
}

export function isKnownMessageType(input: unknown): input is IMessageType {
  return messageTypes.includes(input as IMessageType);
}
