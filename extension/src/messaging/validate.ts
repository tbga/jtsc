import { isKnownMessageType, type IMessage } from "./types";

export function validateMessage(input: unknown): asserts input is IMessage {
  if (!isObject(input)) {
    throw new Error(`Message must be of type "object" and not "null".`);
  }

  if (!("id" in input && "type" in input && "data" in input)) {
    throw new Error(
      `Message must have "id", "type" and "data" fields present.`
    );
  }

  if (!isNonEmptyString(input.id)) {
    throw new Error("Message ID must be a non-empty string.");
  }

  if (!(isNonEmptyString(input.type) && isKnownMessageType(input.type))) {
    throw new Error(`Unknown message type "${input.type}".`);
  }
}

function isObject(input: unknown): input is object {
  const result =
    typeof input === "object" && input !== null && !Array.isArray(input);
  return result;
}

function isNonEmptyString(input: unknown): input is string {
  const result = isString(input) && input.trim().length !== 0;
  return result;
}

function isString(input: unknown): input is string {
  return typeof input === "string";
}
