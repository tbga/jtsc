import type { IReleaseData } from "#messaging";
import { Pre } from "./pre";

interface IProps {
  releaseData: IReleaseData;
}

export function ReleaseData({ releaseData }: IProps) {
  const { id, title, description, created_at, canonical_url } = releaseData;

  return (
    <article className="profile">
      <dl>
        <div>
          <dt>ID:</dt>
          <dd className="profile__id">{id}</dd>
        </div>

        <div>
          <dt>Title:</dt>
          <dd className="profile__title">{title ?? "unknown"}</dd>
        </div>

        <div>
          <dt>Description:</dt>
          <dd className="profile__description">
            {!description ? "unknown" : <Pre>{description}</Pre>}
          </dd>
        </div>

        <div>
          <dt>Creation Date:</dt>
          {/* @TODO formatting once there is a way to share UI components */}
          <dd>{created_at ?? "unknown"}</dd>
        </div>

        <div>
          <dt>Canonical URL:</dt>
          {/* @TODO formatting once there is a way to share UI components */}
          <dd>{!canonical_url ? "unknown" : <Pre>{canonical_url}</Pre>}</dd>
        </div>
      </dl>
    </article>
  );
}
