import { Pre } from "./pre";

import * as styles from "./error.module.scss";

interface IErrorViewProps {
  error: Error;
}

export function ErrorView({ error }: IErrorViewProps) {
  return (
    <dl>
      <div>
        <dt>Name:</dt>
        <dd className="error__name">
          <Pre>{error.name}</Pre>
        </dd>
      </div>

      <div>
        <dt>Message:</dt>
        <dd className="error__message">
          <Pre>{error.message}</Pre>
        </dd>
      </div>

      {error.cause === undefined ? undefined : (
        <div>
          <dt>Caused By:</dt>
          <dd>
            {!(error.cause instanceof Error) ? (
              "Unknown"
            ) : (
              <ErrorView error={error.cause} />
            )}
          </dd>
        </div>
      )}
    </dl>
  );
}

interface IAggregateErrorViewProps {
  error: AggregateError;
}

export function AggregateErrorView({ error }: IAggregateErrorViewProps) {
  return (
    <dl>
      <div>
        <dt>Name:</dt>
        <dd className="error__name">
          <Pre>{error.name}</Pre>
        </dd>
      </div>

      <div>
        <dt>Message:</dt>
        <dd className="error__message">
          <Pre>{error.message}</Pre>
        </dd>
      </div>

      <div>
        <dt>Errors:</dt>
        <dd className="error__errors">
          <details>
            <summary>Details</summary>
            <ol className={styles.errors}>
              {error.errors.map((error, index) => {
                return (
                  <li key={index}>
                    <ErrorView error={error} />
                  </li>
                );
              })}
            </ol>
          </details>
        </dd>
      </div>
    </dl>
  );
}
