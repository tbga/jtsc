import type { IProfileData } from "#messaging";
import { Pre } from "./pre";

interface IProps {
  profileData: IProfileData;
}

export function ProfileData({ profileData }: IProps) {
  const { id, title, description, created_at, other_names, canonical_url } =
    profileData;

  return (
    <article className="profile">
      <dl>
        <div>
          <dt>ID:</dt>
          <dd className="profile__id">{id}</dd>
        </div>

        <div>
          <dt>Name:</dt>
          <dd className="profile__title">{title ?? "unknown"}</dd>
        </div>

        <div>
          <dt>Other Names:</dt>
          <dd>
            {other_names === undefined || other_names.length === 0 ? (
              "unknown"
            ) : (
              <ul>
                {other_names.map((name, index) => (
                  <li key={index}>{name}</li>
                ))}
              </ul>
            )}
          </dd>
        </div>

        <div>
          <dt>Description:</dt>
          <dd className="profile__description">
            {!description ? "unknown" : <Pre>{description}</Pre>}
          </dd>
        </div>

        <div>
          <dt>Join Date:</dt>
          {/* @TODO formatting once there is a way to share UI components */}
          <dd>{created_at ?? "unknown"}</dd>
        </div>

        <div>
          <dt>Canonical URL:</dt>
          {/* @TODO formatting once there is a way to share UI components */}
          <dd>{!canonical_url ? "unknown" : <Pre>{canonical_url}</Pre>}</dd>
        </div>
      </dl>
    </article>
  );
}
