import type { ComponentPropsWithRef } from "react";
import clsx from "clsx/lite";

import * as styles from "./pre.module.scss";

interface IPreProps extends ComponentPropsWithRef<"pre"> {}

export function Pre({ className, ...props }: IPreProps) {
  return <pre className={clsx(styles.block, className)} {...props} />;
}
