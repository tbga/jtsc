import { type ReactNode } from "react";

interface IProps {
  children: ReactNode;
}

export function Layout({ children }: IProps) {
  return (
    <>
      <header className="header">
        <a href="https://gitlab.com/the-coommectors/jtsc/-/tree/master/extension">
          Repository
        </a>
      </header>

      <main className="main page">
        {children}
      </main>

      <footer className="footer" />
    </>
  );
}
