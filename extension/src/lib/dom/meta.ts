import { ensureSelector } from "./lib";

type IMetaProperty =
  | "profile:username"
  | "description"
  | "og:description"
  | "og:url"
  | "twitter:value1"
  | "article:published_time";

export function attemptMetaTagContent(
  inputName: IMetaProperty | IMetaProperty[]
): string | undefined {
  let content: string | undefined;

  if (!Array.isArray(inputName)) {
    content = ensureSelector<HTMLMetaElement>(
      `meta[name="${inputName}"], meta[property="${inputName}"]`
    ).content.trim();
  } else {
    for (const property of inputName) {
      content = document
        .querySelector<HTMLMetaElement>(
          `meta[name="${property}"], meta[property="${property}"]`
        )
        ?.content.trim();

      if (content) {
        break;
      }
    }
  }

  return content;
}

export function ensureMetaTagContent(
  inputName: IMetaProperty | IMetaProperty[]
): string {
  const content = attemptMetaTagContent(inputName);

  if (!content) {
    const message = `Failed to find content for meta tag "${
      !Array.isArray(inputName) ? inputName : inputName.at(-1)
    }".`;
    throw new Error(message);
  }

  return content;
}
