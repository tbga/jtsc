import { ISelector } from "./types";

/**
 * `querySelector()` but throws when nothing found.
 */
export function ensureSelector<ReturnElement extends Element = Element>(
  inputSelector: ISelector,
  parentElement: Element | Document = document
): ReturnElement {
  let result: ReturnElement | null = null;

  if (!Array.isArray(inputSelector)) {
    result = parentElement.querySelector<ReturnElement>(inputSelector);
  } else {
    for (const selector of inputSelector) {
      result = parentElement.querySelector<ReturnElement>(selector);

      if (result !== null) {
        break;
      }
    }
  }

  if (result === null) {
    const message = `Failed to find an element for selector \`${
      !Array.isArray(inputSelector) ? inputSelector : inputSelector.at(-1)
    }\`.`;
    throw new Error(message);
  }

  return result;
}

/**
 * `ensureSelector().textContent?.trim()` but without typescript boilerplate required.
 */
export function ensureTextContent(
  selector: ISelector,
  parentElement: Element | Document = document
): string {
  const content = ensureSelector(selector, parentElement).textContent?.trim();

  if (!content) {
    throw new Error(
      `Failed to find an text content for selector "${selector}".`
    );
  }

  return content;
}

/**
 * `ensureTextContent()` but returns `undefined` instead of throwing on failure.
 */
export function attemptTextContent(
  selector: string,
  parentElement: Element | Document = document
): string | undefined {
  const result = parentElement.querySelector(selector)?.textContent?.trim();

  return result === undefined || result.length === 0 ? undefined : result;
}

/**
 * `ensureSelector().innerText?.trim()` but without typescript boilerplate required.
 */
export function ensureInnerText(
  selector: string,
  parentElement: Element | Document = document
): string {
  const content = ensureSelector<HTMLElement>(
    selector,
    parentElement
  ).innerText?.trim();

  if (!content) {
    throw new Error(
      `Failed to find an text content for selector "${selector}".`
    );
  }

  return content;
}

export function selectLDJSON<ReturnType>(
  selector?: string,
  parentElement: Element | Document = document
) {
  const content = ensureTextContent(
    selector ?? `script[type="application/ld+json"]`,
    parentElement
  );

  const data: ReturnType = JSON.parse(content);

  return data;
}
