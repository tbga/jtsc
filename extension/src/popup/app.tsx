import "../styles/variables.scss";
import "../styles/base.scss";
import "../styles/components.scss";
import { useEffect, useState } from "react";
import {
  Layout,
  ErrorView,
  Loading,
  ProfileData,
  ReleaseData,
  Pre,
  AggregateErrorView,
} from "#components";
import {
  validateMessage,
  type IProfileData,
  type IReleaseData,
} from "#messaging";

import * as styles from "./app.module.scss";

type IStatus = { other_messages?: Error[] } & (
  | { type: "error"; error: Error }
  | { type: "profile"; profileData: IProfileData }
  | { type: "release"; releaseData: IReleaseData }
);

export function Popup() {
  const [status, changeStatus] = useState<IStatus>();
  const heading =
    status?.type === "error"
      ? "Error"
      : status?.type === "profile"
        ? "Profile"
        : status?.type === "release"
          ? "Release"
          : "Loading...";

  useEffect(() => {
    try {
      browser.runtime.onMessage.addListener(listenForMessages);

      browser.tabs.executeScript({ file: "/content/index.js" });
    } catch (error) {
      if (!(error instanceof Error)) {
        console.error(error);
      } else {
        changeStatus({ type: "error", error });
      }
    }

    return () => {
      try {
        browser.runtime.onMessage.removeListener(listenForMessages);
      } catch (error) {
        if (!(error instanceof Error)) {
          console.error(error);
          return;
        }

        changeStatus({ type: "error", error });
      }
    };
  }, []);

  function listenForMessages(
    message: unknown,
    sender: Parameters<
      Parameters<typeof browser.runtime.onMessage.addListener>[0]
    >[1],
    sendResponse: Parameters<
      Parameters<typeof browser.runtime.onMessage.addListener>[0]
    >[2]
  ) {
    try {
      validateMessage(message);
      // @TODO `data` validation

      const { type, data, other_messages } = message;

      switch (type) {
        case "error": {
          changeStatus({ other_messages, type, error: data });
          break;
        }

        case "profile": {
          changeStatus({ other_messages, type, profileData: data });
          break;
        }

        case "release": {
          changeStatus({ other_messages, type, releaseData: data });
          break;
        }

        default: {
          const error = new Error(
            `Unknown message type "${type satisfies never}".`
          );
          changeStatus({ other_messages, type: "error", error });
        }
      }
    } catch (error) {
      if (!(error instanceof Error)) {
        console.error(error);
        return;
      }

      changeStatus({ type: "error", error });
    }
  }

  return (
    <Layout>
      <h1 className="heading">{heading}</h1>
      {status?.type === "error" ? (
        <article>
          <section>
            <ErrorView error={status.error} />
          </section>
        </article>
      ) : status?.type === "profile" ? (
        <ProfileData profileData={status.profileData} />
      ) : status?.type === "release" ? (
        <ReleaseData releaseData={status.releaseData} />
      ) : (
        <Loading />
      )}

      {status?.other_messages && (
        <article>
          <header>
            <h2>Errors</h2>
          </header>
          <section>
            <ol className={styles.errors}>
              {status.other_messages.map((error, index) => (
                <li key={index}>
                  {error instanceof AggregateError ? (
                    <AggregateErrorView error={error} />
                  ) : (
                    <ErrorView error={error} />
                  )}
                </li>
              ))}
            </ol>
          </section>
        </article>
      )}
    </Layout>
  );
}
