import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { Popup } from "./app";

const rootElement = document.getElementById("root");

if (!rootElement) {
  throw new Error("Root element is missing.");
}

const root = createRoot(rootElement);

root.render(
  <StrictMode>
    <Popup />
  </StrictMode>
);
