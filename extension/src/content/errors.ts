import { CustomError } from "@repo/core/errors";
import { NormalizedURL, getPathnameAndSearchParams } from "@repo/core/urls";

export class ProfileCollectorError extends AggregateError {
  constructor(errors: Error[]) {
    super(errors, "Encountered errors while collecting profile data.");
  }
}

export class ReleaseCollectorError extends AggregateError {
  constructor(errors: Error[]) {
    super(errors, "Encountered errors while collecting release data.");
  }
}

export class UnknownSitePathError extends CustomError {
  constructor(url: NormalizedURL, options?: ErrorOptions) {
    super(`Unknown path "${getPathnameAndSearchParams(url)}".`, options);
  }
}
