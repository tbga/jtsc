import { NormalizedURL } from "@repo/core/urls";
import { sleep } from "@repo/core/utils";
import {
  createErrorMessage,
  createProfileMessage,
  createReleaseMessage,
  type IMessage,
} from "#messaging";
import { ProfileCollectorError } from "./errors";
import {
  guessURLType as guessTwitterURLType,
  collectProfile as collectTwitterProfile,
  collectRelease as collectTwitterRelease,
} from "./sites/twitter";
import {
  guessURLType as guessYoutubeURLType,
  collectProfile as collectYoutubeProfile,
  collectRelease as collectYoutubeRelease,
} from "./sites/youtube";
import {
  guessURLType as guessRedditURLType,
  collectProfile as collectRedditProfile,
  collectRelease as collectRedditRelease,
} from "./sites/reddit";
import {
  guessURLType as guessTiktokURLType,
  collectProfile as collectTiktokProfile,
  collectRelease as collectTiktokRelease,
} from "./sites/tiktok";
import {
  guessURLType as guessVimeoURLType,
  collectProfile as collectVimeoProfile,
  collectRelease as collectVimeoRelease,
} from "./sites/vimeo";
import {
  guessURLType as guessPatreonURLType,
  collectProfile as collectPatreonProfile,
  collectRelease as collectPatreonRelease,
} from "./sites/patreon";
import {
  guessURLType as guessBilibiliURLType,
  collectProfile as collectBilibiliProfile,
  collectRelease as collectBilibiliRelease,
} from "./sites/bilibili";
import {
  guessURLType as guessPixivFanboxURLType,
  collectProfile as collectPixivFanboxProfile,
  collectRelease as collectPixivFanboxRelease,
} from "./sites/pixiv-fanbox";
import {
  guessURLType as guessGumroadURLType,
  collectProfile as collectGumroadProfile,
  collectRelease as collectGumroadRelease,
} from "./sites/gumroad";
import {
  guessURLType as guessFantiaURLType,
  collectProfile as collectFantiaProfile,
  collectRelease as collectFantiaRelease,
} from "./sites/fantia";
import {
  guessURLType as guessOnlyFansURLType,
  collectProfile as collectOnlyFansProfile,
  collectRelease as collectOnlyFansRelease,
} from "./sites/onlyfans";
import {
  guessURLType as guessDouyinURLType,
  collectProfile as collectDouyinProfile,
  collectRelease as collectDouyinRelease,
} from "./sites/douyin";
import {
  guessURLType as guessWeiboURLType,
  collectProfile as collectWeiboProfile,
  collectRelease as collectWeiboRelease,
} from "./sites/weibo";
import {
  guessURLType as guessInstagramURLType,
  collectProfile as collectInstagramProfile,
  collectRelease as collectInstagramRelease,
} from "./sites/instagram";
import {
  guessURLType as guessFacebookURLType,
  collectProfile as collectFacebookProfile,
  collectRelease as collectFacebookRelease,
} from "./sites/facebook";
import {
  guessURLType as guessBlueskySocialURLType,
  collectProfile as collectBlueskySocialProfile,
  collectRelease as collectBlueskySocialRelease,
} from "./sites/bluesky-social";
import {
  IProfileCollector,
  IReleaseCollector,
  guessOrigin,
  type IURLType,
} from "./types";

export async function run() {
  try {
    const origin = guessOrigin(location.href);
    let args: Parameters<typeof collectMessage>;

    switch (origin) {
      case "TWITTER": {
        args = [
          guessTwitterURLType,
          collectTwitterProfile,
          collectTwitterRelease,
        ];
        break;
      }

      case "YOUTUBE": {
        args = [
          guessYoutubeURLType,
          collectYoutubeProfile,
          collectYoutubeRelease,
        ];
        break;
      }

      case "REDDIT": {
        args = [guessRedditURLType, collectRedditProfile, collectRedditRelease];
        break;
      }

      case "TIKTOK": {
        args = [guessTiktokURLType, collectTiktokProfile, collectTiktokRelease];
        break;
      }

      case "VIMEO": {
        args = [guessVimeoURLType, collectVimeoProfile, collectVimeoRelease];
        break;
      }

      case "PATREON": {
        args = [
          guessPatreonURLType,
          collectPatreonProfile,
          collectPatreonRelease,
        ];
        break;
      }

      case "BILIBILI": {
        args = [
          guessBilibiliURLType,
          collectBilibiliProfile,
          collectBilibiliRelease,
        ];
        break;
      }

      case "FANBOX": {
        args = [
          guessPixivFanboxURLType,
          collectPixivFanboxProfile,
          collectPixivFanboxRelease,
        ];
        break;
      }

      case "GUMROAD": {
        args = [
          guessGumroadURLType,
          collectGumroadProfile,
          collectGumroadRelease,
        ];
        break;
      }

      case "FANTIA": {
        args = [guessFantiaURLType, collectFantiaProfile, collectFantiaRelease];
        break;
      }

      case "ONLYFANS": {
        args = [
          guessOnlyFansURLType,
          collectOnlyFansProfile,
          collectOnlyFansRelease,
        ];
        break;
      }

      case "DOUYIN": {
        args = [guessDouyinURLType, collectDouyinProfile, collectDouyinRelease];
        break;
      }

      case "WEIBO": {
        args = [guessWeiboURLType, collectWeiboProfile, collectWeiboRelease];
        break;
      }

      case "INSTAGRAM": {
        args = [
          guessInstagramURLType,
          collectInstagramProfile,
          collectInstagramRelease,
        ];
        break;
      }

      case "FACEBOOK": {
        args = [
          guessFacebookURLType,
          collectFacebookProfile,
          collectFacebookRelease,
        ];
        break;
      }

      case "BLUESKY_SOCIAL": {
        args = [
          guessBlueskySocialURLType,
          collectBlueskySocialProfile,
          collectBlueskySocialRelease,
        ];
        break;
      }

      default: {
        throw new Error(`Unknown origin "${origin satisfies never}".`);
      }
    }

    const message = await collectMessage(...args);
    await browser.runtime.sendMessage(message);
  } catch (error) {
    const errorMessage = createErrorMessage(
      error instanceof Error ? error : new Error("Unknown Error")
    );
    await browser.runtime.sendMessage(errorMessage);
  }
}

async function collectMessage(
  guessURLType: (url: NormalizedURL) => IURLType,
  collectProfile: IProfileCollector,
  collectRelease: IReleaseCollector
): Promise<IMessage> {
  const urlType = guessURLType(new NormalizedURL(location.href));

  const maxAttempts = 4;
  let attempts = 1;

  while (attempts <= maxAttempts) {
    try {
      switch (urlType) {
        case "profile": {
          const errors: Error[] = [];
          const data = collectProfile(errors);
          const aggError = !errors.length
            ? undefined
            : new ProfileCollectorError(errors);
          const message = createProfileMessage(data, aggError);

          return message;
        }

        case "release": {
          const errors: Error[] = [];
          const data = collectRelease(errors);
          const aggError = !errors.length
            ? undefined
            : new ProfileCollectorError(errors);
          const message = createReleaseMessage(data, aggError);

          return message;
        }

        default: {
          throw new Error(`Unknown URL type "${urlType satisfies never}".`);
        }
      }
    } catch (error) {
      if (attempts < maxAttempts) {
        await sleep(1000);
        attempts++;
        continue;
      }

      throw error;
    }
  }

  throw new Error("Unreachable code path.");
}
