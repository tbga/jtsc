import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import type { IReleaseData } from "#messaging";
import { IReleaseCollector } from "../../types";
import type { IReleaseJSONLD } from "./types";

/**
 * @TODO full description
 */
export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  try {
    id = ensureSelector<HTMLElement>("[data-post-id]").dataset.postId;
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find release ID.", { cause: error }));
  }

  let jsonLD: IReleaseJSONLD | undefined;
  try {
    jsonLD = collectJSONLD();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const title = jsonLD?.headline;
  const description = jsonLD?.description;
  const createdAt = jsonLD?.datePublished;
  const data: IReleaseData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (title) {
    data.title = title;
  }

  if (description) {
    data.description = description;
  }

  if (createdAt) {
    data.created_at = createdAt;
  }

  return data;
};

function collectJSONLD(): IReleaseJSONLD {
  const content = ensureTextContent(
    `script[type="application/ld+json"]`
  );

  if (!content) {
    throw new Error("Failed to find release JSONLD.");
  }

  const data: IReleaseJSONLD = JSON.parse(content);

  return data;
}

function createCanonicalURL(releaseID: string) {
  return `https://fantia.jp/posts/${releaseID}`;
}
