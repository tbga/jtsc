export interface IReleaseJSONLD {
  datePublished: string;
  dateModified: string;
  headline: string;
  description: string;
}
