import { validateError } from "@repo/core/errors";
import type { IProfileData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined | null;
  try {
    id = ensureSelector<HTMLElement>("[fanclub-id]").getAttribute("fanclub-id");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile ID.", { cause: error }));
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("h1.title");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile name.", { cause: error }));
  }

  let description: string | undefined;
  try {
    description = collectDescription();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IProfileData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (description) {
    data.description = description;
  }

  return data;
};

function collectDescription(): string {
  const short =
    ensureSelector<HTMLDivElement>("div.fanclub-title").textContent?.trim();

  if (!short) {
    throw new Error("Failed to find short description of profile.");
  }

  const long =
    ensureSelector<HTMLDivElement>(".fanclub-comment").textContent?.trim();

  if (!long) {
    throw new Error("Failed to find long description of profile.");
  }

  const description = [short, long].join("\n\n");

  return description;
}

function createCanonicalURL(profileID: string) {
  return `https://fantia.jp/fanclubs/${profileID}`;
}
