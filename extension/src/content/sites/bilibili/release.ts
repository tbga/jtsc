import { validateError } from "@repo/core/errors";
import type { IReleaseData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  try {
    const url = ensureSelector<HTMLLinkElement>(`link[rel="canonical"]`).href;
    id = new URL(url).pathname.split("/").slice(1)[1];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  try {
    title = ensureTextContent("h1");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find title of release."));
  }

  let description: string | undefined;
  try {
    description = document
      .querySelector<HTMLSpanElement>("#v_desc .desc-info-text")
      ?.textContent?.trim();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let publishedDate: string | undefined;
  try {
    publishedDate = document.querySelector<HTMLMetaElement>(
      `meta[itemprop="datePublished"]`
    )?.content;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IReleaseData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (title) {
    data.title = title;
  }

  if (description) {
    data.description = description;
  }

  if (publishedDate) {
    data.created_at = publishedDate;
  }

  return data;
};

function createCanonicalURL(releaseID: string) {
  return `https://www.bilibili.com/video/${releaseID}`;
}
