import type { IProfileData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import { validateError } from "@repo/core/errors";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  try {
    id = collectID();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("#h-name");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let description: string | undefined;
  try {
    description = ensureTextContent(".h-sign");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const profile: IProfileData = {};

  if (id) {
    profile.id = id;
    profile.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    profile.title = name;
  }

  if (description) {
    profile.description = description;
  }

  return profile;
};

function collectID() {
  const href = ensureSelector<HTMLAnchorElement>(
    "#navigator .n-tab-links > .n-index"
  ).href;
  const url = new URL(href);
  const id = url.pathname.split("/").slice(1)[0];

  return id;
}

function createCanonicalURL(profileID: string) {
  return `https://space.bilibili.com/${profileID}`;
}
