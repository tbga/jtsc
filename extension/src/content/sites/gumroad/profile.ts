import { validateError } from "@repo/core/errors";
import type { IProfileData } from "#messaging";
import { ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import type { IProfileInfo } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let profileInfo: IProfileInfo | undefined;
  try {
    profileInfo = collectProfileInfo();
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find profile information.", { cause: error })
    );
  }
  const creatorProfile = profileInfo?.creator_profile;
  const id = creatorProfile?.external_id;
  const name = creatorProfile?.name;
  // join it back in case the name has dots in it
  const otherName = creatorProfile?.subdomain.split(".").slice(0, -2).join(".");
  const description = profileInfo?.bio;

  const data: IProfileData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (otherName) {
    data.other_names = [otherName];
  }

  if (description) {
    data.description = description;
  }

  return data;
};

function collectProfileInfo(): IProfileInfo {
  const content = ensureTextContent(`script[data-component-name="Profile"]`);

  const profileInfo: IProfileInfo = JSON.parse(content);

  return profileInfo;
}

function createCanonicalURL(profileID: string) {
  return `https://gumroad.com/${profileID}`;
}
