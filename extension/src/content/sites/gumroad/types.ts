export interface IProfileInfo {
  creator_profile: {
    external_id: string;
    name: string;
    subdomain: string;
  };
  bio: string;
}

export interface IReleaseInfo {
  product: {
    /**
     * some sort of ID, probably only used for API requests
     */
    id: string;
    /**
     * Title of the release.
     */
    name: string;
    /**
     * This is the ID used for the page URL.
     */
    permalink: string;
    /**
     * Includes HTML tags.
     */
    description_html: string;
    seller: {
      /**
       * ID required for creating profile URL.
       */
      id: string;
      name: string;
      /**
       * URL includes ID required for release URL.
       */
      profile_url: string;
    };
  };
}
