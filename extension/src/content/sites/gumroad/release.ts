import { validateError } from "@repo/core/errors";
import type { IReleaseData } from "#messaging";
import { ensureSelector } from "#lib/dom";
import { IReleaseCollector } from "../../types";
import type { IReleaseInfo } from "./types";

export const collectRelease: IReleaseCollector = (errors) => {
  let releaseInfo: IReleaseInfo["product"] | undefined;
  try {
    releaseInfo = collectReleaseInfo();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let profileID: string | undefined;
  try {
    profileID =
      releaseInfo?.seller &&
      collectProfileID(new URL(releaseInfo?.seller.profile_url));
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const id = releaseInfo?.id;
  const title = releaseInfo?.name;

  let description: string | undefined;
  try {
    description = collectDescription();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IReleaseData = {};

  if (id) {
    data.id = id;
  }

  if (title) {
    data.title = title;
  }

  if (description) {
    data.description = description;
  }

  if (profileID && releaseInfo?.permalink) {
    data.canonical_url = createCanonicalURL(profileID, releaseInfo.permalink);
  }

  return data;
};

function collectReleaseInfo(): IReleaseInfo["product"] {
  const content = ensureSelector<HTMLScriptElement>(
    `script[data-component-name="ProductPage"]`
  ).textContent?.trim();

  if (!content) {
    throw new Error("Failed to find release info.");
  }

  const info: IReleaseInfo["product"] = JSON.parse(content).product;

  return info;
}

function collectProfileID(url: URL): string {
  const id = url.host.split(".").slice(0, -2).join(".");

  return id;
}

/**
 * Collecting it from the page because
 * the one in the info includes HTMl tags.
 */
function collectDescription() {
  const element = ensureSelector("article.product pre > code").cloneNode(true);

  if ((element.firstChild as HTMLDivElement)?.tagName === "DIV") {
    element.removeChild(element.firstChild as Node);
  }

  const description = element.textContent?.trim();

  if (!description) {
    throw new Error("Failed to find release description.");
  }

  return description;
}

function createCanonicalURL(profileID: string, releaseID: string) {
  return `https://${profileID}.gumroad.com/l/${releaseID}`;
}
