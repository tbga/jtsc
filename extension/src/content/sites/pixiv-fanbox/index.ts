import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  const pathnameSegments = url.pathname.split("/").slice(1);

  const isProfilePage = pathnameSegments[0] === "";

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage = pathnameSegments.length === 2 && pathnameSegments[0] === "posts";

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
