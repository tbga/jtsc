import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import type { IProfileData } from "#messaging";
import { IProfileCollector } from "../../types";
import type { IProfileMeta } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  try {
    id = collectID();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("h1");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile name.", { cause: error }));
  }

  let description: string | undefined;
  try {
    description = ensureTextContent(
      // using this very stable selector because
      // other sources truncate description
      "#root > div > div > div > div > div > div > div > div > div:first-child > div:first-child > div:last-child"
    );
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IProfileData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (description) {
    data.description = description;
  }

  return data;
};

export function collectID(): string {
  const metaData: IProfileMeta = JSON.parse(
    ensureSelector<HTMLMetaElement>("#metadata").content
  );
  const id = metaData.urlContext.host.creatorId;

  return id;
}

function createCanonicalURL(profileID: string) {
  return `https://${profileID}.fanbox.cc`;
}
