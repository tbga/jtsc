import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import type { IReleaseData } from "#messaging";
import { collectID as collectProfileID } from "./profile";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  try {
    // there are no other non-URL sources of ID so get one from the document
    id = new URL(location.href).pathname.split("/").slice(1)[1];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let profileID: string | undefined;
  try {
    profileID = collectProfileID();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  try {
    title = ensureTextContent("article h1");
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find title of release.", { cause: error })
    );
  }

  let publishedDate: string | undefined;
  try {
    publishedDate = ensureSelector<HTMLDivElement>(
      "article > div > div:nth-child(2)"
    )?.firstChild?.textContent?.trim();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IReleaseData = {};

  if (id) {
    data.id = id;

    if (profileID) {
      data.canonical_url = createCanonicalURL(profileID, id);
    }
  }

  if (title) {
    data.title = title;
  }

  if (publishedDate) {
    data.created_at = publishedDate;
  }

  return data;
};

function createCanonicalURL(profileID: string, releaseID: string) {
  return `https://${profileID}.fanbox.cc/posts/${releaseID}`;
}
