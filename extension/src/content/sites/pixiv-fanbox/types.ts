export interface IProfileMeta {
  urlContext: {
    host: {
      /**
       * This is only place ID is mentioned outside of URLs.
       */
      creatorId: string
    }
  }
}
