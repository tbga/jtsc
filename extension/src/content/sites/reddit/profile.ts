import { validateError } from "@repo/core/errors";
import { IProfileCollector } from "../../types";

export const collectProfile: IProfileCollector = (errors) => {
  // reddit provides a json view for any page
  // if its pathname is appended with `.json`
  const isJSONPath = new URL(location.href).pathname.endsWith(".json");

  // firefox JSON viewer seems to prevent content script from firing
  if (isJSONPath) {
    throw new Error("JSON endpoints are not supported.");
  }

  let profileID: string | undefined;
  try {
    profileID = collectProfileID();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = collectProfileName();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let joinDate: string | undefined
  try {
    joinDate = document.querySelector("time")?.dateTime.trim()
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const profile: ReturnType<typeof collectProfile> = {};

  if (profileID) {
    profile.id = profileID;
  }

  if (name) {
    profile.title = name;
    profile.canonical_url = createCanonicalProfileURL(name);
  }

  if (joinDate) {
    profile.created_at = joinDate;
  }

  return profile;
};

function collectProfileID(): string {
  const id =
    document.querySelector<HTMLAnchorElement>("a[data-fullname]")?.dataset
      .fullname;

  if (!id) {
    throw new Error("Failed to find a profile ID.");
  }

  return id;
}

function collectProfileName(): string {
  const name = document
    .querySelector<HTMLSpanElement>("#header-bottom-left .pagename")
    ?.textContent?.trim();

  if (!name) {
    throw new Error("Failed to find a profile name.");
  }

  return name;
}

/**
 * There doesn't seem to be a way to construct a URL
 * out of profile ID so using the name one instead.
 */
function createCanonicalProfileURL(userName: string) {
  return `https://old.reddit.com/user/${userName}`;
}
