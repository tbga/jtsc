import type { IReleaseCollector, IURLType } from "../../types";

export { collectProfile } from "./profile";

export function guessURLType(url: URL): IURLType {
  // reddit doesn't care about trailing slash,
  // but also doesn't redirect automatically,
  // so it has to be removed manually
  const isTrailingSlash = url.pathname.endsWith("/");
  const pathnameSegments = !isTrailingSlash
    ? url.pathname.split("/").slice(1)
    : url.pathname.split("/").slice(1, -1);

  const isProfilePage =
    pathnameSegments.length === 2 && pathnameSegments[0] === "user";

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage = false;

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}

export const collectRelease: IReleaseCollector = () => {
  throw new Error("Not implemented.");
};
