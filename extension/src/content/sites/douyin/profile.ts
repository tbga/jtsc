import type { IProfileData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import { validateError } from "@repo/core/errors";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  try {
    id = collectID();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("h1 span span span span span span");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile name", { cause: error }));
  }

  let description: string | undefined;
  try {
    description = collectDescription();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IProfileData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (description) {
    data.description = description;
  }

  return data;
};

/**
 * ID is not stored in a non-URL form
 * so collecting it from canonical URL.
 */
function collectID(): string {
  const href = ensureSelector<HTMLLinkElement>(`link[rel="canonical"]`).href;
  const id = new URL(href).pathname.split("/").slice(1)[1];

  return id;
}

function collectDescription(): string {
  const badge = ensureSelector<HTMLSpanElement>(
    `[data-e2e="badge-role-name"]`
  ).textContent?.trim();

  if (!badge) {
    throw new Error("Failed to find profile badge.");
  }

  const short = ensureSelector<HTMLSpanElement>(
    `div[data-e2e="user-info"] > p > span`
  ).textContent?.trim();

  if (!short) {
    throw new Error("Failed to find profile short description.");
  }

  const description = [badge, short].join("\n\n");

  return description;
}

function createCanonicalURL(profileID: string) {
  return `https://www.douyin.com/user/${profileID}`;
}
