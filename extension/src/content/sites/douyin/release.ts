import type { IReleaseData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IReleaseCollector } from "../../types";
import { validateError } from "@repo/core/errors";

export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  try {
    id = ensureSelector<HTMLDivElement>("div[data-e2e-aweme-id]").dataset
      .e2eAwemeId;
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find release ID.", { cause: error }));
  }

  let title: string | undefined;
  try {
    title = ensureTextContent("h1 > span > span > span");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find release title.", { cause: error }));
  }

  let createdAt: string | undefined;
  try {
    createdAt = collectCreatedAtTimestamp();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IReleaseData = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (title) {
    data.title = title;
  }

  if (createdAt) {
    data.created_at = createdAt;
  }

  return data;
};

function collectCreatedAtTimestamp(): string {
  const element = ensureSelector<HTMLSpanElement>(
    "[data-e2e-aweme-id] > div > div > span"
  ).cloneNode(true);

  element.removeChild(element.firstChild as Node);

  const timestamp = element.textContent?.trim();

  if (!timestamp) {
    throw new Error("Failed to find release creation date.");
  }

  return timestamp;
}

function createCanonicalURL(releaseID: string) {
  return `https://www.douyin.com/video/${releaseID}`;
}
