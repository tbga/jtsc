import { NotImplementedError, validateError } from "@repo/core/errors";
import { getTitleAndDescription } from "@repo/core/strings";
import { NormalizedURL, getPathnameSegments } from "@repo/core/urls";
import {
  attemptTextContent,
  ensureMetaTagContent,
  ensureSelector,
  ensureTextContent,
} from "#lib/dom";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  throw new NotImplementedError()
  // let id: string | undefined;
  // try {
  //   id = getPathnameSegments(new NormalizedURL(location.href))[3];
  // } catch (error) {
  //   validateError(error);

  //   errors.push(error);
  // }

  // let profileID: string | undefined;
  // try {
  //   const href = ensureSelector<HTMLLinkElement>(
  //     `link[rel="alternate"][type="application/json+oembed"]`
  //   ).href;
  //   const memeHref = new URL(href).searchParams.get("url");

  //   if (!memeHref) {
  //     throw new Error("Failed to find embed hyperlink.");
  //   }

  //   profileID = getPathnameSegments(new NormalizedURL(memeHref))[1];
  // } catch (error) {
  //   validateError(error);

  //   errors.push(error);
  // }

  // let title: string | undefined;
  // let description: string | undefined;
  // try {
  //   let content = document
  //     .querySelector(`a[href$="${id}"]`)
  //     ?.parentElement?.nextElementSibling?.firstElementChild?.textContent?.trim();

  //   if (content) {
  //     const parsedContent = getTitleAndDescription(content);

  //     title = parsedContent.title;
  //     description = parsedContent.description;
  //   }
  // } catch (error) {
  //   validateError(error);

  //   errors.push(error);
  // }

  // const data: ReturnType<typeof collectRelease> = {};

  // if (id) {
  //   data.id = id;

  //   if (profileID) {
  //     data.canonical_url = createCanonicalURL(profileID, id);
  //   }
  // }

  // if (title) {
  //   data.title = title;
  // }

  // if (description) {
  //   data.description = description;
  // }

  // return data;
};

/**
 * Looks like this is the only format:
 * https://github.com/bluesky-social/social-app/issues/1221
 */
function createCanonicalURL(profileID: string, releaseID: string) {
  return `https://bsky.app/profile/${profileID}/post/${releaseID}`;
}
