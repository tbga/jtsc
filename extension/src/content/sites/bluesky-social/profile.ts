import { validateError } from "@repo/core/errors";
import {
  attemptMetaTagContent,
  ensureMetaTagContent,
  ensureSelector,
  ensureTextContent,
} from "#lib/dom";
import { IProfileCollector } from "../../types";
import { NormalizedURL, getPathnameSegments } from "@repo/core/urls";

export const collectProfile: IProfileCollector = (errors) => {
  // server and client render result in a different markup
  const isClient = attemptMetaTagContent("profile:username")?.length === 0;

  let id: string | undefined;
  try {
    id = collectID(isClient);
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile ID", { cause: error }));
  }

  let name: string | undefined;
  try {
    name = ensureTextContent(`div[data-testid="profileHeaderDisplayName"]`);
  } catch (error) {
    validateError(error);

    errors.push(new Error("Faield to find profile name.", { cause: error }));
  }

  let handle: string | undefined;
  try {
    handle = collectHandle(isClient);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let description: string | undefined;
  try {
    description = !isClient
      ? ensureMetaTagContent(["description", "og:description"])
      : ensureTextContent(`[data-testid="profileHeaderDescription"]`);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectProfile> = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (handle) {
    data.other_names = [handle];
  }

  if (description) {
    data.description = description;
  }

  return data;
};

function collectID(isClient: boolean): string {
  let id: string;

  if (!isClient) {
    id = ensureMetaTagContent("twitter:value1");
  } else {
    const href = ensureSelector<HTMLLinkElement>(
      `link[rel="alternate"][type="application/json+oembed"]`
    ).href;
    const memeHref = new URL(href).searchParams.get("url");

    if (!memeHref) {
      throw new Error("Failed to find embed hyperlink.");
    }

    id = getPathnameSegments(new NormalizedURL(memeHref))[1];
  }

  return id;
}

function collectHandle(isClient: boolean) {
  let handle: string;

  if (!isClient) {
    handle = ensureMetaTagContent("profile:username");
  } else {
    const href = ensureMetaTagContent("og:url");
    handle = getPathnameSegments(new NormalizedURL(href))[1];
  }

  return handle;
}
function createCanonicalURL(profileID: string) {
  return `https://bsky.app/profile/${profileID}`;
}
