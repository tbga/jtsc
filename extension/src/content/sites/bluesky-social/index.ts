import { NormalizedURL, getPathnameSegments } from "@repo/core/urls";
import { UnknownSitePathError } from "../../errors";
import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: NormalizedURL): IURLType {
  const segments = getPathnameSegments(url);

  const isProfilePage = segments.length === 2 && segments[0] === "profile";

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage = segments.length === 4 && segments[2] === "post";

  if (isReleasePage) {
    return "release";
  }

  throw new UnknownSitePathError(url);
}
