import { validateError } from "@repo/core/errors";
import type { IProfileData } from "#messaging";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  let handle: string | undefined;
  try {
    const href = ensureSelector<HTMLLinkElement>(`link[rel="canonical"]`).href;
    const segments = new URL(href).pathname.split("/").slice(1);
    id = segments[2];
    handle = segments[1];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("h1");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectProfile> = {};

  if (id) {
    data.id = id;
  }

  if (name) {
    data.title = name;
  }

  if (handle) {
    data.other_names = [handle];
  }

  if (id && handle) {
    data.canonical_url = createCanonicalURL(handle, id);
  }

  return data;
};

function createCanonicalURL(handle: string, profileID: string) {
  return `https://www.facebook.com/people/${handle}/${profileID}`;
}
