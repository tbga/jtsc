import { NotImplementedError } from "@repo/core/errors";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  throw new NotImplementedError();
};
