export interface IProfileJSONLD {
  name: string;
  /**
   * Profile ID prepended with `/`.
   */
  url: string;
}

export interface IReleaseJSONLD {
  /**
   * Includes release ID.
   */
  url: string;
  name: string;
  uploadDate: string;
  dateModified: string;
}
