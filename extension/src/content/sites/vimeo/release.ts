import { ensureTextContent } from "#lib/dom";
import type { IReleaseData } from "#messaging";
import { validateError } from "@repo/core/errors";
import { IReleaseCollector } from "../../types";
import type { IReleaseJSONLD } from "./types";

export const collectRelease: IReleaseCollector = (errors) => {
  let jsonLD: IReleaseJSONLD | undefined;
  try {
    const jsonLDContent = ensureTextContent(
      `script[type="application/ld+json"]`
    );
    jsonLD = JSON.parse(jsonLDContent)[0];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const title = jsonLD?.name;

  let description: string | undefined;
  try {
    // description is incomplete in the JSONLD
    // therefore pulling it from the page
    description = ensureTextContent(
      `div[dir="auto"][data-description-content="true"]`
    );
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const createdAt = jsonLD?.uploadDate;
  const release: IReleaseData = {};

  if (jsonLD?.url) {
    const id = new URL(jsonLD.url).pathname.split("/").slice(1)[0];
    release.id = id;
    release.canonical_url = createCanonicalURL(id);
  }

  if (title) {
    release.title = title;
  }

  if (description) {
    release.description = description;
  }

  if (createdAt) {
    release.created_at = createdAt;
  }

  return release;
};

function createCanonicalURL(releaseID: string) {
  return `https://vimeo.com/${releaseID}`;
}
