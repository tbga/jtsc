import { ensureTextContent } from "#lib/dom";
import type { IProfileData } from "#messaging";
import { validateError } from "@repo/core/errors";
import { IProfileCollector } from "../../types";
import type { IProfileJSONLD } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let jsonLD: IProfileJSONLD | undefined;
  try {
    const content = ensureTextContent(`script[type="application/ld+json"]`);
    jsonLD = JSON.parse(content)[0];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const id = jsonLD?.url.trim().slice(1);
  const name = jsonLD?.name.trim();

  let bio: string | undefined;
  try {
    bio = ensureTextContent(
      `#__next div div div div div div div > p[format="basic"][size="200"]`
    );
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile bio.", { cause: error }));
  }

  let joinedAt: string | undefined;
  try {
    joinedAt = ensureTextContent(
      `#__next div div div div div div:nth-child(2) > div > span[format="soft"][size="300"]:nth-child(2)`
    );
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const profile: IProfileData = {};

  if (id) {
    profile.id = id;
    profile.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    profile.title = name;
  }

  if (bio) {
    profile.description = bio;
  }

  if (joinedAt) {
    profile.created_at = joinedAt;
  }

  return profile;
};

function createCanonicalURL(id: string) {
  return `https://vimeo.com/${id}`;
}
