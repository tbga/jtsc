import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

const numericStringRegExp = /^[0-9]+$/;

export function guessURLType(url: URL): IURLType {
  const pathnameSegments = url.pathname.split("/").slice(1);

  // profiles and releases are shared on the same segment
  // and they are only separated by shape
  const isProfilePage =
    pathnameSegments.length === 1 &&
    !numericStringRegExp.test(pathnameSegments[0]);

  if (isProfilePage) {
    return "profile";
  }

  // presumably release IDs are only numbers
  const isReleasePage =
    pathnameSegments.length === 1 &&
    numericStringRegExp.test(pathnameSegments[0]);

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
