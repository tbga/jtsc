import { validateError } from "@repo/core/errors";
import { ensureInnerText, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import type { IProfileInfo } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  let handle: string | undefined;
  try {
    const meta = collectMeta();
    id = meta.id;
    handle = meta.id;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("header > section > div > div > span");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let description: string | undefined;
  try {
    description = ensureInnerText("h1");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectProfile> = {};

  if (id) {
    data.id = id;
  }

  if (name) {
    data.title = name;
  }

  if (handle) {
    data.other_names = [handle];
    data.canonical_url = createCanonicalURL(handle);
  }

  if (description) {
    data.description = description;
  }

  return data;
};

function collectMeta(): { id: string; handle: string } {
  // there are several elements like that and the only differentiator
  // is `data-content-len`
  // assume the element with the highest value is the one needed
  const elements = Array.from(
    document.querySelectorAll<HTMLScriptElement>(
      `script[type="application/json"][data-content-len][data-sjs]`
    )
  );

  const lengthMapping = new Map(
    elements.map((element) => [element, Number(element.dataset.contentLen)])
  );
  const highestLength = Math.max(...lengthMapping.values());
  const pair = Array.from(lengthMapping).find(
    ([element, len]) => len === highestLength
  );

  if (!pair) {
    throw new Error("Failed to find profile info.");
  }

  const content = pair[0].textContent?.trim();

  if (!content) {
    throw new Error("Failed to find profile info content.");
  }

  const info: IProfileInfo = JSON.parse(content);
  const data =
    info.require[0][3][0].__bbox.require[3][3][0].initialRouteInfo.route;
  const id = data.rootView.props.id;
  const handle = data.params.username;

  return { id, handle };
}

function createCanonicalURL(profileID: string) {
  return `https://www.instagram.com/${profileID}`;
}
