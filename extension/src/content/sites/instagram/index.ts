import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  const isTrailingSlash = url.pathname.endsWith("/");
  const pathnameSegments = !isTrailingSlash
    ? url.pathname.split("/").slice(1)
    : url.pathname.split("/").slice(1, -1);

  const isProfilePage = pathnameSegments.length === 1;

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage =
    // the path without profile handle
    (pathnameSegments.length === 2 &&
      (pathnameSegments[0] === "reel" || pathnameSegments[0] === "p")) ||
    //the path with profile handle
    (pathnameSegments.length === 3 &&
      (pathnameSegments[1] === "reel" || pathnameSegments[1] === "p"));

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
