export interface IProfileInfo {
  require: [
    [
      undefined,
      undefined,
      undefined,
      [
        {
          __bbox: {
            require: [
              undefined,
              undefined,
              undefined,
              [
                undefined,
                undefined,
                undefined,
                [
                  {
                    initialRouteInfo: {
                      route: {
                        rootView: {
                          props: {
                            /**
                             * Possibly the internal ID of profile.
                             */
                            id: string;
                          };
                        };
                        params: {
                          /**
                           * The name used in the URL.
                           */
                          username: string;
                        };
                      };
                    };
                  }
                ]
              ],
              undefined,
              undefined,
              undefined,
              undefined
            ];
          };
        },
        undefined,
        undefined
      ]
    ]
  ];
}

export interface IReleaseInfo {
  require: [
    [
      undefined,
      undefined,
      undefined,
      [
        {
          __bbox: {
            define: [];
            require: [
              undefined,
              undefined,
              [
                undefined,
                undefined,
                undefined,
                [
                  {
                    initialRouteInfo: {
                      route: {
                        rootView: {
                          props: {
                            /**
                             * Seems to be some sort of internal ID.
                             */
                            media_id: string;
                          };
                        };
                        params: {
                          /**
                           * ID of release.
                           */
                          shortcode: string;
                        };
                      };
                    };
                  }
                ]
              ],
              undefined,
              undefined,
              undefined,
              undefined
            ];
          };
        },
        undefined,
        undefined
      ]
    ]
  ];
}
