import { validateError } from "@repo/core/errors";
import { ensureSelector } from "#lib/dom";
import { IReleaseCollector } from "../../types";
import type { IReleaseInfo } from "./types";

export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  let slug: string | undefined;
  try {
    const meta = collectMeta();
    id = meta.id;
    slug = meta.slug;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  let description: string | undefined;
  try {
    const content = collectTitleAndDescription();
    title = content.title;
    description = content.description;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let createdAt: string | undefined;
  try {
    createdAt = ensureSelector<HTMLTimeElement>(
      "article div div div a span time"
    ).dateTime;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectRelease> = {};

  if (id) {
    data.id = id;
  }

  if (title) {
    data.title = title;
  }

  if (description) {
    data.description = description;
  }

  if (createdAt) {
    data.created_at = createdAt;
  }

  if (slug) {
    data.canonical_url = createCanonicalURL(slug);
  }

  return data;
};

function collectMeta(): { id: string; slug: string } {
  // there are several elements like that and the only differentiator
  // is `data-content-len`
  // assume the element with the highest value is the one needed
  const elements = Array.from(
    document.querySelectorAll<HTMLScriptElement>(
      `script[type="application/json"][data-content-len][data-sjs]`
    )
  );

  const lengthMapping = new Map(
    elements.map((element) => [element, Number(element.dataset.contentLen)])
  );
  const highestLength = Math.max(...lengthMapping.values());
  const pair = Array.from(lengthMapping).find(
    ([element, len]) => len === highestLength
  );

  if (!pair) {
    throw new Error("Failed to find release info.");
  }

  const content = pair[0].textContent?.trim();

  if (!content) {
    throw new Error("Failed to find release info content.");
  }

  const info: IReleaseInfo = JSON.parse(content);
  const data =
    info.require[0][3][0].__bbox.require[2][3][0].initialRouteInfo.route;
  const id = data.rootView.props.media_id;
  const slug = data.params.shortcode;

  return { id, slug };
}

function collectTitleAndDescription(): {
  title?: string;
  description?: string;
} {
  const content = document.querySelector("h1")?.innerText.trim();

  if (!content) {
    return {};
  }

  const firstNewlineIndex = content.indexOf("\n");
  const title = content
    .slice(0, firstNewlineIndex === -1 ? undefined : firstNewlineIndex)
    .trim();

  const description = content
    .slice(firstNewlineIndex === -1 ? content.length : firstNewlineIndex)
    .trim();

  return {
    title: !title ? undefined : title,
    description: !description ? undefined : description,
  };
}

function createCanonicalURL(slug: string) {
  return `https://www.instagram.com/p/${slug}`;
}
