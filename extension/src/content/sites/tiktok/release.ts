import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  let profileName: string | undefined;
  let releaseID: string | undefined;
  try {
    const data = collectProfileNameAndReleaseID();
    profileName = data.profileName;
    releaseID = data.releaseID;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  try {
    title = ensureTextContent("h1");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find title.", { cause: error }));
  }

  let description: string | undefined;
  try {
    description = ensureTextContent(`[data-e2e="browse-music"]`);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let createdAt: string | undefined;
  try {
    createdAt = ensureTextContent(
      `span[data-e2e="browser-nickname"] span:nth-child(3)`
    );
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const release: ReturnType<typeof collectRelease> = {};

  if (releaseID) {
    release.id = releaseID;

    if (profileName) {
      release.canonical_url = createCanonicalReleaseURL(profileName, releaseID);
    }
  }

  if (title) {
    release.title = title;
  }

  if (description) {
    release.description = description;
  }

  if (createdAt) {
    release.created_at = createdAt;
  }

  return release;
};

function collectProfileNameAndReleaseID() {
  const href = ensureSelector<HTMLLinkElement>(`link[rel="canonical"]`)?.href;

  const url = new URL(href);
  const segments = url.pathname.split("/").slice(1);
  const profileName = segments[0];
  const releaseID = segments[2];

  return {
    profileName,
    releaseID,
  };
}

/**
 * URL is sensitive to profile name therefore it has to be provided.
 */
function createCanonicalReleaseURL(profileName: string, releaseID: string) {
  return `https://www.tiktok.com/${profileName}/video/${releaseID}`;
}
