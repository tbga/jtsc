export interface IProfileJSONLD {
  mainEntity: {
    name: string
    alternateName: string
    description: string
    /**
     * A URL which seems to contain real profile ID
     * in search parameters.
     */
    image: string
  }
}
