import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  // tiktok doesn't care about trailing slash,
  // but also doesn't redirect automatically,
  // so it has to be removed manually
  const isTrailingSlash = url.pathname.endsWith("/");
  const pathnameSegments = !isTrailingSlash
    ? url.pathname.split("/").slice(1)
    : url.pathname.split("/").slice(1, -1);

  const isProfilePage = pathnameSegments.length === 1;

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage =
    pathnameSegments.length === 3 && pathnameSegments[1] === "video";

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
