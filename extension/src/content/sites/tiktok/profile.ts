import { validateError } from "@repo/core/errors";
import { ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import type { IProfileJSONLD } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let profileJSONLD: IProfileJSONLD["mainEntity"] | undefined;
  try {
    const jsonLDstring = ensureTextContent("#ProfilePage");

    profileJSONLD = JSON.parse(jsonLDstring).mainEntity;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let profileID: string | null | undefined;
  try {
    profileID =
      profileJSONLD?.image &&
      new URL(profileJSONLD.image).searchParams.get("userId");

    if (!profileID) {
      throw new Error("Faield to find profile ID.");
    }
  } catch (error) {}

  const name = profileJSONLD?.name;
  const otherName = profileJSONLD?.alternateName;
  const description = profileJSONLD?.description;
  const profile: ReturnType<typeof collectProfile> = {
    description,
  };

  if (profileID) {
    profile.id = profileID;
  }

  if (name) {
    profile.title = name;
  }

  if (otherName) {
    profile.other_names = [otherName];
    profile.canonical_url = createCanonicalProfileURL(otherName);
  }

  if (description) {
    profile.description = description;
  }

  return profile;
};

/**
 * There doesn't seem to be a way to construct a URL
 * out of profile ID so using the name one instead.
 */
function createCanonicalProfileURL(userName: string) {
  return `https://www.tiktok.com/@${userName}`;
}
