import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  const segments = url.pathname.split("/").slice(1);

  const isProfilePage =
    // vanity url or used ID url
    segments.length === 1 ||
    // about page
    (segments.length === 2 && segments[1] === "about");

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage = false;

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
