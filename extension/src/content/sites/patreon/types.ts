export interface IProfileNEXTJSData {
  props: {
    pageProps: {
      bootstrapEnvelope: {
        pageBootstrap: {
          campaign: {
            data: {
              attributes: {
                /**
                 * Name of profile.
                 */
                name: string;
                /**
                 * Other name of profile.
                 */
                vanity?: string;
                /**
                 * Kind of description.
                 */
                creation_name: string;
                /**
                 * Kind of description.
                 */
                one_liner?: string;
                /**
                 * Looks like profile description.
                 */
                summary: string;
                /**
                 * Join date of profile.
                 */
                published_at: string;
              };
              relationships: {
                creator: {
                  data: {
                    /**
                     * ID of profile.
                     */
                    id: string
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}

export interface IProfileLDJSON {
  about: {
    /**
     * A `summary` of profile.
     */
    description: string
  }
}
