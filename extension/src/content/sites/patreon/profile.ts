import { validateError } from "@repo/core/errors";
import type { IProfileData } from "#messaging";
import { ensureInnerText, selectLDJSON } from "#lib/dom";
import { IProfileCollector } from "../../types";
import type { IProfileLDJSON, IProfileNEXTJSData } from "./types";

type IProfileInfo =
  IProfileNEXTJSData["props"]["pageProps"]["bootstrapEnvelope"]["pageBootstrap"]["campaign"]["data"];

export const collectProfile: IProfileCollector = (errors) => {
  let profileInfo: IProfileInfo | undefined;
  try {
    profileInfo = getProfileInfoFromNextjsState();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const id = profileInfo?.relationships.creator.data.id.trim();
  const name = profileInfo?.attributes.name.trim();
  const otherName = profileInfo?.attributes.vanity?.trim();

  let description: string | undefined;
  try {
    description = profileInfo && getDescription(profileInfo);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const joinedAt = profileInfo?.attributes.published_at.trim();

  const profile: IProfileData = {};

  if (id) {
    (profile.id = id), (profile.canonical_url = createCanonicalURL(id));
  }

  if (name) {
    profile.title = name;
  }

  if (otherName) {
    profile.other_names = [otherName];
  }

  if (description) {
    profile.description = description;
  }

  if (joinedAt) {
    profile.created_at = joinedAt;
  }

  return profile;
};

function getProfileInfoFromNextjsState(): IProfileInfo {
  const jsonContent = ensureInnerText("#__NEXT_DATA__");

  const jsonData: IProfileNEXTJSData = JSON.parse(jsonContent);
  const profileInfo =
    jsonData.props.pageProps.bootstrapEnvelope.pageBootstrap.campaign.data;

  return profileInfo;
}

function getDescription(profileInfo: IProfileInfo): string | undefined {
  // the description on the page is only visible on `about` page
  // and the one in the page state is lathed with HTML tags
  const ldJSON = selectLDJSON<IProfileLDJSON>();
  const summary = ldJSON.about.description;
  const description = [
    profileInfo.attributes.creation_name.trim(),
    profileInfo.attributes.one_liner?.trim(),
    summary.trim(),
  ]
    .filter((text) => text)
    .join("\n\n");

  return description.length === 0 ? undefined : description;
}

function createCanonicalURL(id: string) {
  const url = new URL("https://www.patreon.com/user");

  url.searchParams.set("u", id);

  return String(url);
}
