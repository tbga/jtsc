import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  const segments = url.pathname.split("/").slice(1);

  const isProfilePage =
    // not a release page
    (segments.length === 1 &&
      !(url.searchParams.has("v") || segments[0] === "playlist")) ||
    (segments.length === 2 &&
      // profile with ID
      (segments[0] === "channel" ||
        // subpages of profile
        segments[1] === "featured" ||
        segments[1] === "videos" ||
        segments[1] === "shorts" ||
        segments[1] === "streams" ||
        segments[1] === "playlists" ||
        segments[1] === "community" ||
        segments[1] === "search" ||
        segments[1] === "about")) ||
    // subpages of profile ID page
    (segments.length === 3 &&
      segments[0] === "channel" &&
      (segments[2] === "featured" ||
        segments[2] === "videos" ||
        segments[2] === "shorts" ||
        segments[2] === "streams" ||
        segments[2] === "playlists" ||
        segments[2] === "community" ||
        segments[2] === "search" ||
        segments[2] === "about"));

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage =
    segments.length === 1 &&
    segments[0] === "watch" &&
    url.searchParams.has("v");

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
