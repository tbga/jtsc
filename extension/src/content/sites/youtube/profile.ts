import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import type { IProfileCollector } from "../../types";

export const collectProfile: IProfileCollector = (errors) => {
  // profile can be arrived in two ways:
  // 1. direct navigaton by URL
  // 2. client navigation from release
  // in case of latter the SEO tags will be the same as release one
  // therefore unusable
  const isClient =
    document.querySelectorAll("#items a[force-new-state]").length !== 0;

  let profileID: string | undefined;
  try {
    profileID = collectProfileID(isClient);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureSelector<HTMLLinkElement>(
      `[itemprop="author"] > [itemprop="name"]`
    )
      .getAttribute("content")
      ?.trim();
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile name.", { cause: error }));
  }

  let handle: string | undefined;
  try {
    handle = ensureTextContent([
      "p #channel-handle",
      `#page-header yt-content-metadata-view-model div:first-child span[role="text"]`,
    ])
      // slice to get rid of `@`
      .slice(1);
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find profile other name.", { cause: error })
    );
  }

  let description: string | undefined;
  try {
    description = !isClient
      ? document
          .querySelector<HTMLMetaElement>(`meta[property="og:description"]`)
          ?.content.trim()
      : ensureTextContent("#about-container #description-container span");
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find profile description.", { cause: error })
    );
  }

  // extra info container is only shown by clicking on it
  // so it's not always present
  let joinDate: string | undefined;
  try {
    joinDate = ensureTextContent(
      "#additional-info-container > table > tbody > tr:nth-child(7) span > span"
    );
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find profile join date.", { cause: error })
    );
  }

  const profile: ReturnType<typeof collectProfile> = {};

  if (profileID) {
    profile.id = profileID;
    profile.canonical_url = createCanonicalProfileURL(profileID);
  }

  if (name) {
    profile.title = name;
  }

  if (description) {
    profile.description = description;
  }

  if (handle) {
    profile.other_names = [handle];
  }

  if (joinDate) {
    profile.created_at = joinDate;
  }

  return profile;
};

function collectProfileID(isClient: boolean): string {
  let profileID: string | undefined = undefined;

  if (isClient) {
    const href = document.querySelector<HTMLAnchorElement>(
      "#infocard-channel-button a"
    )?.href;

    profileID = !href ? undefined : getProfileIDFromURL(href);
  } else {
    // there is no point for checking plethora of SEO tags
    // since they only exist within the same context as this one
    profileID = document.querySelector<HTMLMetaElement>(
      `meta[itemprop="identifier"]`
    )?.content;
  }

  if (!profileID) {
    throw new Error("Failed to find profile ID.");
  }

  return profileID;
}

function getProfileIDFromURL(inputURL: string | URL) {
  const url = typeof inputURL !== "string" ? inputURL : new URL(inputURL);

  const id: string | undefined = url.pathname.split("/").slice(1)[1];

  return id;
}

function createCanonicalProfileURL(profileID: string) {
  return `https://www.youtube.com/channel/${profileID}`;
}
