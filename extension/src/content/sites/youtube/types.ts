export interface IReleaseJSONLD {
  name: string
  description: string
  /**
   * Contains the release ID
   */
  embedUrl: string
  uploadDate: string
}
