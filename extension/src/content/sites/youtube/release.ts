import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IReleaseCollector } from "../../types";
import type { IReleaseJSONLD } from "./types";

export const collectRelease: IReleaseCollector = (errors) => {
  let jsonLD;
  try {
    jsonLD = collectJSONLD();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const releaseID = jsonLD?.id ?? collectReleaseID();
  const releaseDate = jsonLD?.uploadDate ?? collectReleaseDate();

  let title: string | undefined;
  try {
    title =
      jsonLD?.name.trim() ??
      ensureSelector<HTMLMetaElement>(`meta[itemprop="name"]`).content.trim();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let description: string | undefined;
  try {
    description =
      jsonLD?.description.trim() ??
      ensureTextContent("#description-inline-expander > yt-attributed-string");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const release: ReturnType<typeof collectRelease> = {};

  if (releaseID) {
    release.id = releaseID;
    release.canonical_url = createCanonicalReleaseURL(releaseID);
  }

  if (title) {
    release.title = title;
  }

  if (description) {
    release.description = description;
  }

  if (releaseDate) {
    release.created_at = releaseDate;
  }

  return release;
};

function collectJSONLD(): IReleaseJSONLD & { id: string } {
  const content = ensureSelector<HTMLScriptElement>(
    `#microformat script[type="application/ld+json"]`
  ).innerText;

  const jsonLD: IReleaseJSONLD & { id: string } = JSON.parse(content);
  const id = new URL(jsonLD.embedUrl).pathname.split("/").slice(1)[2];
  jsonLD.id = id;

  return jsonLD;
}

function collectReleaseID(): string | undefined {
  const releaseID =
    document.querySelector<HTMLMetaElement>(`meta[itemprop="identifier"]`)
      ?.content ??
    document.querySelector("ytd-watch-flexy")?.getAttribute("video-id") ??
    undefined;

  return releaseID;
}

function collectReleaseDate(): string | undefined {
  const releaseDate = document.querySelector<HTMLMetaElement>(
    `meta[itemprop="datePublished"]`
  )?.content;

  return releaseDate;
}

function createCanonicalReleaseURL(releaseID: string) {
  const url = new URL("https://www.youtube.com/watch");
  url.searchParams.set("v", releaseID);

  return String(url);
}
