import { validateError } from "@repo/core/errors";
import type { IProfileData } from "#messaging";
import { ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";
import type { IProfileJSONLD } from "./types";

export const collectProfile: IProfileCollector = (errors) => {
  let jsonLD: IProfileJSONLD | undefined;
  try {
    jsonLD = collectJSONLD();
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile JSONLD.", { cause: error }));
  }

  const mainEntity = jsonLD?.mainEntity;
  const id = mainEntity?.identifier;
  const title = mainEntity?.name;
  const otherName = mainEntity?.alternateName;
  const description = mainEntity?.description;
  const createdAt = jsonLD?.dateCreated;

  const data: IProfileData = {};

  if (id) {
    data.id = String(id);
  }

  if (title) {
    data.title = title;
  }

  if (otherName) {
    data.other_names = [otherName];
    data.canonical_url = createCanonicalURL(otherName);
  }

  if (description) {
    data.description = description;
  }

  if (createdAt) {
    data.created_at = createdAt;
  }

  return data;
};

function collectJSONLD(): IProfileJSONLD {
  const content = ensureTextContent(`script[type="application/ld+json"]`);
  const jsonLD: IProfileJSONLD = JSON.parse(content);

  return jsonLD;
}

/**
 * There doesn't seem to be a way to construct the URL
 * from ID.
 */
function createCanonicalURL(profileName: string) {
  return `https://onlyfans.com/${profileName}`;
}
