import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import type { IReleaseData } from "#messaging";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  let id: string | undefined;
  try {
    id = ensureSelector(".b-post").id.split("_")[1];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  try {
    title = ensureTextContent(".g-truncated-text");
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let profileName: string | undefined;
  try {
    profileName = ensureSelector(".g-user-username")
      .textContent?.trim()
      .slice(1);
  } catch (error) {
    validateError(error);

    errors.push(
      new Error("Failed to find profile name of release.", { cause: error })
    );
  }

  let datePublished: string | undefined;
  try {
    // It doesn't look like it stores the timestamp somewhere in the page
    // so using the formatted one
    datePublished = ensureSelector<HTMLSpanElement>(
      ".b-post__date:not(.b-post__date-remaining) > span"
    ).title;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: IReleaseData = {
    created_at: datePublished,
  };

  if (id) {
    data.id = id;

    if (profileName) {
      data.canonical_url = createCanonicalURL(id, profileName);
    }
  }

  if (title) {
    data.title = title;
  }

  if (datePublished) {
    data.created_at = datePublished;
  }

  return data;
};

function createCanonicalURL(releaseID: string, profileName: string) {
  return `https://onlyfans.com/${releaseID}/${profileName}`;
}
