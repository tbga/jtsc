export interface IProfileJSONLD {
  dateCreated: string;
  mainEntity: {
    name: string;
    /**
     * The name used in the URL.
     */
    alternateName: string;
    identifier: number;
    description: string;
  };
}
