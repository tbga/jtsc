import { validateError } from "@repo/core/errors";
import { IReleaseCollector } from "../../types";

/**
 * Collects release data from DOM.
 */
export const collectRelease: IReleaseCollector = (errors) => {
  let releaseElement: HTMLElement | undefined;
  try {
    releaseElement = findReleaseElement();
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  let description: string | undefined;
  try {
    const contentElement = releaseElement?.querySelector<HTMLDivElement>(
      `[data-testid="tweetText"]`
    );

    const content = parseContent(contentElement);
    title = content.title;
    description = content.description;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let createdAtElement: HTMLTimeElement | undefined | null;
  try {
    createdAtElement =
      releaseElement?.querySelector<HTMLTimeElement>("a > time");

    if (!createdAtElement) {
      throw new Error(
        "Failed to find creation timestamp element of the release."
      );
    }
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let releaseLinkElement: HTMLAnchorElement | null | undefined;
  try {
    // the timestamp is wrapped in the link to the release
    releaseLinkElement = createdAtElement?.parentElement as
      | HTMLAnchorElement
      | null
      | undefined;

    if (!releaseLinkElement) {
      throw new Error("Failed to find link element of the release.");
    }
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const release: ReturnType<typeof collectRelease> = {};

  if (releaseLinkElement) {
    const id = new URL(releaseLinkElement.href).pathname.slice(1).split("/")[2];
    release.id = id;
    release.canonical_url = createCanonicalReleaseURL(id);
  }

  if (title) {
    release.title = title;
  }

  if (description) {
    release.description = description;
  }

  if (createdAtElement) {
    release.created_at = createdAtElement.dateTime;
  }

  return release;
};

function findReleaseElement(): HTMLElement {
  // collecting ID from URL because a reply tweet
  // is not guaranteed first `<article>` on the page
  const urlID = new URL(location.href).pathname.slice(1).split("/")[2];
  // releases don't have their JSONLD meta
  // so it has to be collected straight from body
  const releaseElement = Array.from(document.querySelectorAll("article")).find(
    (element) => {
      const createdAtElement =
        element.querySelector<HTMLTimeElement>("a > time");

      if (!createdAtElement) {
        throw new Error(
          "Failed to find creation timestamp element of the release."
        );
      }

      // the timestamp is wrapped in the link to the release
      const releaseLinkElement = createdAtElement?.parentElement as
        | HTMLAnchorElement
        | null
        | undefined;

      if (!releaseLinkElement) {
        throw new Error("Failed to find link element of the release.");
      }

      const tweetID = new URL(releaseLinkElement.href).pathname
        .slice(1)
        .split("/")[2];

      return urlID === tweetID;
    }
  );

  if (!releaseElement) {
    throw new Error(
      `Failed to find \`<article>\` element for the release "${urlID}".`
    );
  }

  return releaseElement;
}

function parseContent(contentElement: HTMLDivElement | null | undefined): {
  title?: string;
  description?: string;
} {
  const result: ReturnType<typeof parseContent> = {};
  const content = contentElement?.textContent?.trim();

  if (!content || content.length === 0) {
    return result;
  }

  // twitter releases do not separate title from description
  // so it's assumed the first line is a title and the rest is description
  const firstNewLineIndex = content.indexOf("\n");
  // `Array.slice()` supports negative indices in the arguments
  // but it has a different semantics from return of `Array.indexOf()`.
  const isNewline = firstNewLineIndex !== -1;
  const title = !isNewline
    ? content
    : content.slice(0, firstNewLineIndex).trim() || undefined;
  const description = !isNewline
    ? undefined
    : content.slice(firstNewLineIndex).trim() || undefined;

  result.title = title;
  result.description = description;

  return result;
}

function createCanonicalReleaseURL(releaseID: string) {
  return `https://twitter.com/i/status/${releaseID}`;
}
