import type { IURLType } from "../../types";

export { collectProfile } from "./profile";
export { collectRelease } from "./release";

export function guessURLType(url: URL): IURLType {
  // twitter doesn't care about trailing slash,
  // but also doesn't redirect automatically,
  // so it has to be removed manually
  const isTrailingSlash = url.pathname.endsWith("/");
  const pathnameSegments = !isTrailingSlash
    ? url.pathname.split("/").slice(1)
    : url.pathname.split("/").slice(1, -1);

  const isProfilePage =
    // the path is a profile handle
    pathnameSegments.length === 1 ||
    // the path is a profile subpage
    (pathnameSegments.length === 2 &&
      (pathnameSegments[1] === "with_replies" ||
        pathnameSegments[1] === "highlights" ||
        pathnameSegments[1] === "media" ||
        pathnameSegments[1] === "likes")) ||
    // the path is a profile ID lookup
    (pathnameSegments.length === 3 && pathnameSegments[1] === "user");

  if (isProfilePage) {
    return "profile";
  }

  const isReleasePage =
    // basic release path
    (pathnameSegments.length === 3 && pathnameSegments[1] === "status") ||
    // file view of the release
    (pathnameSegments.length === 5 && pathnameSegments[1] === "status");

  if (isReleasePage) {
    return "release";
  }

  const search = url.searchParams.size === 0 ? "" : url.search;
  const errorMessage = `Unknown path "${url.pathname}${search}".`;

  throw new Error(errorMessage);
}
