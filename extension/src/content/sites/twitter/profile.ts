import { ensureTextContent } from "#lib/dom";
import { validateError } from "@repo/core/errors";
import { IProfileCollector } from "../../types";
import type { ITwitterJSONLD } from "./types";

/**
 * @TODO check for suspended/not found profile page
 */
export const collectProfile: IProfileCollector = (errors) => {
  let parsedJSONLD: ITwitterJSONLD | undefined;
  try {
    const jsonContent = ensureTextContent(
      "head > script[data-testid='UserProfileSchema-test']"
    );

    parsedJSONLD = JSON.parse(jsonContent);
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to parse profile JSONLD.", { cause: error }));
  }

  const author = parsedJSONLD?.author;

  const name = author?.givenName.trim();
  const handle = author?.additionalName.trim();
  const description = author?.description.trim();
  const created_at = parsedJSONLD?.dateCreated.trim();
  const profile: ReturnType<typeof collectProfile> = {};

  if (author) {
    const id = author.identifier.trim();
    profile.id = id;
    profile.canonical_url = createCanonicalProfileURL(id);
  }

  if (name) {
    profile.title = name;
  }

  if (handle) {
    profile.other_names = [handle];
  }

  if (description) {
    profile.description = description;
  }

  if (created_at) {
    profile.created_at = created_at;
  }

  return profile;
};

function createCanonicalProfileURL(profileID: string) {
  return `https://twitter.com/i/user/${profileID}`;
}
