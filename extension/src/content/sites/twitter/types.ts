export interface ITwitterJSONLD {
  author: ITwitterJSONLDAuthor;
  dateCreated: string;
}

export interface ITwitterJSONLDAuthor {
  givenName: string;
  additionalName: string;
  description: string;
  identifier: string;
}
