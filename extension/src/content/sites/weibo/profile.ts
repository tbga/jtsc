import { validateError } from "@repo/core/errors";
import { ensureSelector, ensureTextContent } from "#lib/dom";
import { IProfileCollector } from "../../types";

export const collectProfile: IProfileCollector = (errors) => {
  let id: string | undefined;
  try {
    id = ensureSelector<HTMLDivElement>("#__sidebar div[uid]")
      .getAttribute("uid")
      ?.trim();

    if (!id) {
      throw new Error("Failed to find profile ID.");
    }
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let name: string | undefined;
  try {
    name = ensureTextContent("main div div div div div div div div div");
  } catch (error) {
    validateError(error);

    errors.push(new Error("Failed to find profile name", { cause: error }));
  }

  let description: string | undefined;
  try {
    // description changes its relative position when expanded
    const selectors = [
      "main > div > div > div > div > div > div > div > div > div > div:nth-child(3) > div > div > div:last-child",
      "main > div > div > div > div > div > div > div > div > div > div:nth-child(2) > div > div > div:last-child",
    ];
    description = ensureTextContent(selectors);
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let joinedAt: string | undefined;
  try {
    // the date is not present when the profile data is not expanded
    joinedAt = ensureTextContent(
      "main > div > div > div > div > div > div > div > div:nth-child(4) > div > div > div:last-child"
    ).split(" ")[0];
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectProfile> = {};

  if (id) {
    data.id = id;
    data.canonical_url = createCanonicalURL(id);
  }

  if (name) {
    data.title = name;
  }

  if (description) {
    data.description = description;
  }

  if (joinedAt) {
    data.created_at = joinedAt;
  }

  return data;
};

function createCanonicalURL(profileID: string) {
  return `https://weibo.com/u/${profileID}`;
}
