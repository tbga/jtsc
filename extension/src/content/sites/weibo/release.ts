import { ensureSelector, ensureTextContent } from "#lib/dom";
import { validateError } from "@repo/core/errors";
import { IReleaseCollector } from "../../types";

export const collectRelease: IReleaseCollector = (errors) => {
  let profileID: string | undefined;
  let releaseID: string | undefined;
  try {
    const ids = collectIDs();
    profileID = ids.profileID;
    releaseID = ids.releaseID;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let title: string | undefined;
  let description: string | undefined;
  try {
    const content = collectTitleAndDescription();
    title = content.title;
    description = content.description;
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  let createdAt: string | undefined;
  try {
    createdAt = ensureTextContent(
      "article header > div > div > div:nth-child(2) > a"
    );
  } catch (error) {
    validateError(error);

    errors.push(error);
  }

  const data: ReturnType<typeof collectRelease> = {};

  if (releaseID) {
    data.id = releaseID;

    if (profileID) {
      data.canonical_url = createCanonicalURL(profileID, releaseID);
    }
  }

  if (title) {
    data.title = title;
  }

  if (description) {
    data.description = description;
  }

  if (createdAt) {
    data.created_at = createdAt;
  }

  return data;
};

function collectIDs(): {
  profileID: string;
  releaseID: string;
} {
  const href = ensureSelector<HTMLAnchorElement>(
    "article header > div > div > div:nth-child(2) > a"
  ).href;

  const [profileID, releaseID] = new URL(href).pathname.split("/").slice(1);

  return { profileID, releaseID };
}

function collectTitleAndDescription(): {
  title?: string;
  description?: string;
} {
  const content = ensureSelector<HTMLDivElement>(
    "article > div > div > div:first-child > div"
  ).innerText?.trim();

  if (!content) {
    return {};
  }

  const firstNewlineIndex = content.indexOf("\n");
  const title = content
    .slice(0, firstNewlineIndex === -1 ? undefined : firstNewlineIndex)
    .trim();

  const description = content
    .slice(firstNewlineIndex === -1 ? content.length : firstNewlineIndex)
    .trim();

  return {
    title: !title ? undefined : title,
    description: !description ? undefined : description,
  };
}

function createCanonicalURL(profileID: string, releaseID: string) {
  return `https://weibo.com/${profileID}/${releaseID}`;
}
