import { matchPattern, presets } from "browser-extension-url-match";
import type { Matcher } from "browser-extension-url-match/dist/types";
import type { IProfileData, IReleaseData } from "#messaging";

export type IProfileCollector = (errors: Error[]) => IProfileData;

export type IReleaseCollector = (errors: Error[]) => IReleaseData;

const SUPPORTED_ORIGIN = {
  TWITTER: ["https://x.com/*", "https://twitter.com/*"],
  YOUTUBE: "https://www.youtube.com/*",
  REDDIT: "https://old.reddit.com/*",
  TIKTOK: "https://www.tiktok.com/*",
  VIMEO: "https://vimeo.com/*",
  PATREON: "https://www.patreon.com/*",
  BILIBILI: ["https://www.bilibili.com/*", "https://space.bilibili.com/*"],
  FANBOX: "https://*.fanbox.cc/*",
  GUMROAD: "https://*.gumroad.com/*",
  FANTIA: "https://fantia.jp/*",
  ONLYFANS: "https://onlyfans.com/*",
  DOUYIN: "https://www.douyin.com/*",
  WEIBO: "https://weibo.com/*",
  INSTAGRAM: "https://www.instagram.com/*",
  FACEBOOK: "https://www.facebook.com/*",
  BLUESKY_SOCIAL: "https://bsky.app/*"
} as const;

export type ISupportedOrigin = keyof typeof SUPPORTED_ORIGIN;

const matcherMap = new Map<keyof typeof SUPPORTED_ORIGIN, Matcher>(
  Object.entries(SUPPORTED_ORIGIN).map(([originKey, patterns]) => {
    const matcher = matchPattern(
      patterns as string | string[],
      presets.firefox
    ).assertValid();

    return [originKey as ISupportedOrigin, matcher];
  })
);

export function guessOrigin(inputURL: string): ISupportedOrigin {
  const url = new URL(inputURL);
  const origin = url.origin;
  let originKey: ISupportedOrigin | undefined = undefined;

  for (const [key, matcher] of matcherMap) {
    if (matcher.match(origin)) {
      originKey = key;
      break;
    }
  }

  if (!originKey) {
    throw new Error(`Unknown origin "${url.origin}".`);
  }

  return originKey;
}

const urlTypes = ["profile", "release"] as const;
export type IURLType = (typeof urlTypes)[number];
