import { homedir } from "node:os";
import { cwd } from "node:process";
import path from "node:path";
import { createReadStream } from "node:fs";
import fs from "node:fs/promises";
import { createHash } from "node:crypto";
import { parseArgs } from "node:util";

/**
 * @typedef {ReadonlyMap<string, string>} IFolderHashMap
 */

/**
 * A mapping of relative paths and their left/right hashes.
 *
 * `undefined` hash means the file is missing on one side.
 * @typedef {Map<string, [string?, string?]>} IHashMismatchesMap
 */

const currentWorkingDirectory = cwd();
/**
 * @type {import("node:util").ParseArgsConfig}
 */
const config = {
  allowPositionals: true,
};

const { positionals } = parseArgs(config);

if (positionals.length !== 2) {
  throw new Error("Must have exactly 2 arguments.");
}

const [left, right] = positionals.map((pathname) => {
  return path.isAbsolute(pathname)
    ? pathname
    : path.resolve(currentWorkingDirectory, pathname);
});
// @ts-expect-error scripts runs at NodeJS 20+
// so top level await is supported
// unlike the browser extension code
await compareFolders(left, right);

/**
 * @param {string} left
 * @param {string} right
 */
async function compareFolders(left, right) {
  const leftHashMap = await collectFolderHashes(left);
  const rightHashMap = await collectFolderHashes(right);
  const hashMismatches = collectHashMismatches(leftHashMap, rightHashMap);
  const leftPath = formatPath(left);
  const rightPath = formatPath(right);

  if (hashMismatches.size === 0) {
    console.log(`Folders "${leftPath}" and "${rightPath}" are equal.`);

    return;
  }

  console.error(`Mismatched folders "${leftPath}" and "${rightPath}" equal:`);
  for (const [pathName, [leftHash, rightHash]] of hashMismatches) {
    const formattedLeftHash = leftHash ?? "Missing";
    const formattedRightHash = rightHash ?? "Missing";
    console.log(`"${pathName}": ${formattedLeftHash} | ${formattedRightHash}`);
  }

  return;
}

/**
 * @param {string} folderPath
 * @returns {Promise<IFolderHashMap>}
 */
async function collectFolderHashes(folderPath) {
  const hashMap = new Map();
  const folder = await fs.opendir(folderPath, {
    encoding: "utf-8",
    recursive: true,
  });

  for await (const entry of folder) {
    if (entry.isDirectory()) {
      continue;
    }

    const entryPath = path.join(entry.path, entry.name);
    const relativePath = path.relative(folderPath, entryPath);

    if (!entry.isFile()) {
      const relativeFolderPath = formatPath(folderPath);

      throw new Error(
        `The path "${relativePath}" of folder "${relativeFolderPath}" is not a file.`
      );
    }

    const hash = await collectFileSHA256Hash(entryPath);

    hashMap.set(relativePath, hash);
  }

  return hashMap;
}

/**
 * @param {string} filePath
 * @returns {Promise<string>}
 */
async function collectFileSHA256Hash(filePath) {
  const hashCalc = createHash("sha256");
  /**
   * @type {string}
   */
  const hash = await new Promise((resolve, reject) => {
    const fileStream = createReadStream(filePath);

    fileStream.on("data", (chunk) => {
      hashCalc.update(chunk);
    });

    fileStream.on("error", (error) => {
      reject(error);
    });

    fileStream.on("end", () => {
      const hashValue = hashCalc.digest("hex");

      resolve(hashValue);
    });
  });

  return hash;
}

/**
 * @param {IFolderHashMap} leftHashMap
 * @param {IFolderHashMap} rightHashMap
 * @returns {IHashMismatchesMap}
 */
function collectHashMismatches(leftHashMap, rightHashMap) {
  /**
   * @type {IHashMismatchesMap}
   */
  const hashMismatches = new Map();
  /**
   * @type {Set<string>}
   */
  const matchedPaths = new Set();

  for (const [filePath, leftHash] of leftHashMap) {
    const rightHash = rightHashMap.get(filePath);

    if (leftHash === rightHash) {
      matchedPaths.add(filePath);
      continue;
    }

    hashMismatches.set(filePath, [leftHash, rightHash]);
  }

  for (const [filePath, rightHash] of rightHashMap) {
    if (matchedPaths.has(filePath)) {
      continue;
    }

    const leftHash = leftHashMap.get(filePath);

    if (rightHash === leftHash) {
      continue;
    }

    hashMismatches.set(filePath, [leftHash, rightHash]);
  }

  return hashMismatches;
}

/**
 * @param {string} pathname
 */
function formatPath(pathname) {
  const relativeFolderPath = pathname.replace(homedir(), "{HOME}");

  return relativeFolderPath;
}
