// @ts-check
const path = require("node:path");
const { cwd } = require("node:process");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const outputPath = path.resolve(__dirname, "dist");

/**
 * @type {import("webpack").Configuration}
 */
const configuration = {
  target: "web",
  entry: {
    popup: { import: "./src/popup/index.tsx", filename: "popup/index.js" },
    content: { import: "./src/content/index.ts", filename: "content/index.js" },
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Popup",
      filename: "popup/index.html",
      template: "./src/popup/index.html",
      chunks: ["popup"],
    }),
    new MiniCssExtractPlugin(),
    new CopyPlugin({
      patterns: [
        { from: "./manifest.json", to: path.join(outputPath, "manifest.json") },
        { from: "./assets", to: path.join(outputPath, "assets") },
      ],
    }),
  ],
  module: {
    parser: {
      javascript: {
        strictExportPresence: true,
      },
    },
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(s[ac]|c)ss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  cache: {
    type: "filesystem",
  },
  output: {
    filename: "[name].js",
    path: outputPath,
    clean: true,
  },
};

module.exports = configuration;
