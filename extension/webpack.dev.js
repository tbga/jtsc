// @ts-check
const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common.js");

/**
 * @type {import("webpack").Configuration}
 */
const developmentConfig = {
  mode: "development",
  devtool: "inline-source-map"
};

module.exports = merge(commonConfig, developmentConfig);
