# JTSC

A program to organize metadata of Eastern, European and Eastern-European art.

## Requirements

- NodeJS 20.1+
- PostgreSQL 13+
- PGRoonga 2+

## Installation

Follow the [installation guide](./docs/installation.md).

## Background Service

### Linux

#### Installation

Requirements:

- [`systemd`](https://systemd.io)

1. Copy [sample service file](./configs/platform/linux/jtsc.service) to `/lib/systemd/system`
2. Replace `<user>` and `<repo_path>` with your values.
3. Reload systemd service:

   ```sh
   sudo systemctl daemon-reload
   ```

4. Start up the service:

   ```sh
   sudo systemctl start jtsc
   ```

#### Manage

See application logs:

```sh
journalctl --boot --unit="jtsc.service"
```

Update the service:

- <repo_path> - a path to the repo

```sh
cd <repo_path>
sudo systemctl stop jtsc.service && git fetch --prune && git pull && npm run install-all && npm run build-all && sudo systemctl start jtsc.service
```

## Develop

### Generate HTTPS Keys

```sh
# check if you have openssl installed
openssl version
# create key
openssl genrsa -out ./backend/configs/development/key.pem
# create a request
openssl req -new -key ./backend/configs/development/key.pem -out ./backend/configs/development/csr.pem
# sign your key
openssl x509 -req -days 9999 -in ./backend/configs/development/csr.pem -signkey ./backend/configs/development/key.pem -out ./backend/configs/development/cert.pem
```

### Development Server

Start the server:

```sh
npm run develop
```

### Database Management

- Run migrations:

  ```sh
  npm run database:migrate
  ```

- Drop all tables and apply migrations"

  ```sh
  npm run database:remigrate
  ```

- Generate mock data:

  ```sh
  npm run database:generate
  ```

- Reset mock data (does not remove tables):

  ```sh
  npm run database:reset
  ```

- Reset and generate mock data:

  ```sh
  npm run database:regenerate
  ```

- Clean all system folders and drop all tables:

  ```sh
  npm run database:wipe
  ```

- Clean all system folders, drop all tables, run migrations and generate mock data:

  ```sh
  npm run database:cleanse
  ```

### Codegen

Run codegen:

```sh
npm run codegen
```

## Database

### Create Backup

- `<folder_path>` - the path to the output folder
- `<database_url>` - [PostgreSQL connection URI](https://www.postgresql.org/docs/13/libpq-connect.html#id-1.7.3.8.3.6)

```sh
pg_dump --format=directory --file=<folder_path> <database_url>
```

### Restore Backup

- `<database_url>` - [PostgreSQL connection URI](https://www.postgresql.org/docs/13/libpq-connect.html#id-1.7.3.8.3.6)
- `<backup_path>` - the path to the input folder

```sh
pg_restore --dbname=<database_url> --clean --if-exists --single-transaction <backup_path>
```

## Troubleshooting

- Windows users cannot create symblic links on non-admin accounts.
- Run `cd apps/pages && npx next telemetry disable` to fix [`ENOWORKSPACES` error](https://github.com/vercel/turborepo/issues/4183#issuecomment-1668737319).
